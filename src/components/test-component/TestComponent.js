import React from 'react'
import './test-component.style.scss'
import nextId from 'react-id-generator'
export default function TestComponent() {
    const [checkedId, setCheckedId] = React.useState("id1");
    return (
        <div className="asset-section-body">
            {charts.map(obj => <input className="d-none" checked={obj.id === checkedId} id={obj.id} type="radio" name="charts" />)}

            <div className={`charts-wrapper`}>
                {
                    charts.map(obj =>
                        <label
                            onClick={() => setCheckedId(obj.id)}
                            htmlFor={obj.id} id={obj.forId}
                            // style={fullScreen ? { top: 0, left: 0, width: "100%" } : {}}
                            className="chart-container position-absolute"
                        >
                            <div className="custom-card">{obj.id}</div>
                            </label>)
                }
            </div>
        </div>
    )
}

const charts = [
    { keyId: nextId(), forId: "cId1", id: "id1" },
    { keyId: nextId(), forId: "cId2", id: "id2" },
    { keyId: nextId(), forId: "cId3", id: "id3" },
]