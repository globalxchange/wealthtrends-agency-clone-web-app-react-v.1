import React from 'react'
import { Link } from 'react-router-dom'

export default function RStepOne({ images, appInfo, details, setDetails, setCurrentStep }) {
    return (
        <div className="register-step-one">
            <img src={!appInfo?.app_icon ? images.agencyDark : appInfo?.app_icon} />
            <p>Enter Your First And Last Name</p>
            <div className="r-s-o-input-wrapper">
                <input value={details.firstName} onChange={e => setDetails({ ...details, firstName: e.target.value })} autoFocus={true} placeholder="First Name" />
                <input onKeyPress={e => !details.firstName || !details.lastName ||e.which !== 13 ? console.log() : setCurrentStep(1)} value={details.lastName} onChange={e => setDetails({ ...details, lastName: e.target.value })} placeholder="Last Name" />
            </div>
            <div className="r-s-o-button-wrapper">
                <button><Link to={`/login`}>Back To Login</Link></button>
                <button disabled={!details.firstName || !details.lastName} onClick={() => setCurrentStep(1)}>Next</button>
            </div>
        </div>
    )
}
