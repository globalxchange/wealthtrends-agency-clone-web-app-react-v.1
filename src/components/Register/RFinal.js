import React from 'react'
import { useHistory } from 'react-router-dom'
import {login} from '../../services/postAPIs';
import Constant from '../../json/constant';
export default function RFinal({ final, details, images, appInfo }) {
    const [loading, setLoading] = React.useState(false);
    const history = useHistory();
    const handleLogin =async () =>{
        setLoading(true)
        let res = await login(details.email, details.password);
        if(res.data.status){
          document.documentElement.setAttribute("data-theme", "light");
          localStorage.setItem("userEmail", details.email);
          localStorage.setItem("userEmailPermanent", details.email);
          localStorage.setItem("deviceKey", res.data.device_key);
          localStorage.setItem("accessToken", res.data.accessToken);
          localStorage.setItem("idToken", res.data.idToken);
          localStorage.setItem("refreshToken", res.data.refreshToken);
          localStorage.setItem(
            "appId",
            Constant.appId[localStorage.getItem("appCode")]
          );

          history.replace("/dashboard");

          setLoading(false);

        }else{

        }
    }
    return (
        <div className="register-final">
            <img src={!appInfo?.app_icon ? images.agencyDark : appInfo?.app_icon} />
            <p>{
                final.success ? "You Have Successfully Completed Your Registration" : "Something Went Wrong. Please Try Again"
            }
            </p>
            <div>
                <button onClick={()=>handleLogin()}>{loading?"Logging You In...":"Log To My Account"}</button>
                <button onClick={()=>history.push("/login")}>Close</button>
            </div>


        </div>
    )
}
