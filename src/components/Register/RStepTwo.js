import React from 'react'
import { createBlockCheckEmail } from '../../services/postAPIs';

export default function RStepTwo({ appInfo, images, setCustomEmail, listOfUsername, details, setDetails, setCurrentStep }) {
    const [loading, setLoading] = React.useState(false);
    const [error, setError] = React.useState(false);
    const smallCaseRegex = /^[a-z0-9_\-]+$/gm;

    const handleSubmit = async () => {
        setLoading(true);
        let res
        try {
            res = await createBlockCheckEmail({ email: details.email, name: `${details.firstName} ${details.lastName}` })
            console.log("create email", res);
            if (res.status === 201) {
                setCurrentStep(2);
                setDetails({ ...details, inboxId: res.data.id })
            }

        } catch (error) {
            setLoading(false);
            setError(true);

        }
    }
    return (
        <div className="register-step-two">
            <img src={!appInfo?.app_icon ? images.agencyDark : appInfo?.app_icon} />
            <p>Choose A BlockCheck ID</p>
            <div className={`rst-input-wrapper ${!details.username || !smallCaseRegex.test(details.username) || details.username < 3 || listOfUsername.includes(details.username) ? "" : "valid-detail"} `}>
                <input
                    onKeyPress={e => !details.username || details.username < 3 || listOfUsername.includes(details.username) || !details.email || e.which !== 13 ? console.log() : error ? setDetails({ ...details, email: '', username: '' }) : handleSubmit()}
                    value={details.username} onChange={e => { setDetails({ ...details, username: e.target.value, email: e.target.value + '@blockcheck.io' }); setError(false); }} autoFocus={true} placeholder="Ex.Shorupan" />
                <h6>@blockcheck.io</h6>
            </div>
            <div className="rst-button-wrapper">
                <button onClick={() => setCustomEmail(true)}>Use Custom Email</button>
                <button style={error ? { backgroundColor: 'red' } : {}} disabled={!details.username || details.username < 3 || listOfUsername.includes(details.username) || !details.email} onClick={() => error ? setDetails({ ...details, email: '', username: '' }) : handleSubmit()}>{loading ? "Creating...." : error ? "Change ID" : "Next"}</button>
            </div>

        </div>
    )
}
