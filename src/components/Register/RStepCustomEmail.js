import React from 'react'

export default function RStepCustomEmail({ appInfo, images, listOfUsername, listOfEmails, setCustomEmail, details, setDetails, setCurrentStep }) {

    const smallCaseRegex = /^[a-z0-9_\-]+$/gm
    const emailRegex = /([\w\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?/gm
    return (
        <div className="register-step-two">
            <img src={!appInfo?.app_icon ? images.agencyDark : appInfo?.app_icon} />
            <p>Create Username And Enter Your Email</p>
            <div className="custom-email-input-wrapper">
                <div className={!details.username || details.username.length < 3  || listOfUsername.includes(details.username) ? "" : "valid-detail"}><input onChange={(e) => setDetails({ ...details, username: e.target.value })} placeholder="Username" /></div>
                <div className={!emailRegex.test(details.email) || listOfEmails.includes(details.email) ? "" : "valid-detail"}><input onChange={(e) => setDetails({ ...details, email: e.target.value })} placeholder="Email" /></div>
            </div>
            <div className="rst-ce-button-wrapper">
                <button onClick={() => setCustomEmail(false)}>Back</button>
                <button
                    onClick={() => setCurrentStep(2)}
                    disabled={!details.username || details.username.length < 3 || listOfUsername.includes(details.username) || !details.email || emailRegex.test(details.email)}
                >Next</button>

            </div>

        </div>
    )
}
