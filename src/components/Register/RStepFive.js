import React from 'react'
import OtpInput from 'react-otp-input';
import { MailSlurp } from 'mailslurp-client'
import { requestForgotPassword, registerNewUser, resendCodeNewUser } from '../../services/postAPIs';
import { getContentOfEmail, getLatestMessages, getListOfEmail } from '../../services/getAPIs';

export default function RStepFive({ appInfo, images, setFinal, customEmail, details, setDetails }) {
    const mailslurp = new MailSlurp({ apiKey: '86cee2f39d56b3b5a6b2e4c827cc1382d1be6bad16a9d35cd0e659ef9272d02c' })
    const [resending, setResending] = React.useState(false);
    const [submitting, setSubmitting] = React.useState(false);
    const [wrongCode, setWrongCode] = React.useState(false);
    const [verifying, setVerifying] = React.useState(false)
    const handleChange = (code) => {
        setDetails({ ...details, otp: code })
    }
    const handleResend = async () => {
        setResending(true)
        setDetails({ ...details, otp: '' })
        if (!customEmail) {
            setVerifying(true)
        }
        let res = await resendCodeNewUser(details.email);
        if (!customEmail) {
            getTheCode();
        }
        setResending(false)
    }
    const handleSubmit = async () => {
        setSubmitting(true);
        let res = await registerNewUser({ email: details.email, code: details.otp });
        if (res.data.status) {
            setFinal({ status: true, success: true })

        } else {
            setDetails({ ...details, otp: '' })
            setWrongCode(true);

        }
        setSubmitting(false)
    }
    const getTheCode = async () => {
        setVerifying(true);
        let res
        try {
            res = await mailslurp.waitController.waitForLatestEmail(details.inboxId, 30000, true)
            console.log("verifyyyyyyyy", res)
            const pattern = 'Your confirmation code is ([0-9]{6})';
            try {
                const result = await mailslurp.emailController.getEmailContentMatch(
                    { pattern },
                    res.id
                );
                if(!result.matches.length){
                    setVerifying(false);
                    return
                }
                let code = result.matches[1]
                setVerifying(false);
                setDetails({ ...details, otp: code })
            } catch (e) {
                    setVerifying(false);
                    return
            }

        } catch (error) {
            setVerifying(false);
            return

        }



    }

    React.useEffect(() => {
        if (customEmail) {

        } else {
            getTheCode()

        }

    }, [])
    return (
        <div className="register-step-two">
            <img src={!appInfo?.app_icon ? images.agencyDark : appInfo?.app_icon} />
            <p>{customEmail ? wrongCode ? "The Verification Code You Entered Is Incorrect" : "Enter The Code That Was Sent To *Email*" : "Verifying BlockCheck ID"}</p>
            <div className="rsf-otp-wrapper">
                <OtpInput
                    shouldAutoFocus={true}
                    isDisabled={!customEmail}
                    value={details.otp}
                    onChange={handleChange}
                    numInputs={6}
                    isInputNum={true}
                    placeholder="123456"
                />


            </div>
            <div className="rs-three-button-wrapper">
                <button onClick={() => verifying ? console.log() : handleResend()}>{customEmail ? resending ? "Resending..." : "Resend Code" : verifying ? "Verifying..." : "Retry Verification"}</button>
                <button onClick={() => handleSubmit()} disabled={details.otp.length !== 6}>{submitting ? "Submitting..." : "Submit"}</button>

            </div>
        </div>
    )
}
