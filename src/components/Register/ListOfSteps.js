import React from 'react'
import nextId from 'react-id-generator'
import NextSVG from '../common-components/assetsSection/NextSVG';

const ListOfSteps = React.memo(({ customEmail, currentStep }) => {

    const stepList = [
        { keyId: nextId(), text: 'Step 1: Identify Yourself ', step: 0 },
        { keyId: nextId(), text: customEmail ? 'Step 2: Email And Username' : 'Step 2: Create BlockCheck ID', step: 1 },
        { keyId: nextId(), text: 'Step 3: Create Password', step: 2 },
        { keyId: nextId(), text: 'Step 4: Confirm Password', step: 3 },
        { keyId: nextId(), text: customEmail ? 'Step 5: Verify Email' : 'Step 5: Verify BlockCheck ID', step: 4 },
    ]
    return (
        <div key={nextId()} className="register-list-of-steps">
            <h2>Registration</h2>
            {
                stepList.map(obj => <h6
                    key={obj.keyId}
                    className={obj.step <= currentStep ? obj.step === currentStep ? "current-step" : "" : "hide-the-step"}
                >
                    {obj.text}
                    <div key={nextId()} className={obj.step < currentStep?`div${obj.step}`:"d-none"}><NextSVG type="ticked" /></div>
                    {/* {obj.step < currentStep ? <NextSVG type="ticked" /> : ""} */}

                </h6>
                )
            }

        </div>
    )
})
export default ListOfSteps;