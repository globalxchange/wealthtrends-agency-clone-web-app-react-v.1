import React from 'react'
import ListOfSteps from './ListOfSteps'
import './register.style.scss'
import CryptoJS from 'crypto-js'
import RStepOne from './RStepOne';
import Images from '../../assets/a-exporter';
import RStepTwo from './RStepTwo';
import RStepCustomEmail from './RStepCustomEmail';
import RStepThree from './RStepThree';
import RStepFour from './RStepFour';
import RStepFive from './RStepFive';
import { getListOfUsernames } from '../../services/getAPIs';
import LoadingAnimation from '../../lotties/LoadingAnimation';
import RFinal from './RFinal';
const key = "gmBuuQ6Er8XqYBd"
export default function Register({ appInfo, brokerInfo }) {
    const [listOfUsername, setListOfUsername] = React.useState([]);
    const [listOfEmails, setListOfEmails] = React.useState([])
    const [currentStep, setCurrentStep] = React.useState(0);
    const [final, setFinal] = React.useState({
        status: false,
        success: false
    })
    const [mainLoading, setMainLoading] = React.useState(true);
    const [customEmail, setCustomEmail] = React.useState(false);
    const [details, setDetails] = React.useState({
        firstName: '',
        lastName: '',
        username: '',
        email: '',
        password: '',
        otp: '',
        inboxId: ''
    });
    const selectPage = () => {
        switch (currentStep) {
            case 0: return <RStepOne setCurrentStep={setCurrentStep} details={details} setDetails={setDetails} images={Images} appInfo={appInfo} />;
            case 1: return customEmail
                ?
                <RStepCustomEmail listOfEmails={listOfEmails} listOfUsername={listOfUsername} setCustomEmail={setCustomEmail} setCurrentStep={setCurrentStep} details={details} setDetails={setDetails} images={Images} appInfo={appInfo} />
                :
                <RStepTwo listOfUsername={listOfUsername} setCustomEmail={setCustomEmail} setCurrentStep={setCurrentStep} details={details} setDetails={setDetails} images={Images} appInfo={appInfo} />;
            case 2: return <RStepThree setCurrentStep={setCurrentStep} details={details} setDetails={setDetails} images={Images} appInfo={appInfo} />;
            case 3: return <RStepFour brokerInfo={brokerInfo} setCurrentStep={setCurrentStep} details={details} setDetails={setDetails} images={Images} appInfo={appInfo} />;
            case 4: return <RStepFive setFinal={setFinal} setCurrentStep={setCurrentStep} details={details} setDetails={setDetails} images={Images} appInfo={appInfo} customEmail={customEmail} />

            default: break;
        }
    }
    const setUpUsernameList = async () => {
        let res = await getListOfUsernames();
        let data = res.data.payload;

        let bytes = CryptoJS.Rabbit.decrypt(data, key);
        let jsonString = bytes.toString(CryptoJS.enc.Utf8);
        let result = JSON.parse(jsonString);
        console.log("usernames", result);
        setListOfUsername([...result.usernames]);
        setListOfEmails([...result.emails])
        setMainLoading(false)

    }
    React.useEffect(() => {
        setUpUsernameList()
    }, [])
    return (
        final.status ?
            <RFinal images={Images} final={final} details={details} appInfo={appInfo} />
            :
            mainLoading ?
                <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                    <LoadingAnimation />
                </div>
                :
                <div className="register-main">
                    <div className="col-md-4 register-main-left">
                        <ListOfSteps customEmail={customEmail} currentStep={currentStep} setCurrentStep={setCurrentStep} />

                    </div>
                    <div className="col-md-8 register-main-right">
                        {
                            selectPage()
                        }

                    </div>

                </div>
    )
}
