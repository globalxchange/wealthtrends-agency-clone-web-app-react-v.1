import React from 'react'
import Constant from '../../json/constant';

export default function RStepThree({ appInfo, images, setDetails, details, setCurrentStep }) {
    const [drawer, setDrawer] = React.useState(false);
    const [state, setState] = React.useState({ number: false, cap: false, special: false, length: false, all: false })
    const oneNumberRegex = /.*[0-9].*/gm
    const oneCapRegex = /.*[A-Z].*/gm
    const oneSpecialRegex = /.*[@#$%^&+=!].*/gm

    const handleChange = (e) => {
        setDetails({ ...details, password: e.target.value });
        checkPassword(e.target.value)
    }
    const checkPassword = (newPassword) => {
        let a = oneNumberRegex.test(newPassword);
        let b = oneCapRegex.test(newPassword);
        let c = oneSpecialRegex.test(newPassword);
        let d = newPassword.length >= 8;

        let e = a && b && c && d
        setState({ number: a, cap: b, special: c, length: d, all: e })
    }
    return (
        <div className="register-step-two">
            <img src={!appInfo?.app_icon ? images.agencyDark : appInfo?.app_icon} />
            <p>Create Your Password</p>
            <div className="custom-email-input-wrapper">
                <div className={state.all ? "valid-detail" : ""}>
                    <input
                        onKeyPress={e => !state.all || e.which !== 13 ? console.log() : setCurrentStep(3)}
                        value={details.password} type="password" onChange={handleChange} placeholder="Password" />
                </div>
            </div>
            <div className="rs-three-button-wrapper">
                <button onClick={() => setDrawer(true)}>Password Requirements</button>
                <button onClick={() => setCurrentStep(3)} disabled={!state.all}>Next</button>

            </div>
            <div className={`pr-step-two-check-list ${drawer ? "drawer-up" : ""}`}>
                <h4>Password Requirements</h4>
                <button onClick={() => setDrawer(false)}>
                    <img src={images.addDark} />
                </button>
                {
                    Constant.passwordChecklist.list.map(obj => <h6>
                        {obj.text} <span style={state[obj.type] ? { backgroundColor: "green" } : { backgroundColor: "red" }} />
                    </h6>
                    )
                }

            </div>


        </div>
    )
}
