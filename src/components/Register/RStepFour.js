import React from 'react'
import { sendEmailToNewUser } from '../../services/postAPIs';

export default function RStepFour({ appInfo, images, setDetails, brokerInfo, details, setCurrentStep }) {
    const [localPassword, setLocalPassword] = React.useState('');
    const [loading, setLoading] = React.useState(false);
    const [message, setMessage] = React.useState('');

    const handleSubmit = async () => {
        setLoading(true)
        let res = await sendEmailToNewUser({
            username: details.username,
            email: details.email,
            password: details.password,
            ref_affiliate: brokerInfo.affiliate_id,
            signedup_app: appInfo.app_code
        })
        if (res.data.status) {
            setCurrentStep(4);
        } else {
            setMessage(res.data.message)

        }
        setLoading(false)

    }
    return (
        <div className="register-step-two">
            <img src={!appInfo?.app_icon ? images.agencyDark : appInfo?.app_icon} />
            <p>Confirm Your Password</p>
            <div className="custom-email-input-wrapper">
                <div className={localPassword === details.password ? "valid-detail" : ""}>
                    <input
                        onKeyPress={e => localPassword !== details.password || e.which === 13 ? console.log() : handleSubmit()}
                        type="password" onChange={e => { setLocalPassword(e.target.value); setMessage('') }} placeholder="Password" />
                </div>
                {!message ? '' : <span>{message}</span>}
            </div>
            <div className="rs-three-button-wrapper">
                <button onClick={() => { setDetails({ ...details, password: '' }); setCurrentStep(2) }}>Change Password</button>
                <button onClick={() => handleSubmit()} disabled={localPassword !== details.password}>{loading ? "Loading....." : "Next"}</button>

            </div>
        </div>
    )
}
