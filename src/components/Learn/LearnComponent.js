import React from "react";
import { useParams } from "react-router-dom";
import ArticleComponent from "../Dashboard/Content/ArticleComponent/ArticleComponent";
import LearnVideoPlayer from "../Dashboard/Content/LearnVideoPlayer/LearnVideoPlayer";
import "./learn.style.scss";
export default function LearnComponent() {
  const { appCode, type, id } = useParams();
  React.useEffect(() => {
    if (type === "article") {
    } else {
    }
  }, [type]);
  return (
    <div className="learn-c-main">
      {type === "video" ? (
        <LearnVideoPlayer video_id={id} />
      ) : (
        <ArticleComponent article_id={id} />
      )}
    </div>
  );
}
