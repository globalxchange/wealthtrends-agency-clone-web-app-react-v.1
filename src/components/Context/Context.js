import React, { Component, createContext } from "react";
import {
  fetchAllCoins,
  getAllAppPresent,
  getEarningsDetails,
  getColorCodeByApp,
  moreAboutApp,
  fetchInvestType,
  paymentPathForInvestmentType,
  getListForMoney,
  getAppOverview,
  getBondsTransactionList,
  getIcedContracts,
  getIcedEarningBalance,
  getMobileAppLink,
  getTotalForexValue,
  convertToUSD,
  getAddedFriends,
  getFriendsDetail,
  getUserDetails,
  getIcedData,
  getMarketData,
  getAllCoinsAvailable,
  exchangeData,
  getGXAssetCoins,
  getMarketValue,
  getAvailablePairs,
  cryptoUSD,
  getMarketCoins,
  getContracts,
  getRegisteredApp,
  getMainInterest,
  getEarningLogs,
  getConversionRate,
  getEarningBalance,
  getUserAccountDetails,
  getBondBalance,
} from "../../services/getAPIs";
import {
  getVSAId,
  getUserForChat,
  findAppId,
  getUserBalances,
  moneyWithdrawStepOne,
  placeTradeAPI,
  decryptPlaceTrade,
  getForexTransaction,
  placeTrade,
  getCryptoTransaction,
  getTotalCryptoValue,
  addFriend,
  getBaseAssets,
  coinsFromVSA,
  getMainBalance,
  getForexCryptoTransaction,
  registerToApps,
  subVaultTransfer,
  addNewUser,
  icedWithdrawStepOne,
} from "../../services/postAPIs";
import nextId from "react-id-generator";
import Images from "../../assets/a-exporter";
import jwt from "jsonwebtoken";
import Axios from "axios";
export const Agency = createContext();

const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; //secret not to be disclosed anywhere.
const devEmail = "rahulrajsb@outlook.com"; //email of the developer.
const path_inside_brain = "root/bets/profileImage/";

export class AgencyProvider extends Component {
  volume = "24Hr_Volume";
  state = {
    enableuser: false,
    assetSelected: null,
    assetClassSelected: null,
    searchBox: false,
    tableTabs: [],
    mainTabs: "accounts",
    allCoins: {
      fiat: [],
      crypto: [],
    },
    icedCoins: null,
    marketCoins: [],
    currentUserDetails: null,
    ledgerOrAnalytics: false,
    forexNetWorth: null,
    cryptoNetWorth: null,
    icedNetWorth: 0,
    marketNetWorth: null,
    coinLoading: true,
    verificationLoading: true,
    collapseSidebar: false,
    analyticsType: "",
    netWorthController: null,
    forexTransactionList: [],
    cryptoTransactionList: [],
    icedTransactionList: [],
    marketTransactionList: [],
    currencyImageList: null,
    currentListLength: 0,
    ledgerActivePage: 1,
    ledgerDisplayList: [],
    sidebarAction: { count: 0, type: "" },
    theme: "light",
    VSAProfileId: "",
    ledgerLoading: true,
    croppingImage: "",
    croppedImage: {
      done: false,
      url: "",
    },
    imageUploading: false,
    friendList: null,
    addFriendDetails: {
      friendEmail: null,
      nickName: null,
      profilePic: null,
      appcode: null,
    },
    isGxUser: false,
    mainModal: false,
    selectedFriendDetails: null,
    selectedFriend: {
      email: "",
      nickName: "",
      profilePic: "",
    },
    coinAddresses: null,
    baseAsset: null,
    tradeStep: 0,
    allAvailableCoins: null,
    numbers: { Bitfinex: null, Binance: null, Kraken: null, Okex: null },
    tradePair: [
      { coin: null, image: null, exchange: null },
      { coin: null, image: null, exchange: null },
    ],
    buySell: null,
    exchangeList: null,
    update3: 0,
    exchangeAverageValue: ["-", "-", "-"],
    cryptoUSD: null,
    selectAsset: {
      exchangeId: null,
      [this.volume]: 0,
      fullImage: "",
      fullImageBW: "",
      imageCurrency: "",
      percentChange: 0,
      price: 0,
      image: "",
      buyPrice: 0,
      sellPrice: 0,
    },
    resetBuySellValue: false,
    executionMarketLoading: false,
    notification: { status: "", message: "" },
    allMarketCoins: null,
    contractCoins: [],
    fiatCoins: [],
    cryptoCoins: [],
    scrollValue: 0,
    usdConversion: null,
    availablePairs: null,
    registeredAppList: null,
    currentApp: null,
    mainBalance: null,
    mainInterest: null,
    overlay: false,

    allApps: [],
    transferState: {
      deposit: true,
      from: { app_code: "", coin: "", profile_id: "" },
      to: { app_code: "", coin: "", profile_id: "" },
      to_amount: "",
      transfer_for: "",
    },
    conversionRates: {},
    instaCryptoSupportedAssets: [],
    transferLoading: false,
    transferValue: 0,
    transfer: {},
    step: 0,
    selectedTransfer: {},
    externalWithdrawStep: 0,
    currentFriendList: [],
    externalWithdrawFriend: {},
    contactStep: 0,
    newPassword: "",
    secondGate: false,
    tradeState: false,
    tradeTabStep: 0,
    tradeDashboardTab: 0,
    tradePairObj: {
      base: null,
      quote: null,
      list: ["", ""],
      exchangeBank: null,
    },
    tradeFinalObj: null,
    tradeStats: null,
    tradeBuySell: false,
    buyValue: "",
    sellValue: "",
    readyForTrade: false,
    finalTradeResponse: null,
    checkedId: "id2",
    userAccountDetails: {},
    editUserAccount: false,
    currentUserDetailsTemp: null,
    userAccountActive: false,
    otherAccountDetails: null,

    //admin
    adminControllers: {
      first: "",
      second: "",
    },
    fundsConfig: {
      add: null,
      crypto: null,
      coin: null,
      num: null,
      action: false,
    },
    fundsFrom: null,
    fundsTo: null,
    fundsStepOneConfig: {
      type: null,
      internal: null,
      both: null,
      step: 0,
    },
    fundsSelectedApp: null,
    fundsLoading: null,
    threeSteps: 0,
    fundsResult: {
      status: null,
      message: null,
    },
    refreshSession: false,
    refreshApp: false,
    adminPassword: false,
    grantAdminAccess: null,
    overviewApp: {},
    appDownloadConfig: null,
    miConfig: {
      mi: null,
      fromApp: null,
      toApp: null,
    },
    miWhichOne: true,
    selectedCoinAndApp: {
      coin: null,
      app: null,
    },
    mainSteps: 3,
    miLoadingStep: 0,
    miAmount: "",
    moneyCoins: {
      from: null,
      to: null,
    },

    bondSum: 0,
    earningSum: 0,
    userChatEnable: false,
    currentUserObj: { username: "", timestamp: "", email: "" },
    preImageUpload: {
      status: false,
      url: null,
      image: "",
    },
    localImage: "",
    imageUploading: false,
    imageUploadResult: "",
    imageResult: null,
    previousMainTab: "",

    ledgerId: "",
    learnVideo: "",
    learnArticle: "",
    devPassword: {
      status: false,
      which: null,
    },
    devAccess: null,

    cStep: 0,
    playVideo: false,
    readArticle: false,
    showApps: false,
    transactionDeposit: false,

    //from handle funds
    type: { keyId: "" },
    //from handle funds
    moreOnApp: null,

    externalAction: "",
    globalImage: "",
    investmentPathOffering: [],
    selectedInvestmentType: null,
    checkingLoggedIn: false,
    appLevelRefresh: false,
    analyticsSection: false,
    allAppsAvailable: [],
    portfolioStep: 99,
    conversionConfig: {
      rate: 1,
      coin: "USD",
      enable: false,
      text: "",
    },
    updatePortfolioRate: false,
    goToSettings: false,
    explainNavbarTerms: false,
    marketSum: "",
    manualUpload: false,
    aspectRatio: {
      aspect: 1 / 1,
    },
    displayBondDate: "",
    differentiator: "bot",
    supportMessage: null,

    tradeNavbarType: {
      keyId: nextId(),
      text: "Forex Vault Balances",
      icon: Images.forexLogo,
      id: "fiat",
    },
    directCoinOverview: {
      status: false,
      coin: null,
      type: "",
      len: 0,
      i: "",
    },
    skipTradeStep: null,
    portfolioActive: false,
    mainAppId: "aadbeb02-5457-4470-9004-1f8ed90f1708",
    earningDetails: null,
    fullScreenChat: false,
    copiedOverlay: {
      status: false,
      title: "",
      data: "",
    },

    //EW
  };
  enableuserfunctiom = () => {
    this.setState({
      enableuser: true,
    });
  };
  enableuserfunctiomclose = () => {
    this.setState({
      enableuser: false,
    });
  };
  componentDidMount() {
    let first = localStorage.getItem("userEmail").toString();
    let second = localStorage.getItem("userEmailPermanent").toString();

    if (first === second) {
      this.setState({ userAccountActive: false });
    } else {
      let obj = localStorage.getItem("otherAccount");
      this.setState({
        userAccountActive: true,
        otherAccountDetails: JSON.parse(obj),
      });
    }
  }
  setCopiedOverlay = (obj) => {
    this.setState({ copiedOverlay: obj });
  };
  setFullScreenChat = (val) => {
    this.setState({ fullScreenChat: val });
  };
  setPortfolioActive = (val) => {
    this.setState({ portfolioActive: val });
  };
  setSkipTradeStep = (val) => {
    this.setState({ skipTradeStep: val });
  };
  setDirectCoinOverview = (val) => {
    this.setState({ directCoinOverview: val });
  };
  setSupportMessage = (val) => {
    this.setState({ supportMessage: val });
  };
  setDifferentiator = (val) => {
    this.setState({ differentiator: val });
  };
  setTradeNavbarType = (obj) => {
    this.setState({ tradeNavbarType: obj });
  };
  setDisplayBondDate = (val) => {
    this.setState({ displayBondDate: val });
  };
  setAspectRatio = (val) => {
    this.setState({
      aspectRatio: val,
    });
  };
  setManualUpload = (val) => {
    this.setState({ manualUpload: val });
  };
  setExplainNavbarTerm = (val) => {
    this.setState({ explainNavbarTerms: val });
  };
  setInvestmentPath = (obj) => {
    this.setState({ investmentPath: obj });
  };
  setGoToSettings = (val) => {
    this.setState({ goToSettings: val });
  };
  setUpdatePortfolio = (val) => {
    this.setState({ updatePortfolioRate: val });
  };
  setPortfolioStep = (step) => {
    this.setState({ portfolioStep: step });
  };
  setPortfolioFinalValue = (val) => {
    this.setState({ portfolioFinalValue: val });
  };
  setConversionConfig = (obj) => {
    this.setState({ conversionConfig: { ...obj } });
  };
  setAnalyticsSection = (val) => {
    this.setState({ analyticsSection: val });
  };
  setCheckingLoggedIn = (val) => {
    this.setState({ checkingLoggedIn: val });
  };
  setSelectedInvestmentType = (obj) => {
    this.setState({ selectedInvestmentType: obj });
  };
  globalHandleFinalImage = (e, aspect = { aspect: 1 / 1 }) => {
    if (!e.target.files[0]) {
      return;
    }
    e.preventDefault();
    const fileReader = new FileReader();
    this.setAspectRatio(aspect);
    fileReader.onloadend = () => {
      // setUploadIcon(fileReader.result);
      // this.setState({ globalImage: fileReader.result }, () => this.globalDataURLToFile(this.state.globalImage, "newImage.jpg"))
      this.setCroppingImage(fileReader.result);
    };
    fileReader.readAsDataURL(e.target.files[0]);
  };
  globalDataURLToFile = (dataurl, filename) => {
    let arr = dataurl.split(","),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    let croppedImg = new File([u8arr], filename, { type: mime });
    let sendImage = croppedImg;
    this.imageUploader(sendImage);
  };
  setExternalAction = (val) => {
    this.setState({ externalAction: val });
  };
  //from handle funds
  setType = (obj) => {
    this.setState({ type: obj });
  };
  //from handle funds
  setTransactionDeposit = (val) => {
    this.setState({ transactionDeposit: val });
  };
  setShowApps = (val) => {
    this.setState({ showApps: val });
  };
  setReadArticle = (val) => {
    this.setState({ readArticle: val });
  };
  setPlayVideo = (val) => {
    this.setState({ playVideo: val });
  };
  setDevPassword = (obj) => {
    this.setState({ devPassword: obj });
  };
  setDevAccess = (val) => {
    this.setState({ devAccess: val });
  };
  setLearnArticle = (val) => {
    this.setState({ learnArticle: val });
  };
  setLearnVideo = (val) => {
    this.setState({ learnVideo: val });
  };
  setLedgerId = (val) => {
    this.setState({ ledgerId: val });
  };

  setPreviousMainTab = (val) => {
    this.setState({ previousMainTab: val });
  };
  setPreImageUpload = (val) => {
    this.setState({ preImageUpload: val });
  };

  imageUploader = async (url) => {
    this.setState({ imageUploading: true });
    let fileName = this.getUnique();
    let file = url;
    const token = jwt.sign({ name: fileName, email: devEmail }, secret, {
      algorithm: "HS512",
      expiresIn: 240,
      issuer: "gxjwtenchs512",
    });
    const formData = new FormData();
    console.log("formdata 1", file);
    formData.append("files", new File([file], fileName, { type: file.type }));
    console.log("formdata", formData);
    try {
      const res = await Axios.post(
        `https://drivetest.globalxchange.io/file/dev-upload-file?email=${devEmail}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
        formData,
        {
          headers: {
            "Access-Control-Allow-Origin": "*",
          },
        }
      );
      const { data } = res;
      if (data.payload.upload_success) {
        this.setCroppedImage({ done: false, url: "" });
        this.setState({
          imageUploading: false,
          localImage: data.payload.url && data.payload.url,
          imageResult: true,
        });
        // this.setState({ preImageUpload: { status: false, url: null, image: '' } })
      } else {
        this.setCroppedImage({ done: false, url: "" });
        this.setState({
          imageUploading: false,
          localImage: "",
          imageResult: false,
        });
        this.setState({
          preImageUpload: { status: false, url: null, image: "" },
        });
        this.changeNotification({
          status: false,
          Messages: "Failed to Send Image",
        });
      }
    } catch (err) {
      alert("Failed");
      console.log("err.message", err.message);
    }
    return 0;
  };

  getUnique = () => {
    let str = `${this.state.currentUserObj.username}${Date.now()}`;
    return str;
  };
  resetImage = () => {
    this.setState({ imageUploading: false, localImage: "", imageResult: null });
  };
  setMoneyCoins = (val) => {
    this.setState({ moneyCoins: val });
  };
  setUserChatEnable = (val) => {
    this.setState({ userChatEnable: val });
  };
  setCurrentUserObj = () => {
    getUserForChat().then((res) => {
      console.log("res.data", res.data);
      if (!res.data.status) return;
      this.setState({
        currentUserObj: {
          username: res.data.payload.username,
          timestamp: res.data.payload.timestamp,
          email: res.data.payload.email,
        },
      });
    });
  };

  resetMI = () => {
    this.setState({
      miConfig: {
        mi: null,
        fromApp: null,
        toApp: null,
      },
      miWhichOne: true,
      selectedCoinAndApp: {
        coin: null,
        app: null,
      },
      mainSteps: 0,
      miLoadingStep: 0,
      miAmount: "",
    });
  };
  setMiAmount = (val) => {
    this.setState({ miAmount: val });
  };
  setMainSteps = (val) => {
    this.setState({ mainSteps: val });
  };

  withdrawFromBondsEarning = async (val) => {
    this.setMainSteps(1);
    this.setState({ miLoadingStep: 1 });
    let iced = this.state.allApps.find((x) => {
      return x.app_code === "ice";
    });

    let obj = {
      email: localStorage.getItem("userEmail"),
      token: localStorage.getItem("idToken"),
      app_code: "ice",
      profile_id: iced?.profile_id,
      coin: this.state.selectedCoinAndApp.coin?.coinSymbol,
      amount: val,
      origin_app_code: this.state.currentApp.app_code,
      toAppCode: this.state.miConfig.toApp.app_code,
    };
    let resCon;
    let rate = 1;
    if (this.state.miConfig.mi) {
      if (
        this.state.cryptoCoins.includes(this.state.moneyCoins.to.coinSymbol) &&
        this.state.fiatCoins.includes(this.state.moneyCoins.from.coinSymbol)
      ) {
        resCon = await getConversionRate(this.state.moneyCoins.from.coinSymbol);
        resCon = resCon.data;
        rate = parseFloat(1 / resCon[this.state.moneyCoins.to.coinSymbol]);
      } else {
        resCon = await getConversionRate(this.state.moneyCoins.to.coinSymbol);
        resCon = resCon.data;
        rate = parseFloat(resCon[this.state.moneyCoins.from.coinSymbol]);
      }
    }

    let objMoney = {
      email: localStorage.getItem("userEmail"),
      token: localStorage.getItem("idToken"),
      app_code: this.state.selectedCoinAndApp.app?.app_code,
      profile_id: this.state.selectedCoinAndApp.app?.profile_id,
      coin: this.state.selectedCoinAndApp.coin?.coinSymbol,
      amount: val,
    };
    let res;
    if (this.state.miConfig.mi) {
      if (
        this.state.miConfig.fromApp.app_code ===
        this.state.miConfig.toApp.app_code
      ) {
        this.setState({ miLoadingStep: 4 });
        res = await moneyWithdrawStepOne(objMoney);
        if (res.data.status) {
          this.getAllMarketCoins();
          this.setMainBalance();
          this.setMainSteps(2);
          this.changeNotification({
            status: true,
            message: "Withdrawn Successfully",
          });
          return;
        } else {
          this.setMainSteps(2);
          this.changeNotification({
            status: false,
            message: "Withdrawn Failed",
          });
          return;
        }
      } else {
        res = await moneyWithdrawStepOne(objMoney);
        if (res.data.status) {
          this.setMainSteps(2);
          this.getAllMarketCoins();
          this.setMainBalance();
          this.changeNotification({
            status: true,
            message: "Withdrawn Successfully",
          });
          return;
        } else {
          this.setMainSteps(2);
          this.changeNotification({
            status: false,
            message: "Withdrawn Failed",
          });
          return;
        }
      }
    } else {
      res = await icedWithdrawStepOne(obj);
    }

    let objTwo = {
      from: {
        app_code: "ice",
        coin: this.state.selectedCoinAndApp.coin?.coinSymbol,
        profile_id: iced.profile_id,
      },
      to: {
        app_code: this.state.selectedCoinAndApp.app?.app_code,
        coin: this.state.selectedCoinAndApp.coin?.coinSymbol,
        profile_id: this.state.selectedCoinAndApp.app?.profile_id,
      },
      to_amount: val,
      transfer_for: "Iced Withdraw",
    };
    let objTwoMoney = {
      from: {
        app_code: this.state.miConfig.fromApp.app_code,
        coin: this.state.moneyCoins.from?.coinSymbol,
        profile_id: this.state.miConfig.fromApp.profile_id,
      },
      to: {
        app_code: this.state.miConfig.toApp.app_code,
        coin: this.state.moneyCoins.to?.coinSymbol,
        profile_id: this.state.miConfig.toApp.profile_id,
      },
      to_amount: val * rate,
      transfer_for: "Iced Withdraw",
    };
    let resTwo;
    if (res.data.status) {
      this.setState({ miLoadingStep: 2 });
      resTwo = await subVaultTransfer(
        this.state.miConfig.mi ? objTwoMoney : objTwo
      );
      this.setState({ miLoadingStep: 3 });
      this.changeNotification({
        status: resTwo.data.status ? true : false,
        message: resTwo.data.status
          ? "Transfer Done Successfully"
          : "Transfer Failed",
      });
      if (resTwo.data.status) {
        // this.resetMI();
        this.setMainSteps(2);
      } else {
        // this.resetMI();

        this.setMainSteps(0);
      }
    } else {
      // this.resetMI();
      this.setState({ miLoadingStep: 4 });
      this.changeNotification({
        status: false,
        message: "Withdrawing unsuccessful",
      });
      this.setMainSteps(0);
    }
  };

  setSelectedCoinAndApp = (val) => {
    this.setState({ selectedCoinAndApp: val });
  };
  setWhichOne = (val) => {
    this.setState({ miWhichOne: val });
  };
  setMiConfig = (val) => {
    this.setState({ miConfig: val });
  };

  setAppDownloadConfig = (val) => {
    this.setState({ appDownloadConfig: val });
  };
  setGrantAdminAccess = (val) => {
    this.setState({ grantAdminAccess: val }, () => {
      val !== null ? this.setAdminPassword(false) : console.log();
    });
  };
  setAdminPassword = (val) => {
    this.setState({ adminPassword: val });
  };
  setRefreshApp = (val) => {
    this.setState({ refreshApp: val });
  };
  setRefreshSession = (val) => {
    this.setState({
      refreshSession: val,
    });
  };
  setFundsResult = (val) => {
    this.setState({ fundsResult: val });
  };
  setThreeSteps = (val) => {
    this.setState({ threeSteps: val });
  };
  setFundsLoading = (val) => {
    this.setState({ fundsLoading: val });
  };
  setFundsSelectedApp = (val) => {
    this.setState({ fundsSelectedApp: val });
  };
  setFundsStepOneConfig = (obj) => {
    this.setState({ fundsStepOneConfig: obj });
  };
  setFundsFrom = (obj) => {
    this.setState({ fundsFrom: obj });
  };
  setFundsTo = (obj) => {
    this.setState({ fundsTo: obj });
  };
  setFundsConfig = (obj) => {
    console.log("funds c", obj);
    this.setState({ fundsConfig: obj });
  };
  setAdminController = (obj) => {
    this.setState({ adminControllers: obj });
  };
  changeLogin = (which, obj) => {
    if (which) {
      this.setState({
        mainBalance: null,
        mainInterest: null,
        allMarketCoins: null,
      });
      this.setRegisteredAppsByUser();
      this.setState({ otherAccountDetails: obj, userAccountActive: true }, () =>
        console.log("other user", this.state.userAccountDetails)
      );
    } else {
      this.setState({
        mainBalance: null,
        mainInterest: null,
        allMarketCoins: null,
      });
      let tempEmail = localStorage.getItem("userEmailPermanent");
      localStorage.setItem("userEmail", tempEmail);
      this.setState({ otherAccountDetails: null, userAccountActive: false });
      this.setRegisteredAppsByUser();
    }
  };

  setCurrentUserDetailsTemp = (val) => {
    this.setState(
      { currentUserDetailsTemp: val },
      this.setState({ selectedFriendDetails: null })
    );
  };
  setEditUserAccount = (val) => {
    this.setState({ editUserAccount: val });
  };
  setCheckedId = (val) => {
    this.setState({
      checkedId: val,
    });
  };
  resetTradeDashboard = () => {
    this.setState({
      tradeDashboardTab: 0,
      tradeFinalObj: null,
      tradeStats: null,
      tradeBuySell: false,
      buyValue: "",
      sellValue: "",
      readyForTrade: false,
      finalTradeResponse: null,
      cStep: 0,
    });
  };
  resetFullTrade = () => {
    this.setState({
      tradeTabStep: 0,
      tradeDashboardTab: 0,
      tradePairObj: {
        base: null,
        quote: null,
        list: ["", ""],
        exchangeBank: null,
      },
      tradeFinalObj: null,
      tradeStats: null,
      tradeState: false,
      tradeBuySell: false,
      buyValue: "",
      sellValue: "",
      readyForTrade: false,
      finalTradeResponse: null,
      cStep: 0,
    });
  };

  resetFullTradeTerminal = () => {
    this.setState({
      tradeTabStep: 0,
      tradeDashboardTab: 0,
      tradePairObj: {
        base: null,
        quote: null,
        list: ["", ""],
        exchangeBank: null,
      },
      tradeFinalObj: null,
      tradeStats: null,
      // tradeState: false,
      tradeBuySell: false,
      buyValue: "",
      sellValue: "",
      readyForTrade: false,
      finalTradeResponse: null,
      cStep: 0,
    });
  };

  setBuyValue = (val) => {
    this.setState({
      buyValue: val,
    });
  };
  setReadyForTrade = (val) => {
    this.setState({
      readyForTrade: val,
    });
  };
  finalTradeExecute = async () => {
    this.setTradeDashboardStep(2);
    let temp = { ...this.state.tradeFinalObj, stats: false };
    let final = await placeTradeAPI(temp);
    let finalExecute = await decryptPlaceTrade(final.data.data);
    if (finalExecute.data.status) {
      this.setState({ finalTradeResponse: finalExecute.data }, () =>
        this.setTradeDashboardStep(3)
      );
      this.setMainBalance();
      this.getAllMarketCoins();
    } else {
      this.setState({ finalTradeResponse: finalExecute.data });
      this.setTradeDashboardStep(3);
    }
  };

  setSellValue = (val) => {
    this.setState({
      sellValue: val,
    });
  };
  setTradeBuySell = (val) => {
    this.setState({ tradeBuySell: val });
  };
  setTradeStats = (val) => [
    this.setState({
      tradeStats: val,
    }),
  ];

  setTradeFinalObj = (val) => [
    this.setState({
      tradeFinalObj: val,
    }),
  ];
  setTradePairObj = (val) => {
    this.setState({
      tradePairObj: val,
    });
  };
  setTradeTabStep = (val) => {
    this.setState({
      tradeTabStep: val,
    });
  };
  setTradeDashboardStep = (val) => {
    this.setState({
      tradeDashboardTab: val,
    });
  };
  setCStep = (val) => {
    this.setState({
      cStep: val,
    });
  };
  setTradeState = (val) => {
    this.setState({
      tradeState: val,
    });
  };
  setSecondGate = (val) => {
    this.setState({ secondGate: val });
  };
  setNewPassword = (val) => {
    this.setState({ newPassword: val });
  };
  setContactStep = (val) => {
    this.setState({ contactStep: val });
  };
  setExternalWithdrawFriend = (obj) => {
    this.setState({
      externalWithdrawFriend: obj,
    });
  };
  setExternalWithdrawStep = (val) => {
    this.setState({ externalWithdrawStep: val });
  };
  setSelectedTransfer = (val) => {
    this.setState({ selectedTransfer: val });
  };
  setToTransfer = (val) => {
    this.setState({ transfer: val });
  };
  setStepMain = (val) => {
    this.setState({ step: val });
  };
  setTransferValue = (val) => {
    this.setState({ transferValue: val });
    // console.log('transfer value inside', val)
    // alert(val)
  };

  finalTransferSubVault = async () => {
    let temp = { ...this.state.transferState };
    console.log("transfer data", temp);
    // this.setState({ transferLoading: true })
    let data;

    try {
      data = await subVaultTransfer(temp);
      console.log("whyyyyyyyyyyyy", data.data);
      if (data.data.status) {
        this.setState(
          {
            fundsResult: {
              status: true,
              message: "Transferred Successfully..!!",
            },
          },
          () => {
            this.setThreeSteps(2);
          }
        );
      } else {
        this.setState(
          { fundsResult: { status: false, message: data.data.message } },
          () => {
            this.setThreeSteps(2);
          }
        );
      }

      this.setMainBalance();
      this.changeNotification({
        status: data.data.status ? true : false,
        message: data.data.status
          ? "Transfer Done Successfully"
          : "Transfer Failed",
      });
    } catch (e) {
      this.changeNotification({
        status: data.data.status ? true : false,
        message: data.data.status
          ? "Transfer Done Successfully"
          : "Transfer Failed",
      });

      this.setState(
        { fundsResult: { status: false, message: data.data.message } },
        () => {
          this.setThreeSteps(2);
        }
      );
    }
    console.log("transfer Data", data.data);

    // this.setState({ transferLoading: false })
    // console.log("transfer REsult", data.data)
    // alert(data.data.status ? "Succesful" : "Failed");

    // this.setState({
    // analyticsType: '',
    //   instaCryptoSupportedAssets: []
    // })
    // this.setStepMain(0)
    // this.setToTransfer({})
  };

  setConversionRates = async (coin) => {
    let data = await getConversionRate(coin);
    this.setState({ conversionRates: data.data });
  };
  setInstaCryptoSupportedAssets = async (app, id) => {
    let data = await getMainBalance(app, id);
    // console.log("insta crypto", data)
    this.setState({ instaCryptoSupportedAssets: data.data.coins_data });
  };
  setTransfer = (val) => {
    // this.setState({ transferState: val }, () => { this.setConversionRates(val.from.coin); }) //Old Transfer
    this.setState({ transferState: val }, () => {
      this.finalTransferSubVault();
    });
  };
  enableOverlay = (val) => {
    this.setState({ overlay: val });
  };
  setMainTabs = (val) => {
    this.setState({ mainTabs: val });
  };
  setRegisteredAppsByUser = () => {
    this.setTradeState(false);
    this.setMainTabs("accounts");
    this.setState({
      mainBalance: null,
      mainInterest: null,
      allMarketCoins: null,
    });
    registerToApps("WealthTrends").then(() => {
      getRegisteredApp().then((res) => {
        this.setState({ allApps: [...res.data.userApps] });
        let temp = res.data.userApps.find((obj) => {
          return obj.app_code === "WealthTrends";
        });
        temp = {
          ...temp,
          _id: temp?.profile_id,
          icon: Images.vaultIcon,
          invert: Images.vaultIconInvert,
          name: temp?.app_name + " Vault",
          type: "valut",
        };
        this.setState(
          {
            registeredAppList: [
              {
                _id: nextId(),
                icon: Images.agencyWhite,
                invert: Images.agencyDark,
                name: "Dashboard",
                type: "dashboard",
              },
              temp,
              {
                _id: nextId(),
                icon: Images.hfBlockcheckSmall,
                invert: Images.hfBlockcheckSmall,
                name: "Blockcheck",
                type: "blockcheck",
              },
            ],
          },
          () => {
            this.setCurrentApp(temp);
          }
        );
      });
    });
  };
  setMobileLink = () => {
    getMobileAppLink().then((res) => {
      this.setState({ mobileLinks: res.data.data });
    });
  };
  setMoreInfoOfApp = async () => {
    let res = await moreAboutApp();
    this.setState({ moreOnApp: res.data.apps[0] }, () => this.setUpAppId());
  };
  initialReset = () => {
    this.setState({
      mainBalance: null,
      mainInterest: null,
      allMarketCoins: null,
      addFriendDetails: {
        ...this.state.addFriendDetails,
        appcode: this.state.currentApp.app_code,
      },
    });
    getUserBalances(
      this.state.currentApp.profile_id,
      this.state.currentApp.app_code
    )
      .then((resTwo) => {
        this.setState({ coinAddresses: resTwo.data?.vault?.coinAddress });
      })
      .catch((error) => console.error("Failed to fetch all APPs"));
    this.setAssetSelected(null);
    this.getFriendsList();
    this.setCurrentUserObj();
    this.adjustColorScheme();
    this.setUpEarningDetails()
    this.setMoreInfoOfApp();
    this.setMainBalance();
    this.getAllMarketCoins();
    this.setMainInterest();
    this.setIcedCoins();
    this.setMobileLink();

    this.investmentPathOffering();
    this.setUserAccountDetails();
    this.setUserDetails();
    this.setMarketCoins();
    this.setAllAvailableApps();
  };
  setUpEarningDetails = async () => {
    let res = await getEarningsDetails();
    this.setState({earningDetails:res.data.users[0]});
  };
  setUpAppId = async () => {
    let res = await findAppId(this.state.currentApp.app_code);
    console.log("main app id", res.data);
    if (res.data.status) {
      this.setState({ mainAppId: res.data.payload.id });
    } else {
      // this.setupNewChatroom()
      this.setState({ mainAppId: "aadbeb02-5457-4470-9004-1f8ed90f1708" });
    }
  };
  adjustColorScheme = async () => {
    let res = await getColorCodeByApp(this.state.currentApp.app_code);
    console.log("colors API", res.data);
    if (!res.data.Apps[0].colors) {
    } else {
      let { colors } = res.data.Apps[0];
      document.documentElement.style.setProperty(
        "--main-color",
        colors.mainColor
      );
      localStorage.setItem("mainColor", colors.mainColor);
      document.documentElement.style.setProperty(
        "--text-color",
        colors.textColor
      );
      localStorage.setItem("textColor", colors.textColor);
      document.documentElement.style.setProperty(
        "--background-color",
        colors.backgroundColor
      );
      localStorage.setItem("bgColor", colors.backgroundColor);
      document.documentElement.style.setProperty(
        "--border-color",
        colors.borderColor
      );
      localStorage.setItem("borderColor", colors.borderColor);
    }
  };
  setAllAvailableApps = async () => {
    let res = await getAllAppPresent();
    this.setState({ allAppsAvailable: res.data.apps });
  };
  investmentPathOffering = async () => {
    let res = await fetchInvestType();

    let tempIT = res.data.investments;
    let tempArrT = [];
    for (let i = 0; i < tempIT.length; i++) {
      let resTwo = await paymentPathForInvestmentType(
        this.state.currentApp.app_code,
        tempIT[i].code
      );
      tempArrT = [...tempArrT, { ...tempIT[i], count: resTwo.data.count }];
      console.log("Offeringssssssss", tempArrT);
    }
    this.setState({
      investmentPathOffering: [...tempArrT],
    });
  };
  callUserDetail = () => {
    this.setUserAccountDetails();
  };
  setUserAccountDetails = async () => {
    try {
      let res = await getUserAccountDetails(this.state.currentApp.app_code);
      console.log("user status", res);
      if (res.status === 200) {
        let tempData = res.data.accounts;
        let tempFiat = [],
          tempCrypto = [];
        let fiatList = [],
          cryptoList = [];
        if (!res.data.accounts?.fiat) {
        } else {
          Object.keys(tempData.fiat).forEach((key) => {
            let i = 0;
            for (i = 0; i < tempData.fiat[key].length; i++) {
              let t = {
                coinSymbol: key,
                coinImage: this.state.currencyImageList[key],
              };
              tempFiat = [...tempFiat, { ...tempData.fiat[key][i], ...t }];
            }
          });
          fiatList = [...Object.keys(tempData.fiat)];
        }
        if (!tempData?.crypto) {
        } else {
          cryptoList = [...Object.keys(tempData.crypto)];
          Object.keys(tempData.crypto).forEach((key) => {
            let i = 0;
            for (i = 0; i < tempData.crypto[key].length; i++) {
              let t = {
                coinSymbol: key,
                coinImage: this.state.currencyImageList[key],
              };
              tempCrypto = [
                ...tempCrypto,
                { ...tempData.crypto[key][i], ...t },
              ];
            }
          });
          cryptoList = [...Object.keys(tempData.crypto)];
        }
        tempData = {
          fiat: [...tempFiat],
          crypto: [...tempCrypto],
        };
        let finalTemp = {
          ...res.data,
          accountList: { ...tempData },
          fiatList: fiatList,
          cryptoList: cryptoList,
        };
        console.log("user accountsyyyyyyyy", cryptoList, fiatList);
        this.setState({ userAccountDetails: { ...finalTemp } });
      } else {
        // alert()
      }
    } catch (error) {
      this.setState({
        userAccountDetails: {
          accountList: { fiat: [], crypto: [] },
          fiatList: [],
          cryptoList: [],
        },
      });
    }
  };
  setCurrentApp = (obj) => {
    this.setState({ currentApp: obj }, () => {
      this.initialReset();
      this.setOverviewApp();
    });
  };
  setOverviewApp = async () => {
    let res;
    try {
      res = await getAppOverview(this.state.currentApp.app_code);
      this.setState({ overviewApp: res.data.apps[0] });
    } catch (e) {
      console.error(e);
    }
  };

  setMainBalance = () => {
    const { app_code, profile_id } = this.state.currentApp;
    getMainBalance(app_code, profile_id)
      .then((res) => {
        if (res.data.status) {
          let temp1 = res.data?.coins_data?.filter((obj) => {
            return obj.type === "fiat";
          });
          let temp2 = res.data?.coins_data?.filter((obj) => {
            return obj.type === "crypto";
          });
          let fiatSum = 0;
          let cryptoSum = 0;
          temp1.forEach((obj) => {
            fiatSum = fiatSum + parseFloat(obj.coinValueUSD);
          });
          temp2.forEach((obj) => {
            cryptoSum = cryptoSum + parseFloat(obj.coinValueUSD);
          });
          this.setState({
            mainBalance: {
              fiat: [...temp1],
              crypto: [...temp2],
            },
            mainSum: {
              fiat: fiatSum,
              crypto: cryptoSum,
            },
          });
        } else {
          this.setState({
            mainBalance: {
              fiat: [],
              crypto: [],
            },
            mainSum: {
              fiat: 0,
              crypto: 0,
            },
          });
        }
      })
      .catch((err) => {
        console.error("Failed to fetch main balance");

        this.setState({
          mainBalance: {
            fiat: [],
            crypto: [],
          },
          mainSum: {
            fiat: 0,
            crypto: 0,
          },
        });
      });
  };

  setMainInterest = () => {
    getMainInterest(this.state.currentApp.app_code).then((res) => {
      if (res.data.result === undefined) {
        this.setState({ mainInterest: [], mainInterestSum: 0 });
        return;
      }
      let temp = res.data.result[0].balances;
      let newArray = [];
      let sumInterest = 0;
      let i = 0,
        len = temp.length;
      for (i = 0; i < len; i++) {
        sumInterest = sumInterest + temp[i].coinValueUSD;
      }
      if (isNaN(sumInterest)) sumInterest = 0;
      this.setState({ mainInterest: [...temp], mainInterestSum: sumInterest });
    });
  };

  getAllMarketCoins = async () => {
    let contracts = await (await getContracts()).data;
    if (contracts.status) {
      contracts = contracts.icedContracts;
    } else {
      contracts = [];
    }
    let contractCoins = contracts.map((obj) => {
      return obj._id;
    });
    let mainTemp = await (
      await getMainBalance(
        this.state.currentApp.app_code,
        this.state.currentApp.profile_id,
        false
      )
    ).data;
    let mainTempInvestment = await (
      await getMainBalance(
        this.state.currentApp.app_code,
        this.state.currentApp.profile_id,
        true
      )
    ).data;
    let bondTemp = await (await getBondBalance()).data;
    let bTemp;
    if (bondTemp.status) {
      bTemp = bondTemp.result[0]?.balances[0]?.iced_balances;
    } else {
      bTemp = [];
    }
    let resBond = await getIcedContracts(); //Second API for Bond
    if (resBond.data.status) {
      resBond = resBond.data.icedContracts;
    } else {
      resBond = [];
    }
    let resTwoBond = await getIcedEarningBalance();
    let matchObj;
    let emptyMatch = false;
    if (resTwoBond.data.status) {
      matchObj = resTwoBond.data.result?.[0].balances;
      emptyMatch = false;
    } else {
      matchObj = [];
      emptyMatch = true;
    }

    let bondSumTemp = 0;
    let earnedFromBonds = 0;
    let earnedFromMM = 0;
    let EFB = matchObj.map((obj) => {
      earnedFromBonds =
        earnedFromBonds + parseFloat(!obj.coinValueUSD ? 0 : obj.coinValueUSD);
    });
    let EFM = await getListForMoney(this.state.currentApp.app_code);

    earnedFromMM = EFM.data.totalBalanceUSD;
    if (EFM.data.status) {
      EFM = EFM.data.result[0].balances;
    } else {
      EFM = [];
    }
    let earning = parseFloat(earnedFromBonds + earnedFromMM);
    let tempBond = resBond.map((obj) => {
      bondSumTemp =
        bondSumTemp +
        parseFloat(obj.currentVoc * this.state.cryptoUSD[obj._id]);
      return {
        ...obj,
        keyId: nextId(),
        coinSymbol: obj._id,
        coinName: this.state.nameImageList?.[obj._id]?.coinName,
        balance: emptyMatch
          ? 0
          : matchObj?.find((objTwo) => {
              return objTwo.coinSymbol === obj._id;
            })?.coinValue,
        coinValue: emptyMatch
          ? 0
          : matchObj?.find((objTwo) => {
              return objTwo.coinSymbol === obj._id;
            })?.coinValue,
        coinValueUSD: emptyMatch
          ? 0
          : matchObj?.find((objTwo) => {
              return objTwo.coinSymbol === obj._id;
            })?.coinValueUSD,
        coinName: this.state.nameImageList?.[obj._id]?.coinName,
        coinImage: this.state.nameImageList?.[obj._id]?.coinImage,
        _24hrchange: this.state.nameImageList?.[obj._id]._24hrchange,
      };
    });

    if (mainTemp.status) mainTemp = mainTemp?.coins_data;
    else mainTemp = [];
    if (mainTempInvestment.status)
      mainTempInvestment = mainTempInvestment?.coins_data;
    else mainTempInvestment = [];

    let cryptoTemp = mainTemp.filter((obj) => {
      return obj.type === "crypto";
    });
    let fiatTemp = mainTemp.filter((obj) => {
      return obj.type === "fiat";
    });
    let marketTemp = mainTempInvestment;
    let marketSum;
    mainTempInvestment.map((obj) => {
      marketSum = marketSum + obj.coinValueUSD;
    });
    console.log("market sum", marketSum);

    let mainTempThree = mainTemp;
    let mainTempThreeInterest = await getGXAssetCoins(
      this.state.currentApp.app_code
    );
    // .interest_rates;
    if (mainTempThreeInterest.data.status) {
      mainTempThreeInterest = mainTempThreeInterest.data.interest_rates;
    } else {
      mainTempThreeInterest = [];
    }

    let earningBalance = await (
      await getEarningBalance(this.state.currentApp.app_code)
    ).data;
    let emptyData = false;

    if (earningBalance.status) {
      emptyData = false;
      earningBalance = earningBalance?.result[0]?.balances[0].liquid_balances;
    } else {
      emptyData = true;
      earningBalance = [];
    }
    let mainTempThreeNew = mainTempThree?.map((obj) => {
      return {
        ...obj,
        interest_rate: !mainTempThreeInterest.find((x) => {
          return x.coin === obj.coinSymbol;
        })
          ? 0
          : mainTempThreeInterest.find((x) => {
              return x.coin === obj.coinSymbol;
            })?.interest_rate,
        coinValueUSD: emptyData
          ? 0
          : earningBalance.find((x) => {
              return x.coinSymbol === obj.coinSymbol;
            })?.coinValueUSD,
        coinValue: emptyData
          ? 0
          : earningBalance.find((x) => {
              return x.coinSymbol === obj.coinSymbol;
            })?.coinValue,
        balance: emptyData
          ? 0
          : earningBalance.find((x) => {
              return x.coinSymbol === obj.coinSymbol;
            })?.coinValue,
        bal: emptyData
          ? 0
          : mainTemp.find((x) => {
              return x.coinSymbol === obj.coinSymbol;
            })?.coinValue,
      };
    });
    let tempObj = {
      fiat: fiatTemp,
      crypto: cryptoTemp,
      asset: mainTempThreeNew,
      bonds: tempBond,
      market: marketTemp,
    };

    this.setState({
      marketSum: marketSum,
      earningSum: earning,
      onlyEarningSum: earnedFromMM,
      onlyBondsSum: earnedFromBonds,
      bondSum: bondSumTemp,
      allMarketCoins: { ...tempObj },
      contractCoins: [...contractCoins],
    });
  };

  setUSDConversion = () => {
    convertToUSD().then((res) => {
      this.setState({ usdConversion: { ...res.data } });
    });
  };
  setScrollValue = (val) => {
    // alert(val-1)
    this.setState({ scrollValue: val > 4 ? val - 4 : 0 });
  };
  setSelectAsset = (val) => {
    this.setState({ selectAsset: val });
  };
  setBuySell = (val) => {
    this.setState({ buySell: val });
  };
  setNumbers = (val) => {
    this.setState({ numbers: { ...val } });
  };
  setTradePair = (val) => {
    this.setState({ tradePair: { ...val } });
  };
  setAllAvailableCoins = () => {
    let arr = [];
    getAllCoinsAvailable().then((res) => {
      res.data.forEach((item, i) => {
        let b = Object.keys(item);
        arr = [...arr, { coin: b[0], exchanges: res.data[i][b[0]].Exchanges }];
      });
      this.setState({ allAvailableCoins: arr });
    });
  };
  setTradeStep = (val) => {
    this.setState({ tradeStep: val });
  };
  setBaseAssets = () => {
    // getBaseAssets()
    //   .then(res => {
    //     this.setState({ baseAsset: res.data })
    //   })
  };

  setSelectedFriend = (val, bool) => {
    // console.log("Email.........", val.friendEmail)
    this.setState({ selectedFriend: val }, () =>
      bool ? console.log() : this.getFriendsProfile(val.friendEmail)
    );
  };

  setImageUploading = (val) => {
    this.setState({ imageUploading: val });
  };

  setCroppedImage = (val) => {
    this.setState({ croppedImage: val });
  };

  setMainModal = (val) => {
    this.setState({ mainModal: val });
  };

  setCroppingImage = (val) => {
    this.setState({ croppingImage: val }, () => this.setMainModal(true));
  };

  checkIfGxUser = (val) => {
    this.setState({ isGxUser: val });
  };

  updateAddFriendDetails = (obj) => {
    this.setState({ addFriendDetails: obj });
  };

  toggleTheme = () => {
    const theme = this.state.theme === "dark" ? "light" : "dark";
    this.setState({ theme });
    document.documentElement.setAttribute("data-theme", theme);
  };

  darkTheme = () => {
    if (this.state.theme === "light") return false;
    else return true;
  };

  getFriendsList = () => {
    getAddedFriends(this.state.currentApp.app_code)
      .then((res) => {
        this.setState({ currentFriendList: res.data });
        let temp = [];
        let temp_obj = {};
        let find = {};
        res.data.map((obj) => {
          find = temp.findIndex(
            (item) =>
              item.alpha.toUpperCase() ===
              obj.nickName.substring(0, 1).toUpperCase()
          );
          if (find === -1) {
            temp_obj = {
              alpha: obj.nickName.substring(0, 1).toUpperCase(),
              list: [obj],
            };
            temp = [...temp, temp_obj];
          } else {
            temp[find] = { ...temp[find], list: [...temp[find].list, obj] };
          }
        });
        let mTemp = temp.findIndex((item) => item.alpha.toUpperCase() === "#");

        console.log("friends alpha", temp);
        this.setState({ friendList: [...temp] });
      })
      .catch(() => {
        this.setState({ friendList: [] });
      });
  };

  onlyNumbers = (e) => {
    if (e.which < 46 || e.which > 57) e.preventDefault();
  };

  setLedgerOrAnalytics = (val) => {
    this.setState({ ledgerOrAnalytics: val });
  };

  setSidebarAction = (val) => {
    this.setState({ sidebarAction: { ...val } });
  };

  getNetWorth = () => {
    getTotalForexValue().then((res) => {
      this.setState({ forexNetWorth: res.data.total_sum });
    });
    let sum = 0;
    getTotalCryptoValue().then((res) => {
      let temp = res.data;
      Object.keys(temp).forEach((key) => {
        if (key === "status") {
          return;
        }
        sum = sum + parseFloat(temp[key]["price_usd"]);
      });
      this.setState({ cryptoNetWorth: sum });
    });
  };

  setLedgerDisplayList = (arr) => {
    this.setState({ ledgerDisplayList: [...arr] });
  };

  setActivePage = (val) => {
    this.setState({ ledgerActivePage: val });
  };

  setCurrentListLength = (val) => {
    this.setState({ currentListLength: val });
  };

  selectTransactions = () => {
    switch (this.state.assetSelected?.name) {
      case "Forex Currencies":
        this.setForexCryptoTransactionList(
          this.state.assetClassSelected.coinSymbol,
          true
        );
        break;
      case "Cryptocurrency":
        this.setForexCryptoTransactionList(
          this.state.assetClassSelected.coinSymbol,
          false
        );
        break;
      case "Earnings":
        this.setEarningTransactionList(
          this.state.assetClassSelected.coinSymbol
        );
        break;
      case "Bonds":
        this.setBondsTransactionList(this.state.assetClassSelected.coinSymbol);
        break;
      default:
        break;
    }
  };
  setBondsTransactionList = async (coin) => {
    this.setState({ ledgerLoading: true });
    try {
      const res = await getBondsTransactionList(coin);
      console.log("list data", res.data);
      this.setState(
        {
          earningTransactionList: res.data.interestLogs,
          ledgerDisplayList: res.data.interestLogs,
        },
        () => {
          this.setState({
            currentListLength: Math.ceil(
              this.state.ledgerDisplayList?.length / 7
            ),
            ledgerLoading: false,
          });
        }
      );
    } catch (e) {
      console.log(e);
      this.setState({ ledgerLoading: false });
    }
  };
  setEarningTransactionList = (coin) => {
    this.setState({ ledgerLoading: true });
    getEarningLogs(this.state.currentApp.app_code, coin).then((res) => {
      // console.log("response earning", res)
      if (!res.data.logs.length) {
        this.setState(
          {
            earningTransactionList: [],
            ledgerDisplayList: [],
          },
          () => {
            this.setState({
              currentListLength: Math.ceil(
                this.state.ledgerDisplayList?.length / 7
              ),
              ledgerLoading: false,
            });
          }
        );
        return;
      }
      this.setState(
        {
          earningTransactionList: res.data.logs[0].logs,
          ledgerDisplayList: res.data.logs[0].logs,
        },
        () => {
          this.setState({
            currentListLength: Math.ceil(
              this.state.ledgerDisplayList.length / 7
            ),
            ledgerLoading: false,
          });
        }
      );
    });
  };

  getBalanceList = () => {
    getVSAId().then((res) => {
      if (res.data.status) {
        // this.setState({ VSAProfileId: res.data.profile_id });
        localStorage.setItem("VsaProfileId", res.data.profile_id);
        getUserBalances(res.data.profile_id).then((resTwo) => {
          // this.setState({ coinAddresses: resTwo.data.vault.coinAddress });
        });
      }
    });
  };

  setForexTransactionList = (coin) => {
    this.setState({ ledgerLoading: true });
    getForexTransaction(coin).then((res) => {
      this.setState(
        {
          forexTransactionList: res.data.txns,
          ledgerDisplayList: res.data.txns,
        },
        () => {
          this.setState({
            currentListLength: Math.ceil(
              this.state.ledgerDisplayList.length / 7
            ),
            ledgerLoading: false,
          });
        }
      );
    });
  };
  setForexCryptoTransactionList = (coin, fiat) => {
    const { app_code, profile_id } = this.state.currentApp;
    this.setState({ ledgerLoading: true });
    getForexCryptoTransaction(app_code, profile_id, coin).then((res) => {
      if (fiat) {
        this.setState(
          {
            forexTransactionList: res.data.txns,
            ledgerDisplayList: res.data.txns,
          },
          () => {
            this.setState({
              currentListLength: Math.ceil(
                this.state.ledgerDisplayList?.length / 7
              ),
              ledgerLoading: false,
            });
          }
        );
      } else {
        this.setState(
          {
            cryptoTransactionList: res.data.txns,
            ledgerDisplayList: res.data.txns,
          },
          () =>
            this.setState({
              currentListLength: Math.ceil(
                this.state.ledgerDisplayList?.length / 7
              ),
              ledgerLoading: false,
            })
        );
      }
    });
  };
  setCryptoTransactionList = (coin) => {
    this.setState({ ledgerLoading: true });
    getCryptoTransaction(coin).then((res) => {
      this.setState(
        {
          cryptoTransactionList: res.data.txns,
          ledgerDisplayList: res.data.txns,
        },
        () =>
          this.setState({
            currentListLength: Math.ceil(
              this.state.ledgerDisplayList?.length / 7
            ),
            ledgerLoading: false,
          })
      );
    });
  };

  setIcedTransactionList = (coin) => {
    this.setState({ ledgerLoading: true });
    let temp = this.state.icedTransactionList.filter((item) => {
      return item.coin?.includes(coin) && item.lock_up_period === 3;
    });
    this.setState({ ledgerDisplayList: [...temp] }, () =>
      this.setState({
        currentListLength: Math.ceil(this.state.ledgerDisplayList.length / 7),
        ledgerLoading: false,
      })
    );
  };

  setMarketTransactionList = (coin) => {
    this.setState({ ledgerLoading: true });
    let temp = this.state.marketTransactionList.filter((item) => {
      return item.coin?.includes(coin);
    });
    this.setState({ ledgerDisplayList: [...temp] }, () =>
      this.setState({
        currentListLength: Math.ceil(this.state.ledgerDisplayList.length / 7),
        ledgerLoading: false,
      })
    );
  };

  backToDefault = () => {
    if (this.state.assetSelected.name === "Forex") {
      this.setState({
        ledgerDisplayList: [...this.state.forexTransactionList],
      });
    } else if (this.state.assetSelected.name === "Crypto") {
      this.setState({
        ledgerDisplayList: [...this.state.cryptoTransactionList],
      });
    }
  };

  handleDebitFilter = () => {
    if (this.state.assetSelected.name === "Forex") {
      let temp = this.state.forexTransactionList.filter((obj) => {
        return obj.credited;
      });
      this.setState({ ledgerDisplayList: [...temp] });
    } else if (this.state.assetSelected.name === "Crypto") {
      let temp = this.state.cryptoTransactionList.filter((obj) => {
        return obj.deposit;
      });
      this.setState({ ledgerDisplayList: [...temp] });
    }
  };

  handleCreditFilter = () => {
    if (this.state.assetSelected.name === "Forex") {
      let temp = this.state.forexTransactionList.filter((obj) => {
        return !obj.credited;
      });
      this.setState({ ledgerDisplayList: [...temp] });
    } else if (this.state.assetSelected.name === "Crypto") {
      let temp = this.state.cryptoTransactionList.filter((obj) => {
        return !obj.deposit;
      });
      this.setState({ ledgerDisplayList: [...temp] });
    }
  };

  handleTimeSort = () => {
    let temp = this.state.ledgerDisplayList.map((obj) => {
      let str = "";
      let t_date = obj.date.split(",")[0].split("/").reverse();
      str = t_date[0] + "/" + t_date[2] + "/" + t_date[1];
      return { ...obj, new_date: new Date(str) };
    });
    let sortTemp = temp.slice().sort((a, b) => a.new_date - b.new_date);
    this.setState({ ledgerDisplayList: sortTemp });
  };

  operationsOnLedgerList = (type) => {
    switch (type) {
      case "Debit":
        this.handleDebitFilter();
        break;
      case "Credit":
        this.handleCreditFilter();
        break;
      case "Date":
        this.handleTimeSort();
        break;
      default:
        this.backToDefault();
        break;
    }
  };

  setNetworkController = (val) => {
    this.setState({ netWorthController: val });
  };

  setCollapseSidebar = (val) => {
    this.setState({ collapseSidebar: val }, () =>
      val ? console.log() : this.setUserChatEnable(false)
    );
  };
  handleAnalytics = (val) => {
    // if (val === this.state.analyticsType) {
    // this.setState({ analyticsType: "" })
    // }
    // else {
    this.setState({ analyticsType: val });
    // }
  };

  resetExchangeAverageValue = () => {
    this.setState({ exchangeAverageValue: ["-", "-", "-"] });
  };
  updateExchangeAverageValue = (val) => {
    this.setState({ exchangeAverageValue: val });
  };

  setIcedCoins = () => {
    getIcedData().then((res) => {
      this.setState({
        icedNetWorth: res.data.total_usd,
        //  icedTransactionList: [...res.data.txns]
      });
      let tempObj = [];
      res.data.coins_data.map((obj) => {
        tempObj = [
          ...tempObj,
          {
            _id: obj._id,
            coinValue: obj.balance_3,
            coinSymbol: obj.coinSymbol,
            coinName: obj.coinSymbol,
            coinImage: obj.coinImage,
            period: "(3M)",
            duration: 3,
          },
          {
            _id: obj._id + "6",
            coinValue: obj.balance_6,
            coinSymbol: obj.coinSymbol,
            coinName: obj.coinSymbol,
            coinImage: obj.coinImage,
            period: "(6M)",
            duration: 6,
          },
        ];
      });
      this.setState({ icedCoins: [...tempObj] });
    });
  };

  setMarketCoins = () => {
    getMarketData().then((res) => {
      let availCoins = ["btc_balance", "eth_balance", "usdt_balance"];
      let tempObj = res.data.interest_balances;
      let i = 0;
      let temp = [];
      Object.keys(tempObj).forEach((key) => {
        if (i === 3) return;
        let coin = key.split("_")[0].toUpperCase();

        temp = [
          ...temp,
          {
            _id: nextId() + "market",
            coinValue: tempObj[availCoins[i]],
            coinSymbol: coin,
            coinName: coin,
            coinImage:
              this.state.currencyImageList &&
              this.state.currencyImageList[coin],
          },
        ];
        i = i + 1;
      });
      this.setState({
        marketNetWorth: res.data.total_earnings_usd,
        marketCoins: [...temp],
        marketTransactionList: [...res.data.txns],
      });
    });
  };

  getAllCoins = () => {
    this.setState({ coinLoading: true });
    fetchAllCoins().then((res) => {
      let temp1 = res.data.coins.filter((item) => {
        return item.type === "fiat";
      });
      let temp2 = res.data.coins.filter((item) => {
        return item.type === "crypto";
      });
      let temp = { fiat: [...temp1], crypto: [...temp2] };
      let tempObj = {};
      let tempObjTwo = {};
      let justCoins = temp1.map((obj) => {
        return obj.coinSymbol;
      });
      let justCoinsTwo = temp2.map((obj) => {
        return obj.coinSymbol;
      });
      res.data.coins.forEach((element) => {
        let t = {
          [`${element.coinSymbol}`]: element?.coinImage,
        };
        let t2 = {
          [`${element.coinSymbol}`]: {
            _24hrchange: element?._24hrchange,
            coinName: element?.coinName,
            _id: element?._id,
            coinImage: element?.coinImage,
          },
        };
        tempObj = { ...tempObj, ...t };
        tempObjTwo = { ...tempObjTwo, ...t2 };
      });
      this.setState(
        {
          currencyImageList: { ...tempObj },
          cryptoCoins: [...justCoinsTwo],
          fiatCoins: [...justCoins],
          nameImageList: { ...tempObjTwo },
        },
        () => this.setRegisteredAppsByUser()
      );
      this.setState(
        {
          allCoins: { ...temp },
        },
        () => {
          this.setState({ coinLoading: false });
        }
      );
    });
  };

  verification = (val) => {
    this.setState({ verificationLoading: val });
  };

  setTableTabs = (val) => {
    this.setState({ tableTabs: [...val] });
  };

  setSearchBox = (val) => {
    this.setState({ searchBox: val });
  };

  setAssetSelected = (value) => {
    this.setState({ assetSelected: value, assetClassSelected: null }, () =>
      value === null ? console.log() : this.setCheckedId(value.id)
    );
  };

  setAssetClassSelected = (value) => {
    this.setState({ assetClassSelected: value }, () => {
      value === null ? console.log() : this.selectTransactions();
      this.resetFullTrade();
    });
  };

  getFriendsProfile = (email) => {
    if (email === "") {
      this.setState({ selectedFriendDetails: null });
    } else {
      getFriendsDetail(this.state.currentApp.app_code, email).then((res) => {
        let tempData = res.data[0].accounts;
        let tempFiat = [],
          tempCrypto = [];
        let fiatList = [],
          cryptoList = [];
        if (!tempData?.fiat) {
        } else {
          Object.keys(tempData.fiat).forEach((key) => {
            console.log("length of INR", tempData.fiat[key].length);
            // alert(this.state.currencyImageList[key].length)
            let i = 0;
            for (i = 0; i < tempData.fiat[key].length; i++) {
              let t = {
                coinSymbol: key,
                coinImage: this.state.currencyImageList[key],
              };
              tempFiat = [...tempFiat, { ...tempData.fiat[key][i], ...t }];
            }
          });
          fiatList = [...Object.keys(tempData.fiat)];
        }

        if (!tempData?.crypto) {
        } else {
          cryptoList = [...Object.keys(tempData.crypto)];
          Object.keys(tempData.crypto).forEach((key) => {
            let i = 0;
            for (i = 0; i < tempData.crypto[key].length; i++) {
              let t = {
                coinSymbol: key,
                coinImage: this.state.currencyImageList[key],
              };
              tempCrypto = [
                ...tempCrypto,
                { ...tempData.crypto[key][i], ...t },
              ];
            }
          });

          cryptoList = [...Object.keys(tempData.crypto)];
        }
        tempData = {
          fiat: [...tempFiat],
          crypto: [...tempCrypto],
        };
        console.log("Updated data", tempData);

        console.log("Receiving", {
          ...res.data[0],
          accountList: { ...tempData },
          fiatList: fiatList,
          cryptoList: cryptoList,
        });
        this.setState({
          selectedFriendDetails: {
            ...res.data[0],
            accountList: { ...tempData },
            fiatList: fiatList,
            cryptoList: cryptoList,
          },
        });
      });
    }
  };

  setUserDetails = () => {
    getUserDetails().then(async (res) => {
      console.log("current User Details", res.data.user);
      this.setState({ currentUserDetails: res.data.user });
      let temp = {
        fullName: res.data.user.name,
        phone: "9999",
        preferredCurrency: "USD",
        appcode: this.state.currentApp.app_code,
      };
      try {
        const add = await addNewUser(temp);
        console.log("User Added", add.data);
      } catch (e) {
        console.log("Failed");
      }
    });
  };

  valueFormatter = (value, coin) => {
    let cryptoCoins = ["ETH", "BTC", "LTC", "SEF"];

    if (value === "" || value === undefined || isNaN(value)) {
      if (cryptoCoins.includes(coin?.toUpperCase())) {
        return [0.0];
      } else {
        return this.valueFormatter(0, coin);
      }
    } else {
      if (cryptoCoins.includes(coin?.toUpperCase())) {
        let tempValue = parseFloat(value)?.toFixed(9)?.toString();
        return value?.toString()?.includes(".")
          ? [tempValue?.substring(0, 6)]
          : [tempValue?.substring(0, 5)];
      } else {
        if (this.state.fiatCoins.includes(coin)) {
          let formatter = new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: coin,
            minimumFractionDigits: 2,
          });

          value = formatter.format(value);
        } else {
          value = parseFloat(value).toFixed(2);
        }
        return [value];
      }
    }
  };
  decimalValidation = (val, coin) => {
    if (!val) return;
    let final = false;
    if (val >= 1) return final;

    let temp = val.toString().split(".")[1];

    let cryptoCoins = ["ETH", "BTC", "LTC", "SEF"];
    if (cryptoCoins.includes(coin)) {
      final = parseInt(temp?.substring(0, 4)) === 0;
    } else {
      final = parseInt(temp?.substring(0, 2)) === 0;
    }
    return final;
  };

  valueFormatterInput = (value, coin) => {
    let cryptoCoins = ["ETH", "BTC", "LTC", "SEF"];

    if (value === "" || value === undefined) {
      if (cryptoCoins.includes(coin?.toUpperCase())) {
        return 0.0;
      } else {
        return 0.0;
      }
    } else {
      if (cryptoCoins.includes(coin?.toUpperCase())) {
        let tempValue = parseFloat(value).toFixed(9).toString();
        return value.toString().includes(".")
          ? tempValue.substring(0, 6)
          : tempValue.substring(0, 5);
      } else {
        value = parseFloat(value).toFixed(2);

        return value;
      }
    }
  };

  coinInitials = (coin) => {
    switch (coin) {
      case "USD":
      case "CAD":
      case "AUD":
      case "COP":
      case "ARS":
      case "USDT":
      case "MXN":
        return "$";
      case "INR":
        return "₹";
      case "GBP":
        return "£";
      case "EUR":
        return "€";
      case "JPY":
      case "CNY":
        return "¥";
      default:
        return "";
    }
  };

  addFriendFunction = () => {
    addFriend(this.state.addFriendDetails);
  };

  getExchangeList = () => {
    exchangeData().then((res) => {
      let tempTwo = res.data.data.map((obj) => {
        return {
          exchangeId: obj.formData.exchangeID,
          [this.volume]: 0,
          price: 0,
          percentChange: 0,
          image: obj.formData.image,
          buyPrice: 0,
          sellPrice: 0,
          fullImage: obj.formData.fullImage,
          fullImageBW: obj.formData.fullImageBW,
        };
      });
      this.setState({
        exchangeList: [...tempTwo],
        exchangeListStatic: [...tempTwo],
      });
    });
  };

  getExchangeTableDataTwo = async () => {
    this.setTradeStep(5);
    let arr = [];
    await exchangeData().then((resTwo) => {
      resTwo.data.data.map((resThree) => {
        arr = [
          ...arr,
          {
            exchangeId: resThree.formData.exchangeID,
            [this.volume]: 0,
            price: 0,
            percentChange: 0,
            image: resThree.formData.image,
            buyPrice: 0,
            sellPrice: 0,
            fullImage: resThree.formData.fullImage,
            fullImageBW: resThree.formData.fullImageBW,
          },
        ];
      });
      this.setState(
        {
          exchangeDataTwo: arr,
          exchangeAverageValue: [0, 0, 0],
        },
        () => {
          this.setTradeStep(6);
        }
      );
    });
    // })
  };

  updateExchangeList = () => {
    if (!this.state.filterExchangeListState[0]) return;

    let temp = this.state.exchangeListStatic.filter((x) => {
      return this.state.filterExchangeListState[0].includes(x.exchangeId);
    });
    this.setState({ exchangeList: [...temp] });
  };
  filterExchangeList = () => {
    if (
      this.state.tradePair[0].coin === "" ||
      this.state.tradePair[1].coin === ""
    )
      return;

    let tempList = [];
    this.state.availablePairs.map((x) => {
      if (
        x.pair ===
        `${this.state.tradePair[0].coin}/${this.state.tradePair[1].coin}`
      ) {
        tempList = [...tempList, x.Exchanges];
      }
    });
    // getAvailablePair
    this.setState(
      {
        filterExchangeListState: [...tempList],
      },
      () => {
        this.updateExchangeList();
      }
    );
  };
  getAvailablePairList = () => {
    getAvailablePairs().then((res) => {
      this.setState({ availablePairs: res.data });
    });
  };
  setUpdate3 = (val) => {
    this.setState({ update3: val });
  };

  handleKeyPress = (e) => {
    if (e.which < 46 || e.which > 57) {
      e.which === 13 ? console.log() : e.preventDefault();
    }
  };
  setCryptoToUSD = () => {
    cryptoUSD().then((res) => {
      this.setState({ cryptoUSD: res.data });
    });
  };

  placeMarketTrade = (exchangeId, paidId1, paidId2, value, buySell) => {
    this.setState({ executionMarketLoading: true });
    let bs = buySell;
    // let pI2 = this.state.selectAsset.exchangeId === "Binance" && this.state.apiCurrency[1].coin === 'USD'?'USDT': 'USD';
    placeTrade(exchangeId, paidId1, paidId2, value, bs).then((res) => {
      if (res.data.status === true) {
        // this.setBaseAssets();
        this.setState(
          {
            tradeBuySellLoading: false,
            value: "",
            terminalExecutionTotalValue: 0,
          },
          () => {
            this.changeNotification({
              resetBuySellValue: !this.state.resetBuySellValue,
              status: true,
              message: res.data.message,
            });
          }
        );
      } else {
        this.setState(
          {
            value: "",
            terminalExecutionTotalValue: 0,
          },
          () => {
            this.setState({ tradeBuySellLoading: false });
            this.changeNotification({
              resetBuySellValue: !this.state.resetBuySellValue,
              status: false,
              message: "Failed",
            });
            // this.changeNotification({ status: false, message: "Failed" });
          }
        );
      }
    });
  };

  changeNotification = (val) => {
    this.setState({ notification: val }, () =>
      setTimeout(
        () =>
          this.setState({
            notification: {
              status: "",
              message: "",
            },
          }),
        2000
      )
    );
  };
  initialLoads = () => {
    this.verification(true);

    this.getAllCoins();
    // this.getBalanceList()

    this.getNetWorth();
    // this.getFriendsList()
    // this.getFriendsProfile()

    // this.setBaseAssets()
    this.setAllAvailableCoins();
    this.getExchangeList();
    this.getAvailablePairList();
    this.setCryptoToUSD();
    // this.getAllMarketCoins()
    this.setUSDConversion();
  };
  render() {
    return (
      <Agency.Provider
        value={{
          ...this.state,
          initialLoads: this.initialLoads,
          setCryptoToUSD: this.setCryptoToUSD,
          changeNotification: this.changeNotification,
          placeMarketTrade: this.placeMarketTrade,
          handleKeyPress: this.handleKeyPress,
          getExchangeList: this.getExchangeList,
          fillExchangeList: this.fillExchangeList,
          getExchangeTableDataTwo: this.getExchangeTableDataTwo,
          setCollapseSidebar: this.setCollapseSidebar,
          valueFormatter: this.valueFormatter,
          getAllCoins: this.getAllCoins,
          imageUploader: this.imageUploader,
          setUserDetails: this.setUserDetails,
          verification: this.verification,
          setAssetSelected: this.setAssetSelected,
          setTableTabs: this.setTableTabs,
          getFriendsList: this.getFriendsList,
          setAssetClassSelected: this.setAssetClassSelected,
          setSearchBox: this.setSearchBox,
          handleAnalytics: this.handleAnalytics,
          setNetworkController: this.setNetworkController,
          getBalanceList: this.getBalanceList,
          setCurrentListLength: this.setCurrentListLength,
          setActivePage: this.setActivePage,
          setLedgerDisplayList: this.setLedgerDisplayList,
          selectTransactions: this.selectTransactions,
          operationsOnLedgerList: this.operationsOnLedgerList,
          getNetWorth: this.getNetWorth,
          setSidebarAction: this.setSidebarAction,
          setLedgerOrAnalytics: this.setLedgerOrAnalytics,
          toggleTheme: this.toggleTheme,
          darkTheme: this.darkTheme,
          updateExchangeAverageValue: this.updateExchangeAverageValue,
          resetExchangeAverageValue: this.resetExchangeAverageValue,
          setBaseAssets: this.setBaseAssets,
          getFriendsProfile: this.getFriendsProfile,
          updateAddFriendDetails: this.updateAddFriendDetails,
          checkIfGxUser: this.checkIfGxUser,
          onlyNumbers: this.onlyNumbers,
          setMainModal: this.setMainModal,
          setCroppingImage: this.setCroppingImage,
          setCroppedImage: this.setCroppedImage,
          setImageUploading: this.setImageUploading,
          setSelectedFriend: this.setSelectedFriend,
          setIcedCoins: this.setIcedCoins,
          setMarketCoins: this.setMarketCoins,
          setTradeStep: this.setTradeStep,
          setAllAvailableCoins: this.setAllAvailableCoins,
          setTradePair: this.setTradePair,
          setBuySell: this.setBuySell,
          setNumbers: this.setNumbers,
          getAvailablePairList: this.getAvailablePairList,
          setUpdate3: this.setUpdate3,
          setSelectAsset: this.setSelectAsset,
          getAllMarketCoins: this.getAllMarketCoins,
          setScrollValue: this.setScrollValue,
          setUSDConversion: this.setUSDConversion,
          setRegisteredAppsByUser: this.setRegisteredAppsByUser,
          setCurrentApp: this.setCurrentApp,
          setMainTabs: this.setMainTabs,
          enableOverlay: this.enableOverlay,
          setSelectedTransfer: this.setSelectedTransfer,
          setToTransfer: this.setToTransfer,
          setStepMain: this.setStepMain,
          setTransferValue: this.setTransferValue,
          finalTransferSubVault: this.finalTransferSubVault,
          setConversionRates: this.setConversionRates,
          setInstaCryptoSupportedAssets: this.setInstaCryptoSupportedAssets,
          setTransfer: this.setTransfer,
          valueFormatterInput: this.valueFormatterInput,
          setExternalWithdrawStep: this.setExternalWithdrawStep,
          setExternalWithdrawFriend: this.setExternalWithdrawFriend,
          setContactStep: this.setContactStep,
          setNewPassword: this.setNewPassword,
          setSecondGate: this.setSecondGate,
          setTradeState: this.setTradeState,
          setTradeTabStep: this.setTradeTabStep,
          setTradeDashboardStep: this.setTradeDashboardStep,
          setTradePairObj: this.setTradePairObj,
          setTradeFinalObj: this.setTradeFinalObj,
          setTradeStats: this.setTradeStats,
          setTradeBuySell: this.setTradeBuySell,
          setBuyValue: this.setBuyValue,
          setSellValue: this.setSellValue,
          finalTradeExecute: this.finalTradeExecute,
          setReadyForTrade: this.setReadyForTrade,
          resetFullTrade: this.resetFullTrade,
          resetFullTradeTerminal: this.resetFullTradeTerminal,
          resetTradeDashboard: this.resetTradeDashboard,
          setCheckedId: this.setCheckedId,
          setEditUserAccount: this.setEditUserAccount,
          setCurrentUserDetailsTemp: this.setCurrentUserDetailsTemp,
          callUserDetail: this.callUserDetail,
          changeLogin: this.changeLogin,
          setAdminController: this.setAdminController,
          coinInitials: this.coinInitials,
          decimalValidation: this.decimalValidation,
          setFundsConfig: this.setFundsConfig,
          setFundsFrom: this.setFundsFrom,
          setFundsTo: this.setFundsTo,
          setFundsStepOneConfig: this.setFundsStepOneConfig,
          setFundsSelectedApp: this.setFundsSelectedApp,
          setFundsLoading: this.setFundsLoading,
          setThreeSteps: this.setThreeSteps,
          setRefreshSession: this.setRefreshSession,
          setRefreshApp: this.setRefreshApp,
          setAdminPassword: this.setAdminPassword,
          setGrantAdminAccess: this.setGrantAdminAccess,
          setAppDownloadConfig: this.setAppDownloadConfig,
          setMiConfig: this.setMiConfig,
          setWhichOne: this.setWhichOne,
          setSelectedCoinAndApp: this.setSelectedCoinAndApp,
          withdrawFromBondsEarning: this.withdrawFromBondsEarning,
          setMainSteps: this.setMainSteps,
          setMiAmount: this.setMiAmount,
          resetMI: this.resetMI,
          setMoneyCoins: this.setMoneyCoins,
          setUserChatEnable: this.setUserChatEnable,
          setPreImageUpload: this.setPreImageUpload,
          resetImage: this.resetImage,
          setPreviousMainTab: this.setPreviousMainTab,
          setMainBalance: this.setMainBalance,
          setLedgerId: this.setLedgerId,
          setLearnVideo: this.setLearnVideo,
          setLearnArticle: this.setLearnArticle,
          setDevPassword: this.setDevPassword,
          setDevAccess: this.setDevAccess,
          setCStep: this.setCStep,
          setPlayVideo: this.setPlayVideo,
          setReadArticle: this.setReadArticle,
          setShowApps: this.setShowApps,
          setTransactionDeposit: this.setTransactionDeposit,
          setType: this.setType,
          enableuserfunctiom: this.enableuserfunctiom,
          enableuserfunctiomclose: this.enableuserfunctiomclose,
          setExternalAction: this.setExternalAction,
          setExternalAction: this.setExternalAction,
          globalHandleFinalImage: this.globalHandleFinalImage,
          setSelectedInvestmentType: this.setSelectedInvestmentType,
          setCheckingLoggedIn: this.setCheckingLoggedIn,
          setAnalyticsSection: this.setAnalyticsSection,
          setConversionConfig: this.setConversionConfig,
          setPortfolioFinalValue: this.setPortfolioFinalValue,
          setPortfolioStep: this.setPortfolioStep,
          setUpdatePortfolio: this.setUpdatePortfolio,
          setGoToSettings: this.setGoToSettings,
          setExplainNavbarTerm: this.setExplainNavbarTerm,
          setManualUpload: this.setManualUpload,
          setAspectRatio: this.setAspectRatio,
          setInvestmentPath: this.setInvestmentPath,
          setDisplayBondDate: this.setDisplayBondDate,
          setTradeNavbarType: this.setTradeNavbarType,
          setDifferentiator: this.setDifferentiator,
          setSupportMessage: this.setSupportMessage,
          setMoreInfoOfApp: this.setMoreInfoOfApp,
          setDirectCoinOverview: this.setDirectCoinOverview,
          setSkipTradeStep: this.setSkipTradeStep,
          setPortfolioActive: this.setPortfolioActive,
          setFullScreenChat: this.setFullScreenChat,
          setCopiedOverlay: this.setCopiedOverlay,
        }}
      >
        {this.props.children}
      </Agency.Provider>
    );
  }
}
