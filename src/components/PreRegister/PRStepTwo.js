import React from 'react'
import Images from '../../assets/a-exporter';
import Constant from '../../json/constant';

export default function PRStepTwo({ details, setDetails, setCurrentStep, appInfo }) {
    const [drawer, setDrawer] = React.useState(false);
    const [state, setState] = React.useState({ number: false, cap: false, special: false, length: false, all: false })
    const oneNumberRegex = /.*[0-9].*/gm
    const oneCapRegex = /.*[A-Z].*/gm
    const oneSpecialRegex = /.*[@#$%^&+=!].*/gm

    const handleChange = (e) => {
        setDetails({ ...details, newPassword: e.target.value });
        checkPassword(e.target.value)

    }
    const checkPassword = (newPassword) => {
        let tempObj = {};
        let a = oneNumberRegex.test(newPassword);
        let b = oneCapRegex.test(newPassword);
        let c = oneSpecialRegex.test(newPassword);
        let d = newPassword.length >= 8;

        let e = a && b && c && d
        setState({ number: a, cap: b, special: c, length: d, all: e })

        console.log("password", state)
    }
    return (
        <div className="pr-step-two">
            <img src={!appInfo.app_icon ? Images.agencyDark : appInfo.app_icon} />
            <p>Great Job. Now Assign A Permanent Password</p>
            <h6>
                <input type="password" onChange={handleChange} placeholder="Password" />
                <span style={state.all ? { backgroundColor: "green" } : { backgroundColor: "red" }} />
            </h6>
            <div className="button-container">
                <button onClick={() => setDrawer(true)}>Password Requirements</button>
                <button onClick={() => setCurrentStep(2)} disabled={!state.all}>Next</button>
            </div>
            <div className={`pr-step-two-check-list ${drawer ? "drawer-up" : ""}`}>
                <h4>Password Requirements</h4>
                <button onClick={() => setDrawer(false)}>
                    <img src={Images.addDark} />
                </button>
                {
                    Constant.passwordChecklist.list.map(obj => <h6>
                        {obj.text} <span style={state[obj.type] ? { backgroundColor: "green" } : { backgroundColor: "red" }} />
                    </h6>
                    )
                }

            </div>

        </div>
    )
}
