import React from 'react'
import Images from '../../assets/a-exporter';
import { login } from '../../services/postAPIs';

export default function PRStepOne({ details, setDetails, setCurrentStep, appInfo }) {
    const emailRegex = /([\w\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?/gm
    const [error, setError] = React.useState(false);
    const [loading, setLoading] = React.useState(false)
    const handleSubmit = async () => {
        setLoading(true);
        let res = await login(details.email, details.password);
        if (res.data.status) {
            setCurrentStep(1);

        } else {
            setCurrentStep(false);
            setError(true);
        }
        setLoading(false);

    }
    return (
        <div className="pr-step-one">
            <img src={!appInfo.app_icon ? Images.agencyDark : appInfo.app_icon} />
            <p>Enter The Temporary Credentials You Got In Your Email</p>
            <div>
                <input autoFocus="true" autoComplete="off" type="email" onChange={(e) => setDetails({ ...details, email: e.target.value })} placeholder="Email" />
                <input autoComplete="new-password" name="emailPassword" type="password" onChange={(e) => setDetails({ ...details, password: e.target.value })} placeholder="Password" />
                {error ? <span>*Wrong Credentials. Please Try Again</span> : ''}
            </div>
            <button onClick={handleSubmit} disabled={!details.email || !details.password || !emailRegex.test(details.email)}>{loading ? "Loading..." : "Next Step"}</button>
        </div>
    )
}
