import React from 'react'
import Images from '../../assets/a-exporter';
import LoadingAnimation from '../../lotties/LoadingAnimation';
import { preRegisterLogin } from '../../services/postAPIs';

export default function PRStepThree({ details, setDetails,setCredentials, setCurrentStep, setFinal, appInfo }) {
    const [cPassword, setCPassword] = React.useState('');
    const [loading, setLoading] = React.useState(false);
    const handleSubmit =async () => {
        setLoading(true);
        let res = await preRegisterLogin({ ...details });
        if (res.data.status) {
            setCurrentStep(0);
            setCredentials({email: details.email, password: details.newPassword})
            setFinal({ status: true, success: true })
        } else {
            setCurrentStep(0);
            setFinal({ status: true, success: false })

        }
        setLoading(false);
    }


    return (
        loading ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />
            </div>
            :
            <div className="pr-step-two">
                <img src={!appInfo.app_icon ? Images.agencyDark : appInfo.app_icon} />
                <p>Great Job. Now Confirm Your Password</p>
                <h6>
                    <input type="password" onChange={e => setCPassword(e.target.value)} placeholder="Password" />
                    <span style={cPassword === details.newPassword ? { backgroundColor: "green" } : { backgroundColor: "red" }} />

                </h6>
                <div className="button-container">
                    <button onClick={() => { setCurrentStep(1); setDetails({ ...details, newPassword: '' }) }}>Change Password</button>
                    <button onClick={() => handleSubmit()} disabled={cPassword !== details.newPassword}>Next</button>
                </div>
            </div>
    )
}
