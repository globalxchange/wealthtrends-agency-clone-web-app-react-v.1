import React from 'react'
import Images from '../../assets/a-exporter';

export default function PRFinalPage({ setPreRegister, setDetails, handlefinalclick, appInfo, setFinal, final }) {
    const restart = () => {
        if (final.success) {
            setPreRegister(false);
            setFinal({ status: false, success: false });
            handlefinalclick();

        } else {
            setDetails({ email: '', password: '', newPassword: '' })
        }
    }
    return (
        <div className="pr-final-image">
            <img src={!appInfo.app_icon ? Images.agencyDark : appInfo.app_icon} />
            <h6>{final.success ? "You Have Successfully Completed Your Registration" : "Something Went Wrong And You Registration Didn't Get Completed Yet"}</h6>
            <div>
                <button onClick={() => restart()}>{final.success ? "Login To Your Account" : "Try Again"}</button>
                <button onClick={() => setPreRegister(false)}>Close</button>
            </div>

        </div>
    )
}
