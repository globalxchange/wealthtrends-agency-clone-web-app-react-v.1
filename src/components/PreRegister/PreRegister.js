import React from 'react'
import nextId from 'react-id-generator'
import { useHistory } from 'react-router-dom'
import LoadingAnimation from '../../lotties/LoadingAnimation';
import NextSVG from '../common-components/assetsSection/NextSVG'
import './pre-register.style.scss'
import PRFinalPage from './PRFinalPage';
import PRStepOne from './PRStepOne';
import PRStepThree from './PRStepThree';
import PRStepTwo from './PRStepTwo';
export default function PreRegister({ appInfo,handlefinalclick,setCredentials, setPreRegister }) {
    const history = useHistory();
    const [final, setFinal] = React.useState({
        status: false,
        success: null
    })
    const [currentStep, setCurrentStep] = React.useState(0);
    const [details, setDetails] = React.useState({
        email: '',
        password: '',
        newPassword: ''
    })

    const stepComponent = () => {
        switch (currentStep) {
            case 0: return <PRStepOne appInfo={appInfo} setCurrentStep={setCurrentStep} details={details} setDetails={setDetails} />
            case 1: return <PRStepTwo appInfo={appInfo} setCurrentStep={setCurrentStep} details={details} setDetails={setDetails} />
            case 2: return <PRStepThree setCredentials={setCredentials} setFinal={setFinal} appInfo={appInfo} setCurrentStep={setCurrentStep} details={details} setDetails={setDetails} />
        }
    }
    React.useEffect(() => {
        if (!appInfo) {
            history.push("/login")
        }
    }, [appInfo])

    return (
        !appInfo ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />
            </div>
            :
        final.status ?
            <PRFinalPage handlefinalclick={handlefinalclick} details={details} setDetails={setDetails} setFinal={setFinal} final={final} appInfo={appInfo} setPreRegister={setPreRegister} />
            :
            <div className="pre-register-main">
                <div className="pre-register-main-left col-md-4 col-12">

                    <h2>Pre-Registration</h2>
                    {
                        steps.map(obj => <h6
                            key={obj.keyId}
                            className={obj.step <= currentStep ? "" : "hide-the-step"}
                        >{obj.text}
                            {obj.step < currentStep ? <NextSVG type="ticked" /> : ""}
                        </h6>)
                    }

                </div>
                <div className="pre-register-main-right col-md-8 col-12">
                    {stepComponent()}
                </div>
            </div>
    )
}
const steps = [
    { keyId: nextId(), text: "Step 1: Identify Yourself", step: 0 },
    { keyId: nextId(), text: "Step 2: Create New Password", step: 1 },
    { keyId: nextId(), text: "Step 2: Confirm Password", step: 2 },
]