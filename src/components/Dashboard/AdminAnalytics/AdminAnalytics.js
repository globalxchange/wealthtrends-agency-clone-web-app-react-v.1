import React from 'react'
import nextId from 'react-id-generator'
import Images from '../../../assets/a-exporter'
import { Agency } from '../../Context/Context'
import './admin-analytics.style.scss'
export default function AdminAnalytics() {
    const  agency  = React.useContext(Agency)
    return (
        <div className="admin-analytics">
            <div className="admin-analytics-header">
                <div className="analytics-header-left">
                    <div>
                        <h3>{agency.currentApp.app_name}</h3>
                        <p>Agency Analytics</p>
                    </div>
                    <div>
                        <input placeholder="Search Agency..." />
                        <span>All Data</span>
                        <button><img src={Images.searchWhite} /></button>
                    </div>

                </div>
                <div className="analytics-header-right">
                    {
                        header.map(obj =>
                            <div>
                                <h3>{obj.value}</h3>
                                <span><img src={obj.flag} />{obj.name}</span>

                                <button>{obj.button}</button>
                            </div>)
                    }

                </div>

            </div>
            <div className="admin-analytics-data">
                <div className="admin-analytics-data-left">
                    {
                        data.map(obj =>
                            <div className="data-wrapper">
                                <div className="data-wrapper-header">
                                    {
                                        obj.header.map(b => <button>{b}</button>)
                                    }
                                </div>
                                <div className="data-wrapper-body">
                                    {
                                        obj.list.map(l =>
                                            <div>
                                                <span>{l.name}</span>
                                                <h6>{l.value}</h6>
                                            </div>
                                        )
                                    }
                                </div>


                            </div>
                        )
                    }

                </div>
                <div className="admin-analytics-data-right">
                    <button>All Time <img src={Images.triangle} /></button>
                    <button>All Products <img src={Images.triangle} /></button>

                </div>

            </div>
            <div className="admin-analytics-body">
                <div className="admin-analytics-body-left">
                    <div className="admin-sidebar">
                        {
                            sidebar.map(obj =>
                                <div>
                                    <h6>{obj}</h6>

                                </div>
                            )
                        }

                    </div>
                    <div className="admin-body">
                        <div className="admin-body-header">
                            {
                                ["Profit", "All Time", "All Products"].map(obj => <button>{obj}</button>)
                            }

                        </div>
                        <div className="admin-body-main">
                            <div className="admin-body-main-left">

                            </div>
                            <div className="admin-body-main-right">
                                {
                                    ["X-Axis Frequency", "Years", "Quarters", "Months", "Weeks", "Weeks"].map(obj => <button>{obj}</button>)
                                }

                            </div>



                        </div>
                        <div className="admin-body-footer">
                            {
                                [2006, 2007, 2008, 2009, 2010, 2011, 2012].map(obj =>
                                    <button>{obj}</button>
                                )
                            }

                        </div>

                    </div>

                </div>





                <div className="admin-analytics-body-right">
                    <div className="admin-transaction-header">
                        <h6>Transactions</h6>

                    </div>
                    <div className="admin-transaction-body">
                        {
                            [1, 1, 1, 1, 1, 1].map(obj =>
                                <div className="transaction-wrapper">
                                    <span className="span" />
                                    <div>
                                        <h6>John Adams</h6>
                                        <span>Spartan Subscriptions</span>
                                    </div>
                                    <span className="span">$123.23</span>
                                </div>
                            )
                        }

                    </div>
                    <div className="admin-transaction-footer">
                        <h6>All Stats</h6>

                    </div>

                </div>


            </div>

        </div>
    )
}
const header = [
    { keyId: nextId(), value: "$124,456.03", flag: Images.flag, name: "Agency Valuation", button: "Analyse" },
    { keyId: nextId(), value: "$1234.03", flag: Images.flag, name: "Agency Vaults", button: "Withdraw" },
]

const data = [
    {
        header: ["Users", 92],
        list: [
            { keyId: nextId(), name: "Customers", value: "9,342" }
        ]
    },
    {
        header: ["Users", 92],
        list: [
            { keyId: nextId(), name: "Txn’s", value: "9,342" },
            { keyId: nextId(), name: "One Time", value: "342" },
            { keyId: nextId(), name: "Reocurring", value: "1,234" },
        ]
    },
    {
        header: ["Users", 92],
        list: [
            { keyId: nextId(), name: "Affiliates", value: "9,342" }
        ]
    },
]
const sidebar = ["Per Product", "Per Transaction", "Per Customer", "Per Affiliate"]