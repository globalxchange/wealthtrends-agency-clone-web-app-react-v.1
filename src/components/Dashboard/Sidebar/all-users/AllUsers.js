import React, { useEffect } from 'react'
import { useContext } from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../assets/a-exporter'
import { getUserList } from '../../../../services/getAPIs'
import { Agency } from '../../../Context/Context'
import './all-users.style.scss'
export default function AllUsers() {
    const agency = useContext(Agency);
    const [userList, setUserList] = React.useState([])
    const [searchTerm, setSearchTerm] = React.useState([])
    const [selectedOption, setSelectedOptions] = React.useState(options[0]);
    const [openOptions, setOpenOptions] = React.useState(false);
    const { setSidebarAction, setCollapseSidebar, changeLogin } = agency;


    const getUsers = async () => {
        try {
            const res = await getUserList(agency.currentApp.app_code);
            console.log("user list", res.data.users);
            setUserList(res.data.users)
        } catch (e) {
            console.error("User list fetching failed", e)
        }

    }
    const handleClick = (obj) => {
        localStorage.setItem("userEmail", obj.email);
        localStorage.setItem("otherAccount", JSON.stringify(obj))
        setSidebarAction({ count: 0, type: '' });
        setCollapseSidebar(false)
        changeLogin(true, obj);
    }

    const filterList = () =>{
        let a =userList.filter(user => {
             return selectedOption.name ==="Username"?
              user.name.toString().toLowerCase().startsWith(searchTerm.toString().toLowerCase())
              : 
              user.email.toString().toLowerCase().startsWith(searchTerm.toString().toLowerCase())
            })
        return a;    

    }

    useEffect(() => {
        getUsers();

    }, [])
    return (
        <div className="all-users">
            <div className="all-users-searchbar">
                <input onChange={(e) => setSearchTerm(e.target.value)} placeholder={`Search By ${selectedOption.name}`} />
                <button onClick={() => setOpenOptions(true)}>{selectedOption.name}</button>
            </div>
            <div className="all-users-body">
                {
                    !openOptions ?
                        filterList().map((obj) =>
                            <div onClick={() => handleClick(obj)} className="user-wrapper">
                                <img src={!obj.profilePic ? Images.face : obj.profilePic} />
                                <div className="user-details-wrapper">
                                    <h5>{obj.name}</h5>
                                    <span>{obj.email}</span>
                                </div>
                            </div>
                        )
                        :
                        <div className="Select-option">
                            <h6>Currently Selected</h6>
                            <div className="selected-option">
                                {selectedOption.name}
                            </div>
                            <h6>Other Options</h6>
                            {
                                options
                                    .filter(obj => { return obj.name !== selectedOption.name })
                                    .map(objTwo =>
                                        <div className="not-selected-option" onClick={() => { setOpenOptions(false); setSelectedOptions(objTwo) }}>
                                            {objTwo.name}
                                        </div>)
                            }


                        </div>
                }
            </div>
        </div>
    )
}
const options = [
    { keyId: nextId(), name: "Username" },
    { keyId: nextId(), name: "Email" },
    { keyId: nextId(), name: "Upline" },
]