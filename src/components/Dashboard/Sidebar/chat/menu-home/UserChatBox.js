import React from 'react'
import './user-chat-box.style.scss'
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import nextId from 'react-id-generator';
import moment from "moment-timezone";
import Images from '../../../../../assets/a-exporter';
import { Agency } from '../../../../Context/Context';
export default function UserChatBox({ messageArray = [], loadMore, getOlderMessages, setOtherUser, setThreadId, setMessageArray, otherUser, sendMessage, userObj, message, setMessage }) {
    const agency = React.useContext(Agency);
    const { globalHandleFinalImage, imageUploading } = agency;
    const [scrollHeight, setScrollHeight] = React.useState()

    const chatListRef = React.useRef();
    React.useEffect(() => {
        if (loadMore) {
            chatListRef.current.scroll({ top: chatListRef.current.scrollHeight - scrollHeight });
            return;
        }
        else if (!chatListRef?.current) return;
        chatListRef.current.scroll({ top: chatListRef.current.scrollHeight });
    }, [messageArray]);
    React.useEffect(() => {
        return () => {
            setThreadId('');
            setMessageArray([]);
            setOtherUser(null)
        }
    }, [])


    return (
        <div className="chat-box-main">
            <div
                ref={chatListRef}
                className="chat-box-body"
            >
                <p onClick={() => { getOlderMessages(); setScrollHeight(chatListRef.current.scrollHeight); console.log("full height before", chatListRef.current.scrollHeight) }} className={!messageArray.length ? "d-none" : ""}>Load More Messages</p>
                {
                    !messageArray.length ?
                        <h6>You Didn't Start Any Conversation With {otherUser.first_name} Yet</h6>
                        :
                        messageArray.map((obj) =>
                            <div style={obj.notSent ? { opacity: 0.1 } : {}} key={obj._id + nextId()}
                                className={`single-message-wrapper ${obj?.type === "file" ?
                                    !obj.sender?.username ? obj?.sender?.toLowerCase() === userObj?.username?.toLowerCase() ? "reverse-it" : "" :
                                        obj?.sender?.username?.toLowerCase() === userObj?.username?.toLowerCase() ? "reverse-it" : ""

                                    :
                                    !obj.sender?.username ? obj?.sender?.toLowerCase() === userObj?.username?.toLowerCase() ? "reverse-it" : "" :
                                        obj?.sender?.username?.toLowerCase() ===
                                            userObj?.username?.toLowerCase()
                                            ? "reverse-it"
                                            : ""
                                    }`}
                            >
                                <div className="messaged-by">
                                    <img height="20px" src={!obj.sender.avatar ? Images.face : obj.sender.avatar} />
                                    <span>{moment(obj.timestamp).format("hh:mm a")}</span>
                                </div>
                                {true ? (
                                    obj.type === "file" ? (
                                        <div className="chat-image-container">
                                            <img src={obj.location} />
                                        </div>
                                    ) : (
                                            <div className="simple-message">
                                                <span>{obj.message}</span>
                                            </div>
                                        )
                                ) : (
                                        <div className="transaction-message">
                                            <span>
                                                <img height="20px" src={Images.bitcoin} /> {obj.coin}
                                            </span>
                                            <span>{obj.amount}</span>
                                        </div>
                                    )}
                            </div>
                        )
                }
            </div>
            <div className="chat-box-footer">
                <label htmlFor="filesUpload">
                    <input
                        onChange={e => globalHandleFinalImage(e, { aspect: 16 / 9 })}
                        id="filesUpload"
                        type="file"
                        className="d-none"
                    />
                    {imageUploading ?
                        <LoadingAnimation size={{ height: "40px", width: "40px" }} /> :
                        <img src={Images.fileUpload} />
                    }
                </label>
                <div className="chat-box-input-wrapper">
                    <input
                        // onPaste={pasteImageHandler}
                        onKeyPress={(e) =>
                            e.which === 13 ? !message ? {} : sendMessage() : {}
                        }
                        value={message}
                        placeholder="Enter Your Message"
                        onChange={(e) => setMessage(e.target.value)}
                    />
                </div>
                <button disabled={!message}
                    onClick={sendMessage}
                >
                    <img src={Images.sendIcon} />
                </button>
            </div>
        </div>
    )
}
