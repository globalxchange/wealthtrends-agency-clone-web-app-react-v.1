import React from 'react'
import ioClient from "socket.io-client";
import axios from "axios";
import { Agency } from '../../../../Context/Context';
import ChatList from './ChatList';
import UserChatBox from './UserChatBox';
import Constant from '../../../../../json/constant';
export default function MenuHome({ setWhichPageChat, refresh, setVisibleMoreInfo, setSelections, setBotController, botHeader, whichPageChat }) {
    const socketRef = React.useRef();
    const userSocketRef = React.useRef();
    const agency = React.useContext(Agency);
    const { mainAppId, localImage, resetImage, setRefreshApp, setDifferentiator, changeNotification } = agency;
    const [userObj, setUserObj] = React.useState(null);
    const [globalUserObj, setGlobalUserObj] = React.useState(null);
    const [userList, setUserList] = React.useState([]);
    const [recentChat, setRecentChat] = React.useState([]);
    const [loading, setLoading] = React.useState(true);
    const [otherUser, setOtherUser] = React.useState(null);
    const [threadId, setThreadId] = React.useState('');
    const [message, setMessage] = React.useState('');
    const [messageArray, setMessageArray] = React.useState([]);
    const [loadMore, setLoadMore] = React.useState(false)

    React.useEffect(() => {
        if (userObj === null) return;
        let email = localStorage.getItem("userEmail");
        let token = localStorage.getItem("idToken");
        socketRef.current = ioClient("https://testsockchatsio.globalxchange.io", {
            query: {
                email: email,
                token: token,
                tokfortest: "nvestisgx",
            },
        });
        userSocketRef.current = ioClient(
            "https://testsockchatsio.globalxchange.io/user",
            {
                query: {
                    email: email,
                    token: token,
                    tokfortest: "nvestisgx",
                },
            }
        );
        // getAdminsAndCheck();
        getFriendsList();
        getAllIntegrationList();
        return () => {
            userSocketRef.current.off("get_online_user_list");
        };
    }, [userObj]);

    const getFriendsList = () => {
        socketRef.current.emit("get_all_user_list", mainAppId, (response) => {
            setUserList(response);
            console.log("User list", response);
            setLoading(false);
            // getFirstMessages();
        })
    }
    const getAllIntegrationList = () => {
        socketRef.current.emit("get_user_interaction_list_read_unread_list", userObj, mainAppId, (res) => {
            console.log("interacted user", res);
            setRecentChat(res.interacting_list);
        })
    }
    const getOlderMessages = () => {
        let temp = {
            thread_id: threadId,
            page: Math.ceil(messageArray.length / 30) + 1,
            app_id: mainAppId
        }
        socketRef.current.emit("direct_message_archive_history", temp, (response) => {
            setLoadMore(true)
            console.log([...messageArray, ...response.reverse()], temp)
            setMessageArray([...response, ...messageArray])

        })
    }
    const sendMessage = () => {
        let temp = {
            message: message,
            thread_id: threadId,
            sender: userObj.username,
            timestamp: Date.now()
        }
        setLoadMore(false)
        setMessageArray([...messageArray, { ...temp, notSent: true }])
        socketRef.current.emit("new_message", temp, mainAppId, (res) => {
            console.log("messageeeee", res)
            if (res === "success") {
                // setMessageArray([...messageArray, { ...temp }])
                if (!messageArray.length) {
                    messageArray[0] = { ...temp, notSent: false };

                } else {
                    messageArray[messageArray.length - 1] = { ...temp, notSent: false };
                }
                setMessageArray([...messageArray]);
                setMessage('')
            } else {

            }
        })
    }
    const sendFileMessage = () => {
        let temp = {
            message: Date.now().toString(),
            thread_id: threadId,
            sender: userObj?.username,
            timestamp: Date.now(),
            filename: Date.now().toString(),
            type: "file",
            location: localImage,

        }
        socketRef.current.emit("new_message", temp, mainAppId, (res) => {
            if (res === "success") {
                setMessageArray([...messageArray, { ...temp }]);
                resetImage()
            } else {
                resetImage();
            }

        })
    }
    React.useEffect(() => {
        if (!localImage) return;
        sendFileMessage();
    }, [localImage])
    React.useEffect(() => {
        return () => {
            setWhichPageChat(false);
        }
    }, [])
    const getThreadId = () => {
        socketRef.current.emit("check_user_interaction_list", userObj, otherUser.username, mainAppId, (res) => {
            console.log("User Chat", res);
            if (Array.isArray(res)) {
                // setThreadId(otherUser.thread_id)
                setMessageArray([...res.reverse()])
                if (!otherUser.thread_id) {
                    let find = recentChat.find(obj => { return obj.username === otherUser.username });

                    setThreadId(find.thread_id);
                } else {
                    setThreadId(otherUser.thread_id);
                }
            } else {
                getAllIntegrationList();
                setThreadId(res);
            }
        })
    }
    React.useEffect(() => {
        if (!socketRef.current) return
        socketRef.current.on('msg_notify', (data) => {
            if (data.thread_id === threadId) {
                if (messageArray[messageArray.length - 1] !== data) {
                    setMessageArray([...messageArray, data]);
                }
            }
        });
        return () => {
            if (socketRef.current) {
                socketRef.current.off('msg_notify');
            }
        };
    }, [threadId, messageArray]);


    React.useEffect(() => {
        if (otherUser === null) return;
        setWhichPageChat(true);
        getThreadId();
    }, [otherUser])

    const handleSignupWithOldData = (item) => {
        axios
            .post(`https://testchatsioapi.globalxchange.io/register_with_chatsio`, {
                first_name: item.first_name, // required
                last_name: item.last_name,
                username: item.username, // required
                bio: item.bio,
                email: item.email, // required
                timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                avatar: item.profile_img,
            })
            .then((res) => {
                if (res.data.status) {
                    registerUserToApp(item.email);
                } else {
                    if (res.data.payload === "Email Already Registered") {
                        registerUserToApp(item.email);
                    } else {
                        // antmsg.error("Registering User with Chats.io Failed");
                    }
                }
            });
    };

    const registerUserToApp = (email) => {
        axios.post(`https://testchatsioapi.globalxchange.io/register_user`, {
            email: email,
            app_id: mainAppId
        })
            .then(res => {
                if (res.data.status) {
                    // setLoading(false);
                    changeNotification({ status: true, message: "You Are Now Registered To This App" })
                    checkUserInApp()
                } else {
                    changeNotification({ status: false, message: "Failed To Register You To This App" })

                }
            })
    }

    const checkUserInApp = () => {
        axios
            .post(
                `https://testchatsioapi.globalxchange.io/get_user`,
                {
                    email: localStorage.getItem("userEmail"),
                    app_id: mainAppId,
                },
                {
                    headers: {
                        email: localStorage.getItem("userEmail"),
                        token: localStorage.getItem("idToken"),
                        app_id: mainAppId,
                    },
                }
            )
            .then((res) => {

                if (res.data === "Auth Failed") {
                    setSelections({ main: botHeader.id, sub: "refresh" });
                    setBotController({ ...Constant.refreshList });
                    setVisibleMoreInfo(false);
                    setRefreshApp(true); refresh(false);
                    return
                }
                console.log(res.data, "hahahah");
                if (!res.data.status) {
                    axios
                        .get(
                            `https://comms.globalxchange.com/user/details/get?email=${localStorage.getItem(
                                "userEmail"
                            )}`
                        )
                        .then((res) => {
                            // setGlobalUserObj(res.data.user);
                            if (res.data.user.profile_img !== "" &&
                                res.data.user.first_name !== "" &&
                                res.data.user.last_name !== "" &&
                                res.data.user.bio !== "") {
                                handleSignupWithOldData(res.data.user);
                                setGlobalUserObj(res.data.user);
                            } else {
                                setDifferentiator("setup")
                            }
                        });
                    // getDefaultAppApp();
                } else {
                    console.log("user info", res.data.payload)
                    setUserObj(res.data.payload);
                }
            });
    };
    React.useEffect(() => {
        checkUserInApp();
    }, [])

    return (
        <>
            {
                whichPageChat ?
                    <UserChatBox
                        getOlderMessages={getOlderMessages}
                        message={message}
                        setMessage={setMessage}
                        otherUser={otherUser}
                        messageArray={messageArray}
                        sendMessage={sendMessage}
                        userObj={userObj}
                        setMessageArray={setMessageArray}
                        setThreadId={setThreadId}
                        setOtherUser={setOtherUser}
                        loadMore={loadMore}
                    />
                    :
                    <ChatList
                        setOtherUser={setOtherUser}
                        recentChat={recentChat}
                        userList={userList}
                        loading={loading}
                    />
            }
        </>
    )
}