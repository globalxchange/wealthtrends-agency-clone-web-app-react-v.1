import React from 'react'
import NextSVG from '../../../../common-components/assetsSection/NextSVG';
import Images from '../../../../../assets/a-exporter';
import nextId from 'react-id-generator';
import moment from "moment-timezone";
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import './chat-list.style.scss'
export default function ChatList({ recentChat, setOtherUser, userList, loading }) {
    return (
        loading ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />
            </div>
            :
            !userList.length ?
                <div className="w-100 h-100 flex-column d-flex justify-content-center align-items-center">
                    <LoadingAnimation type="no-data"/>
                    <h6>There Is No One In You Friend List</h6>
                </div>
                :
                <div className="menu-home-main">
                    <div className="m-h-m-header">
                        <div>
                            <NextSVG type="search" color="#bbbbbb" />
                            <input placeholder="Search Contacts" />
                            <img src={Images.scanner} />
                        </div>

                    </div>
                    <div className="m-h-m-recent">
                        <h5>Recent</h5>
                        <div className="recent-chat-friends-wrapper">
                            {
                                !recentChat.length ?
                                    <div>
                                        <h6>No Chats</h6>
                                    </div>
                                    :
                                    recentChat.map(obj => <div
                                        onClick={() => setOtherUser(obj)}
                                         keyId={obj.id + nextId()}>
                                        <img src={!obj?.avatar ? Images.face : obj?.avatar} />
                                        <h6>{obj.username}</h6>
                                      {/* <p>{moment(obj.msg_timestamp).format('YYYY-MM-DD HH:MM')}</p> */}
                                        <p>{moment.utc(moment(obj.msg_timestamp).tz("Asia/Calcutta").format('YYYY-MM-DD HH:MM')).tz("Asia/Calcutta").startOf('minutes').fromNow()}</p>
                                    </div>)
                            }
                        </div>
                    </div>
                    <div className="m-h-m-friends">
                        <h5>Friends</h5>
                        <div className="friends-list-wrapper">
                            {
                                userList.map(obj => <div
                                    onClick={() => setOtherUser(obj)}
                                    className="single-friend-card">
                                    <img src={!obj?.avatar ? Images.face : obj?.avatar} />
                                    <div>
                                        <h6>{obj.first_name} {obj.last_name}</h6>
                                        <p>{obj.bio}</p>
                                    </div>
                                </div>
                                )
                            }

                        </div>

                    </div>
                </div>
    )
}
