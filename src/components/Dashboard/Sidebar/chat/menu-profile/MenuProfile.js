import React from 'react'
import UserProfile from '../user-profile/UserProfile'
import './menu-profile.style.scss'
export default function MenuProfile({profileType,setProfileType, agency }) {
    return (
        <div className="menu-profile">
            <UserProfile type={profileType} setType={setProfileType} user={agency.currentUserDetails} />
            
        </div>
    )
}
