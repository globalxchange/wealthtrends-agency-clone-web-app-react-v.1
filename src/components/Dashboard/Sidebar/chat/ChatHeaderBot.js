import React from "react";

export default function ChatHeaderBot({
  botController,
  botHeader,
  setWhichTrade,
  botSelections,
  handleClickOnOptions,
}) {
  return (
    <>
      <h6>{botController.title}</h6>
      <div
        className={
          botHeader.id === "learn"
            ? "bot-option-wrapper-learn"
            : "bot-option-wrapper"
        }
      >
        {botController.list.map((obj) => (
          <div
            key={obj.keyId}
            className={
              ["bot", "invest", "terminal"].includes(obj.id)
                ? "hide-it"
                : botSelections.sub === obj.id
                ? "selected-box"
                : ""
            }
            // onClick={() => botHeader.id === "learn" ? setContentStep(1) : handleBotSelection(obj.id)}
            onMouseEnter={() =>
              botSelections.sub === "trade"
                ? setWhichTrade(obj.id)
                : console.log("")
            }
            onMouseLeave={() =>
              botSelections.sub === "trade" ? setWhichTrade("") : console.log()
            }
            onClick={() => handleClickOnOptions(obj, botHeader.id)}
          >
            <span>
              <img
                onError={(e) => (e.target.src = obj.profile_pic)}
                src={obj.icon}
              />
            </span>
            <span>{obj.name}</span>
          </div>
        ))}
      </div>
    </>
  );
}
