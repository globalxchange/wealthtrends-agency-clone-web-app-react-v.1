import React, { useEffect, useState, useRef } from "react";
import { useHistory } from "react-router-dom";
import nextId from "react-id-generator";
import Images from "../../../../assets/a-exporter";
import jwt from "jsonwebtoken";
import ioClient from "socket.io-client";
import Axios from "axios";
import { Agency } from "../../../Context/Context";
import ChatBox from "./chat-box/ChatBox";
import "./chat.style.scss";
import Constant from "../../../../json/constant";
import SwitchComponent from "../../../common-components/switch/Switch";
import LearnMore from "./learn-more/LearnMore";
import Transaction from "./transaction/Transaction";
import { getLearnOptions } from "../../../../services/getAPIs";
import TransactionStep from "./transaction-step/TransactionStep";
import TransactAdd from "./transact-add/TransactAdd";
import { Modal, Upload, Icon, Button, Input, message as antmsg } from "antd";
import axios from "axios";
import S3 from "aws-s3";
import TransactInvest from "./transact-invest/TransactInvest";
import SetupChatProfile from "./setup-chat-profile/SetupChatProfile";
import MenuHome from "./menu-home/MenuHome";
import MenuBot from "./menu-bot/MenuBot";
import MenuSupport from "./menu-support/MenuSupport";
import NextSVG from "../../../common-components/assetsSection/NextSVG";
import MenuProfile from "./menu-profile/MenuProfile";
import MenuSetup from "./menu-setup/MenuSetup";
import OverlaySearch from "./menu-overlay/OverlaySearch";
import OverlayOther from "./menu-overlay/OverlayOther";
import OverlayCategory from "./menu-overlay/OverlayCategory";
import MenuCalculations from "./menu-calculations/MenuCalculations";
const { TextArea } = Input;
export default function Chat({ routeToFunds, refresh }) {
  const history = useHistory();
  let app_id = localStorage.getItem("appId");
  const agency = React.useContext(Agency);
  const [botController, setBotController] = React.useState(
    Constant.botTransact
  );
  const [botHeader, setBotHeader] = React.useState(controller[0]);
  const [showChatBox, setShowChatBox] = React.useState(false);
  const [categoryId, setCategoryId] = React.useState("");
  const [contentStep, setContentStep] = React.useState(10);
  const [displayOverlay, setDisplayOverlay] = React.useState(false);
  const [displayUserInfo, setDisplayUserInfo] = React.useState(false);
  const [transactAddCrypto, setTransactAddCrypto] = React.useState(false);
  const [investStep, setInvestStep] = React.useState(0);
  const [whichPageChat, setWhichPageChat] = React.useState(false);
  const [whichTrade, setWhichTrade] = React.useState("");
  const [botSelections, setSelections] = React.useState({
    main: "",
    sub: "",
  });
  const {
    currentApp,
    setMainTabs,
    setTransactionDeposit,
    transactionDeposit,
    setUserChatEnable,
    resetImage,
    setFundsConfig,
    mainAppId,
    fundsConfig,
    mainBalance,
    supportMessage,
    setSupportMessage,
    localImage,
    externalAction,
    imageResult,
    goToSettings,
    setPreImageUpload,
    setExternalAction,
    updatePortfolioRate,
    setAssetClassSelected,
    setGoToSettings,
    changeNotification,
    setRefreshApp,
    setMainBalance,
    setMainInterest,
    setTradeState,
    directCoinOverview,
    setAssetSelected,
    setSecondGate,
    getAllMarketCoins,
    setCollapseSidebar,
    setDifferentiator,
    differentiator,
    setFullScreenChat,
    fullScreenChat,
  } = agency;
  React.useEffect(() => {
    if (!updatePortfolioRate) return;
    setShowChatBox(true);
    setDisplayUserInfo(true);
    setDisplayOverlay(true);
    setContentStep(0);
  }, [updatePortfolioRate]);

  const [controlOptions, setControlOptions] = React.useState();

  const socketRef = useRef();
  const userSocketRef = useRef();
  const [stepNumber, setStepNumber] = React.useState(0);
  const [threadId, setThreadId] = useState("");
  const [chatSection, setChatSection] = useState(false);
  const [groupReadUnreadList, setGroupReadUnreadList] = useState([]);
  const [selectedGroup, setSelectedGroup] = useState("");
  const [typingMessage, setTypingMessage] = useState("");
  const [typingFlag, setTypingFlag] = useState(false);
  const [messageArray, setMessageArray] = useState([]);
  const [sendingImage, setSendingImage] = React.useState("");
  const [message, setMessage] = useState("");
  const [menuOverlay, setMenuOverlay] = React.useState(false);
  const [globalSearch, setGlobalSearch] = React.useState("other");
  const [globalSearchValue, setGlobalSearchValue] = React.useState("");
  const [messages, setMessages] = useState([]);

  const [userObj, setUserObj] = useState(null);
  const [visibleMoreInfo, setVisibleMoreInfo] = useState(false);
  const [loading, setLoading] = useState(false);
  const [bio, setBio] = useState("");
  const [globalUserObj, setGlobalUserObj] = useState(null);
  const [profileType, setProfileType] = React.useState("home");

  const [step, setStep] = React.useState(0);
  const [stepAddCrypto, setStepAddCrypto] = React.useState(0);

  const config = {
    bucketName: "chatsgx",
    dirName: localStorage.getItem("user_account"),
    region: "us-east-2",
    accessKeyId: "AKIAQD4QYXWFTC6JX6YM",
    secretAccessKey: "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",
  };
  const uploadButton = (
    <div>
      <Icon type={loading ? "loading" : "plus"} />
      <div className="ant-upload-text">Upload</div>
    </div>
  );
  const selectedDifferentiator = () => {
    switch (differentiator) {
      case "chat":
        return (
          <MenuHome
            setWhichPageChat={setWhichPageChat}
            whichPageChat={whichPageChat}
            setSelections={setSelections}
            botHeader={botHeader}
            setBotController={setBotController}
            setVisibleMoreInfo={setVisibleMoreInfo}
            refresh={refresh}
          />
        );
      case "bot":
        return (
          <MenuBot
            setWhichTrade={setWhichTrade}
            contentStep={contentStep}
            handleBotHeader={handleBotHeader}
            botHeader={botHeader}
            botController={botController}
            botHeader={botHeader}
            botSelections={botSelections}
            handleClickOnOptions={handleClickOnOptions}
            displayOverlay={displayOverlay}
            selectContent={selectContent}
            displayUserInfo={displayUserInfo}
            transactionDeposit={transactionDeposit}
            transactAddCrypto={transactAddCrypto}
            showChatBox={showChatBox}
          />
        );
      case "support":
        return (
          <MenuSupport
            setStepNumber={setStepNumber}
            stepNumber={stepNumber}
            ChatBox={ChatBox}
            botHeader={botHeader}
            setShowChatBox={setShowChatBox}
            displayUserInfo={displayUserInfo}
            showChatBox={showChatBox}
            visibleMoreInfo={visibleMoreInfo}
            setVisibleMoreInfo={setVisibleMoreInfo}
            handleNewSignup={handleNewSignup}
            setSelections={setSelections}
            setBotController={setBotController}
            refresh={refresh}
            botSelections={botSelections}
          />
        );
      case "profile":
        return (
          <MenuProfile
            profileType={profileType}
            setProfileType={setProfileType}
            agency={agency}
          />
        );
      case "setup":
        return (
          <MenuSetup
            handleNewSignup={handleNewSignup}
            setVisibleMoreInfo={setVisibleMoreInfo}
            visibleMoreInfo={visibleMoreInfo}
          />
        );
      case "calculations":
        return <MenuCalculations />;
    }
  };
  const handleMenuClick = (obj) => {
    switch (obj.id) {
      case "chat":
        setDifferentiator("chat");
        break;
      case "bot":
        setDifferentiator("bot");
        break;
      case "main":
        setMenuOverlay(!menuOverlay);
        break;
      case "call":
        return;
      case "call":
        return;
    }
  };

  const handleNewSignup = (obj) => {
    if (visibleMoreInfo) {
      axios
        .post(`https://comms.globalxchange.com/user/details/update`, {
          email: localStorage.getItem("userEmail"),
          token: localStorage.getItem("idToken"),
          first_name: obj.firstName,
          last_name: obj.lastName,
          bio: obj.bio,
          profile_img: obj.profile_img,
        })
        .then((res) => {
          // console.log(res.data, "updated global get user");
        });
    }

    axios
      .post(`https://testchatsioapi.globalxchange.io/register_with_chatsio`, {
        first_name: obj.firstName, // required
        last_name: obj.lastName,
        username: globalUserObj.username, // required
        bio: obj.bio,
        email: globalUserObj.email, // required
        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
        avatar: obj.profileImg,
      })
      .then((res) => {
        if (res.data.status) {
          // setVisibleMoreInfo(false);
          registerUserToApp(localStorage.getItem("userEmail"));
          changeNotification({
            status: true,
            message: "You Are Successfully Registered to Chats.io",
          });
        } else {
          // setVisibleMoreInfo(false);
          if (res.data.payload === "Email Already Registered") {
            registerUserToApp(localStorage.getItem("userEmail"), app_id);
            // changeNotification({ status: false, message: "Email Already Exist" })
          } else {
            // antmsg.error("Registering User with Chats.io Failed");
            changeNotification({
              status: false,
              message: "Sorry, Registering User With Chats.io Failed",
            });
          }
        }
      });
  };

  const registerUserToApp = (email) => {
    axios
      .post(`https://testchatsioapi.globalxchange.io/register_user`, {
        email: email,
        app_id: mainAppId,
      })
      .then((res) => {
        if (res.data.status) {
          // setLoading(false);
          setDifferentiator("chat");
          changeNotification({
            status: true,
            message: "You Are Now Registered To This App",
          });
          // checkUserInApp()
        } else {
          changeNotification({
            status: false,
            message: "Failed To Register You To This App",
          });
        }
      });
  };

  const handleExternalClick = () => {
    switch (externalAction) {
      case "invest":
        setContentStep(3);
        setSelections({ main: "transact", sub: "invest" });
        setDisplayOverlay(true);
        break;

      default:
        break;
    }
  };
  React.useEffect(() => {
    handleExternalClick();
  }, [externalAction]);

  const handleMainBack = () => {
    if (transactionDeposit) {
      if (step - 1 < 0) {
        setTransactionDeposit(false);
        setDisplayOverlay(false);
        return;
      }
      setStep(step - 1);
    } else if (transactAddCrypto) {
      if (stepAddCrypto - 1 < 0) {
        setTransactAddCrypto(false);
        setDisplayOverlay(false);
        setContentStep(0);
        return;
      }
      setStepAddCrypto(stepAddCrypto - 1);
    }
    if (whichPageChat) {
      setWhichPageChat(false);
    } else if (contentStep === 3) {
      if (investStep === 3) {
        setContentStep(10);
        setInvestStep(0);
        setDisplayOverlay(false);
        setSelections({ main: "", sub: "" });
        return;
      }
      if (investStep - 1 < 0) {
        setContentStep(0);
        setInvestStep(0);
        setDisplayOverlay(false);
        setSelections({ main: "", sub: "" });
        return;
      }
      setInvestStep(investStep - 1);
    } else if (displayUserInfo) {
      if (profileType !== "home") {
        setProfileType("home");
        return;
      }
      setDisplayUserInfo(false);
      setDisplayOverlay(false);
      setContentStep(10);
      setProfileType("home");
    }
  };

  React.useEffect(() => {
    setStepAddCrypto(0);
  }, [transactAddCrypto]);

  const handleBotSelection = (obj) => {
    switch (obj.id) {
      case "tDeposit":
        setTransactAddCrypto(true);
        setContentStep(5);
        setDisplayOverlay(true);
        setTransactionDeposit(false);

        break;
      case "refresh":
        setSelections({ main: botHeader.id, sub: obj.id });
        setBotController({ ...Constant.refreshList });
        break;

      case "rBalance":
        getAllMarketCoins();
        // setMainInterest();
        setMainBalance();
        setUserChatEnable(false);
        setCollapseSidebar(false);
        break;
      case "rPage":
        setRefreshApp(true);
        refresh(false);
        break;
      case "rApp":
        setRefreshApp(false);
        refresh(true);
        break;
      case "send":
        setSelections({ main: botHeader.id, sub: obj.id });
        setBotController({
          ...Constant.botTransactTwo,
          title: `Lets Get That ${obj.name} Started`,
        });
        break;
      case "trade":
        setSelections({ main: botHeader.id, sub: obj.id });
        setBotController({
          ...Constant.botTransactTrade,
          title: `Lets Get That ${obj.name} Started`,
        });
        break;
      case "Strade":
        setSelections({ main: botHeader.id, sub: obj.id });
        setBotController({
          ...Constant.botTransactTrade,
          title: `Lets Get That ${obj.name} Started`,
        });
        break;
      //currently working
      case "invest":
        setContentStep(3);
        setSelections({ main: botHeader.id, sub: obj.id });
        setDisplayOverlay(true);
        break;
      case "deposit":
        setTransactionDeposit(true);
        setDisplayOverlay(true);
        setContentStep(4);
        setTransactAddCrypto(false);
        break;
      case "quick":
        setMainTabs("accounts");
        setTradeState(true);
        setAssetSelected(Constant.navbarList[0]);
        setSecondGate(true);
        setCollapseSidebar(false);
        break;
      case "terminal":
        setCollapseSidebar(false);
        history.push("dashboard/terminals");
        break;
      case "Sinvest":
        setSelections({ main: botHeader.id, sub: obj.id });
        break;
      case "withdrawal":
        setContentStep(2);
        setDisplayOverlay(true);
        setSelections({ main: botHeader.id, sub: obj.id });
        break;
      case "redirect":
        handleRedirect();
        break;
      case "support":
        handleBySupport();
        break;
      case "platform":
        setContentStep(1);
        setDisplayOverlay(true);
        break;
    }
  };
  const handleClickOnOptions = (obj, id) => {
    if (fundsConfig.add !== null) {
      setAssetClassSelected(null);
    }
    if (id === "learn") {
      setSelections({ main: botHeader.id, sub: obj._id });
      setContentStep(1);
      setDisplayOverlay(true);
      setCategoryId(obj);
    } else {
      handleBotSelection(obj);
    }
  };

  const selectContent = () => {
    switch (contentStep) {
      case 1:
        return (
          <LearnMore
            setShowChatBox={setShowChatBox}
            setCategoryId={setCategoryId}
            categoryId={categoryId}
          />
        );
      case 2:
        return (
          <Transaction
            handleBySupport={(id) => handleBySupport(id)}
            type={botSelections.sub}
          />
        );
      case 3:
        return <TransactInvest step={investStep} setStep={setInvestStep} />;
      case 4:
        return (
          <TransactionStep
            setStep={setStep}
            step={step}
            handleMainBack={handleMainBack}
            botController={botController}
          />
        );

      case 5:
        return (
          <TransactAdd
            setStep={setStepAddCrypto}
            step={stepAddCrypto}
            handleMainBack={handleMainBack}
          />
        );
    }
  };

  const handleRedirect = () => {
    if (["add", "deposit", "trade", "invest"].includes(botSelections.sub)) {
      setUserChatEnable(false);
      setFundsConfig({
        add: true,
        crypto: true,
        coin: mainBalance.crypto[0],
        num: 0,
        action: true,
      });
    } else {
      setUserChatEnable(false);
      setMainTabs("accounts");
      setFundsConfig({
        add: false,
        crypto: true,
        coin: mainBalance.crypto[0],
        num: 0,
        action: true,
      });
    }
  };

  const handleBySupport = (id) => {
    // changeNotification({ status: false, message: "This Service Is Suspended For A While" })
    setSupportMessage(id);
    // let text = !id
    //   ? `I need to you to do a ${botSelections.sub.toUpperCase()}`
    //   : `Hey, I am having issues with transaction ID: ${id}`;
    // setShowChatBox(true);
    setContentStep(0);
    // setDisplayOverlay(true);
    // setMessage(text);
    // let a = setTimeout(() => {
    //   handleSubmitMessage(text, true);
    //   setBotController(
    //     (text = !id ? Constant.botTransact : Constant.botTransaction)
    //   );

    //   clearTimeout(a);
    // }, 150);
  };
  const handleOverlayClick = (obj) => {
    switch (obj.id) {
      case "support":
        setDifferentiator("support");
        break;
      case "profile":
        setDifferentiator("profile");
        break;
    }
    setMenuOverlay(false);
  };

  const handleDispute = () => {
    setShowChatBox(true);
    setContentStep(0);
    setDisplayOverlay(true);
    setMessage(`I need to you to do a ${botSelections.sub.toUpperCase()}`);
  };

  const handleBotHeader = async (obj) => {
    // if (botHeader.id === obj.id) return
    setSelections({ main: "", sub: "" });
    setBotHeader(obj);
    setContentStep(0);
    setDisplayOverlay(false);
    switch (obj.id) {
      case "transact":
        setTransactionDeposit(false);
        setStep(0);
        // setContentStep(0);
        setBotController(Constant.botTransact);
        setShowChatBox(false);
        break;
      case "transaction":
        setTransactAddCrypto(false);
        // setContentStep(0);
        setBotController(Constant.botTransaction);
        setShowChatBox(false);
        setTransactionDeposit(false);
        break;
      case "learn":
        // setContentStep(0);
        setTransactionDeposit(false);
        setTransactAddCrypto(false);
        setStep(0);
        setBotController({ title: "Loading.....", list: [] });
        let res = await getLearnOptions(currentApp.app_code);
        res = res.data.data.map((obj) => {
          return { ...obj, id: obj._id, icon: obj?.profile_pic };
        });
        let temp = {
          title: "What Do You Want To Learn About?",
          list: [...res],
        };
        // temp.list[0] = { ...temp.list[0], icon: currentApp.app_icon }
        setBotController(temp);
        break;
      default:
        break;
    }
  };
  const controlOverlay = () => {
    switch (globalSearch) {
      case "other":
        return <OverlayOther handleOverlayClick={handleOverlayClick} />;
      case "search":
        return <OverlaySearch />;
      case "category":
        return <OverlayCategory />;
    }
  };
  const handleSearch = (val) => {
    setGlobalSearchValue(val);
    if (val.length) {
      setMenuOverlay(true);
      setGlobalSearch("search");
    } else {
      setMenuOverlay(false);
      setGlobalSearch("other");
    }
  };

  useEffect(() => {
    if (goToSettings) {
      setDisplayOverlay(true);
      setContentStep(0);
      setDisplayUserInfo(true);
      setShowChatBox(true);
    }
  }, [goToSettings]);

  useEffect(() => {
    return () => {
      setGoToSettings(false);
    };
  }, []);

  useEffect(() => {
    return () => {
      setTransactionDeposit(false);
      setExternalAction("");
      setDifferentiator("bot");
    };
  }, []);
  React.useEffect(() => {
    if (directCoinOverview.status) {
      setStepAddCrypto(1);
    } else {
      setStepAddCrypto(0);
    }
  }, [transactAddCrypto]);

  useEffect(() => {
    if (!directCoinOverview.status) return;
    setDifferentiator("bot");
    setTransactAddCrypto(true);
    setContentStep(5);
    setDisplayOverlay(true);
    setStepAddCrypto(1);
    setTransactionDeposit(false);
    return () => {};
  }, [directCoinOverview.status]);

  return (
    <>
      {
        <>
          <div className="user-chat-main">
            <div className="user-chat-header">
              <div className="uc-header-logo">
                <button
                  onClick={() => handleMainBack()}
                  className={
                    transactionDeposit ||
                    contentStep === 3 ||
                    transactAddCrypto ||
                    whichPageChat
                      ? ""
                      : "disabled-it"
                  }
                >
                  <NextSVG type="chatBack" height="20px" width="20px" />{" "}
                </button>
                <h3>
                  <img src={currentApp.app_icon} />
                  {currentApp.app_name}
                </h3>
                <button onClick={() => setFullScreenChat(!fullScreenChat)}>
                  <NextSVG type="chatFs" height="20px" width="20px" />{" "}
                </button>
              </div>
            </div>
            <div className="user-chat-search">
              <button
                onClick={() => {
                  if (!menuOverlay) {
                    setMenuOverlay(true);
                    setGlobalSearch("category");
                  } else {
                    if (!globalSearchValue) {
                      setMenuOverlay(false);
                      setGlobalSearch("other");
                    } else {
                      setGlobalSearch("other");
                      setMenuOverlay(false);
                    }
                  }
                }}
                className={
                  menuOverlay && globalSearch === "category"
                    ? "selected-button"
                    : ""
                }
              >
                All
              </button>
              <div>
                <input
                  onChange={(e) => handleSearch(e.target.value)}
                  placeholder="Search Anything"
                />
                <NextSVG type="search" />
              </div>
            </div>
            <div className="user-chat-body">
              <div className="chat-body-differentiator">
                <div
                  className={`menu-overlay-wrapper ${
                    menuOverlay ? "curtains-down" : ""
                  }`}
                >
                  {controlOverlay()}
                </div>
                {selectedDifferentiator()}
              </div>
              <div className="chat-menu-bar">
                {menuList.map((obj, num) => (
                  <button
                    className={differentiator === obj.id ? "selected-menu" : ""}
                    key={obj.keyId}
                    onClick={() => handleMenuClick(obj)}
                  >
                    <NextSVG
                      type={obj.icon}
                      height={num === 2 ? "50%" : "20px"}
                      width={num === 2 ? "50%" : "20px"}
                    />
                    <p>{obj.name}</p>
                  </button>
                ))}
              </div>
            </div>
          </div>
        </>
      }
    </>
  );
}

const controller = [
  {
    keyId: nextId(),
    name: "Actions",
    id: "transact",
    icon: Images.actionButton,
  },
  {
    keyId: nextId(),
    name: "Transactions",
    id: "transaction",
    icon: Images.transactionButton,
  },
  { keyId: nextId(), name: "Learn", id: "learn", icon: Images.learnButton },
];
const menuList = [
  { keyId: nextId(), name: "Chats", id: "chat", icon: "menuChat" },
  { keyId: nextId(), name: "Bot", id: "bot", icon: "menuBot" },
  { keyId: nextId(), name: "Menu", id: "main", icon: "chat" },
  { keyId: nextId(), name: "Calls", id: "call", icon: "menuCall" },
  { keyId: nextId(), name: "Email", id: "email", icon: "menuEmail" },
];

const overlayList = [
  { keyId: nextId(), text: "Speak To Support", id: "support", disable: false },
  { keyId: nextId(), text: "Manage Contacts", id: "contacts", disable: true },
  { keyId: nextId(), text: "Schedule A Meeting", id: "meeting", disable: true },
  {
    keyId: nextId(),
    text: "Chats.io MarketPlace",
    id: "marketPlace",
    disable: true,
  },
  { keyId: nextId(), text: "Edit Profile", id: "profile", disable: false },
];
