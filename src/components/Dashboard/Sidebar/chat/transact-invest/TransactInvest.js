import React from 'react'
import Constant from '../../../../../json/constant';
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import { getAppOverview } from '../../../../../services/getAPIs';
import { Agency } from '../../../../Context/Context';
import './transact-invest.style.scss'
import TransactInvestCalculations from './TransactInvestCalculations';
import TransactInvestCoin from './TransactInvestCoin';
import TransactInvestFinal from './TransactInvestFinal';
import TransactInvestSelect from './TransactInvestSelect';
export default function TransactInvest({ step, setStep }) {
    const agency = React.useContext(Agency);
    const { investmentPath, setInvestmentPath, mainBalance } = agency;
    const [selectedCoin, setSelectedCoin] = React.useState();
    const [currentSteps, setCurrentSteps] = React.useState([]);
    const [activeStep, setActiveStep] = React.useState(0);
    const [loading, setLoading] = React.useState(true)
    const [coinArray, setCoinArray] = React.useState([])
    const [selectedApp, setSelectedApp] = React.useState()
    const [ownershipCoin, setOwnershipCoin] = React.useState('')
    const [details, setDetails] = React.useState({

    })
    const [previouslySelectedId, setPreviouslySelectedId] = React.useState("invest");

    const setUpCurrentStep = (first, second) => {
        let tempArray = [
            { ...first },
            { ...second }
        ]
        setCurrentSteps([...tempArray]);
    }
    const handleSteps = (obj) => {
        setPreviouslySelectedId(obj.id)
        switch (activeStep) {
            case 0: setDetails({ ...details, asset: obj.name }); setActiveStep(1); selectCoins(obj.name); break;
            case 1: setSelectedCoin(obj); setDetails({ ...details, coin: obj.name }); setStep(1);
            // case 2: selectCoins(details.asset); setUpTransactionList(obj.name); break;
        }
    }
    const handleAppClick = (obj) => {
        setSelectedApp(obj);
        setStep(2)
    }
    const handleBackSteps = () => {
        setUpCurrentStep(Constant.botTransact, Constant.transactionAssets);
        setActiveStep(0);

    }
    React.useEffect(() => {
    }, [selectedCoin])

    const selectCoins = (coin) => {
        let tempArray = []
        if (coin === "Fiat") {
            setCoinArray([...mainBalance.fiat])
            tempArray = mainBalance.fiat.map(obj => { return { ...obj, disabled: obj.coinSymbol !== ownershipCoin || !ownershipCoin, name: obj.coinSymbol, icon: obj.coinImage } })
        } else {
            setCoinArray([...mainBalance.crypto])
            tempArray = mainBalance.crypto.map(obj => { return { ...obj, disabled: obj.coinSymbol !== ownershipCoin || !ownershipCoin, name: obj.coinSymbol, icon: obj.coinImage } })
        }
        setUpCurrentStep(Constant.transactionAssets, { ...Constant.transactionCurrency, list: [...tempArray] })
    }
    const selectComponent = () => {
        switch (step) {
            case 0: return <TransactInvestSelect investmentPath={investmentPath} activeStep={activeStep} handleBackSteps={handleBackSteps} previouslySelectedId={previouslySelectedId} handleSteps={handleSteps} currentSteps={currentSteps} />;
            case 1: return <TransactInvestCoin handleAppClick={handleAppClick} coin={selectedCoin} coinArray={coinArray} />
            case 2: return <TransactInvestCalculations setStep={setStep} selectedApp={selectedApp} coin={selectedCoin} />
            case 3: return <TransactInvestFinal setStep={setStep} />
            case 4: return <TransactInvestFinal failed={true} setStep={setStep} />

        }
    }

    const findOwnershipCoin = async () => {
        let res = await getAppOverview(localStorage.getItem('appCode'))
        setOwnershipCoin(res.data?.apps[0]?.ownership_coin);
        setLoading(false)
    }
    React.useEffect(() => {
        findOwnershipCoin()
        setUpCurrentStep(Constant.botTransact, Constant.transactionAssets);
        return () => {
            setInvestmentPath(null)
        }
    }, [])
    React.useEffect(() => {
        if (step === 0) {
            setActiveStep(0);
            setPreviouslySelectedId("invest")
            setUpCurrentStep(Constant.botTransact, Constant.transactionAssets)
            setDetails({
                type: "invest",
                asset: null,
                coin: null

            });

        }
    }, [step])
    return (
        <div className="transact-invest-home">
            {
                loading ?
                    <div className="d-flex justify-content-center align-items-center w-100 h-100">
                        <LoadingAnimation />

                    </div>
                    :
                    selectComponent()
            }
        </div>
    )
}
