import React from 'react'
import nextId from 'react-id-generator'
import InvestEquity from './InvestEquity'

export default function TransactInvestCalculations({ setStep,selectedApp,coin }) {
    return (
        <div className="transact-invest-calculations">
            <div className="tical-header">
                <div>
                    <img src={selectedApp.app_icon} />
                </div>
                <div>
                    <h6>{selectedApp.app_name}</h6>
                    <span>Exchange Tokens</span>
                </div>

            </div>
            <div className="tical-body">
                <div className="tical-tab-header">
                    {
                        tabs.map(obj => <button key={obj.keyId}>{obj.name}</button>)
                    }

                </div>
                <div className="tical-tab-body">
                    <InvestEquity setStep={setStep} coin={coin} selectedApp={selectedApp} />

                </div>

            </div>

        </div>
    )
}
const tabs = [
    { keyId: nextId(), name: "Equity" },
    { keyId: nextId(), name: "Tokens" },
    { keyId: nextId(), name: "Bonds" },
]