import React from 'react'
import { Agency } from '../../../../Context/Context';

export default function TransactInvestFinal({ setStep, failed }) {
    const agency = React.useContext(Agency)
    const {investmentPath} = agency
    return (
        <div className="transact-invest-final">
            <h5>{failed ? "Failed" : "Success"}</h5>
            {/* <p>{failed?`You Have Failed Invested In ${investmentPath?.banker} in ${investmentPath?.country}. Check Your My Investments Page For All The Details`:`You Have Successfully Invested In ${investmentPath?.banker} in ${investmentPath?.country}. Check Your My Investments Page For All The Details`}</p>
            <button onClick={() => setStep(0)}>Go To My Investments</button> */}
        </div>
    )
}
