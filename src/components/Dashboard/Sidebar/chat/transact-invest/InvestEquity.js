import React from 'react'
import nextId from 'react-id-generator'
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import { moreAboutApp } from '../../../../../services/getAPIs'
import { executeInvestmentPaths, fundAppOwnershipCapital, getOfferingEquity } from '../../../../../services/postAPIs';
import { Agency } from '../../../../Context/Context';

export default function InvestEquity({ selectedApp, setStep, coin }) {
    const agency = React.useContext(Agency)
    const { valueFormatterInput, changeNotification, investmentPath, investmentPathID, valueFormatter } = agency
    const [loading, setLoading] = React.useState(true);
    const [importantData, setImportantData] = React.useState({})
    const [nativeValue, setNativeValue] = React.useState('');
    const [usdValue, setUsdValue] = React.useState('')
    const getAllData = async () => {
        let tempObj = {}
        let res = await moreAboutApp(selectedApp.app_code);

        let resTwo = await getOfferingEquity(selectedApp)
        let coinData = resTwo.data.coins_data.find(obj => { return obj.coinSymbol === coin.coinSymbol })
        tempObj = {
            total_allocation: res.data.apps?.[0]?.ownership_capital_required,
            secured_amount: res.data.apps?.[0]?.ownership_capital_balance,
            secured_percentage: 0,
            mid_value: coinData.coinValue,
            rate: coinData.price["USD"]
        }
        setImportantData({ ...tempObj })
        setLoading(false)
    }
    const findValue = (type) => {
        switch (type.id) {
            case "allocation": return coin.coinSymbol
            case "sa": return valueFormatter(importantData.secured_amount, coin.coinSymbol)
            case "sp": return parseFloat(importantData.secured_percentage).toFixed(2) + '%'
            case "rate": return valueFormatter(importantData.rate, coin.coinSymbol)
            case "pc": return valueFormatter(!nativeValue ? 0 : nativeValue, coin.coinSymbol)
            case "pp": return parseFloat((nativeValue / importantData.total_allocation) * 100).toFixed(2) + "%"
        }
    }
    const handleChange = (e, which) => {
        if (which) {
            setUsdValue(e.target.value);
            setNativeValue(valueFormatterInput((e.target.value / importantData.rate), coin.coinSymbol));
        } else {
            setNativeValue(e.target.value);
            setUsdValue(valueFormatterInput(e.target.value * importantData.rate), "USD");
        }

    }
    const handleSubmit = async () => {
        setLoading(true);
        if (!investmentPath) {
            let temp = {
                email: localStorage.getItem("userEmail"),
                token: localStorage.getItem("idToken"),
                user_app_code: localStorage.getItem("appCode"),
                to_app_code: selectedApp.app_code,
                amount: nativeValue
            }
            let res = await fundAppOwnershipCapital(selectedApp.app_code,nativeValue );
            if (res.data.status) {
                changeNotification({ status: true, message: "Successful" })
                setStep(3)
            } else {
                changeNotification({ status: false, message: "Failed" })
                setStep(4)
            }
        } else {
            let temp = {
                email: localStorage.getItem("userEmail"),
                token: localStorage.getItem("idToken"),
                app_code: selectedApp.app_code,
                profile_id: selectedApp.profile_id,
                path_id: investmentPath.path_id,
                user_pay_coin: coin.coinSymbol,
                tokens_amount: null,
                pay_amount: nativeValue,
                stats: false
            }
            let res = await executeInvestmentPaths({ ...temp });
            if (res.data.status) {
                changeNotification({ status: true, message: "Successful" })
                setStep(3)
            } else {
                changeNotification({ status: false, message: "Failed" })
                setStep(4)
            }
        }
        setLoading(false);
    }
    React.useEffect(() => {
        getAllData()
    }, [coin])
    return (
        loading ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />
            </div>
            :
            <div className="invest-equity">
                <div className="ie-header">
                    <h4>Class A</h4>
                    <p>Own A Piece Of The Total Earning Power Of {selectedApp.app_name} By Investing Into The Class A Reserve</p>
                </div>
                <div className="ie-total-allocation">
                    <div className="allocation-header">
                        <h4>{valueFormatter(importantData.total_allocation, coin.coinSymbol)}</h4>
                        <span>Total Allocation</span>
                    </div>
                    <div className="allocation-body">
                        {
                            forAllocation.map(obj => <h6 key={obj.keyId}>
                                <span>{obj.name}</span>
                                <span>{findValue(obj)}</span>
                            </h6>)
                        }
                    </div>
                </div>
                <h6 className="native-value">
                    <span>{selectedApp?.app_name} {coin?.coinSymbol} {coin?.coinName}</span>
                    <span>{importantData.mid_value} {coin?.coinSymbol}</span>
                </h6>
                <div className="ie-calculations">
                    <div className="ie-input-wrapper">
                        <div>
                            <input value={usdValue} onChange={(e) => handleChange(e, true)} placeholder="$0.00" />
                            <span>US Dollars</span>
                        </div>
                        <div>
                            <input value={nativeValue} onChange={(e) => handleChange(e, false)} placeholder={`${valueFormatter(0, coin.coinSymbol)}`} />
                            <span>{coin.coinName}</span>
                        </div>
                    </div>
                    <div className="calculation-body">
                        {
                            forCalculator.map((obj, num) => <h6 key={obj.keyId}>
                                <span>{!num ? `Rate ${coin.coinSymbol}/USD` : obj.name}</span>
                                <span>{findValue(obj)}</span>
                            </h6>)
                        }
                    </div>
                </div>
                <button onClick={() => handleSubmit()}>Invest</button>
            </div>
    )
}
const forAllocation = [
    { keyId: nextId(), name: "Allocation Currency", value: "Bitcoin", id: "allocation" },
    { keyId: nextId(), name: "Secured Amount", value: "0.0162 BTC", id: "sa" },
    { keyId: nextId(), name: "Secured Percentage", value: "0.00%", id: "sp" },
]
const forCalculator = [
    { keyId: nextId(), name: "Rate (BTC/USD)", value: "Bitcoin", id: "rate" },
    { keyId: nextId(), name: "Post Conversion", value: "0.0162 BTC", id: "pc" },
    { keyId: nextId(), name: "Purchasing Power", value: "0.00%", id: "pp" },
]