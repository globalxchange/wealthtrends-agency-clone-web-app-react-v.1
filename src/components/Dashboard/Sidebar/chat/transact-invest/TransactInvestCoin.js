import React from 'react'
import nextId from 'react-id-generator';
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import { fetchValuesOfCoin, getRegisteredApp, getDataForInvestmentPath } from '../../../../../services/getAPIs';
import { Agency } from '../../../../Context/Context';

export default function TransactInvestCoin({ coinArray, handleAppClick, coin }) {
    const agency = React.useContext(Agency);
    const { valueFormatter, allApps, investmentPath } = agency;
    const [loading, setLoading] = React.useState(true);
    const [valueList, setValueList] = React.useState([]);
    const [localCoinsArray, setLocalCoinsArray] = React.useState([]);

    const setUpLocalArray = () => {
        let tempArray = coinArray.filter(obj => { return obj.coinSymbol !== coin.coinSymbol })
        setLocalCoinsArray([coin, ...tempArray])
        setLoading(false);

    }
    const setUpCoinValues = async () => {
        if (!investmentPath?.path_id) {
            let res = await getRegisteredApp();
            setValueList([...res.data.userApps]);
            setUpLocalArray();

        } else {
            let res = await getDataForInvestmentPath(investmentPath.path_id);
            let temp = res.data.paths[0].app_codes.map(obj => {
                if (!allApps.find(x => { return x.app_code === obj })) {
                    return
                } else {
                    let tempApp = allApps.find(x => { return x.app_code === obj })
                    return {
                        keyId: nextId(),
                        app_icon: tempApp.app_icon,
                        app_code: obj,
                        app_name: tempApp.app_code,
                    }
                }
                // let findApp = !allApps.find(x => { return x.app_code === obj }) ? "" : allApps.find(x => { return x.app_code === app }).app_icon
            })
            console.log("Actual list", [...temp])
            setValueList([...temp])
            setUpLocalArray()

        }

    }
    React.useEffect(() => {
        setUpCoinValues();
    }, [coin])
    return (
        loading ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />
            </div>
            :
            <div className="transact-invest-coin">
                <div className="tic-header">
                    <div className="coins-wrapper">
                        {
                            localCoinsArray.map(obj => <div key={obj._id + nextId()} className={obj.coinSymbol === coin.coinSymbol ? "selected-coin" : ""}>
                                <img src={obj.coinImage} />
                                <span>{obj.coinSymbol}</span>
                            </div>
                            )
                        }
                    </div>
                </div>
                <div className="tic-select-coin">
                    <p>Which One Of Your {coin?.coinName} Balances Do You Want To Use?</p>
                    <div className="list-of-coins">
                        {
                            !valueList.length ?
                                <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                                    <LoadingAnimation type="no-data" />
                                </div>
                                :
                                valueList.map(obj =>
                                    <div key={obj.keyId} onClick={() => handleAppClick(obj)}>
                                        <span><img src={obj.app_icon} />{obj.app_name ? obj.app_name.toUpperCase() : ""}</span>
                                        {/* <span>{valueFormatter(obj.value, coin.coinSymbol)} {coin.coinSymbol}</span> */}
                                    </div>
                                )
                        }
                    </div>

                </div>

            </div>
    )
}
