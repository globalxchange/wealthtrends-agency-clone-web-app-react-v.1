import React from 'react'

export default function TransactInvestSelect({ handleSteps, investmentPath, activeStep, handleBackSteps, previouslySelectedId, currentSteps }) {
    return (
        <div className="transact-invest-select">
            {
                currentSteps.map((obj, i) => <div key={obj.keyId} className={`option-wrapper ${i && activeStep ? "increase-height" : ""}`}>
                    <h6>{obj.title}</h6>
                    <div className="options-row">
                        {
                            obj.list.map(x =>
                                <div
                                    key={obj.keyId}
                                    style={x.disabled && !investmentPath ? { opacity: 0.25, pointerEvents: "none" } : {}}
                                    onClick={() => i ? handleSteps(x) : handleBackSteps()}
                                    className={i ? ["completed"].includes(x.id) ? "disable" : "" : previouslySelectedId === x.id ? "selected" : "disable"}>
                                    <img src={x.icon} />
                                    <span>{x.name}</span>
                                </div>
                            )
                        }
                    </div>
                </div>
                )
            }

        </div>
    )
}
