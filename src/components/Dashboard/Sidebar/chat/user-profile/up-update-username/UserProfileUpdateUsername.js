import React from 'react'
import { getListOfUsernames } from '../../../../../../services/getAPIs';
import CryptoJS from 'crypto-js'
import LoadingAnimation from '../../../../../../lotties/LoadingAnimation';
import { updateUsername } from '../../../../../../services/postAPIs';
const key = "gmBuuQ6Er8XqYBd"
export default function UserProfileUpdateUsername({ user, setUserDetails,setType, changeNotification }) {
    const [step, setStep] = React.useState(0);
    const [available, setAvailable] = React.useState(false)
    const [showMessage, setShowMessage] = React.useState(false);
    const [error, setError] = React.useState(false);
    const [userNames, setUserNames] = React.useState([])
    const [username, setUsername] = React.useState('');
    const [mainLoading, setMainLoading] = React.useState(true);
    const smallCaseRegex = /^[a-zA-Z0-9_\-]+$/gm

    const handleChange = (e) => {
        if (showMessage) setShowMessage(false)
        setUsername(e.target.value);
    }
    const checkAvailability = async () => {
        let res = await getListOfUsernames();
        let data = res.data.payload;

        let bytes = CryptoJS.Rabbit.decrypt(data, key);
        let jsonString = bytes.toString(CryptoJS.enc.Utf8);
        let result = JSON.parse(jsonString);
        console.log("usernames", result);
        setUserNames([...result.usernames]);
        setMainLoading(false)
    }
    const handleClick = () => {
        if (userNames.includes(username) || !smallCaseRegex.test(username)) {
            setAvailable(false);
            setShowMessage(true);
        } else {
            setAvailable(true)
            setShowMessage(false)
        }
    }
    const handleSubmit = async () => {
        setMainLoading(true);
        let res = await updateUsername(username);
        if (res.data.status) {
            setStep(1);
            changeNotification({ status: true, message: "Your Username is updated..." })
            setUserDetails();
            setMainLoading(false)
        } else {
            setMainLoading(false)
            changeNotification({ status: false, message: "Sorry We Failed To Update Your Username... Please Try Again.." })
        }

    }

    React.useEffect(() => {
        checkAvailability();
    }, [])

    const changeUsernameSteps = () => {
        switch (step) {
            case 0: return <div className="username-step-one">
                <h4>Update Username</h4>
                <p className={showMessage ? "" : "d-none"}>
                    Sorry @{username} Is Already Taken. Please Try Something Else
                </p>
                {
                    available ?
                        <p className="congratulations">
                            Congratulations @{username} Is Available. Please Confirm The Change
                    </p>
                        :
                        <div>
                            <span>@</span>
                            <input value={username} onChange={handleChange} />
                        </div>
                }
                <h6>
                    {
                        available ?
                            <>
                                <h6>
                                    <button onClick={() => { setAvailable(false); setShowMessage(false) }}>Back</button>
                                    <button onClick={() => handleSubmit()}>Confirm</button>
                                </h6>
                            </>
                            :
                            <button disabled={!username} onClick={() => handleClick()}>
                                Check Availability
                            </button>
                    }

                </h6>

            </div>
            case 1: return <div className="username-step-two">
                <h4>Congratulations</h4>
                <p>Your Username Has Been Changed To {username}</p>
                <button onClick={() => setType("home")}>
                    Back To Profile
            </button>

            </div>

        }
    }

    return (
        <div className="user-profile-username">
            {
                mainLoading ?
                    <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                        <LoadingAnimation />
                    </div>
                    :
                    changeUsernameSteps()
            }

        </div>
    )
}
