import React from 'react'
import Images from '../../../../../assets/a-exporter'
import { Agency } from '../../../../Context/Context'
import UpdateBio from './up-bio/UpdateBio'
import UserProfileHome from './up-home/UserProfileHome'
import UserUpdateInterest from './up-interest/UserUpdateInterest'
import UsernameUpdatePassword from './up-password/UsernameUpdatePassword'
import UpdatePortfolio from './up-portfolio/UpdatePortfolio'
import UserProfileUpdateUsername from './up-update-username/UserProfileUpdateUsername'
import './user-profile.style.scss'
const UserProfile = React.memo(({ user, type, setType }) => {

    const agency = React.useContext(Agency);
    const { changeNotification, setUserDetails, updatePortfolioRate } = agency



    const selectComponent = React.useCallback(() => {
        switch (type) {
            case "home": return <UserProfileHome
                setType={setType}
                user={user}
            />
            case "username": return <UserProfileUpdateUsername
                setUserDetails={setUserDetails}
                setType={setType}
                changeNotification={changeNotification}
                user={user}
            />
            case "password": return <UsernameUpdatePassword
                changeNotification={changeNotification}
                setType={setType}
            />
            case "interest": return <UserUpdateInterest setType={setType} />
            case "portfolio": return <UpdatePortfolio />;
            case "bio": return <UpdateBio setType={setType}/>
        }

    }, [type])


    return (
        <div className="chat-user-profile">
            {selectComponent()}
        </div>
    )
})
export default UserProfile;