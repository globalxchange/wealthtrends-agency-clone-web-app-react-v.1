import React from 'react'
import './update-bio.style.scss'
import { Agency } from '../../../../../Context/Context';
import Images from '../../../../../../assets/a-exporter';
import LoadingAnimation from '../../../../../../lotties/LoadingAnimation';
import { updateUserBio } from '../../../../../../services/postAPIs';
export default function UpdateBio({ setType }) {
    const agency = React.useContext(Agency);
    const [uploading, setUploading] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [userData, setUserData] = React.useState({
        first_name: '',
        last_name: '',
        profile_img: '',
        bio: '',
        name: ''
    })
    const { currentUserDetails, setUserDetails, changeNotification, globalHandleFinalImage, localImage, resetImage } = agency;
    React.useState(() => {
        if (!currentUserDetails) return;
        setUserData({
            first_name: currentUserDetails.first_name,
            last_name: currentUserDetails.last_name,
            profile_img: currentUserDetails.profile_img,
            bio: currentUserDetails.bio,
            name: currentUserDetails.name
        });
    }, [currentUserDetails]);

    const handleSubmit = async () => {
        setLoading(true)
        let res = await updateUserBio(userData);
        if (res.data.status) {
            changeNotification({ status: true, message: "Bio Updated" });
            setUserDetails();
            setType("home")
        } else {
            changeNotification({ status: true, message: "Something Went Wrong. Please Try Again" })
        }
        setLoading(false);

    }

    React.useEffect(() => {
        if (!localImage) return;
        setUserData({ ...userData, profile_img: localImage });
        resetImage();
        setUploading(false);
    }, [localImage])

    return (
        loading ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />
            </div>
            :
            <div className="update-bio-main">
                <div className="update-bio-main-form">
                    <h5>Update Bio</h5>
                    <div>
                        <label htmlFor="update-profile">
                            <input id="update-profile" onChange={(e) => { setUploading(true); globalHandleFinalImage(e) }} type="file" />
                            {uploading ? <LoadingAnimation size={{ height: "8vh", width: "8vh" }} /> : <img src={!userData.profile_img ? Images.face : userData.profile_img} />}
                            <p>Change Profile Picture</p>
                        </label>
                    </div>
                    <div>
                        <p>First Name</p>
                        <input onChange={e => setUserData({ ...userData, first_name: e.target.value })} autoFocus value={userData.first_name} />
                    </div>
                    <div>
                        <p>Last Name</p>
                        <input onChange={e => setUserData({ ...userData, last_name: e.target.value })} value={userData.last_name} />
                    </div>
                    <div>
                        <p>Display Name</p>
                        <input onChange={e => setUserData({ ...userData, name: e.target.value })} value={userData.name} />
                    </div>
                    <div>
                        <p>Bio</p>
                        <textarea onChange={e => setUserData({ ...userData, bio: e.target.value })} value={userData.bio} />
                    </div>
                </div>
                <div className="update-bio-mai-footer">
                    <button onClick={() => setType("home")}>Go Back</button>
                    <button onClick={handleSubmit} disabled={!userData.first_name || !userData.last_name || !userData.profile_img || !userData.bio} >
                        Save
                    </button>
                </div>

            </div>
    )
}
