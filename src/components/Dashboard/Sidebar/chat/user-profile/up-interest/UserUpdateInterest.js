import React from 'react'
import UpdateInterestStepOne from './UpdateInterestStepOne'
import UpdateInterestStepThree from './UpdateInterestStepThree'
import UpdateInterestStepTwo from './UpdateInterestStepTwo'
import LoadingAnimation from '../../../../../../lotties/LoadingAnimation';
import './user-update-interest.style.scss'
import { updateDestinations } from '../../../../../../services/postAPIs';
import { Agency } from '../../../../../Context/Context';
export default function UserUpdateInterest({ setType }) {
    const agency = React.useContext(Agency);
    const { changeNotification } = agency
    const [uiStep, setUiStep] = React.useState(0)
    const [details, setDetails] = React.useState({})
    const [loading, setLoading] = React.useState(false)
    const [destinations, setDestinations] = React.useState();
    const selectComponent = () => {
        switch (uiStep) {
            case 0: return <UpdateInterestStepOne details={details} destinations={destinations} setDestinations={setDestinations} handleSubmit={handleSubmit} setDetails={setDetails} setUiStep={setUiStep} setType={setType} />;
            case 1: return <UpdateInterestStepTwo details={details} setDetails={setDetails} setUiStep={setUiStep} />
            case 2: return <UpdateInterestStepThree handleSubmit={handleSubmit} setUiStep={setUiStep} details={details} setDetails={setDetails} />
        }
    }
    const handleSubmit = async (first, firsObj) => {
        setLoading(true);
        let token = localStorage.getItem('idToken');
        let email = localStorage.getItem('userEmail');
        let obj 
        if(first){
            obj = {
                token: token,
                email: email,
                coin: "USD",
                icedData: {
                    payoutFrequency: firsObj.frequency.id,
                    payoutDestination: firsObj.destination.id,
                    payoutDestination_data: {
                        app_code: firsObj.app.id
                    }
                },
                liquidData: {
                    payoutFrequency: firsObj.frequency.id,
                    payoutDestination: firsObj.destination.id,
                    payoutDestination_data: {
                        app_code: firsObj.app.id
                    }
                }
            }
        }
        else if(details.type.id === "liquid"){
            obj = {
                token: token,
                email: email,
                coin: details.coin.id,
                icedData: {
                    ...destinations.icedData
                },
                liquidData: {
                    payoutFrequency: details.frequency.id,
                    payoutDestination: details.destination.id,
                    payoutDestination_data: {
                        app_code: details.app.id
                    }
                }
            }

        }else{
            obj = {
                token: token,
                email: email,
                coin: details.coin.id,
                liquidData: {
                    ...destinations.liquidData
                },
                icedData: {
                    payoutFrequency: details.frequency.id,
                    payoutDestination: details.destination.id,
                    payoutDestination_data: {
                        app_code: details.app.id
                    }
                }
            }

        }
        let res = await updateDestinations(obj);
        if (res.data.status) {
            changeNotification({ status: true, message: "Destination Updated Successfully" })

        } else {
            changeNotification({ status: false, message: "Something Went Wrong. Please Try Again" })
        }
        setUiStep(0);
        setLoading(false)

    }
    return (
        loading ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />
            </div>
            :

            <div className="user-update-interest w-100 h-100">
                {
                    selectComponent()
                }
            </div>
    )
}
