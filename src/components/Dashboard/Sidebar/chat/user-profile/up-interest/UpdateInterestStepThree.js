import React from 'react'

export default function UpdateInterestStepThree({ setDetails, handleSubmit, details, setUiStep }) {
    return (
        <div className="update-interest-step-three">
            <div className="ui-three-header">
                <h3>Recap</h3>
                <p>Select The Information You Want To Update</p>

            </div>
            <div className="ui-three-body">
                <div className="ui-three-body__header">
                    <img src={details.type.icon} />
                    <h5>{details.type.id === "liquid" ? "MoneyMarket Earnings" : "Bond Earnings"}</h5>

                </div>
                <div className="ui-three-body__body">
                    <div>
                        <span>Frequency</span>
                        <span>{details?.frequency?.title}</span>
                    </div>
                    <div>
                        <span>Destination</span>
                        <span><img src={details?.app.icon} />{details?.app?.title}</span>
                    </div>

                </div>


            </div>
            <div className="ui-three-footer">
                <button onClick={() => { setUiStep(1); setDetails({}) }}>Back</button>
                <button onClick={() => handleSubmit()}>Confirm</button>

            </div>

        </div>
    )
}
