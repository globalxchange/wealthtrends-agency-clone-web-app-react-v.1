
import React from 'react'
import nextId from 'react-id-generator';
import Images from '../../../../../../assets/a-exporter';
import LoadingAnimation from '../../../../../../lotties/LoadingAnimation';
import { Agency } from '../../../../../Context/Context';

export default function UpdateInterestStepTwo({ setUiStep, setDetails, details }) {
    const agency = React.useContext(Agency);
    const { allApps, allCoins } = agency;
    const [list, setList] = React.useState([]);
    const [step, setStep] = React.useState(0);
    const [loading, setLoading] = React.useState(false)

    const setUpList = () => {
        switch (step) {
            case 0: setList([...stepZero]); break;
            case 1: setList([...stepOne]); break;
            case 2: let temp = allApps.map(obj => { return { keyId: nextId(), icon: obj.app_icon, title: obj.app_name, text: "", id: obj.app_code } });
                setList([...temp]); break;
            case 3: setList([...stepThree]); break;
            case 4: let tempTwo = [...allCoins.fiat, ...allCoins.crypto].map(obj => { return { keyId: obj._id + "update_interest", icon: obj.coinImage, title: obj.coinName, text: "", id: obj.coinSymbol } });
                setList([...tempTwo]); break;
        }
        setLoading(false)
    }
    const handleClick = (obj) => {
        switch (step) {
            case 0: setDetails({ ...details, type: obj }); setStep(1); break;
            case 1: setDetails({ ...details, destination: obj }); setStep(2); break;
            case 2: setDetails({ ...details, app: obj }); setStep(3); break;
            // case 3: setDetails({ ...details, frequency: obj }); setUiStep(2); break;
            case 3: setDetails({ ...details, frequency: obj }); setStep(4); break;
            case 4: setDetails({ ...details, coin: obj }); setUiStep(2); break;
        }
        setLoading(false)
    }
    const returnText = () => {
        switch (step) {
            case 0: return "Which Type Of Earnings Would You Like To Update";
            case 1: return "What Type Of Destination ?";
            case 2: return "Select The App For Your New Destination";
            case 3: return "Update The Frequency Of Payments";
            case 4: return "Update The Vault";
        }
    }
    React.useEffect(() => {
        setLoading(true)
        setUpList()
    }, [step])

    return (
        <div className="update-interest-part-two">
            <div className="ui-two-header">
                <h3>Settings</h3>
                <p>{returnText()}</p>
            </div>
            <div className="ui-two-body">
                {
                    loading ?
                        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                            <LoadingAnimation />
                        </div>
                        :
                        list.map(obj => <div
                            key={obj.keyId}
                            style={obj.id === "external" ? { opacity: 0.25, pointerEvents: "none" } : {}}
                            onClick={() => handleClick(obj)} key={obj.keyId}>
                            <h6><img className={step == 2 || step == 4 ? "" : "d-none"} height="20px" src={obj.icon} />{obj.title}</h6>
                            <p>{obj.text}</p>
                            <img className="right-arrow-ui" src={Images.rightArrowNew} />
                        </div>
                        )
                }
            </div>
        </div>
    )
}
const stepZero = [
    { keyId: nextId(), title: "Liquid Earnings", icon: Images.smallMoneyIcon, text: "Interest Earned From Liquid Assets.", id: "liquid" },
    { keyId: nextId(), title: "Bond Earnings", icon: Images.smallIcedIcon, text: "Interest Earned From Locked Assets.", id: "bond" },

]

const stepOne = [
    { keyId: nextId(), title: "Another Vault", text: "Paid To One Of Your Liquid Vaults", id: "vault" },
    { keyId: nextId(), title: "External Address", text: "Automatically Paid To An External Address", id: "external" },

]
const stepThree = [
    { keyId: nextId(), title: "Daily", text: "Get Paid Daily", id: "daily" },
    { keyId: nextId(), title: "Weekly", text: "Get Paid Weekly", id: "weekly" },
    { keyId: nextId(), title: "Monthly", text: "Get Paid Monthly", id: "monthly" },

]