import React from 'react'
import Images from '../../../../../../assets/a-exporter'
import LoadingAnimation from '../../../../../../lotties/LoadingAnimation';
import { getCurrentInterestPayment } from '../../../../../../services/getAPIs';
import { Agency } from '../../../../../Context/Context';

export default function UpdateInterestStepOne({ destinations, setDestinations, setType, handleSubmit, details, setDetails, setUiStep }) {
    const agency = React.useContext(Agency);
    const { allAppsAvailable } = agency;
    const [loading, setLoading] = React.useState(true);
    const [liquidApp, setLiquidApp] = React.useState();
    const [icedApp, setIcedApp] = React.useState();

    const setUpDestination = async () => {
        let res = await getCurrentInterestPayment();
        if (!res.data.destinations.length) {
            let temp = {
                frequency: { id: 'daily' },
                destination: { id: 'vault' },
                app: { id: localStorage.getItem("appCode") },
                type: { id: 'both' }
            }
            handleSubmit(true, temp);
            return;
        }
        setDestinations(res.data.destinations[res.data.destinations.length - 1]);

        setLoading(false);
        const appOne = allAppsAvailable.find(x => { return x.app_code === res.data.destinations[0]?.liquidData?.payoutDestination_data?.app_code })

        const appTwo = allAppsAvailable.find(x => { return x.app_code === res.data.destinations[0]?.icedData?.payoutDestination_data?.app_code })

        setLiquidApp(appOne);
        setIcedApp(appTwo);
    }

    React.useEffect(() => {
        setUpDestination()
    }, [])

    return (
        loading ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />
            </div>
            :
            !destinations ?
                <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                    <LoadingAnimation type="no-data" />
                </div>
                :
                <div className="update-interest-part-one">
                    <div className="ui-header">
                        <h3>Interest Settings</h3>
                        <p>Here Are Your Current Interest Payment Settings </p>
                    </div>
                    <div className="ui-money-market-earning">
                        <div className="money-market-earning-header">
                            <img src={Images.smallMoneyIcon} />
                            <h5>MoneyMarket Earnings</h5>
                        </div>
                        <div className="money-market-earning-body">
                            <div>
                                <span>Frequency</span>
                                <span>{destinations.liquidData.payoutFrequency.toString().substring(0, 1).toUpperCase() + destinations.liquidData.payoutFrequency.toString().substring(1)}</span>
                            </div>
                            <div>
                                <span>Destination</span>
                                <span><img src={liquidApp?.app_icon} />{liquidApp?.app_name}</span>
                            </div>
                        </div>
                    </div>
                    {
                        !destinations.icedData ?
                            <div />
                            :
                            <div className="ui-money-market-earning">
                                <div className="money-market-earning-header">
                                    <img src={Images.agencyDark} />
                                    <h5>Bond Earnings</h5>
                                </div>
                                <div className="money-market-earning-body">
                                    <div>
                                        <span>Frequency</span>
                                        <span>{destinations.icedData.payoutFrequency.toString().substring(0, 1).toUpperCase() + destinations.icedData.payoutFrequency.toString().substring(1)}</span>
                                            
                                    </div>
                                    <div>
                                        <span>Destination</span>
                                        <span><img src={icedApp?.app_icon} />{icedApp?.app_name}</span>
                                    </div>
                                </div>
                            </div>
                    }


                    <div className="ui-footer-buttons">
                        <button onClick={() => setType("home")}>Back</button>
                        <button onClick={() => setUiStep(1)}>Edit</button>
                    </div>
                </div>
    )
}

                    // {
                    //     !destinations.icedData ?
                    //         <div>
                    //         </div>
                    //         :
                    //         <div className="ui-money-market-earning">
                    //             <div className="money-market-earning-header">
                    //                 <img src={Images.agencyDark} />
                    //                 <h5>Bond Earnings</h5>
                    //             </div>
                    //             <div className="money-market-earning-body">
                    //                 <div>
                    //                     <span>Frequency</span>
                    //                     <span>{destinations.icedData.payoutFrequency.toString().substring(0,1).toUpperCase() + destinations.icedData.payoutFrequency.toString().substring(1)}</span>
                    //                     // <span>{destinations.icedData.payoutFrequency}</span>
                    //                 </div>
                    //                 <div>
                    //                     <span>Destination</span>
                    //                     <span><img src={icedApp?.app_icon} />{icedApp?.app_name}</span>
                    //                 </div>
                    //             </div>
                    //         </div>
                    // }