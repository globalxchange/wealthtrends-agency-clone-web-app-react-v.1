import React from 'react'
import LoadingAnimation from '../../../../../../lotties/LoadingAnimation';
import { passwordReset, requestForgotPassword } from '../../../../../../services/postAPIs';
import OtpInput from 'react-otp-input';
import Constant from '../../../../../../json/constant';
import Images from '../../../../../../assets/a-exporter';

export default function UsernameUpdatePassword({ changeNotification, setType }) {
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/
    const [step, setStep] = React.useState(0);
    const [loading, setLoading] = React.useState(0);
    const [value, setValue] = React.useState('');
    const [validPassword, setValidPassword] = React.useState(false);
    const [password, setPassword] = React.useState('');
    const [show, setShow] = React.useState(false);
    const [confirmPassword, setConfirmPassword] = React.useState('')
    const handleChange = (e) => {
        setValue(e)
    }

    const sendEmail = async () => {
        setLoading(true)
        let email = localStorage.getItem("userEmail");
        let res = await requestForgotPassword(email);
        if (res.data.status) {
            setStep(1);
            setLoading(false)
        } else {
            changeNotification({ status: false, message: "Something Went Wrong.Please Try Again" })
            setLoading(false);
        }

    }

    const handleChangePassword = (e) => {
        setPassword(e.target.value);
        setValidPassword(passwordRegex.test(e.target.value));

    }
    const handleUpdatePassword = async () => {
        setLoading(true)
        let email = localStorage.getItem("userEmail");
        let obj = {
            email: email,
            code: value,
            newPassword: password
        }
        let res = await passwordReset(obj);
        if (res.data.status) {
            changeNotification({ status: true, message: "Password Changed Successfully" })
            setLoading(false);
            setStep(3)
        } else {
            setLoading(false)
            setLoading(0)
            changeNotification({ status: false, message: "Something Went Wrong.Please Try Again" })
        }
    }
    const changePasswordSteps = () => {
        switch (step) {
            case 0: return <div className="update-password-step-one">
                <h5>
                    Change Password
                </h5>
                <p>
                    Once You Click Initiate Password Change. You Will Receive A Six Digit Code In Your Email Which You Have To Enter To Complete The Password Change.
                </p>
                <button onClick={() => sendEmail()}>Initiate</button>
            </div>
            case 1: return <div className="update-password-step-two">
                <h5>Enter Code</h5>
                <div className="row-wrapper">
                    <OtpInput shouldAutoFocus={true} isInputNum={true} placeholder="000000" value={value} onChange={handleChange} numInputs={6} />
                </div>
                <button onClick={() => setStep(2)} disabled={!value.length >= 6}>Confirm</button>
            </div>
            case 2: return <div className="update-password-step-three">
                <h5>Enter New Password</h5>
                <div>
                    <span>
                        <input
                            type={show ? "text" : "password"}
                            className={validPassword ? "green-border" : ""}
                            onChange={handleChangePassword}
                            placeholder="New Password"
                        />
                        <img height="16px"
                            onClick={() => setShow(!show)}
                            src={show ? Images.doNotShowPassword : Images.showPassword} />
                    </span>
                    <span>
                        <input
                            type="password"
                            className={password === confirmPassword && password ? "green-border" : ""}
                            disabled={!validPassword}
                            onFocus={() => setShow(false)}
                            placeholder="Confirm New Password"
                            onChange={e => setConfirmPassword(e.target.value)}
                        />
                    </span>
                </div>

                <button
                    onClick={() => handleUpdatePassword()}
                    disabled={!password || password !== confirmPassword}>Confirm</button>
                <div className={validPassword ? "d-none" : "up-checklist"}>
                    <h5>{Constant.passwordChecklist.title}</h5>
                    {
                        Constant.passwordChecklist.list.map(obj => <p key={obj.keyId}>{obj.text}</p>)
                    }
                </div>

            </div>
            case 3: return <div className="update-password-step-one">
                <h5>
                    Change Password
                </h5>
                <p>
                    Once You Click Initiate Password Change. You Will Receive A Six Digit Code In Your Email Which You Have To Enter To Complete The Password Change.
                </p>
                <button onClick={() => setType("home")}>Back To Profile</button>
            </div>
        }

    }
    return (
        <div className="user-profile-update-password">
            {
                loading ?
                    <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                        <LoadingAnimation />
                    </div>
                    :
                    changePasswordSteps()
            }
        </div>
    )
}
