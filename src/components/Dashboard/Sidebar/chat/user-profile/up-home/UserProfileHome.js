import React from 'react'
import nextId from 'react-id-generator'
import { Link } from 'react-router-dom'
import Images from '../../../../../../assets/a-exporter'
import { Agency } from '../../../../../Context/Context'

export default function UserProfileHome({ user, setType }) {
    const container = React.useRef()
    const agency = React.useContext(Agency);
    const [scrolled, setScrolled] = React.useState(false)
    const { enableuserfunctiom, setCollapseSidebar, updatePortfolioRate } = agency;
    const [selectedFooter, setSelectedFooter] = React.useState(footerList[0]);
    const footerRef = React.useRef();
    const findByTitle = (obj) => {
        switch (obj.id) {
            case "auth": return obj.title
            case "username": return user.username
            case "password": return obj.title
            case "country": return "Canada"
        }

    }
    const findValue = (obj) => {
        switch (obj.id) {
            case "auth": return obj.action
            case "username": return obj.action
            case "password": return obj.action
            case "country": return obj.action
        }
    }
    const handleLinkClick = (obj) => {
        switch (obj.id) {
            case "auth": enableuserfunctiom(); setCollapseSidebar(false); break;
            case "username": setType("username"); break;
            case "password": setType("password"); break;
            case "interest": setType("interest"); break;
            case "portfolio": setType("portfolio"); break;
            case "bio": setType("bio"); break;
        }
    }
    const handleWheel = (e) => {
        footerRef.current.scroll({ top: 0, left: footerRef.current.scrollLeft + e.deltaY, behavior: "smooth" });
    }
    const handleScroll = () => {
        console.log("Scroll", container.current.scrollTop)
        if (!container.current.scrollTop) {
            setScrolled(false)
        } else {
            setScrolled(true)
        }
    }
    React.useEffect(() => {
        if (!updatePortfolioRate) return
        setType("portfolio")
    }, [updatePortfolioRate])
    return (
        <div className="user-profile-home">
            <div className="user-profile-home-body">
                <div style={scrolled ? { borderBottom: '1px solid #e5e5e5' } : {}} className="user-settings-header">
                    <h4>Settings</h4>
                    <p>Select The Information You Want To Update</p>
                </div>
                <div onScroll={() => handleScroll()} ref={container} className="user-setting-list">
                    {
                        settingsList.map(obj =>
                            <div key={obj.keyId} onClick={() => handleLinkClick(obj)} key={obj.keyId}
                                className={`setting-child ${["auth", "username", "password", "interest", "portfolio", "bio"].includes(obj.id) ? "" : "disabled-current-div"}`}>
                                <h6>{obj.name}</h6>
                                <p>{obj.text}</p>
                                <img src={Images.rightArrowNew} />
                            </div>
                        )
                    }


                </div>
            </div>
            <div ref={footerRef} onWheel={handleWheel} className="user-profile-home-footer">
                {
                    footerList.map(obj =>
                        <h6
                            key={obj.keyId}
                            className={selectedFooter.keyId === obj.keyId ? "selected-footer" : "un-clickable"}
                            onClick={() => setSelectedFooter(obj)}
                            key={obj.keyId}
                        >
                            {obj.name}
                        </h6>
                    )
                }

            </div>
        </div>
    )
}

const tabHeaders = [
    { keyId: nextId(), name: "About" },
    { keyId: nextId(), name: "Settings" }
]
const settings = [
    { keyId: nextId(), title: "Google Authenticator", action: "Enable", id: "auth", link: "" },
    { keyId: nextId(), title: "", action: "Change Username", id: "username", link: '' },
    { keyId: nextId(), title: "*************", action: "Reset Password", id: "password", link: '' },
    { keyId: nextId(), title: "Canada", action: "Change Country", id: "country", link: '' },
]

const footerList = [
    { keyId: nextId(), name: "Settings" },
    { keyId: nextId(), name: "Checkbook" },
    { keyId: nextId(), name: "Friends" },
    { keyId: nextId(), name: "Apps" },
    { keyId: nextId(), name: "Calendar" },
    { keyId: nextId(), name: "BrokerChain" },
    { keyId: nextId(), name: "Subscriptions" },
    { keyId: nextId(), name: "Communities" },
    { keyId: nextId(), name: "Cards" }
]

const settingsList = [
    { keyId: nextId(), name: "Change Username", text: "Update Your Username", id: "username" },
    { keyId: nextId(), name: "Change Password", text: "Update Password", id: "password" },
    { keyId: nextId(), name: "Configure 2FA", text: "Enable, Disable 2FA", id: "auth" },
    { keyId: nextId(), name: "Interest Settings", text: "Update the destination of your daily earnings", id: "interest" },
    { keyId: nextId(), name: "Poftolio.ai Settings", text: "Import Rate Date  ", id: "portfolio" },
    { keyId: nextId(), name: "Images And Bio", text: "Update Images & Bio", id: "bio" },
    { keyId: nextId(), name: "Update Phone Number", text: "Update Your Phone Number", id: "number" },

]