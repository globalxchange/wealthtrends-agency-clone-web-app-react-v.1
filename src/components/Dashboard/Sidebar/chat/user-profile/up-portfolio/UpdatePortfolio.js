import React from 'react'
import nextId from 'react-id-generator';
import Images from '../../../../../../assets/a-exporter';
import LoadingAnimation from '../../../../../../lotties/LoadingAnimation';
import { cryptoUSD, conversionRateWIthNoFees } from '../../../../../../services/getAPIs';
import { Agency } from '../../../../../Context/Context'
import './update-portfolio.style.scss'
export default function UpdatePortfolio() {
  const agency = React.useContext(Agency);
  const { setUpdatePortfolio, allCoins, valueFormatter,conversionConfig, setConversionConfig } = agency;
  const [expand, setExpand] = React.useState(false);
  const [coinList, setCoinList] = React.useState([])
  const [loading, setLoading] = React.useState(false)

  const setUpCoinList = async () => {
    setLoading(true)
    let res = await cryptoUSD();
    let resTwo = await conversionRateWIthNoFees();
    let rate = 1;
    let tempList = [...allCoins.fiat, ...allCoins.crypto].map(obj => {
      if (!res.data[obj.coinSymbol]) {

      } else {
        if (obj.coinSymbol === "USD") {
          rate = resTwo.data["INR"] / res.data["INR"];

        } else {
          rate = resTwo.data[obj.coinSymbol] / res.data[obj.coinSymbol] * (1 / resTwo.data[obj.coinSymbol]);

        }

        return { coinName: obj.coinName, coinSymbol: obj.coinSymbol, coinImage: obj.coinImage, rate: rate }
      }
    });
    setCoinList([...tempList]);
    setLoading(false)
  }
  const handleClick = (obj) =>{
    setConversionConfig({...conversionConfig, rate: obj.rate,enable: true, coin: obj.coinSymbol});
    setExpand(false)
  }

  React.useEffect(() => {
    setUpCoinList()
    return () => {
      setUpdatePortfolio(false)
    }
  }, [])
  React.useEffect(() => {
    if (expand) {
      setUpCoinList();
    }else{
      setCoinList([])
    }
  }, [expand])
  return (
    <div className="update-portfolio-main">
      <div className="upm-header">
        <h2>Portfolio.ai</h2>
        <span>Here is your current Customizations</span>

      </div>
      <div className="upm-body">
        <div className={`upm-body-child ${expand ? "expand-upm-child" : ""}`}>
          <div onClick={() => setExpand(!expand)} className="upm-body-child-header">
            <img src={Images.coinMarketCap} />
          </div>
          {
            !expand ? '' : <div className="upm-coin-collection">
              {
                loading ?
                  <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                    <LoadingAnimation />
                  </div>
                  :
                  coinList.map(obj => <div key={obj._id + nextId()} onClick={()=>handleClick(obj)}>
                    <span><img height="20px" src={obj.coinImage} />{obj.coinName}</span>
                    <span>{valueFormatter(obj.rate, obj.coinSymbol)}</span>

                  </div>)
              }

            </div>
          }

        </div>
      </div>
    </div>
  )
}
