import React from "react";
import nextId from "react-id-generator";
import Images from "../../../../../assets/a-exporter";
import "./chat-box.style.scss";
import moment from "moment-timezone";
import LoadingAnimation from "../../../../../lotties/LoadingAnimation";

export default function ChatBox({
  showChatBox,
  botHeader,
  setShowChatBox,
  message,
  pasteImageHandler,
  messageArray = [],
  setStepNumber,
  userDetails,
  stepNumber,
  handleFileChange,
  currentUserObj,
  handleSubmitMessage,
  setMessage,
  userObj = { username: '' },
}) {
  const chatListRef = React.useRef();

  const handleScroll = () => {
    if (chatListRef.current.scrollTop === 0) {
      setStepNumber(messageArray.length / 30);
    }
  };
  React.useEffect(() => {
    if (!chatListRef?.current || stepNumber) return;
    chatListRef.current.scroll({ top: chatListRef.current.scrollHeight });
  }, [messageArray]);

  return (
    // !showChatBox ?
    //   <div className="h-100 w-100 d-flex justify-content-center align-items-center pl-2 pr-2 chat-curtain">
    //     <h5 className="text-center">
    //       Click <button onClick={() => setShowChatBox(true)}>Here</button> To Initiate Support Chat
    //   </h5>

    //   </div>
    //   :
      !messageArray.length ?
        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
          <LoadingAnimation type="no-data" size={{ height: 150, width: 150 }} />
        </div>
        :
        <div className="chat-box-main">
          {/* <div
            className={
              !showChatBox || botHeader.id === "learn" ? "chat-curtain" : "d-none"
            }
          >
            <h5 >
              Click <button onClick={() => setShowChatBox(true)}>Here</button> To Initiate Support Chat
        </h5>
          </div> */}
          <div
            onScroll={() => handleScroll()}
            ref={chatListRef}
            className="chat-box-body"
          >
            {
              !messageArray.length ?
                <div>
                  <LoadingAnimation type="no-data" />
                </div>
                :
                messageArray.map((obj) =>
                  <div key={obj._id + nextId()}
                    className={`single-message-wrapper ${obj?.type === "file" ?
                      !obj.sender?.username ? obj?.sender?.toLowerCase() === userObj?.username?.toLowerCase() ? "reverse-it" : "" :
                        obj?.sender?.username?.toLowerCase() === userObj?.username?.toLowerCase() ? "reverse-it" : ""

                      :
                      obj?.sender?.username?.toLowerCase() ===
                        userObj?.username?.toLowerCase()
                        ? "reverse-it"
                        : ""
                      }`}
                  >
                    <div className="messaged-by">
                      <img height="20px" src={!obj.sender.avatar ? Images.face : obj.sender.avatar} />
                      <span>{moment(obj.timestamp).format("hh:mm a")}</span>
                    </div>
                    {true ? (
                      obj.type === "file" ? (
                        <div className="chat-image-container">
                          <img src={obj.location} />
                        </div>
                      ) : (
                          <div className="simple-message">
                            <span>{obj.message}</span>
                          </div>
                        )
                    ) : (
                        <div className="transaction-message">
                          <span>
                            <img height="20px" src={Images.bitcoin} /> {obj.coin}
                          </span>
                          <span>{obj.amount}</span>
                        </div>
                      )}
                  </div>
                )
            }
          </div>
          <div className="chat-box-footer">
            <label htmlFor="filesUpload">
              <input
                onChange={handleFileChange}
                id="filesUpload"
                type="file"
                className="d-none"
              />
              <img src={Images.fileUpload} />
            </label>
            <div className="chat-box-input-wrapper">
              <input
                onPaste={pasteImageHandler}
                onKeyPress={(e) =>
                  e.which === 13 ? handleSubmitMessage() : console.log()
                }
                value={message}
                placeholder="Enter Your Message"
                onChange={(e) => setMessage(e.target.value)}
              />
            </div>
            <button disabled={!message} onClick={handleSubmitMessage}>
              <img src={Images.sendIcon} />
            </button>
          </div>
        </div>
  );

}