import React from 'react'
import SetupChatProfile from '../setup-chat-profile/SetupChatProfile'
import './menu-setup.style.scss'
export default function MenuSetup({ handleNewSignup, setVisibleMoreInfo, visibleMoreInfo }) {
    return (
        <div className="w-100 h-100">
            <SetupChatProfile
                handleNewSignup={handleNewSignup}
                setVisibleMoreInfo={setVisibleMoreInfo}
                visibleMoreInfo={visibleMoreInfo}
            />
        </div>
    )
}
