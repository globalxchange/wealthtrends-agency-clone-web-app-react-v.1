import React from 'react'
import Constant from '../../../../../json/constant';
import { Agency } from '../../../../Context/Context';
import './transact-add.style.scss'
import TransactAddMethods from './TransactAddMethods';
import TransactAddSelect from './TransactAddSelect';
import TransactAddSteps from './TransactAddSteps';
import TransactAddCoinDetails from './TransactAddCoinDetails';
export default function TransactAdd({ step, handleMainBack, setStep }) {
    const agency = React.useContext(Agency);
    const { directCoinOverview, mainBalance,setDirectCoinOverview } = agency;

    const [currentSteps, setCurrentSteps] = React.useState([]);
    const [selectedCoin, setSelectedCoin] = React.useState();
    const [activeStep, setActiveStep] = React.useState(0);
    const [selectedOptions, setSelectedOptions] = React.useState([Constant.botTransact.list[0]])
    const [previouslySelectedId, setPreviouslySelectedId] = React.useState("tDeposit");
    const setUpCurrentStep = (first, second) => {
        let tempArray = [
            { ...first },
            { ...second }
        ]
        setCurrentSteps([...tempArray]);
    }
    const [details, setDetails] = React.useState({
        type: "tDeposit",
        asset: null,
        coin: null
    })

    React.useEffect(() => {
        if (step === 0) {
            setActiveStep(0);
            setPreviouslySelectedId("tDeposit")
            setUpCurrentStep(Constant.botTransact, Constant.transactionAssets)
            setSelectedOptions([Constant.botTransact.list[0]])
            setDetails({
                type: "tDeposit",
                asset: null,
                coin: null

            });

        }
    }, [step])
    const handleSteps = (obj) => {
        setPreviouslySelectedId(obj.id)
        setSelectedOptions([...selectedOptions, obj])
        switch (activeStep) {
            case 0: setDetails({ ...details, asset: obj.name }); setActiveStep(1); selectCoins(obj.name); break;
            case 1: setSelectedCoin(obj); setDetails({ ...details, coin: obj.name }); setStep(1);
            // case 2: selectCoins(details.asset); setUpTransactionList(obj.name); break;
        }
    }
    React.useEffect(()=>{
        if(!selectedCoin){

        }else{
            setDirectCoinOverview({...directCoinOverview, coin: selectedCoin, type: selectedCoin.type})
        }
    },[selectedCoin])

    const selectCoins = (coin) => {
        let tempArray = []
        if (coin === "Fiat") {
            tempArray = mainBalance.fiat.map(obj => { return { ...obj, name: obj.coinSymbol, icon: obj.coinImage } })
        } else {
            tempArray = mainBalance.crypto.map(obj => { return { ...obj, name: obj.coinSymbol, icon: obj.coinImage } })
        }
        setUpCurrentStep(Constant.transactionAssets, { ...Constant.transactionCurrency, list: [...tempArray] })
    }
    const handleBackSteps = () => {
        if (activeStep) {
            setActiveStep(0);
            setPreviouslySelectedId("tDeposit")
            setUpCurrentStep(Constant.botTransact, Constant.transactionAssets)
            setSelectedOptions([Constant.botTransact.list[0]])
            setDetails({
                type: "tDeposit",
                asset: null,
                coin: null

            });
        } else {
            handleMainBack()

        }

    }
    const selectComponent = () => {
        switch (step) {
            case 0: return <TransactAddSelect activeStep={activeStep} handleBackSteps={handleBackSteps} previouslySelectedId={previouslySelectedId} handleSteps={handleSteps} currentSteps={currentSteps} />;
            case 1: return !selectedCoin?null:<TransactAddCoinDetails  setStep={setStep} selectedCoin={selectedCoin}/>
            case 2: return <TransactAddMethods setStep={setStep} selectedCoin={selectedCoin} details={details} />;
            case 3: return <TransactAddSteps />

        }
    }

    React.useEffect(() => {
        setUpCurrentStep(Constant.botTransact, Constant.transactionAssets)

    }, [])
    React.useEffect(() => {
      if(!directCoinOverview.status)return;
      setSelectedCoin(directCoinOverview.coin)
      return () => {
        
      }
    }, [directCoinOverview.status, directCoinOverview.coin])

    return (
        <div className="transact-add-main">
            {
                selectComponent()
            }


        </div>
    )
}
