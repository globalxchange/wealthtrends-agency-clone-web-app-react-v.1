import React from 'react'
import nextId from 'react-id-generator';
import Images from '../../../../../assets/a-exporter';

export default function AddMethods({handleClick, disable, selectedCoin}) {
    return (
        <>
            <h6 className="method-title">How Do You Want To Add {selectedCoin.coinName}?</h6>
            <div className="tam-methods-wrapper">
                {
                    methods.map(obj => <div key={obj.keyId}
                        onClick={() => handleClick(obj.name)}
                        className={obj.name === "Blockcheck" && disable ? "disable" : ""}>
                        <img src={obj.icon} />
                        <span>{obj.name}</span>
                    </div>
                    )
                }
            </div>

        </>
    )
}

const methods = [
    { keyId: nextId(), name: "Blockcheck", icon: Images.hfBlockcheckSmall },
    { keyId: nextId(), name: "Vault", icon: Images.hfVaultSmall },
    { keyId: nextId(), name: "MoneyMarkets", icon: Images.smallMoneyIcon },
    { keyId: nextId(), name: "Ice", icon: Images.smallIcedIcon },
]