import React from "react";
import nextId from "react-id-generator";
import "./transaction-add-coin-details.style.scss";
import Images from "../../../../../assets/a-exporter";
import { getCoinDetails } from "../../../../../services/getAPIs";
import LoadingAnimation from "../../../../../lotties/LoadingAnimation";
import { Agency } from "../../../../Context/Context";
import Constant from "../../../../../json/constant";
export default function TransactAddCoinDetails({ selectedCoin, setStep }) {
  const actionList = [
    {
      keyId: nextId(),
      name: `Add ${selectedCoin.coinName}`,
      icon: Images.fundsQR,
      id: "add",
    },
    {
      keyId: nextId(),
      name: `Buy ${selectedCoin.coinName}`,
      icon: Images.buyCoin,
      id: "buy",
    },
    {
      keyId: nextId(),
      name: `Sell ${selectedCoin.coinName}`,
      icon: Images.sellCoin,
      id: "sell",
    },
    {
      keyId: nextId(),
      name: `Send ${selectedCoin.coinName}`,
      icon: Images.sendCoin,
      id: "send",
    },
    {
      keyId: nextId(),
      name: "Check Transactions",
      icon: Images.checkTransactions,
      id: "transactions",
    },
    {
      keyId: nextId(),
      name: `Cash ${selectedCoin.coinName}`,
      icon: Images.investCoin,
      id: "cash",
    },
  ];
  const agency = React.useContext(Agency);
  const {
    valueFormatter,
    setUserChatEnable,
    conversionConfig,
    setMainTabs,
    setDirectCoinOverview,
    setTradePairObj,
    setLedgerOrAnalytics,
    setFundsFrom,
    setTradeState,
    setMainSteps,
    setAssetSelected,
    setSecondGate,
    setType,
    setFundsTo,
    tradePairObj,
    setScrollValue,
    setSkipTradeStep,
    setFundsStepOneConfig,
    setCollapseSidebar,
    directCoinOverview,
    handleAnalytics,
    setAssetClassSelected,
    setFundsConfig,
    setCopiedOverlay,
  } = agency;
  const [coinDetails, setCoinDetails] = React.useState(null);
  const [loading, setLoading] = React.useState(true);

  const setUpCoinDetails = async () => {
    setLoading(true);
    let res = await getCoinDetails(selectedCoin.coinSymbol);
    if (res.data.status) {
      setCoinDetails(res.data.coins[0]);
    }
    setLoading(false);
  };

  const checkForZeros = (amount) => {
    let temp = amount / 1000000000;
    if (temp < 1) {
      let tempTwo = amount / 1000000;
      return tempTwo < 0
        ? parseFloat(tempTwo).toFixed(2) + "M"
        : parseInt(tempTwo) + "M";
    } else {
      return parseInt(temp) + "B";
    }
  };

  const handleClick = (id) => {
    switch (id) {
      case "add":
        setStep(2);
        break;
      case "send":
        setUserChatEnable(false);
        setMainTabs("accounts");
        setFundsConfig({
          add: false,
          crypto: selectedCoin?.type?.toLowerCase() === "crypto" ? true : false,
          coin: selectedCoin,
          num: 0,
          action: true,
        });
        break;
      case "sell":
        setSkipTradeStep(1);
        setTradePairObj({
          ...tradePairObj,
          quote: {
            _id: selectedCoin.coinSymbol,
            count: 0,
            coin_metadata: { ...selectedCoin },
          },
        });
        setMainTabs("accounts");
        setTradeState(true);
        setAssetSelected(Constant.navbarList[0]);
        setSecondGate(true);
        setCollapseSidebar(false);
        break;
      case "buy":
        setSkipTradeStep(0);
        setTradePairObj({
          ...tradePairObj,
          base: {
            _id: selectedCoin.coinSymbol,
            count: 0,
            coin_metadata: { ...selectedCoin },
          },
        });
        setMainTabs("accounts");
        setTradeState(true);
        setAssetSelected(Constant.navbarList[0]);
        setSecondGate(true);
        setCollapseSidebar(false);
        break;
      case "transactions":
        goToLedger();
        break;
      case "cash":
        setUserChatEnable(false);
        setMainTabs("accounts");

        setFundsConfig({
          add: true,
          crypto: true,
          coin: selectedCoin,
          num: 0,
          action: true,
        });
        handleMethod();
    }
  };
  const handleMethod = () => {
    setFundsFrom(null);
    setFundsTo(null);
    setFundsFrom(null);
    setFundsTo(null);
    setMainSteps(0);
    setType(Constant.depositSection[3]);
    setFundsStepOneConfig({
      type: "iv",
      internal: true,
      both: false,
      step: 1,
    });
  };
  const goToLedger = () => {
    const { coin, type, len, i } = directCoinOverview;
    switch (type) {
      case "fiat":
        handleAnalytics("");
        setAssetSelected(Constant.navbarList[1]);
        afterSomeTime(coin, i);
        setSecondGate(true);
        break;
      case "crypto":
        handleAnalytics("");
        setAssetSelected(Constant.navbarList[0]);
        afterSomeTime(coin, i);
        setSecondGate(true);
        break;
      case "asset":
        handleAnalytics("");
        setAssetSelected(Constant.navbarList[2]);
        afterSomeTime(coin, i);
        setSecondGate(true);
        break;
    }
  };
  const afterSomeTime = (data, i) => {
    let a = setTimeout(() => {
      setAssetClassSelected(data);
      if (i > 1) {
        setScrollValue(i);
      }
      clearTimeout(a);
      setLedgerOrAnalytics(true);
      setCollapseSidebar(false);
    }, 300);
  };

  React.useEffect(() => {
    setUpCoinDetails();
    return () => {
      setDirectCoinOverview({
        status: false,
        coin: null,
        type: "",
        len: "",
        i: "",
      });
    };
  }, [selectedCoin]);

  return loading ? (
    <div className="w-100 h-100 d-flex justify-content-center align-items-center">
      <LoadingAnimation />
    </div>
  ) : (
    <div className="transaction-add-coin-details">
      <div className="t-a-c-d-title">
        <h4>
          <img src={selectedCoin?.coinImage} />
          {selectedCoin?.coinName}
        </h4>
        <h4
          onCopy={() =>
            setCopiedOverlay({
              status: true,
              title: selectedCoin?.coinName,
              data: valueFormatter(coinDetails?.usd_price, "USD"),
            })
          }
        >
          {valueFormatter(coinDetails?.usd_price, "USD")}
        </h4>
      </div>
      <div className="t-a-c-d-market">
        <div>
          <h3 
          onCopy={() =>
            setCopiedOverlay({
              status: true,
              title: 'Market Cap',
              data: checkForZeros(parseInt(coinDetails?.market_cap))
            })
          }
          >
            {checkForZeros(parseInt(coinDetails?.market_cap))}
            <span>Market Cap</span>
          </h3>
        </div>
        <div>
          <h3
          onCopy={() =>
            setCopiedOverlay({
              status: true,
              title: '24 Hr Volume',
              data: checkForZeros(parseInt(coinDetails?.volume_24hr))
            })
          }
          >
            {checkForZeros(parseInt(coinDetails?.volume_24hr))}
            <span>24 Hr Volume</span>
          </h3>
        </div>
        <div>
          <h3
            style={
              coinDetails?._24hrchange < 0
                ? { color: "#da4040" }
                : { color: "#2ea654" }
            }
          >
            {!coinDetails?._24hrchange
              ? conversionConfig.enable
                ? valueFormatter(0, conversionConfig.coin)
                : valueFormatter(0, "USD")
              : conversionConfig.enable
              ? valueFormatter(
                  parseFloat(
                    coinDetails.usd_price *
                      coinDetails?._24hrchange *
                      conversionConfig.rate *
                      0.01
                  ),
                  conversionConfig.enable ? conversionConfig.coin : "USD"
                )
              : valueFormatter(
                  parseFloat(
                    coinDetails.usd_price * coinDetails?._24hrchange * 0.01
                  ),
                  "USD"
                )}
            <span>24HR P/L ($)</span>
          </h3>
        </div>
        <div>
          <h3
            style={
              coinDetails?._24hrchange < 0
                ? { color: "#da4040" }
                : { color: "#2ea654" }
            }
          >
            {parseFloat(Math.abs(coinDetails?._24hrchange)).toFixed(2) + "%"}
            <span>24HR P/L (%)</span>
          </h3>
        </div>
      </div>
      <div className="t-a-c-d-list">
        <h6>Select One Of The Following Actions</h6>
        {actionList.map((obj) => (
          <div
            className={obj.id === "invest" ? "disable-it" : ""}
            onClick={() => handleClick(obj.id)}
          >
            <img src={obj.icon} />
            <h6>{obj.name}</h6>
          </div>
        ))}
      </div>
    </div>
  );
}
{
  /* <div className="t-a-c-d-balance">
    <h3>{valueFormatter(coinDetails?.usd_price, "USD")}</h3>
    <span style={coinDetails?._24hrchange < 0?{color: '#da4040'}:{color: '#2ea654'}}>({valueFormatter(coinDetails?._24hrchange, "USD")})<b></b></span>
</div> */
}
