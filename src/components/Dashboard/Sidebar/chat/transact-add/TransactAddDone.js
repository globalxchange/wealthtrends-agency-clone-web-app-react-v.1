import React from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../../assets/a-exporter';
import { useHistory } from 'react-router-dom';
import { Agency } from '../../../../Context/Context';

export default function TransactAddDone({ setStep, selectedCoin }) {
    const agency = React.useContext(Agency);
    const { setUserChatEnable, setMainTabs, mainBalance, setFundsConfig, setCollapseSidebar, setCheckedId, setAssetSelected } = agency
    const history = useHistory();

    const doneList = [
        { keyId: nextId(), icon: Images.whiteLogoBlockcheck, text: `Another Deposit`, id: "blockcheck", color: "#1A6BB4" },
        { keyId: nextId(), icon: Images.whiteLogoTerminal, text: `Grow Your ${selectedCoin.coinName}`, id: "investment", color: "#464B4E" },
        { keyId: nextId(), icon: Images.whiteLogoTrade, text: `Trade Your ${selectedCoin.coinName}`, id: "terminal", color: "#182542" },
        { keyId: nextId(), icon: Images.whiteLogoConnect, text: `Send Your ${selectedCoin.coinName}`, id: "connect", color: "#1A6BB4" },
    ]
    const handleNextClick = (obj) => {
        switch (obj.id) {
            case "blockcheck": setStep(1); break;
            case "investment": setUserChatEnable(false); setCheckedId("id4"); setCollapseSidebar(false); history.push('/dashboard'); setAssetSelected(null); break;
            case "terminal": setUserChatEnable(false); setCollapseSidebar(false); history.push('/dashboard/Terminals'); break
            case "connect":
                setUserChatEnable(false);
                setMainTabs("accounts");
                setFundsConfig({
                    add: false,
                    crypto: true,
                    coin: mainBalance.crypto[0],
                    num: 0,
                    action: true,
                });
                break;
            default:
                break;
        }
    }
    return (
        <div className="transact-add-done">
            <h5>Congratulations</h5>
            <p>
                You Have Successfully Deposited {selectedCoin.coinSymbol} Into Your Wallet. Your Updated Ethereum Wallet Is Displayed Above. What Would You Like To Do Next?
            </p>
            <div className="transact-add-next-list">
                {
                    doneList.map(obj => <div key={obj.keyId}>
                        <button style={{ backgroundColor: `${obj.color}` }}><img src={obj.icon} /></button>
                        <h5 onClick={() => handleNextClick(obj)} style={{ color: `${obj.color}` }}>{obj.text}</h5>
                    </div>
                    )
                }

            </div>

        </div>
    )
}
