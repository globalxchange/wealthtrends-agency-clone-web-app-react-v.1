import React from "react";
import nextId from "react-id-generator";
import Images from "../../../../../assets/a-exporter";
import Constant from "../../../../../json/constant";
import { Agency } from "../../../../Context/Context";
import AddMethods from "./AddMethods";
import CoinAddressStep from "./CoinAddressStep";
import TransactAddDone from "./TransactAddDone";

export default function TransactAddMethods({ setStep, selectedCoin }) {
  const agency = React.useContext(Agency);
  const [localStep, setLocalStep] = React.useState(0);
  const [overlay, setOverlay] = React.useState(false);
  const [address, setAddress] = React.useState("");
  const [copied, setCopied] = React.useState(false);
  const {
    valueFormatter,
    changeNotification,
    setMainSteps,
    setType,
    setUserChatEnable,
    setMainTabs,
    setFundsConfig,
    setFundsStepOneConfig,
    setFundsTo,
    setFundsFrom,
    setCopiedOverlay
  } = agency;
  const [disable, setDisable] = React.useState(!selectedCoin?.native_deposit);
  const handleClick = (name) => {
    switch (name) {
      case "Blockcheck":
        setLocalStep(1);
        break;

      default:
        setUserChatEnable(false);
        setMainTabs("accounts");

        setFundsConfig({
          add: true,
          crypto: true,
          coin: selectedCoin,
          num: 0,
          action: true,
        });
        handleMethod(name);
        break;
    }
  };
  const handleMethod = (name) => {
    setFundsFrom(null);
    setFundsTo(null);
    switch (name) {
      case "Vault":
        setType(Constant.depositSection[1]);
        setFundsStepOneConfig({
          type: "id",
          internal: true,
          both: false,
          step: 1,
        });
        break;
      case "MoneyMarkets":
        setMainSteps(0);
        setType(Constant.depositSection[2]);
        setFundsStepOneConfig({
          type: Constant.depositSection[2].short,
          internal: Constant.depositSection[2].internal,
          both: Constant.depositSection[2].both,
          step: Constant.depositSection[2].step,
        });
        break;
      case "Ice":
        setMainSteps(0);
        setType(Constant.depositSection[3]);
        setFundsStepOneConfig({
          type: "iv",
          internal: true,
          both: false,
          step: 1,
        });
        break;
    }
  };
  const componentForLocalStep = () => {
    switch (localStep) {
      case 0:
        return (
          <AddMethods
            selectedCoin={selectedCoin}
            handleClick={handleClick}
            disable={disable}
          />
        );
      case 1:
        return (
          <CoinAddressStep
            setLocalStep={setLocalStep}
            setAddressParent={setAddress}
            setOverlay={setOverlay}
            changeNotification={changeNotification}
            setStep={setStep}
            selectedCoin={selectedCoin}
          />
        );
      case 2:
        return (
          <TransactAddDone selectedCoin={selectedCoin} setStep={setLocalStep} />
        );
      default:
        break;
    }
  };
  React.useEffect(() => {
    setDisable(!selectedCoin?.native_deposit);
  }, [selectedCoin]);
  React.useEffect(() => {
    if (!copied) return;
    let a = setTimeout(() => {
      setCopied(false);
    }, 5000);
  }, [copied]);
  return (
    <div className="transaction-add-methods">
      <div
        onCopy={() =>
          setCopiedOverlay({
            status: true,
            title: selectedCoin.coinName,
            data: valueFormatter(
              selectedCoin.coinValue,
              selectedCoin.coinSymbol
            ),
          })
        }
        className="tam-header"
      >
        <span>
          <img src={selectedCoin.coinImage} />
          {selectedCoin.coinName}
        </span>
        <span>
          {valueFormatter(selectedCoin.coinValue, selectedCoin.coinSymbol)}
        </span>
      </div>
      <div className="tam-body">{componentForLocalStep()}</div>
    </div>
  );
}

const methods = [
  { keyId: nextId(), name: "Blockcheck", icon: Images.hfBlockcheckSmall },
  { keyId: nextId(), name: "Vault", icon: Images.hfVaultSmall },
  { keyId: nextId(), name: "MoneyMarkets", icon: Images.smallMoneyIcon },
  { keyId: nextId(), name: "Ice", icon: Images.smallIcedIcon },
];
