import React from "react";
import nextId from "react-id-generator";
import QRCode from "react-qr-code";
import Images from "../../../../../assets/a-exporter";
import LoadingAnimation from "../../../../../lotties/LoadingAnimation";
import {
  getBTCAddress,
  submitHash,
  getTronAddress,
} from "../../../../../services/postAPIs";
import { Agency } from "../../../../Context/Context";

export default function CoinAddressStep({
  changeNotification,
  setStep,
  setLocalStep,
  setOverlay,
  setAddressParent,
  selectedCoin,
}) {
  const agency = React.useContext(Agency);
  const { setCopiedOverlay } = agency;
  const [loading, setLoading] = React.useState(false);
  const [listSteps, setListSteps] = React.useState([]);
  const [address, setAddress] = React.useState("");
  const [showQR, setShowQR] = React.useState(false);
  const [hash, setHash] = React.useState("");
  const [copied, setCopied] = React.useState(false);
  const setUpList = (val) => {
    const tempObj = {
      keyId: nextId(),
      step: 1,
      description: `Copy this ${selectedCoin.coinName} address and input it as the destination address into which ever wallet you are currently using `,
      placeholder: "xxxxxxxxxxxxxxxxxxxxx",
      icon: Images.fundsQR,
    };
    const tempObjTwo = {
      keyId: nextId(),
      step: 2,
      description: `Enter the transaction hash for the transfer for it to reflect into your App Code Vault. `,
      placeholder: "Enter Hash Here",
      icon: Images.fundsQR,
      icon: Images.pasteIcon,
    };
    if (val) {
      setListSteps([tempObj, tempObjTwo]);
    } else {
      setListSteps([tempObj]);
    }
  };
  const handleSubmit = async () => {
    setLoading(true);
    let res = await submitHash(selectedCoin.coinSymbol, hash);
    if (res.data.status) {
      changeNotification({ status: true, message: "Successful" });
      setLocalStep(2);
      setHash("");
    } else {
      changeNotification({ status: false, message: "Unsuccessful" });
      setHash("");
    }
    setLoading(false);
  };
  const copyCodeToClipboard = (str) => {
    navigator.clipboard.writeText(str);
    setCopied(true);
    let a = setTimeout(() => {
      setCopied(false);
      clearTimeout(a);
    }, 4000);
  };
  const getClipboardText = () => {
    navigator.clipboard.readText().then((text) => setHash(text));
  };
  const setUpAddress = async (btc) => {
    let res;
    if (btc) {
      let add = localStorage.getItem("appCode");
      res = await getBTCAddress(add);
    } else {
      let temp = {
        email: localStorage.getItem("userEmail"),
        app_code: localStorage.getItem("appCode"),
        coin: "TRX",
      };
      res = await getTronAddress(temp);
    }
    setAddress(res.data.address);
    setAddressParent(res.data.address);
  };
  React.useEffect(() => {
    let val = ["BTC", "TRX"].includes(selectedCoin.coinSymbol);
    let val2 =
      selectedCoin.eth_token ||
      selectedCoin.coinSymbol === "ETH" ||
      selectedCoin.coinSymbol === "TRX";
    if (!val) {
      setAddress(selectedCoin.coin_address);
    } else {
      setUpAddress(selectedCoin.coinSymbol === "BTC" ? true : false);
    }
    setUpList(val2);
  }, [selectedCoin]);
  return loading ? (
    <div className="w-100 h-100 d-flex justify-content-center align-items-center">
      <LoadingAnimation />
    </div>
  ) : (
    <div className="coin-address-wrapper">
      {listSteps.map((obj, num) => (
        <>
          <div key={obj.keyId} className="step-card">
            <div>
              <h4>Step {obj.step}</h4>
              <span>{obj.description}</span>
            </div>
            <h6
              style={
                !num && copied ? { opacity: 0.5, pointerEvents: "none" } : {}
              }
            >
              <input
                disabled={!num}
                onChange={(e) =>
                  num ? setHash(e.target.value) : console.log()
                }
                value={num ? hash : address}
                placeholder={obj.placeholder}
              />
              <img
                onClick={() => {
                  setCopiedOverlay({
                    status: true,
                    title: "Bitcoin Address",
                    data: address,
                  });
                  copyCodeToClipboard(address);
                }}
                className={!num ? "" : "d-none"}
                src={Images.copy}
              />
              {showQR ? (
                <img
                  src={Images.addDark}
                  className="cancel-image"
                  onClick={() => setShowQR(false)}
                />
              ) : (
                <img
                  onClick={() => (!num ? setShowQR(true) : getClipboardText())}
                  src={obj.icon}
                />
              )}
              <p className={!num && copied ? "" : "d-none"}>Copied</p>
            </h6>
          </div>
          <div className={!num && showQR ? "step-or-wrapper" : "d-none"}>
            <QRCode size={150} value="khkjnkjn" />
          </div>
        </>
      ))}
      <button
        disabled={!hash}
        onClick={() => handleSubmit()}
        className={
          selectedCoin.eth_token ||
          selectedCoin.coinSymbol === "ETH" ||
          selectedCoin.coinSymbol === "TRX"
            ? ""
            : "d-none"
        }
      >
        Submit Hash
      </button>
    </div>
  );
}
