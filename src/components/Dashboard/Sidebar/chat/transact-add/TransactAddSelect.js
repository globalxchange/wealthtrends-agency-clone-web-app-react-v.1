import React from 'react'

export default function TransactAddSelect({ handleSteps, activeStep, handleBackSteps, previouslySelectedId, currentSteps }) {
    const [searchTerm, setSearchTerm] = React.useState('')
    return (
        <div className="transact-add-select">
            {
                currentSteps.map((obj, i) => <div key={obj.keyId} className={`option-wrapper ${i && activeStep ? "increase-height" : ""}`}>
                    {i ? activeStep ?
                        <div className="transact-search-box">
                            <h6>Select Asset</h6>
                            <input onChange={e => setSearchTerm(e.target.value)} placeholder="Type The Name Of The Coin" />
                        </div>
                        : <h6>{obj.title}</h6> : ''}
                    <div className="options-row">
                        {
                            obj.list.filter(x => { return !i ? true : x.name.toLowerCase().startsWith(searchTerm) }).map(x =>
                                <div
                                    key={obj.keyId}
                                    onClick={() => i ? handleSteps(x) : handleBackSteps()}
                                    className={i ? ["completed"].includes(x.id) ? "disable" : "" : previouslySelectedId === x.id ? "selected" : "disable"}>
                                    <img src={x.icon} />
                                    <span>{x.name}</span>
                                </div>
                            )
                        }
                    </div>

                </div>
                )
            }

        </div>
    )
}
