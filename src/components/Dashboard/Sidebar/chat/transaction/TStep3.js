import React from 'react'
import nextId from 'react-id-generator';
import Constant from '../../../../../json/constant';
import { Agency } from '../../../../Context/Context'

export default function TStep3({ selectedTransaction, coinSelected, handleBySupport }) {
    const agency = React.useContext(Agency);
    const { currencyImageList,setLedgerId,setUserChatEnable,setCollapseSidebar, setCheckedId,setLedgerOrAnalytics, setAssetClassSelected, setSecondGate, setAssetSelected, valueFormatter } = agency;
    const [fullAudit, setFullAudit] = React.useState(false)

    const getValues = (obj) => {
        if (obj.nest) {
            if (obj.format) {
                return valueFormatter(selectedTransaction[obj.link][obj.link2], selectedTransaction.coin) + ' ' + selectedTransaction.coin;
            } else {
                return selectedTransaction?.[obj.link]?.[obj.link2]?.toString()?.toUpperCase();
            }

        } else {
            if (obj.format) {
                return valueFormatter(selectedTransaction?.[obj.link], selectedTransaction.coin) + ' ' + selectedTransaction.coin;

            } else {
                return selectedTransaction?.[obj.link]?.toString()?.toUpperCase();
            }
        }
    }
    const handleButtonClick = (x) => {
        if (x === "Dispute") {
            handleBySupport()
        } else {
            if (coinSelected.asset_type.toLowerCase() === "crypto") {
                setCheckedId(Constant.navbarList[0].id)
                setAssetSelected(Constant.navbarList[0]);
                setSecondGate(true);
                setUserChatEnable(false)
                setCollapseSidebar(false)
                let a = setTimeout(() => {
                    setAssetClassSelected(coinSelected);
                    setLedgerId(selectedTransaction._id);
                    setLedgerOrAnalytics(true)
                }, 200)
            } else {
                setCheckedId(Constant.navbarList[1].id)
                setAssetSelected(Constant.navbarList[1]);
                setSecondGate(true);
                setUserChatEnable(false)
                setCollapseSidebar(false)
                let a = setTimeout(() => {
                    setAssetClassSelected(coinSelected);
                    setLedgerId(selectedTransaction._id)
                    setLedgerOrAnalytics(true)
                    clearTimeout(a);
                }, 200)

            }

        }

    }
    return (
        <div className="transaction-step-three">
            <div className="t-step-3-header">
                <img src={currencyImageList[selectedTransaction.coin]} />
                <div>
                    <h6>ID: {selectedTransaction._id}</h6>
                    <span>{selectedTransaction.date}</span>
                </div>
            </div>
            <div className="t-step-3-body">
                {
                    fields.slice(0, fullAudit ? fields.length : 9).map(obj =>
                        <h6 key={obj.keyId}>
                            <span>{obj.name}</span>
                            <span>{getValues(obj)}</span>
                        </h6>
                    )
                }

            </div>
            <div className="audit-section">
                <button onClick={() => setFullAudit(!fullAudit)}>{fullAudit ? "Minimize Audit" : "Get Full Audit"}</button>
            </div>
            <div className="t-step-3-footer">
                {
                    ["Dispute", "Redirect"].map(x => <button key={x + nextId()} onClick={() => handleButtonClick(x)}>{x}</button>)
                }
            </div>
        </div>
    )
}
const fields = [
    { keyId: nextId(), name: "App", format: false, nest: false, link: "app_code" },
    { keyId: nextId(), name: "Banker", format: false, nest: false, link: "banker" },
    { keyId: nextId(), name: "Coin", format: false, nest: false, link: "coin" },
    { keyId: nextId(), name: "Sell Coin", format: false, nest: false, link: "sell_coin" },
    { keyId: nextId(), name: "Sell Amount", format: true, usd: false, nest: false, link: "final_sell_amount" },
    { keyId: nextId(), name: "Buy Coin", format: false, nest: false, link: "buy_coin" },
    { keyId: nextId(), name: "Buy Amount", format: true, usd: false, nest: false, link: "final_buy_amount" },
    { keyId: nextId(), name: "Updated Balance", format: true, usd: false, nest: true, link: "userDebit", link2: "updated_balance" },
    { keyId: nextId(), name: "Current Balance", format: true, usd: false, nest: true, link: "userDebit", link2: "current_balance" },
    { keyId: nextId(), name: "Type", format: false, nest: false, link: "txn_type" },


    { keyId: nextId(), name: "Email", format: false, nest: false, link: "email" },
    { keyId: nextId(), name: "Transaction ID", format: false, nest: false, link: "_id" },
    { keyId: nextId(), name: "Affiliate ID", format: false, nest: false, link: "affiliate_id" },
    { keyId: nextId(), name: "Path ID", format: false, nest: false, link: "path_id" },
    { keyId: nextId(), name: "Profile ID", format: false, nest: false, link: "profile_id" },
    { keyId: nextId(), name: "Banker's App", format: false, nest: true, link: "banker_data", link2: "banker_data" },

    { keyId: nextId(), name: "Identifier", format: false, nest: false, link: "identifier" },
    { keyId: nextId(), name: "User's Debit (Native)", format: true, usd: false, nest: true, link: "userDebit", link2: "amount" },
    { keyId: nextId(), name: "User's Debit (USD)", format: true, nest: true, usd: true, link: "userDebit", link2: "usd_value" },
    { keyId: nextId(), name: "User's Debit Identifier", format: false, nest: true, link: "userDebit", link2: "identifier" },
    { keyId: nextId(), name: "Status", format: false, nest: false, link: "status" },
    { keyId: nextId(), name: "Current Step", format: false, nest: true, link: "current_step_data", link2: "description" },
] 