import React from 'react'
import nextId from 'react-id-generator'

export default function TSTep1({ coinList, handleCoinClick }) {
    return (
        <div className="transaction-step-one">
            <h6 className="t-step-one-title">
                Which Of Your Assets Were Used In The Transaction?
            </h6>
            <div className="coins-wrapper">
                {
                    coinList.map(obj => <div key={obj._i + nextId()} className="t-coins-row" onClick={() => handleCoinClick(obj)}>
                        <img src={obj.coinImage} />
                        <div>
                            <h6>{obj.coinName}</h6>
                        </div>
                    </div>
                    )
                }
            </div>

        </div>
    )
}
