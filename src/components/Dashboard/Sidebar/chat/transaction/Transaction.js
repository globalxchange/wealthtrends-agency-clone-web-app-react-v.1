import React from 'react'
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import { handleWithdrawalList, fetchTransactionDetails } from '../../../../../services/getAPIs';
import { Agency } from '../../../../Context/Context'
import './transaction.style.scss'
import TSTep1 from './TSTep1';
import TStep2 from './TStep2';
import TStep3 from './TStep3';
export default function Transaction({ type, handleBySupport }) {
    const agency = React.useContext(Agency);
    const [tStep, setTStep] = React.useState(0);
    const [coinSelected, setCoinSelected] = React.useState(null)
    const { mainBalance } = agency;
    const [selectedTransaction, setSelectedTransaction] = React.useState({});

    const [coinList, setCoinList] = React.useState([]);

    const selectComponent = () => {
        switch (tStep) {
            case 0: return <TSTep1 handleCoinClick={handleCoinClick} coinList={coinList} />
            case 1: return <TStep2 onClick={displayData} coinList={coinList} />;
            case 2: return <TStep3 coinSelected={coinSelected} handleBySupport={() => handleBySupport(selectedTransaction._id)} selectedTransaction={selectedTransaction} />
            case 5: return <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation type="login" size={{ height: 200, width: 200 }} />
            </div>
        }
    }
    const handleCoinClick = async (obj) => {
        console.log("coin selected", obj);
        setCoinSelected(obj)
        setTStep(5)
        let res = await handleWithdrawalList(obj.coinSymbol);
        setCoinList([...res.data.txns])
        setTStep(1)

    }
    const displayData = async (obj) => {
        setTStep(5)
        let res = await fetchTransactionDetails(obj._id)
        setSelectedTransaction(res.data.txn);
        setTStep(2)

    }

    React.useEffect(() => {
        setCoinList([...mainBalance.fiat, ...mainBalance.crypto])
    }, [mainBalance])

    return (
        <div className="transaction-main">
            {selectComponent()}
        </div>
    )
}
