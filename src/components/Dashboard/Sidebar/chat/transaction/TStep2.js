import React from 'react'
import nextId from 'react-id-generator';
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import { Agency } from '../../../../Context/Context'

export default function TStep2({ coinList, onClick }) {
    const agency = React.useContext(Agency);
    const { currencyImageList, currentApp } = agency;


    return (
        <div className="transaction-step-one">
            <h6 className="t-step-one-title">
                Select Any Transaction
        </h6>
            <div className="coins-wrapper">
                {
                    !coinList.length ?
                        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                            <LoadingAnimation type="no-data" size={{ height: 150, width: 150 }} />
                        </div>
                        :
                        coinList.filter(x => { return x.app_code === currentApp.app_code })
                            .map(obj => <div key={obj._id + nextId()} onClick={() => onClick(obj)} className="t-coins-row">
                                <img src={currencyImageList[obj.coin]} />
                                <div>
                                    <h6>ID: {obj._id}</h6>
                                    <span>{obj.date}</span>
                                </div>
                            </div>
                            )
                }
            </div>

        </div>
    )
}
