import React from 'react'
import Images from '../../../../../assets/a-exporter'

export default function SCPFinish() {
    return (
        <div className="scp-finish-main">
            <img src={Images.chatsIOFull} />
            <span>Finishing Setup....</span>
        </div>
    )
}
