import React from 'react'
import Images from '../../../../../assets/a-exporter'
import NextSVG from '../../../../common-components/assetsSection/NextSVG'

export default function SCPHome({setSteps}) {
    
    return (
        <div className="scp-home-main">
            <div/>
            <div>
                <img src={Images.chatsIOFull} loading="lazy" />
                <span>Its Your First TIme Using Chats.io</span>
            </div>
            <button onClick={()=>setSteps(1)}>
                Let's Get You Setup <NextSVG />
            </button>
        </div>
    )
}
