import React from 'react'
import SCPFinish from './SCPFinish';
import SCPHome from './SCPHome';
import SCPSteps from './SCPSteps';
import './setup-chat-profile.style.scss'
import { Agency } from '../../../../Context/Context';
import Images from '../../../../../assets/a-exporter';
export default function SetupChatProfile({ handleNewSignup, setVisibleMoreInfo, visibleMoreInfo }) {
    const agency = React.useContext(Agency);
    const { currentUserDetails } = agency
    const [step, setSteps] = React.useState(0);
    const [details, setDetails] = React.useState({
        firstName: '',
        lastName: '',
        profile_img: !currentUserDetails.profile_img ? Images.face : currentUserDetails.profile_img,
        bio: ''
    })
    const handleSubmit = () => {
        setSteps(2)
        handleNewSignup(details);

    }
    const selectStep = () => {
        switch (step) {
            case 0: return <SCPHome setSteps={setSteps} />;
            case 1: return <SCPSteps handleSubmit={handleSubmit} setDetails={setDetails} details={details} setSteps={setSteps} />;
            case 2: return <SCPFinish setDetails={setDetails} />
        }
    }
    React.useEffect(() => {
        if (visibleMoreInfo) return;
        setVisibleMoreInfo(false)


    }, [visibleMoreInfo])
    return (
        <div className="setup-chat-profile">
            {
                selectStep()
            }

        </div>
    )
}
