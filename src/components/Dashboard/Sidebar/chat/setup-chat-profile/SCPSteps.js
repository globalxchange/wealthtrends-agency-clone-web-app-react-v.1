import React from 'react'
import Images from '../../../../../assets/a-exporter';
import { Agency } from '../../../../Context/Context';

export default function SCPSteps({ setDetails, details, handleSubmit, setSteps }) {
    const [miniSteps, setMiniSteps] = React.useState(0);
    const [localData, setLocalData] = React.useState(null)
    const [enableUpload, setEnableUpload] = React.useState(false)
    const [uploading, setUploading] = React.useState(false)
    const [preview, setPreview] = React.useState('')
    const agency = React.useContext(Agency);
    const { currentUserDetails, globalHandleFinalImage, localImage, resetImage } = agency;

    const selectFormStep = () => {
        switch (miniSteps) {
            case 0: return <input value={details.firstName} autofocus placeholder="Ex. Steven" onChange={e => setDetails({ ...details, firstName: e.target.value })} className="scp-one" />
            case 1: return <input value={details.lastName} autofocus placeholder="Ex. Smith" onChange={e => setDetails({ ...details, lastName: e.target.value })} className="scp-one" />
            case 2: return <textarea value={details.bio} autofocus placeholder="About You" onChange={e => setDetails({ ...details, bio: e.target.value })} className="scp-two" />
            case 3: return <div className="scp-three">
                <label htmlFor="profile-pic">
                    {
                        enableUpload ? <input onChange={e => { setUploading(true); globalHandleFinalImage(e) }} id="profile-pic" type="file" className="d-none" /> : ''
                    }
                    <img src={enableUpload ? !preview ? Images.fileUpload : preview : details.profile_img} />
                </label>
                <div>
                    <h5>{details.firstName + ' ' + details.lastName}</h5>
                    <span>{details.email}</span>
                </div>
            </div>
            // URL.creatObjectURL()
        }
    }
    const selectText = () => {
        switch (miniSteps) {
            case 0: return "Enter Your First Name"
            case 1: return "Enter Your Last Name"
            case 2: return "Enter Your Bio"
            case 3: return "Do You Want To Change Your Profile Picture?"
        }
    }
    React.useEffect(() => {
        if (!localImage) return;
        setDetails({ ...details, profile_img: localImage });
        resetImage();
        setEnableUpload(false)
        setUploading(false);
        setLocalData(null)
        setPreview();

    }, [localImage])
    const onHandleBackClick = () => {
        switch (miniSteps) {
            case 0: setSteps(0); setDetails({ ...details, firstName: '' }); break;
            case 1: setMiniSteps(0); setDetails({ ...details, lastName: '' }); break;
            case 2: setMiniSteps(1); setDetails({ ...details, bio: '' }); break;
            case 3: if (enableUpload) {
                setLocalData(null)
                setPreview();
                setDetails({ ...details, profile_img: currentUserDetails.profile_img });
                setEnableUpload(false);
            } else {
                setEnableUpload(true)
            }
                break;


        }

    }
    const onHandleFrontClick = () => {
        switch (miniSteps) {
            case 0: setMiniSteps(1); break;
            case 1: setMiniSteps(2); break;
            case 2: setMiniSteps(3); break;
            case 3: if (enableUpload) {
                setUploading(true)
                globalHandleFinalImage(localData)
            } else {
                handleSubmit()
            }
                break;
        }
    }

    const checkFrontDisable = () => {
        switch (miniSteps) {
            case 0: return !details.firstName;
            case 1: return !details.lastName;
            case 2: return !details.bio;
            case 3: if (enableUpload) {
                return localData === null

            } else {


            }
        }

    }
    return (
        <div className="scp-steps-main">
            <div className="scp-steps-header">
                <img src={Images.chatsIOFull} />
                <span>{selectText()}</span>
            </div>
            {selectFormStep()}
            <div className="scp-steps-footer">
                <button onClick={() => onHandleBackClick()}>{miniSteps === 3 ? enableUpload ? "Back" : "Upload New" : "Back"}</button>
                <button disabled={checkFrontDisable()} onClick={() => onHandleFrontClick()}>{miniSteps === 3 ? enableUpload ? uploading ? "Uploading...." : "Upload" : "Keep This" : "Next Step"}</button>
            </div>
        </div>
    )
}
