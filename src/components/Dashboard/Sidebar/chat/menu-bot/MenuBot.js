import React from 'react'
import nextId from 'react-id-generator';
import './menu-bot.style.scss'
import Images from '../../../../../assets/a-exporter';
import ChatHeaderBot from '../ChatHeaderBot';
export default function MenuBot({ contentStep,selectContent,setWhichTrade, displayOverlay, botController, transactionDeposit, transactAddCrypto, handleClickOnOptions, botSelections, handleBotHeader, botHeader }) {
    return (
        <div className="menu-bot-main">
            <div className={contentStep === 3 ? "d-none" : "uc-body-options"}>
                {
                    controller.map((obj) => (
                        <button
                            key={obj.keyId}
                            onClick={() => handleBotHeader(obj)}
                            className={obj.id === botHeader.id ? "option-selected" : ""}
                        >
                            <img height="20px" src={obj.icon} />
                            {obj.name}
                        </button>
                    ))
                }
            </div>
            <div className={`uc-body-option-expanded ${contentStep === 3 || transactionDeposit || transactAddCrypto ? "no-height" : ""}`}>
                <ChatHeaderBot
                setWhichTrade={setWhichTrade}
                    botController={botController}
                    botHeader={botHeader}
                    botSelections={botSelections}
                    handleClickOnOptions={handleClickOnOptions}
                />

            </div>
            <div className={
                `uc-body-chat-box ${transactionDeposit || transactAddCrypto ? "transaction-height" : contentStep === 3 ? "full-height" : ""}`
            }>
                <div
                    className={displayOverlay || botSelections.sub === "trade" ? "chat-overlay-content" : "d-none"}
                >
                    {
                        selectContent()
                    }
                </div>

            </div>

        </div>
    )
}
const controller = [
    { keyId: nextId(), name: "Actions", id: "transact", icon: Images.actionButton },
    { keyId: nextId(), name: "Transactions", id: "transaction", icon: Images.transactionButton },
    { keyId: nextId(), name: "Learn", id: "learn", icon: Images.learnButton },
];