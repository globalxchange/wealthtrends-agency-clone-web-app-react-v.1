import React from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../../assets/a-exporter';
import NextSVG from '../../../../common-components/assetsSection/NextSVG';

export default function OverlayOther({handleOverlayClick}) {
    return (
        <div className="w-100 h-100 overlay-other">
            <div className="m-o-w-search">
        <div>
          <NextSVG type="search" color="#bbbbbb" />
          <input placeholder="What Do You Want To Do?" />
          <img src={Images.scanner} />

        </div>
      </div>
      <div className="m-o-w-body">
        {
          overlayList.map(obj => <div
            onClick={() => { handleOverlayClick(obj) }}
            className={obj.disable ? "disable-this" : ""} key={obj.keyId}>
            <h6>{obj.text}</h6>
          </div>)
        }


      </div>
            
        </div>
    )
}

const overlayList = [
    { keyId: nextId(), text: "Speak To Support", id: "support", disable: false },
    { keyId: nextId(), text: "Manage Contacts", id: "contacts", disable: true },
    { keyId: nextId(), text: "Schedule A Meeting", id: "meeting", disable: true },
    { keyId: nextId(), text: "Chats.io MarketPlace", id: "marketPlace", disable: true },
    { keyId: nextId(), text: "Edit Profile", id: "profile", disable: false },
  ]