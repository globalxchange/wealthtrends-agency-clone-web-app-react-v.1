import React from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../../assets/a-exporter';

export default function OverlayCategory() {
    return (
        <div className="overlay-category">
            <div className="o-c-featured-sections">
                <div className="o-c-f-s-header">
                    <h6>Featured Sections</h6> 

                </div>
                <div className="o-c-f-s-body">
                    <div>
                        <h2>A</h2>
                        <p>All</p>
                    </div>

                </div>

            </div>
            <div className="o-c-all-categories">
                <div className="o-c-a-c-header">
                    <h6>All Categories</h6>
                    

                </div>
                <div className="o-c-a-c-body">
                    {
                        categories.map(obj =><div>
                            <img src={obj.icon} />
                            <h6>{obj.name}</h6>
                        </div>)
                    }

                </div>


            </div>
            
        </div>
    )
}
const categories = [
    {keyId: nextId(), name: "Friends", icon: Images.face},
    {keyId: nextId(), name: "Friends", icon: Images.learnArticle},
    {keyId: nextId(), name: "Friends", icon: Images.learnVideos},
]