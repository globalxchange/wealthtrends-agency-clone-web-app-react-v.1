import React from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../../assets/a-exporter'
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import { getUploadedFile } from '../../../../../services/getAPIs';
import { uploadDocuments } from '../../../../../services/postAPIs';
import { Agency } from '../../../../Context/Context'

export default function TransactionSteps({ currentStep, selectedOptions, setStep }) {

    const agency = React.useContext(Agency);
    const { imageUploader, localImage, resetImage } = agency
    const [files, setFiles] = React.useState([])
    const [currentFile, setCurrentFile] = React.useState(null)
    const [fileName, setFileName] = React.useState('');
    const [overlay, setOverlay] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [fileDone, setFileDone] = React.useState(false)
    const [uploadIcon, setUploadIcon] = React.useState('');
    const [mainLoading, setMainLoading] = React.useState(false)
    const [selectedButton, setSelectedButton] = React.useState(null);

    const resetAll = () => {
        setCurrentFile(null);
        setFileName('');
        setOverlay(false)
        setLoading(false)
        setFileDone(false)
        setUploadIcon('')
        setMainLoading(false)

    }

    const handleExtraButton = (obj) => {
        setSelectedButton(obj.name);
        if (obj.id === "upload") {
            setOverlay(true)
        } else {
            setOverlay(false)
        }
    }
    const handleUpload = () => {
        dataURLtoFile(uploadIcon, "newImage.jpg")
    }

    const handleImageChange = (e) => {
        setLoading(true)
        if (!e.target.files[0]) {
            return
        }
        e.preventDefault();
        setFileName(e.target.files[0])
        const fileReader = new FileReader()
        fileReader.onloadend = () => {
            setUploadIcon(fileReader.result);
        }
        fileReader.readAsDataURL(e.target.files[0])
    }
    React.useEffect(() => {
        if (!uploadIcon) return
        else handleUpload()
    }, [uploadIcon])

    const dataURLtoFile = (dataurl, filename) => {
        let arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);

        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        let croppedImg = new File([u8arr], filename, { type: mime });
        let sendImage = croppedImg;
        imageUploader(sendImage);
    }

    const handleSendFile = async () => {
        setMainLoading(true)
        let obj = {
            file_key: currentFile,
            id: currentStep?._id,
            input: currentStep?.needs_action_value

        }
        let res = await uploadDocuments(obj)
        if (res.data.status) {
            setFileDone(true)
            setFiles([currentFile, ...files])
            setCurrentFile(null)
        } else {
            setFileDone(false)
            setCurrentFile(null)
        }
        setMainLoading(false)

    }
    const setUpFiles = async () => {
        let res = await getUploadedFile(currentStep._id);
        if (res.data.status) {
            setFiles([...res.data.result])
        } else {
            setFiles([])
        }

    }
    React.useEffect(() => {
        if (!localImage)
            return
        setCurrentFile(localImage)
        setFiles([...files, localImage])
        // setDetails({ ...details, icon: localImage });
        setLoading(false)
        resetImage();
        setUploadIcon('')

    }, [localImage]);

    React.useEffect(() => {
        setUpFiles()

    }, [])

    return (
        mainLoading ?
            <LoadingAnimation />
            :
            <div className="transaction-step-current">
                <div className="tsc-header">
                    {
                        selectedOptions.map(obj => <img src={obj.icon} />)
                    }

                </div>
                <div onClick={() => setStep(3)} className="tsc-body">
                    <img src={currentStep?.current_step_data?.thumbnail} />
                </div>
                <div onClick={() => setStep(3)} className="tsc-footer">
                    <div>
                        <img src={currentStep?.current_step_data?.icon} />
                    </div>
                    <div>
                        <h4>{currentStep?.current_step_data.status}</h4>
                        <span>Current Step</span>
                    </div>

                </div>
                <div className="tsc-extra-buttons">
                    {
                        extraButtons.map(obj =>
                            <h6 onClick={() => handleExtraButton(obj)}><img src={obj.icon} />{obj.name}</h6>)
                    }

                </div>
                <div onClick={() => { setOverlay(false); setCurrentFile(null) }} className={overlay ? "transaction-step-overlay" : "d-none"}>
                    <div onClick={(e) => e.stopPropagation()} className="ts-overlay-body">
                        <div className="ts-overlay-header">
                            <img src={Images.serverLogo} />
                    Transaction Cloud
                    </div>
                        <p>
                            {
                                fileDone ?
                                    `Congragulations Your File Was Uploaded  To This Transaction Cloud. ${<b onClick={() => resetAll()}>Click Here</b>} To Return To Transaction Feed` :
                                    "Welcome To The Transaction Cloud. Here You Cad Upload All Relevant Documents Relating To This Ongoing Transaction. The Document Requirements Are Outlined In The Payment Path Steps. Additionally You May Be Requested To Provide Further Documentation By The Support Team."
                            }
                        </p>

                        <div className="file-wrapper">
                            <label htmlFor="cover-photo" className={!currentFile ? "add-file" : "d-none"}>
                                {
                                    loading ?
                                        <LoadingAnimation size={{ height: 80, width: 80 }} />
                                        :
                                        <>
                                            <img src={Images.addDark} />
                                            <input
                                                className="d-none"
                                                type="file"
                                                id="cover-photo"
                                                onChange={e => handleImageChange(e, true)}
                                            />
                                        </>
                                }
                            </label>
                            {
                                files.map((obj, num) => <div className="other-files">
                                    <h2>{num + 1}</h2>
                                </div>)
                            }


                        </div>
                    </div>
                    <div className={!currentFile ? "d-none" : "ts-overlay-footer"}>
                        <div className={!currentFile ? "d-none" : "filename"}>
                            <button>
                                <img src={Images.replaceFile} />
                            </button>
                            <input placeholder="Enter Filename" />
                            <button onClick={() => handleSendFile()}>
                                <img src={Images.sendFile} />
                            </button>
                        </div>
                    </div>
                </div>

            </div>
    )
}
const extraButtons = [
    { keyId: nextId(), name: "Upload Receipt", id: "upload", icon: Images.fileUpload },
    { keyId: nextId(), name: "Add Direction", id: "add", icon: "" },
    { keyId: nextId(), name: "Terminate", id: "terminate", icon: Images.transactionCancelled },
]