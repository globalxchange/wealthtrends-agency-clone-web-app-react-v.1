import React from 'react'
import Constant from '../../../../../json/constant';
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import { getTransactionDepositList } from '../../../../../services/getAPIs';
import { Agency } from '../../../../Context/Context';
import './transaction-step.style.scss'
import TransactionDescribe from './TransactionDescribe';
import TransactionList from './TransactionList';
import TransactionSteps from './TransactionSteps';
import TStepper from './TStepper';
export default function TransactionStep({ botController, setStep, step }) {
    const agency = React.useContext(Agency);
    const { setTransactionDeposit, mainBalance } = agency;
    const [currentSteps, setCurrentSteps] = React.useState([]);
    const [activeStep, setActiveStep] = React.useState(0);
    const [currentStep, setCurrentStep] = React.useState()
    const [transactionList, setTransactionList] = React.useState([])
    const [selectedOptions, setSelectedOptions] = React.useState([Constant.botTransaction.list[0]])
    const [details, setDetails] = React.useState({
        type: "deposit",
        asset: null,
        step: null,
        coin: null
    })
    const [previouslySelectedId, setPreviouslySelectedId] = React.useState("deposit");


    React.useEffect(()=>{
        if(step === 0){
            setActiveStep(0);
            setPreviouslySelectedId("deposit")
            setUpCurrentStep(Constant.botTransaction, Constant.transactionAssets)
            setTransactionList([])
            setSelectedOptions([Constant.botTransaction.list[0]])
            setDetails({
                type: "deposit",
                asset: null,
                step: null,
                coin: null
        
            });

        }
    },[step])
    React.useEffect(() => {
        setUpCurrentStep(Constant.botTransaction, Constant.transactionAssets)

    }, [])
    const selectComponent = () => {
        switch (step) {
            case 0: return <TStepper handleBackSteps={handleBackSteps} previouslySelectedId={previouslySelectedId} handleSteps={handleSteps} currentSteps={currentSteps} />;
            case 1: return <TransactionList setStep={setStep} setCurrentStep={setCurrentStep} setCurrentStep={setCurrentStep} details={details} transactionList={transactionList} />;
            case 2: return <TransactionSteps selectedOptions={selectedOptions} setStep={setStep} currentStep={currentStep} />;
            case 3: return <TransactionDescribe selectedOptions={selectedOptions} currentStep={currentStep.current_step_data} />;
            case 4: return <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />
            </div>
        }
    }
    const handleBackSteps = () => {
        selectedOptions.pop();
        setSelectedOptions([...selectedOptions])
        
        switch (activeStep) {
            case 0: setTransactionDeposit(false); break;
            case 1: setPreviouslySelectedId("deposit"); setDetails({ ...details, step: null }); setActiveStep(0); setUpCurrentStep(Constant.botTransaction, Constant.transactionAssets); break;
            case 2: setPreviouslySelectedId(details.asset.toLowerCase()); setDetails({ ...details, coin: null }); setActiveStep(1); setUpCurrentStep(Constant.transactionAssets, Constant.transactionStatus); break;

        }
    }

    const handleSteps = (obj) => {
        setPreviouslySelectedId(obj.id)
        setSelectedOptions([...selectedOptions, obj])
        switch (activeStep) {
            case 0: setDetails({ ...details, asset: obj.name }); setUpCurrentStep(Constant.transactionAssets, Constant.transactionStatus); setActiveStep(activeStep + 1); break;
            case 1: setDetails({ ...details, step: obj.name });selectCoins(details.asset);
                // setUpCurrentStep(Constant.transactionStatus, Constant.transactionCurrency);
                setActiveStep(activeStep + 1); break;
            case 2: setDetails({ ...details, coin: obj.name }); setStep(4); setUpTransactionList(obj.name); break;
        }
    }
    const selectCoins = (coin) =>{
        let tempArray = []
        if(coin === "Fiat"){
            tempArray = mainBalance.fiat.map(obj => {return {...obj, name: obj.coinSymbol, icon: obj.coinImage}})
        }else{
            tempArray = mainBalance.crypto.map(obj => {return {...obj, name: obj.coinSymbol, icon: obj.coinImage}})
        }
        setUpCurrentStep(Constant.transactionStatus,{...Constant.transactionCurrency, list: [...tempArray]})
    }


    const setUpTransactionList = async (coin) => {
        let res = await getTransactionDepositList( details.step.toLowerCase(), coin);
        setTransactionList(!res.data?.deposits.length ? [] : res.data?.deposits[0]?.txns);
        setStep(1)
    }

    const setUpCurrentStep = (first, second) => {
        let tempArray = [
            { ...first },
            { ...second }
        ]
        setCurrentSteps([...tempArray]);
    }

    return (
        <div className="transaction-step">
            {
                selectComponent()
            }

        </div>
    )
}
