import React from 'react'
import nextId from 'react-id-generator';
import Constant from '../../../../../json/constant';
import DescribeStep from '../../../Content/InstaCrypto/side-panel/DescribeStep'

export default function TransactionDescribe({ currentStep,selectedOptions, institute }) {
    return (
        <div className="transaction-describe">
            <div className="td-history">
                {
                    selectedOptions.map(obj => <img key={obj.icon + nextId()} src={obj.icon} />)
                }
            </div>
            <div className="td-cover-pic">
                <img src={currentStep.thumbnail} className="td-cover-cover" />
                <img src={currentStep.icon} className="td-cover-icon" />
            </div>
            <div className="td-information">
                <h5>{currentStep.name}</h5>
                <h6>{
                    Constant.instaContactList.map(obj => <img key={obj.keyId} src={obj.icon} />)
                }</h6>
                <p>
                    {currentStep.description}
                </p>

            </div>
            <div className="td-relational-institution">
                <h6 className="td-ri-title">Relational Institution</h6>
                <div className="td-ri-row">
                    <div className="td-ri-row-left">

                    </div>
                    <div className="td-ri-row-right">
                        <div>
                            <h6>{!institute?.institute_name}</h6>
                            <span>{institute?.country_name}</span>
                        </div>
                        <p>More About The Involvement Of This Institution</p>

                    </div>

                </div>

            </div>
            <div className="td-screenshots">
                <h6 className="td-s-title">Screenshots</h6>
                <div className="td-s-gallery">
                    {
                        [1,1,1].map(obj =><span></span>)
                    }
                    <span />
                </div>
            </div>
        </div>
    )
}
