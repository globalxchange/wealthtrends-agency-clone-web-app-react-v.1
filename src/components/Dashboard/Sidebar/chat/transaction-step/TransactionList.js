import React from 'react'
import nextId from 'react-id-generator';
import Images from '../../../../../assets/a-exporter'
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import { Agency } from '../../../../Context/Context'

export default function TransactionList({ transactionList, setCurrentStep, setStep, details }) {
    const agency = React.useContext(Agency);
    const { currencyImageList } = agency;
    return (
        <div className="transaction-list">
            <div className="transaction-list-header">
                <span>
                    <img src={currencyImageList[details.coin]} />
                    {details.coin}
                </span>
                <span>{transactionList.length} Transactions</span>
            </div>
            <div className="transaction-list-body">
                {
                    !transactionList.length ?
                        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                            <LoadingAnimation type="no-data" />
                        </div>
                        :
                        transactionList.map(obj => <div key={obj._id + nextId()} onClick={() => { setCurrentStep(obj); setStep(2) }} className="transaction-card">
                            <div className="t-card-body">
                                <div>
                                    <h5>{obj.cus_credit_value}</h5>
                                    <span>Initiated On {obj.date}</span>
                                </div>
                                <button>Current Step</button>

                            </div>
                            <div className="t-card-footer">
                                <img src={Images.bitcoin} />
                                <span>{obj.banker}</span>
                            </div>

                        </div>
                        )
                }

            </div>

        </div>
    )
}
