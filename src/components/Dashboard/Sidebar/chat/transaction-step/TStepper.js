import React from 'react'

export default function TStepper({ handleSteps, handleBackSteps, previouslySelectedId, currentSteps }) {
    return (
        <div className="t-stepper">
            {
                currentSteps.map((obj, i) => <div key={obj.keyId} className="option-wrapper">
                    {i ? <h6>{obj.title}</h6> : ''}
                    <div className="options-row">
                        {
                            obj.list.map(x =>
                                <div
                                    key={obj.keyId}
                                    onClick={() => i ? handleSteps(x) : handleBackSteps()}
                                    className={i ? ["cancelled", "completed"].includes(x.id) ? "disable" : "" : previouslySelectedId === x.id ? "selected" : "disable"}>
                                    <img src={x.icon} />
                                    <span>{x.name}</span>
                                </div>
                            )
                        }
                    </div>

                </div>
                )
            }

        </div>
    )
}
