import React from 'react'
import Images from '../../../../assets/a-exporter'

export default function ChatHeaderHuman({ user }) {
    return (
        <div className="chat-header-human">
            <div className="human-chat-description">
                <div>
                    <img src={Images.coolEmoji} />
                </div>
                <div>
                    <h6>Hey {user.username}</h6>
                    <p>
                        My name is CryptoCam and I am the world’s first interactive financial assistant.
                        You can talk to me anytime between 10 AM to 6 PM EST Monday through Friday.
                        I will try my best to respond outside of those hours as well.
                    </p>
                </div>

            </div>
            {/* <div onClick={() => { setDisplayOverlay(true); setContentStep(0); setDisplayUserInfo(true) }} className="human-chat-dropdown">
                <h6>See Account Details</h6>
                <img className={displayUserInfo ? "" : "rotate-it"} src={Images.triangle} />
            </div> */}

        </div>
    )
}
