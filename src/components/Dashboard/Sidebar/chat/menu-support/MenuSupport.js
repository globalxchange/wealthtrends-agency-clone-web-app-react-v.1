import React, { useEffect, useContext, useState, useRef } from 'react'
import ChatHeaderHuman from '../ChatHeaderHuman'
import SetupChatProfile from '../setup-chat-profile/SetupChatProfile'
import './menu-support.style.scss'
import ioClient from "socket.io-client";
import axios from "axios";
import UserProfile from '../user-profile/UserProfile';
import { Agency } from '../../../../Context/Context';
import Constant from '../../../../../json/constant';
export default function MenuSupport({
    setStepNumber,
    stepNumber,
    botSelections,
    // handleFileChange,
    ChatBox,
    botHeader,
    setShowChatBox,
    // pasteImageHandler,
    displayUserInfo,
    showChatBox,
    visibleMoreInfo,
    setVisibleMoreInfo,
    handleNewSignup,
    setSelections,
    setBotController,
    refresh

}) {
    const socketRef = useRef();
    const userSocketRef = useRef();
    const agency = useContext(Agency)
    const {
        setPreImageUpload, imageResult, localImage, resetImage, setRefreshApp,
        supportMessage, setSupportMessage,mainAppId,changeNotification
    } = agency

    const [sendingImage, setSendingImage] = React.useState("");
    const [threadId, setThreadId] = useState("");
    const [selectedGroup, setSelectedGroup] = useState("");
    const [messageArray, setMessageArray] = useState([]);
    const [groupReadUnreadList, setGroupReadUnreadList] = useState([]);
    const [message, setMessage] = useState("");
    const [chatSection, setChatSection] = useState(false);
    const [typingFlag, setTypingFlag] = useState(false);
    const [typingMessage, setTypingMessage] = useState("");
    const [globalUserObj, setGlobalUserObj] = useState(null);
    const [userObj, setUserObj] = useState(null);

    useEffect(() => {
        // console.log(sendingImage);
        if (!sendingImage) {
            return;
        } else {
            dataURLtoFile(sendingImage, "newImage.jpg");
        }
    }, [sendingImage]);

    const dataURLtoFile = (dataurl, filename) => {
        // console.log("dataurl", dataurl);
        alert()
        let arr = dataurl.split(","),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);

        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        let croppedImg = new File([u8arr], filename, { type: mime });
        // console.log("cropped  in", croppedImg);
        let sendImage = croppedImg;
        setPreImageUpload({ status: true, url: dataurl, image: sendImage });
    };

    const handleBySupport = () => {
        let text = !supportMessage
            ? `I need to you to do a ${botSelections.sub.toUpperCase()}`
            : `Hey, I am having issues with transaction ID: ${supportMessage}`;
        setShowChatBox(true);
        setSupportMessage(null)

        // setDisplayOverlay(true);
        setMessage(text);
        let a = setTimeout(() => {
            handleSubmitMessage(text, true);
            clearTimeout(a);
        }, 150);
    }
    React.useEffect(() => {
        if (supportMessage === null) return;
        if (!messageArray.length) return
        alert(messageArray.length)
        handleBySupport()
    }, [supportMessage, messageArray])

    React.useEffect(() => {
        if (visibleMoreInfo) return
        if (imageResult === null) return;
        if (imageResult) {
            sendImageAsMessage();
        } else {
            alert("failed");
        }
    }, [imageResult]);

    const sendImageAsMessage = () => {
        let message_data = {
            message: Date.now().toString(),
            thread_id: threadId,
            sender: userObj?.username,
            timestamp: Date.now(),
            filename: Date.now().toString(),
            type: "file",
            location: localImage,
        };
        let notification_data = {
            sender: userObj?.username,
            receiver: selectedGroup,
            group: true,
            group_type: "group",
        };

        socketRef.current.emit("new_message", notification_data);

        socketRef.current.emit(
            "new_support_group_message",
            message_data,
            mainAppId,
            (response) => {
                try {
                    if (response !== "success") {
                        message.error("Error in sending message");

                        setPreImageUpload({ status: false, url: null, image: "" });
                        resetImage();
                    } else {
                        // console.log("Image Data", message_data, notification_data, messageArray)
                        setMessageArray([...messageArray, { ...message_data }]);
                        setPreImageUpload({ status: false, url: null, image: "" });
                        resetImage();
                    }

                } catch (e) {
                    console.log(e)
                }
            }
        );
    };
    const getAdminsAndCheck = () => {
        // console.log("chat user", agency.currentUserDetails);
        socketRef.current.emit(
            "get_user_support_group_interaction_list",
            userObj,
            mainAppId,
            (response) => {
                // console.log(response, "kjehbvkejrbveq");
                if (!response[0]) return
                setSelectedGroup(response[0]?.group_name);
                setGroupReadUnreadList([...response]);
                socketRef.current.emit(
                    "get_support_group_chat_history",
                    response[0].group_name,
                    response[0].thread_id,
                    mainAppId,
                    (socketResp) => {
                        setThreadId(response[0].thread_id);
                        setChatSection(true);
                        setMessageArray([...socketResp.reverse()]);
                        // console.log("chat response messages", socketResp[0], socketResp);
                    }
                );
            }

        );
    };
    const getPreviousMessages = () => {
        socketRef.current.emit(
            "oneone_older_messages",
            JSON.stringify({ thread_id: threadId, page: stepNumber }),
            (response) => {
                if (response !== "error") {
                    let chat_history = [...response].reverse();

                    console.log("chat_history", response, stepNumber);
                    /* To know if you have more old messages or not 
                    see the note below and use the code here */

                    setMessageArray([...chat_history, ...messageArray]);
                } else {
                    message.error("Fetching older messages failed", 3);
                }
            }
        );
    };
    useEffect(() => {
        if(userObj === null)return;
        let email = localStorage.getItem("userEmail");
        let token = localStorage.getItem("idToken");
        socketRef.current = ioClient("https://testsockchatsio.globalxchange.io", {
            query: {
                email: email,
                token: token,
                tokfortest: "nvestisgx",
            },
        });
        userSocketRef.current = ioClient(
            "https://testsockchatsio.globalxchange.io/user",
            {
                query: {
                    email: email,
                    token: token,
                    tokfortest: "nvestisgx",
                },
            }
        );
        getAdminsAndCheck();
        return () => {
            userSocketRef.current.off("get_online_user_list");
        };
    }, [userObj]);

    useEffect(() => {
        if(!userSocketRef.current)return
        userSocketRef.current.on("new_message_notify", (data) => {
            // console.log("chat response online", data, groupReadUnreadList);
            if (data.group) {
                let temp = [...groupReadUnreadList];
                const objIndex = temp.findIndex(
                    (obj) => obj.group_name === data.receiver
                );
                if (objIndex === -1) return
                temp[objIndex].sender = data?.sender;

                let newTemp = [...temp.filter((o) => o.group_name === data.receiver)];
                temp = temp.filter((obj) => obj.group_name !== data.receiver);
                newTemp = [...newTemp, ...temp];
                setGroupReadUnreadList([...newTemp]);
                // console.log("chat message group texts", newTemp);
            }
        });
        return () => {
            userSocketRef.current.off("new_message_notify");
        };
    }, [groupReadUnreadList]);

    useEffect(() => {
        if(!socketRef.current)return
        socketRef.current.on("msg_notify", (data) => {
            if (data.thread_id === threadId) {
                if (messageArray[messageArray.length - 1] !== data) {
                    setMessageArray([...messageArray, data]);
                    // console.log("chat messages chat 2", data);
                }
            }
        });
        return () => {
            socketRef.current.off("msg_notify");
        };
    }, [messageArray, threadId]);

    useEffect(() => {
        if(!socketRef.current)return
        socketRef.current.on("someone_typing", (typingUser, socketThreadId) => {
            if (socketThreadId === threadId) {
                setTypingMessage(`${typingUser} is typing`);
                setTypingFlag(true);
                setTimeout(() => {
                    setTypingFlag(false);
                }, 1800);
            } else {
                setTypingFlag(false);
            }
        });
        return () => {
            socketRef.current.off("someone_typing");
        };
    }, [threadId]);

    React.useEffect(() => {
        checkUserInApp();
    }, [])

    const checkUserInApp = () => {
        axios
            .post(
                `https://testchatsioapi.globalxchange.io/get_user`,
                {
                    email: localStorage.getItem("userEmail"),
                    app_id: mainAppId,
                },
                {
                    headers: {
                        email: localStorage.getItem("userEmail"),
                        token: localStorage.getItem("idToken"),
                        app_id: mainAppId,
                    },
                }
            )
            .then((res) => {

                if (res.data === "Auth Failed") {
                    setSelections({ main: botHeader.id, sub: "refresh" });
                    setBotController({ ...Constant.refreshList });
                    setVisibleMoreInfo(false);
                    setRefreshApp(true); refresh(false);
                    return
                }
                console.log(res.data, "hahahah");
                if (!res.data.status) {
                    axios
                        .get(
                            `https://comms.globalxchange.com/user/details/get?email=${localStorage.getItem(
                                "userEmail"
                            )}`
                        )
                        .then((res) => {
                            // setGlobalUserObj(res.data.user);
                            if (res.data.user.profile_img !== "" &&
                                res.data.user.first_name !== "" &&
                                res.data.user.last_name !== "" &&
                                res.data.user.bio !== "") {
                                handleSignupWithOldData(res.data.user);
                                setGlobalUserObj(res.data.user);
                            } else {
                                let t2 = localStorage.getItem("appId")
                                if (t2 === "undefined") {

                                } else {
                                    setVisibleMoreInfo(true);

                                }
                            }
                        });
                    // getDefaultAppApp();
                } else {
                    console.log("user info", res.data.payload)
                    setUserObj(res.data.payload);
                }
            });
    };
    const handleSubmitMessage = (mess, stat) => {
        let messageIn = stat ? mess : message;
        if (messageIn.length > 0) {
            const temp = [...groupReadUnreadList];
            const objIndex = temp.findIndex(
                (obj) => obj.group_name === selectedGroup
            );
            // temp[objIndex].last_messaged_user = userObj.username;
            // setGroupReadUnreadList(temp);
            const notificationData = {
                sender: userObj,
                receiver: selectedGroup,
                group: true,
            };
            userSocketRef.current.emit("new_group_message", notificationData);
            socketRef.current.emit(
                "new_support_group_message",
                {
                    message: messageIn,
                    thread_id: threadId,
                    sender: userObj,
                    timestamp: Date.now(),
                },
                mainAppId,
                (response) => {
                    if (response === "success") {
                        setMessageArray([
                            ...messageArray,
                            {
                                message: messageIn,
                                threadId,
                                sender: userObj,
                                timestamp: Date.now(),
                            },
                        ]);
                        setMessage("");
                    } else {
                    }
                }
            );
        } else {
        }
    };

    const handleSignupWithOldData = (item) => {
        axios
            .post(`https://testchatsioapi.globalxchange.io/register_with_chatsio`, {
                first_name: item.first_name, // required
                last_name: item.last_name,
                username: item.username, // required
                bio: item.bio,
                email: item.email, // required
                timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                avatar: item.profile_img,
            })
            .then((res) => {
                if (res.data.status) {
                    registerUserToApp(item.email);
                } else {
                    if (res.data.payload === "Email Already Registered") {
                        registerUserToApp(item.email);
                    } else {
                        // antmsg.error("Registering User with Chats.io Failed");
                    }
                }
            });
    };
    const registerUserToApp= (email) =>{
        axios.post(`https://testchatsioapi.globalxchange.io/register_user`,{
          email: email,
          app_id: mainAppId
        })
        .then(res =>{
            if(res.data.status){
                // setLoading(false);
                changeNotification({status: true, message: "You Are Now Registered To This App"})
                checkUserInApp()
            }else{
                changeNotification({status: false, message: "Failed To Register You To This App"})
                
            }
        })
      }

    const handleFileChange = (e) => {
        if (e.target.files[0] === undefined) {
            return;
        }
        e.preventDefault();
        const fileReader = new FileReader();
        fileReader.onloadend = () => {
            setSendingImage(fileReader.result);
        };
        fileReader.readAsDataURL(e.target.files[0]);
    };
    const pasteImageHandler = (event) => {
        console.log(event.clipboardData, event.originalEvent?.clipboardData)
        var items = (event.clipboardData || event.originalEvent?.clipboardData)?.items;
        console.log(JSON.stringify(items)); // will give you the mime types
        // find pasted image among pasted items
        var blob = null;
        for (var i = 0; i < items.length; i++) {
            // console.log("url obj", items[i]);
            if (items[i].type.indexOf("image") === 0) {
                blob = items[i].getAsFile();
            }
        }
        // load image if there is a pasted image
        if (blob !== null) {
            var reader = new FileReader();
            reader.onload = function (event) {
                alert(1)
                // console.log("on Paste", event.target.result);
                setSendingImage(event.target.result);
                // setClipImage(event.target.result);  //This is a state where i am storing the image
            };
            reader.readAsDataURL(blob);
            // setVisiblePasteImage(true);  //This is another state that enables a modal .. where I am showing the image
        }
    };

    useEffect(() => {
        stepNumber ? getPreviousMessages() : console.log();
    }, [stepNumber]);

    return (
        <div className="menu-support-main">
            {
                visibleMoreInfo ?
                    <div className="w-100 h-100">
                        <SetupChatProfile visibleMoreInfo={visibleMoreInfo} setVisibleMoreInfo={setVisibleMoreInfo} handleNewSignup={handleNewSignup} />
                    </div>
                    :
                    <>
                        <div
                            className={`uc-body-option-expanded p-0`}
                        >
                            <ChatHeaderHuman
                                user={agency.currentUserDetails}
                            />
                        </div>
                        <div
                            className={
                                `uc-body-chat-box  ${displayUserInfo ? "full-height" : "extra-chat-height"
                                }`}
                        >
                            <ChatBox
                                showChatBox={showChatBox}
                                botHeader={botHeader}
                                setShowChatBox={setShowChatBox}
                                pasteImageHandler={pasteImageHandler}
                                handleFileChange={handleFileChange}
                                setStepNumber={setStepNumber}
                                stepNumber={stepNumber}
                                userDetails={agency.currentUserDetails}
                                currentUserObj={agency.currentUserDetails}
                                messageArray={messageArray}
                                message={message}
                                setMessage={setMessage}
                                handleSubmitMessage={handleSubmitMessage}
                                userObj={userObj}
                            />
                        </div>
                    </>
            }
        </div>
    )
}
