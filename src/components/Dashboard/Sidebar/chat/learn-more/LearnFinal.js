import React from 'react'
import moment from 'moment-timezone';
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import { getVideoList, getArticleList } from '../../../../../services/getAPIs';
import Constant from '../../../../../json/constant';
import { Agency } from '../../../../Context/Context';

export default function LearnFinal({ secondId, content, setShowChatBox, setStep, categoryId }) {
    const agency = React.useContext(Agency);
    const { setMainTabs,setLearnArticle,setPlayVideo,setReadArticle, setLearnVideo } = agency;
    const [articleList, setArticleList] = React.useState({})
    const [theList, setTheList] = React.useState([]);
    const [loading, setLoading] = React.useState(true);
    const [searchTerm, setSearchTerm] = React.useState('');
    const setUpTheList = async (which) => {
        if (which) {
            let res = await getVideoList(secondId._id);
            setLoading(false)
            setTheList(res.data.data);
        } else {
            let res = await getArticleList(secondId._id);
            setTheList(res.data.data)
            setLoading(false)
        }
    }
    const handleVideoArticle = (obj) => {
        if (content.video) {
            setPlayVideo(true);
            setReadArticle(false)
            setLearnVideo(obj.video)
        } else {
            setReadArticle(true)
            setPlayVideo(false);
            setLearnArticle(obj._id)

        }
    }

    React.useEffect(() => {
        setArticleList(content)
        setLoading(true);
        setUpTheList(content.video)

    }, [content])
    return (
        <div className="learn-final">
            <div className="learn-final-header">
                <img src={categoryId.image} />
                <div>
                    <h6>{secondId.title}</h6>
                    <span>{secondId.cv}</span>
                </div>

            </div>
            <div className="learn-final-decider">
                {
                    Constant.whichContent.map((x, i) =>
                        <button
                            className={articleList.id === x.id ? "decided" : ""}
                            onClick={() => setStep(1)}><img src={x.icon} />{x.name}</button>
                    )
                }
            </div>
            <div className="learn-final-content">
                <div className="content-search-bar">
                    <h6>Total Of {theList.length} {articleList.video ? "Videos" : "Articles"} In This Playlist</h6>
                    <input onChange={(e) => setSearchTerm(e.target.value)} placeholder="Search" />
                </div>
                <div className={articleList.video ? "video-article-wrapper" : "article-wrapper"}>
                    {

                        !theList?.length || loading ?
                            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                                <LoadingAnimation type={loading ? "login" : "no-data"} size={{ height: 100, width: 100 }} />

                            </div>
                            :
                            theList.filter(x => { return x.title.toLowerCase().startsWith(searchTerm.toLowerCase()) })
                                .map(obj =>
                                    <div onClick={() => handleVideoArticle(obj)} className="single-video">
                                        <div className="thumbnail-area">
                                            <img src={obj.image} />
                                        </div>
                                        <div className="more-information">
                                            <h6>{obj.title}</h6>
                                            <span>
                                                {articleList.video ? moment(obj.updatedAt).format('DD-MM-YYYY hh:mm a') : obj.desc}

                                            </span>
                                        </div>
                                    </div>
                                )
                    }

                </div>

            </div>

            <div onClick={() => setShowChatBox(true)} className="learn-final-footer">
                <span>
                    Can’t Find What You Are Looking For?
                    </span>
            </div>
        </div>
    )
}
