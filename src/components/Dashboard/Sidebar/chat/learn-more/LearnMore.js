import React from 'react'
import './learn-more.style.scss'
import LearnFinal from './LearnFinal';
import LearnStepOne from './LearnStepOne';
import SelectContent from './SelectContent';
export default function LearnMore({setCategoryId,setShowChatBox, categoryId}) {
    const [step, setStep] = React.useState(0);
    const [secondId, setSecondId] = React.useState('')
    const [content, setContent] = React.useState(null)
    
    
    const [categoryList, setCategoryList] = React.useState([]);
    const selectComponent = () => {
        switch (step) {
            case 0: return <LearnStepOne setSecondId={setSecondId} categoryId={categoryId} setStep={setStep}/>;
            case 1: return <SelectContent categoryId={categoryId} secondId={secondId} setStep={setStep} setContent={setContent}/>
            case 2: return <LearnFinal setShowChatBox={setShowChatBox} setStep={setStep} content={content} categoryId={categoryId} secondId={secondId}/>
        }
    }
    React.useEffect(()=>{
        setStep(0)
    },[categoryId])
    return (
        <div className="learn-more-main">
            {
                selectComponent()
            }
        </div>
    )
}
