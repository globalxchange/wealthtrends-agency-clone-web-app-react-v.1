import React from 'react'
import Constant from '../../../../../json/constant'

export default function SelectContent({ setStep,categoryId,secondId, setContent }) {
    return (
        <div className="select-content">
            <div className="learn-final-header">
                <img src={categoryId.image} />
                <div>
                    <h6>{secondId.title}</h6>
                    <span>{secondId.cv}</span>
                </div>

            </div>
            <h6>How Do You Want To Consume Content? </h6>
            <div className="buttons-wrapper">
                {
                    Constant.whichContent.map(obj =>
                        <button onClick={() => { setStep(2); setContent(obj) }}>
                            <img src={obj.icon} />{obj.name}
                        </button>
                    )
                }
            </div>

        </div>
    )
}