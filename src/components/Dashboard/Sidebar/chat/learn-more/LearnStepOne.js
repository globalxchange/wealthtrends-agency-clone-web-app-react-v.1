import React from 'react'
import Images from '../../../../../assets/a-exporter';
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import { getCategory } from '../../../../../services/getAPIs'

export default function LearnStepOne({ setStep,setSecondId, categoryId }) {
    const [categories, setCategories] = React.useState([])
    const setUpCategory = async () => {
        let res = await getCategory(categoryId._id);
        setCategories(res.data.data);

    }
    React.useEffect(() => {
        setUpCategory()
    }, [categoryId])
    return (
        <div className="learn-more-one">
            <h6 className="page-title">Select One Of The Following Categories</h6>
            {
                !categories?.length?
                <div className="w-100 h-100 d-flex justify-content-center align-items-center"> 
                    <LoadingAnimation type="no-data" size={{height: 100, width: 100}} />
                </div>
                :
                categories.map(obj => <div onClick={() =>{setSecondId(obj); setStep(1)}} className="category-wrapper">
                    <img onError={(e) => { e.target.src = Images.agencyDark }} src={obj.thumbnail} />
                    <div >
                        <h6>{obj.title}</h6>
                        <span>{obj.cv}</span>
                    </div>
                </div>
                )
            }
        </div>
    )
}
