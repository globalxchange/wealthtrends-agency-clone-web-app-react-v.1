import React, { useState, useContext, useEffect, useRef } from "react";
import { useHistory } from "react-router-dom";
import "./sidebar.style.scss";

import Image from "../../../assets/a-exporter.js";
import Constant from "../../../json/constant";
import { Agency } from "../../Context/Context";
import Images from "../../../assets/a-exporter.js";
import nextId from "react-id-generator";
import AllUsers from "./all-users/AllUsers";
import Chat from "./chat/Chat";
import { authenticate } from "../../../services/postAPIs";
export default function Sidebar() {
  const setting = useRef();
  const appsRef = useRef();
  const history = useHistory();
  const agency = useContext(Agency);
  const [importantLinks, setImportantLinks] = useState(null);
  const {
    collapseSidebar,
    registeredAppList,
    currentApp,
    setAdminPassword,
    setStepMain,
    setCheckedId,
    setRefreshSession,
    enableOverlay,
    fullScreenChat,
    setCollapseSidebar,
    setFundsConfig,
    grantAdminAccess,
    fundsConfig,
    setScrollValue,
    mobileLinks,
    setCurrentApp,
    setSearchBox,
    handleAnalytics,
    analyticsType,
    setTradeState,
    overviewApp,
    setAppDownloadConfig,
    assetClassSelected,
    currentUserDetails,
    setAssetSelected,
    assetSelected,
    setLedgerOrAnalytics,
    tradeState,
    setRefreshApp,
    allMarketCoins,
    toggleTheme,
    setAssetClassSelected,
    setPreviousMainTab,
    setShowApps,
    searchBox,
    setRegisteredAppsByUser,
    sidebarAction,
    mainBalance,
    setMainTabs,
    mainTabs,
    setSidebarAction,
    showApps,
    setSecondGate,
    setUserChatEnable,
    userChatEnable,
    enableuserfunctiom,
  } = agency;
  const [listItem, setListItem] = useState({
    ...registeredAppList[0],
    name: registeredAppList[0].name,
    _id: registeredAppList[0]._id,
  });
  const [moreOptions, setMoreOptions] = useState(false);
  const [moreApps, setMoreApps] = useState(false);
  const [loading, setLoading] = React.useState();
  const [sidebarContent, setSidebarContent] = useState({
    title: "Atlas",
    list: [],
  });

  const handleClick = (inte) => {
    if (inte === 0) {
      if (collapseSidebar) {
        setCollapseSidebar(false);
      } else if (
        mainTabs === "accounts" &&
        assetSelected === null &&
        assetClassSelected === null &&
        !tradeState &&
        !collapseSidebar
      ) {
        setCollapseSidebar(true);
        setUserChatEnable(true);
      } else {
        setMainTabs("accounts");
        history.push("/dashboard");
        setTradeState(false);
        setAssetSelected(null);
        setAssetClassSelected(null);
        handleAnalytics("");
        setCollapseSidebar(false);
      }
    } else if (inte === 1) {
      setSearchBox(!searchBox);
      setCollapseSidebar(false);
    } else if (inte === 2) {
      setCollapseSidebar(true);
      setSidebarAction({ count: 1, type: "" });
    }
  };
  const handleClickBottom = (i) => {
    showApps ? setShowApps(false) : setShowApps(true);
  };
  const logout = () => {
    localStorage.setItem("userEmail", "");
    localStorage.setItem("deviceKey", "");
    localStorage.setItem("accessToken", "");
    localStorage.setItem("idToken", "");
    localStorage.setItem("refreshToken", "");
    localStorage.setItem("VsaProfileId", "");
    history.replace("/login");
  };
  const routeToFunds = () => {
    setMainTabs("accounts");
    setCollapseSidebar(false);
    setSidebarContent({ title: "", list: [] });
    setSecondGate(true);
    history.replace("/dashboard");
    if (fundsConfig.crypto) {
      setAssetSelected(Constant.navbarList[0]);
      setCheckedId(Constant.navbarList[0].id);
      let a = setTimeout(() => {
        setSidebarAction({ count: 0, type: "" });
        setAssetClassSelected(fundsConfig.coin);
        setScrollValue(fundsConfig.num);
        clearTimeout(a);
      }, 300);
    } else {
      setAssetSelected(Constant.navbarList[1]);
      setCheckedId(Constant.navbarList[1].id);
      let a = setTimeout(() => {
        setSidebarAction({ count: 0, type: "" });
        setAssetClassSelected(fundsConfig.coin);
        setScrollValue(fundsConfig.num);
        clearTimeout(a);
      }, 100);
    }
    fundsConfig.add
      ? handleAnalytics("Add Funds")
      : handleAnalytics("Send Funds");
    setLedgerOrAnalytics(false);

    setListItem(currentApp);
    setFundsConfig({
      ...fundsConfig,
      action: false,
    });
  };
  React.useEffect(() => {
    if (fundsConfig.action) {
      routeToFunds();
    } else {
    }
  }, [fundsConfig]);

  const handleSidebarClick = (obj, num) => {
    if (["Friends", "Invest", "VSA"].includes(obj.name)) return;
    // setMainTabs("accounts")
    setListItem(obj);

    switch (obj.name) {
      // case "Add Funds": setTradeState(false); setSidebarAction({ count: 2, type: "Add Funds" }); handleAnalytics("Add Funds"); break;
      case "Add Funds":
        setTradeState(false);
        setSidebarAction({ count: 2, type: "Add Funds" });
        setFundsConfig({ ...fundsConfig, add: true });
        break;
      // case "Send Funds": setTradeState(false); setSidebarAction({ count: 2, type: "Send Funds" }); handleAnalytics("Send Funds"); break;
      case "Send Funds":
        setTradeState(false);
        setSidebarAction({ count: 2, type: "Send Funds" });
        setFundsConfig({ ...fundsConfig, add: false });
        break;
      // case "Crypto": setSecondGate(false); setAssetSelected(Constant.navbarList[1]); setCheckedId(Constant.navbarList[1].id); setSidebarAction({ count: 3, type: "Cryptocurrency" }); break;
      case "Crypto":
        setFundsConfig({ ...fundsConfig, crypto: true });
        setSidebarAction({ count: 3, type: "Cryptocurrency" });
        break;
      // case "Fiat": setSecondGate(false); setAssetSelected(Constant.navbarList[0]); setCheckedId(Constant.navbarList[0].id); setSidebarAction({ count: 3, type: "Fiat Currencies" }); break;
      case "Fiat":
        setFundsConfig({ ...fundsConfig, crypto: false });
        setSidebarAction({ count: 3, type: "Fiat Currencies" });
        break;
      case "GXLive":
        window.open("https://globalxchange.com/", "_blank");
        break;
      case "Snapay":
        break;
      case "Nitrogen":
        break;
      case "Invest":
        break;
      case "Analytics":
        setMainTabs("admin-analytics");
        setSidebarAction({ count: 0, type: "" });
        setCollapseSidebar(false);
        break;
      case "VSA":
        break;
      case "Trade":
        setMainTabs("accounts");
        setTradeState(true);
        setAssetSelected(Constant.navbarList[0]);
        setSecondGate(true);
        setCollapseSidebar(false);
        break;
      case "Admin":
        setSidebarAction({ count: 6, type: "Admin" });
        setAdminPassword(true);
        break;
      case "Mobile Apps":
        setSidebarAction({ count: 11, type: "Mobile Apps" });
        break;
      case "IOS Apps":
        setSidebarAction({ count: 0, type: "" });
        setMainTabs("apps");
        setAppDownloadConfig(false);
        break;
      case "Android Apps":
        setMainTabs("apps");
        setAppDownloadConfig(true);
        break;
      case "Marketplace":
        setSidebarAction({ count: 0, type: "" });
        setCollapseSidebar(false);
        setMainTabs("E-commerce");
        break;
      case "BrokerApp":
        setSidebarAction({ count: 0, type: "" });
        setCollapseSidebar(false);
        setMainTabs("broker");
        break;
      case "Developer Tools":
        setSidebarAction({ count: 0, type: "" });
        setCollapseSidebar(false);
        setMainTabs("developer");
        setMoreOptions(false);
        break;
      case "IOS App":
        setSidebarAction({ count: 0, type: "" });
        setCollapseSidebar(false);
        window.open(overviewApp.ios_app_link, "_blank");
        break;
      case "Android App":
        setSidebarAction({ count: 0, type: "" });
        setCollapseSidebar(false);
        break;
      case "User View":
        setSidebarAction({ count: 7, type: "User View" });
        break;
      case "All Users":
        setSidebarAction({ count: 8, type: "All Users" });
        break;
      case "Refresh":
        setSidebarAction({ count: 10, type: "Select Refresh Type" });
        break;
      case "App Level":
        setRefreshApp(false);
        refresh(true);
        break;
      case "On Page":
        setRefreshApp(true);
        refresh(false);
        break;
      // handleAnalytics("Trade");setAssetSelected(Constant.navbarList[0]);
      case "Friends":
        if (listItem.name === "Friends") return;
        setAssetSelected(Constant.navbarList[0]);
        setAssetClassSelected(mainBalance.fiat[0]);
        setTradeState(false);
        handleAnalytics("Friends");
        setSecondGate(true);
        setSidebarAction({ count: 0, type: "" });
        setCollapseSidebar(false);
        break;
      default:
        setFundsConfig({ ...fundsConfig, coin: obj, num: num, action: true });
    }
  };

  const handleNewCryptoApp = (obj) => {
    if (listItem.type === obj.type) return;
    setListItem(obj);

    //For Main APP
    // let temp = { app_code: obj.app_code, profile_id: obj.profile_id, app_name: obj.app_name, app_icon: obj.app_name }
    // setCurrentApp(temp)

    //For Clone
    // alert(obj.type)
    if (obj.type === "blockcheck") {
      setListItem(obj);
      setMainTabs("blockcheck");
      setCollapseSidebar(false);
    } else if (obj.type !== "dashboard") {
      setSecondGate(true);
      setCollapseSidebar(false);
      setCheckedId("id2");
      setMainTabs("accounts");
      setAssetSelected(Constant.navbarList[0]);
      handleAnalytics("");
      setTradeState(false);
    } else {
      setAssetSelected(null);
      setAssetClassSelected(null);
      setCollapseSidebar(false);
      setCheckedId("id2");
      setTradeState(false);
      setSecondGate(true);
      setMainTabs("accounts");
      handleAnalytics("");
      setTradeState(false);
    }
  };

  const handleBackButton = () => {
    if (sidebarAction.count === 0) {
      setUserChatEnable(true);
      setCollapseSidebar(!collapseSidebar);
    } else {
      switch (sidebarAction.count) {
        case 1:
          setListItem(currentApp);
          setCollapseSidebar(false);
          setSidebarAction({ count: 0, type: "" });

          break;
        case 2:
          setSidebarAction({ count: 1, type: "" });
          setListItem(Constant.sidebarListTwo[0]);
          setAssetSelected(null);
          break;
        case 3:
          sidebarContent.title === "Fiat Currencies"
            ? setSidebarAction({ count: 2, type: "Fiat Currencies" })
            : setSidebarAction({ count: 2, type: "Cryptocurrency" });
          setAssetClassSelected(null);
          break;
        case 5:
          setCollapseSidebar(false);
          setListItem(currentApp);
          setSidebarAction({ count: 0, type: "" });
          break;
        case 6:
          setSidebarAction({ count: 5, type: "Ecosystem" });
          setSidebarContent({
            title: "Ecosystem",
            list: [...Constant.sidebarEcosystem],
          });
          break;
        case 7:
          setSidebarAction({ count: 6, type: "Admin" });
          setSidebarContent({
            title: "Admin",
            list: [...Constant.sidebarAdmin],
          });
          break;
        case 8:
          setSidebarAction({ count: 7, type: "user View" });
          setSidebarContent({
            title: "User View",
            list: [...Constant.sidebarUerView],
          });
          break;
        case 10:
          setSidebarAction({ count: 1, type: "" });
          setListItem(Constant.sidebarListTwo[0]);
          setAssetSelected(null);
          break;
        case 11:
          setMainTabs("accounts");
          setSidebarAction({ count: 5, type: "Ecosystem" });
          setSidebarContent({
            title: "Ecosystem",
            list: [...Constant.sidebarEcosystem],
          });
          break;
        default:
          break;
      }
    }
  };

  const refresh = async (app) => {
    setLoading(true);
    let res;
    try {
      res = await authenticate();
      if (res.data.status) {
        setLoading(false);
        setRefreshSession(false);
        setListItem(currentApp);
        setCollapseSidebar(false);
        setSidebarAction({ count: 0, type: "" });
        if (app) {
          setRegisteredAppsByUser();
        } else {
        }
      } else {
        setSidebarAction({ count: 0, type: "" });
        setListItem(currentApp);
        setCollapseSidebar(false);
        setLoading(false);
        setRefreshSession(true);
      }
    } catch (e) {
      setSidebarAction({ count: 0, type: "" });
      setListItem(currentApp);
      setCollapseSidebar(false);
      setLoading(false);
      setRefreshSession(true);
    }
  };

  useEffect(() => {
    moreOptions
      ? window.addEventListener("mousedown", handleMouseDown)
      : window.removeEventListener("mousedown", handleMouseDown);
  }, [moreOptions]);

  useEffect(() => {
    moreApps
      ? window.addEventListener("mousedown", handleMouseDownTwo)
      : window.removeEventListener("mousedown", handleMouseDownTwo);
  }, [moreApps]);

  useEffect(() => {
    let temp = mobileLinks?.find((obj) => {
      return obj.Key === "dgpayments";
    }); //Here name of App will appear;
    setImportantLinks(temp?.formData);
    console.log("last links", temp);
  }, [mobileLinks]);

  const handleMouseDown = (e) => {
    if (setting.current === null) return;
    else if (setting.current.contains(e.target)) {
      // alert()
    } else {
      if (moreOptions) {
        setMoreOptions(false);
        enableOverlay(false);
      }
    }
  };

  const handleMouseDownTwo = (e) => {
    if (appsRef.current === null) return;
    else if (appsRef.current.contains(e.target)) {
      // alert()
    } else {
      if (!e.target.className.toString().includes("exception")) {
        setMoreApps(false);
      }
    }
  };
  const handleAccountTool = (i) => {
    if (i === 3) {
      logout();
    } else if (i === 2) {
      setMainTabs("investor");
      setMoreOptions(false);
      enableuserfunctiom();
    }
    if (i === 0) {
      // toggleTheme()
    }
  };
  const handleApps = (title, num) => {
    switch (title) {
      case "Web Apps":
        setMoreApps(false);
        enableOverlay(false);
        if (num) {
          setMainTabs("broker");
        } else {
          setMainTabs("E-commerce");
        }
        break;
      case "Mobile Apps":
        setMoreOptions(false);
        enableOverlay(false);
        if (num) {
          window.open("https://" + importantLinks.ioslink, "_blank");
        } else {
          window.open("https://" + importantLinks.androidlink, "_blank");
        }
        break;
      default:
        return;
    }
  };

  useEffect(() => {
    switch (sidebarAction.count) {
      case 1:
        setSidebarContent({
          title: "New Action",
          list: [...Constant.sidebarListTwo],
        });
        if (tradeState) {
          setListItem(Constant.sidebarListTwo[2]);
        } else {
          switch (analyticsType) {
            case "":
              setListItem({
                ...currentApp,
                name: currentApp.app_name,
                _id: currentApp.profile_id,
              });
              break;
            case "Add Funds":
              setListItem(Constant.sidebarListTwo[0]);
              break;
            case "Send Funds":
              setListItem(Constant.sidebarListTwo[1]);
              break;
            case "Friends":
              setListItem(Constant.sidebarListTwo[3]);
              break;
            default:
              break;
          }
        }
        // setListItem(analyticsType === "" || analyticsType === "Add Funds" ? Constant.sidebarListTwo[0] : Constant.sidebarListTwo[1])
        break;
      case 2:
        setStepMain(0);
        setSidebarContent({
          title:
            sidebarAction.type === "Add Funds" ? "Add Funds" : "Send Funds",
          list: [...Constant.sidebarListThree],
        });
        break;
      case 3:
        let crypto = mainBalance.crypto.map((obj) => {
          return {
            ...obj,
            icon: obj.coinImage,
            invert: obj.coinImage,
            name: obj.coinName,
          };
        });
        let fiat = mainBalance.fiat.map((obj) => {
          return {
            ...obj,
            icon: obj.coinImage,
            invert: obj.coinImage,
            name: obj.coinName,
          };
        });
        setSidebarContent({
          title:
            sidebarAction.type === "Cryptocurrency"
              ? "Cryptocurrency"
              : "Fiat Currencies",
          list:
            sidebarAction.type === "Cryptocurrency" ? [...crypto] : [...fiat],
        });
        break;

      case 5:
        setSidebarContent({
          title: "Ecosystem",
          list: [...Constant.sidebarEcosystem],
        });
        break;
      case 6:
        if (!grantAdminAccess) return;
        setSidebarContent({ title: "Admin", list: [...Constant.sidebarAdmin] });
        break;
      case 7:
        setSidebarContent({
          title: "User View",
          list: [...Constant.sidebarUerView],
        });
        break;
      case 8:
        setSidebarContent({ title: "All Users", list: [] });
        break;
      case 10:
        setSidebarContent({
          title: "Select Refresh Type",
          list: [...Constant.sidebarRefresh],
        });
        break;

      case 11:
        setSidebarContent({
          title: "Mobile Apps",
          list: [...Constant.sidebarMobileApps],
        });
        break;
      default:
        setSidebarContent({ title: currentApp.app_name, list: [] });
        break;
    }
  }, [sidebarAction, grantAdminAccess, tradeState, registeredAppList]);
  // useEffect(()=>{
  //     if(assetClassSelected !== null && !sidebarAction.count){
  //         setSidebarAction({count: 1, type: ''})
  //         setSidebarContent({ title: "New Action", list: [...Constant.sidebarListTwo] });
  //         setListItem(analyticsType === "" || analyticsType === "Deposit" ? Constant.sidebarListTwo[0] : Constant.sidebarListTwo[1])
  //         // break;

  //     }else if(assetClassSelected === null){
  //         setSidebarAction({count: 0, type: ''})

  //         // setSidebarContent({ title: "DG Payments", list: [...registeredAppList] })
  //     }
  // },[ assetClassSelected])

  useEffect(() => {
    if (analyticsType === "" && sidebarAction.count === 1) {
      setListItem(
        analyticsType === "" || analyticsType === "Add Funds"
          ? Constant.sidebarListTwo[0]
          : Constant.sidebarListTwo[1]
      );
    }
  }, [analyticsType, sidebarAction]);
  return (
    <div
      style={
        allMarketCoins && mainBalance
          ? {}
          : { pointerEvents: "none", width: "auto" }
      }
      className={`sidebar ${fullScreenChat ? "occupy-full-width" : ""} ${
        showApps ? "apps-menu-active" : ""
      } ${collapseSidebar ? "invert-colors" : ""}`}
    >
      <div className="sidebar-stick">
        {/* <div className="sidebar-stick-overlay">
                    <button onClick={() => setShowApps(false)} className={showApps ? "" : "d-none"}>x</button>
                </div> */}
        <div className="sidebar-stick-upper">
          {Constant.sidebarImages.map((obj, i) => (
            <span className="arrow-button">
              <img
                key={obj.keyId}
                alt=""
                onClick={() => handleClick(i)}
                src={i ? obj.inverted : currentApp.app_icon}
              />
              <span className={obj.text === null ? "d-none" : ""}>
                {obj.text}
              </span>
            </span>
          ))}
        </div>
        <div className="sidebar-stick-below">
          <button onClick={() => handleClickBottom()}>
            <img src={Images.gxLiveWhiteLogo} />
            <span>Explore Apps</span>
          </button>
          <button onClick={() => logout()}>
            <img src={Images.logOut} />{" "}
          </button>
        </div>
      </div>
      <div
        style={
          fullScreenChat
            ? { width: `calc(100% - 80px)`, transition: "0.3s" }
            : collapseSidebar
            ? { width: "24vw", paddingRight: 5, transition: "0.3s" }
            : { pointerEvents: "none" }
        }
        className={`sidebar-content ${
          fullScreenChat ? "full-screen-chat" : ""
        } ${userChatEnable ? "p-0" : ""}`}
      >
        {userChatEnable ? (
          <Chat refresh={refresh} routeToFund={routeToFunds} />
        ) : (
          <>
            <h4>{sidebarContent.title}</h4>
            <div className="list-wrapper">
              {sidebarAction.count === 8 ? (
                <AllUsers />
              ) : (
                sidebarContent.list.map((obj, i) => (
                  <h6
                    key={obj._id}
                    // style={collapseSidebar ? {} : { display: 'none' }}
                    style={
                      ["Friends", "Invest", "VSA"].includes(obj.name)
                        ? { opacity: 0.35, cursor: "not-allowed" }
                        : {}
                    }
                    onClick={() =>
                      sidebarAction.count
                        ? handleSidebarClick(obj, i + 1)
                        : handleNewCryptoApp(obj)
                    }
                    className={listItem._id === obj._id ? "selected" : ""}
                  >
                    <img
                      onError={(e) => (e.target.src = Image.gxMainLogo)}
                      className={obj.icon === "" ? "default" : ""}
                      height="16px"
                      width="16px"
                      src={
                        listItem._id === obj._id
                          ? agency.theme !== "light"
                            ? obj.invert
                            : obj.icon
                          : agency.theme !== "light"
                          ? obj.icon
                          : obj.invert
                      }
                    />
                    {obj.name}
                    <button
                      className={
                        loading && listItem._id === obj._id
                          ? "loading-start"
                          : "d-none"
                      }
                    ></button>
                  </h6>
                ))
              )}
            </div>
          </>
        )}
      </div>
      {/* <button onClick={() => handleBackButton()} className="collapse-sidebar">
                <img
                    style={collapseSidebar ? { transform: "rotate(180deg)" } : {}}
                    src={Image.rightArrow} />
                    <span>Chats.io</span>
            </button> */}
    </div>
  );
}
