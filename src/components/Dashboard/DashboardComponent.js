import React, { useContext } from "react";
import "./dashboard.style.scss";
import Sidebar from "./Sidebar/Sidebar";
import Content from "./Content/Content";
import { Agency } from "../Context/Context";
import SkeletonMain from "../common-components/skeleton-main/SkeletonMain";
import MainModal from "../common-components/MainModal/MainModal";
import NotificationComponent from "./notification/notification.component";
import SidebarSkeleton from "../common-components/sidebar-skeleton/SidebarSkeleton";
import Broker from "../common-components/BrokerLandingPage/BrokerLandingPage";
import UserEnablecallde from "./EnableUSer/EnableUSer";
import PortalCopied from "../common-components/PortalCopied/PortalCopied";
export default function DashboardComponent() {
  const agency = useContext(Agency);
  const { mainBalance, allMarketCoins, currentApp, calBroker } = agency;

  return (
    <div className="dashboard">
      <PortalCopied />
      {currentApp ? <Sidebar /> : <SidebarSkeleton />}
      {allMarketCoins && mainBalance ? <Content /> : <SkeletonMain />}

      {/* git add .
git commit -m ""
git push origin branch-name */}

      {/* <Broker/> */}

      <MainModal />
      <NotificationComponent />
      <UserEnablecallde />
    </div>
  );
}
