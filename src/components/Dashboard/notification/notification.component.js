import React, { useContext } from 'react';
import './notification.syle.scss';
import { Agency } from '../../Context/Context';
import Images from '../../../assets/a-exporter';
const NotificationComponent = () => {
    const agency = useContext(Agency)
    const { notification, changeNotification } = agency;
    return (
        <>
            {
                notification.status === '' ?
                    null
                    :
                    <div style={notification.status === '' ? { display: 'none' } : notification.status ? { backgroundColor: '#70cd5f' } : { backgroundColor: '#de4b25' }} className={`notification ${notification.status !== '' ? "show" : "hide"}`}>
                        <h6>Message: {notification.message}</h6>
                        <img onClick={() => changeNotification({ status: '', message: '' })} src={Images.add} />
                    </div>
            }
        </>
    )
};
export default NotificationComponent;
