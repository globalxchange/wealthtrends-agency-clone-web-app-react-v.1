import React from 'react'
import nextId from 'react-id-generator';
import Images from '../../../../assets/a-exporter'
import Constant from '../../../../json/constant'
import LoadingAnimation from '../../../../lotties/LoadingAnimation';
import { instaCurrencyVault, instaReadyToExecute, instaFetchPaths, instaFindCountry, instaFindBanker, instaFindOtherCurrency, instaFindMethod } from '../../../../services/getAPIs';
import { Agency } from '../../../Context/Context';
import ExecutionPanel from './execution-panel/ExecutionPanel';
import './instacrypto.style.scss'
import SidePanel from './side-panel/SidePanel';
export default function InstaCryptoMain() {
    const agency = React.useContext(Agency);
    const mainRef = React.useRef()
    const { changeNotification } = agency;
    const [mainList, setMainList] = React.useState([]);
    const [loading, setLoading] = React.useState(false);
    const [executionPanel, setExecutionPanel] = React.useState(false);
    const [finalData, setFinalData] = React.useState(null);
    const [pathList, setPathList] = React.useState([]);
    const [pathLoading, setPathLoading] = React.useState(true);
    const [currentState, setCurrentState] = React.useState(0);
    const [currentFrom, setCurrentFrom] = React.useState("one");
    const [instituteId, setInstituteId] = React.useState('');
    const [scrollButton, setScrollButton] = React.useState(false);
    const [inputActive, setInputActive] = React.useState(false);
    const [searchTerm, setSearchTerm] = React.useState('')


    const [importantId, setImportantId] = React.useState({
        type: "",
        coin: "",
        country: "",
        method: "", banker: ""
    })
    const resetAll = () => {
        setMainList([Constant.instaCryptoType]);
        setExecutionPanel(false);
        setFinalData(null)
        setPathList([]);
        setFooterList([]);
        setCurrentState(0)
        setImportantId({
            type: "",
            coin: "",
            country: "",
            method: "", banker: ""
        })
    }
    const [footerList, setFooterList] = React.useState([]);

    const handleScroll = (id) => {
        let target = document.getElementById(id);
        console.log("scroll", id)
        mainRef.current.scroll({ top: target?.offsetTop, behavior: "smooth" })
        console.log("scroll", target?.offsetTop + 50)

    }
    const handleBack = () => {
        if (currentState === 6) {
            footerList.pop();
            setFooterList([...footerList]);
            setCurrentState(currentState - 1);
            handleScroll(currentFrom);
            return;
        }
        mainList.pop();
        footerList.pop();

        setMainList([...mainList]);
        setFooterList([...footerList]);
        setCurrentState(currentState - 1);
        switch (currentState) {
            case 5: handleScroll("four"); break;
            case 4: handleScroll("three"); break;
            case 3: handleScroll("two"); break;
            case 2: handleScroll("one"); break;
        }
    }

    const handleClick = async (from, id, obj, num) => {

        switch (from) {
            case "one":
                if (currentState !== 0) {
                    if (currentState - 1 === num) {
                        handleBack(from)
                    }
                    return
                }
                setCurrentFrom("one")
                setLoading(true);
                let res = await instaCurrencyVault(id);
                if (!res.data.pathData.to_currency.length) {
                    changeNotification({ status: false, message: "Message: Sorry.. It's Not Available Yet" })
                    setLoading(false);
                    return
                }
                setImportantId({ ...importantId, type: id });
                let tempArrayOne = res.data.pathData.to_currency.map(obj => {
                    return { ...obj, name: obj?.coin_metadata?.coinSymbol, icon: obj?.coin_metadata?.coinImage, id: obj._id }
                })
                console.log(res.data.pathData.to_currency)
                let tempOne = {
                    id: "two",
                    keyId: nextId(),
                    text: "Which Currency Vault Do You Want To Fund?",
                    list: [...tempArrayOne]
                }
                setMainList([...mainList, tempOne]);
                setLoading(false);
                setCurrentState(1)
                break;

            case "two":
                if (currentState !== 1) {
                    if (currentState - 1 === num) {
                        handleBack(from)
                    }
                    return
                }
                setCurrentFrom("one")
                setLoading(true);
                let resX = await instaFindOtherCurrency(importantId.type, id);
                if (!resX.data.pathData.to_currency.length) {
                    changeNotification({ status: false, message: "Message: Sorry.. It's Not Available Yet" })
                    setLoading(false);
                    return
                }
                let tempArrayX = resX.data.pathData.to_currency.map(obj => {
                    return { ...obj, name: obj.coin_metadata.coinSymbol, icon: obj.coin_metadata.coinImage, id: obj._id }
                })
                setImportantId({ ...importantId, coin: id });
                let tempX = {
                    id: "three",
                    keyId: nextId(),
                    text: "What Currency Are You Using?",
                    list: [...tempArrayX]
                }
                setMainList([...mainList, tempX]);
                setLoading(false);
                setCurrentState(2)
                break;
            case "three":
                if (currentState !== 2) {
                    if (currentState - 1 === num) {
                        handleBack(from)
                    }
                    return
                }
                setCurrentFrom("two")
                setLoading(true);
                let resTwo = await instaFindCountry(importantId.type, importantId.coin, id);
                if (!resTwo.data.pathData.country.length) {
                    changeNotification({ status: false, message: "Message: Sorry.. It's Not Available Yet" })
                    setLoading(false);
                    return
                }
                let tempArrayTwo = resTwo.data.pathData.country.map(obj => {
                    return { ...obj, name: obj._id, icon: obj.metadata.image, id: obj._id }
                })
                setImportantId({ ...importantId, toCoin: id })
                let tempTwo = {
                    id: "four",
                    keyId: nextId(),
                    text: "What Country Are You Transacting From?",
                    list: [...tempArrayTwo]
                }
                setMainList([...mainList, tempTwo]);
                setLoading(false);
                setCurrentState(3)
                break;

            case "four":
                if (currentState !== 3) {
                    if (currentState - 1 === num) {
                        handleBack(from)
                    }
                    return
                }
                setCurrentFrom("three")
                setLoading(true);
                let resThree = await instaFindMethod(importantId.type, importantId.coin, id, importantId.toCoin);
                if (!resThree.data.pathData.country.length) {
                    changeNotification({ status: false, message: "Message: Sorry.. It's Not Available Yet" })
                    setLoading(false);
                    return
                }
                let tempArrayThree = resThree.data.pathData.paymentMethod.map(obj => {
                    return { ...obj, name: obj.metadata.name, icon: obj.metadata.icon, id: obj.metadata.code }
                })
                setImportantId({ ...importantId, country: id });
                let tempThree = {
                    id: "five",
                    keyId: nextId(),
                    text: "What Payment Method Do You Wish To Use?",
                    list: [...tempArrayThree]
                }
                setMainList([...mainList, tempThree]);
                setLoading(false);
                setCurrentState(4)
                break;


            case "five":
                if (currentState !== 4) {
                    if (currentState - 1 === num) {
                        handleBack(from)
                    }
                    return
                }
                setCurrentFrom("four")
                setLoading(true);
                let resFour = await instaFindBanker(importantId.type, importantId.coin, importantId.country, importantId.toCoin, id);

                if (!resFour.data.pathData.banker.length) {
                    changeNotification({ status: false, message: "Message: Sorry.. It's Not Available Yet" })
                    setLoading(false);
                    return
                }
                setImportantId({ ...importantId, method: id });
                let tempArrayFour = resFour.data.pathData.banker.map(obj => {
                    return { ...obj, name: obj.displayName, icon: obj?.icons?.image1, id: obj._id }
                })
                let tempFour = {
                    id: "six",
                    keyId: nextId(),
                    text: "What Payment Method Do You Wish To Use?",
                    list: [...tempArrayFour]
                }
                setMainList([...mainList, tempFour]);
                setLoading(false);
                setCurrentState(5)
                break;

            case "six":
                if (currentState !== 5) {
                    if (currentState - 1 === num) {
                        handleBack(from)
                    }
                    return
                }
                setCurrentFrom("five")
                setLoading(true);
                let resFive = await instaReadyToExecute(importantId.type, importantId.coin, importantId.country, importantId.toCoin, importantId.method, id);

                if (!resFive.data.pathData.paymentPaths.length) {
                    changeNotification({ status: false, message: "Message: Sorry.. It's Not Available Yet" })
                    setLoading(false);
                    return
                }
                setImportantId({ ...importantId, banker: id });
                console.log("finalData", resFive.data);
                let pathId = resFive.data.pathData.paymentPaths[0].path_ids[0]
                let resSix = await instaFetchPaths(pathId);
                let tObj = resSix.data.paths[0].total_steps;
                setInstituteId(resSix.data.paths[0].institution_id);
                let tArr = Object.keys(tObj).map((key, i) => {
                    return tObj[`step${i + 1}`]
                });
                console.log("path steps", tArr);
                setPathList(tArr);
                setPathLoading(false);
                let tempObj = {
                    pathId: pathId,
                    banker: {
                        id: resFive.data.pathData.banker[0]._id,
                        name: resFive.data.pathData.banker[0].displayName,
                        icon: resFive.data.pathData.banker[0].icons.image1
                    },
                    to: {
                        coin: resFive.data.pathData.to_currency[0]._id,
                        coinImage: resFive.data.pathData.to_currency[0].coin_metadata.coinImage
                    },
                    from: {
                        coin: resFive.data.pathData.from_currency[0]._id,
                        coinImage: resFive.data.pathData.from_currency[0].coin_metadata.coinImage
                    },
                    method: {
                        id: resFive.data.pathData.paymentMethod[0]._id,
                        name: resFive.data.pathData.paymentMethod[0].metadata.name,
                        icon: resFive.data.pathData.paymentMethod[0].metadata.icon
                    }
                }

                setFinalData(tempObj)
                setExecutionPanel(true)
                setLoading(false);
                setCurrentState(6)
                break;



            default:
                break;
        }
        handleScroll(from)
        setFooterList([...footerList, obj])
    }
    const handleScrollWrapper = (id) => {
        let temp = document.getElementById(id);
        temp.scroll({ top: 0, left: temp.scrollLeft + 150, behavior: "smooth" })


    }
    const findElement = (id) => {
        let temp = document.getElementById(id + "inside");
        console.log("element", temp.offsetWidth, temp.scrollWidth);
        if (temp.scrollWidth > temp.offsetWidth) {
            setScrollButton(true)
        } else {
            setScrollButton(false)
        }

    }
    React.useEffect(() => {
        setMainList([Constant.instaCryptoType])
    }, []);

    React.useEffect(() => {
        setSearchTerm('')
        if (!mainList.length) return
        let temp = mainList[mainList.length - 1].id;
        let a = setTimeout(() => {
            findElement(temp);
            clearTimeout(a);
        }, 200)
    }, [mainList])


    return (
        <div className="instacrypto-main-wrapper">
            <div className={loading ? "insta-loader" : "d-none"}>
                <LoadingAnimation size={{ height: 180, width: 180 }} />
            </div>
            <div className={`instacrypto-main ${executionPanel ? "adjust-content" : "full-content"}`}>
                <div className="instacrypto-header">
                    <h2>Welcome To <img src={Images.instaCryptoMainLogo} /></h2>
                </div>
                <div className="instacrypto-body">
                    <div className={executionPanel ? "insta-execution-panel" : "d-none"}>
                        {executionPanel ?
                            <ExecutionPanel
                                resetAll={resetAll}
                                finalData={finalData} />
                            :
                            ''
                        }
                    </div>
                    <div ref={mainRef} className="instacrypto-main-list-wrapper">
                        {

                            mainList.map((obj, num) =>
                                <div key={obj.keyId} id={obj.id} className="insta-step-one">
                                    <p>{obj.text}</p>
                                    <div className="insta-step-one-wrapper-main">
                                        <div id={obj.id + "inside"} className="insta-step-one-wrapper">
                                            {
                                                obj.list.filter(x => { return !x.name || num !== currentState ? true : x.name.toLowerCase().startsWith(searchTerm.toLowerCase()) }).map(objTwo =>
                                                    <div
                                                        key={!obj?.coin_metadata?._id ? obj.id : obj.coin_metadata._id}
                                                        className={
                                                            num < currentState ? objTwo.name === footerList[num]?.name
                                                                ?
                                                                "selected-card"
                                                                :
                                                                "disable-card"
                                                                :
                                                                ""
                                                        }
                                                        onClick={() => handleClick(obj.id, objTwo.id, objTwo, num)}>
                                                        <img className="inside-image" src={objTwo.icon} />
                                                        <span>{objTwo.name}</span>
                                                    </div>
                                                )
                                            }
                                        </div>
                                        <button onClick={() => handleScrollWrapper(obj.id + "inside")} className={num === currentState && scrollButton ? "scroll-image-button" : "d-none"}>
                                            <img className="scroll-image" src={Images.rightArrowNew} />
                                        </button>
                                        <div className={`search-wrapper ${inputActive ? "" : "no-width-main"} ${num === currentState ? "" : "d-none"}`}>
                                            <input onChange={(e) => setSearchTerm(e.target.value)} placeholder="Search" className={inputActive ? "input-full-width" : "input-no-width"} />
                                            <button onClick={() => setInputActive(!inputActive)}>
                                                <img src={inputActive ? Images.addDark : Images.searchDark} />
                                            </button>
                                            {

                                            }
                                        </div>

                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
                <div className="instacrypto-footer">
                    {
                        footerList.map((obj, num) => <div onClick={() => setExecutionPanel(false)}>
                            <img
                                onClick={() => (currentState - 1) && !executionPanel ? handleBack() : null}
                                className={(currentState - 1) === num ? "enable-back" : "disable-back"} src={obj.icon} />
                        </div>
                        )
                    }

                </div>
            </div>
            <div className={`instaCrypto-right-panel ${executionPanel ? "for-panel" : "no-panel"}`}>
                {
                    executionPanel ?
                        <SidePanel instituteId={instituteId} finalData={finalData} loading={pathLoading} list={pathList} />
                        :
                        ''
                }
            </div>
        </div>
    )
}
