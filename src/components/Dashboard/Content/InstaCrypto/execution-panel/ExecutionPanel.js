import React from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../../assets/a-exporter'
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import { placeTradeAPI, decryptPlaceTrade } from '../../../../../services/postAPIs';
import { Agency } from '../../../../Context/Context';

export default function ExecutionPanel({ finalData, resetAll }) {
    const agency = React.useContext(Agency);
    const { valueFormatter, currentApp, changeNotification } = agency;
    const [loading, setLoading] = React.useState(false);
    const [sendingValue, setSendingValue] = React.useState('');
    const [receivingValue, setReceivingValue] = React.useState('');
    const decimalRegex = /^(\d*)\.?(\d){0,6}$/

    const handleSubmitFinal = async () => {
        setLoading(true)
        let token = localStorage.getItem("idToken")
        let email = localStorage.getItem("userEmail")
        let tempObj = {
            token: token,
            email: email,
            app_code: currentApp.app_code,
            profile_id: currentApp.profile_id,
            stats: false,
            from_amount: sendingValue,
            path_id: finalData.pathId
        }
        let res = await placeTradeAPI(tempObj);
        let last = await decryptPlaceTrade(res.data.data);
        if (last.data.status) {
            resetAll();
            changeNotification({ status: true, message: "Trade Successfully" });

        } else {
            setLoading(false)
            changeNotification({ status: false, message: "Trade Unsuccessful" })
        }

    }
    const handleSubmit = async () => {
        setLoading(true)
        let token = localStorage.getItem("idToken")
        let email = localStorage.getItem("userEmail")
        let fromOrTwo = !receivingValue ? true : false;

        let tempObj = {
            token: token,
            email: email,
            app_code: currentApp.app_code,
            profile_id: currentApp.profile_id,
            stats: true,
            path_id: finalData.pathId
        }
        if (fromOrTwo) {
            tempObj = {
                ...tempObj,
                from_amount: sendingValue
            }
        } else {
            tempObj = {
                ...tempObj,
                purchased_amount: receivingValue
            }
        }

        let res = await placeTradeAPI(tempObj);
        let last = await decryptPlaceTrade(res.data.data);
        if (fromOrTwo) {
            setReceivingValue(last.data.finalToAmount)
        } else {
            setSendingValue(last.data.finalFromAmount)
        }
        setLoading(false);

    }
    const localHandleKeyPress = (e) => {
        if (e.which === 13) {
            if (!sendingValue && !receivingValue)
                return;
            e.preventDefault();
            handleSubmit()
        }
    }

    const handleChange = (e, from) => {

        if (!decimalRegex.test(e.target.value))
            return

        if (from) {
            setSendingValue('');
            setReceivingValue(e.target.value);
        } else {
            setReceivingValue('');
            setSendingValue(e.target.value);
        }

    }
    return (
        loading ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation size={{ height: 190, width: 190 }} />
            </div>
            :
            <div className="insta-execution-panel-child">
                <div className="execution-panel-header">
                    <h4><img src={finalData?.banker?.icon} /> {finalData?.banker?.name}</h4>
                </div>
                <div className="execution-panel-body">
                    {
                        inputs.map((obj, num) => <div className="execution-panel-input-wrapper">
                            <span>{obj.title}</span>
                            <div className="insta-crypto-box">
                                <span>
                                    <input
                                        onKeyPress={localHandleKeyPress}
                                        value={num ? receivingValue : sendingValue}
                                        onChange={(e) => handleChange(e, num)}
                                        placeholder={valueFormatter(0, finalData?.[obj.id]?.coin)}
                                    />
                                </span>
                                <span><img src={finalData?.[obj.id]?.coinImage} />{finalData?.[obj.id]?.coin}</span>
                            </div>
                        </div>
                        )
                    }
                </div>
                <div className="execution-panel-footer">
                    <button
                        disabled
                    >Fee Audit</button>
                    <button
                        disabled={!sendingValue && !receivingValue  }
                        onClick={() => !sendingValue || !receivingValue ? handleSubmit() : handleSubmitFinal()
                        }
                    >
                        {!sendingValue || !receivingValue 
                            ?
                            "Generate Quote"
                            :
                            "Start"
                        }
                    </button>
                </div>
            </div>
    )
}
const inputs = [
    { keyId: nextId(), title: "You Are Sending ", id: "from" },
    { keyId: nextId(), title: "You Are Receiving ", id: "to" },
]