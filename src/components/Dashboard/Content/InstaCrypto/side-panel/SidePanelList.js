import React from 'react'
import nextId from 'react-id-generator';

export default function SidePanelList({ list, setCurrentStepData, setDescribe }) {
    return (
        <>
            {
                list.map(obj => <div key={nextId()} className="insta-path-wrapper">
                    <div className="insta-path-wrapper-body">
                        <img onClick={() => { setCurrentStepData(obj); setDescribe(true) }} src={obj?.thumbnail} />
                        <h5>{obj?.name}</h5>

                    </div>
                    <div className="insta-path-wrapper-bottom">
                        <div>
                            <img onClick={() => console.log(obj)} src={obj?.icon} />
                        </div>
                        <div>
                            <h5>{obj?.status}</h5>
                            <span>Current Step</span>
                        </div>

                    </div>

                </div>)
            }

        </>
    )
}
