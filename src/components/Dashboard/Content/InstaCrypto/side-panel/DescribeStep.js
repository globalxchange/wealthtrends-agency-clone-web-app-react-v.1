import React from 'react'
import Constant from '../../../../../json/constant';
import { Agency } from '../../../../Context/Context';

export default function DescribeStep({ obj, institute }) {

    const agency = React.useContext(Agency);
    const { setPlayVideo, setLearnVideo, setReadArticle } = agency;

    const handleContact = (id) => {
        switch (id) {
            case "video":
            break;    
        }
        setPlayVideo(true); setLearnVideo(""); setReadArticle(false); 
    }
    React.useEffect(() => {
    }, [obj])
    return (
        <div className="insta-describe-step">
            <div className="describe-cover">
                <img src={obj?.thumbnail} className="describe-full-image" />
                <img src={obj?.icon} className="describe-floating-image" />
            </div>
            <div className="describe-about">
                <h5>{obj?.name}</h5>
                <h6>{Constant.instaContactList.map(obj => <img key={obj.keyId} onClick={() => handleContact(obj.id)} src={obj.icon} />)}</h6>
                <p>{obj?.description}</p>

            </div>

            <div className="describe-related">
                <h6>Related Institution</h6>
                <div className="describe-card">
                    <div className="describe-card-left">
                        <img src={institute?.profile_image}/>
                    </div>
                    <div className="describe-card-right">
                        <div>
                            <h6>{institute?.institute_name}</h6>
                            <span>{institute?.country_name}</span>
                        </div>
                        <span className="learn-more">Learn More About The Involvement Of This Institution</span>

                    </div>

                </div>

            </div>
            <div className="describe-screenshots">
                <h6>Screenshots</h6>
                <div className="screenshot-wrapper">
                    {
                        [].map(obj => <img />)
                    }

                </div>

            </div>

        </div>
    )
}
