import React from 'react'
import LoadingAnimation from '../../../../../lotties/LoadingAnimation'
import { getInstitutesList } from '../../../../../services/getAPIs';
import DescribeStep from './DescribeStep';
import SidePanelList from './SidePanelList'

export default function SidePanel({ loading, list,instituteId, finalData }) {
    const [currentStepData, setCurrentStepData] = React.useState({});
    const [describe, setDescribe] = React.useState(false);
    const [institute, setInstitute] = React.useState([]);

    const setUpInstituteList= async () =>{
        let res = await getInstitutesList();
        let tempObj = res.data.data.find(obj =>{return obj._id === instituteId });
        setInstitute(tempObj);
        console.log("institute", tempObj);


    }
    React.useEffect(() => {
        setUpInstituteList()
    }, [])
    return (
        loading
            ?
            <div className="h-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />
            </div>
            :
            <div className="insta-side-panel">
                <div className="insta-side-panel-header">
                    <h4><img src={finalData?.banker?.icon} /> {finalData?.banker?.name}</h4>
                    <h6 onClick={()=>setDescribe(false)}>Fund With <img src={finalData?.method?.icon} /> {finalData?.method?.name}</h6>
                </div>
                <div className="insta-side-panel-body">
                    {
                        describe
                            ?
                            <DescribeStep institute={institute} obj={currentStepData} />
                            :
                            <SidePanelList setCurrentStepData={setCurrentStepData} setDescribe={setDescribe} list={list} />
                    }

                </div>

            </div>
    )
}
