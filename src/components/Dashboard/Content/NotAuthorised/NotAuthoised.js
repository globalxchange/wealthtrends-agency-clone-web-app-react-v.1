import React from 'react'

export default function NotAuthorized() {
    return (
        <div className="w-100 h-100 d-flex justify-content-center align-items-center ">
            <h1>You Are Not Authorized To Access This Page</h1>
        </div>
    )
}
