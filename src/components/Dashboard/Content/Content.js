import React, { useContext } from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import "./content.style.scss";

import { Agency } from "../../Context/Context";
import SearchBox from "../SearchBox/SearchBox";
import OtpInput from "react-otp-input";
import Bokerpage from "../../common-components/BrokerLandingPage/BrokerLandingPage";
import EcommerceLandingPage from "../../common-components/Mainpagetogglecommerce/MainpagetoggleEcommerce";
import MainAccountContent from "./main-account-content/MainAccountContent";
import Images from "../../../assets/a-exporter";
import AdminAnalytics from "../../common-components/VSA/Controller/ControllerDashboard";
import RefreshSession from "./RefreshSession";
import DeveloperTools from "./DeveloperTools/DeveloperTools";
import InvestorRelation from "./InvestorRelation/InvestorRelation";
import MobileAppSetUp from "./MobileAppSetUp/NewMobileSetUp/NewMobileSetUpController";
import BlockcheckMain from "./Blockcheck/BlockcheckMain";
import AppsMenu from "./AppsMenu/AppsMenu";
import NewMobile from "./MobileAppSetUp/NewMobileSetUp/NewMobileSetUpController";
import LearnVideoPlayer from "./LearnVideoPlayer/LearnVideoPlayer";
import ArticleComponent from "./ArticleComponent/ArticleComponent";
import TerminalMain from "./terminal/terminal.main";
import ComingSoon from "./ComingSoon";
import InstaCryptoMain from "./InstaCrypto/InstaCryptoMain";
import NotAuthoised from "./NotAuthorised/NotAuthoised";

export default function Content() {
  const agency = useContext(Agency);
  const [pin, setPin] = React.useState("");
  const adminPasswordRef = React.useRef();
  const {
    searchBox,
    explainNavbarTerms,
    fullScreenChat,
    imageUploader,
    playVideo,
    readArticle,
    showApps,
    imageUploading,
    setGrantAdminAccess,
    preImageUpload,
    setPreImageUpload,
    overlay,
    setAdminPassword,
    adminPassword,
    otherAccountDetails,
    refreshSession,
    setSearchBox,
    setCollapseSidebar,
    userAccountActive,
    changeLogin,
    setSidebarAction,
    collapseSidebar,
    mainTabs,
  } = agency;

  // const selectMainComponent = () => {
  //     switch (mainTabs) {
  //         case "accounts": return <MainAccountContent />;
  //         case "broker": return <div className="allow-overflow"><Bokerpage /></div>;
  //         case "E-commerce": return <div className="allow-overflow"><EcommerceLandingPage /></div>;
  //         case "admin-analytics": return <AdminAnalytics />;
  //         case "investor": return <InvestorRelation />;
  //         case "developer": return <DeveloperTools />;
  //         case "blockcheck": return <BlockcheckMain />
  //         case "apps": return <MobileAppSetUp/>;
  //         case "appsMenu": return <AppsMenu />;
  //         case "video": return <LearnVideoPlayer />;
  //         case "article": return <ArticleComponent />;
  //         case "terminal": return <TerminalMain />
  //         case "instacrypto": return <InstaCryptoMain />;
  //         default: return;
  //     }
  // }
  const handleChange = (e) => {
    setPin(e);
  };
  const handleSubmit = () => {
    if (pin == 4141) {
      setGrantAdminAccess(true);
    } else {
      setGrantAdminAccess(false);
    }
    let a = clearTimeout(() => {
      setGrantAdminAccess(null);
      clearTimeout(a);
    }, 500);
  };

  const handleMouseDown = (e) => {
    console.log("Touch", e, adminPassword);
    if (adminPasswordRef.current === null) return;
    else if (adminPasswordRef.current.contains(e.target)) {
    } else {
      // if (refreshSession) {
      setAdminPassword(false);
      // }
    }
  };
  React.useEffect(() => {
    adminPassword
      ? window.addEventListener("mousedown", handleMouseDown)
      : window.removeEventListener("mousedown", handleMouseDown);
  }, [adminPassword]);
  return (
    <div
      style={
        playVideo || explainNavbarTerms || readArticle || showApps
          ? { position: "relative" }
          : {}
      }
      className={`dashboard-content d-flex ${
        fullScreenChat ? "hide-it-completely" : ""
      } ${collapseSidebar ? "collapse" : "compress"}`}
    >
      <div
        style={searchBox ? { width: "75%" } : { width: "100%" }}
        className="content-left"
      >
        <div
          onClick={() => setSearchBox(false)}
          className={
            searchBox || overlay
              ? "content-overlay show"
              : "content-overlay hide"
          }
        />

        <Switch>
          <Route
            path="/dashboard/broker"
            component={() => (
              <div className="allow-overflow">
                <Bokerpage />
              </div>
            )}
          />
          <Route
            path="/dashboard/market-place"
            component={() => (
              <div className="allow-overflow">
                <EcommerceLandingPage />
              </div>
            )}
          />
          <Route path="/dashboard/Blockcheck" component={BlockcheckMain} />
          <Route path="/dashboard/Instacrypto" component={InstaCryptoMain} />
          <Route path="/dashboard/Terminals" component={TerminalMain} />
          <Route path="/dashboard/BrokerApp" component={ComingSoon} />
          <Route path="/dashboard/API MAchine" component={DeveloperTools} />
          <Route path="/dashboard/DD Cloud" component={InvestorRelation} />
          <Route path="/dashboard/Nitrogen" component={AdminAnalytics} />
          <Route path="/dashboard/IOS" component={NewMobile} />
          <Route path="/dashboard/Android" component={NewMobile} />
          <Route path="/dashboard/NoAccess" component={NotAuthoised} />
          <Route path="/dashboard" component={MainAccountContent} />
        </Switch>

        {/* {selectMainComponent()} */}
        <div className={userAccountActive ? "d-none" : "d-none"}>
          <h6>
            Currently Viewing{" "}
            <img
              src={
                !otherAccountDetails?.profile_img
                  ? otherAccountDetails?.profile_img
                  : Images.face
              }
            />{" "}
            <span>{otherAccountDetails?.name}</span>
          </h6>
          <h6>
            <button
              onClick={() => {
                setCollapseSidebar(true);
                setSidebarAction({ count: 8, type: "All Users" });
              }}
            >
              ChangeUser
            </button>
            <button onClick={() => changeLogin(false, null)}>
              Clear User View
            </button>
          </h6>
        </div>
      </div>
      <div
        style={searchBox ? { width: "25%" } : { width: "0%" }}
        className="content-right"
      >
        <SearchBox />
      </div>
      <div className={refreshSession ? "dashboard-modal-new" : "d-none"}>
        <div className="session-reload-modal">
          <RefreshSession />
        </div>
      </div>
      <div
        className={preImageUpload.status ? "pre-image-upload-modal" : "d-none"}
      >
        <div className="image-viewer">
          <div className="image-container">
            <img src={preImageUpload.url} />
          </div>
          <div className="image-buttons">
            <button
              className={imageUploading ? "d-none" : ""}
              onClick={() =>
                setPreImageUpload({ status: false, url: null, image: "" })
              }
            >
              Cancel
            </button>
            <button
              className={imageUploading ? "d-none" : ""}
              onClick={() => imageUploader(preImageUpload.image)}
            >
              Next
            </button>
            <button className={!imageUploading ? "d-none" : ""}>
              Sending.......
            </button>
          </div>
        </div>
      </div>
      <div
        className={explainNavbarTerms ? "explain-more-navbar-layout" : "d-none"}
      ></div>
      <div
        className={playVideo || readArticle ? "video-player-overlay" : "d-none"}
      >
        {playVideo ? (
          <LearnVideoPlayer />
        ) : readArticle ? (
          <ArticleComponent />
        ) : null}
      </div>
      <div className={showApps ? "display-apps-menu" : ""}>
        {/* <button onClick={() => setShowApps(false)} className={showApps ? "close-app-menu" : "d-none"}>
                    <img src={Images.add} />
                </button> */}
        {showApps ? <AppsMenu /> : null}
      </div>
    </div>
  );
}
