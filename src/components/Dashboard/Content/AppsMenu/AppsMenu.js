import React from "react";
import { Link, useHistory } from "react-router-dom";
import Images from "../../../../assets/a-exporter";
import Constant from "../../../../json/constant";
import { fetchNativePlatformList } from "../../../../services/getAPIs";
import { Agency } from "../../../Context/Context";
import "./apps-menu.style.scss";
export default function AppsMenu() {
  const agency = React.useContext(Agency);
  const {
    currentApp,
    setAppDownloadConfig,
    setDifferentiator,
    setUserChatEnable,
    setCollapseSidebar,
    setMainTabs,
    setSidebarAction,
    previousMainTab,
    setShowApps,
    setAdminPassword,
    grantAdminAccess,
  } = agency;
  const [platformData, setPlatformData] = React.useState(null);

  const getNativePlatformList = async () => {
    let res = await fetchNativePlatformList(currentApp.app_code);
    console.log("data", res.data.apps[0]);
    setPlatformData(res.data.apps[0]);
  };
  const handleClick = (id) => {
    switch (id) {
      case "Blockcheck":
        setMainTabs("blockcheck");
        break;
      case "Terminals":
        setMainTabs("terminal");
        break;
      case "Instacrypto":
        setMainTabs("instacrypto");
        break;
      case "BrokerApp":
        break;
      case "Chats.io":
        setMainTabs("accounts");
        setUserChatEnable(true);
        setCollapseSidebar(true);
        return;
      case "API MAchine":
        setMainTabs("developer");
        break;
      case "DD Cloud":
        setMainTabs("investor");
        return;
    }
  };
  const handleAdmin = () => {
    setSidebarAction({ count: 6, type: "Admin" });
    if (grantAdminAccess) {
      setCollapseSidebar(true);
      setMainTabs(previousMainTab);
    } else {
      setAdminPassword(true);
    }
  };
  const handleNativePlatform = (obj) => {
    // window.open(`https://${platformData[obj.link]}`, '_blank')
    switch (obj.name) {
      case "IOS":
        setMainTabs("apps");
        setAppDownloadConfig(false);
        break;
      case "Android":
        setMainTabs("apps");
        setAppDownloadConfig(true);
        break;
      case "Windows":
        window.open(`https://${platformData[obj.link]}`, "_blank");
        break;
      case "Mac":
        window.open(`https://${platformData[obj.link]}`, "_blank");
        break;
      case "Linux":
        window.open(`https://${platformData[obj.link]}`, "_blank");
        break;
    }
  };

  const handleLinkClick = (e, name, obj) => {
    switch (name) {
      case "OnHold":
        e.preventDefault();
        if (grantAdminAccess) {
          setSidebarAction({ count: 7, type: "User View" });
          setCollapseSidebar(true);
        } else {
          setAdminPassword({ status: true, which: "onhold" });
        }
        break;

      case "Chats.io":
        e.preventDefault();
        setUserChatEnable(true);
        setCollapseSidebar(true);
        setDifferentiator("chat");
        break;
      case "OnHold":
        e.preventDefault();
        if (grantAdminAccess) {
          setSidebarAction({ count: 7, type: "User View" });
          setCollapseSidebar(true);
        } else {
          setAdminPassword({ status: true, which: "onhold" });
        }
        break;
      case "Windows":
        e.preventDefault();
        window.open(`https://${platformData[obj.link]}`, "_blank");
        break;
      case "Mac":
        e.preventDefault();
        window.open(`https://${platformData[obj.link]}`, "_blank");
        break;
      case "Linux":
        e.preventDefault();
        window.open(`https://${platformData[obj.link]}`, "_blank");
        break;
    }
    if (name === "IOS") {
      setAppDownloadConfig(false);
    } else if (name === "Android") {
      setAppDownloadConfig(true);
    }
    setShowApps(false);
  };
  React.useEffect(() => {
    getNativePlatformList();
  }, []);

  return (
    <div className="apps-menu-main">
      <div className="apps-menu-header">
        <img className="app-menu-current" src={currentApp.app_icon} />
        <div>
          <h1>{currentApp.app_name} App Store</h1>
          <h6>
            Powered By <img src={Images.gxLiveIconInvert} />
          </h6>
        </div>
      </div>
      <div className="apps-menu-native-apps">
        <h6 className="apps-row-title">{currentApp.app_name} Native Apps</h6>
        <div className="platform-app-row">
          {Constant.nativePlatform.map((obj) => (
            <div
              className={`platform-wrapper ${
                !platformData?.[obj.link] ? "disable-it" : ""
              }`}
            >
              <div onClick={() => handleNativePlatform(obj)}>
                <Link
                  onClick={(e) => handleLinkClick(e, obj.name)}
                  to={`/dashboard/${obj.name}`}
                >
                  <img src={obj.icon} />
                </Link>
              </div>
              <Link
                onClick={(e) => handleLinkClick(e, obj.name, obj)}
                to={`/dashboard/${obj.name}`}
              >
                <h6>{obj.name}</h6>
              </Link>
            </div>
          ))}
        </div>
      </div>
      <div className="apps-menu-plugins">
        <h6 className="apps-row-title">GX Plugins</h6>
        <div className="apps-plugin-row">
          {Constant.appsPlugin.map((obj) => (
            <div className="apps-plugin-wrapper">
              <div style={{ backgroundColor: obj.color }}>
                <Link
                  onClick={(e) => handleLinkClick(e, obj.name)}
                  to={`/dashboard/${obj.name}`}
                >
                  <img src={obj.icon} />
                </Link>
              </div>
              <Link
                onClick={(e) => handleLinkClick(e, obj.name)}
                to={`/dashboard/${obj.name}`}
              >
                <h6>{obj.name}</h6>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
