import React, { useState, useContext, useRef, useEffect } from 'react'
import './ledger-table.style.scss'

import nextId from 'react-id-generator'

import SwitchComponent from '../../../common-components/switch/Switch'
import Images from '../../../../assets/a-exporter'
import Constant from '../../../../json/constant'
import Dropdown from '../../../common-components/Dropdown/Dropdown'
import { Agency } from '../../../Context/Context'
import LedgerContent from './LedgerContent'
import TabHeader from '../../../common-components/tab-header/TabHeader'
export default function LegderTable({ ledger, setLedger }) {
    const drop = useRef();

    const agency = useContext(Agency);
    const { setTableTabs, tableTabs, handleAnalytics, assetClassSelected, operationsOnLedgerList, analyticsType } = agency
    const [dropdown, setDropdown] = useState({ status: false, title: "" });
    const [tempArray, setTempArray] = useState([]);
    const [currentAnalystTab, setCurrentAnalystTab] = useState([]);

    const handleClick = (obj) => {
        if (tempArray.includes(obj.name)) {
            alert("Already Opened");
            return;
        }
        operationsOnLedgerList(obj.name)
        setTempArray([...tempArray, obj.name])
        let tempObj = { keyId: nextId(), ...obj }
        setDropdown({ ...dropdown, status: false })
        setTableTabs([...tableTabs, tempObj])
    }
    useEffect(() => {
        let temp = Constant.analyticsList.find(obj => { return obj.display === analyticsType })
        if (temp) {
            setCurrentAnalystTab([temp])
        } else {
            setCurrentAnalystTab([])
        }
    }, [analyticsType])
    useEffect(() => {
        console.log("kkk", assetClassSelected)
        window.addEventListener("mousedown", handleMouse);
        return () => window.removeEventListener("mousedown", handleMouse);
    }, [assetClassSelected])

    const handleMouse = (e) => {
        if (e.target.className === "dropdown-item")
            return
        if (drop.current?.contains(e.target || e.target.className === "dropdown-item")) {
        } else {
            setDropdown({ status: false, title: "" })
        }
    }
    const handleDefault = () => {
        handleAnalytics("")
    }

    return (
        <div className="ledger-table">
            <div className="ledger-table-header">
                {/* <div className="d-flex wrapper"> */}
                <div onClick={() => handleDefault()} className="header-first-child position-relative">
                    <Dropdown
                        status={dropdown}
                        icon={assetClassSelected.coinImage}
                        title={assetClassSelected.coinSymbol}
                        setDropdown={() => setDropdown({ status: true, title: assetClassSelected.coinSymbol })}
                        list="tableDropdown" status={dropdown}
                        onClick={(val) => handleClick(val)}
                        onClickTwo={() => operationsOnLedgerList()}
                        off={!ledger}
                    />
                </div>
                <div ref={drop} className="header-second-child">
                    {
                        (ledger ? tableTabs : currentAnalystTab).map(obj =>
                            <div key={obj.keyId} className="tab-header">
                                {
                                    ledger ? <Dropdown
                                        status={dropdown}
                                        icon={assetClassSelected.coinImage}
                                        title={obj.display}
                                        setDropdown={() => setDropdown({ status: true, title: obj.display })}
                                        list={obj.listId} status={dropdown}
                                        onClick={(val) => handleClick(val)}
                                        onClickTwo={() => operationsOnLedgerList(obj.name)}
                                    />
                                        :
                                        <TabHeader icon={obj.icon} title={obj.display} />
                                }
                            </div>
                        )

                    }
                </div>
                {/* </div> */}
                <h6 className="header-third-child"><span style={ledger ? { opacity: 0.35 } : {}}>Controller</span> <SwitchComponent trigger={ledger} onClick={() => setLedger(!ledger)} /> <span style={!ledger ? { opacity: 0.35 } : {}}>Ledger</span></h6>
            </div>
            {/* <LedgerContent /> */}
        </div>
    )
}
