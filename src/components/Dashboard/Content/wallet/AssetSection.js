import React, { useContext, useEffect, useState, useRef } from 'react'
import './asset-section.style.scss'
import Constant from '../../../../json/constant'
import AssetClassCell from '../../../common-components/AssetClassCell'
import Images from '../../../../assets/a-exporter'
import { Agency } from '../../../Context/Context';
const AssetSection = React.memo(() => {
    const slider = useRef();
    const [coinList, setCoinList] = useState([]);
    const [sliderWidth, setSliderWidth] = useState(0)
    const [netWorth, setNetWorth] = useState(0);
    const [leftScroll, setLeftScroll] = useState(false);
    const [rightScroll, setRightScroll] = useState(true);
    const [searchTerm, setSearchTerm] = useState('');
    const agency = useContext(Agency);
    const [clicked, setClicked] = useState(false);
    const { assetClassSelected, collapseSidebar, scrollValue, decimalValidation,
        mainBalance, marketSum, tradeState, allMarketCoins, onlyEarningSum,
        mainSum, mainInterestSum, assetSelected, bondSum, earningSum, onlyBondsSum,
        setAssetClassSelected, valueFormatter, setAssetSelected,
        setNetworkController, netWorthController } = agency;

    useEffect(() => {
        if (scrollValue) {
            handleDynamicScroll();
        } else {
            slider.current.scroll({ top: 0, left: 0, behavior: "smooth" })
        }
    }, [scrollValue])


    useEffect(() => {
        if (!assetSelected) {
            setNetWorth(valueFormatter(mainSum.fiat + earningSum + bondSum + mainSum.crypto + mainInterestSum, "USD"))
            setCoinList([]);
            return;
        }
        switch (assetSelected?.name) {
            case "Forex Currencies":
                !mainBalance.fiat[0] || tradeState ? setAssetSelected(null) : setAssetClassSelected(mainBalance.fiat[0])
                setNetWorth(valueFormatter(mainSum.fiat, "USD"))
                setCoinList([...mainBalance.fiat]); break;
            case "Cryptocurrency":
                !mainBalance.crypto[0] || tradeState ? setAssetSelected(null) : setAssetClassSelected(mainBalance.crypto[0])
                setNetWorth(valueFormatter(mainSum.crypto, "USD"))
                setCoinList([...mainBalance.crypto]); break;
            case "Earnings":
                !allMarketCoins.asset[0] || tradeState ? setAssetSelected(null) : setAssetClassSelected(allMarketCoins.asset[0])
                setNetWorth(valueFormatter(onlyEarningSum, "USD"))
                setCoinList([...allMarketCoins.asset]); break;
            case "Bonds":
                !allMarketCoins.bonds[0] || tradeState ? setAssetSelected(null) : setAssetClassSelected(allMarketCoins.bonds[0])
                setNetWorth(valueFormatter(onlyBondsSum, "USD"));
                setCoinList([...allMarketCoins.bonds]); break;
            case "Market":
                !allMarketCoins.market[0] || tradeState ? setAssetSelected(null) : setAssetClassSelected(allMarketCoins.market[0])
                setNetWorth(valueFormatter(marketSum, "USD"));
                setCoinList([...allMarketCoins.market]); break;
                break;
            default:
                setNetWorth(valueFormatter(mainSum.fiat + marketSum + earningSum + bondSum + mainSum.crypto + mainInterestSum, "USD"))
                setCoinList([]); break;
        }
    }, [assetSelected, tradeState, mainBalance, bondSum, earningSum]);
    const getSumValue = (type) => {
        switch (type) {
            case "Liquid": return valueFormatter(mainSum.fiat + mainSum.crypto, "USD");
            case "Earnings": return valueFormatter(earningSum, "USD");
            case "Bonds": return valueFormatter(bondSum, "USD");
            default: break;
        }
    }
    useEffect(() => {
        if (!assetSelected)
            setSliderWidth(slider.current?.offsetWidth)

    }, [assetSelected])
    const handleDynamicScroll = () => {
        // let del = scrollValue / 4;
        // alert(slider.current.offsetWidth)
        let total = (sliderWidth * (1 / 4)) * (scrollValue - 1);
        slider.current.scroll({ top: 0, left: sliderWidth + total - 1, behavior: "smooth" })
    }
    const handleClickTwo = (obj) => {
        if (obj.keyId === netWorthController?.keyId && clicked) {
            setNetworkController(null);
            setClicked(false)
        } else {
            setNetworkController(obj);
            setClicked(true)
        }
    }
    const handleHover = (obj, enter) => {
        if (clicked) {
            return;
        }
        else {
            if (enter) {
                setNetworkController(obj);
            }
            else {
                setNetworkController(null);
            }
        }
    }
    const checkPositions = (num) => {
        if (num > 100) {
            setLeftScroll(true)
            if (num >= slider.current.scrollWidth - slider.current.offsetWidth - 100) {
                setRightScroll(false);
            } else {
                setRightScroll(true);
            }
        } else {
            setLeftScroll(false)
        }

    }
    const handleWheel = (e) => {
        checkPositions(slider.current.scrollLeft)

        slider.current.scroll({ top: 0, left: slider.current.scrollLeft + ((e.deltaY / Math.abs(e.deltaY)) * (slider.current.offsetWidth * 0.5)), behavior: "smooth" })
    }
    const handleLeftScroll = () => {
        slider.current.scroll({ top: 0, left: slider.current.scrollLeft - (slider.current.offsetWidth * 0.75), behavior: "smooth" });
        checkPositions(slider.current.scrollLeft - slider.current.offsetWidth)
    }
    const handleRightScroll = () => {
        slider.current.scroll({ top: 0, left: slider.current.scrollLeft + (slider.current.offsetWidth * 0.75), behavior: "smooth" })
        checkPositions(slider.current.scrollLeft + slider.current.offsetWidth)
    }
    return (
        <div className="asset-section-main position-relative">
            <div className="asset-section-left">
                <div className="a-s-f-wrapper">
                    <img src={assetSelected.icon} />
                    <div>
                        <input value={searchTerm} onChange={e => setSearchTerm(e.target.value.toLowerCase())} placeholder="Search for Vault" />
                        <img src={Images.searchDark} />
                    </div>
                </div>
            </div>
            <div className="asset-section-right">

                <div onClick={() => handleLeftScroll()} className={leftScroll && assetSelected !== null ? "coin-move-left" : "d-none"}>
                    <img src={Images.leftArrowNew} />
                </div>
                <div style={collapseSidebar ? { right: '0%' } : {}} onClick={() => handleRightScroll()} className={rightScroll && assetSelected !== null ? "coin-move-right" : "d-none"}>
                    <img src={Images.rightArrowNew} />
                </div>
                <div onWheel={handleWheel} ref={slider} className={`${collapseSidebar ? "asset-collapse-wrapper__collapse" : ""} asset-collapse-wrapper`}>
                    {
                        assetSelected !== null && !coinList.length ?
                            <h5>No Assets Found</h5>
                            :
                            coinList.filter(x => { return x.coinSymbol.toLowerCase().startsWith(searchTerm) || x.coinName.toLowerCase().startsWith(searchTerm) }).map(obj =>
                                <div key={obj._id + "assets"} className={`asset-wrapper ${collapseSidebar ? "asset-wrapper__collapse" : ""}`}>
                                    <div onClick={() => { setAssetClassSelected(obj) }} className={`asset-wrapper-child ${assetClassSelected?._id === obj?._id ? "selected-asset" : ""}`}>
                                        <h5>
                                            <img loading="lazy" src={obj.coinImage} />
                                            <span className="asset-value">{decimalValidation(!obj.coinValue ? 0 : obj.coinValue, obj.coinSymbol) ? parseFloat(obj.coinValue).toFixed(6) : valueFormatter(!obj.coinValue ? 0 : obj.coinValue, obj.coinSymbol)}</span>
                                        </h5>
                                        <p>{obj.coinName}</p>
                                    </div>
                                </div>
                            )
                    }
                </div>
                <span
                    className={assetSelected !== null && assetClassSelected === null ? 'select-asset-text' : 'd-none'}
                >
                    Please Select One Of Your {assetSelected?.name} Vaults <img alt="" src={Images.triangle} />
                </span>
            </div>
        </div>
    )
})
export default AssetSection