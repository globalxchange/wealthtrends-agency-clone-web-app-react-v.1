import React, { useContext, useEffect, useState } from "react";
import QRCode from "qrcode.react";
import "./deposit-step-two.style.scss";
import { Agency } from "../../../../../Context/Context";
import Images from "../../../../../../assets/a-exporter";
import {
  getBTCAddress,
  getTronAddress,
} from "../../../../../../services/postAPIs";
export default function DepositStepTwo({}) {
  const agency = useContext(Agency);
  const [copied, setCopied] = useState(false);
  const [qrlink, setQrLink] = useState("");
  const [load, setLoad] = useState(false);
  const { setCopiedOverlay, assetClassSelected, currentApp } = agency;
  useEffect(() => {
    console.log("address", assetClassSelected);
    if (
      assetClassSelected.coinSymbol === "BTC" ||
      assetClassSelected.coinSymbol === "TRX"
    ) {
      setQrLink("");
    } else {
      console.log(assetClassSelected);
      if (assetClassSelected.native_deposit)
        setQrLink(assetClassSelected?.coin_address);
      else setQrLink(assetClassSelected?.coin_address);
    }
  }, [assetClassSelected]);

  const handleCopy = () => {
    navigator.clipboard.writeText(qrlink);
    setCopied(true);
    let a = setTimeout(() => {
      setCopied(false);
      clearTimeout(a);
    }, 2000);
  };
  const getAddress = async () => {
    if (
      assetClassSelected.coinSymbol !== "BTC" &&
      assetClassSelected.coinSymbol !== "TRX"
    )
      return;
    setLoad(true);
    let addressData;
    if (assetClassSelected.coinSymbol === "BTC") {
      addressData = await getBTCAddress(currentApp.app_code);
    } else {
      let temp = {
        email: localStorage.getItem("userEmail"),
        app_code: localStorage.getItem("appCode"),
        coin: "TRX",
      };
      addressData = await getTronAddress(temp);
    }

    setQrLink(addressData.data.address);
    setLoad(false);
  };
  return (
    <div className="deposit-step-two">
      {/* <button onClick={() => setStepDeposit(0)} className="step-two-back-button"><img src={Images.triangle} /></button> */}
      <div className="qr-code-wrapper">
        <h5>Deposit {assetClassSelected.coinName}</h5>
        <QRCode
          style={{
            height: "50%",
            width: "50%",
            opacity: qrlink === "" ? 0.35 : 1,
          }}
          value={qrlink}
        />
        {qrlink === "" ? (
          <div className="pl-0">
            <button onClick={() => getAddress()}>
              {load ? "Fetching Address...." : "Click Here To Get Address"}
            </button>
          </div>
        ) : (
          <div>
            <p>{qrlink}</p>
            <span
              className="copy-wrapper"
              onClick={() => {
                setCopiedOverlay({
                  status: true,
                  title: `${assetClassSelected.coinName} Address`,
                  data: qrlink,
                });
                handleCopy();
              }}
            >
              <img src={Images.copy} />
              <span className={copied ? "" : "d-none"}>copied</span>
            </span>
          </div>
        )}
      </div>
    </div>
  );
}
