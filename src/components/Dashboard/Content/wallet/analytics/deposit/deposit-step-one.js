import React, { useContext } from 'react'
import { Agency } from '../../../../../Context/Context'
import Constant from '../../../../../../json/constant'

import './deposit-step-one.style.scss'
export default function DepositStepOne({handleClick}) {
    const agency = useContext(Agency);
    const { name } = agency.assetSelected;
    return (
        <div className={`deposit-crypto ${name === "Forex" ? "d-none" : ""}`}>
            {
                Constant.depositSection.map(obj =>
                    <div onClick ={()=>handleClick(obj.text)} className="crypto-cards" key={obj.keyId}>
                        <h6>{obj.text}</h6>
                        <img src={obj.image} />
                    </div>
                )
            }
        </div>
    )
}
