import React from 'react'
import './withdrawal-step-one.scss'

import Constant from '../../../../../../json/constant'
export default function WithdrawalStepOne() {
    return (
        <div className="analytics-withdrawal">
            {
                Constant.withdrawtSection.map(obj =>
                    <div key={obj.keyId}>
                        <h6>{obj.text}</h6>
                        <img src={obj.image} />
                    </div>)
            }
        </div>
    )
}
