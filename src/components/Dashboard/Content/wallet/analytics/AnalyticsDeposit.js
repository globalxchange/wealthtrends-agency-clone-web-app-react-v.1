import React, { useContext,useState, useEffect } from 'react'
import './analytics-deposit.scss'

import Images from '../../../../../assets/a-exporter'
import { Agency } from '../../../../Context/Context'
import Constant from '../../../../../json/constant'
import { getAppLinks } from '../../../../../services/getAPIs'
import DepositStepOne from './deposit/deposit-step-one'
import DepositStepTwo from './deposit/deposit-step-two'
import DownloadInstaCrypto from './download-insta-crypto/DownloadInstaCrypto'
import DepositMain from './deposit/deposit-main'
export default function AnalyticsDeposit() {
    const agency = useContext(Agency);
    const { name } = agency.assetSelected;
    const [stepDeposit, setStepDeposit] = useState(0)



    const selectStep = () =>{
        if(name === "Forex"){
            return <DownloadInstaCrypto />
        }
        else if(name === "Earnings"){

        }
        else
        {
        switch(stepDeposit){
            case 0: return <DepositStepOne handleClick= {(name)=>handleClick(name)} />;
            case 1: return <DepositStepTwo setStepDeposit={setStepDeposit} />;
            default: return;
        }
    }
    }
    const handleClick = (name) =>{
        switch(name){
            case "External": setStepDeposit(1);
            default: return;
        }
    }

    return (
        <div className="analytics-deposit">
            <DepositMain />

        </div>
    )
}
