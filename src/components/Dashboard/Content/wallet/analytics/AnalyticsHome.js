import React, { useRef, useEffect, useState } from 'react'
import './analytics-home.style.scss'
import { BarChart, XAxis, YAxis, Bar, Legend, ResponsiveContainer } from 'recharts'
import NetWorthCard from '../../../../common-components/NetWorthCard/NetWorthCard'
export default function AnalyticsHome() {
    const barChartArea = useRef()
    const [size, setSize] = useState({ height: 0, width: 0 })

    useEffect(() => {
        setSize({ width: barChartArea.current?.offsetWidth, height: barChartArea.current?.offsetHeight })

    }, [barChartArea.current?.offsetWidth])
    return (
        <div className="analytics-home">
            <div className="analytics-home-left">
                <div ref={barChartArea} className=" d-flex w-100 h-100">
                    <ResponsiveContainer>
                        <BarChart width={barChartArea.current?.offsetWidth} height={barChartArea.current?.offsetHeight} data={data}>
                            <XAxis fontSize={12} axisLine={false} tickLine={false} dataKey="name" />
                            <YAxis fontSize={12} axisLine={false} tickLine={false} />
                            {/* <Legend /> */}
                            <Bar dataKey="pv" fill="#f1f1f1" />
                        </BarChart>
                    </ResponsiveContainer>


                </div>
            </div>
            <div className="analytics-home-right d-flex flex-wrap ">
                {
                    [1, 2, 3, 4, 5, 6].map(item => <NetWorthCard />)
                }

            </div>
        </div>
    )
}

const data = [
    {
        "name": "Feb 1st 2020",
        "uv": 4000,
        "pv": 20
    },
    {
        "name": "Feb 2nd 2020",
        "uv": 3000,
        "pv": 40
    },
    {
        "name": "Feb 3rd 2020",
        "uv": 2000,
        "pv": 30
    },
    {
        "name": "Feb 4th 2020",
        "uv": 2780,
        "pv": 70
    },
    {
        "name": "Feb 5th 2020",
        "uv": 2780,
        "pv": 90
    }
]