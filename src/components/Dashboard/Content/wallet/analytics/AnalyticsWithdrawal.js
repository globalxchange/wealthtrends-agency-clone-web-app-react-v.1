import React, { useContext, useState } from 'react'
import './analytics-withdrawal.style.scss'
import Constant from '../../../../../json/constant'
import { Agency } from '../../../../Context/Context'
import DownloadInstaCrypto from './download-insta-crypto/DownloadInstaCrypto';
import WithdrawalStepOne from './withdrawal/WithdrawalStepOne';
export default function AnalyticsWithdrawal() {
    const agency = useContext(Agency);
    const { name } = agency.assetSelected;
    const [stepWithdraw, setStepWithdraw] = useState(0);

    const selectComponent = () =>{
        if(name === "Forex")
        {
            return <DownloadInstaCrypto />
        }else{
           return <WithdrawalStepOne />
        }
    }
    return (
        <div className="w-100 h-100">
            {selectComponent()}

        </div>
        
    )
}
