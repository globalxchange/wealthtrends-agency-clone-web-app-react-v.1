import React, { useState, useEffect } from 'react'
import './download-insta-crypto.scss'
import Images from '../../../../../../assets/a-exporter'
import { getAppLinks } from '../../../../../../services/getAPIs';
import { Agency } from '../../../../../Context/Context';
import Constant from '../../../../../../json/constant'
export default function DownloadInstaCrypto() {
    const [link, setLink] = useState({ ioslink: '', androidlink: '' });
    const agency = React.useContext(Agency);
    const { setAssetSelected, setStepMain, analyticsType,setCheckedId, setToTransfer, setInstaCryptoSupportedAssets, setSelectedTransfer, allApps } = agency;
    useEffect(() => {
        getAppLinks()
            .then(res => {
                setLink(res.data.data[0].formData);
            })
    }, [])

    const handleTransfer = (transfer) => {

        if (analyticsType === "Deposit") {
            let instaCrypto = allApps.find(obj => obj.app_code === "instacrypto");
            setInstaCryptoSupportedAssets(instaCrypto.app_code, instaCrypto.profile_id)
            setAssetSelected(Constant.navbarList[1]);
            setCheckedId(Constant.navbarList[1].id)
            setToTransfer(instaCrypto);
            setStepMain(1)
        }else{
            let instaCrypto = allApps.find(obj => obj.app_code === "instacrypto");
            setInstaCryptoSupportedAssets(instaCrypto.app_code, instaCrypto.profile_id)
            setAssetSelected(Constant.navbarList[1]);
            setCheckedId(Constant.navbarList[1].id)
            // setSelectedTransfer(Constant.withdrawSection[1])
            setToTransfer(instaCrypto);
            setStepMain(1)
        }
    }
    return (
        <React.Fragment>
            <div className="w-100 h-100 d-flex">
                <div className={`deposit-left`}>
                    <img src={Images.instaCrypto} />
                </div>
                <div className={`deposit-right`}>
                    <div className="deposit-right-top">
                        <div className="logo-wrapper">
                            <img src={Images.instaCryptoLogo} />
                        </div>
                        <div className="button-wrapper">
                            <button onClick={() => window.open(`https://${link.androidlink}`, '_blank')}><img src={Images.android} /> Android</button>
                            <button onClick={() => window.open(`https://${link.ioslink}`, '_blank')}><img src={Images.apple} /> IOS</button>
                        </div>
                    </div>
                    <div className="deposit-right-bottom">
                        <h2>Got Fiat? We Got You Covered</h2>
                        <p>We Have Partnered With InstaCrypto.com For All Fiat To Crypto Transactions. There App Connects You To All The Local & Global Vendors Who Can Turn Your Fiat To Crypto.</p>
                        <div className="deposit-bottom-button-wrapper">
                            <button onClick={() => handleTransfer(true)}>I Have Got My Funds In My InstaCrypto</button>
                            <button onClick={() => handleTransfer(false)}>I Want To Transfer Fiat To Another App</button>

                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}
