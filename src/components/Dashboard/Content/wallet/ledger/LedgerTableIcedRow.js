import React, { useContext, useEffect } from 'react'
import './ledger-table-row.scss'
import { Agency } from '../../../../Context/Context'

export default function LedgerTableIcedRow({ content, onClick, selectedData, expand, image, keyId }) {
    const agency = useContext(Agency);
    const { valueFormatter, nameImageList, decimalValidation, coinInitials } = agency;
    React.useEffect(() => {
        console.log("Content", content)
    }, [content])
    return (
        <div
            onClick={() => selectedData?._id === content._id ? onClick(!expand) : onClick(true)}
            key={keyId} className={`iced-ledger-table-row ${selectedData?._id === content._id ? "selected-row" : ""}`}
        >
            <h6 ><img src={image} />
                {
                    !expand ?
                        content.withdraw ? "Withdrawal Of Earnings" : "Liquid Interest Payment"
                        : content.date + " EST"
                }
            </h6>
            <h6
                style={expand ? { width: "40%", textAlign: "left" } : {}}
            >
                {

                    expand ?
                        decimalValidation(content.credited_interest, content.coin)
                            ?
                            parseFloat(content.credited_interest).toFixed(6)
                            :
                            valueFormatter(content.credited_interest, content.coin)
                        :
                        content.date + " EST"
                }
            </h6>
            <h6
                className={expand ? "d-none" : ""} >
                {
                    decimalValidation(content.credited_interest, content.coin)
                        ?
                        parseFloat(content.credited_interest).toFixed(6)
                        :
                        valueFormatter(content.credited_interest, content.coin)}
            </h6>
            <h6 className={expand ? "d-none" : content.withdraw ? "negative" : ""}>{content.withdraw ? content.amount : valueFormatter(0, content.coin)}</h6>
            <h6 className={expand ? "d-none" : ""}>{decimalValidation(content.updated_interest, content.coin) ? parseFloat(content.updated_interest).toFixed(6) : valueFormatter(content.updated_interest, content.coin)}</h6>
        </div>
    )
}
const coins = {
    ETH: 'Ethereum',
    BTC: 'Bitcoin',
    USDT: 'Tether'
}