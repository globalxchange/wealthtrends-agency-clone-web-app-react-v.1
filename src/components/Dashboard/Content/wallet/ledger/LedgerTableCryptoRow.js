import React, { useContext, useEffect } from 'react'
import './ledger-table-row.scss'
import { Agency } from '../../../../Context/Context'

export default function LedgerTableCryptoRow({ content, selectedData, onClick, expand, image, keyId }) {
    const agency = useContext(Agency);
    const { valueFormatter, operationsOnLedgerList, decimalValidation } = agency;

    const titleFormatter = (data) => {
        const { coin, deposit, txn_type } = data;
        let title = "";
        if (txn_type === "GXTrade") {
            title += `Quick Trade ${deposit ? "(Buy Bitcoin)" : "(Sell Bitcoin)"}`;
        } else {
            switch (coin) {
                case "BTC":
                    title += "Bitcoin";
                    break;
                case "ETH":
                    title += "Ethereum";
                    break;
                case "USD":
                    title += "US Dollar";
                    break;
                case "CAD":
                    title += "Canadian Dollar";
                    break;
                case "GXT":
                    title += "Global Xchange Token";
                    break;
                default:
            }
            title += ` ${data.deposit ? "Deposit" : "Withdraw"}`
        }
        return title;
    }
    return (
        <div
            onClick={() => selectedData?._id === content._id ? onClick(!expand) : onClick(true)}
            key={keyId}
            className={`ledger-table-row ${selectedData?._id === content._id ? "selected-row" : ""}`}>
            <h6 ><img src={image} />{titleFormatter(content)} </h6>
            <h6
                // onClick={() => expand ? onClick(false) : {}}
                style={expand ? { width: "30.7%", textAlign: "left" } : {}} >
                {
                    expand ?
                        decimalValidation(content.amount, content.coin)
                            ?
                            parseFloat(content.amount).toFixed(6)
                            :
                            valueFormatter(content.amount, content.coin)
                        :
                        content.date + " EST"
                }
            </h6>
            <h6 style={content.deposit ? { color: '#2ea654',fontWeight: 600 } : {}} className={expand ? "d-none" : ""}
            // onClick={() => true ? onClick(!expand) : {}}
            >
                {
                    content.deposit
                        ?
                        decimalValidation(content.amount, content.coin)
                            ?
                            parseFloat(content.amount).toFixed(6)
                            :
                            valueFormatter(content.amount, content.coin)
                        : valueFormatter(0, content.coin)
                }
            </h6>
            <h6 style={!content.deposit ? { color: '#da4040',fontWeight: 600 } : {}} className={expand ? "d-none" : ""} >
                {
                    !content.deposit
                        ?
                        decimalValidation(content.amount, content.coin)
                            ?
                            parseFloat(content.amount).toFixed(6)
                            :
                            valueFormatter(content.amount, content.coin)
                        :
                        valueFormatter(0, content.coin)
                }
            </h6>
            <h6 className={expand ? "d-none" : ""}>{valueFormatter(content.updated_balance, content.coin)}</h6>
        </div>
    )
}
