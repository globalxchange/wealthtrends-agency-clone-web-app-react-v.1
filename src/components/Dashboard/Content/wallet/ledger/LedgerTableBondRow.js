import React from 'react'
import './ledger-table-row.scss';
import { Agency } from '../../../../Context/Context'
export default function LedgerTableBondRow({ content, selectedData, onClick, expand, image, keyId }) {
    const agency = React.useContext(Agency);
    const { valueFormatter, decimalValidation, assetClassSelected } = agency;
    return (
        <div
            onClick={() => selectedData?._id === content._id ? onClick(!expand) : onClick(true)}
            key={keyId} className={`iced-ledger-table-row ${selectedData?._id === content._id ? "selected-row" : ""}`}
        >
            <h6 ><img src={image} />
                {
                    !expand ?
                        `Interest Payment From Bond ${content.contract_id}` :
                        content.date + " EST"
                }
            </h6>
            <h6
                style={expand ? { width: "40%", textAlign: "left" } : {}}
            >
                {

                    expand ?
                        decimalValidation(content.credited_interest, content.coin)
                            ?
                            parseFloat(content.credited_interest).toFixed(6)
                            :
                            valueFormatter(content.credited_interest, content.coin)
                        :
                        content.date + " EST"
                }
            </h6>
            {/* <h6>{valueFormatter(content.current_interest, content.coin)}</h6> */}

            <h6 style={!content.withdraw ? { color: '#2ea654',fontWeight: 600 } : {}}
                // onClick={() => true ? onClick(!expand) : {}}
                className={expand ? "d-none" : ""} >
                {
                    content.withdraw ?
                        valueFormatter(0, content.coin)
                        :
                        decimalValidation(content.credited_interest, content.coin)
                            ?
                            parseFloat(content.credited_interest).toFixed(6)
                            :
                            valueFormatter(content.credited_interest, content.coin)
                }
            </h6>
            <h6 style={content.withdraw ? { color: '#da4040',fontWeight: 600 } : {}} className={expand ? "d-none" : ""}>
                {
                    !content.withdraw ?
                        valueFormatter(0, content.coin)
                        :
                        decimalValidation(content.amount, content.coin)
                            ?
                            parseFloat(content.amount).toFixed(6)
                            :
                            valueFormatter(content.amount, content.coin)
                }
            </h6>
            <h6 className={expand ? "d-none" : ""}>{valueFormatter(content.updated_interest, content.coin)}</h6>
        </div>
    )
}
