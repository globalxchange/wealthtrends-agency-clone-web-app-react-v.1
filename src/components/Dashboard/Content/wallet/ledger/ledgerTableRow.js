import React, { useContext, useEffect } from 'react'
import './ledger-table-row.scss'
import { Agency } from '../../../../Context/Context'

export default function LedgerTableRow({ content, selectedData, onClick, expand, image, keyId }) {
    const agency = useContext(Agency);
    const { valueFormatter, decimalValidation } = agency;

    return (

        <div
            onClick={() => selectedData?._id === content._id ? onClick(!expand) : onClick(true)}
            key={keyId}
            className={`ledger-table-row ${selectedData?._id === content._id ? "selected-row" : ""}`}>
            <h6 ><img src={image} />{content.txn_type} </h6>
            <h6
                // onClick={() => expand ? onClick(false) : {}}
                style={expand ? { width: "30.7%", textAlign: "left" } : {}} >
                {
                    expand ?
                        decimalValidation(content.native_value, content.coin)
                            ?
                            parseFloat(content.native_value).toFixed(6)
                            :
                            valueFormatter(content.native_value, content.coin)
                        :
                        content.date + " EST"
                }
            </h6>
            <h6 style={content.deposit ? { color: '#2ea654', fontWeight: 600 } : {}} className={expand ? "d-none" : ""}
            >
                {
                    content.deposit
                        ?
                        decimalValidation(content.native_value, content.coin)
                            ?
                            parseFloat(content.native_value).toFixed(6)
                            :
                            valueFormatter(content.native_value, content.coin)
                        : valueFormatter(0, content.coin)
                }
            </h6>
            <h6 style={!content.deposit ? { color: '#da4040', fontWeight: 600 } : {}} className={expand ? "d-none" : ""} >
                {
                    !content.deposit
                        ?
                        decimalValidation(content.native_value, content.coin)
                            ?
                            parseFloat(content.native_value).toFixed(6)
                            :
                            valueFormatter(content.native_value, content.coin)
                        :
                        valueFormatter(0, content.coin)
                }
            </h6>
            <h6 className={expand ? "d-none" : ""}>{valueFormatter(content.updated_balance, content.coin)}</h6>
        </div>
    )
}








{/* <div key={keyId} className="ledger-table-row">
<h6 ><img src={image} />{content.txn_type} </h6>
<h6 ><span>({content.date})</span></h6>
<h6 >{content.credited ? valueFormatter(content.native_value, content.coin) : '-- --'}</h6>
<h6 >{!content.credited ? valueFormatter(content.native_value, content.coin) : '-- --'}</h6>
<h6>{valueFormatter(content.updated_balance, content.coin)}</h6>
</div> */}
