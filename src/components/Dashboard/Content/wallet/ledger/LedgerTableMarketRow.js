import React, { useContext, useEffect } from 'react'
import './ledger-table-row.scss'
import { Agency } from '../../../../Context/Context'

export default function LedgerTableMarketRow({ content, image, keyId }) {
    const agency = useContext(Agency);
    const { valueFormatter } = agency;
    return (
        <div key={keyId} className="market-ledger-table-row">
            <h6 ><img src={image} />{coins[content.coin] + " Interest "} <span>({content.date + " EST"})</span></h6>
            <h6 >{!content.withdraw ? `${parseFloat(content.interest_rate).toFixed(6)} %` : `${parseFloat(content.current_interest).toFixed(6)} %`}</h6>
            <h6 >{!content.withdraw ? valueFormatter(content.earned_usd_value, "USD") : valueFormatter(content.usd_value, "USD")}</h6>

            <h6>{valueFormatter(content.updated_interest, content.coinSymbol)}</h6>
        </div>
    )
}

const coins = {
    ETH: 'Ethereum',
    BTC: 'Bitcoin',
    USDT: 'Tether'
}