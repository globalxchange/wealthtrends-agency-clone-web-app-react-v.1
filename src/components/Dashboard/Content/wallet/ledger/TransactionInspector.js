import React from "react";
import Images from "../../../../../assets/a-exporter";
import LoadingAnimation from "../../../../../lotties/LoadingAnimation";
import { cryptoUSD } from "../../../../../services/getAPIs";
import NextSVG from "../../../../common-components/assetsSection/NextSVG";
import { Agency } from "../../../../Context/Context";
import "./transaction-inspector.style.scss";
export default function TransactionInspector({ type, data }) {
  const agency = React.useContext(Agency);
  const [loading, setLoading] = React.useState(true);
  // const [rate, setRate] = React.useState(0)
  const {
    decimalValidation,
    valueFormatter,
    setDifferentiator,
    collapseSidebar,
    setCollapseSidebar,
    conversionConfig,
    setUserChatEnable,
    setUpdatePortfolio,
    setCopiedOverlay
  } = agency;
  const [details, setDetails] = React.useState({
    payment: null,
    date: null,
    amount: null,
    amountUSD: null,
  });
  const findCurrentRate = async () => {
    let res = await cryptoUSD();
    setUpDetails(res.data[data?.coin]);
  };
  const setUpDetails = (rate) => {
    switch (type) {
      case "Forex Currencies":
        conversionConfig.enable
          ? setDetails({
              payment: `${data.txn_type}`,
              date: data?.date,
              amount: decimalValidation(data?.native_value, data?.coin)
                ? parseFloat(data?.native_value).toFixed(6) + " " + data?.coin
                : valueFormatter(data?.native_value, data?.coin) +
                  " " +
                  data?.coin,
              amountUSD: decimalValidation(
                data?.usd_value * conversionConfig.rate,
                conversionConfig.coin
              )
                ? parseFloat(data?.usd_value * conversionConfig.rate).toFixed(
                    6
                  ) + ` ${conversionConfig.coin}`
                : valueFormatter(
                    data?.usd_value * conversionConfig.rate,
                    `${conversionConfig.coin}`
                  ) + ` ${conversionConfig.coin}`,
              currentAmountUSD: decimalValidation(
                data.amount * rate * conversionConfig.rate,
                conversionConfig.coin
              )
                ? parseFloat(data.amount * rate * conversionConfig.rate) +
                  conversionConfig.coin
                : valueFormatter(
                    data.amount * rate * conversionConfig.rate,
                    conversionConfig.coin
                  ) + ` ${conversionConfig.coin}`,
            })
          : setDetails({
              payment: `${data.txn_type}`,
              date: data?.date,
              amount: decimalValidation(data?.native_value, data?.coin)
                ? parseFloat(data?.native_value).toFixed(6) + " " + data?.coin
                : valueFormatter(data?.native_value, data?.coin) +
                  " " +
                  data?.coin,
              amountUSD: decimalValidation(data?.usd_value, "USD")
                ? parseFloat(data?.usd_value).toFixed(6) + " USD"
                : valueFormatter(data?.usd_value, "USD") + " USD",
              currentAmountUSD: decimalValidation(data.amount * rate, "USD")
                ? parseFloat(data.amount * rate) + "USD"
                : valueFormatter(data.amount * rate, "USD") + " USD",
            });
        break;
      case "Cryptocurrency":
        conversionConfig.enable
          ? setDetails({
              payment: `${data.coin} ${
                data.deposit ? "Deposit" : "Withdrawal"
              }`,
              date: data?.date,
              amount: decimalValidation(data?.amount, data?.coin)
                ? parseFloat(data?.amount).toFixed(6) + " " + data?.coin
                : valueFormatter(data?.amount, data?.coin) + " " + data?.coin,
              amountUSD: decimalValidation(
                data?.amount_usd * conversionConfig.rate,
                conversionConfig.coin
              )
                ? parseFloat(data?.amount_usd * conversionConfig.rate).toFixed(
                    6
                  ) + ` ${conversionConfig.coin}`
                : valueFormatter(
                    data?.amount_usd * conversionConfig.rate,
                    conversionConfig.coin
                  ) + ` ${conversionConfig.coin}`,
              currentAmountUSD: decimalValidation(
                data.amount * rate * conversionConfig.rate,
                conversionConfig.coin
              )
                ? parseFloat(data.amount * rate * conversionConfig.rate) +
                  conversionConfig.coin
                : valueFormatter(
                    data.amount * rate * conversionConfig.rate,
                    conversionConfig.coin
                  ) + ` ${conversionConfig.coin}`,
            })
          : setDetails({
              payment: `${data.coin} ${
                data.deposit ? "Deposit" : "Withdrawal"
              }`,
              date: data?.date,
              amount: decimalValidation(data?.amount, data?.coin)
                ? parseFloat(data?.amount).toFixed(6) + " " + data?.coin
                : valueFormatter(data?.amount, data?.coin) + " " + data?.coin,
              amountUSD: decimalValidation(data?.amount_usd, "USD")
                ? parseFloat(data?.amount_usd).toFixed(6) + " USD"
                : valueFormatter(data?.amount_usd, "USD") + " USD",
              currentAmountUSD: decimalValidation(data.amount * rate, "USD")
                ? parseFloat(data.amount * rate) + "USD"
                : valueFormatter(data.amount * rate, "USD") + " USD",
            });
        break;
      case "Earnings":
        conversionConfig.enable
          ? setDetails({
              payment: `${
                data.withdraw ? "Debited To" : "Credited From"
              } Interest Payment`,
              date: data?.date,
              amount: decimalValidation(data?.credited_interest, data?.coin)
                ? parseFloat(data?.credited_interest).toFixed(6) +
                  " " +
                  data?.coin
                : valueFormatter(data?.credited_interest, data?.coin) +
                  " " +
                  data?.coin,
              amountUSD: decimalValidation(
                data?.credited_usd_value * conversionConfig.rate,
                conversionConfig.coin
              )
                ? parseFloat(
                    data?.credited_usd_value * conversionConfig.rate
                  ).toFixed(6) + ` ${conversionConfig.coin}`
                : valueFormatter(
                    data?.credited_usd_value * conversionConfig.rate,
                    conversionConfig.coin
                  ) + ` ${conversionConfig.coin}`,
              currentAmountUSD: decimalValidation(
                data.credited_interest * rate * conversionConfig.rate,
                conversionConfig.coin
              )
                ? parseFloat(
                    data.credited_interest * rate * conversionConfig.rate
                  ) + conversionConfig.coin
                : valueFormatter(
                    data.credited_interest * rate * conversionConfig.rate,
                    conversionConfig.coin
                  ) + ` ${conversionConfig.coin}`,
            })
          : setDetails({
              payment: `${
                data.withdraw ? "Debited To" : "Credited From"
              } Interest Payment`,
              date: data?.date,
              amount: decimalValidation(data?.credited_interest, data?.coin)
                ? parseFloat(data?.credited_interest).toFixed(6) +
                  " " +
                  data?.coin
                : valueFormatter(data?.credited_interest, data?.coin) +
                  " " +
                  data?.coin,
              amountUSD: decimalValidation(data?.credited_usd_value, "USD")
                ? parseFloat(data?.credited_usd_value).toFixed(6) + " USD"
                : valueFormatter(data?.credited_usd_value, "USD") + " USD",
              currentAmountUSD: decimalValidation(
                data.credited_interest * rate,
                "USD"
              )
                ? parseFloat(data.credited_interest * rate) + "USD"
                : valueFormatter(data.credited_interest * rate, "USD") + " USD",
            });
        break;
      case "Bonds":
        conversionConfig.enable
          ? setDetails({
              payment: `Interest Payment From Bond ${data.contract_id}`,
              date: data?.date,
              amount:
                parseFloat(data?.credited_interest).toFixed(6) +
                " " +
                data?.coin,
              amount: decimalValidation(data?.credited_interest, data?.coin)
                ? parseFloat(data?.credited_interest).toFixed(6) +
                  " " +
                  data?.coin
                : valueFormatter(data?.credited_interest, data?.coin) +
                  " " +
                  data?.coin,
              amountUSD: decimalValidation(
                data?.credited_usd_value * conversionConfig.rate,
                conversionConfig.coin
              )
                ? parseFloat(
                    data?.credited_usd_value * conversionConfig.rate
                  ).toFixed(6) + ` ${conversionConfig.coin}`
                : valueFormatter(
                    data?.credited_usd_value * conversionConfig.rate,
                    conversionConfig.coin
                  ) + ` ${conversionConfig.coin}`,
              currentAmountUSD: decimalValidation(
                data.credited_interest * rate * conversionConfig.rate,
                conversionConfig.coin
              )
                ? parseFloat(
                    data.credited_interest * rate * conversionConfig.rate
                  ) + conversionConfig.coin
                : valueFormatter(
                    data.credited_interest * rate * conversionConfig.rate,
                    conversionConfig.rate
                  ) + ` ${conversionConfig.coin}`,
            })
          : setDetails({
              payment: `Interest Payment From Bond ${data.contract_id}`,
              date: data?.date,
              amount:
                parseFloat(data?.credited_interest).toFixed(6) +
                " " +
                data?.coin,
              amount: decimalValidation(data?.credited_interest, data?.coin)
                ? parseFloat(data?.credited_interest).toFixed(6) +
                  " " +
                  data?.coin
                : valueFormatter(data?.credited_interest, data?.coin) +
                  " " +
                  data?.coin,
              amountUSD: decimalValidation(data?.credited_usd_value, "USD")
                ? parseFloat(data?.credited_usd_value).toFixed(6) + " USD"
                : valueFormatter(data?.credited_usd_value, "USD") + " USD",
              currentAmountUSD: decimalValidation(
                data.credited_interest * rate,
                "USD"
              )
                ? parseFloat(data.credited_interest * rate) + "USD"
                : valueFormatter(data.credited_interest * rate, "USD") + " USD",
            });
        break;
      default:
        return;
    }
    setLoading(false);
  };
  React.useEffect(() => {
    findCurrentRate();
  }, [data]);

  return (
    <div className="transaction-inspector">
      {loading ? (
        <div className="h-100 w-100 d-flex justify-content-center align-items-center">
          <LoadingAnimation />
        </div>
      ) : (
        <>
          <h2 style={collapseSidebar ? { overflow: "hidden" } : {}}>
            Transaction Inspector
            {/* <img onClick={() => { setCollapseSidebar(true); setUserChatEnable(true); setUpdatePortfolio(true) }} src={Images.portfolioAI} /> */}
            <button
              className={collapseSidebar ? "d-none" : ""}
              onClick={() => {
                setCollapseSidebar(true);
                setDifferentiator("profile");
                setUserChatEnable(true);
                setUpdatePortfolio(true);
              }}
            >
              <NextSVG height="20px" width="19px" type="portfolio" />
            </button>
          </h2>

          <div className="transaction-rows">
            <span>{details.date + " EST"}</span>
            <h5>{details.payment}</h5>
          </div>
          <div
            onCopy={() =>
              setCopiedOverlay({
                status: true,
                title: "Amount",
                data: details.amount,
              })
            }
            className="transaction-rows"
          >
            <span>Amount</span>
            <h5>{details.amount}</h5>
          </div>
          <div
            onCopy={() =>
              setCopiedOverlay({
                status: true,
                title: "Value In USD Then",
                data: details.amountUSD,
              })
            }
            className="transaction-rows"
          >
            <div>
              <span>Value In USD Then</span>
              <h5>{details.amountUSD}</h5>
            </div>
            <div
              onCopy={() =>
                setCopiedOverlay({
                  status: true,
                  title: "Value In USD Now",
                  data: details.currentAmountUSD,
                })
              }
            >
              <span>Value In USD Now</span>
              <h5>{details.currentAmountUSD}</h5>
            </div>
          </div>
        </>
      )}
    </div>
  );
}
