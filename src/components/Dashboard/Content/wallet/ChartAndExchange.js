import React from 'react'
import nextId from 'react-id-generator'
import './chart-and-exchange.style.scss'
import AnalyticsChart from './chart-exchange/AnalyticsChart'
import AnalyticsExchange from './chart-exchange/AnalyticsExchange'
import PairsComponent from './chart-exchange/PairsComponent'
export default function ChartAndExchange() {
    const [selectedHeader, setSelectedHeader] = React.useState(headers[0])
    const selectComponent = (obj) => {
        switch (obj.name) {
            case "Chart": return <AnalyticsChart />;
            case "Exchanges": return <AnalyticsExchange />;
            case "Pairs": return <PairsComponent />

        }
    }

    return (
        <div className="chart-and-exchange-main">
            <div className="chart-exchange-header">
                {
                    headers.map(obj =>
                        <h6
                            // onClick={() => setSelectedHeader(obj)}
                            className={obj.keyId === selectedHeader.keyId
                                ?
                                "selected-header"
                                :
                                ""
                            }
                        >{obj.name}
                        </h6>
                    )
                }

            </div>
            <div className="chart-exchange-body">
                {
                    headers.map(obj => <div className={obj.name === selectedHeader.name ? "w-100 h-100 d-flex" : "d-none"}>
                        {selectComponent(obj)}

                    </div>)
                }
            </div>

        </div>
    )
}
const headers = [
    { keyId: nextId(), name: "Pairs" },
    { keyId: nextId(), name: "Chart" },
    { keyId: nextId(), name: "Exchanges" }
]