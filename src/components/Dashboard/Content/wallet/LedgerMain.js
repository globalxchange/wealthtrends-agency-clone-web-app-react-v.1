import React, { useContext, useEffect } from 'react'
import './ledger-main.scss'
import LedgerContent from './LedgerContent'
import Pagination from '../../../common-components/pagination/Pagination'
import { Agency } from '../../../Context/Context'
export default function LedgerMain() {
    const agency = useContext(Agency)
    const {currentListLength,setLedgerOrAnalytics, setActivePage}= agency;
    React.useEffect(()=>{
        return ()=>{
            setLedgerOrAnalytics(false)
        }
    },[])
    return (
        <div className="ledger-wrapper">
            <LedgerContent />
            <div className="ledger-pagination">

                <div className="pagination-wrapper">
                    <Pagination length={currentListLength} activePage ={(val)=>setActivePage(val)} />
                </div>

            </div>

        </div>
    )
}
