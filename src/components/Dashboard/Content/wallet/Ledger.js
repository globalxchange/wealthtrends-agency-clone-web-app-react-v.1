import React, { useState, useContext } from 'react'
import './ledger.style.scss'
import LegderTable from './LegderTable'
import Analytics from './Analytics'
import LedgerMain from './LedgerMain'
import { Agency } from '../../../Context/Context'
import ControllerMainComponent from './controller/controller-main.component'
import ChartAndExchange from './ChartAndExchange'
export default function Ledger() {
    const agency = useContext(Agency);
    const { setLedgerOrAnalytics, analyticsSection, ledgerOrAnalytics, collapseSidebar } = agency
    return (
        <div style={ledgerOrAnalytics ? {} : { paddingBottom: 0 }} className={`ledger-main ${collapseSidebar ? "compress__width" : ""}`}>
            <div style={{ transition: '0.3s' }} className={`ledger-or-analytics-wrapper ${ledgerOrAnalytics ? "w-100" : analyticsSection ? "w-50" : "w-100"} h-100`}>
                <LegderTable setLedger={setLedgerOrAnalytics} ledger={ledgerOrAnalytics} />
                {
                    ledgerOrAnalytics ? <LedgerMain /> : <ControllerMainComponent />
                }

            </div>
            {
                ledgerOrAnalytics ? '' : analyticsSection ? <ChartAndExchange /> : ""
            }
        </div>
    )
}
