import React, { useState, useContext } from 'react'
import './net-worth-controller.style.scss'
import Constant from '../../../../json/constant'
import { Agency } from '../../../Context/Context'
import Images from '../../.././/../assets/a-exporter'
export default function NetWorthController() {
    const agency = useContext(Agency);
    const [timeFrame, setTimeFrame] = useState(Constant.netWorthController[0].list[0])
    const [statistics, setStatistics] = useState(Constant.netWorthController[1].list[0])
    const {netWorthController} = agency

    const handleClick = (obj, i) =>{
        i?setStatistics(obj):setTimeFrame(obj)
    }
    return (
        <div className={`net-worth-controller ${netWorthController === null?"height-none": "height-full"}`}>

            <h5>Net-Worth Controller</h5>
            {
                Constant.netWorthController.map((obj, i) =>
                    <div className="options-wrapper" key={obj.keyID}>
                        <span>{obj.title}</span>
                        <div>
                            {
                                obj.list.map(obj =>
                                    <button
                                        onClick={()=>handleClick(obj, i)}
                                        className={timeFrame.keyId === obj.keyId || statistics.keyId === obj.keyId?"selected":""}
                                        key={obj.keyId}
                                    >
                                        {obj.name}
                                    </button>
                                )
                            }
                        </div>
                    </div>
                )
            }
            <button className="calculate">Calculate <img src={Images.threeArrow}/> </button>


        </div>
    )
}
