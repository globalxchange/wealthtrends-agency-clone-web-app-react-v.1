import React, { useContext, useEffect, useState } from 'react'
import './ledger-content.scss'
import Constant from '../../../../json/constant'
import { Agency } from '../../../Context/Context'
import LedgerTableRow from './ledger/ledgerTableRow';
import LoadingAnimation from '../../../../lotties/LoadingAnimation';
import LedgerTableCryptoRow from './ledger/LedgerTableCryptoRow';
import LedgerTableIcedRow from './ledger/LedgerTableIcedRow';
import LedgerTableMarketRow from './ledger/LedgerTableMarketRow';
import TransactionInspector from './ledger/TransactionInspector';
import LedgerTableBondRow from './ledger/LedgerTableBondRow';
export default function LedgerContent() {
    const agency = useContext(Agency);
    const [coinImage, setCoinImage] = useState();
    const [selectedData, setSelectedData] = useState(null);
    const [type, setType] = useState()
    const [expand, setExpand] = useState()
    const { currencyImageList, assetSelected, assetClassSelected, selectTransactions, ledgerDisplayList, ledgerLoading, ledgerActivePage } = agency;


    useEffect(() => {
        setCoinImage(currencyImageList[assetClassSelected.coinSymbol]);
        selectTransactions();
        setType(assetSelected.name)
        setExpand(false)
    }, [assetClassSelected, assetSelected]);

    const handleExpand = (obj, val) =>{
        if(val){
            setSelectedData(obj);
            setExpand(true)
        }else{
            setSelectedData(null);
            setExpand(false)

        }

    }
    useEffect(() => {
        expand?setSelectedData(ledgerDisplayList[7 * (ledgerActivePage - 1)]):setSelectedData(null)
        
    }, [ledgerActivePage])
    

    const selectRow = (obj) => {
        switch (type) {
            case "Forex Currencies": return <LedgerTableRow selectedData={selectedData} expand={expand} onClick={(val)=>handleExpand(obj, val)} keyId={obj._id} content={obj} image={coinImage} />;
            case "Cryptocurrency": return <LedgerTableCryptoRow selectedData={selectedData} expand={expand} onClick={(val)=>handleExpand(obj, val)} keyId={obj._id} content={obj} image={coinImage} />;
            case "Earnings": return <LedgerTableIcedRow selectedData={selectedData} expand={expand} onClick={(val)=>handleExpand(obj, val)} keyId={obj._id} content={obj} image={coinImage} />;
            case "Bonds": return <LedgerTableBondRow selectedData={selectedData} expand={expand}  onClick={(val)=>handleExpand(obj, val)} keyId={obj._id} content={obj} image={coinImage}/>;
            default: return;
        }
    }
    return (
        <>
            <div className={classes[`${assetSelected.name}`]}>
                {
                    (Constant[expand?'ledgerSubheaderIcedTwo':`${objs[`${assetSelected.name}`]}`]).map(obj => <span key={obj.keyId}>{obj.name}</span>)
                }
            </div>
            <div className="ledger-table-body">
                <div className="w-100 h-100">
                    {
                        ledgerLoading ?
                            <div className="d-flex justify-content-center align-items-center h-100 w-100"><LoadingAnimation type="fetch" size={{ height: 250, width: 200 }} /></div>
                            :
                            ledgerDisplayList.length ?
                                ledgerDisplayList.slice((7 * (ledgerActivePage - 1)), (7 * ledgerActivePage)).map(obj => selectRow(obj))
                                :
                                <div className="d-flex h-100 flex-column justify-content-center align-items-center">
                                    <LoadingAnimation type="no-data" size={{ height: 150, width: 150 }} loop={false} />
                                    <h6>No Transaction Found.</h6>
                                </div>
                    }
                </div>
                {expand?<TransactionInspector type={type} data={selectedData} />:''}
            </div>
        </>
    )
}
const objs = {
    Earnings: 'ledgerSubheaderIced',
    'Forex Currencies': 'ledgerSubheader',
    Cryptocurrency: 'ledgerSubheader',
    Bonds: 'ledgerSubheaderIced',
}
const classes = {
    Earnings: 'ledger-table-sub-header-iced',
    'Forex Currencies': 'ledger-table-sub-header',
    Cryptocurrency: 'ledger-table-sub-header',
    Bonds: 'ledger-table-sub-header-iced',
    

}