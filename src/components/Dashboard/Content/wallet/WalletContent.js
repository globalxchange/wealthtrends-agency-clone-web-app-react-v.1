import React, { useContext } from 'react'
import './wallet-content.scss'
import AssetsSectionComponent from '../../../common-components/assetsSection/AssetsSection'
import { Agency } from '../../../Context/Context'
import Ledger from './Ledger'
import TradeIndex from '../../Trade/TradeIndex'
export default function WalletContent() {
    const agency = useContext(Agency);
    const {assetClassSelected,tradeState, assetSelected,netWorthController, secondGate} = agency;
    return (
        <div style={netWorthController === null?{}:{opacity: 0.35}} className="wallet-content">
            {/* {assetSelected=== null? */}
            {/* tradeState?<TradeIndex />:<AssetsSectionComponent/> */}
            {/* : */}
            {
            assetClassSelected===null || !secondGate?tradeState?<TradeIndex />:<AssetsSectionComponent/>:<Ledger/>
            }
        </div>
    )
}
