import React from "react";
import nextId from "react-id-generator";
import Images from "../../../../../assets/a-exporter";
import Constant from "../../../../../json/constant";
import {
  conversionAPI,
  conversionRateWIthNoFees,
  cryptoUSD,
  getEarningsDetails,
} from "../../../../../services/getAPIs";
import { Agency } from "../../../../Context/Context";
import { conversionRateCMC } from "../../../services/getAPIs";
import "./portfolio-ai.style.scss";
export default function PortfolioAI() {
  const [coinType, setCoinType] = React.useState();
  const agency = React.useContext(Agency);
  // const [earningDetails, setEarningDetails] = React.useState();
  const [loading, setLoading] = React.useState(false);
  const [mainLoading, setMainLoading] = React.useState(true);
  const {
    setCopiedOverlay,
    earningDetails,
    portfolioActive,
    setPortfolioActive,
    conversionConfig,
    setConversionConfig,
    valueFormatter,
    mainSum,
  } = agency;
  const decimalRegex = /^(\d*)\.?(\d){0,6}$/;
  const [config, setConfig] = React.useState({
    type: "",
    asset: "",
    coinType: "fiat",
    coin: "",
    reference: "",
    rate: "",
    text: "",
  });

  React.useEffect(() => {
    console.log("main sum", mainSum);
  }, [mainSum]);
  return loading ? (
    <div className="w-100 h-100 d-flex align-items-center pl-5">
      <h5 className="m-0">Loading....</h5>
    </div>
  ) : (
    <div className="new-portfolio-ai">
      <div>
        {portfolioActive ? (
          <button
            onClick={() => setPortfolioActive(false)}
            className="close-button"
          >
            <p>Close Portfolio</p> <img src={Images.addDark} />
          </button>
        ) : (
          <button
            onClick={() => setPortfolioActive(true)}
            className="portfolio-button"
          >
            {<img src={Images.portfolioFullLogo} />}
          </button>
        )}
        <h6
          onCopy={() =>
            setCopiedOverlay({
              status: true,
              title: "Net-Worth",
              data:valueFormatter(
                parseFloat(
                  earningDetails.total_holdings * conversionConfig?.rate
                ),
                conversionConfig?.coin
              )
            })
          }
          className="net-worth-button"
        >
          <span>Net-Worth</span>
          <h6>
            {valueFormatter(
              parseFloat(
                earningDetails.total_holdings * conversionConfig?.rate
              ),
              conversionConfig?.coin
            )}
          </h6>
        </h6>
      </div>
    </div>
  );
}
