import React from 'react'
import './terminals-navbar.style.scss'
import Images from '../../../../assets/a-exporter';
import nextId from 'react-id-generator';
import { Agency } from '../../../Context/Context';
export default function TerminalsNavbar() {
    const [dropdown, setDropdown] = React.useState(false);
    // const [selectedType, setSelectedType] = React.useState(dropdownList[0]);
    const agency = React.useContext(Agency);
    const { setTradeNavbarType, tradeNavbarType } = agency;
    return (
        <div className="terminals-navbar">
            <div className="terminals-navbar-left">
                <img src={Images.tokenSwapFull} />
            </div>
            <div className="terminals-navbar-right">
                <div className={`t-n-r-dropdown ${dropdown ? "expand-height" : ""}`}>
                    <button onClick={() => setDropdown(!dropdown)}>
                        <img className={dropdown ? "" : "turn-it-down"} src={Images.triangle} />
                    </button>
                    <div className="selected-type">
                        <img src={tradeNavbarType.icon} /><h6>{tradeNavbarType.text}</h6>
                    </div>
                    {
                        dropdownList.filter(x => { return x.id !== tradeNavbarType.id }).map(obj =>
                            <div className={["fiat", "crypto"].includes(obj.id) ? "" : "disable"} onClick={() => { setDropdown(false); setTradeNavbarType(obj) }} key={obj.keyId}>
                                <img src={obj.icon} /><h6>{obj.text}</h6>
                            </div>
                        )
                    }
                </div>
            </div>
        </div>
    )
}
const dropdownList = [
    { keyId: nextId(), text: "Forex Vault Balances", icon: Images.forexLogo, id: "fiat" },
    { keyId: nextId(), text: "Crypto Vault Balances", icon: Images.bitcoin, id: "crypto" },
    { keyId: nextId(), text: "News", icon: Images.newsLogo, id: "news" },
    { keyId: nextId(), text: "Offers", icon: Images.offersLogo, id: "offers" },
    { keyId: nextId(), text: "Signals", icon: Images.signalsLogo, id: "signal" },
]