import React from 'react'
import './header-options.style.scss'
import Constant from '../../../../../../../json/constant'
export default function HeaderOptions({ search, handleOptions, tradePair, step, check, buySell }) {

    return (
        <div className="header-options">
            {
                Constant.tradeHeaderOption.map((obj, i) =>
                    <button
                        className={step === 0 ? "initial-page" : obj.active.includes(step) ? "selected" : "not-selected"}
                        onClick={() => handleOptions(obj, i + 1)}
                        key={obj.keyId}>
                        {
                            check(i) ?
                                <React.Fragment>
                                    <img
                                    className="mr-3"
                                        src={i === 0 ? tradePair[1].image : tradePair[0].image} />
                                    {i === 0 ? tradePair[1].coin : tradePair[0].coin}
                                </React.Fragment>
                                : <React.Fragment>
                                    {buySell !== null && i ===2 ?buySell.toUpperCase():obj.display}
                                    <img className={i === 1 ? "" : "d-none"} src={search} />
                                </React.Fragment>
                        }
                    </button>
                )
            }
        </div>
    )
}
