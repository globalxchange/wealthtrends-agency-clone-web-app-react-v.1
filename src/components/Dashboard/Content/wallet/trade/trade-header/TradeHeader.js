import React, { useContext } from 'react'
import './trade-header.style.scss'
import { Agency } from '../../../../../Context/Context'
import HeaderOptions from './header-options/HeaderOptions';
import Images from '../../../../../../assets/a-exporter'
import HeaderSearch from './header-search/HeaderSearch';
export default function TradeHeader({ setSearchTerm }) {
    const agency = useContext(Agency);
    const { tradeStep, setTradeStep, tradePair, setBuySell, buySell } = agency;
    const selectHeader = () => {
        switch (tradeStep) {
            case 0:
            case 1:
            case 4:
            case 5:
            case 6:
            case 2: return <HeaderOptions search={Images.search} handleOptions={handleOptions} buySell={buySell} tradePair={tradePair} check={checking} step={tradeStep} />;
            case 3: return <HeaderSearch setSearchTerm={setSearchTerm} />
        }
    }
    const checking = (num) => {
        switch (num) {
            case 0: if (tradePair[1].coin === null)
                return false;
            else
                return true;
            case 1: if (tradePair[0].coin === null)
                return false;
            else
                return true;
            case 2: return false;
            default: return;
        }

    }
    const handleOptions = (obj, num) => {

        if (num <= tradeStep) {
            switch (obj.code) {
                case "base": setTradeStep(1); break;
                case "quote":
                    if (tradeStep === 2)
                        setTradeStep(3);
                    else
                        setTradeStep(2);
                    break;
                case "direction": setTradeStep(3); break;
                default: return;
            }
        } else if (tradeStep === 0, num === 1) {
            setTradeStep(1)

        }
    }
    return (
        <React.Fragment>
            {selectHeader()}
        </React.Fragment>
    )
}
