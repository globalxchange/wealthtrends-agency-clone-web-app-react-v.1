import React from 'react'
import './header-search.style.scss'
export default function HeaderSearch({setSearchTerm}) {
    return (
        <div className="header-search">
            <input onChange={e =>setSearchTerm(e.target.value)} autoFocus={true} placeholder="Search The Name Of The Asset You Want To Trade"/>
        </div>
    )
}
