import React, { useEffect } from 'react'

export default function FooterBack({logo, goBack}) {
    return (
        <div className="footer-back">
            <h6 onClick={goBack}><img src={logo}/>Back</h6>
            
        </div>
    )
}
