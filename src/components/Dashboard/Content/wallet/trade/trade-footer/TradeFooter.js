import React, { useContext } from 'react'
import './trade-footer.style.scss'
import { Agency } from '../../../../../Context/Context'
import FooterRow from './FooterRow';
import Images from '../../../../../../assets/a-exporter'
import FooterBack from './FooterBack';
export default function TradeFooter() {
    const agency = useContext(Agency);
    const { tradeStep, setTradeStep, exchangeAverageValue, valueFormatter, tradePair } = agency;

    const selectFooter = () => {
        switch (tradeStep) {
            case 0:
            case 1:
            case 2: return <FooterRow values={exchangeAverageValue} valueFormatter={valueFormatter} base={tradePair[1].coin}/>
            case 3: return <FooterBack logo={Images.fancyNext} goBack={() => setTradeStep(2)} />
            case 6: return <FooterRow values={exchangeAverageValue} valueFormatter={valueFormatter} base={tradePair[1].coin} />;
            default: return;

        }
    }
    return (
        <React.Fragment>
            {selectFooter()}

        </React.Fragment>
    )
}
