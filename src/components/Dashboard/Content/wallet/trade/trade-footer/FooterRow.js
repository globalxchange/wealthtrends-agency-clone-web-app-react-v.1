import React from 'react'

export default function FooterRow({ values, valueFormatter, base }) {
    return (
        <div className="footer-row">

            <span>Terminal Average</span>
            <span>{values[0]==='-'?'0.00':valueFormatter(values[0], base)}</span>
            <span>{values[0]==='-'?'0.00':valueFormatter(values[1], base)}</span>
            <span>{values[0]==='-'?'0.00':parseFloat(values[2]).toFixed(2) + '%'}</span>
        </div>
    )
}
