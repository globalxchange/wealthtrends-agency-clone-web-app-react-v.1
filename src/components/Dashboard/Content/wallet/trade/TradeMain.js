import React from 'react'
import './trade-main.style.scss'
import TradeHeader from './trade-header/TradeHeader'
import TradeBody from './trade-body/TradeBody'
import TradeFooter from './trade-footer/TradeFooter'
import { Agency } from '../../../../Context/Context'
export default function TradeMain() {
    const [searchTerm, setSearchTerm] = React.useState('');
    const agency = React.useContext(Agency);
    const {filterExchangeList, tradePair} = agency;
    React.useEffect(()=>{
        filterExchangeList()
    },[tradePair])
    return (
        <main className="trade-main">
            <nav className="trade-main-header">
                <TradeHeader setSearchTerm={setSearchTerm} />
            </nav>
            <article className="trade-main-body">
                <TradeBody searchTerm={searchTerm} />
            </article>
            <footer className="trade-main-footer">
                <TradeFooter />

            </footer>
        </main>
    )
}
