import React from 'react'
import './trade-base-exchange.style.scss'
import Constant from '../../../../../../../json/constant'
import BaseRow from './BaseRow'
import QuoteRow from './ExchangeRow'
export default function TradeBaseExchange({subHeader, list, base, onClick}) {
    return (
        <main className="trade-base-quote">
            <nav>
                {
                    subHeader.map(obj =><span>{obj.name}</span>)
                }
            </nav>
            <article >
                {
                    list.map(obj =><BaseRow onClick={()=>onClick(obj)} data ={obj} />)
                }
            </article>
            
        </main>
    )
}
