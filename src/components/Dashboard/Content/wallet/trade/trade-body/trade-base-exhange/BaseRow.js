import React, { useContext } from 'react'
import { Agency } from '../../../../../../Context/Context'

export default function BaseRow({data, onClick}) {
    const agency = useContext(Agency);
    const {valueFormatter} = agency;
    return (
        <div onClick={onClick} className="trade-base-row">
            <span><img src={`https://cryptoicons.org/api/icon/${data.coin.toLowerCase()}/30`}/>{data.coin}</span>
            <span>{valueFormatter(data.balance, data.coin)}</span>
            <span>{valueFormatter(data.USDeqv, 'USD')}</span>
            <span>--.--</span>
            <span>1517</span>
            
        </div>
    )
}
