import React from 'react'
import './trade-direction.style.scss'
export default function TradeDirection({list, handleClick,base, quote}) {
    return (
        <div className="trade-direction">
            {
                list.map(obj =>
                <div onClick ={()=>handleClick(obj)} className="trade-direction-row">
                    <h6>{obj.name}</h6>
                <span>{quote}/ {base}</span>
                </div>
                )
            }
        </div>
    )
}
