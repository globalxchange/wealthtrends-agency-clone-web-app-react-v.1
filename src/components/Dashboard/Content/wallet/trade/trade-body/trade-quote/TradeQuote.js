import React, { useRef } from 'react'
import './trade-quote.style.scss'
const TradeQuote =React.memo( ({ hide, list,setSearchTerm,baseAsset, handleList,onClick }) => {
    const quoteScroll = useRef(null)
    return (
        <main className="trade-quote">
            <div className={`trade-quote-heading ${hide ? "normal" : "extra"}`}>
                <span>Frequently Traded Against The US Dollar</span>
            </div>
            <article ref={quoteScroll} onScroll={() => handleList(quoteScroll.current)} className={`trade-quote-body ${hide ? "normal-body" : "extra-body"}`}>
                {
                    list.map(obj => <div onClick={()=>onClick(obj)} className="quote-row">
                        <h6>
                            <img src ={`https://cryptoicons.org/api/icon/${obj.coin.toLowerCase()}/30`}/>
                            {obj.coin}
                        </h6>
                        <span>
                            Compare Over {baseAsset === 'USD'?1:4} Exchanges
                        </span>
                    </div>)
                }

            </article>

        </main>
    )
})
export default TradeQuote;