import React, { useContext, useEffect, useState } from 'react'
import './trade-body.style.scss'
import { Agency } from '../../../../../Context/Context'
import TradeWelcome from './trade-welcome/TradeWelcome';
import Images from '../../../../../../assets/a-exporter'
import TradeBaseExchange from './trade-base-exhange/TradeBaseExchange';
import Constant from '../../../../../../json/constant'
import TradeQuote from './trade-quote/TradeQuote';
import TradeDirection from './trade-direction/TradeDirection';
import TradeLoading from './trade-loading/TradeLoading';
import TradeExchange from './trade-exchange/TradeExchange';
import TradeBuySellComponent from './trade-buy-sell/trade-buy-sell.component';
export default function TradeBody({ searchTerm }) {
    const agency = useContext(Agency);
    const [coinList, setCoinList] = useState([])
    const { tradeStep, setTradeStep,setSelectAsset, valueFormatter,selectAsset, getExchangeTableDataTwo, fillExchangeList, exchangeList, baseAsset, allAvailableCoins, tradePair, setTradePair, buySell, setBuySell } = agency;
    const [sliceNumber, setSliceNumber] = useState(20);

    const handleBaseSelect = (obj) => {
        setTradePair([{ ...tradePair[0] }, { coin: obj.coin, image: `https://cryptoicons.org/api/icon/${obj.coin.toLowerCase()}/30`, exchange: 0 }])
        setTradeStep(2);
    }
    const handleQuoteSelect = (obj) => {
        console.log("selected", obj)
        setTradePair([{ coin: obj.coin, image: `https://cryptoicons.org/api/icon/${obj.coin.toLowerCase()}/30`, exchange: obj.exchanges.length }, { ...tradePair[1] }])
        setTradeStep(4);
    }
    const handleList = (ref) => {
        if (ref.offsetHeight + ref.scrollTop > ref.scrollHeight - (0.5 * ref.offsetHeight)) {
            setSliceNumber(sliceNumber + 3);
        }

    }
    const handleDirection = (obj) => {
        setBuySell(obj.name.toLowerCase())
        getExchangeTableDataTwo()
        fillExchangeList()
        // setTradeStep(6);
    }
    const selectBody = () => {
        switch (tradeStep) {
            case 0: return <TradeWelcome logo={Images.tradeLogo} next={Images.fancyNext} />;
            case 1: return <TradeBaseExchange onClick={handleBaseSelect} subHeader={Constant.tradeBaseQuoteHeader} list={baseAsset} base={true} />
            case 2:
            case 3: return <TradeQuote baseAsset={tradePair[1].coin} onClick={handleQuoteSelect} hide={tradeStep === 3} handleList={handleList} list={coinList} />
            case 4: return <TradeDirection list={Constant.tradeBuySell} base={tradePair[1].coin} quote={tradePair[0].coin} handleClick={handleDirection} />
            case 5: return <TradeLoading length={exchangeList.length} base={tradePair[1].coin} logo={Images.loading} />
            case 6: return <TradeExchange headers={Constant.tradeExchangeHeader} />
            default: return;
        }
    }
    useEffect(()=>{
        setSelectAsset({ exchangeId: null, ['24Hr_Volume']: 0, fullImage: null, fullImageBW: null, imageCurrency: null, percentChange: 0, price: 0, image: null, buyPrice: 0, sellPrice: 0 })
    },[tradeStep])
    useEffect(() => {
        if (tradeStep === 3) {
            let temp = allAvailableCoins.filter(obj => { return obj.coin.toLowerCase().startsWith(searchTerm.toLowerCase()) && searchTerm !== '' && obj.coin !== tradePair[1].coin });
            setCoinList([...temp]);

        } else {
            let temp = allAvailableCoins.slice(0, sliceNumber).filter(obj => { return obj.coin !== tradePair[1].coin });
            setCoinList([...temp]);
        }

    }, [sliceNumber, tradePair, searchTerm, tradeStep])
    return (
        <React.Fragment>
            <main className="trade-body-main">
                <div className={`trade-main-left ${buySell !== null && tradePair[0].coin !== null && tradeStep === 6 && selectAsset.exchangeId !== null?"compromised":"full"}`}>
                    {selectBody()}
                </div>
                <div className={`trade-main-right ${buySell !== null && tradePair[0].coin !== null && tradeStep === 6 && selectAsset.exchangeId !== null?"trade":"no-trade"}`}>
                    <TradeBuySellComponent />
                </div>
            </main>
        </React.Fragment>
    )
}
