import React, {useState, useContext} from 'react';
import './trade-buy-sell.style.scss';
import nextId from "react-id-generator";
import MarketComponent from "./market/market.component";
import { Agency } from '../../../../../../Context/Context';
const TradeBuySellComponent = () =>{
    const agency = useContext(Agency)
    const [selectHeader, setSelectHeader] = useState(header[1]);
    return(
        <div className="buy-sell">
            <div className="buy-sell-header">
                {
                    header.map(obj =><button className={selectHeader.name === obj.name?"selected":""} key={obj.keyId}>{obj.name}</button>)
                }
            </div>
            <div className="buy-sell-body">
                <MarketComponent/>

            </div>

        </div>
    )
};

const header = [
    {keyId: nextId(), name: "Limit"},
    {keyId: nextId(), name: "Market"}
];

export default TradeBuySellComponent;
