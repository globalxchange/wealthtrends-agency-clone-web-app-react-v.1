import React, { useEffect, useRef } from 'react'
import socketIOClient from 'socket.io-client';
import './trade-exchange.style.scss'
import { Agency } from '../../../../../../Context/Context'
export default function TradeExchange({ headers }) {
    let tradeWS = React.useRef();
    const displayListRef = React.useRef();
    const [displayList, setDisplayList] = React.useState([]);
    const [count, setCount] = React.useState(0);
    const first = useRef(0);
    const second = useRef(0);
    const third = useRef(0);
    const fourth = useRef(0);

    const agency = React.useContext(Agency)
    const { exchangeList, numbers,setSelectAsset,selectAsset, tradePair, valueFormatter, setNumbers, exchangeListUpdate, setUpdate3, update3 } = agency;
    React.useEffect(() => {

    }, [exchangeList])

    useEffect(() => {
    }, [update3])
    useEffect(() => {
        displayListRef.current = [...exchangeList];
        setDisplayList([...exchangeList]);
        let temp = { Bitfinex: null, "Binance": null, "Kraken": null, "Okex": null };
        exchangeList.map((obj, i) => {
            if (obj.exchangeId === 'Bitfinex')
                temp = { ...temp, Bitfinex: i };
            if (obj.exchangeId === 'Binance')
                temp = { ...temp, Binance: i };
            if (obj.exchangeId === 'Kraken')
                temp = { ...temp, Kraken: i };
            if (obj.exchangeId === 'Okex')
                temp = { ...temp, Okex: i };
            if (obj.exchangeId === "ZB")
                temp = { ...temp, ZB: i };
            if (obj.exchangeId === "Bitmax")
                temp = { ...temp, Bitmax: i }
        })
        setNumbers({ ...temp });
    }, [exchangeList, exchangeListUpdate]);
    useEffect(() => {
    }, [first.current, second.current, third.current, fourth.current]);

    useEffect(() => {
        if (displayList.length >= 1) {
            if (count === 0) {
                webSocket()
            }
            setCount(1);
        }
        let total=0 , tVolume = 0, percentT= 0, i;
        for(i = 0; i < displayListRef.current.length; i ++){
            total = total + parseFloat(displayListRef.current[i].price);
            tVolume = tVolume + parseFloat(displayListRef.current[i]['24Hr_Volume']);
            percentT = percentT + parseFloat(displayListRef.current[i].percentChange);
        }
        agency.updateExchangeAverageValue([total, tVolume, percentT/displayListRef.current.length])
    }, [displayList, first.current, second.current, third.current]);

    useEffect(() => {
        tradeWS.current = socketIOClient("https://terminal.apimachine.com");
        return () => {
            tradeWS.current.close();
            setNumbers({ Bitfinex: null, Binance: null, Kraken: null, Okex: null, ZB: null, Bitmax: null })
        }
    }, []);
    useEffect(() => {
    }, [update3])

    const webSocket = () => {
        console.log('Socket DATA Connection');
        tradeWS.current.on("data", data => {
            first.current = first.current + 1;
            let pair = `${tradePair[0].coin}${tradePair[1].coin}`;
            let a = data.mData.filter(res => {
                return res.symbol === pair;
            });
            if (numbers.Binance === null)
                return;
            setUpdate3(update3 + 1);
            displayListRef.current[numbers.Binance] = {
                ...displayListRef.current[numbers.Binance],
                price: !a[0] ? 0 : a[0].askPrice,
                ['24Hr_Volume']: !a[0] ? 0 : a[0].quoteVolume,
                percentChange: !a[0] ? 0 : parseFloat(a[0].priceChangePercent)
            };
        });
        tradeWS.current.on("dataBitfinex", data => {
            // setUpdate1(update1 + 1);
            second.current = second.current + 1;
            let pair = `t${tradePair[0].coin}${tradePair[1].coin}`;
            let a = data.mData.filter(res => {
                return res[0] === pair;
            });
            if (numbers.Bitfinex === null)
                return;
            setUpdate3(update3 + 1);
            displayListRef.current[numbers.Bitfinex] = {
                ...displayListRef.current[numbers.Bitfinex],
                price: !a[0] ? 0 : a[0][1],
                ['24Hr_Volume']: !a[0] ? 0 : a[0][2],
                percentChange: !a[0] ? 0 : 0
            };
        });
        tradeWS.current.on("dataKraken", data => {
            let pair = `${tradePair[0].coin}-${tradePair[1].coin}`;
            let a = data.mData.filter(res => {
                return res.instrument === pair;
            });
            if (numbers.Kraken === null)
                return;
            setUpdate3(update3 + 1);
            displayListRef.current[numbers.Kraken] = {
                ...displayListRef.current[numbers.Kraken],
                price: !a[0] ? 0 : a[0].ask,
                ['24Hr_Volume']: !a[0] ? 0 : a[0].baseVolume,
                percentChange: !a[0] ? 0 : parseFloat(a[0].percentChange)
            };
        });
        tradeWS.current.on("dataOkex", data => {
            fourth.current = fourth.current + 1;
            let pair = `${tradePair[0].coin}-${tradePair[1].coin}`;
            let a = data.mData.filter(res => {
                return res.product_id === pair;
            });
            if (numbers.Okex === null)
                return;
            setUpdate3(update3 + 1);
            displayListRef.current[numbers.Okex] = {
                ...displayListRef.current[numbers.Okex],
                price: !a[0] ? 0 : a[0].ask,
                ['24Hr_Volume']: !a[0] ? 0 : a[0].base_volume_24h,
                percentChange: !a[0] ? 0 : 1
            };
        });
        tradeWS.current.on("dataZB", data => {
            let pair = `${tradePair[0].coin.toLowerCase()}${tradePair[1].coin.toLowerCase()}`;
            let a = data.mData.filter(res => {
                return res[0] === pair
            });
            if (numbers.ZB === null)
                return;
            setUpdate3(update3 + 1);
            displayListRef.current[numbers.ZB] = {
                ...displayListRef.current[numbers.ZB],
                price: !a[0] ? '' : a[0][1].last,
                ['24Hr_Volume']: !a[0] ? '' : a[0][1].vol,
                percentChange: 0
            }
        })

        tradeWS.current.on("dataBitmax", data => {
            let pair = `${tradePair[0].coin}/${tradePair[1].coin}`;
            let a = data.mData.filter(res => {
                return res.symbol === pair
            });
            if (numbers.ZB === null)
                return;
            setUpdate3(update3 + 1);
            displayListRef.current[numbers.Bitmax] = {
                ...displayListRef.current[numbers.Bitmax],
                price: a[0]?.close,
                ['24Hr_Volume']: a[0]?.volume,
                percentChange: 0
            }
        }
        )
    };
    const handleSelect= (obj) =>{
        if(obj.exchangeId === selectAsset.exchangeId){
            setSelectAsset({ exchangeId: null, ['24Hr_Volume']: 0, fullImage: null, fullImageBW: null, imageCurrency: null, percentChange: 0, price: 0, image: null, buyPrice: 0, sellPrice: 0 })
        }else{
            setSelectAsset(obj)
        }
    }
    return (
        <main className="trade-exchange">
            <nav>
                {
                    headers.map(obj => <span>{obj.name}</span>)
                }
            </nav>
            <article className="trade-exchange-body">
                {
                    displayListRef.current?.map(
                        obj => <div className={`trade-exchange-row ${obj.exchangeId === selectAsset.exchangeId?"selected":""}`}>
                            <span><img src={obj.image} />{obj.exchangeId}</span>
                            <span>{obj.price}</span>
                            <span>{obj['24Hr_Volume'] === ''?'--.--':valueFormatter(parseFloat(obj['24Hr_Volume']), tradePair[1].coin)}</span>
                            <span>{obj.percentChange}</span>
                            <span><button onClick={()=>handleSelect(obj)}>Trade</button></span>
                        </div>
                    )
                }
            </article>
        </main>
    )
}
