import React from 'react'
import "./trade-loading.style.scss"
export default function TradeLoading({logo, base, length}) {
    return (
        <div className="trading-loading">
            <div>
                <img src={logo}/>
                {/* <span>67%</span> */}
            </div>
            <h6>Fetching {base} Prices From {length} Exchanges</h6>
            
        </div>
    )
}
