import React, { useContext } from 'react'
import './trade-welcome.style.scss';
import { Agency } from '../../../../../../Context/Context';
export default function TradeWelcome({logo, next}) {
    const agency = useContext(Agency);
    const {setTradeStep} = agency;
    return (
        <div className="trade-welcome">
            <img className="trade-logo" src={logo}/>
            <div onClick={()=>setTradeStep(1)}>
                <span>Welcome To Your Terminal</span>
                <img src={next}/>
            </div>
            
        </div>
    )
}
