import React, { useContext, useState } from 'react'
import './analytis.style.scss'
import { Agency } from '../../../Context/Context'
import AnalyticsHome from './analytics/AnalyticsHome';
import Constant from '../../../../json/constant'
import AnalyticsDeposit from './analytics/AnalyticsDeposit';
import AnalyticsWithdrawal from './analytics/AnalyticsWithdrawal';
export default function Analytics() {
    const agency = useContext(Agency);
    const { analyticsType } = agency;
    const [selectedHeader, setSelectedHeader] = useState(Constant.analyticsHeader[0])

    const selectComponent =() =>{
        switch (analyticsType) {
            case "": return <AnalyticsHome />;
            case "Deposit": return <AnalyticsDeposit />; 
            case "Withdraw": return <AnalyticsWithdrawal />;
            default:
                break;
        }
    }

    return (
        <div className="analytics-main">
            <div className="analytics-main-top">
                {selectComponent()}
            </div>
            <div className="analytics-main-bottom">
                <div className="analytics-header">
                    {
                        Constant.analyticsHeader.map(obj =>
                            <span
                                onClick={()=>setSelectedHeader(obj)}
                                className={obj.keyId === selectedHeader.keyId ? "selected" : ""}>
                                {obj.name}
                            </span>
                        )
                    }
                </div>
            </div>
        </div>
    )
}
