import React, { useState, useContext } from 'react'
import './controller-main.style.scss'
import { Agency } from '../../../../Context/Context';
import ControllerHome from './controller-home/ControllerHome';
import nextId from 'react-id-generator';
import HandleFunds from './handle-funds/HandleFunds';
export default function ControllerMainComponent() {
    const agency = useContext(Agency);
    const { analyticsType } = agency
    const selectMainComponent = (name) => {
        switch (name) {
            case '': return <ControllerHome />;
            case 'Friends': return <ControllerHome transfer={true} />;
            case "Add Funds": return <HandleFunds />;
            case "Send Funds": return <HandleFunds />;
        }
    }
    return (
        <React.Fragment>
            {
                <div className="display-main-content">
                    {
                        selectMainComponent(analyticsType)
                    }
                </div>
            }

        </React.Fragment>
    )
}
const headers = [
    { keyId: nextId(), name: '' },
    { keyId: nextId(), name: 'Deposit' },
    { keyId: nextId(), name: 'Withdraw' },
    { keyId: nextId(), name: 'Trade' },
    { keyId: nextId(), name: 'Transfer' },
    { keyId: nextId(), name: 'Transfer' },
]