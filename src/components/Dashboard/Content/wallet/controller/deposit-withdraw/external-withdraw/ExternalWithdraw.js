import React from 'react'
import EWStepOne from './step-one/EWStepOne';
import { Agency } from '../../../../../../Context/Context';
import EWStepTwo from './step-two/EWstepTwo';
import EWStepThree from './step-three/EWStepThree';

export default function ExternalWithdraw() {
    const agency = React.useContext(Agency);
    const {externalWithdrawStep}= agency
    const selectComponent = () =>{
        switch (externalWithdrawStep) {
            case 0: return <EWStepOne />;
            case 1: return <EWStepTwo />;
            case 2: return <EWStepThree />;
            case 3: break;
        }

    }
    return (
        <React.Fragment>
            {selectComponent()}
        </React.Fragment>
    )
}
