import React from 'react'
import LoadingAnimation from '../../../../../../../../../lotties/LoadingAnimation';
import { Agency } from '../../../../../../../../Context/Context'
import FriendAsset from '../../../../controller-friends/friends-contactbook/friend-asset/FriendAsset';
import './step-two-right.style.scss'
export default function StepTwoRight({ setEWFromToCurrency, active, displayList, setLocalStep, ewFromToCurrency }) {
    const agency = React.useContext(Agency);
    const [addAccount, setAddAccount] = React.useState(false);
    const { selectedFriendDetails,currentUserDetails, valueFormatter } = agency;

    const handleClick = (obj) => {
        active ? setEWFromToCurrency([ewFromToCurrency[0], obj]) : setEWFromToCurrency([obj, ewFromToCurrency[1]]);
        setLocalStep(1)
    }

    return (
        <div className="step-two-right">
            <div className="step-two-right-title">
                <h5>{active?selectedFriendDetails?.nickName:currentUserDetails.name}</h5>
    <span onClick={()=>setAddAccount(!addAccount)}>{addAccount?"Back To List": "Add Accounts"}</span>
            </div>
            <div className="step-two-right-body">
                {
                    addAccount?
                    <div className="add-account-wrapper">
                        {
                            selectedFriendDetails===null?'':
                            <FriendAsset setStepTwo={()=>setAddAccount(false) } ew={true} setAddFriend={()=>{}} />
                        }

                    </div>
                    :
                    !displayList.length?
                    <div className="w-100 h-100 justify-content-center align-items-center d-flex">
                        <LoadingAnimation type="empty" size={{height: 100, width: 100}} />
                    </div>
                    :
                    [...displayList].map(obj =>
                        <div onClick={() => handleClick(obj)} className="step-two-row-wrapper">
                            <span>
                                <img src={obj.coinImage} />{obj.coinSymbol}
                            </span>
                            <span>
                                {!obj.coinValueUSD ? valueFormatter(0, "USD") : valueFormatter(obj.coinValueUSD, "USD")}
                            </span>
                        </div>
                    )
                }

            </div>


        </div>
    )
}
