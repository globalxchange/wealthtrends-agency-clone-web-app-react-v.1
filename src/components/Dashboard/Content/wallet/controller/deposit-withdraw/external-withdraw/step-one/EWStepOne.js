import React from 'react'
import './ew-step-one.style.scss'
import { Agency } from '../../../../../../../Context/Context'
import FriendsContactBook from '../../../controller-friends/friends-contactbook/FriendsContactBook';
export default function EWStepOne() {
    const agency = React.useContext(Agency);
    const [addFriend, setAddFriend] = React.useState(false)
    const { currentFriendList, setExternalWithdrawStep,setSelectedFriend, setExternalWithdrawFriend, getFriendsList } = agency;

    const handleFriend = (obj) => {
        setExternalWithdrawFriend(obj);
        setExternalWithdrawStep(1);
        setSelectedFriend(obj)
        console.log("selected friend", obj)

    }
    const doneAdding = () => {
        getFriendsList()
        setAddFriend(false)
    }
    return (
        <div className="ew-step-one">
            <nav className="ew-step-one-title">
                <h5>Select The Friend You Want To Send Money To</h5>
                <p onClick={() => setAddFriend(!addFriend)}>{addFriend ? "Come Back To List" : "Add New Friend Here"}</p>
            </nav>
            <article className="ew-step-one-body">
                {
                    addFriend ?
                        <FriendsContactBook onDone={doneAdding} add={true} hide={true} />
                        :
                        currentFriendList.map((obj) =>
                            <div onClick={() => handleFriend(obj)} className="user-wrapper">
                                <span>
                                    <img src={obj.profilePic} />
                                    <p>{obj.nickName}</p>
                                </span>
                            </div>
                        )
                }
            </article>
        </div>
    )
}
