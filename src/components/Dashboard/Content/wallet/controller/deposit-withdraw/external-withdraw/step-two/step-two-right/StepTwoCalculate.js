import React from 'react'
import './step-two-calculate.style.scss'
import nextId from 'react-id-generator'
export default function StepTwoCalculate({ ewFromToCurrency }) {
    return (

        <div className="ew-calculator">
            <div className="wd-calculator-title">
                <h5>Choose Amount</h5>
                <span>Step 3</span>

            </div>
            <div className="wd-calculator-body">
                <div>{buttons.map(obj => <button >{obj.name}</button>)}</div>

                <div>
                    <h6><input placeholder="0.00" /> {ewFromToCurrency[0].coinSymbol}</h6>
                    <h6><input placeholder="0.00" /> {ewFromToCurrency[1].coinSymbol}</h6>
                </div>

            </div>
            <button className="submit-button">
                Withdraw
            </button>

        </div>
    )
}

const buttons = [
    { keyId: nextId(), name: "All", val: 1 },
    { keyId: nextId(), name: "50.0%", val: 0.5 },
    { keyId: nextId(), name: "Custom %", val: 1 },
]