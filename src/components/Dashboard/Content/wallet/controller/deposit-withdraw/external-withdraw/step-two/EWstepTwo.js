import React from 'react'
import './ew-step-two.style.scss'
import StepTwoLeft from './step-two-left/StepTwoLeft'
import StepTwoRight from './step-two-right/StepTwoRight'
import StepTwoCalculate from './step-two-right/StepTwoCalculate'
import { Agency } from '../../../../../../../Context/Context'
export default function EWStepTwo() {
    const [active, setActive] = React.useState(1);
    const [localStep, setLocalStep] = React.useState(0);
    const [ewFromToCurrency, setEWFromToCurrency] = React.useState([{}, {}]);
    const [displayList, setDisplayList] = React.useState([]);
    const agency = React.useContext(Agency);
    const { assetClassSelected, selectedFriendDetails, mainBalance, mainInterest, assetSelected } = agency;

    const selectWhich = () => {
        switch (localStep) {
            case 0: return <StepTwoRight active={active} ewFromToCurrency={ewFromToCurrency} displayList={displayList} setLocalStep={setLocalStep} setEWFromToCurrency={setEWFromToCurrency} />;
            case 1: return <StepTwoCalculate ewFromToCurrency={ewFromToCurrency} />;
            default: break;
        }
    }
    React.useEffect(() => {
        if (active) {
            if (selectedFriendDetails !== null) setDisplayList([...selectedFriendDetails.accountList.crypto, ...selectedFriendDetails.accountList.fiat]);
            else setDisplayList([])
        } else {
            switch (assetSelected?.name) {
                case "Forex Currencies":
                    setDisplayList([...mainBalance.fiat]); break;
                case "Cryptocurrency":
                    setDisplayList([...mainBalance.crypto]); break;
                case "Earnings":
                    setDisplayList([...mainInterest]); break;
                default:
                    setDisplayList([]); break;
            }
        }

    }, [active, selectedFriendDetails])
    React.useEffect(() => {
        console.log("setEWFromToCurrency", assetClassSelected, ewFromToCurrency[1])
        setEWFromToCurrency([assetClassSelected, ewFromToCurrency[1]])
    }, [assetClassSelected])
    return (
        <div className="ew-step-two">
            <div className="ew-step-two-wrapper">
                <StepTwoLeft setLocalStep={setLocalStep} ewFromToCurrency={ewFromToCurrency} setActive={setActive} active={active} />

            </div>
            <div className="ew-step-two-wrapper">
                {selectWhich()}

            </div>


        </div>
    )
}
