import React from 'react'
import './step-three.style.scss'
export default function EWStepThree() {
    return (
        <div className="ew-step-three">
        <input autoFocus={true} placeholder="Write A Note About This Transaction .." />
        <div>
            <button >No, Thank You</button>
            <button>I'm Done</button>
        </div>
    </div>
    )
}
