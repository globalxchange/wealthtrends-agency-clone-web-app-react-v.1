import React, { useContext } from 'react'
import { Agency } from '../../../../../../../../Context/Context'
import './step-two-left.style.scss'
export default function StepTwoLeft({ setActive,setLocalStep, ewFromToCurrency, active }) {
    const agency = useContext(Agency);
    const { assetClassSelected } = agency;
    return (
        <div className="step-two-left">
            <div className="step-two-left-title">
                <h5>Select The Currencies</h5>
                <span>Step 2</span>

            </div>
            <div className="step-two-left-body">
                <div onClick={() => {setActive(0);setLocalStep(0)}} className={`accounts-wrapper ${!active ? "active" : ""}`}>
                    <span>From</span>
                    <div>
                        <img height="40" src={ewFromToCurrency[0]?.coinImage} />
                    </div>

                </div>
                <div onClick={() => {setLocalStep(0); setActive(1)}} className={`accounts-wrapper ${active ? "active" : ""}`}>
                    <span>To Account</span>
                    <div>
                        <img height="40" src={ewFromToCurrency[1]?.coinImage} />
                    </div>

                </div>

            </div>

        </div>
    )
}
