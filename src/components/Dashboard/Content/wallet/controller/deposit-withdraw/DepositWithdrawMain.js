import React, { useEffect, useContext } from 'react'
import './deposit-main.style.scss'
import Constant from '../../../../../../json/constant'
import DepositStepTwo from '../../analytics/deposit/deposit-step-two';
import WDInternalComponent from './internal/WDInternalComponent';
import { Agency } from '../../../../../Context/Context';
import DownloadInstaCrypto from '../../analytics/download-insta-crypto/DownloadInstaCrypto';
import ExternalWithdraw from './external-withdraw/ExternalWithdraw';
export default function DepositWithdrawMain({ deposit }) {
    const [currentList, setCurrentList] = React.useState([]);
    // const [selected, setSelected] = React.useState(deposit ? Constant.depositSection[1] : Constant.withdrawSection[1]);
    // const [toTransfer, setToTransfer] = React.useState({})
    const agency = useContext(Agency);
    const { setTransfer, transferState, setExternalWithdrawStep, analyticsType, setSelectedTransfer,
        setStepMain, selectedTransfer, setTransferValue, currentApp, assetSelected, assetClassSelected } = agency;
    // useEffect(()=>{
    //     // deposit?setSelectedTransfer(Constant.depositSection[0]):setSelectedTransfer(Constant.withdrawSection[0])
    // },[])
    // useEffect(()=>[

    // ],[])

    useEffect(() => {
        if (analyticsType === "Add Funds") {
            if (!deposit) {
                return
            }
            let obj = { app_code: currentApp.app_code, profile_id: currentApp.profile_id, coin: assetClassSelected.coinSymbol }
            setTransfer({ ...transferState, to: { ...obj }, deposit: true })
            setCurrentList(Constant.depositSection);
            setSelectedTransfer(Constant.depositSection[1])
        }
        else if (analyticsType === "Send Funds") {
            if (deposit) {
                return
            }
            console.log("transfer valuesss", assetClassSelected)
            setTransferValue(assetClassSelected.coinValue);
            let obj = { app_code: currentApp.app_code, profile_id: currentApp.profile_id, coin: assetClassSelected.coinSymbol }
            setTransfer({ ...transferState, from: { ...obj }, deposit: false });
            setCurrentList(Constant.withdrawSection)
            setSelectedTransfer(Constant.withdrawSection[1])
        }
        else {
            setStepMain(0)
        }

        let obj = { app_code: currentApp.app_code, profile_id: currentApp.profile_id, coin: assetClassSelected.coinSymbol }

        return () => {

        }
    }, [analyticsType, assetClassSelected])

    const selectComponent = () => {
        if (deposit) {

            switch (selectedTransfer?.text) {
                case "External Deposit": return <DepositStepTwo />;
                case "Internal Deposit": return <WDInternalComponent />;
                case "Request Money": break;
            }
        } else {

            // setStepMain(0)
            switch (selectedTransfer?.text) {
                case "External Withdraw": return <ExternalWithdraw />;
                case "Internal Withdraw": return <WDInternalComponent />;
                case "Request Money": break;
            }

        }
    }
    const checkToDisable = (i) => {
        if (i === 2) {
            return false;
        } else if (i === 0) {
            return assetClassSelected.native_deposit
        } else {
            return true
        }
    }

    return (
        <div className="deposit-withdraw-main">
            {
                assetSelected.name === "Forex Currencies" ?
                    <DownloadInstaCrypto /> :
                    <>
                        <div className="main-left">
                            {
                                currentList.map((obj, i) =>
                                    <div
                                        onClick={() => { setExternalWithdrawStep(0); setStepMain(0); checkToDisable(i) ? setSelectedTransfer(obj) : console.log() }}
                                        style={i === 0 && !checkToDisable(i) ? { opacity: 0.35 } : {}}
                                        className={obj.text === selectedTransfer?.text ? "selected-type" : ""}
                                        key={obj.keyId}
                                    >
                                        <h6><img src={obj.image} />{obj.text}</h6>
                                    </div>)
                            }

                        </div>
                        <div className="main-right">
                            {
                                selectComponent()
                            }

                        </div>
                    </>
            }

        </div>
    )
}
