import React from 'react'
import { Agency } from '../../../../../../../Context/Context'
import LoadingAnimation from '../../../../../../../../lotties/LoadingAnimation'

export default function SupportedAsset({ toList, transfer, handleClick, coinsList }) {
    const agency = React.useContext(Agency)
    return (
        <div className="supported-assets">
            <div className="supported-assets-title">
                <h5>{toList ? transfer.app_name : agency.currentApp.app_name}</h5>
                <span>Supported Assets</span>
            </div>
            <div className="supported-assets-body">
                {
                    !coinsList.length ?
                        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                            <LoadingAnimation type="no-data" size={{ height: 100, width: 200 }} />
                        </div>
                        :
                        coinsList.map(
                            obj =>
                            <div onClick={() => handleClick(obj)}>
                                <span>
                                    <img src={obj.coinImage} />{obj.coinName}
                                </span>
                                <span>
                                    {agency.valueFormatter(obj.coinValue, obj.coinSymbol)}
                                </span>
                            </div>
                        )
                }
            </div>

        </div>
    )
}
