import React from 'react'
import './step-three.style.scss'
import { Agency } from '../../../../../../../Context/Context';
import LoadingAnimation from '../../../../../../../../lotties/LoadingAnimation';
export default function StepThree() {
    const agency = React.useContext(Agency);
    const [message, setMessage] = React.useState({transfer_for:''})
    const { transferState, setTransfer, transferLoading, finalTransferSubVault } = agency;

    const handleMessage = (e) => {
        setMessage({transfer_for: e.target.value})
    }
    const handleSubmit = () => {
        finalTransferSubVault(message)
    }
    return (
        <React.Fragment>
            {
                transferLoading?
                <div className="h-100 w-100 d-flex justify-content-center align-items-center">
                    <LoadingAnimation type="login" size={{height: 200, width: 200}}/>
                </div>
                :
                <div className="step-three">
                    <input autoFocus={true} onChange={e => handleMessage(e)} placeholder="Write A Note About This Transaction .." />
                    <div>
                        <button onClick={() => handleSubmit()}>No, Thank You</button>
                        <button onClick={() => handleSubmit()}>I'm Done</button>
                    </div>
                </div>
            }
        </React.Fragment>
    )
}
