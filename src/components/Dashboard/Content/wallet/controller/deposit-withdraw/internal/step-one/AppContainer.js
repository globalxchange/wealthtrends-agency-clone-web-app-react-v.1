import React from 'react'
import './wd-step-one.style.scss'
import Constant from '../../../../../../../../json/constant'
import { Agency } from '../../../../../../../Context/Context';
export default function AppContainer({  }) {
    const [appList, setAppList] = React.useState([]);
    const agency = React.useContext(Agency);
    const { allApps, analyticsType, setToTransfer, setStepMain, setInstaCryptoSupportedAssets } = agency;


    React.useEffect(() => {
        let count = (allApps?.length / 5) + 1;
        let temp = [];
        for (let i = 0; i < count - 1; i++) {
            temp = [...temp, { list: allApps.slice(5 * i, 5 * (i + 1)) }]
        }
        setAppList(temp)
    }, [allApps])

    const handleClick = (obj) => {
        setInstaCryptoSupportedAssets(obj.app_code, obj.profile_id)
        setToTransfer(obj);
        setStepMain(1)
    }
    return (

        <div className="wd-step-one">
            <div className="wd-step-one-title">
                <h5>{analyticsType === "Add Funds" ? "Select The App You Want To Send Money To" : "Select The App You Want To Send Money To"}</h5>
                <span className="subtitle">Step 1</span>
            </div>
            <div className="wd-step-one-body">
                {
                    appList.map(obj =>
                        <div className="row-wrapper">{
                            obj.list.map(objTwo =>
                                <div>
                                    <span onClick={() => handleClick(objTwo)} className="image-wrapper">
                                        <img src={objTwo.app_icon} />
                                        <p>{objTwo.app_name}</p>
                                    </span>
                                </div>
                            )
                        }</div>
                    )
                }
            </div>

        </div>
    )
}
