import React, { useContext, useEffect } from 'react'
import './wd-step-two.style.scss'
import { Agency } from '../../../../../../../Context/Context'
import WDStepOne from '../step-one/AppContainer';
import SupportedAsset from './SupportedAsset';
import WDCalculator from './WDCalculator';
export default function WDStepTwo({  }) {
    const [step, setStep] = React.useState(0);
    const [toList, setToList] = React.useState(true);
    const [fromToCoins, setFromToCoins] = React.useState([]);
    const [coinsList, setCoinsList] = React.useState([]);
    // const []
    const [transferCoin, setTransferCoin] = React.useState(null);
    const agency = useContext(Agency);
    const { assetClassSelected,transfer, setAssetClassSelected, analyticsType,setTransferValue, currentApp, transferState, setTransfer, mainBalance, instaCryptoSupportedAssets } = agency;
    const selectComponent = () => {
        switch (step) {
            case 0: return <SupportedAsset transfer={transfer} toList={toList} coinsList={coinsList} handleClick={handleClick} coin={assetClassSelected.coinImage} />;
            case 1: return <WDCalculator value ={analyticsType==="Add Funds"?setFromToCoins[1]?.coinValue: setFromToCoins[0]?.coinValue}  />;
            default: return;
        }
    }
    const handleClick = (obj) => {
        if (analyticsType === "Add Funds") {
            let temp = { app_code: transfer.app_code, profile_id: transfer.profile_id, coin: obj.coinSymbol }
            if (toList) {
                console.log("temp obj", obj)
                let tempO = { name: `From ${transfer.app_name}`, coin: obj.coinSymbol, coinImage: obj.coinImage, coinValue: obj.coinValue }
                setTransferValue(obj.coinValue)
                setFromToCoins([fromToCoins[0], { ...tempO }])
            } else {

                let tempO = { name: `To ${currentApp.app_name}`, coinValue: obj.coiValue, coin: obj.coinSymbol, coinImage: obj.coinImage }
                setFromToCoins([{ ...tempO }, fromToCoins[1]])
                setAssetClassSelected(obj)
            }
            toList ? setTransfer({ ...transferState, from: { ...temp } }) : setTransfer({ ...transferState, to: { ...temp } })

        } else {
            let temp = { app_code: transfer.app_code, profile_id: transfer.profile_id, coin: obj.coinSymbol }
            if (toList) {
                let tempO = { name: `To ${transfer.app_name}`, coin: obj.coinSymbol, coinImage: obj.coinImage, coinValue: obj.coinValueUSD }
                setFromToCoins([fromToCoins[0], { ...tempO }])
            } else {
                let tempO = { name: `From ${currentApp.app_name}`, coinValue: obj.coiValueUSD, coin: obj.coinSymbol, coinImage: obj.coinImage }
                setFromToCoins([{ ...tempO }, fromToCoins[1]])
                setTransferValue(obj.coinValue)
                setAssetClassSelected(obj)
                
            }
            !toList ? setTransfer({ ...transferState, from: { ...temp } }) : setTransfer({ ...transferState, to: { ...temp } })
        }

        setStep(1)
    }

    useEffect(() => {
        setStep(0)
        if (analyticsType === "Add Funds") {
            let array = [
                { name: `To ${currentApp.app_name}`, coinValue: assetClassSelected.coiValueUSD, coin: assetClassSelected.coinSymbol, coinImage: assetClassSelected.coinImage },
                { name: `From ${transfer.app_name}`, coin: null, coinImage: null }
            ]
            setFromToCoins([...array])
        } else {
            let array = [
                { name: `From ${currentApp.app_name}`, coin: assetClassSelected.coinSymbol, coinValue: assetClassSelected.coiValueUSD, coinImage: assetClassSelected.coinImage },
                { name: `To ${transfer.app_name}`, coin: null, coinImage: null }
            ]
            setFromToCoins([...array])

        }

    }, [analyticsType, assetClassSelected])

    React.useEffect(() => {
        if (toList) {
            setCoinsList(instaCryptoSupportedAssets)
        } else {
            setCoinsList([...mainBalance.crypto])
        }
    }, [toList, instaCryptoSupportedAssets])

    return (
        <div className="wd-step-two">
            <div className="step-two-left">
                <div className="step-two-left-title">
                    <h5>Select The Currencies</h5>
                    <span>Step 2</span>
                </div>
                <div className="step-two-left-body">
                    {
                        fromToCoins.map((obj, i) =>
                            <div style={analyticsType==="Add Funds"?{order: i?1:2}:{}} className={`${!i && !toList ? "completed" : i && toList ? "completed" : ""} insta-crypto-agency`}>
                                <span>{obj.name}</span>
                                <div onClick={() => { i ? setToList(true) : setToList(false); setStep(0) }}>
                                    <img src={obj.coinImage} />
                                </div>
                            </div>
                        )
                    }

                </div>

            </div>
            <div className="step-two-right">
                {selectComponent()}

            </div>

        </div>
    )
}
