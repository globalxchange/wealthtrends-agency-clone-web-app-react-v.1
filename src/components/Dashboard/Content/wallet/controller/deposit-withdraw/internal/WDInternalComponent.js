import React from 'react'
import './wd-internal.style.scss'
import AppContainer from './step-one/AppContainer'
import WDStepTwo from './step-two/WDStepTwo'
import StepThree from './step-three/StepThree'
import { Agency } from '../../../../../../Context/Context'
export default function WDInternalComponent() {
    const agency = React.useContext(Agency);
    
    const selectComponent= ()=>{
        switch(agency.step){
            case 0: return <AppContainer  />;
            case 1: return <WDStepTwo />;
            case 2: return <StepThree />
        }
    }
    return (
    <React.Fragment>
        {selectComponent()}
    </React.Fragment>
    )
}
