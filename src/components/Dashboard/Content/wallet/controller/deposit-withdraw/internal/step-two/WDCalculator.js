import React, { useEffect, useState } from 'react'
import nextId from 'react-id-generator'
import { Agency } from '../../../../../../../Context/Context'

export default function WDCalculator() {
    const agency = React.useContext(Agency);
    const [rate, setRate] = React.useState(1)
    const { transferState, setTransfer,valueFormatterInput,valueFormatter,transferValue,setStepMain, conversionRates } = agency;
    const [fromValue, setFromValue] = useState('');
    const [toValue, setToValue] = useState('');
    const [error, setError] = useState(false);
    const [selected, setSelected] = useState()

    useEffect(() => {
        setRate(conversionRates[transferState.to?.coin])

    }, [transferState, conversionRates]);

    const handleChange = (e, from) => {
        setError(false)
        if (from) {
            setFromValue(e.target.value)
            let val = valueFormatterInput(e.target.value * (1 / rate), transferState.to?.coin)
            setToValue(val)
        } else {
            setToValue(e.target.value)
            let val = valueFormatterInput(e.target.value * ( rate), transferState.from?.coin)
            setFromValue(val)
        }
    }

    const handleSubmit=()=>{
        if(fromValue > transferValue){
            setError(true);
            return
        }else{
            setError(false)
        }
        let temp = {};
        temp = {
            from: transferState.from,
            to: transferState.to,
            to_amount: toValue
        }

        setTransfer({...transferState, ...temp});
        setStepMain(2);
    }

    const handlePercentage = (obj) =>{
            setError(false)
            setSelected(obj)
            let mul = obj.val * transferValue
            setFromValue(valueFormatterInput(mul, transferState.from?.coin));
            let ans = valueFormatterInput(mul * (1 / rate), transferState.to?.coin)
            setToValue(ans)
    }

    return (
        <div className="wd-calculator">
            <div className="wd-calculator-title">
                <h5>Choose Amount</h5>
                <span>Step 3</span>

            </div>
            <div className="wd-calculator-body">
                <div>{buttons.map(obj => <button className={obj.name === selected?.name?"selected":''} onClick = {()=>handlePercentage(obj)} key={obj.keyId}>{obj.name}</button>)}</div>

                <div>
                    <h6><input placeholder={valueFormatter(0.0, transferState.from?.coin)[1]} value={fromValue} onChange={(e) => handleChange(e, true)} /> {transferState.from?.coin}</h6>
                    <h6><input placeholder={valueFormatter(0.0, transferState.to?.coin)[1]} value={toValue} onChange={(e) => handleChange(e, false)} /> {transferState.to?.coin}</h6>
                </div>

            </div>
            <button disabled={!toValue} style={error?{backgroundColor: '#de4b25'}:{}} onClick = {()=>handleSubmit()} className="submit-button">
                {error?"Exceeded Amount":"Withdraw"}
            </button>

        </div>
    )
}
const buttons = [
    { keyId: nextId(), name: "All" , val: 1},
    { keyId: nextId(), name: "50.0%" , val: 0.5},
    { keyId: nextId(), name: "Custom %", val: 1 },
]