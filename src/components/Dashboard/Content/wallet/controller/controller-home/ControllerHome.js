import React,{useState, useEffect} from 'react'
import Constant from '../../../../../../json/constant'
import ControllerAnalytics from '../controller-analytics/ControllerAnalytics';
import ControllerFriends from '../controller-friends/ControllerFriends';
import ControllerBanker from '../controller-bankers/ControllerBanker';
import ControllerInterest from '../controller-interest/ControllerInterest';
import ControllerMarkets from '../controller-markets/ControllerMarkets';
import ControllerTaxes from '../controller-taxes/ControllerTaxes';
import './controller-home.style.scss'
export default function ControllerHome({transfer}) {
    const [selected, setSelected] = useState(Constant.controllerHeader[0]);
    
    const selectController = (name) => {
        switch (name) {
            case "Analytics": return <ControllerAnalytics />;
            case "Friends": return <ControllerFriends />
            case "Bankers": return <ControllerBanker />;
            case "Interest": return <ControllerInterest />;
            case "Markets": return <ControllerMarkets />;
            case "Taxes": return <ControllerTaxes />;
            default: return;
        }
    }
    useEffect(()=>{
        transfer?setSelected(Constant.controllerHeader[1]):setSelected(Constant.controllerHeader[0])
    },[transfer])
    return (
        <div className="controller-main">
                {/* <div className="controller-header">
                    {
                        Constant.controllerHeader.map(obj =>
                            <span
                                onClick={() => setSelected(obj)}
                                className={obj.keyId === selected.keyId ? "selected" : ""}
                                key={obj.keyId}>{obj.name}</span>
                        )
                    }
                </div> */}
                <div className="controller-body">
                    {/* {selectController()} */}
                    {/* {
                        Constant.controllerHeader.map(obj =><div key={obj.keyId} aria-hidden={!obj.name === selected.name} className= {obj.name === selected.name?"display-content":"hide-content"}>{selectController(obj.name)}</div>)
                    } */}
                    <div className="display-content">{selectController(selected.name)}</div>
                </div>
            </div>
    )
}
