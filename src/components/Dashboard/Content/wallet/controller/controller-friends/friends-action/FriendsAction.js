import React, { useContext, useRef, useEffect } from 'react'
import './friends-action.style.scss'
import Images from '../../../../../../../assets/a-exporter';
import Constant from '../../../../../../../json/constant';
import { Agency } from '../../../../../../Context/Context'
export default function FriendsAction() {
    const chatBody = useRef()

    const agency = useContext(Agency)
    const { currencyImageList, selectedFriendDetails } = agency;
    useEffect(() => {
        chatBody.current.scrollTop = chatBody.current.scrollHeight;
    }, [chatBody.current?.offsetWidth]);


    return (

        <div className="friends-action">
            <div className="contact-action-header">
                <img className="first-image" src={Images.threeBets} />
                <div>
                    <h6>{selectedFriendDetails?.nickName}</h6>
                    <span>Powered by Chats.io </span>
                </div>
                <button>TXN Log <img src={Images.triangle} /></button>
            </div>
            <div ref={chatBody} className="contact-action-body">
                {
                    Constant.friendsChat.map(obj =>
                        <div style={obj.self ? { flexDirection: "row-reverse" } : {}} className="chat-row">
                            <div className="image-wrapper">
                                <img src={Images.threeBets} />
                                <span>{obj.date}</span>
                            </div>
                            <div className="details-wrapper">
                                <span><img src={currencyImageList[obj.coin]} />{obj.coinName}</span>
                                <span>{obj.amount}</span>

                            </div>


                        </div>
                    )
                }

            </div>

        </div>
    )
}
