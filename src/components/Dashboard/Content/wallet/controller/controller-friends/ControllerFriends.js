import React from 'react'
import './controller.friends.scss'
import FriendsContactBook from './friends-contactbook/FriendsContactBook'
import FriendsProfile from './friends-profile/FriendsProfile'
import FriendsAction from './friends-action/FriendsAction'
export default function ControllerFriends() {
    return (
        <div className="controller-friends">
            <div className="friends-left">
                <FriendsContactBook />
            </div>
            <div className="friends-middle">
                <FriendsProfile />
            </div>
            <div className="friends-right">
                <FriendsAction />
            </div>
            
        </div>
    )
}
