import React, { useState, useContext, useEffect } from 'react'
import './friends-profile.style.scss'
import nextId from 'react-id-generator'
import Images from '../../../../../../../assets/a-exporter'
import { Agency } from '../../../../../../Context/Context'
export default function FriendsProfile() {
    const agency = useContext(Agency)
    const [selected, setSelected] = useState(options[1])
    const [selectedAsset, setSelectedAsset] = useState(assetOptions[1]);
    const [coinList, setCoinList] = useState([]);
    const [copied, setCopied] = useState(false);
    const [details, setDetails] = useState({});
    const [selectedCoin, setSelectedCoin] = useState(null);
    const { allCoins, currentUserDetails, editUserAccount, userAccountDetails, selectedFriendDetails, setContactStep, theme } = agency;

    useEffect(() => {
        // alert(selected.name)
        if (selected.name === "Your Profile") {
            if (selectedAsset.name === "Crypto")
                setCoinList(userAccountDetails.accountList.crypto)
            else
                setCoinList(userAccountDetails.accountList.fiat)
        } else {
            {
                if (selectedAsset.name === "Fiat") {
                    setCoinList(selectedFriendDetails.accountList.fiat)

                } else {
                    setCoinList(selectedFriendDetails.accountList.crypto)

                }

            }
        }
    }, [selectedAsset, selected])

    const handleRotate = (n) => {
        if (n < 0) {
            let rItems = [];
            let temp_array = coinList
            for (let i = 0; i < Math.abs(n); i++) {
                rItems = [...rItems, temp_array.shift()]
            }
            setCoinList([...temp_array, ...rItems])
        } else {
            let rItems = [];
            let temp_array = coinList
            for (let i = 0; i < n; i++) {
                rItems = [...rItems, temp_array.pop()]
            }
            setCoinList([...rItems, ...temp_array,])

        }

    }
    useEffect(() => {
        if (selectedFriendDetails?.email === undefined) {
            setSelected(options[1])
        }
        if (selected.name === "Your Profile")
            setDetails({ email: currentUserDetails.email, name: currentUserDetails.name, image: currentUserDetails.profile_img })
        else {
            setDetails({ email: selectedFriendDetails?.friendEmail, name: selectedFriendDetails?.nickName, image: selectedFriendDetails?.profilePic });
            if (!selectedFriendDetails?.accounts) {
                setCoinList([])
            } else {

            }
        }


    }, [selected, selectedFriendDetails])
    const handleCopied = () => {
        setCopied(true)
        let a = setTimeout(() => {
            setCopied(false); clearTimeout(a)
        }, 2000);
    }
    const selectThisAsset = (coin) => {
        setSelectedCoin(coin)

    }
    return (

        <div className="friends-profile">
            <div className="contact-profile-header">
                {
                    options.map((obj, i) =>
                        <button
                            onClick={() => { selectedFriendDetails?.friendEmail && setSelected(obj); setSelectedCoin(null) }}
                            // className={!i?selectedFriendDetails ===null?"hide-header":""  : obj.keyId === selected.keyId ? "selected" : ""}
                            className={selectedFriendDetails === null ? !i ? "hide-header" : "selected" : obj.keyId === selected.keyId ? "selected" : ""}
                            key={obj.keyId}>{obj.name}</button>
                    )
                }

            </div>
            <div className="contact-profile-body">
                <div className="contact-profile">
                    <img onError={(e) => e.target.src = theme === 'light' ? Images.agencyDark : Images.agencyWhite} src={details.image === '' ? theme === 'light' ? Images.agencyDark : Images.agencyWhite : details.image} />
                    <div>
                        <h5>{details.name}</h5>
                        <span onClick={() => selected.name === "Your Profile" && !editUserAccount ? console.log() : setContactStep(1)}>{selected.name === "Your Profile" && !editUserAccount ? details.email : "Edit Accounts"}</span>
                    </div>

                </div>
                <div className="contact-asset">
                    <div className="contact-asset-header">
                        {
                            assetOptions.map(obj =>
                                <button
                                    onClick={() => { setSelectedCoin(null); setSelectedAsset(obj) }}
                                    className={obj.keyId === selectedAsset.keyId ? "selected" : ""}
                                    key={obj.keyId}>{obj.name}</button>
                            )
                        }
                    </div>
                    <div className="contact-asset-wrapper">

                        <div className="contact-asset-coins">
                            <button onClick={() => handleRotate(2)} className={coinList.length < 6 ? "d-none" : "move-left"}><img src="https://img.icons8.com/small/16/000000/expand-arrow.png" /></button>
                            <button onClick={() => handleRotate(-2)} className={coinList.length < 6 ? "d-none" : "move-right"}><img src="https://img.icons8.com/small/16/000000/expand-arrow.png" /></button>
                            {
                                !coinList.length ?
                                    <h6>No Accounts Added Yet</h6>
                                    :
                                    coinList.slice(0, 5).map((obj, i) =>
                                        <img
                                            onClick={() => { selectThisAsset(obj); coinList.length < 6 ? console.log() : handleRotate(2 - i) }}
                                            style={{ order: i + 1 }}
                                            src={obj.coinImage}
                                            className={selectedCoin?.accountID === obj.accountID ? "selected-coin" : ""}
                                        />
                                    )
                            }

                        </div>
                        <div className={selectedAsset.name === "Fiat" || !coinList.length ? "d-none" : "contact-asset-action"}>
                            <div className="copy-clipboard">
                                <h6>{selectedCoin === null ? "Select A Coin" : selectedCoin.address}</h6>
                                <span className={copied ? "" : "d-none"}>copied</span>
                                <img
                                    className={selectedCoin === null ? "d-none" : ""}
                                    onClick={() => { navigator.clipboard.writeText("dgdkGnj54hgfvHGVHGfvhgkjhnkjnkjNKJB567"); handleCopied() }} src={agency.theme === "light" ? Images.copy : Images.copyInvert} />
                            </div>
                            <div className="contact-action">
                                {
                                    actions.map(obj => <button key={obj.keyId}>{obj.name}</button>)
                                }
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </div>
    )
}

const options = [
    { keyId: nextId(), name: "Selected" },
    { keyId: nextId(), name: "Your Profile" }
]
const assetOptions = [
    { keyId: nextId(), name: "Crypto" },
    { keyId: nextId(), name: "Fiat" }
]
const actions = [
    { keyId: nextId(), name: "Edit" },
    { keyId: nextId(), name: "Send" },
    { keyId: nextId(), name: "Request" }
]