import React, { useState, useContext } from 'react'
import './friend-email.style.scss'
import { IsGXUser } from '../../../../../../../../services/getAPIs'
import { Agency } from '../../../../../../../Context/Context';
import LoadingAnimation from '../../../../../../../../lotties/LoadingAnimation';
import Images from '../../../../.././../../../assets/a-exporter'
export default function FriendEmail({ step, setStep }) {
    const [value, setValue] = useState('');
    const [valid, setValid] = useState({ onGoing: false, end: true });
    const [load, setLoad] = useState(false);
    const [done, setDone] = useState(false);
    const [message, setMessage] = useState("")
    const agency = useContext(Agency);
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    const { checkIfGxUser, addFriendDetails, updateAddFriendDetails, theme } = agency;

    const handleChange = (e) => {
        let check =  emailRegex.test(String(e.target.value).toLowerCase())
        setValid({ ...valid, onGoing: check });
        setValue(e.target.value);
    }

    const handleSubmit = () => {
        // setStep(1);

        if (value.includes('@') && value.includes('.')) {

            setLoad(true)
            setValid({ onGoing: false, end: true });
            IsGXUser(value)
                .then(res => {
                    setDone(true)
                    checkIfGxUser(res.data);
                    updateAddFriendDetails({ ...addFriendDetails, friendEmail: value })
                    setMessage(res.data?"GX User": "Not A GX User");
                    let a = setTimeout(() => { setStep(1); clearTimeout(a) }, 1000)
                })
        } else {
            setValid({ ...valid, end: false })
        }
    }
    return (
        <form onSubmit={(e) => { e.preventDefault(); }} className="friend-email">
            <h6>What Is Your Friend's Email</h6>
            <input type="email" className={valid.end ? "" : "error"} onChange={e => handleChange(e)} autoFocus={true} placeholder="Ex.shorupan@gmail.com" />
            <div className={load ? "verifying-animation" : "d-none"}>
                <LoadingAnimation size={{ height: 50, width: 75 }} type={done ? "done" : "verify"} loop={done ? false : true} />
                <span>{message === "" ? "Verifying Your Friend...." : message}</span>
            </div>
            <button onClick={() => handleSubmit()} className={valid.onGoing ? "next-button" : "d-none"}><img src={theme === "light" ? Images.nextButton : Images.nextButtonInvert} /></button>
        </form>
    )
}
