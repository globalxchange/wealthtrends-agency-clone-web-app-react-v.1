import React from 'react'

export default function AccountHandleStepOne({setDeleteState, buttonList, setAccountStep }) {
    const handleClick = (name) => {
        switch (name) {
            case "add": setAccountStep(3); break;
            case "update": setAccountStep(1);setDeleteState(false); break;
            case "remove":setDeleteState(true); break;
        }
    }
    return (
        <div className="account-handle-step-one">
            <h1>You Want To?</h1>
            {
                buttonList.map(obj =>
                    <button onClick={() => handleClick(obj.id)}>{obj.name}</button>
                )
            }
        </div>
    )
}
