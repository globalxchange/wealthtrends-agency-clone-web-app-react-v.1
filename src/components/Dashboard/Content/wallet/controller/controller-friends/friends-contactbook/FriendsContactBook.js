import React, { useContext, useState } from 'react'
import './friends-contactbook.style.scss'
import { Agency } from '../../../../../../Context/Context'
import Images from '../../../../../../../assets/a-exporter'
import ContactList from './contact-list/ContactList'
import FriendEmail from './friend-email/FriendEmail'
import FriendUpload from './friend-upload/FriendUpload'
import FriendAsset from './friend-asset/FriendAsset'
export default function FriendsContactBook({add= null ,onDone=()=>console.log(),  hide = false}) {
    const agency = useContext(Agency)
    const [addFriend, setAddFriend] = useState(add);
    const [state, setState] = useState({ friendName: '' });
    const [step, setStep] = useState(0);
    const [stepTwo,setStepTwo] = useState(0)
    const { theme, contactStep, setEditUserAccount } = agency;

    const selectComponent = () => {
        if (!addFriend || addFriend === null) {
            switch(contactStep){
                case 0: return <ContactList setStepTwo={setStepTwo} setAddFriend={setAddFriend} searchTerm={state.friendName} />;
                case 1: return <FriendAsset setStepTwo={setStepTwo} />;


            }

        } else {
            switch (step) {
                case 0: return <FriendEmail step={step} setStep={setStep} />;
                case 1: return <FriendUpload onDone={onDone} hide={hide} step={step} setAddFriend={setAddFriend} setStep={setStep} />;
                case 2: return <FriendAsset setAddFriend={setAddFriend} step={step} setStep={setStep} />;
                default: return <FriendEmail step={step} setStep={setStep} />
            }
        }
    }
    const handleChange = (e) => {
        setState({ [e.target.name]: e.target.value })
    }
    return (
        <div className="friends-contact-book">
            <div className={`contact-book-header ${hide?"d-none":""}`}>
                <div className="contact-book-header-search">

                    {
                        addFriend === null ?
                            <h6 className="mb-0 font-weight-bold">{contactStep ?"Editing Account For":"Your Contact Book"}</h6>
                            :
                            addFriend ?
                                <h6 className="mb-0 font-weight-bold">Add New Friend</h6>
                                :
                                <input autoFocus name="friendName" onChange={handleChange} placeholder="Type Your Friends Name" />}
                </div>
                <div onClick={() => !addFriend && addFriend !== null ? setAddFriend(null) : setAddFriend(false)} className="contact-book-header-search-button">
                    <img src={theme === "dark" ? Images.searchWhite : Images.searchDark} />
                </div>
                <div onClick={() => { setAddFriend(true); setStep(0) }} className="contact-book-header-add">
                    <img src={theme === "dark" ? Images.add : Images.addDark} />
                </div>
            </div>
            <div className={`contact-book-body ${hide?"h-100":""}`}>
                {selectComponent()}
            </div>
        </div>
    )
}
