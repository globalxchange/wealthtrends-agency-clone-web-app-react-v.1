import React from 'react'
import { Agency } from '../../../../../../../../Context/Context';

export default function AccountAddCrypto({coin,update,selectedAccount,handleUpdateAccountCrypto, handleAddAccountCrypto}) {
    const agency = React.useContext(Agency);
    const [state, setState] = React.useState({address:'',addressNickName: ''})
    const [load, setLoad] = React.useState(false)
    React.useEffect(()=>{
        if(update){
            setState({address: selectedAccount.address, addressNickName: selectedAccount.addressNickName})
        }
    },[])


    const handleChange= (e) =>{
        setState({...state, [e.target.name]: e.target.value})
    }
    return (
        <div className="account-add-crypto">
            <div className="account-add-crypto-header">
                <h2>{update?"Update":"Add"} {coin} Account</h2>
            </div>
            <div className="account-add-crypto-body">
                <h6><span>{coin} Address:</span> <input value={state.address} name="address" onChange={handleChange} placeholder={`${coin} Address`} /> </h6>
                <h6><span>Address Nickname:</span> <input value={state.addressNickName} name="addressNickName" onChange={handleChange}  placeholder={`${coin} Address Nickname`} /> </h6>
                <button 
                onClick = {()=>{update?handleUpdateAccountCrypto(state): handleAddAccountCrypto(state);setLoad(true)}}>{load?`${update?"Updating":"Adding"} ${coin} Account....`:`${update?"Update":"Add"} Account`}</button>
            </div>

        </div>
    )
}
