import React, { useContext, useEffect, useState } from 'react'
import nextId from 'react-id-generator';
import { AddFiatAccount, addCryptoAccount, AddUserFiatAccount, updateCryptoAccount, addUserCryptoAccount, updateFiatAccount, deleteAccount } from '../../../../../../../../../services/postAPIs';
import { Agency } from '../../../../../../../../Context/Context';
import './account-handle.style.scss'
import AccountAddCrypto from './AccountAddCrypto';
import AccountAddFiat from './AccountAddFiat';
import AccountHandleStepOne from './AccountHandleStepOne';
import AccountList from './AccountList';
export default function AccountHandle({ accountsCoinList, selectedCoin, crypto, setLocalStep }) {
    const [accountStep, setAccountStep] = useState(0);
    const [buttonList, setButtonList] = useState([]);
    const [deleteState, setDeleteState] = useState(false)
    const agency = useContext(Agency);

    const { selectedFriendDetails, currentApp, getFriendsProfile, callUserDetail, editUserAccount, currentUserDetailsTemp, changeNotification } = agency
    const [selectedAccount, setSelectedAccount] = useState([])

    useEffect(() => {
        if (!accountsCoinList.includes(selectedCoin.coinSymbol)) setButtonList(buttons.slice(0, 1))
        else setButtonList(buttons)
        if (editUserAccount) {
            if (crypto) {
                if (!currentUserDetailsTemp.accounts?.crypto?.[selectedCoin.coinSymbol]) {
                    setSelectedAccount({})

                } else {
                    setSelectedAccount(currentUserDetailsTemp.accounts?.crypto[selectedCoin.coinSymbol][0])
                }
            } else {
                if (!currentUserDetailsTemp.accounts?.fiat?.[selectedCoin.coinSymbol]) {
                    setSelectedAccount({})
                } else {
                    setSelectedAccount(currentUserDetailsTemp.accounts?.fiat[selectedCoin.coinSymbol][0])
                }
            }

        } else {
            if (crypto) {
                if (!selectedFriendDetails.accounts?.crypto?.[selectedCoin.coinSymbol]) {
                    setSelectedAccount({})

                } else {
                    setSelectedAccount(selectedFriendDetails.accounts?.crypto[selectedCoin.coinSymbol][0])
                }
            } else {
                if (!selectedFriendDetails.accounts?.fiat?.[selectedCoin.coinSymbol]) {
                    setSelectedAccount({})
                } else {
                    setSelectedAccount(selectedFriendDetails.accounts?.fiat[selectedCoin.coinSymbol][0])
                }
            }
        }


    }, [])
    React.useEffect(() => {
        if(deleteState){
            setAccountStep(1);
        }else{

        }

    }, [deleteState])


    const handleSelectedAccount = (obj, which) =>{
        if(which){
            handleDeleteAccount();
            return;
        }else{
            setSelectedAccount(obj);
            setAccountStep(2)

        }
    }
    const handleAddAccount = async (state) => {
        let temp = {
            currency: selectedCoin.coinSymbol,
            AccountingToolInstitutionalID: state.institutionalId,
            AccountNo: state.account,
            additionalInfo: {},
            appcode: currentApp.app_code
        }
        let res;
        try {
            res = editUserAccount ? await AddUserFiatAccount(temp) : await AddFiatAccount({ ...temp, friendEmail: selectedFriendDetails.friendEmail });
            changeNotification({ status: true, message: "Account Added" })
            editUserAccount ? callUserDetail() : getFriendsProfile(selectedFriendDetails.friendEmail);
            setLocalStep(0)
        } catch (e) {
            changeNotification({ status: false, message: "Failed" })
            setLocalStep(0)
        }
    }
    const handleDeleteAccount =async () =>{
        try{
            let del =await deleteAccount(selectedAccount.accountID);
            changeNotification({ status: true, message: "Account Deleted" })
            setDeleteState(false)
            setLocalStep(0);
            editUserAccount ? callUserDetail() : getFriendsProfile(selectedFriendDetails.friendEmail);

        }catch(e){
            changeNotification({ status: false, message: "Account Deleted" })
            setDeleteState(false)
            setLocalStep(0);

        }
    }
    const handleUpdateAccount = async (state) => {
        // alert("Update Fiat")
        let temp = {
            accountID: selectedAccount.accountID,
            AccountingToolInstitutionalID: state.institutionalId,
            AccountNo: state.account,
            additionalInfo: {}
        }
        let res;
        try {
            res = await updateFiatAccount(temp);
            changeNotification({ status: true, message: "Account Updated" })
            if (editUserAccount) callUserDetail()
            else getFriendsProfile(selectedFriendDetails.friendEmail)
            setLocalStep(0)

        } catch (e) {
            setLocalStep(0)
            changeNotification({ status: false, message: "Update failed" })

        }
        if (res.status >= 400) {
        } else {

        }
    }
    const handleAddAccountCrypto = async (state) => {
        let temp = {
            currency: selectedCoin.coinSymbol,
            coinTicker: "string",
            address: state.address,
            addressNickName: state.addressNickName,
            appcode: currentApp.app_code,
            // friendEmail: selectedFriendDetails.friendEmail
        }
        let res;
        try {
            res = editUserAccount ? await addUserCryptoAccount(temp) : await addCryptoAccount({ ...temp, friendEmail: selectedFriendDetails.friendEmail });
            changeNotification({ status: true, message: "Account Added" })
            setLocalStep(0)
            if (editUserAccount) callUserDetail()
            else getFriendsProfile(selectedFriendDetails.friendEmail)
        } catch (e) {
            changeNotification({ status: false, message: "Failed" })
            setLocalStep(0)
            console.error(e);
        }


    }
    const handleUpdateAccountCrypto = async (state) => {
        let temp = {
            accountID: selectedAccount.accountID,
            coinTicker: "string",
            address: state.address,
            addressNickName: state.addressNickName,
        }
        let res;
        try {
            res = await updateCryptoAccount(temp)
            changeNotification({ status: true, message: "Account Updated" })
            if (editUserAccount) callUserDetail()
            else getFriendsProfile(selectedFriendDetails.friendEmail)
            setLocalStep(0)

        } catch (e) {
            alert(res.status)
            getFriendsProfile(selectedFriendDetails.friendEmail);
            setLocalStep(0)
            changeNotification({ status: true, message: "Account Updated" })

        }
    }

    const selectComponent = () => {
        switch (accountStep) {
            case 0: return <AccountHandleStepOne setDeleteState={setDeleteState} handleDeleteAccount={handleDeleteAccount} setAccountStep={setAccountStep} buttonList={buttonList} />;
            case 1: return <AccountList setSelectedAccount={setSelectedAccount} deleteState={deleteState} setAccountStep={setAccountStep} handleSelectedAccount={handleSelectedAccount} coin={selectedCoin.coinSymbol} crypto={crypto} />
            case 2: return crypto ?
                <AccountAddCrypto selectedAccount={selectedAccount} update={true} handleUpdateAccountCrypto={handleUpdateAccountCrypto} coin={selectedCoin.coinSymbol} />
                :
                <AccountAddFiat selectedAccount={selectedAccount} update={true} handleUpdateAccount={handleUpdateAccount} coin={selectedCoin.coinSymbol} />;
            case 3: return crypto ?
                <AccountAddCrypto handleAddAccountCrypto={handleAddAccountCrypto} coin={selectedCoin.coinSymbol} />
                :
                <AccountAddFiat handleAddAccount={handleAddAccount} coin={selectedCoin.coinSymbol} />;
            default: return;
        }
    }
    return (
        <div className="account-handle">
            {selectComponent()}
        </div>
    )
}
const buttons = [
    { keyId: nextId(), name: "Add Account", id: "add" },
    { keyId: nextId(), name: "Update Account", id: "update" },
    { keyId: nextId(), name: "Remove Account", id: "remove" }
]