import React from 'react'
import { AddFiatAccount } from '../../../../../../../../../services/postAPIs';
import { Agency } from '../../../../../../../../Context/Context'

export default function AccountAddFiat({ coin,update,selectedAccount, handleUpdateAccount, handleAddAccount }) {
    const agency = React.useContext(Agency);
    const [state, setState] = React.useState({institutionalId:'',account: ''})
    const [load, setLoad] = React.useState(false)
    React.useEffect(()=>{
        if(update){
            setState({institutionalId: selectedAccount.AccountingToolInstitutionalID, account: selectedAccount.AccountNo})
        }
    },[])

    const handleChange= (e) =>{
        setState({...state, [e.target.name]: e.target.value})
    }
    return (
        <div className="account-add-fiat">
            <div className="account-add-fiat-header">
                <h2>{update?"Update":"Add"} {coin} Account</h2>
            </div>
            <div className="account-add-fiat-body">
                <h6><span>Institutional ID:</span> <input value ={state.institutionalId} name="institutionalId" onChange={handleChange} placeholder="Institutional ID" /> </h6>
                <h6><span>Account No:</span> <input value={state.account} name="account" onChange={handleChange} placeholder="Account No." /> </h6>
                <button onClick = {()=>{update? handleUpdateAccount(state): handleAddAccount(state);setLoad(true)}}>{load?`${update?"Updating":"Adding"} ${coin} Account....`:`${update?"Update":"Add"} Account`}</button>
            </div>
        </div>
    )
}
