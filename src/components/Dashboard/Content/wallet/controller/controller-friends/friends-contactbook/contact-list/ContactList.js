import React, { useContext, useState, useEffect } from 'react'
import './contact-list.style.scss'
import ContactCard from '../../../../../../../common-components/contact-card/ContactCard'
import { Agency } from '../../../../../../../Context/Context'
export default function ContactList({ searchTerm, setStepTwo, setAddFriend }) {
    const agency = useContext(Agency);
    const [displayFriendsList, setDisplayFriendsList] = useState([])
    const { friendList, selectedFriend, userAccountDetails,setCurrentUserDetailsTemp,setEditUserAccount,currentUserDetails, setSelectedFriend } = agency;
    const handleClick = (obj, alpha) => {
        if(alpha === "#You"){
            setCurrentUserDetailsTemp(obj)
            setEditUserAccount(true);
            setStepTwo(1)
            setSelectedFriend(obj,true)
            return;
        }

        setEditUserAccount(false);
        setStepTwo(1)
        setSelectedFriend(obj)

        // }
    }
    useEffect(() => {
        sortAndAssign();

      
    }, [friendList])
    const sortAndAssign = () => {
        let temp = friendList === null ? [] : friendList;
        let sortTemp = temp.sort(compare);
        let userDetails = {
            alpha: "#You", list: [{...userAccountDetails, nickName:currentUserDetails.username,friendEmail: currentUserDetails.email, profilePic: currentUserDetails.profile_img }]
        }
        setDisplayFriendsList([userDetails, ...sortTemp])
    }
    const compare = (a, b) => {
        const alphaA = a.alpha.toUpperCase();
        const alphaB = b.alpha.toUpperCase();
        let comparison = 0;
        if (alphaA > alphaB) {
            comparison = 1;
        } else if (alphaA < alphaB) {
            comparison = -1;
        }
        return comparison;
    }


    return (
        <div className="contact-list">

            {
                displayFriendsList.length === 0 ?
                    <div className="no-friends">
                        <h6>No Friends Found.</h6>
                        <button onClick={() => setAddFriend(true)}>Add a new friend</button>
                    </div>
                    :
                    displayFriendsList.filter(obj => { return obj.alpha.includes(searchTerm.substring(0, 1).toUpperCase()) })
                        .map(obj =>
                            <div className="contact-group" key={obj.keyId}>
                                <h6>{obj.alpha}</h6>
                                <div className="contact-wrapper">
                                    {
                                        obj.list.filter(
                                            objTwo => {
                                                return objTwo.nickName?.toLowerCase()?.startsWith(searchTerm.toLowerCase()) || objTwo.email?.toLowerCase()?.startsWith(searchTerm.toLowerCase())
                                            }
                                        ).map(objThree =>
                                            <ContactCard
                                                onClick={() => handleClick(objThree, obj.alpha)}
                                                selected={objThree.nickName === selectedFriend.nickName}
                                                image={objThree.profilePic}
                                                name={objThree.nickName}
                                            />)
                                    }
                                </div>

                            </div>
                        )
            }

        </div>
    )
}
