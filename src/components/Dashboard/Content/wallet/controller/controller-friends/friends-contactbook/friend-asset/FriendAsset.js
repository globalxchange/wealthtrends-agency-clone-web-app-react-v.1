import React, { useState, useContext, useEffect } from 'react'
import './friend-asset.style.scss'
import Images from '../../../../../../../../assets/a-exporter'
import nextId from 'react-id-generator'
import { Agency } from '../../../../../../../Context/Context'
import { addFriend } from '../../../../../../../../services/postAPIs'
import LoadingAnimation from '../../../../../../../../lotties/LoadingAnimation'
import AccountHandle from './account-handle/AccountHandle'
export default function FriendAsset({ setStepTwo, setAddFriend, ew }) {
    const agency = useContext(Agency)
    const [selected, setSelected] = useState(options[0]);
    const [displayList, setDisplayList] = useState([]);
    const [selectedCoinList, setSelectedCoinList] = useState([]);
    const [accounts, setAccounts] = useState([]);
    const [loading, setLoading] = useState(false);
    const [accountsCoinList, setAccountsCoinList] = useState([])
    const [result, setResult] = useState(null);
    const [message, setMessage] = useState('');
    const [localStep, setLocalStep] = useState(0);
    const [selectedCoin, setSelectedCoin] = useState({})
    const { addFriendDetails, setContactStep, updateAddFriendDetails,currentUserDetailsTemp, editUserAccount,selectedFriendDetails, isGxUser } = agency;

    const handleList = (obj) => {
        setLocalStep(1)
        setSelectedCoin(obj)
    }
    useEffect(() => {
        isGxUser ? handleSubmit() : console.log()

    }, [])
    const handleSubmit = () => {
    }

    useEffect(() => {
        if (selected.name === "Crypto") {
            setDisplayList(agency.allCoins.crypto);
            setAccountsCoinList(editUserAccount?currentUserDetailsTemp.cryptoList:selectedFriendDetails.cryptoList)
        }
        else {
            setDisplayList(agency.allCoins.fiat);
            setAccountsCoinList(editUserAccount?currentUserDetailsTemp.fiatList: selectedFriendDetails.fiatList)
        }
    }, [selected, selectedFriendDetails])
    return (

        loading ?
            <div className="w-100 h-100 d-flex align-items-center justify-content-center flex-column">
                <LoadingAnimation type={result === null ? "add" : result ? "done" : "failed"} size={{ height: 150, width: 110 }} loop={result === null ? true : false} />
                <h6>{result === null ? "Adding Your Friend...." : result ? "Friend Added Successfully." : message}</h6>
            </div>
            :
            <div className="friend-asset">
                <div className="friend-asset-header">
                    <img src={editUserAccount?currentUserDetailsTemp.profilePic: selectedFriendDetails.profilePic} />
                    <div>
                        <h5>{editUserAccount?currentUserDetailsTemp.nickName: selectedFriendDetails.nickName}</h5>
                        <p>{editUserAccount?currentUserDetailsTemp.friendEmail:selectedFriendDetails.friendEmail}</p>
                    </div>
                    <button
                        onClick={() =>ew? setStepTwo(): setContactStep(0)}
                        disabled={false
                        }
                        className="complete-button">Save</button>

                </div>
                <div className="friend-asset-wrapper">
                    <div className="friend-asset-sub-header">
                        {
                            options.map(obj =>
                                <button
                                    onClick={() => {setLocalStep(0);setSelected(obj)}}
                                    className={selected.name === obj.name ? "selected" : ""}
                                    key={obj.keyId}>{obj.name}</button>
                            )
                        }

                    </div>
                    <div className="friend-asset-body">
                        {
                            localStep ?
                                <AccountHandle crypto={selected.crypto} selectedCoin={selectedCoin} accountsCoinList={accountsCoinList} setLocalStep={setLocalStep} />
                                :
                                displayList.map(obj =>
                                    <div className={selectedCoinList.includes(obj.coinSymbol) ? "selected friend-asset-body-row" : "friend-asset-body-row"}>
                                        <span><img src={obj.coinImage} />{obj.coinName}</span>
                                        <button className={accountsCoinList.includes(obj.coinSymbol)?"update-button":""} onClick={() => handleList(obj)}>{accountsCoinList.includes(obj.coinSymbol) ? "Open" : "Add"}</button>
                                    </div>
                                )
                        }
                    </div>
                </div>

            </div>

    )
}
const options = [
    { keyId: nextId(), name: "Crypto" , crypto: true},
    { keyId: nextId(), name: "Fiat", crypto: false }

]