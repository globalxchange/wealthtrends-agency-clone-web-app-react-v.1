import React, { useContext, useEffect } from 'react'
import { Agency } from '../../../../../../../../Context/Context'

export default function AccountList({ coin,deleteState, crypto,setAccountStep,setSelectedAccount, handleSelectedAccount }) {
    const agency = useContext(Agency);
    const { selectedFriendDetails, currencyImageList,editUserAccount,userAccountDetails } = agency;
    const [currentList, setCurrentList] = React.useState([])

    useEffect(() => {
        if(editUserAccount){
            setCurrentList(crypto ? userAccountDetails.accounts.crypto[coin] : userAccountDetails.accounts.fiat[coin])

        }else{
            setCurrentList(crypto ? selectedFriendDetails.accounts.crypto[coin] : selectedFriendDetails.accounts.fiat[coin])

        }
    }, [])
    const handleClick = (obj) =>{
        if(!deleteState){
            handleSelectedAccount(obj, false);

        }else{
            setSelectedAccount(obj)
            let a = setTimeout(()=>{handleSelectedAccount(obj, true); clearTimeout(a)},100) ;

        }
        
    }

    return (
        <div className="account-list">
            {
                currentList.map(obj =>
                    <div onClick={()=>handleClick(obj)} className="account-list-row">
                        <div>
                            <img src={currencyImageList[coin]} />
                        </div>
                        <div>
                            <p>{crypto?"Address":"Account No."}</p>
                            <h6>{crypto?obj.address :obj.AccountNo}</h6>
                        </div>
                    </div>)
            }

        </div>
    )
}
