import React, { useContext, useState, useEffect } from 'react'
import './friend-upload.style.scss'
import jwt from 'jsonwebtoken'
import { Agency } from '../../../../../../../Context/Context'
import Images from '../../../../../../../../assets/a-exporter'
import Axios from 'axios'
import nextId from 'react-id-generator'
import LoadingAnimation from '../../../../../../../../lotties/LoadingAnimation'
import { addFriend } from '../../../../../../../../services/postAPIs'
export default function FriendUpload({ onDone, hide, setStep, setAddFriend }) {
    const agency = useContext(Agency);
    const [details, setDetails] = useState({ profilePic: '', nickName: '' });
    const [allow, setAllow] = useState(false)
    const { isGxUser, addFriendDetails, currentApp, onlyNumbers, getFriendsList, theme, updateAddFriendDetails, setMainModal, croppedImage, setCroppedImage, setCroppingImage, setImageUploading } = agency;
    const [localImage, setLocalImage] = useState('')
    const [imageUploadResult, setImageUploadResult] = useState(true)
    const [loading, setLoading] = useState(false);
    const [result, setResult] = useState(null);
    const [message, setMessage] = useState('');
    const numRegex = /^\d{9}$/

    const handleSubmit = () => {
        updateAddFriendDetails({ ...addFriendDetails, ...details });        // checkDetails()
    }
    useEffect(() => {
        if (addFriendDetails.nickName !== null) {
            handleSubmitFinal()
        }
    }, [addFriendDetails])

    const handleSubmitFinal = async () => {

        setLoading(true);
        let res;
        try {
            res = await addFriend(addFriendDetails);
            getFriendsList();
            setResult(true)
            updateAddFriendDetails({
                friendEmail: null,
                nickName: null,
                profilePic: null,
                app_code: currentApp.app_code
            });
            let a = setTimeout(() => {
                setLoading(false);
                getFriendsList();
                hide ? onDone() : console.log()
                setAddFriend(null);
                setStep(0);
                clearTimeout(a);
            }, 1200);
        } catch (e) {
            updateAddFriendDetails({
                friendEmail: null,
                nickName: null,
                profilePic: null,
                app_code: currentApp.app_code
            });
            setResult(false);
            setMessage(res?.data?.message)
            let a = setTimeout(() => {
                setLoading(false)
                setStep(0);
                clearTimeout(a);
            }, 1200);


        }

    }

    const getUnique = () => {
        let str = `${addFriendDetails.email}${Date.now()}`
        return str;
    }
    const secret = 'uyrw7826^&(896GYUFWE&*#GBjkbuaf'; //secret not to be disclosed anywhere.
    const devEmail = 'rahulrajsb@outlook.com'; //email of the developer.
    const path_inside_brain = 'root/bets/profileImage/';

    const handleChange = (e) => {
        checkForAllow(false);
        setDetails({ ...details, [e.target.name]: e.target.value })
    }
    const handleImageChange = (e) => {
        if (e.target.files[0] === undefined) {
            return
        }
        e.preventDefault();
        const fileReader = new FileReader()
        fileReader.onloadend = () => {
            setCroppingImage(fileReader.result);
        }
        fileReader.readAsDataURL(e.target.files[0])
    }
    useEffect(() => {
        if (croppedImage.done)
            imageUploader(croppedImage.url)
    }, [croppedImage])
    useEffect(() => {
        isGxUser ? console.log() : setDetails({ ...details, fullName: '', phone: '' })
    }, [isGxUser])

    const imageUploader = async (url) => {
        setImageUploading(true)
        let fileName = getUnique();
        let file = url;
        const token = jwt.sign({ name: fileName, email: devEmail }, secret, {
            algorithm: 'HS512',
            expiresIn: 240,
            issuer: 'gxjwtenchs512',

        });
        const formData = new FormData();
        formData.append('files', new File([file], fileName, { type: file.type }));
        try {
            const res = await Axios.post(
                `https://drivetest.globalxchange.io/file/dev-upload-file?email=${devEmail}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
                formData,
                {
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            const { data } = res;
            if (data.payload && data.payload.upload_success) {
                setDetails({ ...details, profilePic: data.payload.url });
                setLocalImage(data.payload.url && data.payload.url)
                setCroppedImage({ done: false, url: '' })
                setMainModal(false);
                setImageUploading(false);
                checkForAllow(true);
            } else {
                setImageUploadResult(false);
                setLocalImage('');
                setMainModal(false);
                setCroppedImage({ done: false, url: '' })
                setImageUploading(false)
            }
        }
        catch (err) {
            console.log('err.message', err.message);
        }
        return 0

    }
    const checkForAllow = (extra) => {
        if (isGxUser) {
            if (details.nickName !== '' && (details.profilePic !== '' || extra)) {
                setAllow(true);
            } else {
                setAllow(false)
            }
        } else {
            if (details.nickName !== '' && (details.profilePic !== '' || extra) && details.fullName !== '' && numRegex.test(details.phone)) {
                setAllow(true);
            } else {
                setAllow(false)
            }

        }

    }


    return (

        loading ?
            <div className="w-100 h-100 d-flex align-items-center justify-content-center flex-column">
                <LoadingAnimation type={result === null ? "add" : result ? "done" : "failed"} size={{ height: 150, width: 110 }} loop={result === null ? true : false} />
                <h6>{result === null ? "Adding Your Friend...." : result ? "Friend Added Successfully." : message}</h6>
            </div>
            :
            <form onSubmit={(e) => e.preventDefault()} className="friend-upload">
                <label style={imageUploadResult ? {} : { border: '2px solid red' }} htmlFor="upload" className="d-flex justify-content-center align-items-center">
                    <input onChange={handleImageChange} id="upload" className="d-none" type="file" accept="image/*" />
                    <img onError={(e) => e.target.src = Images.src} className={localImage === '' ? '' : "go-full"} src={localImage === '' ? Images.camera : localImage} />
                </label>
                <div className="mb-4">
                    <h6>Assign A Nickname</h6>
                    <input
                        autocomplete="off"
                        onKeyPress={e => e.which === 13 && details.nickName !== '' ? isGxUser ? handleSubmit() : '' : ''}
                        onChange={(e) => handleChange(e)}
                        name="nickName"
                        style={isGxUser ? {} : { fontSize: "130%" }}
                        autoFocus={true}
                        placeholder="Ex.KingShorupan"
                        important
                    />
                </div>
                <div className={isGxUser ? "d-none" : "d-flex"}>
                    <div className="w-50">
                        <h6>Enter Full name</h6>
                        <input
                            autocomplete="off"
                            onChange={(e) => handleChange(e)}
                            name="fullName" style={{ fontSize: "120%" }}
                            className="w-100"
                            placeholder="Ex.John Snow"
                            important={isGxUser ? false : true}
                        />
                    </div>
                    <div className="w-50">
                        <h6>Enter Phone Number</h6>
                        <input
                            autocomplete="off"

                            onKeyPress={e => e.which === 13 && details.nickName !== '' ? allow ? handleSubmit() : '' : onlyNumbers(e)}
                            onChange={(e) => handleChange(e)}
                            placeholder="Ex.+91 XXX XXX XXXX"
                            name="phone" style={{ fontSize: "120%" }}
                            className="w-100"
                            important={isGxUser ? false : true}
                        />
                    </div>
                </div>
                <button onClick={() => handleSubmit()} className={allow ? "next-button" : "d-none"}><img src={theme === "light" ? Images.nextButton : Images.nextButtonInvert} /></button>
            </form>
    )
}
