import React from 'react'
import LoadingAnimation from '../../../../../../../../../../lotties/LoadingAnimation'

export default function FDOStepThreeLoading() {
    return (
        <div className="h-100 h-100 d-flex justify-content-center align-items-center">
            <LoadingAnimation type="login" size={{ height: 170, width: 170 }} />
        </div>
    )
}
