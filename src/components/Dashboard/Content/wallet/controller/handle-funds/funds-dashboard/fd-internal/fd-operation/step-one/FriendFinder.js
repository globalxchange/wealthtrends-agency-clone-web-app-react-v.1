import React from 'react'
import Constant from '../../../../../../../../../../json/constant'
export default function FriendFinder({handleTransferMoney}) {
    return (
        <div className="friend-finder">
            <div>
                <h6>Who Are You Sending Money To?</h6>
            </div>
            {
                Constant.friendFinder.map(obj =>
                    <div key={obj.keyId}>
                        <span>{obj.text}</span>
                        <button onClick= {()=>handleTransferMoney(obj, 1)}>
                            <img src={obj.image} /> 
                        </button>

                    </div>
                    )
            }

            
        </div>
    )
}
