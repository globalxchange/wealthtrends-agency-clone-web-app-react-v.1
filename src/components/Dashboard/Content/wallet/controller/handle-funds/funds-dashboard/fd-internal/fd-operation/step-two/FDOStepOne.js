import React from 'react'
import { Agency } from '../../../../../../../../../Context/Context'

export default function FDOStepOne({ deposit, setTwoSteps }) {
    const agency = React.useContext(Agency);
    const { currentApp, fundsConfig, fundsFrom, valueFormatter, fundsTo, nameImageList } = agency;
    
    return (
        <div className="fund-operation-step-two-home">
            <div className="fd-home-title">
                <span><img src={deposit?fundsTo?.app_icon:fundsFrom?.app_icon} /></span>
                <span>{deposit?fundsTo?.app_name:fundsFrom?.app_name}</span>
            </div>
            <div className="fd-home-body">
                <h6>{deposit ? "Deposit Into" : "Withdrawing From"}</h6>
                <div>
                    <span><img src={nameImageList[deposit ? fundsTo?.coin : fundsFrom?.coin]?.coinImage} />{nameImageList[deposit ? fundsTo?.coin : fundsFrom?.coin]?.coinName}</span>
                    <span>{valueFormatter(deposit ? fundsTo?.coinValue : fundsFrom?.coinValue, deposit ? fundsTo?.coin : fundsFrom?.coin)}</span>
                </div>
                <p onClick={() =>{if(!fundsFrom)return;  setTwoSteps(4) }}>I want To Change The Asset</p>
            </div>
            <div className="fd-home-footer">
                <button disabled={!fundsFrom || !fundsTo} onClick={() => deposit ?!fundsFrom?.coin?setTwoSteps(1): setTwoSteps(3) :!fundsTo?.coin?setTwoSteps(2): setTwoSteps(3)}>Proceed</button>
            </div>
        </div>
    )
}
