import React from 'react'
import Images from '../../../../../../../../../../assets/a-exporter';
import { Agency } from '../../../../../../../../../Context/Context'

export default function FDOStepTwoOverview({setTwoSteps}) {
    const agency = React.useContext(Agency);
    const { fundsFrom, fundsTo, valueFormatter, nameImageList } = agency

    return (
        <div className="fund-operation-step-two-overview">
            <div className="fo-overview-body">
                {
                    [fundsFrom, fundsTo].map((obj, i) =>
                        <div className="overview-child-wrapper">
                            <h4><img src={obj?.app_icon} /> {obj?.app_name}</h4>
                            <p>
                                <span>{i ? "Depositing Into" : "Withdrawing From"}</span>
                                <span onClick={()=>i?setTwoSteps(2):setTwoSteps(1)}>Change</span></p>
                            <div>
                                <span><img src={nameImageList[obj?.coin]?.coinImage} />{nameImageList[obj?.coin]?.coinName}</span>
                                <span>{valueFormatter(obj?.coinValue, obj?.coin)}</span>
                            </div>
                        </div>
                    )
                }
            </div>
            <div className="fo-overview-footer">
                <button onClick={()=>setTwoSteps(0)}><img src={Images.fancyNext} /></button>
                <button>Proceed</button>
            </div>

        </div>
    )
}
