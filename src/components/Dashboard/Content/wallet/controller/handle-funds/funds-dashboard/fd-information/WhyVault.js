import React from 'react'
import Images from '../../../../../../../../assets/a-exporter'

export default function WhyVault() {
    return (
        <div className="why-blockcheck-main">
            <div>
                <h4>
                    Where Would You Use <img src={Images.hfVault} />
                </h4>
                <p>When You Are Trying To Send Money To Another Vault Supported App</p>
            </div>
            <button>Use Vault</button>

        </div>
    )
}
