import React from 'react'
import Images from '../../../../../../../../../../assets/a-exporter'
import LoadingAnimation from '../../../../../../../../../../lotties/LoadingAnimation';
import { Agency } from '../../../../../../../../../Context/Context'

export default function SelectedApps() {
    const agency = React.useContext(Agency);
    const { setFundsSelectedApp, fundsSelectedApp,currentApp, fundsFrom, fundsTo } = agency;

    React.useEffect(() => {
        setFundsSelectedApp(fundsTo);
    }, [fundsFrom?.profile_id, fundsTo?.profile_id])
    return (
        <React.Fragment>
            {
                fundsFrom === null || fundsTo === null
                    ?
                    <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                        <LoadingAnimation type="login" size={{ height: 150, width: 150 }} />
                    </div>
                    :
                    <div className="selected-app">
                        <div className="selected-app-header">
                            <img src={Images.connect} />
                        </div>
                        <div className="selected-app-body">
                            {
                                [fundsFrom, fundsTo].map((obj, i) =>
                                    <div className={`app-wrapper ${fundsSelectedApp?.profile_id === obj.profile_id ? "" : ""} `}>
                                        <span>Sending {!i ? "From" : "To"}</span>
                                        <div onClick={() => setFundsSelectedApp({...obj, to: i})}>
                                            <img src={obj.app_icon} />
                                            <p className={obj.profile_id === currentApp.profile_id?"":"d-none"}>Current</p>
                                        </div>
                                        <span>{obj.app_name}</span>
                                    </div>
                                )
                            }
                        </div>
                        <div />
                    </div>
            }
        </React.Fragment>
    )
}
