import React from 'react'
import FDOperation from './fd-internal/fd-operation/FDOperation';
import FDResult from './fd-internal/fd-result/FDResult';
import './funds-dashboard.style.scss'
export default function FundsDashboard({ steps, changeSteps }) {
    const selectComponent = () => {
        switch (steps) {
            case 0: return <FDOperation changeSteps={changeSteps} />;
            case 1: return <FDResult />;
        }
    }
    return (
        <div className="funds-dashboard">
            {
                selectComponent()
            }

        </div>
    )
}
