import React from 'react'
import { Agency } from '../../../../../../../../Context/Context'
import './fd-operation.style.scss'
import FDAppContainer from './step-one/FDAppContainer'
import FDOStepThree from './step-three/FDOStepThree'
import FDOStepTwoList from './step-two/FDOStepTwoList'
import FriendFinder from './step-one/FriendFinder'
import Connect from './step-one/Connect'
import ConnectList from './step-one/ConnectList'
import SelectedApps from './step-one/SelectedApps'
import FDOStepThreeLoading from './step-three/FDOStepThreeLoading'
import FDOStepThreeResult from './step-three/FDOStepThreeResult'
import FDOStepOne from './step-two/FDOStepOne'
import FDOStepTwoOverview from './step-two/FDOStepTwoOverview'
export default function FDOperation() {
    const [oneSteps, setOneSteps] = React.useState(0);
    const [twoSteps, setTwoSteps] = React.useState(0);
    const [activeSection,setActiveSection] = React.useState("one")
    const [friendAppList, setFriendAppList] = React.useState([]);
    const agency = React.useContext(Agency);
    const { fundsStepOneConfig, currentApp, fundsTo, fundsFrom, threeSteps, setAssetClassSelected, setScrollValue, setThreeSteps, setFundsFrom, fundsConfig, setFundsTo } = agency;
    const stepOneSelection = () => {
        switch (oneSteps) {
            case 0: return;
            case 1: return <FDAppContainer handleApp={handleApp} deposit={true} />
            case 2: return <FDAppContainer handleApp={handleApp} deposit={false} />
            case 3: return <FriendFinder handleTransferMoney={handleTransferMoney} />;
            case 4: return <Connect setFriendAppList={setFriendAppList} handleTransferMoney={handleTransferMoney} />;
            case 5: return <ConnectList handleTransferMoney={handleTransferMoney} friendAppList={friendAppList} />;
            case 6: return <SelectedApps />
        }
    }
    const steTwoSelection = () => {
        switch (twoSteps) {
            case 0: return <FDOStepOne setTwoSteps={setTwoSteps} deposit={fundsConfig.add} />
            case 1: return <FDOStepTwoList handleAsset={handleAsset} setTwoSteps={setTwoSteps} from={true} />
            case 2: return <FDOStepTwoList handleAsset={handleAsset} setTwoSteps={setTwoSteps} from={false} />
            case 3: return <FDOStepTwoOverview setTwoSteps={setTwoSteps} />
            case 4: return <FDOStepTwoList handleAsset={handleAsset} setTwoSteps={setTwoSteps} from={!fundsConfig.add} other={true} />

        }

    }
    const stepThreeSelection = () => {
        switch (threeSteps) {
            case 0: return <FDOStepThree />;
            case 1: return <FDOStepThreeLoading />;
            case 2: return <FDOStepThreeResult />

        }
    }
    const handleAsset = (from, obj, num, other) => {
        if (from) {
            setFundsFrom({ ...fundsFrom, coinValue: obj.coinValue, coin: obj.coinSymbol, type : obj.asset_type });
            other?setTwoSteps(0):setTwoSteps(3);
        } else {
            setFundsTo({ ...fundsTo, coinValue: obj.coinValue, coin: obj.coinSymbol, type : obj.asset_type })
            other?setTwoSteps(0):setTwoSteps(3);
        }

    }
    const handleApp = (obj, fromDeposit) => {
        switch (fundsStepOneConfig.type) {
            case "ed": return;
            case "iw":
            case "id": handleInternal(obj, fromDeposit); break;
            case "tm": handleTransferMoney(obj, 0); break;
            default: return;
        }
    }
    const handleInternal = (obj, fromDeposit) => {
        if (fundsStepOneConfig.both) {
            if (fromDeposit) {
                setFundsFrom({ coinValue: fundsConfig.coin.coinValue, coin: fundsConfig.coin.coinSymbol,type: fundsConfig.coin.asset_type , ...obj });
                setOneSteps(2);
            } else {
                setFundsTo({ coinValue: fundsConfig.coin.coinValue, coin: fundsConfig.coin.coinSymbol,type: fundsConfig.coin.asset_type , ...obj });
                setOneSteps(6);
            }
        } else if (fundsConfig.add) {
            setFundsFrom({ coinValue: '', coin: '', ...obj });
            setFundsTo({ coinValue: fundsConfig.coin.coinValue, coin: fundsConfig.coin.coinSymbol, ...currentApp,type: fundsConfig.coin.asset_type , });
            setOneSteps(6);
        } else {
            setFundsTo({ coinValue: '', coin: '', ...obj });
            setFundsFrom({ coinValue: fundsConfig.coin.coinValue, coin: fundsConfig.coin.coinSymbol, ...currentApp,type: fundsConfig.coin.asset_type , });
            setOneSteps(6);
        }

    }
    const handleTransferMoney = (obj, comingFrom) => {
        switch (comingFrom) {
            case 0: setFundsFrom({ coinValue: fundsConfig.coin.coinValue, coin: fundsConfig.coin.coinSymbol, ...obj, type: fundsConfig.coin.asset_type  }); setOneSteps(3); break;
            case 1: setOneSteps(4); break;
            case 2: setOneSteps(3); break;
            case 3: setOneSteps(5); break;
            case 4: 
            setFundsTo({  ...obj });
                setOneSteps(6); break;
            default: break;
        }
    }
    const HandleExternal = (obj) => {


    }

    React.useEffect(() => {
        setOneSteps(fundsStepOneConfig.step);


    }, [fundsStepOneConfig]);
    React.useEffect(() => {
        if (fundsConfig.add) {
            setFundsTo({ coinValue: fundsConfig.coin.coinValue,type: fundsConfig.coin.asset_type , coin: fundsConfig.coin.coinSymbol, ...currentApp });
        } else {
            setFundsFrom({ coinValue: fundsConfig.coin.coinValue,type: fundsConfig.coin.asset_type , coin: fundsConfig.coin.coinSymbol, ...currentApp });
        }
    }, [fundsConfig.action])
    return (
        <div className="fd-operation">
            <div onClick={()=>activeSection==="one"?console.log():setActiveSection("one")} className={`fd-operation-left ${activeSection === "one"?"selected-section":""}`}>
                {
                    stepOneSelection()
                }
            </div>
            <div onClick={()=>activeSection==="two"?console.log():setActiveSection("two")} className={`fd-operation-center ${activeSection === "two"?"selected-section":""}`}>
                {
                    steTwoSelection()
                }
            </div>
            <div onClick={()=>activeSection==="three"?console.log():setActiveSection("three")} className={`fd-operation-right ${activeSection === "three"?"selected-section":""}`}>
                {stepThreeSelection()}
            </div>
        </div>
    )
}
