import React, { useContext } from 'react'
import Images from '../../../../../../../../assets/a-exporter';
import { Agency } from '../../../../../../../Context/Context'
import './fd-information.style.scss'
import WhyBlockCheck from './WhyBlockCheck';
import WhyVault from './WhyVault';
export default function FDInformation({whichOverlay, setWhichOverlay}) {
    const agency = useContext(Agency);
    const { fundsConfig, setCollapseSidebar, setSidebarAction } = agency;

    const handleClick = () =>{
        setCollapseSidebar(true);
        setSidebarAction({ count: 1, type: "" })
    }
    const selectOverlay = () =>{
        //ed id mm iv
        console.log("which overlay", whichOverlay)
        switch(whichOverlay?.short){
            case "ed": return <WhyBlockCheck />
            case "id": return <WhyVault />
            default: setWhichOverlay(null); break; 

        }
    }
    return (
        <div className="fd-information">
            <div className={`fd-information-overlay ${!whichOverlay?"":"show-overlay"}`}>
                {selectOverlay()}
            </div>
            <div className="fd-information-above">
                <h1>Transaction Helper</h1>
                <p>
                    Looks Like You Are Trying To {fundsConfig.add ? "Add" : "Send"} Some {fundsConfig.coin.coinName}. If So, Then
                </p>
                <button>
                    {[1,1,1].map(x=> <img src={Images.fancyNext } />)}
                    {
                        fundsConfig.add
                            ?
                            "Select The Destination Type"
                            :
                            "Select Source Of Transfer"
                    }
                </button>
            </div>
            <div className="fd-information-below">
                <p onClick={()=>handleClick()}>
                I Am Trying To Do A Different Type Of Transaction
                </p>

            </div>
        </div>
    )
}
