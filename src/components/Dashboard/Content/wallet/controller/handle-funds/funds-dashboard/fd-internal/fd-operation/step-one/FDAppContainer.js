import React from 'react'
import { Agency } from '../../../../../../../../../Context/Context'
import Images from '../../../../../../../../../../assets/a-exporter'
export default function FDAppContainer({ deposit, handleApp }) {
    const agency = React.useContext(Agency);
    const [appList, setAppList] = React.useState([]);
    const [searchTerm, setSearchTerm] = React.useState('');
    const [toggle, setToggle] = React.useState(false)

    const { allApps } = agency


    React.useEffect(() => {
        let list = allApps.filter(x =>{return x.app_name.toString().toLowerCase().startsWith(searchTerm.toString().toLowerCase())})
        let count = (list?.length / 3) + 1;
        let temp = [];
        for (let i = 0; i < count - 1; i++) {
            temp = [...temp, { list: list.slice(3 * i, 3 * (i + 1)) }]
        }
        setAppList(temp)
    }, [allApps, searchTerm]);
    


    return (
        <div className={`fund-operation-step-one ${deposit?"only-amim":"only-none-anim"}`}>
            <div className="fo-step-one-title">
                <span className="title-span"><span>Connect /</span><span>BlockCheck</span></span>
                {
                    deposit?
                    <h6 className="swipe-from-right">Which Of Your GX Apps Are You Sending From?</h6>:
                    <h6 className="swipe-from-right-two">Which Of Your GX Apps Are You Sending To?</h6>
                }
            </div>
            <div className={`fo-step-one-main ${deposit?"swipe-from-right":"swipe-from-right-two"} `}>
                <div className="fo-step-one-main-search-bar">
                    <input 
                    onChange={(e)=>setSearchTerm(e.target.value)} 
                    placeholder="Search Your Apps"
                     />
                </div>
                <div className="fo-step-one-main-app-containers">
                    {
                        appList.map(obj =>
                            <div className="app-row-wrapper">{
                                obj.list.map(objTwo =>
                                    <div onClick={()=>handleApp(objTwo, deposit)}>
                                        <span className="image-wrapper">
                                            <img onError={(e)=>e.target.src = Images.agencyDark} src={objTwo.app_icon} />
                                            <p>{objTwo.app_name}</p>
                                        </span>
                                    </div>
                                )
                            }</div>
                        )
                    }

                </div>

            </div>


        </div>
    )
}
