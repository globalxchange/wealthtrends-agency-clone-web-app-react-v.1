import React from 'react'
import Images from '../../../../../../../../assets/a-exporter'

export default function WhyBlockCheck() {
    return (
        <div className="why-blockcheck-main">
            <div>
                <h4>
                    Where Would You Use <img src={Images.blockCheck} />
                </h4>
                <p>You Would Use BlockCheck When You Need To Send Funds To An External Address Or Destination</p>
            </div>
            <button>Use BlockCheck</button>

        </div>
    )
}
