import React from 'react'
import LoadingAnimation from '../../../../../../../../../../lotties/LoadingAnimation';
import { getMainBalance } from '../../../../../../../../../../services/postAPIs';
import { Agency } from '../../../../../../../../../Context/Context';

export default function FDOStepTwoList({ from, handleAsset, other }) {
    const [fromList, setFromList] = React.useState([]);
    const [toList, setToList] = React.useState([]);
    const [currentList, setCurrentList] = React.useState([]);
    const [searchTerm, setSearchTerm] = React.useState('');

    const agency = React.useContext(Agency);
    const { fundsFrom, fundsTo, valueFormatter, currentApp, setFundsFrom, setFundsTo, fundsSelectedApp } = agency;

    const setList = async (which) => {
        if (which) {
            let res;
            try {
                res = await getMainBalance(fundsFrom.app_code, fundsFrom.profile_id);
                setFromList(res.data.coins_data)
            } catch (e) {
                console.error(e);
                setFromList([])
            }
        } else {
            let res;
            try {
                res = await getMainBalance(fundsTo.app_code, fundsTo.profile_id);
                setToList(res.data.coins_data)
            } catch (e) {
                console.error(e);
                setToList([])
            }
        }
    }
    React.useEffect(() => {
        {
            if (from) {
                setCurrentList([...fromList])
            } else if (!from) {
                setCurrentList([...toList])
            } else {
                setCurrentList([])
            }
        }
    }, [fundsSelectedApp, fromList, toList])


    React.useEffect(() => {
        if (fundsFrom !== null) {
            setList(true)
        }

    }, [fundsFrom])
    React.useEffect(() => {
        if (fundsTo !== null) {
            setList(false)
        }
    }, [fundsTo])

    return (
        <div className="fund-operation-step-two">
            <div className="fo-step-two-title">
                <h6>Pick {from ? "Fulfillment" : "Settlement"} Asset</h6>
                <span>From Your {from ? fundsFrom?.app_name : fundsTo?.app_name} Balances</span>
                <input onChange={(e) => setSearchTerm(e.target.value)} autoFocus={true} placeholder="Search Asset" />

            </div>
            <div className="fo-step-two-main">
                {
                    !currentList.length
                        ?
                        <div className="w-100 h-100 flex-column d-flex justify-content-center align-items-center">
                            <LoadingAnimation type="no-data" size={{ height: 120, width: 120 }} />
                            {/* <span className="font-weight-bold mt-3">Please Select An App</span> */}
                        </div>
                        :
                        currentList.filter(
                            x => { return x.coinSymbol?.toString()?.toLowerCase()?.startsWith(searchTerm?.toLowerCase()) || x.coinName?.toString()?.toLowerCase()?.startsWith(searchTerm?.toLowerCase()) }
                        )
                            .map((obj, i) =>
                                <div className={from ? fundsFrom?.coin === obj?.coinSymbol ? "selected-coin" : "" : fundsTo.coin === obj.coinSymbol ? "selected-coin" : ""} onClick={() => handleAsset(from, obj, i, other)}>
                                    <span><img src={obj?.coinImage} /> {obj?.coinName}</span>
                                    <span>{valueFormatter(obj?.coinValue, obj?.coinSymbol)}</span>
                                </div>
                            )
                }

            </div>

        </div>
    )
}
