import React from 'react'
import { Agency } from '../../../../../../../../../Context/Context'

export default function FDOStepThreeResult() {
    const agency = React.useContext(Agency);
    const {fundsResult,fundsFrom,setThreeSteps, fundsTo,valueFormatter, transferState} = agency;
    const handleClick = () =>{
        setThreeSteps(0);
    }
    return (
        <div className={`fod-step-three-result ${fundsResult.status?"successful":"Unsuccessful"}`}>
            <p>
                {
                fundsResult.status
                ?
                `Successfully Transferred ${valueFormatter(transferState?.to_amount, transferState?.to?.coin)}
                from ${fundsFrom?.app_name} to ${fundsTo?.app_name}
                `
                :
                `Unfortunately None Of Our Exchange Partners Currently Support The Exchange Of ${transferState?.from?.coin} To ${transferState?.to?.coin}.addCryptoAccount
                Please Try Another Settlement Currency.
                `
            }
            </p>
            <button onClick={()=>handleClick()}>Return Back</button>
            
        </div>
    )
}
