import React from 'react'
import Images from '../../../../../../../../../../assets/a-exporter'

export default function ConnectList({friendAppList, handleTransferMoney}) {
    return (
        <div className="connect-list">
            <div className="connect-list-header">
                <img src={Images.connect} />
                <span>Which App Do You Want To Send To?</span>
            </div>
            <div className="connect-list-body">
                <span className="shadow-new" />
                {
                    friendAppList.map(obj => <h6 onClick={()=>handleTransferMoney(obj, 4)}><img src={obj.app_icon} /> {obj.app_name}</h6>)
                }
            </div>
        </div>
    )
}
