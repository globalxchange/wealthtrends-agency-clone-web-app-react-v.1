import React from 'react'
import Images from '../../../../../../../../../../assets/a-exporter'
import LoadingAnimation from '../../../../../../../../../../lotties/LoadingAnimation';
import { friendAppList } from '../../../../../../../../../../services/getAPIs'
export default function Connect({ handleTransferMoney, setFriendAppList }) {
    const [localStep, setLocalStep] = React.useState(0);
    const [email, setEmail] = React.useState('');
    const [inValid, setInvalid] = React.useState(false)
    const [friend, setFriend] = React.useState({ email: null })
    const emailRegex = /([\w\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?/gm

    const handleEmail = () => {
        let check = emailRegex.test(email);
        if (check) {
            setLocalStep(2);
            getFriendApps()
            setInvalid(false);
        } else {

            setInvalid(true);
        }
    }
    const getFriendApps = async () => {
        let res;
        try {
            res = await friendAppList(email);
            if (res.data.status) {
                setFriend({ email: email });
                setFriendAppList(res.data.userApps)
            } else {
                setFriend({ email: null })
            }
            setLocalStep(1);


        } catch (e) {
            setFriend({ email: null })
            setLocalStep(1);
        }

    }
    const getClipboardText = () => {
        navigator.clipboard.readText()
            .then(
                text => setEmail(text)
            )
    }

    const selectComponent = () => {
        switch (localStep) {
            case 0: return <div className="connect-main-body">
                <div>
                    <span style={inValid ? { borderBottom: '1px solid red' } : {}}>
                        <input value={email} placeholder="Enter Email ID" onChange={(e) => setEmail(e.target.value)} className="Email" />
                        <img onClick={() => getClipboardText()} src={Images.pasteIcon} />
                    </span>
                    <button className="fancy-button-wrapper" onClick={() => handleEmail()}><img src={Images.fancyNext} /></button>
                </div>
            </div>;
            case 1: return <div className="connect-main-body-confirm">
                <div></div>
                <div className="email-details">
                    <img src={Images.face} />
                    <div>
                        <h6>{friend?.email === null ? "User Not found" : email.split('@')[0]}</h6>
                        <span>{friend?.email === null ? '' : friend?.email}</span>
                    </div>
                </div>
                <div className="bottom-buttons">
                    <button onClick={() => handleTransferMoney({}, 2)}>Go Back</button>
                    <button disabled={friend.email === null} onClick={() => handleTransferMoney({}, 3)}>Proceed</button>
                </div>
            </div>
            case 2: return <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation type="login" size={{ height: 150, width: 150 }} /></div>


        }
    }
    return (
        <div className="connect-main">
            <div className="connect-main-header">
                <img src={Images.connect} />
                <span>What Is The Recipient’s Email?</span>

            </div>
            {
                selectComponent()
            }


        </div>
    )
}
