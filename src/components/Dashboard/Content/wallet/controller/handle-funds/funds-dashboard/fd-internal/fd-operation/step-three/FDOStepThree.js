import React, { useState, useEffect } from 'react'
import nextId from 'react-id-generator'
import { conversionAPI } from '../../../../../../../../../../services/getAPIs';
import { Agency } from '../../../../../../../../../Context/Context'

export default function FDOStepThree() {
    const agency = React.useContext(Agency);
    const [fromValue, setFromValue] = useState('');
    const [rate, setRate] = React.useState(null)
    const [toValue, setToValue] = useState('');
    const [error, setError] = useState(false);
    const [message, setMessage] = useState("")
    const [selected, setSelected] = useState()
    const { fundsFrom, fundsTo, setThreeSteps, fundsSelectedApp, valueFormatter, setTransfer, setConversionRates, valueFormatterInput, conversionRates } = agency;


    const handleChange = (e, from) => {
        if (rate === null)
            return
        setError(false)
        if (from) {
            setFromValue(e.target.value)
            let val = valueFormatterInput(e.target.value * (1 / rate), fundsTo?.coin)
            setToValue(val)
        } else {
            setToValue(e.target.value)
            let val = valueFormatterInput(e.target.value * (rate), fundsFrom?.coin)
            setFromValue(val)
        }
    }
    const handleSubmit = () => {
        if (fromValue > fundsFrom.coinValue) {
            setError(true);
            return
        } else {
            setError(false)
        }
        let temp = {};
        temp = {
            from: { app_code: fundsFrom.app_code, coin: fundsFrom.coin, profile_id: fundsFrom.profile_id },
            to: { app_code: fundsTo.app_code, coin: fundsTo.coin, profile_id: fundsTo.profile_id },
            to_amount: toValue
        }
        setThreeSteps(1)
        setTransfer({ ...temp, transfer_for: message });
    }
    const handlePercentage = (obj) => {
        setError(false)
        setSelected(obj)
        let mul = obj.val * fundsFrom.coinValue
        setFromValue(valueFormatterInput(mul, fundsFrom?.coin));
        let ans = valueFormatterInput(mul * (1 / rate), fundsTo?.coin)
        setToValue(ans)
    }

    const setUpConversionRate = async () => {
        if (fundsFrom === null || fundsTo === null)
            return;
        let res = await conversionAPI(fundsFrom?.coin, fundsTo?.coin);
        setRate(res.data[`${fundsTo?.coin?.toLowerCase()}_${fundsFrom?.coin?.toLowerCase()}`])
    }

    useEffect(() => {
        if (fundsFrom?.coin === fundsTo?.coin) {
            setRate(1);
        }
        else {
            setUpConversionRate()
        }
        setToValue('');
        setFromValue('');

    }, [fundsTo, fundsFrom, fundsSelectedApp, conversionRates]);
    // useEffect(() => {
    //     if (fundsFrom?.coin === fundsTo?.coin) {
    //         setRate(1);
    //     } else if (fundsFrom?.type === "Crypto" && fundsTo?.type === "Fiat" || fundsFrom?.coin === "GXT" && fundsTo?.type === "Crypto") {
    //         setConversionRates(fundsTo?.coin)
    //     } else {
    //         setConversionRates(fundsFrom?.coin)
    //     }

    // }, [fundsTo, fundsFrom])
    return (
        <div className="fund-operation-step-three">
            <div className="wd-calculator-title">
                <h6>Choose Amount</h6>
                <span>Step 3</span>
            </div>
            <div className="wd-calculator-body">
                <div>{buttons.map(obj => <button className={obj.name === selected?.name ? "selected" : ''} onClick={() => handlePercentage(obj)} key={obj.keyId}>{obj.name}</button>)}</div>
                <div>
                    <h6><input placeholder={valueFormatter(0.0, fundsFrom?.coin)} value={fromValue} onChange={(e) => handleChange(e, true)} /> {!fundsFrom ? "TBD" : fundsFrom.coin}</h6>
                    <h6><input placeholder={valueFormatter(0.0, fundsTo?.coin)} value={toValue} onChange={(e) => handleChange(e, false)} /> {!fundsTo ? "TBD" : fundsTo.coin}</h6>
                </div>
                <div className="get-details">
                    <h6>Transferring For?</h6>
                    <input onChange={(e) => setMessage(e.target.value)} placeholder="Enter...." />
                </div>
            </div>

            <button disabled={!toValue || fundsSelectedApp === null} style={error ? { backgroundColor: '#de4b25' } : {}} onClick={() => handleSubmit()} className="submit-button">
                {error ? "Exceeded Amount" : "Send"}
            </button>

        </div>
    )
}

const buttons = [
    { keyId: nextId(), name: "All", val: 1 },
    { keyId: nextId(), name: "50.0%", val: 0.5 },
    { keyId: nextId(), name: "Custom %", val: 1 },
]