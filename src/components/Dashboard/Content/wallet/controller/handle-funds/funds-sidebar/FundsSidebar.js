import React from 'react'
import Images from '../../../../../../../assets/a-exporter';
import Constant from '../../../../../../../json/constant';
import { Agency } from '../../../../../../Context/Context';
import './funds-sidebar.style.scss'
export default function FundsSidebar({ hide, type, setType,setWhichOverlay, whichOverlay, }) {
    const [currentList, setCurrentList] = React.useState([]);
    const [depositClass, setDepositClass] = React.useState(false)
    const agency = React.useContext(Agency);
    const { fundsConfig, resetMI, assetClassSelected,  setFundsStepOneConfig, setFundsFrom, setFundsTo } = agency;

    const handleClick = (obj) => {
        resetMI()
        if (obj.keyId === type.keyId || type.keyId) {
            setType({ keyId: '' })
            setFundsFrom(null);
            setFundsTo(null);
            setFundsStepOneConfig({
                deposit: null,
                internal: null,
                both: null
            })
        } else {
            setType(obj);
            setFundsFrom(null);
            setFundsTo(null);
            setFundsStepOneConfig({
                type: obj.short,
                internal: obj.internal,
                both: obj.both,
                step: obj.step
            })
        }
    }

    React.useEffect(() => {
        if (fundsConfig.add) {
            let tempArray = Constant.depositSection;
            if (!fundsConfig.crypto) {
                tempArray[0] = { ...tempArray[0], hide: true, image: Images.instaCrypto, text: "Via An InstaCrypto Banker" }
            } else if (!assetClassSelected.native_deposit) {
                tempArray[0] = { ...tempArray[0], hide: true }
                tempArray = [...tempArray]
            } else {
                tempArray[0] = { ...tempArray[0], hide: false, text: `A ${assetClassSelected.coinName} Address` }
            }

            setCurrentList(tempArray)
        } else {
            let tempArray = Constant.withdrawSection;
            if (!fundsConfig.crypto) {
                tempArray[0] = { ...tempArray[0], hide: false, image: Images.instaCrypto, text: "Via An InstaCrypto Banker" }
            } else {
                tempArray[0] = { ...tempArray[0], hide: false, text: `A ${assetClassSelected.coinSymbol} Address Via BLOCKCHECK` }
            }
            setCurrentList(tempArray)
        }
        setDepositClass(fundsConfig.add)
    }, [fundsConfig.action, assetClassSelected])

    return (
        <div className="funds-sidebar">
            <div className={`funds-sidebar-body ${depositClass ? "deposit" : ""}`}>
                {
                    currentList.map((obj, i) =>
                        <div
                            onMouseEnter={() => setWhichOverlay(obj)}
                            onMouseLeave={() => setWhichOverlay(null)}
                            className={obj.hide ? "hide-this" : type.keyId === obj.keyId ? "selected-side" : ""}
                            onClick={() => obj.hide ? console.log() : handleClick(obj)}
                        >
                            <img
                                style={!fundsConfig.add && i === 2 ? { transform: `scale(1.2)` } : {}}
                                className={hide ? "small-image" : ""}
                                src={hide ? obj.newImageSmall : obj.newImage} />
                        </div>
                    )
                }

            </div>
            {/* <div className={`funds-footer ${depositClass ? "deposit-footer" : ""}`}>
                <span>A Prospect</span>

            </div> */}

        </div>
    )
}
