import React from 'react';
import './result-page.style.scss';
export default function ResultPage({ success, clearAll,message }) {
    return (
        <div className="w-100 h-100 d-flex justify-content-center align-items-center flex-column">
            <p>{message}</p>
            <button onClick={()=>clearAll()} className={`border-0 text-light mt-3 ${!success ? "bg-danger" : "bg-success"}`}>Return</button>
        </div>
    )
}
