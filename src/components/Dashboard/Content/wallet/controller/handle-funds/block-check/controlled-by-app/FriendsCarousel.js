import React from 'react'
import Images from '../../../../../../../../assets/a-exporter';
import { getStreamlinedFriendList } from '../../../../../../../../services/getAPIs';
import { Agency } from '../../../../../../../Context/Context'
import './friend-carousel.style.scss'
export default function FriendsCarousel() {
    const agency = React.useContext(Agency);
    const { currentApp } = agency;

    const [friendList, setFriendList] = React.useState([]);

    const fetchStreamlinedDestination = async () => {
        let res = await getStreamlinedFriendList(currentApp.app_code);
        res = res.data.destinations;
        setFriendList(res);
    }

    React.useEffect(() => {
        fetchStreamlinedDestination()
    })
    return (
        <div className="friend-carousel">
            <div className="friend-carousel-header">
                <img src={Images.blockCheck} />
                <span>Instant Checks</span>

            </div>
            <div className="friend-carousel-body">
                {
                    [1, 2, 3, 5, 6].map(oj =><div>

                    </div>
                    )

                }

            </div>

        </div>
    )
}
