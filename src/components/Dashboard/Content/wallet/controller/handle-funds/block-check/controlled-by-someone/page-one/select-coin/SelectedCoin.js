import React from 'react'
import Images from '../../../../../../../../../../assets/a-exporter';
import { Agency } from '../../../../../../../../../Context/Context'
export default function SelectedCoin({ setCStep, setMethod }) {
    const agency = React.useContext(Agency);
    const { currentApp, fundsConfig, valueFormatter } = agency;
    return (
        <div className="selected-coin">
            <div className="selected-coin-above">
                <div className="selected-coin-title-input">
                    <div className="sc-title">
                        <h2>{currentApp.app_code}<img src={currentApp.app_icon} /></h2>
                        <h6>Withdrawing From</h6>
                    </div>
                </div>
                <div className="sc-box">
                    <span><img src={fundsConfig.coin.coinImage} />{fundsConfig.coin.coinName}</span>
                    <span>{valueFormatter(fundsConfig.coin.coinValue, fundsConfig.coin.coinSymbol)}</span>
                </div>
                <p onClick={() => setCStep(1)}>I Want To Change The Asset</p>
            </div>
            <div className="selected-coin-below">
                <button onClick={() => setMethod(13)}>Proceed</button>
            </div>
        </div>
    )
}
