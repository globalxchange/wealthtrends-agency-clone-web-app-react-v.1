import React from 'react'
import PageOne from './page-one/PageOne';
import './controlled-by-someone.style.scss'
import Constant from '../../../../../../../../json/constant';
import { Agency } from '../../../../../../../Context/Context';
import LoadingAnimation from '../../../../../../../../lotties/LoadingAnimation';
import ResultPage from './result-page/ResultPage';
import { blockcheckRequest } from '../../../../../../../../services/postAPIs';
export default function ControlledBySomeone() {
    const [address, setAddress] = React.useState('');
    const [details, setDetails] = React.useState({question: '',answer: '', confirm: '',message: '', mobile: '' });
    const [method, setMethod] = React.useState(0);
    const [currentCode, setCurrentCode] = React.useState(Constant.countryCodes[0]);
    const [userCurrentCode, setUserCurrentCode] = React.useState(Constant.countryCodes[0])
    const [cryptoValue, setCryptoValue] = React.useState('');
    const [usdValue, setUsdValue] = React.useState('');
    const [currentState, setCurrentState] = React.useState(0);
    const [success, setSuccess] = React.useState(false);
    const [message, setMessage] = React.useState('');
    const [emailAddress, setEmailAddress] = React.useState('')
    const [knowsAddress, setKnowsAddress] = React.useState(false)

    const agency = React.useContext(Agency);
    const { currentApp,setMainBalance, fundsConfig, userAccountDetails } = agency;

    const clearAll = () => {
        setAddress('');
        setDetails({});
        setMethod(0);
        setCurrentCode(Constant.countryCodes[0]);
        setCryptoValue('');
        setCurrentState(0);
        setMessage(''); setSuccess(false)
    }

    const handleSubmit = async () => {
        let token = localStorage.getItem("idToken");
        setCurrentState(1);
        let obj = {
            bcr_type: "sender_escrow",
            initiator_name: userAccountDetails.fullName,
            initiator_profileId: currentApp.profile_id,
            initiator_appCode: currentApp.app_code,
            initiator_email: localStorage.getItem("userEmail").toString(),
            token: token,
            coin: fundsConfig.coin.coinSymbol,
            amount: cryptoValue,
            crypto_address: address,
            endUser_name: details.recipient,
            endUser_phone: `${currentCode.dial_code}${details.mobile}`,
            security_question: details.question,
            security_answer: details.answer,
            endUser_email: emailAddress,
            initiator_phone: `${userCurrentCode.dial_code}${details.userMobile}`,
            custom_message: details.message

        }
        console.log("sending data", obj)
        let res;
        try {
            res = await blockcheckRequest(obj);
            if (res?.data?.status) {
                setMainBalance();
                setSuccess(true)
                setMessage(res?.data?.message);
                setCurrentState(2);
            } else {
                setSuccess(true)
                setMessage(res?.data?.message)
                setCurrentState(2);
    
            }
            
        } catch (error) {
            setSuccess(true)
            setMessage("Please, Try Again...")
            setCurrentState(2);
            
        }

    }
    React.useEffect(()=>{
        console.log("details", details)
    },[details])
    const selectPage = () => {
        switch (currentState) {
            case 0: return <PageOne
                address={address}
                method={method}
                userCurrentCode={userCurrentCode}
                setUserCurrentCode={setUserCurrentCode}
                setAddress={setAddress}
                details={details}
                knowsAddress={knowsAddress}
                usdValue={usdValue}
                setDetails={setDetails}
                setUsdValue={setUsdValue}
                setMethod={setMethod}
                currentCode={currentCode}
                setCurrentCode={setCurrentCode}
                cryptoValue={cryptoValue}
                setCryptoValue={setCryptoValue}
                handleSubmit={handleSubmit}
                emailAddress={emailAddress}
                setEmailAddress={setEmailAddress}
                setKnowsAddress={setKnowsAddress}

            />;
            case 1: return <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation type="login" size={{ height: 200, width: 200 }} />
            </div>;
            case 2: return <ResultPage message={message} success={success} clearAll={clearAll} />;
        }
    }

    return (
        <div className="controlled-by-someone">
            {selectPage()}

        </div>
    )
}
