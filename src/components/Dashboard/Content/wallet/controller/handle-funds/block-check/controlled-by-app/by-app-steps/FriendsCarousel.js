import React from 'react'
import Images from '../../../../../../../../../assets/a-exporter';
import LoadingAnimation from '../../../../../../../../../lotties/LoadingAnimation';
import { fetchAllBankers, getStreamlinedFriendList } from '../../../../../../../../../services/getAPIs';
import { Agency } from '../../../../../../../../Context/Context'
import './friend-carousel.style.scss'
export default function FriendsCarousel({ setStep, setSelectedUser }) {
    const agency = React.useContext(Agency);
    const [styles, setStyles] = React.useState([])
    const { currentApp } = agency;
    const [friendList, setFriendList] = React.useState([]);
    const [originalList, setOriginalList] = React.useState([]);
    const [loading, setLoading] = React.useState(true)


    const fetchStreamlinedDestination = async () => {
        let res = await fetchAllBankers();
        res = res.data.data;
        if (!res.length) {
            setStep(1);
            return;
        }
        if (res.length > 3) {
            setOriginalList(res);
            res = res.slice(0, 3)
            setFriendList(res)
        } else {
            setOriginalList(res);
            setFriendList(res)
        }
        setFriendList(res)
        switch (res.length) {
            case 0: setStyles([
            ]); break;
            case 1: setStyles([
                { opacity: 1, scaleBy: 1, z: 1 }
            ]); break;
            case 2: setStyles([
                { opacity: 1, scaleBy: 1, z: 0 },
                { opacity: 1, scaleBy: 1, z: 1 }
            ]); break;
            case 3: setStyles([
                { opacity: 0.5, scaleBy: 0.95, z: 1 },
                { opacity: 1, scaleBy: 1.25, z: 2 },
                { opacity: 0.5, scaleBy: 0.95, z: 1 },
            ]);

                break;
            case 4: setStyles([
                { opacity: 0.5, scaleBy: 1 },
                { opacity: 1, scaleBy: 1.25 },
                { opacity: 1, scaleBy: 1.25 },
                { opacity: 0.75, scaleBy: 1 },
            ])
                break;

            default: setStyles([
                { opacity: 0.5, scaleBy: 0.75 },
                { opacity: 0.75, scaleBy: 1 },
                { opacity: 1, scaleBy: 1.25 },
                { opacity: 0.75, scaleBy: 1 },
                { opacity: 0.5, scaleBy: 0.75 },
            ])
                break;
        }
        setLoading(false)
    }
    const updateList = (num) => {
        if (originalList.length > 3) {
            switch (num) {
                // case 0:
                //     let temp0 = originalList;
                //     let a0 = originalList.pop();
                //     let b0 = originalList.pop();
                //     originalList.unshift(a0);
                //     originalList.unshift(b0);
                //     temp0 = originalList.slice(0, 3);
                //     setFriendList(temp0)
                //     break;
                case 0:
                    let temp1 = originalList;
                    let a1 = originalList.pop();
                    originalList.unshift(a1);
                    temp1 = originalList.slice(0, 3);
                    setFriendList(temp1)
                    break;
                case 2:
                    let temp3 = originalList;
                    let a3 = originalList.shift();
                    originalList.push(a3);
                    temp3 = originalList.slice(0, 3);
                    setFriendList(temp3)
                    break;
                // case 4:
                //     let temp4 = originalList;
                //     let a4 = originalList.shift();
                //     let b4 = originalList.shift();
                //     originalList.push(a4);
                //     originalList.push(b4);
                //     temp4 = originalList.slice(0, 3);
                //     setFriendList(temp4)
                //     break;

                default:
                    break;
            }
        } else {
            switch (originalList.length) {
                case 2:
                case 3:
                    if (num === 1) {
                        return
                    }
                    let temp1 = friendList
                    let sw1 = temp1[1];
                    temp1[1] = temp1[num];
                    temp1[num] = sw1;
                    setFriendList([...temp1])
                    break;
                // case 4:
                // case 5:
                //     if (num === 2) {
                //         return
                //     }

                //     let temp4 = friendList;
                //     console.log("first", temp4)
                //     let sw4 = temp4[2];
                //     temp4[2] = temp4[num];
                //     temp4[num] = sw4;
                //     console.log("first 2", temp4)
                //     setFriendList([...temp4]);
                //     break;


                default:
                    break;
            }

        }

    }

    React.useEffect(() => {
        fetchStreamlinedDestination()
    }, [])
    return (
        <div className="friend-carousel">
            <div className="friend-carousel-header">
                <h3>Verified Destinations By</h3>
                <h6>By {currentApp.app_code.toString().toUpperCase()}</h6>

            </div>
            <div className="friend-carousel-body">
                {
                    loading ?
                        <div>
                            <LoadingAnimation type="login" size={{ height: 200, width: 200 }} />
                        </div>
                        :
                        friendList.map((obj, i) =>
                            <div
                                onClick={() => updateList(i)}
                                style=
                                {{
                                    backgroundImage: `url(${obj?.profilePicURL})`,
                                    opacity: styles[i]?.opacity,
                                    transform: `scale(${styles[i]?.scaleBy})`,
                                    margin: friendList.length === 2 ? "0 10px" : "0",
                                    zIndex: styles[i]?.z
                                }}
                            >
                                <h6>
                                    {obj.bankerTag}
                                    <span>{obj.country}</span>
                                </h6>
                            </div>
                        )

                }

            </div>

        </div>
    )
}
