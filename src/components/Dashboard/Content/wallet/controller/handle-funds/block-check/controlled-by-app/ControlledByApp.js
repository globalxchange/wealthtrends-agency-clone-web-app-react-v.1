import React from 'react'
import './controlled-by-app.style.scss'
import FriendsCarousel from './by-app-steps/FriendsCarousel'
import ByAppsCheckbook from './by-app-steps/ByAppsCheckbook';
import ByAppsForm from './by-app-steps/ByAppsForm';
import ByAppsCoinList from './by-app-steps/ByAppsCoinList';
import LoadingAnimation from '../../../../../../../../lotties/LoadingAnimation';
import { addContact, getMainBalance, blockcheckRequest } from '../../../../../../../../services/postAPIs';
import { Agency } from '../../../../../../../Context/Context';
import ByAppDashboard from './ByAppDashboard';
export default function ControlledByApp() {
    const agency = React.useContext(Agency);
    const { changeNotification,setMainBalance, currentApp, mainBalance, userAccountDetails, currencyImageList } = agency;
    const [cryptoValue, setCryptoValue] = React.useState('');
    const [usdValue, setUSDValue] = React.useState('');
    const [step, setStep] = React.useState(0);
    const [selectedUser, setSelectedUser] = React.useState(null);
    const [mainStep, setMainStep] = React.useState(0);
    const [list, setList] = React.useState([]);
    const [refresh, setRefresh] = React.useState(false);
    const [details, setDetails] = React.useState({
        recipient: '',
        email: '',
        address: '',
        number: '',
        coin: ''
    })
    const resetAll = () => {
        setStep(0);
        setDetails({ recipient: '', email: '', address: '', number: '', coin: '' });
        setMainStep(0);
        setSelectedUser(null);
        setCryptoValue('')
        setUSDValue('')
    }
    const selectComponent = () => {
        switch (step) {
            case 0: return <FriendsCarousel setMainStep={setMainStep} setSelectedUser={setSelectedUser} setStep={setStep} />;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5: return <ByAppsForm
                setDetails={setDetails}
                method={step}
                setStep={setStep}
                details={details}
                handleAddContact={handleAddContact}
            />;
            case 6: return <ByAppsCoinList list={list} setList={setList} details={details} setDetails={setDetails} setStep={setStep} />;
            case 7: return <div className="h-100 w-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation type="login" size={{ height: 200, width: 200 }} />
            </div>

        }
    }
    const handleAddContact = async () => {
        let email = localStorage.getItem('userEmail');
        setStep(7)
        let temp = {
            initiator_email: email,
            endUser_email: details.email,
            endUser_name: details.recipient,
            endUser_phone: details.number,
            coin: details.coin.coinSymbol,
            destination_data: details.address
        }
        let res = await addContact(temp);
        if (res.data.status) {
            resetAll();
            changeNotification({ status: true, message: "Contact Added Successfully" });
            setRefresh(!refresh);

        } else {
            resetAll();
            changeNotification({ status: false, message: "Failed To Add Contact" })
        }
    }
    const selectMainComponent = () => {
        switch (mainStep) {
            case 0: return <><div className="by-app-left">
                <ByAppsCheckbook coinList={list} setMainStep={setMainStep} setStep={setStep} setSelectedUser={setSelectedUser} refresh={refresh} />
            </div>
                <div className="by-app-right">
                    {
                        selectComponent()
                    }
                </div>
            </>;
            case 1: return <ByAppDashboard
            setSelectedUser={setSelectedUser}
                handleSubmit={handleSubmit}
                currencyImageList={currencyImageList}
                selectedUser={selectedUser}
                cryptoValue={cryptoValue}
                usdValue={usdValue}
                setCryptoValue={setCryptoValue}
                setUSDValue={setUSDValue}
            />;
            case 2: return <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation type="login" size={{ height: 200, width: 200 }} />
            </div>
            default:
                break;
        }
    }
    const assignCoin = () => {

    }
    const fetchAndSetList = async () => {
        let arr = [ ...mainBalance.crypto]
        setList([...arr])
    }
    const handleSubmit = async() => {
        setMainStep(2);
        let token = localStorage.getItem("idToken");
        console.log("before", selectedUser)
        let obj = {
            bcr_type: "sender_escrow",
            initiator_name: userAccountDetails.fullName,
            initiator_profileId: currentApp.profile_id,
            initiator_appCode: currentApp.app_code,
            initiator_email: localStorage.getItem("userEmail").toString(),
            token: token,
            coin: selectedUser.coin.coinSymbol,
            amount: cryptoValue,
            crypto_address: selectedUser.destination_data,
            endUser_name: selectedUser.name,
            endUser_phone: selectedUser.phone,
            security_question: selectedUser.question,
            security_answer: selectedUser.answer,
            endUser_email: selectedUser.email,
            initiator_phone: ``,
            custom_message: ''

        }
        console.log(obj)
        let res;
        try {
            res = await blockcheckRequest(obj);
            if (res?.data?.status) {
                setMainBalance();
                changeNotification({status: true, message: "Transferred Successfully"})
                resetAll()
            } else {
                changeNotification({status: false, message: "Transfer Failed"})
                resetAll()
    
            }
            
        } catch (error) {
            changeNotification({status: false, message: "Transfer Failed"});
            resetAll()
            
        }

    }

    React.useEffect(() => {
        fetchAndSetList();
    }, [])
    React.useEffect(() => {
        if (!selectedUser)
            return;
        console.log("selected", selectedUser)
        setMainStep(1)
    }, [selectedUser])
    return (
        <div className="controlled-by-app">
            {
                selectMainComponent()
            }
        </div>
    )
}
