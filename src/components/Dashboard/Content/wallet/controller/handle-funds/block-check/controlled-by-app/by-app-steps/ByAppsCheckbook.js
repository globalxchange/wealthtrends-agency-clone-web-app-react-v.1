import React from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../../../../../../assets/a-exporter'
import LoadingAnimation from '../../../../../../../../../lotties/LoadingAnimation';
import { getCheckbookList } from '../../../../../../../../../services/getAPIs';
import { Agency } from '../../../../../../../../Context/Context';
import './by-app-checkbook.style.scss'
export default function ByAppsCheckbook({ setMainStep,refresh, coinList, setStep, setSelectedUser }) {
    const [search, setSearch] = React.useState({
        type: "name",
        term: "",
        show: "Name"
    });
    const [loading, setLoading] = React.useState(true)
    const handleClick = (obj) => {
        let coinTemp = coinList.find(x => { return x.coinSymbol === obj.coin });
        let tempObj = {
            ...obj, coin: coinTemp
        }
        setSelectedUser({ ...tempObj });
    }

    const [list, setList] = React.useState([]);
    const agency = React.useContext(Agency);
    const { currencyImageList } = agency;

    const fetchAndSet = async () => {
        let res = await getCheckbookList();
        let newArr = res.data.data.filter(x =>{return x.name !== "viraj"});
        setLoading(false)
        setList(newArr);
    }
    const filterList = () => {
        let temp = list.filter(obj => { return obj?.[search.type]?.toString()?.toLowerCase()?.startsWith(search?.term?.toString()?.toLowerCase()) })
        return temp;
    }
    React.useEffect(() => {
        fetchAndSet();
    }, [refresh])
    return (
        <div className="by-apps-checkbook">
            <div className="by-app-cb-header">
                <div>
                    <img src={Images.blockCheck} />
                    <h6>My Checkbook</h6>
                </div>
                <button onClick={() => setStep(1)}>
                    <img src={Images.addDark} /><span>Add New</span>
                </button>

            </div>
            <div className="by-app-cb-body">
                <div className="bh-app-cb-search-bar">
                    <span><input onChange={(e) => setSearch({ ...search, term: e.target.value })} placeholder={`Search By ${search.show}`} /></span>
                    <span>
                        {
                            filters.map(obj =>
                                <button className={obj.id === search.type ? "selected-type" : ""} onClick={() => setSearch({ ...search, show: obj.text, type: obj.id })}
                                    key={obj.keyId}
                                >{obj.text}
                                </button>
                            )
                        }
                    </span>

                </div>
                <div className="bh-app-cb-list">
                    {
                        loading?
                        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                            <LoadingAnimation type="login" size={{height: 200, width: 200}} />
                        </div>
                        :
                        filterList().map(obj =>
                            <div onClick={() => handleClick(obj)}
                                className="by-app-cb-row">
                                <div><h6>{obj.name}</h6></div>
                                <div><span><img src={currencyImageList[obj.coin]} />{obj.coin}</span></div>
                                <div>
                                    <span>
                                        <img src={obj.phone ? Images.textIcon : Images.mailIcon} />
                                        via {obj.phone ? "Text" : "Email"}
                                    </span>
                                </div>

                            </div>
                        )
                    }

                </div>

            </div>

        </div>
    )
}
const filters = [
    { keyId: nextId(), text: "Name", id: "name" },
    { keyId: nextId(), text: "All Coins", id: "coin" },
    { keyId: nextId(), text: "All Apps", id: "app" },
]