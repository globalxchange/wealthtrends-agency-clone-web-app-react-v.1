import React from 'react'
import Images from '../../../../../../../../assets/a-exporter'
import Constant from '../../../../../../../../json/constant'
import { Agency } from '../../../../../../../Context/Context'

export default function BCStepOne({ setMainStep }) {
    const agency = React.useContext(Agency);
    const { currentApp } = agency;
    const handleClick = (obj) => {
        switch (obj.id) {
            case "you": setMainStep(1); break;
            case "other": setMainStep(2); break;
            case "app":setMainStep(3); break;
                // setMainStep(3); 
                // break;
            default: break;
        }
    }
    return (
        <div className="bc-step-one">
            <div className="bc-image-wrapper">
                <img src={Images.blockCheck} />
            </div>
            <p className="bc-text-wrapper">
                Are You Sending Bitcoins To An External Wallet Address Which:
            </p>
            <div className="bc-type-list">
                {
                    Constant.blockCheckList.map((obj, i) =>
                        <div onClick={() => handleClick(obj)}>
                            <img src={obj.image} />
                            <span style ={{color: `${obj?.color}`}}>{obj.text}{i === 2 ? currentApp.app_name : ""}</span>
                        </div>
                    )
                }
            </div>
        </div>
    )
}
