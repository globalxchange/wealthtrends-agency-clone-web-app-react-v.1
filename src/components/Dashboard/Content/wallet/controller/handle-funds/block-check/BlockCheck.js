import React from 'react'
import BCStepOne from './bc-step-one/BCStepOne'
import './block-check.style.scss'
import ControlledByApp from './controlled-by-app/ControlledByApp'
import ControlledByMe from './controlled-by-me/ControlledByMe'
import ControlledBySomeone from './controlled-by-someone/ControlledBySomeone'
import PageOne from './controlled-by-someone/page-one/PageOne'
export default function BlockCheck() {
    const [mainStep, setMainStep] = React.useState(0);
    const selectMainComponent = () => {
        switch (mainStep) {
            case 0: return <BCStepOne setMainStep={setMainStep} />;
            case 1: return <ControlledByMe />;
            case 2: return <ControlledBySomeone />;
            case 3: return <ControlledByApp />;
        }
    }
    return (
        <div className="block-check-main">
            {selectMainComponent()}
        </div>
    )
}
