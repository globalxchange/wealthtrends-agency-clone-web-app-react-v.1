import React from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../../../../../../assets/a-exporter'
import Constant from '../../../../../../../../../json/constant';
import './by-apps-form.style.scss'
export default function ByAppsForm({ method, setDetails, handleAddContact, details, setStep }) {
    const phoneRegex = /^(\d){9,12}$/;
    const emailRegex = /([\w\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?/gm;
    const [dropdown, setDropdown] = React.useState(false)
    const [code, setCode] = React.useState(Constant.countryCodes[0]);

    const selectByMethod = () => {
        switch (method) {
            case 1: return <div className="input-wrapper">
                <input onKeyPress={handleKeyPress} onChange={(e) => setDetails({ ...details, [e.target.name]: e.target.value })} value={details?.recipient} name="recipient" placeholder="Name of Recipient" />
            </div>
            case 2: return <>

                <div className="options-row">
                    {
                        methodList.map(obj => <div className="option-single-wrapper">
                            <div onClick={() => handleOptions(obj)}>
                                <img src={obj.image} />
                            </div>
                            <p>
                                {obj.text}
                            </p>

                        </div>
                        )
                    }
                </div>
            </>
            case 3: return <div className="input-wrapper">
                <img src={Images.mailIcon} />
                <input onKeyPress={handleKeyPress} onChange={(e) => setDetails({ ...details, [e.target.name]: e.target.value })} value={details?.email} name="email" placeholder={`Enter ${details.recipient}'s Email`} /></div>;
            case 4: return <div className="input-wrapper">
                <span onClick={() => setDropdown(!dropdown)}>
                    {code.dial_code}
                </span>
                <input onKeyPress={handleKeyPress} onChange={(e) => setDetails({ ...details, [e.target.name]: e.target.value })} value={details?.number} name="number" placeholder={`Enter ${details.recipient}'s Number`} />
                <div className={dropdown ? "dropdown-code" : "d-none"}>
                    {
                        Constant.countryCodes.map(obj =>
                            <h6 onClick={() => { setCode(obj); setDropdown(false) }}>
                                <span>{obj.name}</span><span>{obj.dial_code}</span>
                            </h6>
                        )
                    }

                </div>
            </div>;
            case 5: return <div className="input-wrapper">
                <input onKeyPress={handleKeyPress} onChange={(e) => setDetails({ ...details, [e.target.name]: e.target.value })} value={details?.address} name="address" placeholder={`Enter ${details.coin.coinSymbol} Address`} /><img onClick={() => getClipboardText()} src={Images.pasteIcon} /></div>;
        }
    }
    const getClipboardText = () => {
        navigator.clipboard.readText()
            .then(
                text => setDetails({ ...details, address: text })
            )
    }
    const handleProceed = () => {
        switch (method) {
            case 1: setStep(2); break;
            case 2: break;
            case 3: setStep(6); break;
            case 4: setDetails({ ...details, number:`${code.dial_code}`+ details.number });setStep(6); break;
            case 5: handleAddContact(); break;
        }
    }
    const handleOptions = (obj) => {
        switch (obj.id) {
            case "email":
                setStep(3); break;
            case "text":
                setStep(4); break;
            default: break;
        }
    }
    const checkDisabled = () => {
        switch (method) {
            case 1: return !details.recipient ? true : false;
            case 2: break;
            case 3: return !emailRegex.test(details.email) ? true : false;
            case 4: return !phoneRegex.test(details.number) ? true : false;
            case 5: return !details.address ? true : false;
        }
    }
    const handleKeyPress = (e) => {
        switch (method) {
            case 1: e.which === 13 && details.recipient ? setStep(2) : console.log(); break;
            case 2: break;
            case 3: e.which === 13 && emailRegex.test(details.email) ? setStep(6) : console.log(); break;
            case 4: if (e.which < 46 || e.which > 57) { e.preventDefault(); if(e.which === 13 && phoneRegex.test(details.number)){  setDetails({ ...details, number:`${code.dial_code}`+ details.number });setStep(6)  }}; break;
            case 5: e.which === 13 && details.address ? handleAddContact() : console.log(); break;

        }
    }
    return (
        <div className="by-apps-form-child">
            <div className="by-app-form-title">
                <img src={Images.blockCheck} />
                <h6>New Contact</h6>
            </div>
            <h6 className={method !== 2 ? "d-none" : "middle-text"}>Select One Of The Following Methods To Be The Default Delivery Mechanism</h6>
            <div className="by-apps-form-method">
                {
                    selectByMethod()
                }
            </div>
            <button disabled={checkDisabled()} onClick={() => handleProceed()} className={method === 2 ? "d-none" : ""}>
                Proceed
            </button>

        </div>
    )
}

const methodList = [
    { keyId: nextId(), image: Images.mailIcon, text: "Email", id: "email" },
    { keyId: nextId(), image: Images.textIcon, text: "Text", id: "text" },
    { keyId: nextId(), image: Images.friendsIcon, text: "Friends", id: "friends" },
]
