import React from 'react'
import Images from '../../../../../../../../../assets/a-exporter'
import './cbm-receipt.style.scss'
export default function CBMReceipt({ forList,id, date, coinImage, usdImage, coin, notes, cryptoValue, address, usdValue, payee }) {
    return (
        <div className={`cbm-receipt-main ${forList ? "for-the-list" : ""}`}>
            <div className="cmb-receipt-card">
                <div className="cmb-card-header">
                    <img src={Images.blockCheck} />
                    <h6><span>Date:</span><span>{date}</span></h6>
                </div>
                <div className="cmb-amount">
                    <div className="cmb-amount-coin">
                        <h6>
                            <span>Pay To The Order Of </span>
                            <span>{payee}</span>
                        </h6>
                        <h6>
                            <span>
                                {cryptoValue}
                            </span>
                            <span>
                                <img src={coinImage} />
                                {coin}
                            </span>
                        </h6>
                    </div>
                    <div className="cmb-amount-usd">
                        <h6>
                            <span>
                                {usdValue}
                            </span>
                        </h6>
                        <h6>
                            <img src={usdImage} /> US Dollars
                        </h6>

                    </div>

                </div>
                <div className="cmb-notes-wrapper">
                    <h6>
                        <span>Notes:</span>
                        <span>
                            {notes}
                        </span>
                    </h6>
                    <h6>
                        <span>To:</span>
                        <span>
                            {address}
                        </span>
                    </h6>

                </div>
            </div>
    <p className={forList?"":"d-none"}>{id}</p>

        </div>
    )
}
