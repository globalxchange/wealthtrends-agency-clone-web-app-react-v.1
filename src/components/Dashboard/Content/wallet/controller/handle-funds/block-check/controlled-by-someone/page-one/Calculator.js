import React from 'react'
import { Agency } from '../../../../../../../../Context/Context'

export default function Calculator({cryptoValue,setMethod, usdValue, handleValues,handleSubmit}) {
    const agency = React.useContext(Agency);
    const {valueFormatter,fundsConfig } = agency;
    return (
        <div className="cbse-page-right">
                <div className="title-wrapper">
                    <h1>{valueFormatter(fundsConfig.coin.coinValue, fundsConfig.coin.coinSymbol)}</h1>
                    <h6>{fundsConfig.coin.coinName} Balance</h6>
                </div>
            
            <div className="input-wrapper">
                    <div>
                        <input
                            value={cryptoValue}
                            onKeyPress={agency.onlyNumbers}
                            onChange={(e) => handleValues(true, e.target.value)}
                            placeholder={`${valueFormatter(0, fundsConfig.coin.coinSymbol)}`}
                        />
                        <span>{fundsConfig.coin.coinSymbol}</span>
                    </div>
                    <div>
                        <input
                            value={usdValue}
                            onKeyPress={agency.onlyNumbers}
                            onChange={(e) => handleValues(false, e.target.value)}
                            placeholder="$0.00"
                        />
                        <span>USD</span>
                    </div>
                </div>
            <button disabled={!cryptoValue || !usdValue} onClick={() => setMethod(7)}>
                Proceed
                </button>
        </div>
    )
}
