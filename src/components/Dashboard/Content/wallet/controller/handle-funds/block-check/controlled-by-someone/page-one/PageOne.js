import React from 'react'
import nextId from 'react-id-generator';
import Images from '../../../../../../../../../assets/a-exporter';
import Constant from '../../../../../../../../../json/constant';
import { conversionAPI } from '../../../../../../../../../services/getAPIs';
import { Agency } from '../../../../../../../../Context/Context';
import CBMReceipt from '../../controlled-by-me/receipt/CBMReceipt';
import Calculator from './Calculator';
import SelectCoin from './select-coin/SelectCoin';

export default function PageOne({ address, setEmailAddress, setUserCurrentCode, userCurrentCode, knowsAddress, setKnowsAddress, method, usdValue, setUsdValue, setMethod, setAddress, handleSubmit, details, setDetails, currentCode, setCurrentCode, cryptoValue, setCryptoValue }) {

    const [valid, setValid] = React.useState(false);
    const [emailValid, setEmailValid] = React.useState(false)
    const [dropDown, setDropDown] = React.useState(false);
    const [dropDownUser, setDropDownUser] = React.useState(false);
    const [searchTerm, setSearchTerm] = React.useState('');
    const [searchTermUser, setSearchTermUser] = React.useState('');
    const [emailOrPhone, setEmailOrPhone] = React.useState(false);

    const phoneRegex = /^(\d){9,12}$/;
    const emailRegex = /([\w\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?/gm
    const [rate, setRate] = React.useState(null);
    const agency = React.useContext(Agency);
    const { fundsConfig, valueFormatter, valueFormatterInput, currencyImageList } = agency;

    const selectMethod = () => {
        switch (method) {
            case 0: return <>
                <p className="para-wrapper">Select On Of The Following Methods To Create A Blockcheck™</p>
                <div className="method-wrapper">
                    {
                        methodList.map(obj => <div className="image-text-wrapper"
                            onClick={() => handleMethod(obj.id)}
                        >
                            <div>
                                <img src={obj.image} />
                            </div>
                            <span>{obj.text}</span>
                        </div>)
                    }
                </div>
            </>;
            case 1: return <div className="recipient-wrapper">
                <input
                    autoFocus={true}
                    onKeyPress={(e) => e.which === 13 ? emailOrPhone ? setMethod(6) : setMethod(2) : console.log()}
                    name="recipient"
                    onChange={(e) => setDetails({ ...details, [e.target.name]: e.target.value })}
                    placeholder="Name of Recipient..." />
            </div>;
            case 2: return <div className={`phone-number-recipient ${valid ? "valid-number" : ""}`}>
                <span className="span-left" onClick={() => setDropDown(!dropDown)}>{currentCode.dial_code}</span>
                <span className="span-right
                ">
                    <input
                        autocomplete="nope"
                        autoFocus={true}
                        onChange={e => { phoneRegex.test(e.target.value) ? setValid(true) : setValid(false); setDetails({ ...details, [e.target.name]: e.target.value }) }}
                        onKeyPress={(e) => handleKeyPress(e)}
                        name="mobile"
                        placeholder="Enter Phone Number" />
                </span>

                <div className={dropDown ? "country-code-dropdown" : "d-none"}>
                    <div className="dropdown-header">
                        <input
                            autocomplete="nope"
                            onChange={(e) => setSearchTerm(e.target.value)}
                            autoFocus={true}
                            name="strange"
                            placeholder="Search Your Country....."
                        />
                    </div>
                    <div className="dropdown-body">
                        {
                            Constant.countryCodes.filter(obj => { return obj.name.toString().toLowerCase().startsWith(searchTerm.toString().toLowerCase()) })
                                .map(obj =>
                                    <h6 onClick={() => { setCurrentCode(obj); setDropDown(false) }}>
                                        <span>{obj.name}</span><span>{obj.dial_code}</span>
                                    </h6>
                                )
                        }
                    </div>
                </div>

            </div>;
            case 3: return <div className="question-wrapper">
                <h6>Do You Have The Recipients Wallet Address?</h6>
                <h6>
                    <button onClick={() => { setKnowsAddress(false); setMethod(7) }}>No</button>
                    <button onClick={() => { setKnowsAddress(true); setMethod(7) }}>Yes</button>
                </h6>
            </div>
            case 4:
            case 5:
                return <div className="address-receiver">
                    <div>
                        <input
                            autocomplete="nope"
                            autoFocus={true}
                            value={address}
                            onChange={e => setAddress(e.target.value)}
                            placeholder="Ethereum Address....."
                        />
                        <img onClick={() => getClipboardText()} src={Images.pasteIcon} /></div>

                </div>
            case 6: return <div className="email-wrapper">
                <img src={Images.mailIcon} />
                <input
                    autoFocus={true}
                    onKeyPress={(e) => e.which === 13 ? setMethod(12) : console.log()}
                    name="email"
                    onChange={(e) => { emailRegex.test(e.target.value) ? setEmailValid(true) : setEmailValid(false); setEmailAddress(e.target.value) }}
                    placeholder={`Enter ${details?.recipient}'s Email...`} />

            </div>;
            case 7: return <div className="recipient-wrapper">
                <input
                    value={details.question}
                    autoFocus={true}
                    onKeyPress={(e) => e.which === 13 && details.question ? setMethod(8) : console.log()}
                    name="question"
                    onChange={(e) => setDetails({ ...details, [e.target.name]: e.target.value })}
                    placeholder="What Is The Question" />

            </div>;
            case 8: return <div className="recipient-wrapper">
                <input
                    value={details.answer}
                    autoFocus={true}
                    onKeyPress={(e) => e.which === 13 && details.answer ? setMethod(9) : console.log()}
                    name="answer"
                    onChange={(e) => setDetails({ ...details, [e.target.name]: e.target.value })}
                    placeholder="What Is The Answer" />

            </div>;
            case 9: return <div className="recipient-wrapper">
                <input
                    value={details.confirm}
                    autoFocus={true}
                    onKeyPress={(e) => e.which === 13 && details.confirm.toString().toLowerCase() === details.answer.toString().toLowerCase() ? setMethod(10) : console.log()}
                    name="confirm"
                    onChange={(e) => setDetails({ ...details, [e.target.name]: e.target.value })}
                    placeholder="Confirm The Answer" />

            </div>;
            case 10: return <div className="recipient-wrapper">
                <input
                    value={details.message}
                    autoFocus={true}
                    onKeyPress={(e) => e.which === 13 && details.message ? handleSubmit() : console.log()}
                    name="message"
                    onChange={(e) => setDetails({ ...details, [e.target.name]: e.target.value })}
                    placeholder="Enter Custom Message" />

            </div>;

            case 11: return <div className="users-number-wrapper">
                <span>Do You Want To Stay Up To Date With This Transaction With Live SMS? If So, Enter Your Phone Number Here</span>
                <div className="enter-phone-wrapper">
                    <span onClick={() => setDropDownUser(!dropDownUser)}>
                        {userCurrentCode.dial_code}
                    </span>
                    <span>
                        <input
                            autocomplete="nope"
                            autoFocus={true}
                            onChange={e => { phoneRegex.test(e.target.value) ? setValid(true) : setValid(false); setDetails({ ...details, [e.target.name]: e.target.value }) }}
                            onKeyPress={(e) => handleKeyPress(e, true)}
                            name="userMobile"
                            placeholder="000 000 0000" />
                    </span>
                    <div className={!dropDownUser ? "d-none" : "cc-dropdown"}>
                        <div className="cc-dropdown-header">
                            <input onChange={e => setSearchTermUser(e.target.value)} placeholder="Enter Your Country" />
                        </div>
                        <div className="cc-dropdown-body">
                            {
                                Constant.countryCodes.filter(x => { return x.name.toString().toLowerCase().startsWith(searchTermUser.toString().toLowerCase()) })
                                    .map(obj => <div onClick={() => { setUserCurrentCode(obj); setDropDownUser(false) }}>
                                        <span>{obj.dial_code}</span><span>{obj.name}</span>
                                    </div>)
                            }
                        </div>

                    </div>
                </div>


            </div>

        }
    }
    const selectButton = () => {
        switch (method) {
            case 0: return null;
            case 1: return <button disabled={!details.recipient} onClick={() => emailOrPhone ? setMethod(6) : setMethod(2)}>Proceed</button>;
            case 2: return <button disabled={!valid} onClick={() => valid ? setMethod(12) : null}>Proceed</button>;
            case 3: return null;
            case 4: return
            case 5: return <button disabled={!address && knowsAddress} onClick={() => setMethod(5)}>Proceed</button>;
            case 6: return <button disabled={!emailValid} onClick={() => setMethod(12)}>Proceed</button>
            case 7: return <button disabled={!details.question} onClick={() => setMethod(8)}>Proceed</button>
            case 8: return <button disabled={!details.answer} onClick={() => setMethod(9)}>Proceed</button>
            case 9: return <button disabled={details.confirm.toString().toLowerCase() !== details.answer.toString().toLowerCase()} onClick={() => setMethod(10)}>Proceed</button>
            case 10: return <button disabled={!details.message} onClick={() => handleSubmit()}>Proceed</button>
            case 11: return <button disabled={!valid} onClick={() => valid ? handleUserNum() : console.log}>Proceed</button>
        }
    }
    const handleUserNum = () => {
        setMethod(1);
        setValid(false)
    }
    const handleMethod = (id) => {
        switch (id) {
            case "email": setEmailOrPhone(true); setMethod(11); break;
            case "text": setEmailOrPhone(false); setMethod(11); break;
            default: break;
        }
    }

    const dateSender = () => {
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();

        today = mm + '/' + dd + '/' + yyyy;
        return today
    }
    const getClipboardText = () => {
        navigator.clipboard.readText()
            .then(
                text => setAddress(text)
            )
    }

    const handleKeyPress = (e, user) => {
        if (valid) {
            if (e.which === 13) {
                user ? setMethod(1) : setMethod(12);
            }
            // e.preventDefault();

        } else if (e.which < 48 || e.which > 57)
            e.preventDefault();
    }
    const handleValues = (crypto, value) => {
        if (rate === null)
            return
        if (crypto) {
            setCryptoValue(value);
            setUsdValue(valueFormatterInput(parseFloat(value * rate), "USD"))
        } else {
            setUsdValue(value);
            setCryptoValue(valueFormatterInput(parseFloat(value * (1 / rate)), fundsConfig.coin.coinSymbol))
        }
    }
    const setConversionRate = async () => {
        let { coin } = fundsConfig;
        let res = await conversionAPI(coin.coinSymbol, "USD");
        setRate(res.data[`${coin.coinSymbol.toLowerCase()}_usd`])
    }
    React.useEffect(() => {
        setConversionRate();
        setCryptoValue('');
        setUsdValue('')
    }, [fundsConfig.coin])
    return (
        <div className="cbse-page-one">
            <div className="cbse-page-one-left">
                {
                    method === 12 || method === 13 ?
                        method === 12 ? <SelectCoin setMethod={setMethod} /> : <Calculator setMethod={setMethod} cryptoValue={cryptoValue} usdValue={usdValue} handleSubmit={handleSubmit} handleValues={handleValues} />
                        :
                        <div className="extra-div">
                            <div className="method-title">
                                <img src={Images.blockCheck} />
                                <h6>Alliance Checks</h6>

                            </div>
                            {
                                selectMethod()
                            }
                            {selectButton()}
                        </div>
                }
            </div>
            <div className="receipt-someone">
                <CBMReceipt date={dateSender()} coinImage={currencyImageList[fundsConfig.coin.coinSymbol]} usdImage={currencyImageList["USD"]} coin={fundsConfig.coin.coinSymbol} notes={details.message} cryptoValue={valueFormatter(cryptoValue, fundsConfig.coin.coinSymbol)} address={details.mobile} usdValue={valueFormatter(usdValue, "USD")} payee="" />
            </div>

        </div>
    )
}

const methodList = [
    { keyId: nextId(), image: Images.mailIcon, text: "Email", id: "email" },
    { keyId: nextId(), image: Images.textIcon, text: "Text", id: "text" },
    { keyId: nextId(), image: Images.friendsIcon, text: "Friends", id: "friends" },
]
