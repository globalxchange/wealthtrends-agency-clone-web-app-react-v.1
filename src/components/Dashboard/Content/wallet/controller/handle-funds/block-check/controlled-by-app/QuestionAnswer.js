import React from 'react'
import Images from '../../../../../../../../assets/a-exporter'

export default function QuestionAnswer({ question, setLeftStep, selectedUser, setSelectedUser, }) {

    const handleClick = () => {
        if(question){
            if(!selectedUser.question)
                return
            setLeftStep(1);
        }else{
            if(!selectedUser.answer)
                return
            setLeftStep(2);
        }
    }
    const handleChange = (e) =>{
        if(question){
            setSelectedUser({...selectedUser, question: e.target.value})
        }else{
            setSelectedUser({...selectedUser, answer: e.target.value})
        }
    }
    const handleKeyPress= (e) =>{
        if(question){
            if(e.which === 13){
                !selectedUser.question?console.log():setLeftStep(1);
            }
        }else{
            if(e.which === 13){
                !selectedUser.answer?console.log():setLeftStep(2);
            }

        }

    }
    return (
        <div className="question-answer">
            <div className="qa-title">
                <img src={Images.blockCheck} />

            </div>
            <div className="qa-input-wrapper">
                <input onKeyPress={handleKeyPress} value={selectedUser[question?"question": "answer"]} onChange= {handleChange} placeholder={question ? "Enter Your Question" : "Enter your Answer"} />

            </div>
            <button disabled ={question?!selectedUser.question:!selectedUser.answer} onClick={() => handleClick()}>
                Proceed
            </button>

        </div>
    )
}
