import React from 'react'
import Images from '../../../../../../../../assets/a-exporter'
import LoadingAnimation from '../../../../../../../../lotties/LoadingAnimation';
import { cryptoUSD, fetchPathId } from '../../../../../../../../services/getAPIs';
import { controlledByMeOne, controlledByMeTwo } from '../../../../../../../../services/postAPIs';
import { Agency } from '../../../../../../../Context/Context';
import CBMReceipt from './receipt/CBMReceipt';

export default function ControlledByMe() {
    const [address, setAddress] = React.useState("");
    const [cryptoValue, setCryptoValue] = React.useState('');
    const [usdValue, setUsdValue] = React.useState('');
    const [rate, setRate] = React.useState(1);
    const [step, setStep] = React.useState(0)
    const [pathId, setPathId] = React.useState('');
    const [loading, setLoading] = React.useState(true);
    const [submitLoading, setSubmitLoading] = React.useState(false);
    const [notes, setNotes] = React.useState('');
    const agency = React.useContext(Agency);
    const { fundsConfig, setMainBalance,valueFormatterInput,currencyImageList, currentApp, changeNotification, valueFormatter } = agency;

    const reset = () =>{
        setAddress('');
        setCryptoValue('');
        setUsdValue('');
        setStep(0);
        setNotes('')
    }
    const getClipboardText = () => {
        navigator.clipboard.readText()
            .then(
                text => setAddress(text)
            )
    }

    const setConversionRate = async () => {
        let res = await cryptoUSD();
        let { coin } = fundsConfig;
        setRate(res.data[coin.coinSymbol])
    }

    const handleValues = (crypto, value) => {
        if (crypto) {
            setCryptoValue(value);
            setUsdValue(valueFormatterInput(parseFloat(value * rate), "USD"))
        } else {
            setUsdValue(value);
            setCryptoValue(valueFormatterInput(parseFloat(value * (1 / rate)), fundsConfig.coin.coinSymbol))
        }
    }

    const getPathId = async () => {
        let res;
        try {
            res = await fetchPathId(fundsConfig.coin.coinSymbol);
            console.log("path_id", res.data)
            setPathId(res.data.paths[0].path_id);
            setLoading(false);
        } catch (e) {
            console.error(e);
            // alert("Try Again");
            setLoading(false);
        }
    }
    const selectStep = () => {
        switch (step) {
            case 0: return <div className="enter-address-wrapper">
                <input value={address} onChange={(e)=>setAddress(e.target.value)} placeholder={`Enter ${fundsConfig.coin.coinSymbol} Address`} />
                <img onClick={() => getClipboardText()} src={Images.pasteIcon} />
            </div>;
            case 1: return <div className="enter-values-wrapper">
                <h6><input value={cryptoValue} onChange={(e) => handleValues(true, e.target.value)} placeholder={`${valueFormatterInput(0, fundsConfig.coin.coinSymbol)}`} /><span>{fundsConfig.coin.coinSymbol}</span></h6>
                <h6><input value={usdValue} onChange={(e) => handleValues(false, e.target.value)} placeholder={`${valueFormatterInput(0, "USD")}`} /><span>USD</span></h6>
            </div>;
            case 2: return <div className="notes-wrapper">
                <input onChange={(e) => setNotes(e.target.value)} placeholder="Attach Notes To The Transaction" />
            </div>
            default: break;
        }
    }
    const handleProceed = () => {
        switch (step) {
            case 0: !address ? console.log() : setStep(step + 1); break;
            case 1: !cryptoValue ? console.log() : setStep(step + 1); break;
            case 2: handleSubmit(); break;
            default:
                break;
        }
    }
    const checkDisabled = () => {
        switch (step) {
            case 0: return !address ? true : false
            case 1: return !cryptoValue ? true : false
            case 2: return !notes ? true : false
            default:
                break;
        }
    }
    const dateSender = () => {
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();

        today = mm + '/' + dd + '/' + yyyy;
        return today
    }
    const handleSubmit = async () => {
        setSubmitLoading(true)
        let temp = {
            token: localStorage.getItem("idToken"),
            email: localStorage.getItem("userEmail"),
            app_code: currentApp.app_code,
            profile_id: currentApp.profile_id,
            from_amount: cryptoValue,
            stats: false,
            identifier: address,
            path_id: pathId,
            userWithdrawData: address,
            friendId: ''
        }
        let res = await controlledByMeOne({ data: { ...temp }, encode: true });
        let encode = res.data.data
        if (res.data.status) {
            let resTwo = await controlledByMeTwo({ data: encode });
            if (resTwo.data.status) {
                setMainBalance();
                changeNotification({ status: true, message: "Trade Successful" })
                setSubmitLoading(false);
                reset();

            } else {
                changeNotification({ status: false, message: "Trade Failed" })
                setSubmitLoading(false);
                reset();

            }
        } else {
            changeNotification({ status: false, message: "Trade Failed" })
            setSubmitLoading(false);
                reset();
        }

    }
    React.useEffect(() => {
        setLoading(true);
        setConversionRate();
        setCryptoValue('');
        setUsdValue('');
        getPathId();
    }, [fundsConfig.coin])
    return (
        loading ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation type="login" size={{ height: 175, width: 175 }} />
            </div>
            :
            <div className="bc-step-two">
                <div className="bc-step-two-left">
                    {
                        submitLoading ?
                            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                                <LoadingAnimation type="login" size={{ height: 175, width: 175 }} />
                            </div>
                            :
                            <div className="bc-step-two-child">
                                <div className="bc-two-amount-title">
                                    <img src={Images.blockCheck} />
                                    <h6>Personal Checks</h6>
                                </div>
                                {/* <div className="bc-two-body"> */}
                                    {
                                        selectStep()
                                    }
                                {/* </div> */}
                                <button disabled={checkDisabled()} onClick={() => handleProceed()}>
                                    Proceed
                                </button>
                            </div>
                    }

                </div>
                <div className="bc-step-two-right">
                    <CBMReceipt
                        date={dateSender()}
                        notes={notes}
                        cryptoValue={valueFormatter(cryptoValue, fundsConfig.coin.coinSymbol)}
                        usdValue={valueFormatter(usdValue, "USD")}
                        address={address}
                        coin={fundsConfig.coin.coinSymbol}
                        coinImage={currencyImageList[fundsConfig.coin.coinSymbol]}
                        usdImage={currencyImageList["USD"]}

                    />
                </div>

            </div>
    )
}
