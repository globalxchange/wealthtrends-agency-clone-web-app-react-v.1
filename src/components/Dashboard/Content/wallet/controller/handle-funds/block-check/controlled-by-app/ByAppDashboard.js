import React from 'react'
import CBMReceipt from '../controlled-by-me/receipt/CBMReceipt'
import ByAppCalculator from './ByAppCalculator'
import QuestionAnswer from './QuestionAnswer';

export default function ByAppDashboard({ cryptoValue, setSelectedUser, handleSubmit, currencyImageList, selectedUser, usdValue, details, setCryptoValue, setUSDValue }) {
    const [leftStep, setLeftStep] = React.useState(0);

    const selectLeftComponent = () => {
        switch (leftStep) {
            case 0: return <QuestionAnswer setLeftStep={setLeftStep} setSelectedUser={setSelectedUser} selectedUser={selectedUser} question={true} />;
            case 1: return <QuestionAnswer setLeftStep={setLeftStep} setSelectedUser={setSelectedUser} selectedUser={selectedUser} question={false} />;
            case 2: return <ByAppCalculator
                handleSubmit={handleSubmit}
                selectedUser={selectedUser}
                details={details}
                cryptoValue={cryptoValue}
                usdValue={usdValue}
                setCryptoValue={setCryptoValue}
                setUSDValue={setUSDValue}
            />
        }
    }

    const dateSender = () => {
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();
        today = mm + '/' + dd + '/' + yyyy;
        return today
    }
    React.useEffect(()=>{
        setSelectedUser({...selectedUser, answer: '', question: ''})
    },[])
    return (
        <div className="by-app-dashboard">
            <div className="by-app-dashboard-left">
                {
                    selectLeftComponent()
                }

            </div>
            <div className="by-app-dashboard-right">
                <CBMReceipt
                    date={dateSender()}
                    coinImage={selectedUser?.coin?.coinImage}
                    coin={selectedUser?.coin?.coinSymbol}
                    cryptoValue={cryptoValue}
                    usdValue={usdValue}
                    address = {selectedUser.destination_data}
                    // address={selectedUser?.coin?.crypto_address}
                    usdImage={currencyImageList["USD"]}
                />
            </div>
        </div>
    )
}
