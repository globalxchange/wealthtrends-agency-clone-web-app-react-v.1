import React from 'react'
import './select-coin.style.scss'
import SelectCoinList from './SelectCoinList'
import SelectedCoin from './SelectedCoin'
export default function SelectCoin({setMethod}) {
    const [CStep, setCStep] = React.useState(0);
    const selectSelectCoinStep = () =>{
        switch(CStep){
            case 0: return <SelectedCoin setMethod={setMethod} setCStep={setCStep} />;
            case 1: return <SelectCoinList setCStep={setCStep}/>
        }
    }
    return (
        <div className="select-coin">
            {selectSelectCoinStep()}

            
        </div>
    )
}
