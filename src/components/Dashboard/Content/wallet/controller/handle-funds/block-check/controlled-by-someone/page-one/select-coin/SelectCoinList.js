import React from 'react'
import LoadingAnimation from '../../../../../../../../../../lotties/LoadingAnimation';
import { getMainBalance } from '../../../../../../../../../../services/postAPIs';
import { Agency } from '../../../../../../../../../Context/Context';

export default function SelectCoinList({ setCStep }) {
    const [searchTerm, setSearchTerm] = React.useState('');
    const [currentList, setCurrentList] = React.useState([]);

    const agency = React.useContext(Agency);
    const { currentApp, valueFormatter, setFundsConfig, fundsConfig } = agency;

    const handleList = async () => {
        let res;
        try {
            res = await getMainBalance(currentApp.app_code, currentApp.profile_id);
            setCurrentList(res.data.coins_data)
        } catch (e) {
            console.error(e);
            setCurrentList([]);
        }
    }
    const handleClick = (obj) => {
        setFundsConfig({ ...fundsConfig, coin: obj })
        setCStep(0);

    }
    React.useEffect(() => {
        handleList();
    }, [])

    return (
        <div className="select-coin-list">
            <div className="select-coin-list-header">
                <h5>Pick Fulfillment Asset</h5>
                <span>From Your {currentApp.app_code.toString().toUpperCase()} Balance</span>
                <input placeholder="Search Any Asset" onChange={(e) => setSearchTerm(e.target.value)} />

            </div>
            <div className="select-coin-list-body">
                {

                    !currentList.length
                        ?
                        <div className="w-100 h-100 flex-column d-flex justify-content-center align-items-center">
                            <LoadingAnimation type="no-data" size={{ height: 120, width: 120 }} />
                            {/* <span className="font-weight-bold mt-3">Please Select An App</span> */}
                        </div>
                        :
                        currentList.filter(
                            x => { return x.coinSymbol.toString().toLowerCase().startsWith(searchTerm.toLowerCase()) || x.coinName.toString().toLowerCase().startsWith(searchTerm.toLowerCase()) }
                        ).map(obj =>
                            <div onClick={() => handleClick(obj)}>
                                <span><img src={obj.coinImage} /> {obj.coinName}</span>
                                <span>{valueFormatter(obj.coinValue, obj.coinSymbol)}</span>
                            </div>
                        )
                }

            </div>


        </div>
    )
}
