import React from 'react'
import { Agency } from '../../../../../../../../Context/Context';
import './by-apps-coin-list.style.scss'
export default function ByAppsCoinList({setStep,details,setList, list, setDetails}) {
    const agency = React.useContext(Agency);
    const { valueFormatter } = agency;

    const handleClick = (obj) =>{
        setDetails({...details, coin: obj});
        setStep(5)

    }

    React.useEffect(() => {
    }, [])
    return (
        <div className="by-apps-coin-list">
            <div className="by-apps-coin-list-header">
                <h4>Assign A Default Asset</h4>
                <h6>For Your New Contact</h6>

            </div>
            <div className="by-apps-coin-list-body">
                {
                    list.map(obj => <div onClick={()=>handleClick(obj)} className="by-app-coin-list-row">
                        <span><img src={obj.coinImage} />{obj.coinName}</span>
                        <span>{valueFormatter(obj.coinValue, obj.coinSymbol)}</span>
                    </div>
                    )
                }

            </div>


        </div>
    )
}
