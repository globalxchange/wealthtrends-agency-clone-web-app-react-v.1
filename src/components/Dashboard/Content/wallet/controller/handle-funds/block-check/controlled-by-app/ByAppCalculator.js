import React from 'react'
import { conversionAPI } from '../../../../../../../../services/getAPIs';
import { Agency } from '../../../../../../../Context/Context'

export default function ByAppCalculator({ cryptoValue, handleSubmit, usdValue, selectedUser, setCryptoValue, setUSDValue }) {
    const [rate, setRate] = React.useState(null);
    const agency = React.useContext(Agency);
    const { cryptoUSD, valueFormatterInput, valueFormatter } = agency;
    const handleChange = (val, which) => {
        if(rate === null)
            return
        if (which) {
            setCryptoValue(val);
            let other = parseFloat(val * rate);
            setUSDValue(valueFormatterInput(other, "USD"));

        } else {
            setUSDValue(val);
            let other = parseFloat(val * (1 / rate));
            setCryptoValue(valueFormatterInput(other, selectedUser?.coin?.coinSymbol));
        }

    }
    const setUpRate = async () => {
        let res = await conversionAPI(selectedUser?.coin?.coinSymbol, "USD");
        setRate(res.data[`${selectedUser?.coin?.coinSymbol.toLowerCase()}_usd`])
    }
    React.useState(() => {
        setUpRate()
    }, [selectedUser?.coin?.coinSymbol])
    return (
        <div className="by-app-calculator">
            <div className="by-app-calculator-title">
                <h3>{valueFormatter(selectedUser?.coin?.coinValue, selectedUser?.coin?.coinSymbol)}</h3>
                <h6>{selectedUser?.coin?.coinName} Balance</h6>
            </div>
            <div className="by-app-calculator-input-wrapper">
                <h6><input autoFocus={true} placeholder={`${valueFormatter(0, selectedUser?.coin?.coinSymbol)}`} value={cryptoValue} onChange={(e) => handleChange(e.target.value, true)} /> <span>{selectedUser?.coin?.coinSymbol}</span></h6>
                <h6><input placeholder={`${valueFormatter(0, "USD")}`} value={usdValue} onChange={(e) => handleChange(e.target.value, false)} /> <span>USD</span></h6>
            </div>
            <button onClick={() => handleSubmit()} disabled={cryptoValue <= 0 || !cryptoValue || cryptoValue > selectedUser?.coin?.coinValue}>
                Proceed
            </button>

        </div>
    )
}
