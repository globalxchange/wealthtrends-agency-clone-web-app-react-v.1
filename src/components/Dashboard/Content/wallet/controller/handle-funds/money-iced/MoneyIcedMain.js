import React from 'react'
import nextId from 'react-id-generator';
import Images from '../../../../../../../assets/a-exporter';
import { Agency } from '../../../../../../Context/Context';
import MiDashboard from './mi-dashboard/MiDashboard';
import MiLoading from './mi-loading/MiLoading';
import MiResult from './mi-result/MiResult';
import './money-iced-main.style.scss'
export default function MoneyIcedMain({ id }) {

    const agency = React.useContext(Agency);
    const { setWhichOne, setMiConfig, currentApp,allApps, setMainSteps, mainSteps } = agency;
    const selectMainComponent = () => {
        switch (mainSteps) {
            case 0: return <MiDashboard setMainSteps={setMainSteps} />;
            case 1: return <MiLoading />;
            case 2: return <MiResult />
        }
    }

    React.useEffect(() => {
        if (id === "iced") {
            let temp = allApps.find((x)=>{return x.app_code === "ice"})
            let obj = {
                mi: false,
                fromApp: {
                    ...temp,
                    text: "Sending From",
                },
                toApp: {
                    ...currentApp,
                    text: "Sending To"
                }
            }
            setMiConfig({...obj})

        }else{
            let temp = {
                mi: true,
                fromApp: {
                    text: "Sending From", 
                    ...currentApp,
                    app_name: currentApp.app_name + " Money Markets"

                },
                toApp: {
                    text: "Sending To", 
                    ...currentApp
                }
            }
            console.log("mi-money", temp);
            setWhichOne(false)
            setMiConfig({...temp})

        }

    }, [id])
    return (
        <div className="money-iced-main">
            {selectMainComponent()}
        </div>
    )
}
