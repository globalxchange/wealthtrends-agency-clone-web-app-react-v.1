import React from 'react'
import Images from '../../../../../../../../../../assets/a-exporter';
import { Agency } from '../../../../../../../../../Context/Context'

export default function ICAppContainers({ setIvSteps, }) {
    const agency = React.useContext(Agency);
    const { allApps, miConfig, miWhichOne, setMiConfig } = agency;
    const [searchTerm, setSearchTerm] = React.useState('');

    const handleClick = (obj) => {
        if (miConfig.mi) {
            setMiConfig({ ...miConfig, fromApp: { ...obj,app_name: `${obj.app_name} Money Market`, text: "Sending From" }, toApp: { ...obj, text: "Sending To" } })
        }
        else if (miWhichOne) {
            setMiConfig({ ...miConfig, toApp: { ...obj, text: "Sending To" } })
        } else {
            setMiConfig({ ...miConfig, fromApp: { ...obj, text: "Sending From" } })
        }
        setIvSteps(0)
    }
    const returnText = () => {
        return miWhichOne ?
            "To Which Of Your Apps Do You Want To Send Your Interest?"
            :
            "From Which App Do You Want To Withdraw Your Interest?"
    }
    return (
        <div className="mi-iv-app-container">
            <div className="mi-iv-apps-header">
                <h6>{returnText()}</h6>
            </div>
            <div className="mi-iv-apps-body">
                <div className="mi-apps-body-search-bar">
                    <input onChange={(e) => setSearchTerm(e.target.value)} placeholder="Search You Apps" />
                </div>
                <div className="mi-apps-body-app-list">
                    {
                        allApps.filter(obj => { return obj.app_name.toString().toLowerCase().startsWith(searchTerm.toString().toLowerCase()) })
                            .map(obj => <div className="app-icon-wrapper">
                                <div onClick={() => handleClick(obj)}>
                                    <img onError={(e) => e.target.src = Images.agencyDark} src={obj.app_icon} />
                                    <span>{obj.app_name}</span>
                                </div>

                            </div>
                            )
                    }

                </div>

            </div>
        </div>
    )
}
