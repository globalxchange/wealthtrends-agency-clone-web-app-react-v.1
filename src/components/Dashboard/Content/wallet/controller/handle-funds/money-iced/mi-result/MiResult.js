import React from 'react'
import nextId from 'react-id-generator';
import Images from '../../../../../../../../assets/a-exporter';
import { Agency } from '../../../../../../../Context/Context';
import './mi-result.style.scss'
export default function () {
    const agency = React.useContext(Agency);
    const { miConfig, miLoadingStep, selectedCoinAndApp, miAmount, valueFormatter } = agency;
    return (
        <div className="mi-result-main">
            <div className="mi-result-top">
                <h3>Transaction Helper</h3>
                <span>Great Job. Your Deposit Has Been Successful.
                Here Are Some Things You Can Do With The {valueFormatter(miAmount,selectedCoinAndApp.coin?.coinSymbol)} {selectedCoinAndApp.coin?.coinName} You Have</span>
            </div>
            <div className="mi-result-bottom">
                {
                    apps.map(obj =><div style={{backgroundColor: `${obj.color}`}}>
                        <img src={obj.image} />
                    </div>)
                }

            </div>
        </div>
    )
}
const apps = [
    {id: nextId(), image: Images.tellerBox, text: "", color: "#182542"},
    {id: nextId(), image: Images.connectBox, text: "", color: "#1A6BB4"},
    {id: nextId(), image: Images.vaultBox, text: "", color: "#626669"},
]