import React from 'react'
import nextId from 'react-id-generator';
import Images from '../../../../../../../../../../assets/a-exporter'
import { Agency } from '../../../../../../../../../Context/Context'

export default function ICHome({ setIvSteps }) {
    const agency = React.useContext(Agency);
    const { miConfig, currentApp, setWhichOne,miWhichOne, allApps } = agency
    const [apps, setApps] = React.useState([]);
    const [normal, setNormal] = React.useState(true);
    const [selectApp, setSelectApp] = React.useState();

    const handleApp = (num, obj) => {
     
        if (miConfig.mi) {
            if(num)
                return
            if (num) {
                setWhichOne(true);
                setSelectApp("Sending To");
            } else {
                setSelectApp("Sending From");
                setWhichOne(false);
            }
            // normal?setNormal(!normal):setIvSteps(1);
        } else {
            if (!num) {
                // setWhichOne(null)
                // setIvSteps(1)
            } else {
                setWhichOne(true);
                normal?setNormal(!normal):setIvSteps(1);
                // setSelectApp("Sending To")
            }

        }
    }
    React.useEffect(() => {
        setNormal(true);
        let app = allApps.find((x)=>{return x.app_code === "ice"})
        let temp = {
            ...app,
            text: "Sending From"
        }
        if (!miConfig.mi) {
            setApps([temp, { ...miConfig.toApp }]);
            setSelectApp("Sending To")
        } else {
            setApps([miConfig.fromApp, { ...miConfig.toApp }]);
            miWhichOne?setSelectApp("Sending To"):setSelectApp("Sending From")
        }
    }, [miConfig.mi])
    return (
        <div className="mi-iv-home">
            <div className="mi-iv-home-header">
                <img src={Images.hfConnect} />
                <span>|</span>
                <img src={miConfig.mi ? Images.navbarMoney : Images.navbarIced} />
            </div>
            <div className="mi-iv-home-apps">
                {
                    apps.map((obj, num) =>
                        <div onClick={() => handleApp(num, obj)} className={`app-wrapper ${selectApp === obj.text ? "app-selected" : ""}`}>
                            <span>{obj?.text}</span>
                            <div>
                                <img src={obj?.app_icon} />
                            </div>
                            <span>{obj?.app_name}</span>
                        </div>
                    )
                }
            </div>
            <div className="mi-iv-home-footer">
                <div className="button-text-wrapper">
                    <p onClick={() => setNormal(!normal)}>{normal?"Want To Change The Selected Apps?": "Click On Any App Above To Change"}</p>
                    {
                        normal ?
                            <button>Next Step</button>
                            :
                            <div>
                                You Will Be Adding Funds To Your {currentApp.app_name} With Earnings From Your Bonds. Please <b onClick={() => setIvSteps(1)}>Click Here</b> If You Want Your Funds Sent To Another App
                        </div>
                    }

                </div>

            </div>

        </div>
    )
}
