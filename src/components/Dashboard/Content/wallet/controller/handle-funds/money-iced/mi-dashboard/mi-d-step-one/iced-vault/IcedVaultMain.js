import React from 'react'
import ICAppContainers from './ICAppContainers';
import './iced-vault-main.style.scss';
import ICHome from './ICHome';
export default function IcedVaultMain() {
    const [ivSteps, setIvSteps] = React.useState(0);

    const decideStep = () =>{
        switch(ivSteps){
            case 0: return <ICHome setIvSteps={setIvSteps} />;
            case 1: return <ICAppContainers setIvSteps={setIvSteps} />;
            default: 
        }
    }

    return (
        <div className="mi-iced-vault-main">
            {
                decideStep()
            }
        </div>
    )
}
