import React from 'react'
import { Agency } from '../../../../../../../Context/Context';
import IcedVaultMain from './mi-d-step-one/iced-vault/IcedVaultMain';
import MoneyMarketMain from './mi-d-step-one/money-market/MoneyMarketMain';
import MiCalculator from './mi-d-step-three/MiCalculator';
import MIDList from './mi-d-step-two/MIDList';
import './mi-dashboard.style.scss'
export default function MiDashboard({ setMainSteps }) {
    const [dashboardSteps, setDashboardSteps] = React.useState(0);
    const [currentWrapper, setCurrentWrapper] = React.useState(0);

    const agency = React.useContext(Agency);
    const { mi } = agency.miConfig;

    // const decideStepOne = () =>{
    //     return mi?<MoneyMarketMain />:<IcedVaultMain />
    // }
    return (
        <div className="mi-dashboard-main">
            <div onClick={() => setCurrentWrapper(0)} className={`mi-d-step-one-wrapper ${!currentWrapper ? "selected-wrapper" : ""}`}>
                <IcedVaultMain />
            </div>
            <div onClick={() => setCurrentWrapper(1)} className={`mi-d-step-two-wrapper ${currentWrapper === 1 ? "selected-wrapper" : ""}`}>
                <MIDList />
            </div>
            <div onClick={() => setCurrentWrapper(2)} className={`mi-d-step-three-wrapper ${currentWrapper === 2 ? "selected-wrapper" : ""}`}>
                <MiCalculator />
            </div>
        </div>
    )
}
