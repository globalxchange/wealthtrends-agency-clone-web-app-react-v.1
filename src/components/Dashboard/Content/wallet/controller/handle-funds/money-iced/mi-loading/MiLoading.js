import React from 'react'
import { Agency } from '../../../../../../../Context/Context'
import './mi-loading.style.scss'
export default function MiLoading() {
    const agency = React.useContext(Agency);
    const { miConfig, miLoadingStep, selectedCoinAndApp,miAmount, valueFormatter } = agency;
    return (
        <div className={`mi-loading-main ${miConfig.mi && (miConfig.fromApp.app_code === miConfig.toApp.app_code)?"hide-second-loader":""}`}>
            <div className={`mi-loading-main-left ${miLoadingStep === 1?"selected-side":""}`}>
                <h6>
                    Extracting Earnings From The Ice Protocol Into Your
                </h6>
                <div>
                    <img src={miConfig.fromApp?.app_icon} />
                    <span>{miConfig.fromApp?.app_name}</span>
                </div>
            </div>
            <div className={`mi-loading-main-right ${miLoadingStep === 2?"selected-side":""}`}>
                <h6>
                    Transferring {valueFormatter(miAmount, selectedCoinAndApp.coin?.coinSymbol)} {selectedCoinAndApp.coin?.coinName} From {miConfig.fromApp?.app_name} To You {miConfig.toApp?.app_name} Vault
                </h6>
                <div>
                    <img src={miConfig.toApp?.app_icon} />
                    <span>{miConfig.toApp?.app_name}</span>
                </div>

            </div>
        </div>
    )
}

