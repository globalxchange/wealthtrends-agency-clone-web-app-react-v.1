import React from 'react'
import { conversionAPI } from '../../../../../../../../../services/getAPIs';
import { Agency } from '../../../../../../../../Context/Context'
import './mi-calculator.style.scss'
export default function MiCalculator() {
    const agency = React.useContext(Agency);
    const { selectedCoinAndApp,changeNotification,setMiAmount,moneyCoins, valueFormatterInput, withdrawFromBondsEarning, valueFormatter, miConfig, cryptoUSD } = agency
    const [rate, setRate] = React.useState(null);
    const [cryptoValue, setCryptoValue] = React.useState('');
    const [usdValue, setUsdValue] = React.useState('');
    const [warnings, setWarnings] = React.useState([])

    const handleChange = (e, type) => {
        if(rate === null)
            return
        if (selectedCoinAndApp?.coin === null)
            return
        if (type) {
            setMiAmount(e.target.value)
            setCryptoValue(e.target.value);
            setUsdValue(valueFormatterInput(parseFloat(e.target.value * rate), "USD"));
        } else {
            setUsdValue(e.target.value);
            setCryptoValue(valueFormatterInput(parseFloat(e.target.value * (1 / rate)), selectedCoinAndApp?.coin?.coinSymbol));
            setMiAmount(parseFloat(e.target.value * (1 / rate)))
        }
    }
    const handleSubmit = ()=>{
        if(cryptoValue >selectedCoinAndApp.coin.coinValue && !miConfig.mi ){
            changeNotification({status: false, message: "Exceeding Your Balance"})
        }else
        {
            if(miConfig.mi){
                let temp = checkWarnings();
                if(!temp.length){
                    withdrawFromBondsEarning(cryptoValue);
                }
                else{
                    setWarnings([...temp])
                }
            }else{
                withdrawFromBondsEarning(cryptoValue);
            }
        }
    }
    const checkWarnings = () =>{

        let temp= [];
        // if(moneyCoins?.from === null){
        //     temp =[...temp, `*Select Vault from ${miConfig?.fromApp?.app_name} App`]
        // }
        if(moneyCoins.to === null){
            temp =[...temp, `*Select A Vault from ${miConfig?.toApp?.app_name} App`]
        }
        // if(miConfig.fromApp?.app_code === miConfig?.toApp?.app_code){
        //     temp = [...temp, `*You Can't Transfer To Same App`]
        // };
        !cryptoValue?console.log():setWarnings([...temp]);
        return temp;
    }

    const setUpConversionRate = async() =>{
        let res = await conversionAPI(selectedCoinAndApp?.coin?.coinSymbol, "USD");
        setRate(res.data[`${selectedCoinAndApp?.coin?.coinSymbol.toLowerCase()}_usd`])
        
    } 
    React.useEffect(()=>{
        checkWarnings();
    },[moneyCoins, miConfig])

    React.useEffect(() => {
        setCryptoValue(''); setUsdValue('');
        setUpConversionRate()
    }, [selectedCoinAndApp])

    return (
        <div className="mi-calculator-main">
            <div className="mi-calculator-above">
                <div className="calci-header">
                    <h3>{!selectedCoinAndApp.coin ? "Select A Vault" : valueFormatter(selectedCoinAndApp.coin.coinValue, selectedCoinAndApp.coin.coinSymbol)}</h3>
                    <span>Withdrawable Balance From Earnings Via {miConfig.mi ? "Money Markets" : "Iced Vault"}</span>
                </div>
                <div className="mi-enter-data-wrapper">
                    <h6>
                        <input value={cryptoValue} onChange={(e) => handleChange(e, true)} placeholder="0.000" />
                        <span>{!selectedCoinAndApp.coin ? "TBD" : selectedCoinAndApp.coin.coinSymbol}</span>
                    </h6>
                    <h6>
                        <input value={usdValue} onChange={(e) => handleChange(e, false)} placeholder="$0.00" />
                        <span>USD</span>
                    </h6>
                </div>
                <div className="warnings">
                    {
                        warnings.map(x =><h6>{x}</h6>)
                    }

                </div>
            </div>
            <button
                onClick={() => handleSubmit()}
                disabled={!cryptoValue || !selectedCoinAndApp.coin}>Proceed</button>
        </div>
    )
}
