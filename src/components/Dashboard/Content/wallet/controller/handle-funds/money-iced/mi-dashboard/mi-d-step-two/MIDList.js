import React from 'react'
import Images from '../../../../../../../../../assets/a-exporter';
import LoadingAnimation from '../../../../../../../../../lotties/LoadingAnimation';
import { getBondEarning, getListForMoney } from '../../../../../../../../../services/getAPIs';
import { Agency } from '../../../../../../../../Context/Context';
import './mid-list.style.scss'
export default function MIDList() {
    const agency = React.useContext(Agency);
    const { miConfig, miWhichOne,selectedCoinAndApp, valueFormatter,setMoneyCoins,moneyCoins,  setSelectedCoinAndApp } = agency;
    const [selectedCoin, setSelectedCoin] = React.useState(null);

    const [coinList, setCoinList] = React.useState([]);
    const [searchTerm, setSearchTerm] = React.useState("");
    const setTheList = async () => {

        if (miConfig.mi) {
            if (miWhichOne) {
                let res = await getListForMoney(miConfig?.toApp?.app_code);
                if (!res.data.status) {
                    res = []
                } else {
                    res = res.data.result[0].balances[0].liquid_balances
                }
                setCoinList(res)
            } else {

                let res = await getListForMoney(miConfig?.fromApp?.app_code);
                if (!res.data.status) {
                    res = []
                } else {
                    res = res.data.result[0].balances[0].liquid_balances
                }
                setCoinList(res)
            }
        } else {
            let res = await getBondEarning();
            if (!res.data.status) {
                res = []
            } else {
                res = res.data.result[0].balances
            }
            setCoinList(res);

        }

    }
    const handleCoinClick = (obj) => {
        if (miConfig.mi) {
            if(miConfig.fromApp.app_code === miConfig.toApp.app_code){
                setSelectedCoinAndApp({
                    coin: obj,
                    app: miConfig.fromApp
                })
                setMoneyCoins({...moneyCoins,to: obj, from: obj})

            }
            else if(!miWhichOne){
                alert()
                setSelectedCoinAndApp({
                    coin: obj,
                    app: miConfig.fromApp
                })
                setMoneyCoins({...moneyCoins, from: obj})
            }else{
                setMoneyCoins({...moneyCoins, to: obj})

            }
        } else {
            setSelectedCoin(obj);
            setSelectedCoinAndApp({
                coin: obj,
                app: miConfig.toApp
            })
        }

    }
    const returnText = () => {
        if(miConfig.mi){
            return `Which Money Earnings Vault Are You ${miWhichOne?"Depositing To":"Withdrawing From"}?`
        }else{
            return `Which Iced Earnings Vault Are You Withdrawing From?`

        }

    }
    const checkCoin=(obj) =>{
        if(miConfig.mi && !miWhichOne){
            return selectedCoinAndApp?.coin?.coinSymbol === obj.coinSymbol

        }else if(miConfig.mi && miWhichOne){
            return moneyCoins?.to?.coinSymbol === obj.coinSymbol
        }else{
            return selectedCoinAndApp?.coin?.coinSymbol === obj.coinSymbol
        }
    }

    React.useEffect(() => {
        setSelectedCoinAndApp({ app: null, coin: null });
        setMoneyCoins({from: null, to: null})
    }, [miConfig])

    React.useEffect(()=>{
        if (miConfig.mi === null)
            return
        setCoinList([])
        setTheList();
    },[miWhichOne, miConfig])

    return (
        <div className="mi-list-main">
            <div className="mi-list-main-header">
                <h2><img src={miConfig.mi ? Images.smallMoneyIcon : Images.smallIcedIcon} /> Earnings</h2>
                <p>{returnText()}</p>
                <input placeholder="Search Your Asset" onChange={(e) => setSearchTerm(e.target.value)} />
            </div>
            <div className="mi-list-main-body">
                {
                    !coinList.length
                        ?
                        <div className="animation-div w-100 h-100 d-flex justify-content-center align-items-center">
                            <LoadingAnimation type="no-data" size={{ height: 120, width: 120 }} />
                        </div>
                        :
                        coinList.filter(x => { return x.coinName.toString().toLowerCase().startsWith(searchTerm.toString().toLowerCase()) })
                            .map(x => <div className={checkCoin(x) ? "coin-selected" : ""} onClick={() => handleCoinClick(x)}>
                                <span><img src={x.coinImage} /> {x.coinName}</span>
                                <span>{valueFormatter(x.coinValue, x.coinSymbol)}</span>
                            </div>
                            )
                }

            </div>
        </div>
    )
}
