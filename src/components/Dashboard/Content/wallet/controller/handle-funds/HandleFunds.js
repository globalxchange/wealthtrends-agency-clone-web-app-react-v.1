import React from 'react'
import Constant from '../../../../../../json/constant';
import { Agency } from '../../../../../Context/Context';
import AnalyticsDeposit from '../../analytics/AnalyticsDeposit';
import DepositStepTwo from '../../analytics/deposit/deposit-step-two';
import BlockCheck from './block-check/BlockCheck';
import FDInformation from './funds-dashboard/fd-information/FDInformation';
import FundsDashboard from './funds-dashboard/FundsDashboard'
import FundsDashBoardWExternal from './funds-dashboard/FundsDashBoardExternal';
import FundsSidebar from './funds-sidebar/FundsSidebar'
import './handle-funds.style.scss'
import MoneyIcedMain from './money-iced/MoneyIcedMain';
const HandleFunds = React.memo(() => {
    const [dashboardStep, setDashboardStep] = React.useState(0);
    const agency = React.useContext(Agency);
    const [whichOverlay, setWhichOverlay] = React.useState(null)
    const { setFundsConfig, fundsConfig, setType, type, assetClassSelected } = agency

    const selectDashboard = () => {
        console.log("XXXXXXX", type.id)
        switch (type.id) {
            case "internal": return <FundsDashboard changeSteps={setDashboardStep} steps={dashboardStep} />;
            case "external": return <DepositStepTwo />
            case "money": return <FundsDashboard changeSteps={setDashboardStep} steps={dashboardStep} />;
            case "blockcheck": return <BlockCheck />;
            case "moneyMarket": console.log("RUNNNNNNNNNNNNNNNNNNNNNING"); return <MoneyIcedMain id={type.id} />;
            case "iced": ; return <MoneyIcedMain id={type.id} />;
            default: return <FDInformation setWhichOverlay={setWhichOverlay} whichOverlay={whichOverlay} />
        }
    }
    React.useEffect(() => {
        console.log("crypto type")
        setFundsConfig({ ...fundsConfig, coin: assetClassSelected });
        // setType({ keyId: '' })

    }, [assetClassSelected])
    React.useEffect(() => {
        return () => {
            setType({ keyId: '' });
        }
    }, [])
    React.useEffect(() => {

    }, [type])

    return (
        <div className="handle-funds">
            <div className={`handle-funds-sidebar ${!type.keyId ? "normal-sidebar" : "compressed-sidebar"}`}>
                <FundsSidebar setWhichOverlay={setWhichOverlay} whichOverlay={whichOverlay} type={type} setType={setType} hide={type.keyId} />
            </div>
            <div className={`handle-funds-dashboard ${!type.keyId ? "normal-dashboard" : "expand-dashboard"} `}>
                {
                    selectDashboard()
                }
            </div>

        </div>
    )
}
)
export default HandleFunds;