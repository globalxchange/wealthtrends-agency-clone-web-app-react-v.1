import React, { useContext, useState, useEffect } from "react";
import "./controller-analytics.style.scss";
import Constant from "../../../../../../json/constant";
import { Agency } from "../../../../../Context/Context";
import { ResponsiveContainer, LineChart, XAxis, YAxis, Line } from "recharts";
import AnalyticsValue from "../../../../../common-components/analytics-values/AnalyticsValue";
import { getAnalyticsValue } from "../../../../../../services/getAPIs";
const ControllerAnalytics = React.memo(() => {
  const agency = useContext(Agency);
  const {
    assetClassSelected,
    valueFormatter,
    setCopiedOverlay,
    conversionConfig,
    setAnalyticsSection,
    decimalValidation,
  } = agency;
  const [selected, setSelected] = useState();
  const [analyticsData, setAnalyticsData] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    handleAnalyticsData();
  }, [assetClassSelected]);
  const handleAnalyticsData = async () => {
    try {
      let res = await getAnalyticsValue("vault", assetClassSelected.coinSymbol);
      setLoading(false);
      setAnalyticsData(res.data);
    } catch (e) {
      setLoading(false);
    }
  };

  const getValue = (id) => {
    switch (id) {
      case "value":
        return conversionConfig.enable
          ? valueFormatter(
              assetClassSelected.coinValueUSD * conversionConfig.rate,
              conversionConfig.coin
            )
          : valueFormatter(assetClassSelected.coinValueUSD, "USD");
      case "roi":
        return !assetClassSelected?._24hrchange || !assetClassSelected.coinValue
          ? "0.00%"
          : parseFloat(Math.abs(assetClassSelected?._24hrchange)).toFixed(2) +
              "%";
      case "pl":
        return !assetClassSelected?._24hrchange
          ? conversionConfig.enable
            ? valueFormatter(0, conversionConfig.coin)
            : valueFormatter(0, "USD")
          : conversionConfig.enable
          ? valueFormatter(
              parseFloat(
                assetClassSelected.coinValueUSD *
                  assetClassSelected?._24hrchange *
                  conversionConfig.rate *
                  0.01
              ),
              conversionConfig.enable ? conversionConfig.coin : "USD"
            )
          : valueFormatter(
              parseFloat(
                assetClassSelected.coinValueUSD *
                  assetClassSelected?._24hrchange *
                  0.01
              ),
              "USD"
            );
      case "equity":
        return decimalValidation(
          assetClassSelected.coinValue,
          assetClassSelected.coinSymbol
        )
          ? parseFloat(assetClassSelected.coinValue).toFixed(6)
          : valueFormatter(
              assetClassSelected.coinValue,
              assetClassSelected.coinSymbol
            );
      case "hold":
        let sub = !assetClassSelected.withdrawal_balance
          ? assetClassSelected.coinValue
          : assetClassSelected.withdrawal_balance;
        let hold = Math.abs(assetClassSelected.coinValue - sub);
        return valueFormatter(hold, assetClassSelected.coinSymbol);
      case "withdraw":
        return valueFormatter(
          !assetClassSelected.withdrawal_balance
            ? assetClassSelected.coinValue
            : assetClassSelected.withdrawal_balance,
          assetClassSelected.coinSymbol
        );
    }
  };
  React.useEffect(() => {
    setAnalyticsSection(true);
    return () => {
      setAnalyticsSection(false);
    };
  }, []);
  return (
    <div className="controller-analytics">
      <div className="analytics-dashboard">
        <div className="analytics-dashboard-left">
          <div
            onCopy={() =>
              setCopiedOverlay({
                status: true,
                title: "Equity",
                data: getValue("equity") + " " + assetClassSelected.coinSymbol,
              })
            }
            className="equity-section"
          >
            <p>Equity</p>
            <h1>
              <span className="span-value">
                {getValue("equity")}
                <span className="span-symbol">
                  {assetClassSelected.coinSymbol}
                </span>
              </span>
            </h1>
          </div>
          <div className="liquidity-other-section">
            {Constant.analyticsBalance.slice(0, 3).map((obj, i) => (
              <div
                onCopy={() =>
                  setCopiedOverlay({
                    status: true,
                    title: obj.name,
                    data:
                      (!getValue(obj.id) ? "0.00%" : getValue(obj.id) + " ") +
                      (!i
                        ? conversionConfig.enable
                          ? conversionConfig.coin
                          : "USD"
                        : assetClassSelected.coinSymbol),
                  })
                }
                key={obj.keyId}
              >
                <p>{obj.name}</p>
                <h4>
                  <span
                    style={
                      getValue(obj.id).toString().length > 9
                        ? { fontSize: "22px" }
                        : {}
                    }
                    className="span-value"
                  >
                    {!getValue(obj.id) ? "0.00%" : getValue(obj.id)}
                    <span
                      style={
                        getValue(obj.id).toString().length > 8
                          ? { fontSize: "9px" }
                          : {}
                      }
                      className="span-symbol"
                    >
                      {!i
                        ? conversionConfig.enable
                          ? conversionConfig.coin
                          : "USD"
                        : assetClassSelected.coinSymbol}
                    </span>
                  </span>
                </h4>
              </div>
            ))}
          </div>
          <div className="_24hr-section">
            {Constant.analyticsBalance.slice(3, 5).map((obj, i) => (
              <div
                onCopy={() =>
                  setCopiedOverlay({
                    status: true,
                    title: obj.name,
                    data:
                      getValue(obj.id) +
                      " " +
                      (!i
                        ? ""
                        : conversionConfig.enable
                        ? conversionConfig.coin
                        : "USD"),
                  })
                }
                className={
                  parseFloat(assetClassSelected._24hrchange) < 0
                    ? "negative"
                    : "positive"
                }
                key={obj.keyId}
              >
                <p>{obj.name}</p>
                <h4>
                  <span className="span-value">
                    {getValue(obj.id)}
                    <span className="span-symbol">
                      {!i
                        ? ""
                        : conversionConfig.enable
                        ? conversionConfig.coin
                        : "USD"}
                    </span>
                  </span>
                </h4>
              </div>
            ))}
          </div>
        </div>
        <div className="analytics-dashboard-right">
          <div className="analytics-right-wrapper">
            <div
              className={
                !loading && !analyticsData?.bankers
                  ? "d-none"
                  : "analytics-right-header"
              }
            >
              <span>
                Trade {assetClassSelected.coinName} Instantly On These Exchanges
              </span>
            </div>
            <div
              className={`analytics-right-body ${
                !loading && !analyticsData?.bankers ? "h-100" : ""
              }`}
            >
              {!loading && !analyticsData?.bankers ? (
                <div className="h-100 w-100 d-flex justify-content-center align-items-center">
                  <p className="text-center">
                    Unfortunately None Of The Exchanges On The Accounting Tool
                    Terminal Support {assetClassSelected.coinName}
                  </p>
                </div>
              ) : (
                (loading
                  ? [1, 1, 2, 2]
                  : !analyticsData?.bankers
                  ? []
                  : [...analyticsData?.bankers]
                ).map((obj) => (
                  <div
                    className={`analytics-right-row ${
                      loading ? "loading" : ""
                    }`}
                  >
                    {loading ? (
                      ""
                    ) : (
                      <AnalyticsValue
                        image={analyticsData?.bankerData?.[obj].icons.image1}
                        name={obj}
                        symbol="USD"
                        value={valueFormatter(
                          analyticsData?.bankerData?.[obj]?.[
                            assetClassSelected.coinSymbol
                          ]?.buy?.avg,
                          assetClassSelected.coinSymbol
                        )}
                        positive={
                          analyticsData?.bankerData?.[obj]?.[
                            assetClassSelected.coinSymbol
                          ]?.buy?.avg >
                          analyticsData?.bankerData?.[obj]?.[
                            assetClassSelected.coinSymbol
                          ]?.sell?.avg
                        }
                      />
                    )}
                  </div>
                ))
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});
export default ControllerAnalytics;
