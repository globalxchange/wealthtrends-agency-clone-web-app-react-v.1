import React, { useContext, useState } from 'react'
import './navbar.style.scss'
import Constant from '../../../../json/constant'
import { Agency } from '../../../Context/Context';
import SwitchComponent from '../../../common-components/switch/Switch';
import Images from '../../../../assets/a-exporter';
import PortfolioAI from './portfolio-ai/PortfolioAI';
import PortalModal from '../../../common-components/PortalModal/PortalModal';
import AssetClasses from './AssetClasses';
export default function Navbar() {
    const agency = useContext(Agency);
    const navbarScroll = React.useRef();
    const [leftScroll, setLeftScroll] = useState(false);
    const [hoveredId, setHoveredId] = React.useState('')
    const [rightScroll, setRightScroll] = useState(true);
    const { assetSelected, setAssetSelected, handleAnalytics, currentApp, setAssetClassSelected, explainNavbarTerms,
        assetClassSelected, setSecondGate, setCheckedId, checkedId, currentUserDetails, setExplainNavbarTerm, collapseSidebar,
        setCollapseSidebar, netWorthController, setDifferentiator, setUserChatEnable, setGoToSettings } = agency;
    const [trigger, setTrigger] = useState(false);


    const moreInfo = {
        id2: {
            para: `Clicking Here Will Pull Up All The Crypto Assets Supported By ${currentApp.app_name}. You Can Transact & ManageYour Holdings In Each Cryptocurrency By Clicking On One Of The Assets.`,
            title: "Crypto Portfolio"
        },
        id1: {
            para: `Clicking Here Will Pull Up All The Fiat Assets Supported By ${currentApp.app_name}. You Can Transact & Manage Your Holdings In Each Fiat Currency By Clicking On One Of The Assets.`,
            title: "Fiat Portfolio"
        },
        id3: {
            para: "Here You Will Find All The Earnings Vaults For Both Fiat And Cryptocurrency. These Are Vaults Specifically Designed To Collect Your Daily Interest Payments ",
            title: "MoneyMarket Vaults"
        },
        id4: {
            para: "",
            title: ""
        },
    }
    const handleClick = (obj) => {
        // alert(obj.id)
        if (checkedId === obj.id) {
            setSecondGate(false)
            setAssetSelected(null);
            setAssetClassSelected(null);
            handleAnalytics('')
        }
        else {
            if (assetClassSelected !== null) {
                setAssetSelected(obj)
                setSecondGate(true)
                setCheckedId(obj.id)

            } else {
                setSecondGate(true)
                setCheckedId(obj.id)

            }
            // setAssetSelected(obj)
        }
    }
    const checkPosition = (num) => {
        if (num !== 0) {
            setLeftScroll(true);
            if (num >= navbarScroll.current.scrollHeight - 40) {
                setRightScroll(false);
            }
        } else if (num <= 0) {
            setLeftScroll(false)
            setRightScroll(true)
        }
    }
    const handleWheel = (e) => {
        checkPosition(navbarScroll.current.scrollLeft)
        navbarScroll.current.scroll({ top: 0, left: navbarScroll.current.scrollLeft + e.deltaY * 2, behavior: 'smooth' })
    }
    const handleLeftScroll = () => {
        navbarScroll.current.scroll({ top: 0, left: 0, behavior: 'smooth' });
        checkPosition(0)
    }
    const handleRightScroll = () => {
        navbarScroll.current.scroll({ top: 0, left: navbarScroll.current.offsetWidth, behavior: 'smooth' });
        checkPosition(navbarScroll.current.offsetWidth)
    }
    return (
        <div style={netWorthController === null ? {} : { opacity: 0.35 }} className="content-navbar">
            <div className="title-wrapper">
                <div className="title-wrapper-pic">
                    <img onError={e => e.target.src = Images.face} onClick={() => { setUserChatEnable(true); setDifferentiator("profile"); setGoToSettings(true); setCollapseSidebar(true) }} src={!currentUserDetails?.profile_img ? Images.face : currentUserDetails.profile_img} />
                    <p>Open Settings</p>
                </div>
                <div className="title-wrapper-text">
                    <h3>{currentUserDetails?.name}</h3>
                    <span>@{currentUserDetails.username}</span>
                </div>
            </div>
            <div className={`navbar-right ${assetSelected === null?"":"extra-width" }`}>
                {assetSelected === null ? <PortfolioAI /> : <AssetClasses />}
            </div>
        </div>
    )
}