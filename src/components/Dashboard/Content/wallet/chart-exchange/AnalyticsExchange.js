import React from 'react'
import { getAnalyticsValue } from '../../../../../services/getAPIs';
import AnalyticsValue from '../../../../common-components/analytics-values/AnalyticsValue';
import { Agency } from '../../../../Context/Context'

export default function AnalyticsExchange() {
    const agency = React.useContext(Agency);
    const { assetClassSelected, valueFormatter } = agency;
    const [analyticsData, setAnalyticsData] = React.useState();
    const [loading, setLoading] = React.useState(true);

    React.useEffect(() => {
        setLoading(true)
        handleAnalyticsData()
    }, [assetClassSelected])
    const handleAnalyticsData = async () => {
        try {
            let res = await getAnalyticsValue("vault", assetClassSelected.coinSymbol);
            setLoading(false)
            setAnalyticsData(res.data);
        } catch (e) {
            setLoading(false)
        }
    }
    return (
        <div className="analytics-exchange">
            <div className="analytics-exchange-header">
                <span>Trade {assetClassSelected.coinName} Instantly On These Exchanges</span>
            </div>
            <div className="analytics-exchange-body">
                {
                    !loading && !analyticsData?.bankers ?
                        <div className="h-100 w-100 d-flex justify-content-center align-items-center">
                            <p className="text-center">Unfortunately None Of The Exchanges On The Accounting Tool Terminal Support {assetClassSelected.coinName}</p>
                        </div>
                        :
                        (loading ? [1, 1, 2, 2] : !analyticsData?.bankers ? [] : [...analyticsData?.bankers]).map(obj =>
                            <div className={`analytics-right-row ${loading ? "loading" : ""}`}>
                                {
                                    loading ?
                                        '' :
                                        <AnalyticsValue
                                            image={analyticsData?.bankerData?.[obj].icons.image1}
                                            name={obj}
                                            symbol="USD"
                                            value={
                                                valueFormatter(analyticsData?.bankerData?.[obj]?.[assetClassSelected.coinSymbol]?.buy?.avg, assetClassSelected.coinSymbol)

                                            }
                                            positive={analyticsData?.bankerData?.[obj]?.[assetClassSelected.coinSymbol]?.buy?.avg > analyticsData?.bankerData?.[obj]?.[assetClassSelected.coinSymbol]?.sell?.avg}

                                        />

                                }
                            </div>
                        )
                }


            </div>

        </div>
    )
}
