import React from 'react'
import { Agency } from '../../../../Context/Context';
import { buyingCoinsList } from '../../../../../services/getAPIs';
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import Constant from '../../../../../json/constant';

export default function PairsComponent() {
    const agency = React.useContext(Agency);
    const { assetClassSelected, setTradePairObj, setMainTabs,
        setTradeState,setAssetSelected,setSecondGate,setCollapseSidebar,
        setSkipTradeStep,tradePairObj } = agency;
    const [currencyList, setCurrencyList] = React.useState([]);
    const [loading, setLoading] = React.useState(true);
    const [searchTerm, setSearchTerm] = React.useState('')

    const setUpCurrencyList = async () => {
        setLoading(true)
        let res = await buyingCoinsList(assetClassSelected?.coinSymbol)
        setCurrencyList(res.data.pathData?.to_currency);
        setLoading(false)

    }
    const handleClick = (obj) =>{
        setSkipTradeStep(2);
        setTradePairObj({
            ...tradePairObj,
            quote: {_id: assetClassSelected.coinSymbol, count: 0, coin_metadata: {...assetClassSelected}},
            base: {...obj}
        });
        setMainTabs("accounts"); setTradeState(true);
        
        setAssetSelected(Constant.navbarList[0]);
        setSecondGate(true); setCollapseSidebar(false);
    }

    React.useEffect(() => {
        if (!assetClassSelected?.coinSymbol) return
        setUpCurrencyList()
    }, [assetClassSelected?.coinSymbol])
    return (
        loading ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />
            </div>
            :
            <div className="pairs-component">
                <h5>Convert {assetClassSelected.coinName} Into Any Of The Assets</h5>
                <div className="pairs-input">
                    <input value={searchTerm} onChange={e => setSearchTerm(e.target.value.toLowerCase())} placeholder={`Search Currencies`} />

                </div>
                <div className="coin-list-to-pair">
                    {
                        currencyList.filter(x=>{return x.coin_metadata?.coinSymbol.toLowerCase().startsWith(searchTerm) || x.coin_metadata?.coinName.toLowerCase().startsWith(searchTerm)})
                        .map(obj => <div onClick={()=>handleClick(obj)}>
                            <img height="20px" src={obj.coin_metadata.coinImage} />
                            <h6>{obj.coin_metadata.coinName}</h6>
                        </div>)
                    }

                </div>

            </div>
    )
}
