import React, { Component, useContext, useEffect, useState } from 'react'
import { createChart } from 'lightweight-charts';
import axios from "axios";
import {
    LineChart, Line, XAxis, ResponsiveContainer, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
let ws;
export default function AnalyticsChart() {
    const [wsOpen, setWsOpen] = useState(false);
    const chartRef = React.useRef()
    const chartContainerRef = React.useRef();
    const [loading, setLoading] = React.useState(true)
    const chartSeriesRef = React.useRef()
    const [coinPrice, setCoinPrice] = React.useState(0)
    const chartCanvasRef = React.useRef()
    const [chartData, setChartData] = React.useState([])
    const chartDataRef = React.useRef()
    const [chartDimensions, setChartDimensions] = useState({
        width: 0,
        height: 0,
    });
    const [wsResp, setWsResp] = useState({
        asks: [
            [0, 0],
            [0, 0],
            [0, 0],
            [0, 0],
            [0, 0],
        ],
        bids: [
            [0, 0],
            [0, 0],
            [0, 0],
            [0, 0],
            [0, 0],
        ]
    });

    const [largest, setLargest] = useState([0, 0]);
    const [conversion, setConversion] = useState(1);
    const [sums, setSums] = useState({ asks: 0, bids: 0 });
    const [unSubscribe, setUnSubscribe] = useState({
        method: 'UNSUBSCRIBE',
        params: [`btcusdt@depth5@1000ms`],
        id: 1,
    });

    const createAreaChart = () => {
        chartRef.current = createChart(chartCanvasRef.current, {
            width: chartDimensions.width,
            height: chartDimensions.height,
        });

        chartRef.current.applyOptions({
            layout: {
                backgroundColor: '#ffffff',
                textColor: 'rgba(255, 255, 255, 0.9)',
            },
            grid: {
                vertLines: {
                    visible: false,
                    color: '#334158',
                },
                horzLines: {
                    visible: false,
                    color: '#334158',
                },
            },
            priceScale: {
                borderVisible: false,
                borderColor: '#485c7b',
                position: 'none',
            },
            timeScale: {
                borderVisible: false,
                borderColor: '#485c7b',
                visible: false,
                timeVisible: false,
                secondsVisible: false,
            },
            crosshair: {
                vertLine: {
                    visible: false,
                    labelVisible: false,
                },
                horzLine: {
                    visible: false,
                    labelVisible: false,
                },
                mode: 1,
            },
        });
    };
    useEffect(() => {
        if (chartRef.current) {
            if (chartSeriesRef.current) {
                chartRef.current.removeSeries(chartSeriesRef.current);
            }

            chartSeriesRef.current = chartRef.current.addCandlestickSeries({
                upColor: 'rgba(255, 144, 0, 1)',
                downColor: '#000',
                borderDownColor: 'rgba(255, 144, 0, 1)',
                borderUpColor: 'rgba(255, 144, 0, 1)',
                wickDownColor: 'rgba(255, 144, 0, 1)',
                wickUpColor: 'rgba(255, 144, 0, 1)',
            });
            getBinanceData()

            chartSeriesRef.current.applyOptions({
                upColor: '#2A2E39',
                downColor: '#186AB4',
                borderUpColor: '#2A2E39',
                borderDownColor: '#186AB4',
                wickUpColor: '#2A2E39',
                wickDownColor: '#186AB4',
                priceLineVisible: false,
            });
        }
    }, [chartRef.current]);
    const dimensionsHandler = () => {
        chartRef.current.resize(
            chartContainerRef.current.clientWidth,
            chartContainerRef.current.clientHeight ,

        )
        // setChartDimensions({
        //     width: chartContainerRef.current.clientWidth,
        //     height: chartContainerRef.current.clientHeight - 30,
        // });
    };
    useEffect(() => {
        if (!chartRef.current || !chartContainerRef.current) return;
        dimensionsHandler()
    }, [chartContainerRef.current?.clientWidth])
    useEffect(() => {
        createAreaChart();
        // workaround to fix chart width
        setTimeout(() => chartRef.current.timeScale().fitContent(), 100);
        return () => { };
    }, []);

    // const terminal = useContext(IcedTerminal);
    useEffect(() => {
        axios.get(`https://comms.globalxchange.com/coin/getCmcPrices?convert=USD`)
            .then(res => {
                setConversion(res.data.btc_price);
            })
    }, []);
    useEffect(() => {
        ws = new WebSocket(
            `wss://stream.binance.com:9443/ws/btcusdt@depth5@1000ms`
        );
        ws.onopen = () => {
            setWsOpen(true);
        };
        ws.onmessage = (event) => {
            const response = JSON.parse(event.data);
            if (response.result === null)
                return;
            setWsResp(response);
            let arr = response.bids.map((obj, i) => {
                return { uv: response.bids[i][1], pv: response.asks[i][1] }
            })
            // setChartData([...arr])
            chartDataRef.current = [...arr]
        };
        ws.onclose = () => {
            ws.close();
        };
        return () => {
            ws.close();
        };

    }, []);
    useEffect(() => {
        try {
            if (wsOpen) {
                const subscribe = {
                    method: 'SUBSCRIBE',
                    params: [`btcusdt@depth5@1000ms`],
                    id: 1,
                };
                ws.send(JSON.stringify(unSubscribe));
                ws.send(JSON.stringify(subscribe));
                setUnSubscribe({
                    method: 'UNSUBSCRIBE',
                    params: [`btcusdt@depth5@1000ms`],
                    id: 1,
                });
            }

        } catch (error) {

        }
    }, [wsOpen]);

    useEffect(() => {
    }, [wsResp])
    useEffect(() => {
        window.addEventListener('resize', dimensionsHandler);
        dimensionsHandler();
        return () => {
            window.removeEventListener('resize', dimensionsHandler);
        };
    }, []);
    const getBinanceData = () => {
        setLoading(true)
        axios.get(`https://api.binance.com/api/v3/klines?symbol=BTCUSDT&interval=1m&limit=80`)
            .then(res => {
                const dataSet = res.data || [];

                const formattedDataSet = [];

                if (dataSet) {
                    dataSet.forEach(item => {
                        const [openTime, open, high, low, close] = item;

                        setCoinPrice(open || 0);

                        const dataObj = {
                            time: openTime,
                            open,
                            high,
                            low,
                            close,
                        };

                        formattedDataSet.push(dataObj);
                    });
                }
                chartSeriesRef.current.setData(formattedDataSet);
                chartRef.current.timeScale().fitContent();
                // console.log("CHARTTTT DATA",formattedDataSet )

            })
        setLoading(false)
    }
    return (
        <div className="analytics-chart flex-fill" ref={chartContainerRef}>
            <div className={loading ? "chart-overlay" : "d-none"}>
                <LoadingAnimation />
            </div>
            <div ref={chartCanvasRef}>

            </div>

        </div>
    )
}
