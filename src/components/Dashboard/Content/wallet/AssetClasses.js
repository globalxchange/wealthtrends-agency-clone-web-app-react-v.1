import React from 'react'
import './assets-classes.style.scss'
import Constant from '../../../../json/constant';
import { Agency } from '../../../Context/Context';
export default function AssetClasses() {
    const agency = React.useContext(Agency);
    const { setAssetSelected, assetSelected } = agency;
    const handleClick = (obj) => {
        if (assetSelected?.keyId === obj.keyId) {
            // setAssetSelected(null);
        }
        else {
            setAssetSelected(obj)
        }
    }
    return (
        <div className="asset-classes-main">
            {
                Constant.navbarList.slice(0, 5).map(obj =>
                    <button
                    className={assetSelected.keyId === obj.keyId?"selected-asset":""}
                    onClick={() => handleClick(obj)}><img src={obj.icon} /></button>
                )
            }
        </div>
    )
}
