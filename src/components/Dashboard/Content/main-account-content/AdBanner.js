import React from "react";
import { useHistory } from "react-router-dom";
import "./ad-banner.style.scss";
import Images from "../../../../assets/a-exporter";
import {
  getLearnOptions,
  fetchNavbarId,
  getAllAppPresent,
  getAllContentUnderNavbar,
  getAllAds,
  getBannerAds,
} from "../../../../services/getAPIs";
import { Agency } from "../../../Context/Context";
export default function AdBanner() {
  const history = useHistory();
  const [theList, setTheList] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  const agency = React.useContext(Agency);
  const {
    setPlayVideo,
    setReadArticle,
    setLearnVideo,
    moreOnApp,
    currentApp,
  } = agency;

  const setUpTheList = async () => {
    let res;
    try {
      let app = localStorage.getItem("appCode");
      res = await getBannerAds(app, moreOnApp.created_by, "ads" + app);
      if (
        res.data.status &&
        (res.data.data.articles.length !== 0 ||
          res.data.data.videos.length !== 0)
      ) {
        setTheList([...res.data.data.videos, ...res.data.data.articles]);
        setLoading(false);
      } else {
        res = await getBannerAds("VSA", "shorupan@gmail.com", "adsVSA");
        if (res.data.status) {
          setTheList([...res.data.data.videos, ...res.data.data.articles]);
          setLoading(false);
        }
      }
    } catch (error) {
      setTheList([]);
      setLoading(false);
    }
    //VERY IMPORTANT
    // let res = await getLearnOptions(localStorage.getItem("appCode"));
    // let temp = res.data.data;
    // let tempArray = []
    // let mainTempArray = []
    // for (let i = 0; i < temp.length; i++) {
    //     let res = await fetchNavbarId(temp[i]?.fxa_app_id);
    //     let f = res.data.data.find(obj => { return obj.navTitle === "Home Page" });
    //     if (!f) { }
    //     else {
    //         tempArray = [...tempArray, f]
    //     }
    // }
    // for (let k = 0; k < tempArray.length; k++) {
    //     let res = await getAllContentUnderNavbar(tempArray[k]._id);

    //     console.log("temp array pre pre", res.data.data)
    //     mainTempArray = [...mainTempArray, ...res.data.data.videos, ...res.data.data.articles];
    // }
    // setTheList([...mainTempArray])
    // setLoading(false);
    // console.log("temp array", mainTempArray)
    //VERY IMPORTANT
  };
  const handleClick = (obj) => {
    if (!obj.video) {

      history.push(`/learn/article/${obj._id}`);
    } else {
      history.push(`/learn/video/${obj.video}`);

    }
  };
  React.useEffect(() => {
    if (!moreOnApp) return;
    setUpTheList();
  }, [moreOnApp]);
  return loading ? (
    <div className="ad-banner-loading">
      {[1, 1, 1, 1, 1, 1, 1, 1, 1, 1].map((obj) => (
        <div />
      ))}
    </div>
  ) : (
    <div className="ad-banner-main">
      {theList.map((obj) => (
        <div onClick={() => handleClick(obj)} className="single-banner">
          <div className="single-banner-left">
            <h6>{obj.title}</h6>
            <h6>
              {!obj.attachment?.tag1?.icon || !obj.attachment?.tag2?.icon
                ? ""
                : ["tag1", "tag2"].map((x) => (
                    <button>
                      <img height="20px" src={obj?.attachment?.[x]?.icon} />{" "}
                      {obj?.attachment?.[x]?.name}
                    </button>
                  ))}
            </h6>
          </div>
          <div className="single-banner-right">
            <img src={!obj.icon ? obj.image : obj.icon} />
          </div>
        </div>
      ))}
    </div>
  );
}
