import React from "react";
import "./user-portfolio.style.scss";
import { getEarningsDetails } from "../../../../services/getAPIs";
import Images from "../../../../assets/a-exporter";
import { Agency } from "../../../Context/Context";
import nextId from "react-id-generator";

export default function UserPortfolio() {
  const slider = React.useRef();
  const agency = React.useContext(Agency);
  const {
    collapseSidebar,
    conversionConfig,
    setCopiedOverlay,
    setDifferentiator,
    setCollapseSidebar,
    valueFormatter,
    earningDetails,
    setUserChatEnable,
    setUpdatePortfolio,
    currencyImageList,
  } = agency;
  // const [mainLoading, setMainLoading] = React.useState(true);
  const [firstCard, setFirstCard] = React.useState(0);

  const onLeftClick = () => {
    if (firstCard === 0) {
      setFirstCard(balanceList.length - 1);
      return;
    }
    setFirstCard(firstCard - 1);
  };

  const onRightClick = () => {
    if (firstCard === balanceList.length - 1) {
      setFirstCard(0);
      return;
    }
    setFirstCard(firstCard + 1);
  };

  const calculatePercentage = (num) => {
    if (num - firstCard < 0) {
      return balanceList.length - (firstCard - num);
    } else {
      return num - firstCard;
    }
  };

  return !earningDetails ? (
    <div className="earning-details-loading">
      {[1, 1, 1, 1, 1, 1, 1, 1, 1, 1].map((obj) => (
        <div />
      ))}
    </div>
  ) : (
    <div className="user-portfolio-balance">
      <div
        // onWheel={handleWheel}
        ref={slider}
        className="trade-balance-wrapper"
      >
        {balanceList.map((obj) => (
          <div
            onCopy={() =>
              setCopiedOverlay({
                status: true,
                title: obj.name,
                data: valueFormatter(
                  earningDetails?.[obj.link] * conversionConfig.rate,
                  conversionConfig.coin
                ),
              })
            }
            // style={{ borderLeft: `15px solid ${obj.color}` }}
            style={{
              borderLeft: `15px solid ${obj.color}`,
              transform: `translateX(${calculatePercentage(obj.num) * 120}%)`,
            }}
          >
            <span>{obj.name}</span>
            <h4>
              {valueFormatter(
                earningDetails?.[obj.link] * conversionConfig.rate,
                conversionConfig.coin
              )}
            </h4>
            <p
              onClick={() => {
                setCollapseSidebar(true);
                setDifferentiator("calculations");
                setUserChatEnable(true);
                setUpdatePortfolio(true);
              }}
            >
              Calculations
            </p>
            <button>
              <img src={currencyImageList[conversionConfig.coin]} />
              {conversionConfig.coin}
            </button>
          </div>
        ))}
        <div />
      </div>
      <div
        onClick={() => onLeftClick()}
        className={true ? "coin-move-left" : "d-none"}
      >
        <img src={Images.leftArrowNew} />
      </div>
      <div
        style={collapseSidebar ? { right: "0%" } : {}}
        onClick={() => onRightClick()}
        className={true ? "coin-move-right" : "d-none"}
      >
        <img src={Images.rightArrowNew} />
      </div>
    </div>
  );
}
const balanceList = [
  {
    keyId: nextId(),
    name: "Total Holdings",
    id: "total_holdings",
    link: "total_holdings",
    color: "#F7931A",
    num: 0,
  },
  {
    keyId: nextId(),
    name: "Liquid Holdings",
    id: "liquid_holdings",
    link: "liquid_holdings",
    color: "#3485C0",
    num: 1,
  },
  {
    keyId: nextId(),
    name: "Total Liquid Earnings",
    id: "totalLiquidEarnings",
    link: "totalLiquidEarnings",
    color: "#2EA654",
    num: 2,
  },
  {
    keyId: nextId(),
    name: "Liquid Earnings Balance",
    id: "liquidInterest",
    link: "liquidInterest",
    color: "#F7931A",
    num: 3,
  },
  {
    keyId: nextId(),
    name: "Total Bond Earnings",
    id: "totalBondEarnings",
    link: "totalBondEarnings",
    color: "#6669B0",
    num: 4,
  },
  {
    keyId: nextId(),
    name: "Bond Earnings Balance",
    id: "totalBondEarnings",
    link: "totalBondEarnings",
    color: "#2EA654",
    num: 5,
  },
];
