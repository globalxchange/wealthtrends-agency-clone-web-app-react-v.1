import React from 'react'
import Navbar from '../wallet/Navbar'
import AssetSection from '../wallet/AssetSection'
import NetWorthController from '../wallet/NetWorthController'
import WalletContent from '../wallet/WalletContent'
import { Agency } from '../../../Context/Context'
import AdBanner from './AdBanner'
import TerminalsNavbar from '../wallet/TerminalsNavbar'
import TradeBalance from './TradeBalance'
import UserPortfolio from './UserPortfolio'
export default function MainAccountContent() {
    const agency = React.useContext(Agency);
    const { setMainTabs, assetSelected, portfolioActive, tradeState } = agency;

    React.useEffect(() => {
        setMainTabs("accounts");
        return ()=>{
            setMainTabs("")
        }
    }, [])
    return (
        <div className="w-100 h-100">
            {tradeState ? <TerminalsNavbar /> : <Navbar />}
            {/* <UserPortfolio /> */}
            {assetSelected === null ? tradeState ? <TradeBalance /> : portfolioActive ? <UserPortfolio /> : <AdBanner /> : <AssetSection />}
            <NetWorthController />
            <WalletContent />
        </div>
    )
}
