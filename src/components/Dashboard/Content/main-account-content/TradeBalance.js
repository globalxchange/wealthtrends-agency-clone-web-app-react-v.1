import React from 'react'
import './trade-balance.style.scss'
import { Agency } from '../../../Context/Context';
import Images from '../../../../assets/a-exporter';
export default function TradeBalance() {
    const slider = React.useRef();
    const agency = React.useContext(Agency);
    const [currentList, setCurrentList] = React.useState([]);
    const [leftScroll, setLeftScroll] = React.useState(false);
    const [rightScroll, setRightScroll] = React.useState(true);
    const { tradeNavbarType, mainBalance,collapseSidebar, tradePairObj, valueFormatter } = agency;
    
    const setUpCurrentList = () => {
        switch (tradeNavbarType.id) {
            case "fiat": setCurrentList([...mainBalance.fiat]); break;
            case "crypto": setCurrentList([...mainBalance.crypto]); break;
            default: setCurrentList([])
        }
    }
    
    const checkPositions = (num) => {
        if (num > 100) {
            setLeftScroll(true)
            if (num >= slider.current.scrollWidth - slider.current.offsetWidth - 100) {
                setRightScroll(false);
            } else {
                setRightScroll(true);
            }
        } else {
            setLeftScroll(false)
        }

    }
    const handleWheel = (e) => {
        checkPositions(slider.current.scrollLeft)

        slider.current.scroll({ top: 0, left: slider.current.scrollLeft + ((e.deltaY / Math.abs(e.deltaY)) * (slider.current.offsetWidth * 0.5)), behavior: "smooth" })
    }
    const handleLeftScroll = () => {
        slider.current.scroll({ top: 0, left: slider.current.scrollLeft - (slider.current.offsetWidth * 0.75), behavior: "smooth" });
        checkPositions(slider.current.scrollLeft - slider.current.offsetWidth)
    }
    const handleRightScroll = () => {
        slider.current.scroll({ top: 0, left: slider.current.scrollLeft + (slider.current.offsetWidth * 0.75), behavior: "smooth" })
        checkPositions(slider.current.scrollLeft + slider.current.offsetWidth)
    }


    React.useEffect(() => {
        setUpCurrentList()
    }, [tradeNavbarType])
    return (
        <div className="trade-balance">
            <div onWheel={handleWheel} ref={slider} className="trade-balance-wrapper">
                {
                    currentList.map(obj => <div className={tradePairObj.quote?._id === obj.coinSymbol ? "selected-balance" : ""}>
                        <img height="20px" src={obj.coinImage} /><h6>
                            {valueFormatter(obj.coinValue, obj.coinSymbol)}
                            <p>{obj.coinName}</p></h6>
                    </div>
                    )
                }

            </div>
            <div onClick={() => handleLeftScroll()} className={leftScroll ? "coin-move-left" : "d-none"}>
                <img src={Images.leftArrowNew} />
            </div>
            <div style={collapseSidebar ? { right: '0%' } : {}} onClick={() => handleRightScroll()} className={rightScroll ? "coin-move-right" : "d-none"}>
                <img src={Images.rightArrowNew} />
            </div>

        </div>
    )
}
