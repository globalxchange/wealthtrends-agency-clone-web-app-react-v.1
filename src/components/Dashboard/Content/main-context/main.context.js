import React, {Component, createContext} from 'react';
import {getUserForChat, login, placeTrade,authenticate, vaultBalance} from "../../services/postAPIs";
import nextId from "react-id-generator";
import {
    cryptoUSD,
    exchangeData, getAllCoins,
    getAssetData, getAvailavlePairs,
    getForexCryptoList,
    getIcedList, getMarketData, getMarketValue,
    getUserDetails
} from "../../services/getAPIs";
import axios from "axios";
import {terminalCoins} from "../../services/postAPIs";
import {defaultCoin, fire, logo, logoBlack} from "../../images/terminal-images";
import {staticHeader, staticSubHeaderThree} from "../terminal/headersSubHeaders";

export const IcedTerminal = createContext();
export class IcedTerminalProvider extends Component{

    volume = '24Hr_Volume';
    state = {
        update1: false,
        update2: false,
        update3: false,
        update4: false,
        darkMode: false,
        loggedIn: false,
        notification: {status: '', message: ''},
        first: 0,
        value: '',
        terminalSidebar: false,
        extraState: false,
        exchangeList: [],
        exchangeListStatic: [],
        exchangeListUpdate: false,
        tabs: [{id :nextId() ,type:"Terminal", img: logoBlack}],
        selectedTab: {},
        size:{x1:[3,5,0,0], x2:[6,5,3,0], x3:[3,5,9,0], x4: [3,5,0,5], x5:[6,5,3,5], x6:[3,5,9,5]},
        layoutState: [
            {size: [9,5,0,0], component: ['b', 'chart']},
            {size: [3,5,9,0], component: ['a', 'binance']},
            {size: [3,5,0,5], component: ['d', 'liquid']},
            {size: [6,5,3,5], component: ['e', 'exchange']},
            {size: [3,5,9,5], component: ['c', 'convert']},
            // {size: [3,5,9,5], component: ['f', 'buy']},
        ],
        availablePairs : [],
        numbers : {Bitfinex: null, "Binance": null, "Kraken": null, "Okex": null},
        filterExchangeListState : [],
        addNewTab: [{type: false, app: "Home", k: "f"}],
        exchangeLength: 0,
        header: staticHeader[2],
        fullScreenLanding: false,
        exchangeDataTwo: [],
        subHeader: staticSubHeaderThree[0],
        hoveringOverChart: false,
        baseAsset: true,
        commonSubHeader:'Forex',
        terminalStage: 0,
        terminalSearchTerm: '',
        mainValueType: 'Net-Worth',
        mainValue: {liquid:0, passive: 0, managed: 0, total: 0, display: 0, type: 'Net-Worth'},
        loginSuccessFul: '',
        userDetails: {profile_img: '', name: ''},
        terminalCurrencyList: [],
        mainLoader: true,
        exchangeDataList: [],
        liquidTerminalList: [],
        liquidForexList: [],
        liquidCryptoList: [],
        LPMSums : {totalSum: 0,forex: 0, crypto: 0, token: 0, liquid: 0,iced: 0, earning: 0, indicies: 0,passive: 0, terminal: 0, futures: 0, funds: 0, managed:0},
        earningList: [],
        icedList: [],
        liquidListLoader: false,
        exchangeAverageValue: ['-', '-', '-'],
        exchangeTableLoader:false,
        apiCurrency: [{coin: '', image: '', exchanges: 0},{coin: '', image: '', exchanges: 0}],
        exchangeSearchTerm : '',
        exchangeDropDownStage: 0,
        progressNumber: 0,
        buySell: '',
        LPMHeader: "Liquid",
        selectAsset: {exchangeId:'Loading...', [this.volume]: 0,fullImage:'',fullImageBW: '',imageCurrency:'', percentChange: 0, price: 0, image: '', buyPrice: 0, sellPrice: 0},
        selectedDropDown: {coin: '', image: fire, name: ''},
        fromCurrency: {coinSymbol: ''},
        toCurrency:{coinSymbol: ''},
        terminalConversionRate: 0,
        chartSidebar: false,
        terminalInverseConverseRate: 0,
        terminalExecutionCurrency: '',
        selectedExchangeCurrency: 'BTC',
        terminalExecutionTotalValue: '0.00',
        terminalCurrencyObj: [{coin: 'BTC'}, {coin: 'USD'}],
        selectTerminalCurrency : 'USD',
        cryptoUSD: {BTC: 0, ETH: 0, USDT: 0},
        executionMarketLoading: false,
        executionSuccess : '',
        allCoins: [],
        changeChart: false,
        showChat: false,
        currentUserObj: {username: '', timestamp: '', email: ''},
        displayList: [],
        cardStyle: {
            zIndex: [1,2,3,4,3,2,1],
            scale: [0.6, 0.7334, 0.867, 1, 0.867, 0.7334, 0.6]
        },
        globalSearch: '',
        globalSearchDisplay: false,
        vaultHeaderCoin: {},
        streamTab: "Central",
        loginLoading: true,
        loginModal: false,
        theme: "light",
    };

    authenticateUser = async() =>{
        let res = await authenticate();
        if(res.data.status){
            this.setLoginModal(false);
        }else{
            this.setLoginModal(true);
        }
    }
    toggleTheme = () => {
      const theme = this.state.theme === 'dark' ? 'light' : 'dark';
      this.setState({ theme });
      document.documentElement.setAttribute("data-theme", theme);
    }
    setLoadingLoading = (val) =>{
        this.setState({loginLoading: val})
    }
    setLoginModal = (val) =>{
        this.setState({loginModal: val})
    }
    changeStreamTab = (val) =>{
        this.setState({streamTab: val})
    }
    setVaultHeaderCoin = (val) =>{
      this.setState({vaultHeaderCoin: {...val}})
    };
    setDarkMode = (val) =>{
      this.setState({darkMode: val});
    };
    setLoggedIn = (val) =>{
        this.setState({loggedIn: val})
    }
    getVaultBalance = ()=>{
        vaultBalance()
            .then(res =>{
                console.log("vault balance", res.data);
                this.setState({vaultBalance: res.data});
            })
    };
    getUserObj = () =>{
        getUserForChat()
            .then(res =>{
                console.log("res.data", res.data);
                this.setState({
                    currentUserObj: {
                        username: res.data.payload.username,
                        timestamp: res.data.payload.timestamp,
                        email: res.data.payload.email
                    }
                })
            });
    };
    setShowChat = (val) =>{
      this.setState({showChat: val})
    };
    setNumbers = (val) =>{
        this.setState({numbers: {...val}})
    };
    setFullScreenLanding = (val) =>{
      this.setState({fullScreenLanding: val})
    };
    changeNotification = (val) =>{
        this.setState({notification: val}, () => setTimeout(() => this.setState({
            notification: {
                status: '',
                message: ''
            }
        }), 2000));
    };
    filterExchangeList = () => {
        console.log("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF FEL");
        if(this.state.apiCurrency[0].coin === '' || this.state.apiCurrency[1].coin === '')
            return;

        let tempList = [];
        this.state.availablePairs.map(x => {
            if(x.pair === `${this.state.apiCurrency[0].coin}/${this.state.apiCurrency[1].coin}`){
                tempList = [...tempList, x.Exchanges]
            }
        });
        console.log('available pairs', tempList);
        this.setState({
            filterExchangeListState: [...tempList]},()=>{this.updateExchangeList(); console.log('welcome context', this.state.filterExchangeListState)});

    };
    setUpdate1 = () =>{
        this.setState({update1: this.state.update1 + 1})
    };
    setUpdate2 = () =>{
        this.setState({update2: this.state.update1 + 1})
    };
    setUpdate3 = () =>{
        this.setState({update3: this.state.update1 + 1})
    };
    setUpdate4 = () =>{
        this.setState({update4: this.state.update1 + 1})
    };
    getExchangeList = () =>{
      exchangeData()
          .then(res =>{
              let tempTwo = res.data.data.map(obj =>{
                  return {exchangeId: obj.formData.exchangeID, [this.volume]: 0, price: 0, percentChange: 0, image: obj.formData.image,buyPrice: 0, sellPrice: 0, fullImage: obj.formData.fullImage, fullImageBW: obj.formData.fullImageBW}
              });
              this.setState({exchangeList: [...tempTwo], exchangeListStatic: [...tempTwo]});
          })
    };
    setGlobalSearch = (val) =>{
        if(val !== ''){
            this.setState({globalSearchDisplay: true})
        }else{
            this.setState({globalSearchDisplay: false})
        }
        this.setState({globalSearch: val});
    };
    resetCardStyle = () =>{
        this.setState({cardStyle: {zIndex: [1,2,3,4,3,2,1], scale: [0.6, 0.7334, 0.867, 1, 0.867, 0.7334, 0.6]}})
    };
    changeCardStyle = (val) =>{
        switch (val) {
            case 0: this.setState({cardStyle: {zIndex: [7,6,5,4,3,2,1], scale: [1,0.933,0.8666,0.8,0.7332,0.662,0.6]}}); break;
            case 1: this.setState({cardStyle: {zIndex: [5,6,5,4,3,2,1], scale: [0.92,1,0.92,0.84,0.76,0.68,0.6]}}); break;
            case 2: this.setState({cardStyle: {zIndex: [3,4,5,4,3,2,1], scale: [0.8, 0.9, 1, 0.9, 0.8, 0.7, 0.6]}}); break;
            case 3: this.setState({cardStyle: {zIndex: [1,2,3,4,3,2,1], scale: [0.6, 0.7334, 0.867, 1, 0.867, 0.7334, 0.6]}}); break;
            case 4: this.setState({cardStyle: {zIndex: [1,2,3,4,5,4,3], scale: [0.6,0.7,0.8,0.9,1,0.9,0.8]}}); break;
            case 5: this.setState({cardStyle: {zIndex: [1,2,3,4,5,6,5], scale: [0.6,0.68,0.76,0.84,0.92,1,1,0.92]}}); break;
            case 6: this.setState({cardStyle: {zIndex: [1,2,3,4,5,6,7], scale: [0.6, 0.662,0.7332,0.8,0.8666,0.933,1]}}); break;
        }
    };
    setTabs = (value, flag) =>{
        const {tabs} = this.state;
        this.setState({tabs: value}, ()=>flag?this.setState({selectedTab: this.state.tabs[this.state.tabs.length -1]}): console.log())
    };
    setDisplayList = (val) =>{
      this.setState({displayList: val})
    };
    setTerminalSidebar = (val) =>{
      this.setState({terminalSidebar: val})
    };
    setSelectedTab = (val, reset) => {
        this.setState({selectedTab: val});
        if(reset){
            this.setState({selectedTab: this.state.tabs[0]});
        }
    };
    getAvailablePairsList = () =>{
        getAvailavlePairs()
            .then(res =>{
                this.setState({availablePairs: res.data})
            });
    };
    componentDidMount() {
        // this.authenticateUser();
        cryptoUSD()
            .then(res =>{
                this.setState({cryptoUSD: res.data})
            });
    }
    setExtraState = () =>{
        this.setState({extraState: !this.state.extraState})
    }
    setChangeChart = () =>{
      this.setState({changeChart: !this.state.changeChart})
    };
    addingNewTab = (val, id, app) =>{
      this.setState({addNewTab: val}, ()=>
      {this.setState({layoutState: [...this.state.layoutState,{size:[3,5,0,10],component: [id, app]}]})})
    };
    removeTab = (id) =>{
        let a = this.state.layoutState.filter(data =>{
            if(data.component[0] !== id){
                return true
            }
            else{
                return false
            }
        });
        this.setState({layoutState: a});
    };
    changeLayout = (val) =>{
        this.setState({layoutState: val}, ()=>this.setChangeChart())
    };
    changeSize = (val) =>{
      this.setState({size: val})
    };
    getAllCoinFunction = () =>{
        let arr = [];
        getAllCoins()
            .then(res =>{
                res.data.forEach((item, i) => {
                    let b = Object.keys(item);
                    arr = [...arr, {coin: b[0], exchanges: res.data[i][b[0]].Exchanges}];
                });
                this.setState({allCoins: arr});
            });
    };
    setValue = (val) =>{
        this.setState({value: val})
    };
    getExchangeLength = () =>{
        getMarketData()
            .then((res )=>{
                this.setState({exchangeLength: res.data.mData.length});
                localStorage.setItem('exchangeLength', res.data.mData.length);
            })
    };
    placeMarketTrade = (exchangeId, paidId1, paidId2, value, buySell) =>{
        this.setState({executionMarketLoading: true});
        let bs = buySell;
        // let pI2 = this.state.selectAsset.exchangeId === "Binance" && this.state.apiCurrency[1].coin === 'USD'?'USDT': 'USD';
        placeTrade(exchangeId, paidId1,paidId2, value, bs )
            .then(res =>{
                if (res.data.status === true) {
                    this.setLiquidTerminalList();
                    this.setState({
                        executionMarketLoading: false,
                        value: '',
                        terminalExecutionTotalValue: 0
                    }, () => {
                        this.changeNotification({status: true, message: res.data.message});
                    });
                } else {
                    this.setState({
                        value: '',
                        terminalExecutionTotalValue: 0
                    }, () => {
                        this.setState({executionMarketLoading: false});
                        this.changeNotification({status: false, message: "Failed"});
                        this.changeNotification({status: false, message: "Failed"});
                    });
                }
            })
    };
    onImageError = (e) =>{
        e.target.src = defaultCoin;
    };
    setBaseAsset = (val) =>{
      this.setState({baseAsset: val})
    };
    setTerminalStage = (val) =>{
        this.setState({terminalStage: val})
    };
    setTerminalSearchTerm = (val) =>{

        this.setState({terminalSearchTerm: val=== ''?'': val})
    };
    setHeader = (val) =>{
    this.setState({header: val})
    };
    setSubHeader = (val) =>{
        this.setState({subHeader: val})
    };
    setTerminalCurrencyObj = (val) =>{
        this.setState({terminalCurrencyObj: [{coin: val}, this.state.terminalCurrencyObj[1]]})
    };
    setCommonSubHeader = (val) =>{
        this.setState({commonSubHeader: val})
    };
    setBuySell = (val) =>{
        this.setState({buySell: val})
    };
    setTerminalExecutionValue = val =>{
      this.setState({terminalExecutionTotalValue: val})
    };
    setSelectTerminalCurrency = (val) =>{
        this.setState({selectTerminalCurrency: val})
    };
    setTerminalExecutionCurrency = (val) =>{
      this.setState({terminalExecutionCurrency: val})
    };
    setChartSidebar = (val,c) =>{
        if(c === 'open-sidebar' && this.state.chartSidebar){
        }else{

            this.setState({chartSidebar: val})
        }
    };

    resetLPMSums = () =>{
        this.setState({LPMSums: {totalSum: 0,forex: 0, crypto: 0, token: 0, liquid: 0,iced: 0, earning: 0, indicies: 0,passive: 0, terminal: 0, futures: 0, funds: 0, managed:0}})
    };
    setUserDetails = () =>{
        getUserDetails()
            .then(res=>{
                this.setState({userDetails: res.data.user});
                localStorage.setItem('userDetails',JSON.stringify(res.data.user))
            })
    };
    setLoginSuccessFul = (val) =>{
        this.setState({loginSuccessFul: val})
    };
    changeHoveringOverChart = (val) =>{
        this.setState({hoveringOverChart: val})
    };
    login = (obj) =>{
        this.setState({loginSuccessFul: 'loading'});
        localStorage.setItem('exchangeLength','0');

        login(obj.email, obj.password)
            .then(res =>{
                if(res.data.status){
                    localStorage.setItem('userEmail', obj.email);
                    localStorage.setItem('deviceKey', res.data.device_key);
                    localStorage.setItem('accessToken', res.data.accessToken);
                    localStorage.setItem('idToken', res.data.idToken);
                    localStorage.setItem('refreshToken', res.data.refreshToken);
                    this.setState({loginSuccessFul: 'success', loggedIn: true});
                    // this.getExchangeLength();
                    this.getUserObj();
                    this.getCurrencyList();
                    this.setUserDetails();
                    // this.getAllCoinFunction();
                    // this.getVaultBalance()
                    this.setLoginModal(false)
                }
                else{
                    this.setState({loginSuccessFul: 'failed', loggedIn: false});
                }
            })
    };

    setLPMHeader = (val) =>{
        this.setState({LPMHeader:val})
    }
    setMainLoader = (val) =>{
        this.setState({mainLoader: val})
    }
    getCurrencyList = () =>{
        this.setState({mainLoader: true});
        getForexCryptoList()
            .then((res)=>{
                this.setState({terminalCurrencyList: res.data.coins},
                    ()=>{this.setState(
                        {
                            fromCurrency: this.state.terminalCurrencyList[0],
                            toCurrency: this.state.terminalCurrencyList[0]
                        },()=>{this.setState({mainLoader: false});this.setTerminalConversionRate()}
                    )
                    })
            })
    };


    setTerminalConversionRate = () =>{
        if(!this.state.fromCurrency || !this.state.toCurrency)
            return;
        if(this.state.fromCurrency.asset_type === 'Crypto' || this.state.toCurrency.asset_type === 'Crypto' ){
            if(this.state.fromCurrency.asset_type === 'Crypto'){
                axios.get(`https://comms.globalxchange.com/coin/getCmcPrices?convert=${this.state.toCurrency.coinSymbol}`)
                    .then(res =>{
                        const str = `${this.state.fromCurrency.coinSymbol.toLowerCase()}_price`;
                        this.setState({terminalConversionRate: res.data[str], terminalInverseConverseRate: (1 / res.data[str])});
                    })
            }else{
                axios.get(`https://comms.globalxchange.com/coin/getCmcPrices?convert=${this.state.fromCurrency.coinSymbol}`)
                    .then(res=>{
                        const str = `${this.state.toCurrency.coinSymbol.toLowerCase()}_price`;
                        this.setState({terminalConversionRate: 1 / res.data[str], terminalInverseConverseRate: (res.data[str])
                        })
                    })
            }
        }else{
            axios.get(`https://comms.globalxchange.com/forex/convert?buy=${this.state.toCurrency.coinSymbol}&from=${this.state.fromCurrency.coinSymbol}`)
                .then(res =>{
                    const str = `${this.state.toCurrency.coinSymbol.toLowerCase()}_${this.state.fromCurrency.coinSymbol.toLowerCase()}`;
                    const inverse_str = `${this.state.fromCurrency.coinSymbol.toLowerCase()}_${this.state.toCurrency.coinSymbol.toLowerCase()}`;
                    this.setState({terminalConversionRate: res.data[inverse_str], terminalInverseConverseRate: res.data[str] })
                })
        }
    };
    setLiquidTerminalList = () =>{
        this.setState({liquidListLoader: true});
        let forex = this.state.terminalCurrencyList.filter((x)=>{return x.asset_type === 'Fiat'});
        let crypto = this.state.terminalCurrencyList.filter((x)=>{return x.asset_type === 'Crypto'});
        let cryptoCoins = crypto.slice(0,3);
        let tokens = crypto.slice(3,4);

        let forexSum = 0, crypyoSum = 0, tkns = 0;
        forex.map((x)=> forexSum = forexSum + x.coinValueUSD);
        cryptoCoins.map((x)=> crypyoSum = crypyoSum + x.coinValueUSD);
        tokens.map((x)=> tkns = tkns + x.coinValueUSD);
        let total = forexSum + crypyoSum + tkns
        this.setState({
            liquidForexList: forex,
            liquidCryptoList: cryptoCoins,
            liquidTokensList: tokens,
            LPMSums: {...this.state.LPMSums, forex: forexSum, crypto: crypyoSum,tokens: tkns, liquid: total, totalSum : this.state.LPMSums.totalSum + total}
        },()=>console.log('LPMSums', this.state.LPMSums));
        getIcedList()
            .then((res)=> {
                let icedSum = 0;
                res.data.coins_data.map((x) => {
                    res.data.coins_data.map((x)=>
                        icedSum = x.usdValue
                    );
                    this.setState({
                        earningList: res.data.coins_data,
                        icedList: [...this.state.icedList,
                            {...x, balance: x.balance_3, usdValue: x.usdValue_3, period: 3,coinSymbol: x.coinSymbol + ` (${3}M)`},
                            {...x, balance: x.balance_6, usdValue: x.usdValue_6, period: 6,coinSymbol: x.coinSymbol + ` (${6}M)`}
                        ]
                    })
                });

                this.setState({
                    LPMSums:
                        {
                            ...this.state.LPMSums,
                            iced: icedSum,
                            earning: icedSum,
                            passive: icedSum,
                            totalSum: this.state.LPMSums.totalSum + icedSum
                        }
                },()=>console.log('LPMSums2', this.state.LPMSums));
            });
        terminalCoins()
            .then(res =>{
                if(res.data.status === false) {
                    this.setState({liquidTerminalList: []},()=>this.setState({liquidListLoader: false}));
                    return;
                }
                let sum = 0;
                res.data.map((x)=>sum = sum + x.USDeqv);
                this.setState({
                        liquidTerminalList: res.data,
                        LPMSums: {...this.state.LPMSums, terminal: sum, managed: sum, totalSum: this.state.LPMSums.totalSum + sum }
                    }
                    , () => {this.setState(
                        {
                            mainValue:
                                {
                                    type: 'Net-Worth',
                                    liquid: this.state.LPMSums.liquid,
                                    passive: this.state.LPMSums.passive,
                                    managed: this.state.LPMSums.managed,
                                    total: this.state.LPMSums.totalSum,
                                    display: this.state.LPMSums.totalSum
                                }

                    ,liquidListLoader: false})});
            });

    };

    // {liquid:0, passive: 0, managed: 0, total: 0, display: 0}
    setSelectAsset = (val)=>{
        this.setState({selectAsset: val})
    };
    setSelectedDropDown = (val) =>{
        this.setState({
            selectedDropDown: val
        })
    };
    setExchangeStage = (val) =>{
        this.setState({exchangeDropDownStage: val})
    };
    setExchangeSearchTerm = (val) =>{
        this.setState({exchangeSearchTerm: val})
    };
    getCoinSymbol = (coin)=>{
        switch (coin) {
            case 'USD':
            case 'CAD':
            case 'AUD':
            case 'MXN':
            case 'ARS':
            case 'COP':
                return '$';
            case 'INR': return '₹';
            case 'GBP': return '£';
            case 'EUR': return '€';
            case 'CNY': return '¥';
            case 'JPY': return '¥';
            case 'AED': return 'د.إ';
            default: return '';



        }
    }
    setAPICurrency= (val) =>{
        this.setState({apiCurrency: val});
    };
    setFirst = (val) =>{
        this.setState({first: val})
    };
    formatter = new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "USD",
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
    });
    getExchangeTableData = ()=>{
        this.setTerminalStage(5);
        let a ;
        a = setInterval(()=>{
            if(this.state.progressNumber < 95)
                this.setState({progressNumber: this.state.progressNumber + 2})
        },400);

        if(this.state.apiCurrency[0].coin === '')
            return;
        this.setState({exchangeTableLoader: true,exchangeAverageValue: ['-', '-', '-']});
        getAssetData(this.state.apiCurrency[0].coin, this.state.apiCurrency[1].coin)
            .then(res =>{
                if(res.data.length === 0){
                    this.setState({
                        exchangeTableLoader:false,
                        exchangeDataList: [],
                        exchangeAverageValue: ['-', '-', '-'],
                        exchangeSearchTerm : '',
                        exchangeDropDownStage: 0,
                    });
                    return;
                }
                let len = res.data.length;
                let sum = 0, vol=0, intr = 0;
                res.data.map((x)=>{
                    sum = sum + x.price;
                    vol = vol + x['24Hr_Volume'];
                    intr = intr + x.percentChange;
                });
                sum = sum/len;
                // vol = vol;
                intr = intr/len;
                this.setTerminalCurrencyObj(this.state.apiCurrency[0].coin);
                this.setState({
                        selectAsset: res.data[0],
                        progressNumber: 100,
                        exchangeDataList: [...res.data],
                        exchangeAverageValue: [this.formatter.format(sum), this.formatter.format(vol), intr.toFixed(2)+'%']
                    },

                    ()=>{
                        clearInterval(a);
                        this.setTerminalStage(6);
                        this.setState(
                            {
                                progressNumber: 0,
                                exchangeTableLoader: false,
                                exchangeSearchTerm : '',
                                exchangeDropDownStage: 0,
                            })});
            })
    };
    valueFormatter = (coin, value) =>{
        if(isNaN(value))
            return ;

        let cryptoCoins = ["ETH", "BTC", "LTC", "SEF","BCH","XMR","XRP"];
        if(value === ''){
            if(cryptoCoins.includes(coin.toUpperCase())){
                return 0.0000 ;
            }else{
                return  0.00
            }
        }else{
            if(cryptoCoins.includes(coin.toUpperCase())){
                let tempValue = parseFloat(value).toFixed(9).toString();
                return value.toString().includes('.')? tempValue.substring(0,6): tempValue.substring(0,5);
            }else{
                return  parseFloat(value).toFixed(2)
            }
        }
    };
    changeMainValue = (type) =>{
        switch (type) {
            case "Liquid": this.setState( {mainValue: {liquid:this.state.LPMSums.forex, passive: this.state.LPMSums.crypto, managed: this.state.LPMSums.tokens, total: this.state.LPMSums.liquid, display: this.state.LPMSums.liquid, type: type}});break;
            case "Passive": this.setState({mainValue: {liquid:this.state.LPMSums.iced, passive: this.state.LPMSums.earning, managed: this.state.LPMSums.indicies, total: this.state.LPMSums.passive, display: this.state.LPMSums.passive, type: type}});break;
            case "Managed": this.setState( {mainValue: {liquid:this.state.LPMSums.terminal, passive: this.state.LPMSums.futures, managed: this.state.LPMSums.funds, total: this.state.LPMSums.managed, display: this.state.LPMSums.managed, type: type}});break;
            default: this.setState( {mainValue: {liquid:this.state.LPMSums.liquid, passive: this.state.LPMSums.passive, managed: this.state.LPMSums.managed, total: this.state.LPMSums.totalSum, display: this.state.LPMSums.totalSum, type: 'Net-Worth'}});break;
        }
    };
    changeMainValueType = (type) =>{
        this.setState({mainValueType: type},()=>this.changeMainValue(type))
    };
    resetExchangeAverageValue = () =>{
        this.setState({exchangeAverageValue: ['-', '-', '-']})
    };
    updateExchangeAverageValue = (val) =>{
      this.setState({exchangeAverageValue: val})
    };
    updateExchangeList = () =>{
    
        if(!this.state.filterExchangeListState[0])
            return;

        let temp = this.state.exchangeListStatic.filter(x => {;return this.state.filterExchangeListState[0].includes(x.exchangeId)});
        this.setState({exchangeList: [...temp]})
    }
    fillExchangeList = () =>{

            let exchnages = ["Bitfinex", "Binance", "Kraken", "Okex","ZB"];

            getMarketValue('Bitfinex', this.state.apiCurrency[0].coin, this.state.apiCurrency[1].coin)
                .then(res =>{
                    let temp = this.state.exchangeList.map((resTwo, i) =>{
                        if(resTwo.exchangeId === 'Bitfinex'){
                            this.state.exchangeList[i] = {...this.state.exchangeList[i], [this.volume]: res.data.volume, price: res.data.lastPrice, percentChange: 0};
                            this.setState({exchangeListUpdate: !this.state.exchangeListUpdate})
                        }
                    });
                });

            getMarketValue('', this.state.apiCurrency[0].coin, this.state.apiCurrency[1].coin)
                .then(res =>{
                    let temp = this.state.exchangeList.map((resTwo,i) =>{
                        if(resTwo.exchangeId === 'Binance'){
                            this.state.exchangeList[i] = {...this.state.exchangeList[i], [this.volume]: res.data.volume, price: res.data.lastPrice, percentChange: res.data.priceChangePercent};
                            this.setState({exchangeListUpdate: !this.state.exchangeListUpdate})
                        }
                    });
                });

            getMarketValue('Kraken', this.state.apiCurrency[0].coin, this.state.apiCurrency[1].coin)
                .then(res =>{
                    let temp = this.state.exchangeList.map((resTwo,i) =>{
                        if(resTwo.exchangeId === 'Kraken'){
                            this.state.exchangeList[i] = {...this.state.exchangeList[i], [this.volume]: res.data.volume, price: res.data.lastPrice, percentChange: 0};
                            this.setState({exchangeListUpdate: !this.state.exchangeListUpdate})
                        }
                    });
                });

            getMarketValue('Okex', this.state.apiCurrency[0].coin, this.state.apiCurrency[1].coin)
                .then(res =>{
                    let temp = this.state.exchangeList.map((resTwo,i) =>{
                        if(resTwo.exchangeId === 'Okex'){
                            this.state.exchangeList[i] = {...this.state.exchangeList[i], [this.volume]: res.data.volume, price: res.data.lastPrice, percentChange: 0};
                            this.setState({exchangeListUpdate: !this.state.exchangeListUpdate})
                        }
                    });
                });
        getMarketValue('Bitmax', this.state.apiCurrency[0].coin, this.state.apiCurrency[1].coin)
            .then(res => {
                let temp = this.state.exchangeList.map((resTwo, i) => {
                    if (resTwo.exchangeId === 'Bitmax') {
                        this.state.exchangeList[i] = {
                            ...this.state.exchangeList[i],
                            [this.volume]: res.data.volume,
                            price: res.data.lastPrice,
                            percentChange: 0
                        };
                        this.setState({exchangeListUpdate: !this.state.exchangeListUpdate})
                    }
                });
            });
        getMarketValue('ZB', this.state.apiCurrency[0].coin, this.state.apiCurrency[1].coin)
            .then(res => {
                let temp = this.state.exchangeList.map((resTwo, i) => {
                    if (resTwo.exchangeId === 'ZB') {
                        this.state.exchangeList[i] = {
                            ...this.state.exchangeList[i],
                            [this.volume]: res.data.volume,
                            price: res.data.lastPrice,
                            percentChange: 0
                        };
                        this.setState({exchangeListUpdate: !this.state.exchangeListUpdate})
                    }
                });
            });



    };
    getExchangeTableDataTwo =async () =>{
        this.setTerminalStage(5);
        let arr = [];
                await exchangeData()
                    .then(resTwo=>{
                        resTwo.data.data.map(resThree =>{
                            arr = [...arr, {exchangeId: resThree.formData.exchangeID,[this.volume]: 0, price: 0, percentChange: 0, image: resThree.formData.image, buyPrice: 0, sellPrice: 0,fullImage: resThree.formData.fullImage, fullImageBW: resThree.formData.fullImageBW}]
                        });
                        this.setState({
                            exchangeDataTwo: arr,
                            exchangeAverageValue: [0, 0, 0],

                        }, () => {
                            this.setTerminalStage(6);
                        });
                    })
            // })
    };
    handleKeyPress = (e) =>{
        if(e.which < 46 || e.which > 57){
            e.preventDefault();
        }
    };
    refreshStates = () =>{
        this.setState({
            update1: false,
            update2: false,
            update3: false,
            update4: false,
            darkMode: false,
            notification: {status: '', message: ''},
            first: 0,
            value: '',
            terminalSidebar: false,
            extraState: false,
            exchangeList: [],
            exchangeListStatic: [],
            exchangeListUpdate: false,
            tabs: [{id :nextId() ,type:"Terminal", img: logoBlack}],
            selectedTab: {},
            size:{x1:[3,5,0,0], x2:[6,5,3,0], x3:[3,5,9,0], x4: [3,5,0,5], x5:[6,5,3,5], x6:[3,5,9,5]},
            layoutState: [
                {size: [9,5,0,0], component: ['b', 'chart']},
                {size: [3,5,9,0], component: ['a', 'binance']},
                {size: [3,5,0,5], component: ['d', 'liquid']},
                {size: [6,5,3,5], component: ['e', 'exchange']},
                {size: [3,5,9,5], component: ['c', 'convert']},
                // {size: [3,5,9,5], component: ['f', 'buy']},
            ],
            availablePairs : [],
            numbers : {Bitfinex: null, "Binance": null, "Kraken": null, "Okex": null},
            filterExchangeListState : [],
            addNewTab: [{type: false, app: "Home", k: "f"}],
            exchangeLength: 0,
            header: staticHeader[2],
            fullScreenLanding: false,
            exchangeDataTwo: [],
            subHeader: staticSubHeaderThree[0],
            hoveringOverChart: false,
            baseAsset: true,
            commonSubHeader:'Forex',
            terminalStage: 0,
            terminalSearchTerm: '',
            mainValueType: 'Net-Worth',
            mainValue: {liquid:0, passive: 0, managed: 0, total: 0, display: 0, type: 'Net-Worth'},
            loginSuccessFul: '',
            userDetails: {profile_img: '', name: ''},
            terminalCurrencyList: [],
            mainLoader: true,
            exchangeDataList: [],
            liquidTerminalList: [],
            liquidForexList: [],
            liquidCryptoList: [],
            LPMSums : {totalSum: 0,forex: 0, crypto: 0, token: 0, liquid: 0,iced: 0, earning: 0, indicies: 0,passive: 0, terminal: 0, futures: 0, funds: 0, managed:0},
            earningList: [],
            icedList: [],
            liquidListLoader: false,
            exchangeAverageValue: ['-', '-', '-'],
            exchangeTableLoader:false,
            apiCurrency: [{coin: '', image: '', exchanges: 0},{coin: '', image: '', exchanges: 0}],
            exchangeSearchTerm : '',
            exchangeDropDownStage: 0,
            progressNumber: 0,
            buySell: '',
            LPMHeader: "Liquid",
            selectAsset: {exchangeId:'Loading...', [this.volume]: 0,fullImage:'',fullImageBW: '',imageCurrency:'', percentChange: 0, price: 0, image: '', buyPrice: 0, sellPrice: 0},
            selectedDropDown: {coin: '', image: fire, name: ''},
            fromCurrency: {coinSymbol: ''},
            toCurrency:{coinSymbol: ''},
            terminalConversionRate: 0,
            chartSidebar: false,
            terminalInverseConverseRate: 0,
            terminalExecutionCurrency: '',
            selectedExchangeCurrency: 'BTC',
            terminalExecutionTotalValue: '0.00',
            terminalCurrencyObj: [{coin: 'BTC'}, {coin: 'USD'}],
            selectTerminalCurrency : 'USD',
            cryptoUSD: {BTC: 0, ETH: 0, USDT: 0},
            executionMarketLoading: false,
            executionSuccess : '',
            allCoins: [],
            changeChart: false,
            showChat: false,
            currentUserObj: {username: '', timestamp: '', email: ''},
            displayList: [],
            cardStyle: {
                zIndex: [1,2,3,4,3,2,1],
                scale: [0.6, 0.7334, 0.867, 1, 0.867, 0.7334, 0.6]
            },
            globalSearch: '',
            globalSearchDisplay: false,
            vaultHeaderCoin: {}
        });
    };
    render() {
        return (
            <IcedTerminal.Provider
                value={{
                    ...this.state,
                    login: this.login,
                    setUpdate1: this.setUpdate1,
                    setUpdate2: this.setUpdate2,
                    setUpdate3: this.setUpdate3,
                    setUpdate4: this.setUpdate4,
                    setNumbers: this.setNumbers,
                    refreshStates: this.refreshStates,
                    changeHoveringOverChart: this.changeHoveringOverChart,
                    changeMainValueType: this.changeMainValueType,
                    changeMainValue: this.changeMainValue,
                    valueFormatter: this.valueFormatter,
                    getCoinSymbol: this.getCoinSymbol,
                    setBaseAsset: this.setBaseAsset,
                    setSelectTerminalCurrency: this.setSelectTerminalCurrency,
                    setUserDetails: this.setUserDetails,
                    getCurrencyList: this.getCurrencyList,
                    resetExchangeAverageValue: this.resetExchangeAverageValue,
                    setLPMHeader: this.setLPMHeader,
                    getUserObj: this.getUserObj,
                    setTabs: this.setTabs,
                    getExchangeList: this.getExchangeList,
                    getExchangeTableDataTwo: this.getExchangeTableDataTwo,
                    setTerminalSearchTerm: this.setTerminalSearchTerm,
                    resetLPMSums: this.resetLPMSums,
                    setGlobalSearch: this.setGlobalSearch,
                    setLiquidTerminalList: this.setLiquidTerminalList,
                    setSelectAsset: this.setSelectAsset,
                    setAPICurrency: this.setAPICurrency,
                    setLoginSuccessFul: this.setLoginSuccessFul,
                    setFirst: this.setFirst,
                    setTerminalSidebar: this.setTerminalSidebar,
                    getExchangeTableData: this.getExchangeTableData,
                    formatter: this.formatter,
                    setChartSidebar: this.setChartSidebar,
                    setSelectedDropDown: this.setSelectedDropDown,
                    setExchangeStage: this.setExchangeStage,
                    setExchangeSearchTerm: this.setExchangeSearchTerm,
                    setTerminalExecutionCurrency: this.setTerminalExecutionCurrency,
                    setTerminalExecutionValue: this.setTerminalExecutionValue,
                    setBuySell: this.setBuySell,
                    setCommonSubHeader: this.setCommonSubHeader,
                    setTerminalCurrencyObj: this.setTerminalCurrencyObj,
                    setHeader: this.setHeader,
                    setSubHeader: this.setSubHeader,
                    setTerminalStage: this.setTerminalStage,
                    placeMarketTrade: this.placeMarketTrade,
                    setValue: this.setValue,
                    onImageError: this.onImageError,
                    setVaultHeaderCoin: this.setVaultHeaderCoin,
                    handleKeyPress: this.handleKeyPress,
                    getAllCoinFunction: this.getAllCoinFunction,
                    changeSize: this.changeSize,
                    setFullScreenLanding: this.setFullScreenLanding,
                    changeNotification : this.changeNotification,
                    changeLayout:this.changeLayout,
                    addingNewTab: this.addingNewTab,
                    removeTab: this.removeTab,
                    setChangeChart: this.setChangeChart,
                    updateExchangeAverageValue: this.updateExchangeAverageValue,
                    setDisplayList: this.setDisplayList,
                    setExtraState: this.setExtraState,
                    setSelectedTab: this.setSelectedTab,
                    changeCardStyle: this.changeCardStyle,
                    resetCardStyle: this.resetCardStyle,
                    getAvailablePairsList: this.getAvailablePairsList,
                    fillExchangeList: this.fillExchangeList,
                    filterExchangeList: this.filterExchangeList,
                    updateExchangeList: this.updateExchangeList,
                    setDarkMode: this.setDarkMode,
                    getVaultBalance: this.getVaultBalance,
                    setShowChat: this.setShowChat,
                    changeStreamTab : this.changeStreamTab,
                    setLoggedIn: this.setLoggedIn,
                    setLoginModal: this.setLoginModal,
                    setLoadingLoading: this.setLoadingLoading,
                    setMainLoader: this.setMainLoader,
                    toggleTheme: this.toggleTheme
                }}
            >
                {this.props.children}
            </IcedTerminal.Provider>
        )
    }

}
