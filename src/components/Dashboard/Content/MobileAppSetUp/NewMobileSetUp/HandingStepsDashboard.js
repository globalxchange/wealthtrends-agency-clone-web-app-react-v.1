import React,{useState}from 'react'
import mobileScreen from '../../../../../assets/mobileScreen.png'
import Ios from '../../../../../assets/apple.png'
import AndriodImg from '../../../../../assets/and.png'
// logoApp

import { Agency } from '../../../../Context/Context'
import StepOne from './StepOne'
import StepTwo from './StepTwo'
import StepThree from './StepThree'
import MessageFunction from './Step4'
// mobileScreen
export default function HandingStepsDashboard({colocode,loadingfinal,newuseraddfunctionss,handlephone,handleEmail,setroute,steponeFunction,messageFucntion,getphonenumber,dropshowfunction,countrycodeFunction,dropDown,countrycode,imgaStore,getEmailText,AppName,Andriod,ioslink,Dataapp,itemname,collectionofData}) {


    const AnalyStep = () => {
        switch (setroute) {
          case "one":
            return (
           <StepOne
Dataapp={Dataapp}
itemname={itemname}
collectionofData={collectionofData}
steponeFunction={steponeFunction}
colocode={colocode}
/> 
            
          
            )
            case "two":
                return (
 <StepTwo
getEmailText={getEmailText}
handleEmail={handleEmail}
colocode={colocode}
/> 
            
              
                )
                case "three":
                    return (
                        <StepThree
                        countrycode={countrycode}
                        dropDown={dropDown}
                        countrycodeFunction={countrycodeFunction}
                        dropshowfunction={dropshowfunction}
                        getphonenumber={getphonenumber}
                        handlephone={handlephone}
                        colocode={colocode}
                        />        
                  
                    )
                    case "four":
                        return (
                            <MessageFunction
                            newuseraddfunctionss={newuseraddfunctionss}
                            messageFucntion={messageFucntion}
                            loadingfinal={loadingfinal}
                            colocode={colocode}
                            />     
                      
                        )
        }
        }

    const agency = React.useContext(Agency);
    const { appDownloadConfig, currentApp } = agency;
    return (
        <div className="handlingStepdahboard">
            <div className="row m-0">
                <div className="col-md-6 col-xl-7 leftSidecontainer">
                <div className="">
   <div className="d-flex">
            <img style={{height: "5rem"}} src={imgaStore} alt=""/>
            <div>
            <div className="sub_img_Text_div">
                    <h1 style={{color:colocode}}>{AppName}</h1>
                    <p style={{color:colocode}}>Built For {appDownloadConfig?"Android":"IOS"}</p>
                </div>
            </div>
            </div>
            <div className="mainversiondiv">
            <span style={{color:colocode}}>View Update History</span>
        </div>
            <div className="label-ain-div-custom ">
<label className="invite-_button-cutom"style={{background:colocode}}>Invite</label>
{
    appDownloadConfig?
<label className="ioslable-cutom" onClick={()=>window.open(Andriod,'_blank')}> 
  <div className="applethe">
        <img className="spaceingdt" src={AndriodImg} alt=""/>
        <div>
            <p className="downloadon" style={{color:colocode}}>Download On</p>
            <p className="appstore" style={{color:colocode}}>Google Play</p>
        </div>
        </div></label>
    :
    <label className="ioslable-cutom"style={{color:colocode,borderColor:colocode}} onClick={()=>window.open(ioslink,'_blank')}>
        
        <div className="applethe">
        <img className="spaceingdt" src={Ios} alt=""/>
        <div>
            <p className="downloadon" style={{color:colocode}}>Download On</p>
            <p className="appstore"style={{color:colocode}}>App Store</p>
        </div>
        </div>
        </label>
}

</div>
            <div className="">
           
               <div className="d-flex">
               <div className="">
<img src={mobileScreen} alt=""/>
</div>

               </div>
            </div>

   
        </div>
                </div>
                <div className="col-md-6 col-xl-5 pr-0 borderRighHandsideDiv pl-0">
{/* <StepOne
Dataapp={Dataapp}
itemname={itemname}
collectionofData={collectionofData}
/> */}

{/* <StepTwo
getEmailText={getEmailText}
/> */}
{/* <StepThree
countrycode={countrycode}
dropDown={dropDown}
countrycodeFunction={countrycodeFunction}
dropshowfunction={dropshowfunction}
getphonenumber={getphonenumber}
/> */}

{/* <MessageFunction
messageFucntion={messageFucntion}
/> */}

{AnalyStep()}
                </div>

            </div>
            
        </div>
    )
}
