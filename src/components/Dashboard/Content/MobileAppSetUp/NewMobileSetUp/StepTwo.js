import React from 'react'

export default function StepTwo({Dataapp,colocode,handleEmail,collectionofData,getEmailText}) {
    return (
        <div className="mobileSetOne">

     
        <div className="moblisectionbe">
        
            <div>
                <h2 className="stepheader" style={{color:colocode}}>Step 2</h2>
                <p className="steptext" style={{color:colocode}}>Enter Recipient’s Email</p>
            </div>
            <div className="divcontainerapp">
    <input  style={{color:colocode,borderColor:colocode}} className="inputEmail" placeholder="Enter Email" onChange={getEmailText}/>
            </div>

            
        </div>

<div className="bottomSectionnext" onClick={handleEmail} style={{background:colocode}}> 
<p>Next</p>
</div>

</div>
    )
}
