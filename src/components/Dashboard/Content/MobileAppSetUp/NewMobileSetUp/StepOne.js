import React from 'react'
import mrk from  '../../../../../assets/mrk.png'

export default function StepOne({Dataapp,itemname,steponeFunction,collectionofData,colocode}) {
    return (
        <div className="mobileSetOne">

     
        <div className="moblisectionbe">
        
            <div>
                <h2 className="stepheader" style={{color:colocode}}>Step 1</h2>
                
                <p className="steptext" style={{color:colocode}}>Which App Do You Want To Send To Them?</p>
            </div>
            <div className="divcontainerapp">
                {
                    Dataapp.map(item=>
                        {
                            return(
                                <div className={itemname===item.app_code?"d-flex selectlinkdatatrue":"d-flex selectlinkdata"} onClick={()=>collectionofData(item)}>
                                   <div className="d-flex" style={{alignItems:"center"}}>

                             
                                   
                                    <img style={{height: "5rem"}} src={item.app_icon} alt=""/>
                                    <div className="sub_img_Text_div">
                            <h2 style={{color:colocode}}>{item.app_name}</h2>
                            <h4 style={{color:colocode}}>{item.short_description}</h4>
                                    </div>
                              
                                </div>
                                {itemname===item.app_code?
                                <img src={mrk} alt=""/>:null}
                                </div>
                            )
                        })
                }
            </div>

            
        </div>

<div className="bottomSectionnext" style={{background:colocode}} onClick={steponeFunction}> 
<p>Next</p>
</div>

</div>
    )
}
