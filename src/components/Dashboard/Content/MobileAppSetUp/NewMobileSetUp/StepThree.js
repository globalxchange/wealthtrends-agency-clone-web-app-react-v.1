import React from 'react'
import Constant from '../../../../../json/constant';
export default function StepThree({handlephone,Dataapp,colocode,dropDown,dropshowfunction,getphonenumber,itemname,collectionofData,getEmailText,countrycode,countrycodeFunction}) {

    const [currentCode, setCurrentCode] = React.useState(Constant.countryCodes[0]);
 
    const [searchTerm, setSearchTerm] = React.useState('');
    const [valid, setValid] = React.useState(false);


    return (
        <div className="mobileSetOne">

     
        <div className="moblisectionbe">
        
            <div>
                <h2 className="stepheader" style={{color:colocode}}>Step 3</h2>
                <p className="steptext" style={{color:colocode}}>Enter Recipient’s Number</p>
            </div>
            <div className="divcontainerapp">
            <div className="country-code-dropdown">
                    <div className="">    
                        <input
                        style={{color:colocode,borderColor:colocode}}
                            onChange={(e) => setSearchTerm(e.target.value)}
                            autoFocus={true}
                            placeholder="+1"
                            value={countrycode}
                            className="countrycodeinputbox"
                            onClick={dropshowfunction}
                            readonly
                        />

<input
                          style={{color:colocode,borderColor:colocode}}
                            autoFocus={true}
                            placeholder="000-000-000"
                            className="countrycodetype"
                            onChange={getphonenumber}
                        />
                    </div>

{
    dropDown?
    <div className="dropdownbodystyling">
    <input 
    style={{color:colocode}}
            onChange={(e) => setSearchTerm(e.target.value)}
            autoFocus={true}
            placeholder="Search....."
            className="Search....."
            style={{width:"100%" ,border: "1px solid #8080804f"}}
        />
        {
            Constant.countryCodes.filter(obj => { return obj.name.toString().toLowerCase().startsWith(searchTerm.toString().toLowerCase()) })
                .map(obj =>
                    <h6 style={{color:colocode}} onClick={() => countrycodeFunction(obj.dial_code)}>
                        <span style={{color:colocode}}>{obj.name}</span><span>{obj.dial_code}</span>
                    </h6>
                )
        }
    </div>
:
null
}

                  



                </div>
            </div>

            
        </div>

<div className="bottomSectionnext"style={{background:colocode}} onClick={handlephone}> 
<p>Next</p>
</div>

</div>
    )
}
