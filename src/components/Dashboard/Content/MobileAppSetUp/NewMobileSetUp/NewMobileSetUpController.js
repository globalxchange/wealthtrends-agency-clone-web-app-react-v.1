import React,{useState} from 'react'
import NewmobileSetUp from './NewMobileSetUp'
import axios from "axios";
import HandlingStepDashboard from './HandingStepsDashboard'
import { Agency } from '../../../../Context/Context'
export default function NewMobileSetUpController() {
const [approute,setapprout]=useState("main")
const [imgaStore,setimgaStore]=useState()
const [AppName,setAppName]=useState("")
const [ioslink,setioslink]=useState("")
const [Andriod,setAndriod]=useState("")
const [Dataapp,setDataapp]=useState([])
const [itemname,setitemname]=useState("")
const [dropDown, setDropDown] = React.useState(false);
const [email, setEmail] = React.useState('');
const [phoneNumber,setphoneNumber]=useState(0)
const [countrycode,setcountrycode]=useState("+1")
const [message,setmessage]=useState("")
const [loadingfinal,setloadingfinal]=useState(false)
const [setroute,setsetrout]=useState("one")
const emailRegex = /([\w\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?/gm
const phoneRegex = /^(\d){9,12}$/;
const agency = React.useContext(Agency);
const { appDownloadConfig, currentApp } = agency;
const [colocode,setcolocode]=useState("")
/* METHOD: POST, URL: "https://comms.globalxchange.com/coin/vault/service/send/app/links/invite", BODY: { "userEmail": "mani@nvestbank.com", // SENDING USER EMAIL, MUST BE A GXuSER "app_code": "instacrypto", // APP CODE "email": "manichandra.teja@gmail.com", // RECIPIENT EMAIL "mobile": "+919515014406", // RECIPIENT PHONE "app_type": "android", // ios / android "custom_message": "hey Yo! Cool Man!" // custom Message set by User } */




const newuseraddfunctionss = () => {
    if(message!=="")
    {
        setloadingfinal(true)
    axios.post("https://comms.globalxchange.com/coin/vault/service/send/app/links/invite", {
        userEmail:localStorage.getItem("userEmail").toString(),
        app_code:itemname,
        email:email,
        mobile:`${countrycode}${phoneNumber}`,
        app_type: appDownloadConfig ?"android":"ios",
        custom_message:message
    })
           .then(async res => {
             console.log("jhgdgfdgdfg",res)
           if (res.data.status) {
         
            setapprout("main")
             setloadingfinal(false)
             }
           
           })
       
        }
        else
        {
alert("Fill The Field")
        }
   
     }
const handleEmail = () => {
    let check = emailRegex.test(email);
    if (check) {
        setsetrout("three")
    } else {

       alert("Invalid Gmail")
    }
}
const handlephone = () => {
    let check = phoneRegex.test(phoneNumber);
    if (check) {
        setsetrout("four")
    } else {

       alert("Invalid Number")
    }
}
const getEmailText = (e) => {
    setEmail(e.target.value)
}
const messageFucntion=async(e)=>{
    await setmessage(e.target.value)
    console.log("sdasdasd",e)
}
const getphonenumber=(e)=>
{
    setphoneNumber(e.target.value)
}
const newuseraddfunction = () => {
    let collect=localStorage.getItem("appCode")
    axios.get(`https://comms.globalxchange.com/gxb/apps/get?app_code=${localStorage.getItem("appCode")}`)
           .then(async res => {
               console.log("",res)
           if (res.data.status) {
              console.log("motf",res.data.apps[0])
             setimgaStore(res.data.apps[0].app_icon)
             setAppName(res.data.apps[0].app_name)
             setioslink(res.data.apps[0].ios_app_link)
             setAndriod(res.data.apps[0].android_app_link)
             setcolocode(res.data.apps[0].color_codes[0])
             if(collect==="broker_app" || collect==="instacrypto" )
             {
                newuseraddfunctiona()
            
             }
else
{
    setDataapp([res.data.apps[0]])

       


}
            
        }  
       })

   
       
       
      
   
     }

     const dropshowfunction=()=>
     {
        setDropDown(true)
     }
    const countrycodeFunction=(e)=>{
        setcountrycode(e)
        setDropDown(false)
     }

     const collectionofData=(e)=>{
        setitemname(e.app_code)
     }

     const steponeFunction=()=>
     {
         if(itemname!="")
         {
            setsetrout("two")
         }
         else
         {
             alert("Please Select The App")
         }
     }
     const newuseraddfunctiona = () => {

        axios.get(`https://comms.globalxchange.com/gxb/apps/get`)
               .then(async res => {
                  
               if (res.data.status) {
            
    
                 let users = res.data.apps.filter(obj => obj.app_code === "broker_app" || obj.app_code === "instacrypto")
                 setDataapp(users)
                 console.log("users",res.data.apps)
            }  
           })
    
       
           
           
          
       
         }

     React.useEffect(() => {
        newuseraddfunction()
     
    }, [])
const inventationSwtich=()=>
{
    setapprout("sub")
}
    const AnalyStep = () => {
        switch (approute) {
          case "main":
            return (
         
    
              <NewmobileSetUp
              imgaStore={imgaStore}
               AppName={AppName}
               ioslink={ioslink}
               Andriod={Andriod}
               inventationSwtich={inventationSwtich}
               colocode={colocode}
               />
            
          
            )
            case "sub":
                return (
                <HandlingStepDashboard
                  Dataapp={Dataapp}
                  imgaStore={imgaStore}
                  AppName={AppName}
                  ioslink={ioslink}
                  Andriod={Andriod}
                  Dataapp={Dataapp}
                  itemname={itemname}
                  collectionofData={collectionofData}
                  getEmailText={getEmailText}
                  countrycode={countrycode}
                  dropDown={dropDown}
                  countrycodeFunction={countrycodeFunction}
                  dropshowfunction={dropshowfunction}
                  getphonenumber={getphonenumber}
                  messageFucntion={messageFucntion}
                  steponeFunction={steponeFunction}
                  setroute={setroute}
                  handleEmail={handleEmail}
                  handlephone={handlephone}
                  newuseraddfunctionss={newuseraddfunctionss}
                  loadingfinal={loadingfinal}
                  colocode={colocode}
                />
        
              
                
              
                )
        }
        }
    return (
        <div>
            {AnalyStep()}
        </div>
    )
}
