import React from 'react'
import * as animationData from "./loader.json";
import Lottie from "react-lottie";
export default function StepFour({Dataapp,colocode,collectionofData,newuseraddfunctionss,messageFucntion,loadingfinal}) {
    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData.default,
        rendererSettings: {
          preserveAspectRatio: "xMidYMid slice"
        }
      };
    return (
        <div className={loadingfinal?"mobileSetOneloading":"mobileSetOne"}>

     


     {
         loadingfinal?
<Lottie
  options={defaultOptions}
width="200px"
height="200px"
/>

         :
         <>
         <div className="moblisectionbe">
        
         <div>
             <h2 className="stepheader" style={{color:colocode}}>Step 4</h2>
             <p className="steptext" style={{color:colocode}}>Type A Friendly Invitation</p>
         </div>
         <div className="divcontainerapp">
 <input style={{color:colocode,borderColor:colocode}}  className="inputEmail" type="text " placeholder="Ex.  Hey I Found This Really Cool App" onChange={messageFucntion}/>
         </div>

         
     </div>

<div className="bottomSectionnext" style={{background:colocode}} onClick={newuseraddfunctionss}> 
<p>Next</p>
</div>
</>
     }


</div>
    )
}
