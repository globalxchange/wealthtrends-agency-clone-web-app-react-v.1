import React from 'react'

export default function MASStepThree({setStep}) {

    React.useEffect(() => {
        let a = setTimeout(()=>{
            setStep(0); clearTimeout(a);
        },4000)
        
    }, [])
    return (
        <p className="step-three">Congrats. You Have Sent Out The BrokerApp</p>
    )
}
