import React from 'react'

export default function MASStepTwo({ setStep, SetCustomMessage, handleSubmit }) {
    return (
        <div className="sms-details">
            <h5>Anything You Want The SMS To Say? </h5>
            <input
                onChange={e => SetCustomMessage(e.target.value)}
                onKeyPress={(e) => e.which === 13 ? handleSubmit() : console.log()}
                placeholder="Type....."
                autoFocus={true}
            />
        </div>
    )
}
