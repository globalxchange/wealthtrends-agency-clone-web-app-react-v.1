import React from 'react'
import LoadingAnimation from '../../../../lotties/LoadingAnimation';
import { mobileAppProcess } from '../../../../services/postAPIs';
import { Agency } from '../../../Context/Context'
import MASStepOne from './MASStepOne';
import MASStepThree from './MASStepThree';
import MASStepTwo from './MASStepTwo';
import './mobile-app-set-up.style.scss'
export default function MobileAppSetUp() {
    const agency = React.useContext(Agency);
    const { appDownloadConfig, currentApp } = agency;
    const [step, setStep] = React.useState(0);
    const [mobile, setMobile] = React.useState('');
    const [customMessage, SetCustomMessage] = React.useState('');
    const selectStep = () => {
        switch (step) {
            case 0: return <MASStepOne setMobile={setMobile} android={appDownloadConfig} setStep={setStep} />;
            case 1: return <MASStepTwo handleSubmit={handleSubmit} SetCustomMessage={SetCustomMessage} setStep={setStep} />;
            case 2: return <div><LoadingAnimation setStep={setStep} type="login" size={{ height: 250, width: 250 }} /> </div>;
            case 3: return <MASStepThree setStep={setStep} />
        }
    }
    const handleSubmit = async () => {
        setStep(2);
        let tempObj = {
            userEmail: localStorage.getItem("userEmail").toString(),
            app_code: currentApp.app_code,
            email: localStorage.getItem("userEmail").toString(),
            mobile: mobile,
            app_type: appDownloadConfig ? "android" : "ios",
            custom_message: customMessage
        };

        let res;
        try {
            res = await mobileAppProcess(tempObj);
            if (res.data.status) {
                setStep(3);
            } else {
                setStep(0);
            }
        } catch (e) {
            console.error(e)
        }
    }
    React.useEffect(() => {
        setStep(0);
        setMobile('');
        SetCustomMessage('');
    }, [appDownloadConfig])
    console.log("sdsdad",appDownloadConfig)
    return (
        <div className="app-config-set-up">
            {selectStep()}
        </div>
    )
}
