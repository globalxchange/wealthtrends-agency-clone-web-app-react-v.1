import React from 'react'
import Images from '../../../../assets/a-exporter';
import Constant from '../../../../json/constant';

export default function MASStepOne({ android, setMobile, setStep }) {
    const [currentCode, setCurrentCode] = React.useState(Constant.countryCodes[0]);
    const [dropDown, setDropDown] = React.useState(false);
    const [searchTerm, setSearchTerm] = React.useState('');
    const [valid, setValid] = React.useState(false);

    const phoneRegex = /^(\d){9,12}$/;
    const handleKeyPress = (e) =>{
        if(valid){
            if(e.which === 13)
               {setStep(1);
            }
            // e.preventDefault();

        }else if(e.which <48 || e.which > 57 )
            e.preventDefault();
    }
    return (
        <div className="mas-step-one">
            <h2><img src={android ? Images.appsAndroid : Images.appsApple} /> {android ? "Android" : "IOS"} Setup Process</h2>
            <p>Welcome To The Assisted Setup To Get The Early Version Of The BrokerApp On Your IPhone. Will You Be Doing Following This Setup Page On Your Desktop While Setting Up The App On Your Iphone? Enter Your Number & Get The App Direct Download Sent To You Device</p>
            <div className="enter-number-wrapper">
                <button onClick={() => setDropDown(!dropDown)}>{currentCode.dial_code}</button>
                <input
                    onChange={e => { phoneRegex.test(e.target.value) ? setValid(true) : setValid(false); setMobile(`${currentCode.dial_code}${e.target.value}`); }}
                    onKeyPress={(e) => handleKeyPress(e)}
                    placeholder="000-0000-000" />
                <button disabled={!valid} onClick={() => setStep(1)}><img src={Images.fancyNext} /></button>
                <div className={dropDown ? "country-code-dropdown" : "d-none"}>
                    <div className="dropdown-header">
                        <input
                            onChange={(e) => setSearchTerm(e.target.value)}
                            autoFocus={true}
                            placeholder="Search Your Country....."
                        />
                    </div>
                    <div className="dropdown-body">
                        {
                            Constant.countryCodes.filter(obj => { return obj.name.toString().toLowerCase().startsWith(searchTerm.toString().toLowerCase()) })
                                .map(obj =>
                                    <h6 onClick={() => { setCurrentCode(obj); setDropDown(false) }}>
                                        <span>{obj.name}</span><span>{obj.dial_code}</span>
                                    </h6>
                                )
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}
