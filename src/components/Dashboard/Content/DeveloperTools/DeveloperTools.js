import React from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../assets/a-exporter';
import './developer-tools.style.scss';
export default function DeveloperTools() {
    const [selectHeader, setSelectHeader] = React.useState(headerButtons[0])
    return (
        <div className="developer-tools-main">
            <div className="developer-tools-wrapper">
                <div className="developer-tools-header">
                    <h1>Developer Tools</h1>
                    <h6>Here Is The Latests Developer Tools Your Team Can Leverage To Enchance Your Ageny</h6>

                </div>
                <div className="developer-tools-body">
                    <div className="developer-tools-tab-header">
                        {
                            headerButtons.map(obj =>
                                <button
                                    onClick={() => setSelectHeader(obj)}
                                    className={obj.keyId === selectHeader.keyId ? "selected-header" : ""}
                                    key={obj.keyId}>
                                        <img src={obj.image} /> 
                                        </button>)
                        }

                    </div>
                    <div className="developer-tools-tab-body">
                        {
                            [1, 2, 34, 5, 6, 6, 7].map(obj =>
                                <div className="developer-tools-tab-row">
                                    <div></div>
                                    <div>
                                        <h6>Understanding The Syndicate ....</h6>
                                        <span>Last Updated On February 21st 2020</span>
                                    </div>

                                </div>
                            )
                        }

                    </div>

                </div>


            </div>

        </div>
    )
}

const headerButtons = [
    { keyId: nextId(), image: Images.APIMachine },
    { keyId: nextId(), image: Images.DeployMachine },
    { keyId: nextId(), image: Images.DeployMachine },
]