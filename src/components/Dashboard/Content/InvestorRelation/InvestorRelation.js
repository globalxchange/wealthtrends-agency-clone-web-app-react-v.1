import React from 'react'
import nextId from 'react-id-generator'
import LoadingAnimation from '../../../../lotties/LoadingAnimation';
import { ddCloudList } from '../../../../services/getAPIs';
import './investor-relation.style.scss'
export default function InvestorRelation() {
    const [selectHeader, setSelectHeader] = React.useState(headerButtons[0]);
    const [list, setList] = React.useState([]);
    const [loading, setLoading] = React.useState(false)

    const setListFRomAPI= async()=>{
        setLoading(true);
        let res = await ddCloudList();
        setList([...res.data.data]);
        setLoading(false);
    }
    React.useEffect(()=>{
        if(selectHeader.name === "Offerings")
            setListFRomAPI();
        else
            setList([]);    
    },[selectHeader])
    return (
        <div className="investor-relation-main">
            <div className="investor-relation-wrapper">
                <div className="investor-relation-header">
                    <h1>Due Diligence Cloud</h1>
                    <h6>You Have Been Given Exclusive Access To Find, Analyze, & Engage With All The Investor Relations Material</h6>

                </div>
                <div className="investor-relation-body">
                    <div className="investor-relation-tab-header">
                        {
                            headerButtons.map(obj =>
                                <button
                                    onClick={() => setSelectHeader(obj)}
                                    className={obj.keyId === selectHeader.keyId ? "selected-header" : ""}
                                    key={obj.keyId}>{obj.name}</button>)
                        }

                    </div>
                    <div className="investor-relation-tab-body">
                        {
                            loading?
                            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                                <LoadingAnimation type ="no-data" size={{height: 150, width: 150}} />
                            </div>
                            :
                            list.map(obj =>
                                <div className="investor-relation-tab-row">
                                    <div></div>
                                    <div>
                            <h6>{obj.title}</h6>
                            <span>Last Updated On {obj.updatedAt.substring(0,10)}</span>
                                    </div>

                                </div>
                            )
                        }

                    </div>

                </div>


            </div>

        </div>
    )
}

const headerButtons = [
    { keyId: nextId(), name: "Company Background" },
    { keyId: nextId(), name: "Offerings" },
    { keyId: nextId(), name: "Compliance" },
    { keyId: nextId(), name: "Competitors" },
    { keyId: nextId(), name: "Cyber Security" },
]