import React from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../assets/a-exporter'
import LoadingAnimation from '../../../../lotties/LoadingAnimation'
import { fetchChecklist } from '../../../../services/getAPIs'
import SwitchComponent from '../../../common-components/switch/Switch'
import { Agency } from '../../../Context/Context'
import CBMReceipt from '../wallet/controller/handle-funds/block-check/controlled-by-me/receipt/CBMReceipt'
import './blockcheck.style.scss'
export default function BlockcheckMain() {
    const agency = React.useContext(Agency);
    const { currencyImageList, valueFormatter } = agency
    const [loading, setLoading] = React.useState(true);
    const [switchState, setSwitchState] = React.useState(false);
    const [checkList, setCheckList] = React.useState([]);
    const [searchByMail, setSearchByMail] = React.useState('');
    const [filterByStatus, setFilterByStatus] = React.useState([''])

    const getSetList = async () => {
        let res;
        try {
            res = await fetchChecklist();
            if (res.data.status) {
                setCheckList([...res.data.bcrs])
            } else {
                setCheckList([])
            }
            setLoading(false)
        } catch (error) {
            console.error(error)
            setCheckList([]);
            setLoading(false)
        }
    }
    const handleClick = (id) => {
        window.open(`https://blockcheck.io/#/${id}`, "_blank")
    }
    const filterTheList = () => {
        let temp = checkList.filter(obj => {
            return obj.initiator_email.toString().toLowerCase().startsWith(searchByMail.toString().toLowerCase()) &&
                obj.status.toString().toLowerCase().includes(...filterByStatus)
        })
        return temp;
    }
    const handleButtonClick = (obj, x) => {
        if (obj.type === "Check Status") {
            if (filterByStatus.includes(x.id)) {
                console.log("filterByStatus remove", filterByStatus)
                let newA = filterByStatus.filter(y => { return y !== x.id });
                if (!newA.length) {
                    setFilterByStatus([''])
                } else {
                    setFilterByStatus([...newA])
                }

            } else {
                console.log("filterByStatus add", filterByStatus);
                if (filterByStatus[0] === '') {
                    setFilterByStatus([x.id])
                } else {
                    setFilterByStatus([...filterByStatus, x.id])

                }

            }
        }

    }
    React.useEffect(() => {
        console.log("filterByStatus", filterByStatus)
    }, [filterByStatus])
    React.useEffect(() => {
        setLoading(true)
        getSetList()
    }, [])

    return (
        <div className="blockcheck-main">
            <div className="bcm-search-bar">
                <div>
                    <img src={Images.blockCheck} />
                    <h6>The Safest Way To Send Crypto</h6>
                </div>
                <div>
                    <input onChange={(e) => setSearchByMail(e.target.value)} placeholder="Search BlockChecks By Sender Email..|" />
                </div>

            </div>
            <div className="bcm-header">
                <h6>{!switchState ? "All Checks" : "Checks For Me"}</h6>
                <h6>
                    <span>All</span>
                    <SwitchComponent trigger={switchState} onClick={() => setSwitchState(!switchState)} />
                    <span>For Me</span>
                </h6>

            </div>
            <div className="bcm-body">
                <div className="bcm-body-search-filter">
                    <div className="bcm-body-search-filter-wrapper">
                        <div className="bcm-search-by-mail">
                            <img src={Images.blockCheck} />
                            <h6>Lets Find Your Check</h6>
                            <div>
                                <input onChange={(e) => setSearchByMail(e.target.value)} placeholder="Search BlockChecks By Sender Email.." />
                            </div>

                        </div>
                        <div className="ncm-search-by-filter">
                            <h4>Filter By</h4>
                            {
                                filters.map(obj => <div key={obj.keyId}>
                                    <h6>{obj.type}</h6>
                                    {
                                        obj.list.map(x => <button
                                            className={filterByStatus.includes(x.id) ? "active-button" : ""}
                                            onClick={() => handleButtonClick(obj, x)}>{x.name}</button>)
                                    }

                                </div>)
                            }


                        </div>

                    </div>

                </div>
                <div className="bcm-bodycheck-list">
                    {
                        loading ?
                            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                                <LoadingAnimation type="login" size={{ height: 200, width: 200 }} />
                            </div>
                            :
                            !filterTheList().length ?
                                <div className="w-100 flex-column h-100 d-flex justify-content-center align-items-center">
                                    <LoadingAnimation type="no-data" size={{ height: 200, width: 200 }} />
                                    <h6>No Checks....</h6>
                                </div>
                                :
                                filterTheList().map(obj =>
                                    <div onClick={() => handleClick(obj._id)} className="block-check-wrapper">
                                        <div style={obj.bcr_type.includes("sender") ? {} : { backgroundColor: "#464b4E" }} className="blockcheck-strip">
                                            
                                        </div>
                                        <div className="blockcheck-cheque">
                                            <CBMReceipt
                                                forList={true}
                                                id={obj._id}
                                                date={obj.status_logs?.[0]?.date?.split(',')[0]}
                                                coinImage={currencyImageList[obj.coin]}
                                                usdImage={currencyImageList["USD"]}
                                                coin={obj.coin}
                                                notes={"***************"}
                                                cryptoValue={valueFormatter(obj.amount, obj.coin)}
                                                address="***************"
                                                usdValue={""}
                                            />
                                        </div>
                                    </div>)
                    }

                </div>

            </div>

        </div>
    )
}

const filters = [
    {
        keyId: nextId(),
        type: "Check Type",
        list: [
            { keyId: nextId(), name: "To cash", id: "cash" },
            { keyId: nextId(), name: "To Sign", id: "sign" },
        ]
    },
    {
        keyId: nextId(),
        type: "Check Status",
        list: [
            { keyId: nextId(), name: "Sent", id: "sent" },
            { keyId: nextId(), name: "Accepted", id: "accepted" },
            { keyId: nextId(), name: "Rejected", id: "rejected" },
        ]
    },

]
