import React from 'react'
import { Link } from 'react-router-dom';
import LoadingAnimation from '../../../lotties/LoadingAnimation';
import { login } from '../../../services/postAPIs';
import { Agency } from '../../Context/Context';
import './refresh-session.style.scss'
export default function RefreshSession() {
    const agency = React.useContext(Agency);
    const { setRefreshSession, refreshApp, setCheckingLoggedIn, appLevelRefresh, setRegisteredAppsByUser } = agency;
    const [loginCredential, setLoginCredential] = React.useState({
        email: localStorage.getItem("userEmailPermanent"),
        password: ''
    });
    const [wrong, setWrong] = React.useState(false);
    const [loading, setLoading] = React.useState(false);

    const refreshToken = () => {
        setLoading(true)
        login(loginCredential.email, loginCredential.password)
            .then(res => {
                if (res.data.status) {
                    setRefreshSession(false);
                    localStorage.setItem("deviceKey", res.data.device_key);
                    localStorage.setItem("accessToken", res.data.accessToken);
                    localStorage.setItem("idToken", res.data.idToken);
                    localStorage.setItem("refreshToken", res.data.refreshToken);
                    setCheckingLoggedIn(false)
                    setLoading(false)
                    if (!refreshApp) {
                        setRegisteredAppsByUser();
                    } else {

                    }
                } else {
                    setLoading(false)
                    setWrong(true);
                }
            })
    }

    return (
        <div className="refresh-session-main">
            {
                loading ?
                    <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                        <LoadingAnimation type="login" size={{ height: 150, width: 150 }} />
                    </div>
                    :
                    <>
                        <div className="refresh-session-header">
                            <h6>Session Expired</h6>
                        </div>
                        <div className="refresh-session-body">

                            {
                                wrong
                                    ?
                                    <span>You Have Entered An Incorrect Password. Please Try Again Or Login With A <Link to="/login"><b>Different Account</b></Link> </span>
                                    :
                                    <span> You Have To Enter Your Password Again</span>
                            }

                            <input
                                name="password"
                                onChange={(e) => setLoginCredential({ ...loginCredential, password: e.target.value })}
                                value={loginCredential.password}
                                type="password"
                            />
                            <button onClick={() => refreshToken()} className={wrong ? "wrong-password" : ""}>
                                {loading ? "Refreshing...." : "LOGIN"}</button>
                        </div>
                    </>
            }
        </div>
    )
}
