import React from 'react'
import ReactPlayer from 'react-player'
import moment from 'moment-timezone';
import Images from '../../../../assets/a-exporter';
import { fetchBrainVideoLink } from '../../../../services/postAPIs';

import './learn-video-player.style.scss'
import LoadingAnimation from '../../../../lotties/LoadingAnimation';
import { getVideoInfo } from '../../../../services/getAPIs';
export default function LearnVideoPlayer({video_id}) {
    const [videoLink, setVideoLink] = React.useState('');
    const [isReady, setIsReady] = React.useState(false);
    const [videoInfo, setVideoInfo] = React.useState(null);


    const setUpVideoLink = async () => {
        console.log("video ID", video_id)
        let res = await fetchBrainVideoLink(video_id);
        setVideoLink(res.data)
    }
    const setUpVideoInfo =async () =>{
        let res = await getVideoInfo(video_id);
        if(res.data.status){
            setVideoInfo(res.data.data)

        }else{

        }
    }

    React.useEffect(() => {
        setUpVideoLink();
        setUpVideoInfo()
    }, [video_id])

    return (
        !videoInfo?
        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
            <LoadingAnimation  />
        </div>
        :
        <div className="learn-video-player-main">
            <div className="video-playing-area">
                {/* <img onClick={() => { setLearnVideo(false); setPlayVideo("") }} src="https://img.icons8.com/windows/32/ffffff/delete-sign.png" /> */}
                {isReady ? '' : <div className={false ? "" : "video-overlay-curtain"}><LoadingAnimation /></div>}
                <ReactPlayer
                    pip={true}
                    controls={true}
                    playing={true}
                    onReady={() => setIsReady(true)}
                    width="100%"
                    height="100%"
                    url={videoLink}
                    fallback={() => <h1 style={{ color: "red" }}>Hye</h1>}
                />

            </div>
            <div className="video-playing-info">
                <p>
                    <span>#instacrypto</span>
                    <span>#nvest</span>
                    <span>#crypto</span>
                </p>
                <h5>{videoInfo.title}</h5>
                <h6>
                    <span>{moment(videoInfo.updatedAt).format('DD-MM-YYYY hh:mm a')}</span>
                    <span>
                        <button>Share</button>
                        <button>Helpful?</button>
                    </span>
                </h6>

            </div>

        </div>
    )
}
