import Interweave from 'interweave';
import moment from 'moment-timezone';
import React from 'react'
import Images from '../../../../assets/a-exporter';
import LoadingAnimation from '../../../../lotties/LoadingAnimation';
import { fetchArticleIdId } from '../../../../services/getAPIs';
// import { Agency } from '../../../Context/Context'
import './article-component.style.scss'
export default function ArticleComponent({article_id}) {
    // const agency = React.useContext(Agency);
    // const { learnArticle, setLearnArticle ,setReadArticle} = agency;
    const [articleDetails, setArticleDetails] = React.useState(null)
    const [loading, setLoading] = React.useState(true)
    const setUpArticleDetails = async()=>{
        setLoading(true)
        let res = await fetchArticleIdId(article_id)
        console.log("full article", res.data)
        if(res.data.status){
            setArticleDetails(res.data.data.article)
            setLoading(false)
        }else{
            setArticleDetails(null)
            setLoading(false)

        }

    }

    React.useEffect(()=>{
        setUpArticleDetails()
    },[article_id])
    return (
        loading?
        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
            <LoadingAnimation type="login" size={{height: 200, width: 200}} />
        </div>
        :
        <div className="article-component">
            <h1>{articleDetails?.title}</h1>
            <div className="author-info">
                <div>
                    <img src={articleDetails?.icon} />
                </div>
                <div>
                    <span>{articleDetails?.desc}</span>
                    <span>{moment(articleDetails?.updatedAt).format('DD-MM - YYYY')}</span>
                </div>

            </div>
            <div className="article-cover-wrapper">
                <img src={articleDetails?.media} />

            </div>
            <div className="article-wrapper">
                {
                    <Interweave content={articleDetails?.article} />
                }

            </div>

        </div>
    )
}
