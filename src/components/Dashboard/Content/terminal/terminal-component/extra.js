// import React, {Component, useState} from 'react';
// import './terminal-buy.style.scss'
// import {arrowDown, arrowDownBlack, BTC, chevron, face, leftArrow} from "../../../../images/terminal-images";
// const TerminalBuyComponent = React.memo(()=>{
//     const [count, setCount] = useState(0);
//     const [selectTerm, setSelectTerm] = useState(terms[count]);
//     const [first, setFirst] = useState(true);
//     return (
//         <div className="terminal-buy">
//             <div className="terminal-buy-header">
//                 {first?'':<button onClick={()=>setFirst(!first)} className="button-left"><img src={arrowDownBlack}/></button>}
//                 <div>{first?'BITCOIN': 'TOP BITCOIN TRADER'}</div>
//                 {first?<button onClick={()=>setFirst(!first)} className="button-right"><img src={arrowDownBlack}/></button>:''}
//             </div>
//             <div className="terminal-buy-body">
//                 <div className="terminal-buy-body-header">
//                     <div className="terminal-buy-body-header-left">
//                         <img src={first?face:BTC}/>
//                     </div>
//                     <div className="terminal-buy-body-header-right">
//                         <span>
//                             <h6>{first?'John Snow':'About Bitcoin'}</h6>
//                             <span>{first?'CTO XYZ':''}</span>
//                         </span>
//                     </div>
//                 </div>
//                 <div className="terminal-buy-body-body">
//                     <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
//                         Itaque vero blanditiis asperiores soluta commodi dolore voluptatum ab dolorem,
//                         maiores accusantium voluptate earum
//                     </p>
//                 </div>
//                 <div className="terminal-buy-body-footer">
//                     {
//                         <div className="terminal-term">
//                             {console.log('mod', count)}
//                             <span>{terms[Math.abs(count) % 3].text}</span>
//                             <h6>{terms[Math.abs(count) % 3].amount}</h6>
//                         </div>
//                     }
//                     <button onClick={()=>setCount(count - 1)} className="move-left"><img src={arrowDownBlack}/></button>
//                     <button onClick={()=>setCount(count + 1)} className="move-right"><img src={arrowDownBlack}/></button>
//                 </div>
//             </div>
//             <div className="terminal-buy-footer">
//                 <button>Invest</button>
//                 <button>Learn</button>
//             </div>
//         </div>
//     );
// });
// const terms = [
//     {text: 'TERM 1', amount: '$7,899'},
//     {text: 'TERM 2', amount: '$3030'},
//     {text: 'TERM 3', amount: '$2975'},
// ];
// export default TerminalBuyComponent;
