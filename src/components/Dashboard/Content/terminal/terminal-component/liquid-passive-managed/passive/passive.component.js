import React, {useState} from 'react';
import './passive.style.scss';
import PassiveOneComponent from "./passive-one/passive-one.component";
import PassiveTwoComponent from "./passive-two/passive-two.component";
const PassiveComponent = () =>{
    const [page,setPage] = useState(true);
    return(
        <div className="passive-main">
            {
                page?<PassiveOneComponent setPage = {setPage}/>:<PassiveTwoComponent/>
            }
        </div>
    )
}

export default PassiveComponent;
