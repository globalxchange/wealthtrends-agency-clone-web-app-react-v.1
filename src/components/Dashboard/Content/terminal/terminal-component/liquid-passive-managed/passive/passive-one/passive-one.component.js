import React from 'react';

const PassiveOneComponent = ({setPage}) =>{
    return(
        <button onClick={()=>setPage(false)}>Passive One</button>
    )
}

export default PassiveOneComponent;
