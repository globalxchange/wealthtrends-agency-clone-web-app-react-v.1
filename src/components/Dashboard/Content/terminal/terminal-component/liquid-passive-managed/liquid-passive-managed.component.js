import React, {useContext, useEffect, useState} from 'react';
import './liquid-passive-managed.style.scss';
import {staticHeader, staticSubHeaderThree, staticSubHeaderTwo, staticSubHeaderOne} from "../../headersSubHeaders";
import LiquidComponent from "./liquid/liquid.component";
import {IcedTerminal} from "../../../main-context/main.context";
import nextId from "react-id-generator"
import {fullScreenButton} from "../../../../images/terminal-images";
const LiquidPassiveManagedComponent = React.memo(()=>{
    const [term, setTerm] = useState('');
    const terminal = useContext(IcedTerminal);
    const {header,setHeader, subHeader, setSubHeader} = terminal;
    const selectSubHeader = () =>{
        switch (header) {
            case 'Liquid': return staticSubHeaderOne;
            case 'Passive': return staticSubHeaderTwo;
            case 'Managed': return staticSubHeaderThree;
            default: return;
        }
    };
    const icedTerminal = useContext(IcedTerminal);
    const {layoutState} = icedTerminal;


    const handleHeaderClick = (x) =>{
        terminal.setLPMHeader(x);
        setHeader(x);
        switch (x) {
            case 'Liquid': setSubHeader(staticSubHeaderOne[0]);icedTerminal.setCommonSubHeader(staticSubHeaderOne[0]); break;
            case 'Passive': setSubHeader(staticSubHeaderTwo[0]);icedTerminal.setCommonSubHeader(staticSubHeaderTwo[0]); break;
            case 'Managed': setSubHeader(staticSubHeaderThree[0]);icedTerminal.setCommonSubHeader(staticSubHeaderThree[0]); break;
            default: return;
        }
    };

    const handleFullSize = () =>{
        if(terminal.layoutState[2].size[1]=== 10){
            terminal.changeLayout([{size: [9,5,0,0], component: ['b', 'chart']},layoutState[1], {size: [3,5,0,5], component: ['d', 'liquid']}, layoutState[3],layoutState[4]])
        }else{
            terminal.changeLayout([{size: [9,5,0,10], component: ['b', 'chart']},layoutState[1], {size: [12,10,0,0], component: ['d', 'liquid']}, layoutState[3], layoutState[4],])
        }
    };
    useEffect(()=>{
        if(header === 'Liquid')
            setTerm('');
        else if(header === 'Managed')
            setTerm('term')
    },[header]);
    return (
        <div className={icedTerminal.darkMode?"liquid-passive-managed-dark":"liquid-passive-managed"}>
            <div className="liquid-passive-managed-header position-relative">
                {
                    staticHeader.map((x) =>
                        <button
                            onMouseEnter={() => icedTerminal.changeMainValue(x)}
                            onMouseLeave={() => icedTerminal.changeMainValue(icedTerminal.mainValueType)}
                            onClick={() => {
                                handleHeaderClick(x);
                            }} className={header === x ? 'selected' : ''}>{x}</button>)
                }
                <img className="full-button" src={fullScreenButton} onClick={()=>handleFullSize()}/>
            </div>
            <div className="liquid-passive-managed-sub-header">
                {selectSubHeader().map((x) => <span onClick={() => {
                    icedTerminal.setCommonSubHeader(x);
                    setSubHeader(x)
                }} className={subHeader === x ? 'selected' : ''}>{x}</span>)}
            </div>
            <div className="middle-area-container">
                <LiquidComponent setTerm={setTerm} term={term} subHeader={subHeader}/>
            </div>
            <div className="liquid-passive-managed-footer d-flex align-items-center">
                <h6 className=" text-white">{header} {subHeader} Value</h6>
                <h6 className=" text-white">{terminal.formatter.format(terminal.LPMSums[subHeader.toLowerCase()])}</h6>
            </div>
        </div>
    );
});

export default LiquidPassiveManagedComponent;
