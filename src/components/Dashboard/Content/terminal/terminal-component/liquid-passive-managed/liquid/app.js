import React from 'react';
import './App.scss';
import {Switch, Route} from 'react-router-dom';
import { createChart } from "lightweight-charts";
import ChatMainIndex from "./component/chat-main/chat-main.index";
import {ChartsContextProvider} from "./component/context/chart.context";

function App() {
    // const chart = createChart(document.body, {width: 400, height: 300});
    // const lineSeries = chart.addLineSeries();
    // lineSeries.setData([
    //     { time: '2019-04-11', value: 80.01 },
    //     { time: '2019-04-12', value: 96.63 },
    //     { time: '2019-04-13', value: 76.64 },
    //     { time: '2019-04-14', value: 81.89 },
    //     { time: '2019-04-15', value: 74.43 },
    //     { time: '2019-04-16', value: 80.01 },
    //     { time: '2019-04-17', value: 96.63 },
    //     { time: '2019-04-18', value: 76.64 },
    //     { time: '2019-04-19', value: 81.89 },
    //     { time: '2019-04-20', value: 74.43 },
    // ]);
    return (
        <div className="trading-view-chart">
            {/*{chart}*/}
            <ChartsContextProvider>
            <Switch>
                <Route path ="/" component ={ChatMainIndex}/>
            </Switch>
            </ChartsContextProvider>


        </div>
    );
}

export default App;
