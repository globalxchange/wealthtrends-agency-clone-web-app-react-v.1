import React, { useContext, useState, useEffect } from 'react';
import nextId from "react-id-generator";
import './iquid.style.scss';
import { IcedTerminal } from "../../../../main-context/main.context";
import { comingSoon } from "../../../../../images/terminal-images";
const LiquidComponent = React.memo(({ subHeader, setTerm, term }) => {
    const terminal = useContext(IcedTerminal);
    const [coinList, setList] = useState([]);
    const { liquidTerminalList, liquidListLoader, liquidTokensList,loggedIn, valueFormatter, getCoinSymbol, icedList, earningList, formatter, liquidForexList, liquidCryptoList } = terminal;
    useEffect(() => {
        if (subHeader === 'Terminal')
            setTerm('term');
        else if (['Iced', 'Earning'].includes(subHeader))
            setTerm('iced');
        switch (subHeader) {
            case 'Forex': setList(liquidForexList); break;
            case 'Crypto': setList(liquidCryptoList); break;
            case 'Tokens': setList(liquidTokensList); break;
            case 'Iced': setList(icedList); break;
            case 'Earning': setList(earningList); break;
            case 'Terminal': setList(liquidTerminalList); break;
            default: setList([]);
        }
    }, [subHeader, liquidTerminalList, liquidCryptoList, liquidForexList]);
    return (
        <React.Fragment>
            {
                ['Indicies', 'Futures', 'Funds'].includes(subHeader) ?
                    <div className={terminal.darkMode ? "liquid-passive-managed-body-dark" : "liquid-passive-managed-body"}>
                        <img src={comingSoon} className="coming-soon-image" />
                    </div>
                    :!loggedIn?<div className={terminal.darkMode ? "liquid-passive-managed-body-dark" : "liquid-passive-managed-body"}>
                        {
                            [1,2,3,4].map(item =><div className="liquid-passive-managed-table-row">
                                <div>--</div>
                                <div>--</div>
                                <div>--</div>
                            </div>)
                        }
                    </div>
                    :
                    <div className={terminal.darkMode ? "liquid-passive-managed-body-dark" : "liquid-passive-managed-body"}>
                        {
                            liquidListLoader ?
                                <h6 className="liquid-loading">Loading...</h6>
                                :
                                coinList.map((obj) => <div key={nextId()} className="liquid-passive-managed-table-row">
                                    <div className="">
                                        <img
                                            // onError={terminal.onImageError}
                                            src={term === 'term' ? `https://cryptoicons.org/api/icon/${obj.coin.toLowerCase()}/30` : obj.coinImage} />
                                        <span className="mb-0">{term === 'term' ? obj.coin : obj.coinSymbol}</span>
                                    </div>
                                    <div className="">
                                        {term === 'term' || term === 'iced' ?
                                            <span className="mb-0">
                                                {term === 'iced' ? valueFormatter(obj.coinSymbol, obj.balance, 'liquid 1') : obj.coin === 'USD' ? formatter.format(obj.balance) : valueFormatter(obj.coin, obj.balance, 'liquid 2')}</span> :
                                            <span className="mb-0">
                                                {getCoinSymbol(obj.coinSymbol)}{valueFormatter(obj.coinSymbol, obj.coinValue, 'liquid 3')}
                                            </span>
                                        }
                                    </div>
                                    <div className="">
                                        {term === 'term' ?
                                            <span className="mb-0">
                                                {terminal.formatter.format(obj.USDeqv)}
                                            </span> :
                                            term === 'iced' ?
                                                <span className="mb-0">{terminal.formatter.format(obj.usdValue)}</span>
                                                :
                                                <span className="mb-0">{terminal.formatter.format(obj.coinValueUSD)}</span>
                                        }
                                    </div>
                                </div>)
                        }
                    </div>
            }
        </React.Fragment>
    );
});
export default LiquidComponent;
