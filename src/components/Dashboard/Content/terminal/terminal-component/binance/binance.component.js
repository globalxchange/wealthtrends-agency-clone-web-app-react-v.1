import React, {Component, useContext, useEffect, useState} from 'react';
import axios from "axios";
import './binance.style.scss';
import {IcedTerminal} from "../../../main-context/main.context";
let ws;
const BinanceComponent = () =>{
    const [wsOpen, setWsOpen] = useState(false);
    const [wsResp, setWsResp] = useState({
        asks: [
            [0, 0],
            [0, 0],
            [0, 0],
            [0, 0],
            [0, 0],
        ],
        bids: [
            [0, 0],
            [0, 0],
            [0, 0],
            [0, 0],
            [0, 0],
        ]
    });

    const [largest, setLargest] = useState([0,0]);
    const [conversion, setConversion] = useState(1);
    const [sums, setSums] = useState({asks: 0, bids: 0});
    const [unSubscribe, setUnSubscribe] = useState({
        method: 'UNSUBSCRIBE',
        params: [`btcusdt@depth5@1000ms`],
        id: 1,
    });
    const terminal = useContext(IcedTerminal);
    useEffect(()=>{
        axios.get(`https://comms.globalxchange.com/coin/getCmcPrices?convert=USD`)
            .then(res =>{
                setConversion(res.data.btc_price);
            })
    },[terminal.darkMode]);
    useEffect(()=>{
        ws = new WebSocket(
            `wss://stream.binance.com:9443/ws/btcusdt@depth5@1000ms`
        );
        ws.onopen = () => {
            setWsOpen(true);
        };
        ws.onmessage = (event) => {
            const response = JSON.parse(event.data);
            if(response.result === null)
                return;
            setWsResp(response);
            setTotalSums(response.bids, response.asks);
            checkLargest(response.bids);
        };
        ws.onclose = () => {
            ws.close();
        };
        return () => {
            ws.close();
        };

    },[]);
    const setTotalSums = (bids, asks) =>{
        let sum = 0;
        let sum1 = 0;
        let i;
        for (i = 0; i< bids.length; i ++){
            sum = sum + parseFloat(bids[i][1]);
            sum1 = sum1 + parseFloat(asks[i][1]);
        }
        setSums({asks: sum1, bids: sum});

    };
    const checkLargest = (arr) =>{
        let i;
        let first = arr[0][0];
        let temp = arr;
        let t;
        for(i = 0; i < arr.length - 1; i ++){
            if(first < temp[i+1][0]){
                t = temp[i + 1][0];
                temp[i + 1][0] = temp[0][0];
                temp[0][0] = t;
            }
        }
        setLargest(temp[0]);

    };
    useEffect(() => {
        if (wsOpen) {
            const subscribe = {
                method: 'SUBSCRIBE',
                params: [`btcusdt@depth5@1000ms`],
                id: 1,
            };
            ws.send(JSON.stringify(unSubscribe));
            ws.send(JSON.stringify(subscribe));
            setUnSubscribe({
                method: 'UNSUBSCRIBE',
                params: [`btcusdt@depth5@1000ms`],
                id: 1,
            });
        }
    }, [wsOpen]);

    useEffect(()=>{
    },[wsResp])
    return (
        <div className={terminal.darkMode ? "binance-main-dark" : "binance-main"}>
            <div className='binance-main-header'>
                {
                    header.map(item => <button>{item}</button>)
                }

            </div>
            <div className="binance-main-body">
                <div className="binance-main-body-ask">
                    {
                        wsResp.asks.map(arr =>
                            <div
                                style={{background: `linear-gradient(to left,rgba(239, 83, 80, 0.17) ${(arr[1] / sums.asks) * 100}%, transparent 0)`}}>
                                <button>{terminal.formatter.format(parseFloat(arr[0]))}</button>
                                <button>{terminal.valueFormatter('BTC', arr[1])}</button>
                                <button>{terminal.formatter.format(arr[1] * conversion)}</button>
                            </div>
                        )
                    }
                </div>
                <div className="binance-main-body-center">
                    <button>{terminal.valueFormatter('BTC', largest[1])}</button>
                    <button>{terminal.formatter.format(largest[0])}</button>
                </div>
                <div className="binance-main-body-bid">                  {
                    wsResp.bids.map(arr =>
                        <div
                            style={terminal.darkMode ?
                                {background: `linear-gradient(to left,rgba(2, 58, 254, 0.17) ${(arr[1] / sums.bids) * 100}%, transparent 0)`}
                                : {background: `linear-gradient(to left,rgba(112, 197, 130, 0.17) ${(arr[1] / sums.bids) * 100}%, transparent 0)`}}>
                            <button>{terminal.formatter.format(parseFloat(arr[0]))}</button>
                            <button>{terminal.valueFormatter('BTC', arr[1])}</button>
                            <button>{terminal.formatter.format(arr[1] * conversion)}</button>
                        </div>
                    )
                }

                </div>

            </div>

        </div>
    );
};
const header = ["Price", "Amount", "USD"];

export default BinanceComponent;
