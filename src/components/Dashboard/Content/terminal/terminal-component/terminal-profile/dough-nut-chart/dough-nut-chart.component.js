import React, {useContext, useEffect, useState} from 'react';
import {IcedTerminal} from "../../../../main-context/main.context";

const DoughNutChartComponent = ({donut}) =>{
    const [colors, setColors] = useState(["#E8EBF0","#8B8B8B", "#000000" ]);
    const icedTerminal = useContext(IcedTerminal);
    useEffect(()=>{
        // if(icedTerminal.hoveringOverChart || icedTerminal.mainValueType !== 'Net-Worth')
        //     setColors(['#ffe985', '#9700c9','#ff6691']);
        // else{
        //     setColors(["#464b4e","#8c90f5", "#90e4a2" ]);
        // }
    },[]);
    return(
        <svg height="100%" width="100%">
            class="donut">
            <circle cx={donut.c[0]} cy={donut.c[1]} r={donut.r} fill="transparent"/>
            <circle cx={donut.c[0]} cy={donut.c[1]} r={donut.r} fill="transparent" stroke="#d2d3d4" strokeLinecap="round" strokeWidth="15"/>
            <circle cx={donut.c[0]} cy={donut.c[1]} r={donut.r} fill="transparent" stroke={`${colors[0]}`} strokeLinecap="round" strokeWidth="15"
                    strokeDasharray={`${donut.sd3[0]} ${donut.sd3[1]}`} strokeDashoffset={`${donut.sdo3}`}/>
            <circle cx={donut.c[0]} cy={donut.c[1]} r={donut.r} fill="transparent" stroke={`${colors[1]}`} strokeLinecap="round" strokeWidth="15"
                    strokeDasharray={`${donut.sd2[0]} ${donut.sd2[1]}`} strokeDashoffset={`${donut.sdo2}`}/>
            <circle cx={donut.c[0]} cy={donut.c[1]} r={donut.r} fill="transparent" stroke={`${colors[2]}`} strokeLinecap="round" strokeWidth="15"
                    strokeDasharray={`${donut.sd1[0]} ${donut.sd1[1]}`} strokeDashoffset={`${donut.sdo1}`}/>
        </svg>
    )
};

export default DoughNutChartComponent;
