import React, {useContext, useEffect, useRef, useState} from 'react';
import './terminal-profile.style.scss';
import ReactResizeDetector from 'react-resize-detector';
import {arrowDown, face, fullScreenButton, user} from "../../../../images/terminal-images";
import {IcedTerminal} from "../../../main-context/main.context";
import DoughNutChartComponent from "./dough-nut-chart/dough-nut-chart.component";
const TerminalProfileComponent = React.memo(()=>{
    let don = useRef();
    const [sizes, setSizes] = useState([]);
    const icedTerminal = useContext(IcedTerminal);
    const [profile,setProfile] = useState('Net-Worth');
    const [donut, setDonut] = useState({
        c: [], r: '', sd1: [], sdo1:[],
        sd2: [], sdo2:[], sd3: [], sdo3:[],
    });
    const {liquid, passive, managed,total, extraState } = icedTerminal.mainValue;
    const {size, layoutState} = icedTerminal;
    useEffect(()=>{
            let d = document.getElementById('donut');
            let y = d.offsetHeight; let x = d.offsetWidth;
        let circumstance = 2 * 3.14 * (0.25 * y);
            let divider = total;
            let p1 = managed/divider; let p2 = passive/divider; let p3 = liquid/divider;
            setDonut({
                xy: [x, y], c: [0.5 * x, 0.5 * y],
                r: [0.25 * y],
                sd1: [circumstance * p1, circumstance * (1 - p1)], sdo1: 0.25 * circumstance,
                sd2: [circumstance * p2, circumstance * (1 - p2)], sdo2: ((1 - p1) + 0.25)* circumstance,
                sd3: [(circumstance * p3),(circumstance * (1 - p3))], sdo3: (1 - (p1 + p2) + 0.25) * circumstance
            });
            let ele = document.getElementById("profile");
            ele.addEventListener("resize", handleElement);

            window.addEventListener('resize', handleSize);
            return ()=>{
                window.removeEventListener('resize', handleSize)
            }
        },[sizes,don.current?.offsetHeight, icedTerminal.mainValue, icedTerminal.changeChart, layoutState, extraState]);

    const handleSize = () => {
        console.log('changing');
        setSizes([window.innerHeight, window.innerWidth])
    };
    const handleElement = () =>{
        alert();
        console.log('changing')
    }
    const handleClick = (name) =>{
        if(name === icedTerminal.mainValueType){
            setProfile('Net-Worth'); icedTerminal.changeMainValueType('Net-Worth')
        }else{
            setProfile(name); icedTerminal.changeMainValueType(name);
        }
    };
    const handleFullSize = () =>{
        // if(icedTerminal.layoutState[2].size[1]=== 10){
        //     icedTerminal.changeLayout([{size: [9,5,0,0], component: ['b', 'chart']},layoutState[1], {size: [3,5,0,5], component: ['a', 'profile']}, layoutState[3], layoutState[4]])
        // }else{
        //     icedTerminal.changeLayout([{size: [9,5,0,10], component: ['b', 'chart']},layoutState[1], {size: [12,10,0,0], component: ['a', 'profile']}, layoutState[3], layoutState[4]])
        // }
    };

    return (

        <div ref={don} id="profile" className="terminal-profile">
            <div className="terminal-profile-header">
                {
                    profileButtons.map((obj) => <button
                        // onMouseEnter={()=>{icedTerminal.changeMainValue(obj.name);icedTerminal.changeHoveringOverChart(true)}}
                        // onMouseLeave={()=>{icedTerminal.changeMainValue(icedTerminal.mainValueType); icedTerminal.changeHoveringOverChart(false)}}
                        onClick={() => {handleClick(obj.name)}}
                        className={obj.name === profile ? 'selected' : ''}>{obj.name}</button>)
                }
            </div>
            <div id="donut"  className="terminal-profile-body">
                {
                    donut.c.length === 0 ?
                        '' :
                        <DoughNutChartComponent donut={donut}/>
                }
                <div className="terminal-amount">
                    <p>{icedTerminal.mainValue.type}</p>
                    <h5>{icedTerminal.formatter.format(icedTerminal.mainValue.display)}</h5>
                </div>

            </div>
            <div onClick={()=>handleFullSize()} className="terminal-body-footer">
            </div>
        </div>
    );
});
const profileButtons = [
    {name: "Liquid"},{name: "Passive"}, {name: "Managed"}
];

const data = [20, 40, 40];
export default TerminalProfileComponent;
