import React, {Component, useEffect, useState} from 'react';
import './terminal-buy.style.scss'
import EAppHomeComponent from "./e-app-home/e-app-home.component";
import ChatAppComponent from "./chat-app/chat-app.component";
import FundAppComponent from "./fund-app/fund-app.component";
import TransactionHistoryComponent from "./transaction-history/transaction-history.component";
import TradeStreamComponent from "./trade-stream/trade-stream.component";


const TerminalBuyComponent = React.memo(({value})=>{
    const [appSelector, setAppSelector] = useState('Home');//Todo: chane it to Home
    useEffect(()=>{
        console.log('change in value', value);
    },[]);

    const selector = () =>{
      switch (appSelector) {
          case "Home": return <EAppHomeComponent setAppSelector={ setAppSelector}/>;
          case "Chats": return <ChatAppComponent setAppSelector={setAppSelector}/>;
          case "Algo Swith": return <TransactionHistoryComponent/>;
          case "Funds": return <FundAppComponent independent = {false} setAppSelector = {setAppSelector}/>;
          case "Trade Stream": return <TradeStreamComponent out = {false}/>;
          default: return <EAppHomeComponent setAppSelector={setAppSelector}/>
      }
    };
    return (
        <div className="terminal-buy">
            {selector()}
        </div>
    );
});
export default TerminalBuyComponent;
