import React, {Component, useContext, useState} from 'react';
import './e-app-home.style.scss'
import {
    accountingTool,
    algoSwith,
    chats,
    Eapp, fullScreenButton,
    funds,
    onHold,
    searchIcon,
    stream
} from "../../../../../images/terminal-images";
import {IcedTerminal} from "../../../../main-context/main.context";
import nextId from "react-id-generator";
const EAppHomeComponent = ({setAppSelector}) =>{
    const terminal = useContext(IcedTerminal);
    const [overlay, setOverlay] = useState(false);
    const [tempData, settempData] = useState({});
    const {layoutState} = terminal;
    const handleFullSize = () =>{
        if(terminal.layoutState[5].size[1]=== 10){
            terminal.changeLayout([{size: [3,5,0,0], component: ['a', 'profile']},layoutState[1], layoutState[2],layoutState[3],layoutState[4],{size: [3,5,9,5], component: ['f', 'buy']}])
        }else{
            terminal.changeLayout([{size: [3,5,0,10], component: ['a', 'profile']},layoutState[1], layoutState[2],layoutState[3],layoutState[4], {size: [12,10,0,0], component: ['f', 'buy']}])
        }
    };
    const addComponent = (where) =>{
        if(where){
            let id = nextId();
            terminal.addingNewTab([...terminal.addNewTab, {type: true, app: tempData.text, k: id}], id, tempData.text);
            // terminal.changeLayout([...layoutState, {size:[3,5,0,10],component: ['g', 'buy']}]);
            setOverlay(false);
        }
        else{
            setAppSelector(tempData.text);
            setOverlay(false);
        }
    };
    const overlayCheck = (x) =>{
        settempData(x);
        setOverlay(true);
    };
    return (
        <React.Fragment>
            <div className="e-app-home">
                <div className="e-app-home-header position-relative">
                    <img src={Eapp}/>

                    <img className="full-button" src={fullScreenButton} onClick={()=>handleFullSize()}/>
                </div>
                <div className="e-app-home-body">
                    <div className={overlay?"e-app-overlay":"d-none"}>
                        <h6>Open In</h6>
                        <h6><button onClick={()=>addComponent(false)}>Here</button><button onClick={()=>addComponent(false)}>New Box</button></h6>
                    </div>
                    <div className="e-app-home-body-input-container">
                        <h6>Welcome To The</h6>
                        <h2>Exchange Marketplace</h2>
                    </div>
                    <div className="app-list">
                        <div className="e-app-home-body-slider">
                            {
                                sliderList.slice(0,3).map((x) => <div className="e-app-home-body-slider-single">
                                    <div key={x.text} onClick={() => overlayCheck(x)} className="slider-image-container"
                                         style={{backgroundColor: `${x.color}`, color: "white"}}>
                                        <img src={x.image}/>
                                    </div>
                                    <p>{x.text}</p>
                                </div>)
                            }
                        </div>
                        <div className="e-app-home-body-slider">
                            {
                                sliderList.slice(3,6).map((x) => <div className="e-app-home-body-slider-single">
                                    <div key={x.text} onClick={() => overlayCheck(x)}  className="slider-image-container"
                                         style={{backgroundColor: `${x.color}`, color: "white"}}>
                                        <img src={x.image}/>
                                    </div>
                                    <p>{x.text}</p>
                                </div>)
                            }
                        </div>
                    </div>

                </div>
                <div className="e-app-home-footer">
                    <button>Terminal Level</button>
                    <button>Premium</button>

                </div>
            </div>
        </React.Fragment>
    );
};

const sliderList  = [
    {text: "Chats", image: chats, color: '#186AB4'},
    {text: "Funds", image: funds, color: '#787885'},
    {text: "Trade Stream", image: stream, color: '#70C582'},
    {text: "On Hold", image: onHold, color: '#35495E'},
    {text: "Accounting Tool", image: accountingTool, color: '#186AB4'},
    {text: "Algo Swith", image: algoSwith, color: '#292934'},
];

export default EAppHomeComponent;
