import React, {Component, useContext, useEffect, useState} from 'react';
import './fund-app.style.scss';
import FundAppOneComponent from "./fund-app-one/fund-app-one.component";
import FundAppThreeComponent from "./fund-app-three/fund-app-three.component";
import {backButton, backGray, cancel, funds, fundsLogo} from "../../../../../images/terminal-images";
import FundAppFourComponent from "./fund-app-four/fund-app-four.component";
import FundAppFiveComponent from "./fund-app-five/fund-app-five.component";
import {IcedTerminal} from "../../../../main-context/main.context";
const FundAppComponent = ({setAppSelector, independent,cId}) =>{
    const icedTerminal = useContext(IcedTerminal);
    const {removeTab} = icedTerminal;
    const [fundStep, setFundStep] = useState(0);
    const [selectedButtons, setSelectedButtons] = useState({first:'', second: ''});
    const [percentValue, setPercentValue] = useState('');
    const [timeSpan, setTimeSpan] = useState(false);
    const selectFundApp = () =>{
      switch (fundStep) {
          case 0: return <FundAppOneComponent setSelectedButttons = {setSelectedButtons} selectedButtons={selectedButtons} fundStep = {fundStep} setFundStep = {setFundStep}/>;
          case 1: return <FundAppOneComponent setSelectedButttons = {setSelectedButtons} selectedButtons={selectedButtons} fundStep = {fundStep} setFundStep = {setFundStep}/>;
          case 2: return <FundAppThreeComponent setTimeSpan = {setTimeSpan} timeSpan = {timeSpan} selectedButtons={selectedButtons} fundStep = {fundStep} setFundStep = {setFundStep}/>;
          case 3: return <FundAppFourComponent fundStep = {fundStep} setFundStep = {setFundStep}/>;
          case 4: return <FundAppFiveComponent setPercentValue = {setPercentValue} fundStep = {fundStep} setFundStep = {setFundStep}/>;
          default: return;
      }
    };
    const headerText = () =>{
      switch (fundStep) {
          case 0: return '';
          case 1: return 'Asset Classes';
          case 2: return `Top ${selectedButtons.second} Funds`;
          case 3: return "Shorupan's Fund";
          default: return 'Subscribe To Shorupan’s Fund'
      }
    };
    useEffect(()=>{
        console.log('Idddddddddddddddddddddd', cId)
    },[])
    return (
        <React.Fragment>
            <div className="funds-app">
                <div className="funds-app-header">
                    <div className={timeSpan?'select-time-span':'d-none'}>
                        <h6>Select Time Span</h6>
                    </div>
                    <img className={fundStep === 0?'':'d-none'} src={fundsLogo}/>
                    <div className={fundStep === 0 || timeSpan?'d-none':'funds-app-header-left'}>
                        <img src={funds}/>
                    </div>
                    <div className={fundStep === 0 || timeSpan?'d-none':'funds-app-header-right'} >
                        <h6>{headerText()}</h6>
                    </div>
                </div>
                <div style={timeSpan?{borderLeft: 0, borderRight: 0}:{}} className="funds-app-body">
                    {selectFundApp()}
                </div>
                <div style={timeSpan?{justifyContent: 'space-between', borderTop: 0}:{}} className="funds-app-footer">
                    <button className={timeSpan?'d-none':''} onClick={()=>{independent?removeTab(cId):setAppSelector('Home')}}><img src={cancel}/></button>
                    <button disabled={timeSpan} onClick={()=>setFundStep(fundStep -1)} className={fundStep === 0?'d-none': ''}><img src={backGray}/></button>
                    <button style={fundStep === 3?{width: '36%'}:{}} onClick={()=>setFundStep(fundStep +1)} className={fundStep === 3?'': 'd-none'}>Full Profile</button>
                    <button style={fundStep === 3?{width: '36%'}:{}}  disabled={percentValue === ''?true:false} className={fundStep > 2?'': 'd-none'}>Subscribe</button>
                    <button className={timeSpan?'':'d-none'} onClick={()=>setTimeSpan(false)}><img src={cancel}/></button>

                </div>
            </div>
        </React.Fragment>
    );
};

export default FundAppComponent;
