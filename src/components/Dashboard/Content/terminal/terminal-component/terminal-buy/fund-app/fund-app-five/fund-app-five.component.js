import React, {useContext, useState} from 'react';
import DonutChartComponent from "./donut-chart.component";
import {IcedTerminal} from "../../../../../main-context/main.context";
import { circleDark, circle, face } from '../../../../../../images/terminal-images';

const FundAppFiveComponent = ({setPercentValue}) =>{
    const [portfolioList, setPortfolioList] = useState(true);
    const [selectedValue, setSelectedValue] = useState(circleValue[0]);
    const icedTerminal = useContext(IcedTerminal);
    const handleClick = (obj) =>{
        setSelectedValue(obj);
        switch (obj.type) {
            case "Allocated": setPortfolioList(false); break;
            default: return;

        }
    };
    return (
        <div className="fund-app-five-body">
            <div className="fund-app-five-header">Portfolio Composition</div>
            <div className="fund-app-five-chart-container">
                <div className="w-50 h-100">
                    <DonutChartComponent selectedValue ={selectedValue}/>
                </div>
                <div className="w-50 h-100 fund-app-five-chart-container-right">
                    {
                        (portfolioList?circleValue.slice(0,2):circleValue.slice(2,6)).map((obj) =>
                            <div
                                style={obj.type === selectedValue.type?{opacity: 1}:portfolioList?{opacity:1}:{opacity: 0.5}}
                                onClick={()=>handleClick(obj)}>
                                <img src={obj.type === selectedValue.type?circleDark:circle}/><span>{obj.type}</span><span> - </span><span> {obj.value}%</span>
                            </div>)
                    }
                </div>

            </div>
            <div className="fund-app-five-slider">
                {
                    circleValue.slice(2,6).map((obj) =>
                        <div
                            onClick={()=>{setPortfolioList(false);setSelectedValue(obj)}}
                            style={selectedValue.type === obj.type?{border: '1px solid #464b4e'}:{}}
                            className="fund-app-five-slider-list">
                            <div className="fund-app-five-slider-list-container">
                                <div><img src={face}/></div>
                                <div><h6>{obj.type}</h6><h6>$435.83</h6></div>
                                <div><h6>{parseFloat(obj.value).toFixed(1)}%</h6></div>
                            </div>
                        </div>)
                }

            </div>
            <div className="fund-app-five-percentage">
                <div>
                    <input onKeyPress={icedTerminal.handleKeyPress} onChange={(e)=>setPercentValue(e.target.value)} placeholder="00.0%"/>
                    <h6>Enter Percentage</h6>
                </div>

            </div>

        </div>
    );
};

const circleValue = [
    {type: 'Liquid', value: 56.0},
    {type: 'Allocated', value: 54.0},
    {type: 'Shorupan1', value: 30.0},
    {type: 'Shorupan2', value: 36.0},
    {type: 'Shorupan3', value: 27.0},
    {type: 'Shorupan4', value: 39.0},
];

export default FundAppFiveComponent;
