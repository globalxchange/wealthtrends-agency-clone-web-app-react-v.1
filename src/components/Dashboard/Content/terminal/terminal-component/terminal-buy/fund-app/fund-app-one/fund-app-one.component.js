import React from 'react';

const FundAppOneComponent = ({setFundStep, fundStep, setSelectedButttons, selectedButttons}) =>{
    const handleClick = (x) => {
        if(fundStep){
            setSelectedButttons({...selectedButttons, second: x})
        }else{
            setSelectedButttons({...selectedButttons, first: x})
        }
        setFundStep(fundStep + 1);
    };
    return (
        <div className="fund-app-one-body">
            <div>
                <h5>{fundStep ? fundsStepTwo.text1 : fundsStepOne.text1}</h5>
                <h6>{fundStep ? fundsStepTwo.text2 : fundsStepOne.text2}</h6>
            </div>
            {
                (fundStep ? fundsStepTwo.list : fundsStepOne.list).map((x) =>
                    <div onClick={() => handleClick(x)} key={x}>
                        <h6 className="mb-0">{x}</h6>
                    </div>)
            }
        </div>
    );
};
const buttonList = ["Discover Traders", "Manage Your Portfolio", "Launch Your Fund"];
const fundsStepOne = {
    text1: 'Wecome To Funds',
    text2: 'Access To Top Traders In Crypto',
    list: ["Discover Traders", "Manage Your Portfolio", "Launch Your Fund"]
};
const fundsStepTwo = {
    text1: 'Find Traders',
    text2: 'Which Asset Do You Want To See',
    list: ["Crypto", "Forex", "Futures"]
};
export default FundAppOneComponent;
