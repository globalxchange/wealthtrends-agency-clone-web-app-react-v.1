import React, {Component} from 'react';
import nextId from "react-id-generator";

const SpanSelectorComponent = ({timeSpan,spans, setTimeSpan, setSelectedSpan}) =>{
    return(

        <div style={timeSpan?{}: {display: 'none'}} className="time-span-container">
            {
                spans.map((x, i)=><button onClick={()=>{setTimeSpan(false); setSelectedSpan(i)}}>{x.text}</button>)
            }
        </div>
    )
};
export default SpanSelectorComponent;
