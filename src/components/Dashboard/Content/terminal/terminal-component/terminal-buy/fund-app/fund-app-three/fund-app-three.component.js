import React, {useContext, useEffect, useRef, useState} from 'react';
import {face, funds, searchIcon} from "../../../../../../images/terminal-images";
import SpanSelectorComponent from "./span-selector/span-selector.component";
import {IcedTerminal} from "../../../../../main-context/main.context";

const FundAppThreeComponent = ({setFundStep, fundStep, setTimeSpan, timeSpan}) =>{
    const [scrollPosition, setScrollPosition] = useState(false);
    const [selectedSpan , setSelectedSpan] = useState(2);
    const icedTerminal = useContext(IcedTerminal);
    useEffect(()=>{
    },[]);
    const handleScroll = (e) =>{
       const s = document.getElementById("scroll");
        if(s.scrollTop > 60){
            setScrollPosition(true);
        }else{
            setScrollPosition(false);
        }
    };
    return (
        <div className="fund-app-two-body">
            <SpanSelectorComponent setSelectedSpan={setSelectedSpan } spans = {spans} timeSpan ={timeSpan} setTimeSpan = {setTimeSpan}/>
            <div className={`fund-app-two-body-above ${scrollPosition ? 'scroll-hide' : 'scroll-show'}`}>
                <div className="fund-app-two-input">
                    <h2 className="w-100">The Top 10 Funds</h2>
                    <p className="w-100">
                       Generated <span className="font-weight-bold">{icedTerminal.formatter.format(spans[selectedSpan].value)}</span> In profits For Their Client In The Last <button onClick={()=>setTimeSpan(true)} className="button-span">{spans[selectedSpan].show}</button>
                    </p>
                </div>
            </div>
            <div onScroll={handleScroll} id="scroll"
                 className={`fund-app-two-body-below ${scrollPosition ? 'full-height' : 'normal-height'}`}>
                {
                    [1, 1, 1, 1, 1, 1, 1].map((x, i) =>
                        <div style={scrollPosition ? {height: '20%'} : {}} className="fund-app-two-body-below-list-container">

                            <div className="counter">{i+1}</div>
                            <div onClick={() => setFundStep(fundStep + 1)}
                                 className="fund-app-two-body-below-list">

                                <div className="step-three-face">
                                    <img src={face}/>
                                </div>
                                <div><h6>Shorupan</h6><h6>Funds Bank: 121</h6></div>
                                <div><h6 className="text-right w-100">234</h6><h6
                                    className="text-right w-100">Subscribers</h6></div>
                                <div className="text-right"><h6 className="text-right w-100">34.2%</h6><h6
                                    className="text-right w-100">Account ROI</h6></div>
                            </div>
                        </div>)
                }

            </div>
        </div>
    );
};

const spans = [
    {id: 0, text: "All Time", show: "All Time", value: 200000},
    {id: 1, text: "Last 7 Days", show: "7 Days", value: 30000},
    {id: 2, text: "Last 24 Hours", show: "24 Hours", value: 10000},
    {id: 3, text: "Last 30 Days", show: "30 Days", value: 90000},
    {id: 4, text: "Last 1 Year", show: "1 Year", value: 100000},
];
export default FundAppThreeComponent;
