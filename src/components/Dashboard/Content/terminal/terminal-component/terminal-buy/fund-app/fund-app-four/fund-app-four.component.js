import React, {Component, useEffect, useState} from 'react';
import {face} from "../../../../../../images/terminal-images";

const FundAppFourComponent = ({setFundStep, fundStep}) =>{
    const [chartHeight, setChartHeight] = useState('');
    const [chartWidth, setChartWidth] = useState('');
    const [changeSize, setChangeSize] = useState(false);
    const [extra, setExtra] = useState(0.07);
    useEffect(()=>{
        let ele = document.getElementById('line-graph');
        setChartHeight(ele.offsetHeight);
        setChartWidth(ele.offsetWidth);
        window.addEventListener('resize', handleSize);
        return ()=>{
            window.removeEventListener('resize', handleSize)
        }
    },[changeSize]);
    const handleSize = () =>{
        setChangeSize(!changeSize);
    }
  return(
      <div className="fund-app-four-body">
          <div id = "line-graph" className="fund-app-four-body-upper">
              <div className="values-absolute">
                  <h6>$345,997.09</h6>
                  <h6>Fund Valuation</h6>
              </div>
              <svg width={chartWidth} height={chartHeight}>
                  <path
                      d={`M 0 ${chartHeight} L0 ${0.75 * chartHeight} C${0.22 * chartWidth} ${0.35 * chartHeight}, ${0.34 * chartWidth} ${0.35 * chartHeight}, ${0.47 * chartWidth} ${0.60 * chartHeight}  S${0.70 * chartWidth} ${0.45 * chartHeight}, ${0.78 * chartWidth} ${(0.42 - extra) * chartHeight} S${0.92 * chartWidth} ${(0.42 - extra) * chartHeight} ${chartWidth} ${0.85*chartHeight} L${chartWidth} ${chartHeight}`}
                      fill="#787885" style={{opacity: '0.2'}}/>
                  <path fill = "transparent" stroke="#787885" strokeWidth="4" d={`M${0.100 * chartWidth} ${chartHeight - (0.385* chartHeight)} C${0.22 * chartWidth} ${0.35 * chartHeight}, ${0.34 * chartWidth} ${0.35 * chartHeight}, ${0.47 * chartWidth} ${0.60 * chartHeight} S${0.70 * chartWidth} ${0.45 * chartHeight}, ${0.78 * chartWidth} ${(0.42 - extra) * chartHeight} S${0.92 * chartWidth} ${(0.42 - extra) * chartHeight} ${chartWidth} ${0.85*chartHeight}`}/>
                  <g stroke="#787885" strokeWidth="3" fill="#31313c">
                      <circle id="pointA" cx={`${0.10 * chartWidth}`} cy={chartHeight - (0.385* chartHeight)} r="12"/>
                      <circle id="pointB" cx={`${chartWidth*0.83}`} cy={chartHeight * (0.40 - (extra * 1.2))} r="12"/>
                  </g>
                  <line x1={`${0.10 * chartWidth}`} y1={chartHeight -  (0.35* chartHeight ) + 10} x2={`${0.10 * chartWidth}`} y2={`${0.76 * chartHeight }`} stroke="#787885" />
                  <line x1={`${chartWidth*0.83}`} y1={chartHeight * (0.40 - (extra * 1.2))+ 10} x2={`${chartWidth*0.83}`} y2={`${0.76 * chartHeight }`} stroke="#787885" />
                  <g font-size="12" font-family="sans-serif" fill="#ffffff" stroke="none"
                     text-anchor="middle">
                      <text x={`${0.14 * chartWidth}`}
                            y={`${0.85 * chartHeight}`}>March 21st 2020
                      </text>
                  </g>
                  <g font-size="9" font-family="sans-serif" fill="#ffffff" stroke="none"
                     text-anchor="middle">
                      <text font-weight="400" x={`${0.14 * chartWidth}`}
                            y={`${0.92 * chartHeight}`}>Launch Date
                      </text>
                  </g>
                  <g font-size="12" font-family="sans-serif" fill="#ffffff" stroke="none"
                     text-anchor="middle">
                      <text x={`${0.83 * chartWidth}`}
                            y={`${0.85 * chartHeight}`}>April 21st 2020
                      </text>
                  </g>
                  <g font-size="9" font-family="sans-serif" fill="#ffffff" stroke="none" text-anchor="middle">
                      <text x={`${0.83 * chartWidth}`}
                            y={`${0.92 * chartHeight}`}>All Time High
                      </text>
                  </g>
              </svg>

          </div>
          <div className="fund-app-four-body-lower">
              <div className="fund-app-four-name">
                  <div className="fund-app-four-name-image"><img src={face}/></div>
                  <div className="fund-app-four-name-name">
                      <div className="fund-app-four-name-name-upper">
                          <h6>Shorupan</h6>
                          <h6>234</h6>
                      </div>
                      <div className="fund-app-four-name-name-lower">
                          <h6>Verified</h6>
                      </div>

                  </div>

              </div>
              <div className="fund-app-four-other">
                  <div><h5>34.2%</h5><h6>Account ROI</h6></div>
                  <div><h5>432</h5><h6>Trades</h6></div>
                  <div><h5>41.8%</h5><h6>Winning (%)</h6></div>

              </div>
          </div>

      </div>
  )
};
export default FundAppFourComponent;
