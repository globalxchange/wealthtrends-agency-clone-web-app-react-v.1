import React, {Component, useEffect, useState} from 'react';
import './donut-chart.style.scss';
const DonutChartComponent = ({selectedValue}) =>{
    const [donut, setDonut] = useState({c: [], r: '', sd1: [], sdo1:[], sd2: [], sdo2:[]});
    const [widthHeight, setWidthHeight] = useState([]);
    const [changeSize, setChangeSize] = useState(false);
    const [colors, setColors] = useState(["#353540","#353540", "#e4e4e6" ]);
    useEffect(()=>{
        let ele = document.getElementById('donut-chart-funds');
        let x = ele.offsetWidth, y = ele.offsetHeight;
        setWidthHeight([x, y]);
        let circumstance = 2 * 3.14 * (0.32 * x);
        let divider = values.total;
        let p1 = (100 -selectedValue.value)/ 100, p2 = (selectedValue.value) / divider;
        setDonut({
            xy: [x, y], c: [0.5 * x, 0.5 * y],
            r: [0.32 * x],
            sd1: [circumstance * p1, circumstance * (1 - p1)], sdo1: 0.25 * circumstance,
            sd2: [circumstance * p2, circumstance * (1 - p2)], sdo2: ((1 - p1) + 0.25)* circumstance
        });
        window.addEventListener('resize', handleSize);
        return ()=>{
            window.removeEventListener('resize', handleSize)
        }

    },[changeSize, selectedValue]);
    useEffect(()=>{
    },[widthHeight]);
    const handleSize = () =>{
        setChangeSize(!changeSize);
    };
    return (
        <div id="donut-chart-funds" className="donut-chart-funds">
            <div style={{height: `${widthHeight[0] * 0.54}px`, width: `${widthHeight[0] * 0.54}px`}} className="donut-chart-funds-circle">
                <div>
                    <h5>{parseFloat(selectedValue.value).toFixed(1)}%</h5>
                    <h6>{selectedValue.type}</h6>
                </div>
            </div>
            <svg height="100%" width="100%">
                class="donut">
                <circle cx={donut.c[0]} cy={donut.c[1]} r={donut.r} fill="#fff"/>
                <circle cx={donut.c[0]} cy={donut.c[1]} r={donut.r} fill="transparent" stroke="#d2d3d4"
                        strokeWidth="8"/>
                <circle cx={donut.c[0]} cy={donut.c[1]} r={donut.r} fill="transparent" stroke={`${colors[1]}`}
                        strokeWidth="8"
                        strokeDasharray={`${donut.sd2[0]} ${donut.sd2[1]}`} strokeDashoffset={`${donut.sdo2}`}/>
                <circle cx={donut.c[0]} cy={donut.c[1]} r={donut.r} fill="transparent" stroke={`${colors[2]}`}
                        strokeWidth="8"
                        strokeDasharray={`${donut.sd1[0]} ${donut.sd1[1]}`} strokeDashoffset={`${donut.sdo1}`}/>
            </svg>


        </div>
    );
};
const values = {first: 40, second: 60, total: 100};
const circlevalues = {
    Liquid: '56.0',
    Allocated: '56.0',
    Shorupan1: '12.0',
    Shorupan2: '12.0',
    Shorupan3: '12.0',
    Shorupan4: '12.0',
};
export default DonutChartComponent;
