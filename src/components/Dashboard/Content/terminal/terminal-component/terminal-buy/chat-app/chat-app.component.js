import React, {Component} from 'react';
import './chat-app.styel.scss'
import {backButton, chats, fullScreenButton} from "../../../../../images/terminal-images";
const ChatAppComponent = ({setAppSelector}) =>{
    return(
        <div className="chat-app">
            <div className="chat-app-header">
                <img onClick={()=>setAppSelector("Home")} src={backButton}/>
                <img src={chats}/>
                <img src={fullScreenButton}/>
            </div>
            <div className="chat-app-body">

            </div>

        </div>
    )
};

export default ChatAppComponent;
