import React, {Component, useContext, useEffect, useState} from 'react';
import './trade-stream.style.scss'
import {stream} from "../../../../../images/terminal-images";
import TradeStreamStepOneComponent from "./step-one/trade-stream-step-one.component";
import TradeStreamStepFourComponent from "./step-four/trade-stream-step-four.component";
import {IcedTerminal} from "../../../../main-context/main.context";
const TradeStreamComponent = ({out, cId}) =>{
    const [step, setStep] = useState(1);
    const terminal = useContext(IcedTerminal);
    useEffect(()=>{
    },[cId]);

    const selectStep = () =>{
      switch (step) {
          case 1:
          case 2:
          case 3: return <TradeStreamStepOneComponent step = {step} setStep = {setStep}/>;
          case 4: return <TradeStreamStepFourComponent/>;
      }
    };
    const cross = {
        top: '50%',
        right: '5%',
        transform: 'translateY(-50%)',
        color: 'white'
    };
    return(
        <div className="trade-stream-main">
            <div className="trade-stream-header position-relative">
                <span onClick={()=>terminal.removeTab(cId)} style={out?cross:{display:'none'}} className=" position-absolute">X</span>
                <img src={stream}/>
            </div>
            <div className="trade-stream-body">
                {selectStep()}
            </div>
            <div className="trade-stream-footer">
                <h6>{cId}</h6>
            </div>
        </div>
    )
};

export default TradeStreamComponent;
