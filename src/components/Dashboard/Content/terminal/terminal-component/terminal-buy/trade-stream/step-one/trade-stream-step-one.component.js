import React, {Component, useContext} from 'react';
import {IcedTerminal} from "../../../../../main-context/main.context";
import nextId from "react-id-generator";
const TradeStreamStepOneComponent = ({step, setStep}) =>{
    const terminal = useContext(IcedTerminal);
    const {tabs, setTabs} = terminal;
    const selectButtons = () =>{
      switch (step) {
          case 1: return stepOne;
          case 2: return stepTwo;
          case 3: return stepThree;
          default: return ;
      }
    };
    const handleClick = (i) =>{
        switch (step) {
            case 1: setStep(2); break;
            case 2: !i?handleVideoChat():setStep(3);break;
            case 3: setStep(4); break;
            default: return;
        }
    };
        const handleVideoChat = () =>{
        setTabs([...tabs, {id: nextId(), type: 'Terminal Live', img: ''}], true)
      // window.open('terminal-live', '_blank');setStep(1);
    };
    return (
        <div className="trade-stream-step-one">
            <div className="step-one-text">
                <h5>{selectButtons().text}</h5>
            </div>
            {
                selectButtons().buttons.map((item,i) =>
                        <div className={item.classname}>
                            <button onClick={()=>handleClick(i)}>{item.type}</button>
                        </div>
                    )
            }
        </div>
    );
};

const stepOne = {
    text: 'When Are You Going To Stream?',
    buttons: [{type:"Go Live Now", classname :"step-one-go-live"}, {type:"Schedule For Later", classname: "step-one-later"}]
};
const stepTwo = {
    text: 'Is Your Stream Free Or Are You Going To Charge For It?',
    buttons: [{type:"No Cost", classname :"step-one-go-live"}, {type:"I Want To Charge", classname: "step-one-later"}]
};
const stepThree = {
  text: "How Do You Want To Charge?",
    buttons: [{type:"One TIme Fee", classname :"step-one-go-live"}, {type:"Assign This Stream To One Of My Pulse Plans", classname: "step-one-later"}]
};

export default TradeStreamStepOneComponent;
