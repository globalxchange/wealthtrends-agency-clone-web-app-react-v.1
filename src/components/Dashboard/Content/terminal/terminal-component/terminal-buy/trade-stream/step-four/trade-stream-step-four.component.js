import React, {Component} from 'react';
import {BTC, ETH} from "../../../../../../images/terminal-images";

const TradeStreamStepFourComponent = () =>{
    return(
        <div className="trade-stream-step-four">
            <div className="step-four-text">
                <h5>How Do You Want To Charge?</h5>
            </div>
            <div className="debited-container">
                <h6>Your Liquid USDT Vault Will Be Debited</h6>
                <div>
                    <h6><img src={BTC}/>Bitcoin</h6><input placeholder="0.0000"/>
                </div>
            </div>
            <div className="credited-container">
                <h6>Your ICP BTC Vault Will Be Credited</h6>
                <div>
                    <h6><img src={ETH}/>Etheruem</h6><input placeholder="0.0000"/>
                </div>
            </div>

        </div>
    )
};

export default TradeStreamStepFourComponent;
