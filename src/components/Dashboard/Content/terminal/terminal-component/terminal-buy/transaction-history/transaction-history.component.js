import React, {useContext, useEffect, useState} from 'react';
import './transaction-history.style.scss';
import {IcedTerminal} from "../../../../main-context/main.context";
import {arrowDownBlack} from "../../../../../images/terminal-images";
const TransactionHistoryComponent = () =>{
    const [state, setState] = useState({size: {height: 0, width: 0}, drop: false});
    const [selected, setSelected] = useState(data[0]);
    const icedTerminal = useContext(IcedTerminal);
    const {size} = state;
    useEffect(()=>{
        let ele = document.getElementById('history');
        setState({size: {height: ele.offsetHeight, width: ele.offsetWidth}});
    },[icedTerminal.changeChart]);
    const handleImageClick = (x) =>{
        if(x.id === selected.id && state.drop === true){
            setState({...state, drop: false})

        }else {
            setState({...state, drop: true})
        }
    };
    return(
        <div id = "history" className="transaction-history">
            <div className="transaction-history-header">
                <h6>Transaction History</h6>

            </div>
            <div className='transaction-history-body'>
               {/*352*/}
               <div className={size.width > 767?"history-table-header":'d-none'}>
                   {obj.map(item =><div>{item.display}</div>)}

               </div>
                <div className="history-table-body">
                    {data.map((x)=><div onClick={()=>setSelected(x)} className={size.width>767?"table-row-container":"table-row-container-small position-relative"}>
                        <img style={size.width >767?{display: 'none'}:{}} onClick={()=>handleImageClick(x)} className={`arrow-down ${state.drop && x.id === selected.id?'yes':'no'}`} src={arrowDownBlack}/>
                        {
                            obj.map((item)=><div className={state.drop && x === selected?'drop-down-active':'drop-down-disabled'}>
                                <h6 className={size.width > 767?'d-none':''}>{item.display}</h6><h6>{x[item.find]}</h6>
                            </div>)
                        }
                    </div>)}

                </div>

            </div>
        </div>
    )
};
const obj = [{display: 'Crypto Trade', find: 'trade'},{display: 'Sender Id', find: 'sId'},{display: 'Transaction Id', find: 'transactionId'},{display: 'Time', find: 'time'},{display: 'Status', find: 'status'},{display: 'Amount', find: 'amount'}];
const data = [
    {
        id: 0,trade: 'Bitcoin', sId: 'sfd-5scddd', transactionId: 'D6GHHF7', time: '10:20:45', status:'completed', amount: '0.0024BTC'
    },
    {
        id: 2,trade: 'Bitcoin', sId: 'sfd-5scddd', transactionId: 'D6GHHF7', time: '10:20:45', status:'completed', amount: '0.0024BTC'
    },
    {
        id: 3,trade: 'Bitcoin', sId: 'sfd-5scddd', transactionId: 'D6GHHF7', time: '10:20:45', status:'completed', amount: '0.0024BTC'
    },
    {
        id: 4, trade: 'Bitcoin', sId: 'sfd-5scddd', transactionId: 'D6GHHF7', time: '10:20:45', status:'completed', amount: '0.0024BTC'
    },
    {
        id: 5,trade: 'Bitcoin', sId: 'sfd-5scddd', transactionId: 'D6GHHF7', time: '10:20:45', status:'completed', amount: '0.0024BTC'
    },
    {
        id: 6,trade: 'Bitcoin', sId: 'sfd-5scddd', transactionId: 'D6GHHF7', time: '10:20:45', status:'completed', amount: '0.0024BTC'
    },
];

export default TransactionHistoryComponent;
