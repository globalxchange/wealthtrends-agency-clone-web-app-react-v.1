import React from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../../../assets/a-exporter';
import LoadingAnimation from '../../../../../../lotties/LoadingAnimation';
import { decryptPlaceTrade, placeTradeAPI } from '../../../../../../services/postAPIs';
import { Agency } from '../../../../../Context/Context';
import './buy-sell.style.scss'
export default function BuySellComponent() {
    const agency = React.useContext(Agency);
    const { tradePairObj, mainBalance, changeNotification, setMainBalance, resetFullTradeTerminal, valueFormatter, currentApp, valueFormatterInput } = agency;
    const [selectedHeader, setSelectedHeader] = React.useState(panelHeader[0]);
    const [sliderValue, setSliderValue] = React.useState(0);
    const [selectedType, setSelectedType] = React.useState(dropdownList[0]);
    const [dropdown, setDropdown] = React.useState(false)
    const [buyValue, setBuyValue] = React.useState('');
    const [balance, setBalance] = React.useState(valueFormatter(0, "USD"));
    const [balanceInWallet, setBalanceInWallet] = React.useState('');
    const [finalRate, setFinalRate] = React.useState('');
    const [loading, setLoading] = React.useState('')

    const findBaseValue = () => {
        let val = [...mainBalance.crypto, ...mainBalance.fiat].find(obj => { return obj.coinSymbol === tradePairObj?.quote?._id })
        let bal = (val.coinValue)
        setBalance(valueFormatter(0, tradePairObj?.quote?._id));
        !bal ? setSliderValue(100) : console.log()
        setBalanceInWallet(bal)
    }
    const setUpRate = () => {
        selectedType.id ? setFinalRate(1 / tradePairObj.exchangeBank.finalRate) : setFinalRate(tradePairObj.exchangeBank.finalRate)
    }
    React.useEffect(() => {
        if (!tradePairObj.exchangeBank) return
        setUpRate()
    }, [selectedType])

    const handleChange = (e) => {

        if (e.target.value > 100 || e.target.value < 0 || !balanceInWallet || !finalRate) return;
        let bal = ((balanceInWallet) * (e.target.value / 100))
        setBalance(valueFormatter(bal, tradePairObj.quote._id));
        setSliderValue(e.target.value);
        selectedType.id ?
            setBuyValue(valueFormatterInput(bal * (finalRate), tradePairObj.base._id))
            :
            setBuyValue(valueFormatterInput(bal, tradePairObj.quote._id))
    }
    const reset = () => {
        setBuyValue('');
        // resetFullTradeTerminal()
    }

    const handleSubmit = async () => {
        setLoading(true);
        let token = localStorage.getItem('idToken');
        let email = localStorage.getItem('userEmail');
        let tempObj;
        if (selectedType.id) {
            tempObj = {
                token: token,
                email: email,
                app_code: currentApp.app_code,
                profile_id: currentApp.profile_id,
                purchased_amount: buyValue,
                stats: false,
                path_id: tradePairObj?.exchangeBank?.pathId
            }
        } else {
            tempObj = {
                token: token,
                email: email,
                app_code: currentApp.app_code,
                profile_id: currentApp.profile_id,
                from_amount: buyValue,
                stats: false,
                path_id: tradePairObj?.exchangeBank?.pathId
            }
        }
        let first = await placeTradeAPI(tempObj)
        let second = await decryptPlaceTrade(first.data.data);
        if (second.data.status) {
            setLoading(false)
            reset();
            setMainBalance();
            changeNotification({ status: true, message: "Congratulations...!! Your Trade Was Successful" })
            findBaseValue();
        } else {
            setLoading(false)
            reset();
            changeNotification({ status: false, message: "Sorry... Ypu Trade Failed. Please Try Again" })

        }
    }

    React.useEffect(() => {
        if (!finalRate) return
        setSliderValue(0);
        if (selectedType.id) {
            setBuyValue(valueFormatterInput(0, tradePairObj.base._id))
            setBalance(valueFormatter(0, tradePairObj.base._id))
        } else {
            setBuyValue(valueFormatterInput(0, tradePairObj.quote._id))
            setBalance(valueFormatter(0, tradePairObj.quote._id))
        }

    }, [selectedType])

    React.useEffect(() => {
        setFinalRate('')
        if (!tradePairObj.base) return
        findBaseValue()
    }, [tradePairObj.quote])

    React.useEffect(() => {
        setFinalRate('')
    }, [tradePairObj.base])

    React.useEffect(() => {
        if (!tradePairObj.exchangeBank) return
        setUpRate()
    }, [tradePairObj.exchangeBank])


    return (
        <div className="buy-sell-main">
            <div className="buy-sell-header">
                <button>Sell</button>
                <button>Buy</button>
            </div>
            <div className="buy-sell-body">
                <div className="buy-sell-panel-header">
                    {
                        panelHeader.map(obj => <span className={obj.keyId === selectedHeader.keyId ? "selected" : ""}>
                            {obj.name}
                        </span>)
                    }
                </div>
                <div className="buy-sell-panel">
                    {loading ?
                        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                            <LoadingAnimation />
                        </div>
                        :
                        <>
                            <div className="panel-market-wrapper">
                                <div>
                                    <span>Price</span>
                                    <input value="Market" />

                                </div>
                                <button>{!tradePairObj.quote ? "TBD" : tradePairObj.quote._id}</button>
                            </div>
                            <div className="panel-amount-wrapper">
                                <div>
                                    <span onClick={() => setDropdown(!dropdown)}>
                                        {selectedType.name}
                                        <img src={Images.triangle} />
                                        <div className={dropdown ? "" : "d-none"}>
                                            {
                                                dropdownList.map(obj =>
                                                    <h6 onClick={() => setSelectedType(obj)}>{obj.name}</h6>
                                                )
                                            }

                                        </div>
                                    </span>
                                    <input
                                        disabled={!finalRate || !tradePairObj.exchangeBank}
                                        value={buyValue}
                                        onChange={e => setBuyValue(e.target.value)}
                                        placeholder="0.0000"
                                    />

                                </div>
                                {
                                    selectedType.id ?
                                        <button>{!tradePairObj.base ? "TBD" : tradePairObj.base._id}</button>
                                        :
                                        <button>{!tradePairObj.quote ? "TBD" : tradePairObj.quote._id}</button>
                                }

                            </div>
                            <label className="range-wrapper" htmlFor="slider">
                                <p>Drag The Bar</p>
                                <div style={{ width: `calc(18% + ${sliderValue * 0.82}%)` }} className="range-left">
                                    <span>{balance + ' ' + `${tradePairObj?.quote?._id}`}</span>
                                </div>
                                <div style={{ width: `calc(100% -18% + ${sliderValue * 0.82}% )` }} className="range-right">

                                </div>
                                <input
                                    disabled={!finalRate || !tradePairObj.exchangeBank}
                                    value={sliderValue}
                                    onChange={handleChange}
                                    type="range"
                                    min="-20" max="120"
                                    className="slider"
                                    id="myRange"
                                />
                            </label>
                        </>
                    }

                </div>

            </div>
            <div className="buy-sell-footer">
                <button
                    disabled={!finalRate || !tradePairObj.exchangeBank || buyValue > (balanceInWallet * finalRate) || !balanceInWallet}

                    onClick={handleSubmit}>{
                        selectedType.id ?
                            `Buy ${buyValue} ${!tradePairObj?.base ? '' : tradePairObj?.base?._id}`
                            :
                            `Buy ${buyValue} ${tradePairObj?.quote?._id} Worth Of ${tradePairObj?.base?._id} `
                    }</button>

            </div>

        </div>
    )
}

const panelHeader = [
    { keyId: nextId(), name: "Market", id: "market" },
    { keyId: nextId(), name: "Limit", id: "limit" },
    { keyId: nextId(), name: "Stop Limit", id: "stop" },
    { keyId: nextId(), name: "Algo", id: "algo" },
]

const dropdownList = [
    { keyId: nextId(), name: "Amount", id: true },
    { keyId: nextId(), name: "Total", id: false },
]