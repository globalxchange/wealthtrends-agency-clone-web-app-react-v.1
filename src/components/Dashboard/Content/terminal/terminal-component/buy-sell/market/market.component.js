import React, { Component, useContext, useEffect, useState } from "react";
// import './market.style.scss';
import nextId from "react-id-generator";
import { IcedTerminal } from "../../../../main-context/main.context";
const MarketComponent = () => {
  const [price, setPrice] = useState(0);
  const [amount, setAmount] = useState(0);
  const [multiplier, setMultiplier] = useState(0);
  const [amountValue, setAmountValue] = useState("");
  const [priceValue, setPriceValue] = useState("");
  const [ratePrice, setRatePrice] = useState("");
  const [rateAmount, setRateAmount] = useState("");
  const [totals, setTotals] = useState("");
  const [selectedPercent, setSelectedPercent] = useState(0);
  const terminal = useContext(IcedTerminal);
  const {
    apiCurrency,
    selectAsset,
    cryptoUSD,
    terminalStage,
    liquidTerminalList,
    placeMarketTrade,
    resetBuySellValue,
  } = terminal;
  useEffect(() => {
    require("./market.style.scss");
    setMultiplier(selectAsset.price);
    setRateAmount(1 / cryptoUSD[`${apiCurrency[0].coin}`]);
    apiCurrency[1].coin === "USD"
      ? setRatePrice(1)
      : setRatePrice(1 / cryptoUSD[`${apiCurrency[1].coin}`]);
    let a = {};
    if (liquidTerminalList.length > 0) {
      liquidTerminalList.map((x) => {
        a = { ...a, [x.coin]: [x.balance, x.USDeqv] };
      });
      setTotals(a);
    }
  }, [selectAsset, apiCurrency, terminalStage]);
  useEffect(() => {
    setAmountValue("");
    setPriceValue("");
  }, [apiCurrency, selectAsset, terminalStage]);
  useEffect(() => {
    setAmountValue("");
    setPriceValue("");
    setPrice(0);
    setAmount(0);
  }, [resetBuySellValue]);
  const handleAmount = (value) => {
    if (value === "") {
      setAmountValue("");
      setPriceValue("");
      return;
    }
    setAmountValue(value);
    if (amount === 0) {
      let x = value * multiplier;
      if (price === 0) {
        setPriceValue(parseFloat(x).toFixed(5));
      } else {
        let y = x * (1 / ratePrice);
        setPriceValue(parseFloat(y).toFixed(5));
      }
    } else {
      let x = value * rateAmount * multiplier;
      console.log("calculation", value, rateAmount, multiplier);
      if (price === 0) {
        setPriceValue(parseFloat(x).toFixed(5));
      } else {
        let y = x * (1 / ratePrice);
        setPriceValue(parseFloat(y).toFixed(5));
      }
    }
  };
  const handlePrice = (value) => {
    if (value === "") {
      setPriceValue("");
      setAmountValue("");
      return;
    }
    setPriceValue(value);
    if (price === 0) {
      let x = value * (1 / multiplier);
      if (amount === 0) {
        setAmountValue(parseFloat(x).toFixed(5));
      } else {
        let y = x * (1 / rateAmount);
        setAmountValue(parseFloat(y).toFixed(5));
      }
    } else {
      let x = value * ratePrice * (1 / multiplier);
      if (amount === 0) {
        setAmountValue(parseFloat(x).toFixed(5));
      } else {
        let y = x * (1 / rateAmount);
        console.log("calculation USD USD", x, 1 / rateAmount, y);
        setAmountValue(parseFloat(y).toFixed(5));
      }
    }
  };
  const handlePercent = (obj) => {
    setSelectedPercent(obj.value);
    price === 0
      ? handlePrice(
          parseFloat(totals[`${apiCurrency[1].coin}`][0]).toFixed(5) * obj.value
        )
      : handlePrice(
          parseFloat(totals[`${apiCurrency[1].coin}`][1]).toFixed(5) * obj.value
        );
  };
  const handleSubmit = (type) => {
    if (type === "buy") {
      if (amount === 1) {
        placeMarketTrade(
          selectAsset.exchangeId,
          apiCurrency[0].coin,
          apiCurrency[1].coin,
          amountValue * rateAmount,
          "buy"
        );
      } else {
        placeMarketTrade(
          selectAsset.exchangeId,
          apiCurrency[0].coin,
          apiCurrency[1].coin,
          amountValue,
          "buy"
        );
      }
    } else {
      if (amount === 1) {
        placeMarketTrade(
          selectAsset.exchangeId,
          apiCurrency[0].coin,
          apiCurrency[1].coin,
          amountValue * rateAmount,
          "sell"
        );
      } else {
        placeMarketTrade(
          selectAsset.exchangeId,
          apiCurrency[0].coin,
          apiCurrency[1].coin,
          amountValue,
          "sell"
        );
      }
    }
  };
  return (
    <div className={terminal.darkMode ? "market-main-dark" : "market-main"}>
      <div className="market-main-dashboard">
        <div className="market-amount">
          <div className="market-amount-above">
            <h6>Amount</h6>
            <h6>
              {buttons.map((obj) => (
                <button
                  className={selectedPercent === obj.value ? "selected" : ""}
                  disabled={
                    selectAsset.exchangeId === "Loading..." ||
                    terminalStage !== 6
                  }
                  onClick={() => handlePercent(obj)}
                  key={obj.keyId}
                >
                  {obj.percent}
                </button>
              ))}
            </h6>
          </div>
          <div className="market-amount-below">
            <h6>
              {[apiCurrency[0].coin, "USD"].map((item, i) => (
                <button
                  disabled={
                    selectAsset.exchangeId === "Loading..." ||
                    terminalStage !== 6
                  }
                  onClick={() => {
                    setAmount(i);
                    setPriceValue("");
                    setAmountValue("");
                  }}
                  className={i === amount ? "selected" : ""}
                  key={item}
                >
                  {item === "" ? "B.P." : item}
                </button>
              ))}
            </h6>
            <input
              onKeyPress={terminal.handleKeyPress}
              disabled={
                selectAsset.exchangeId === "Loading..." || terminalStage !== 6
              }
              value={amountValue}
              onChange={(e) => handleAmount(e.target.value)}
              placeholder={`0.00 ${amount === 1 ? "USD" : apiCurrency[0].coin}`}
            />
          </div>
        </div>
        <div className="market-price">
          <h6>Price</h6>
          <div className="market-price-input">
            <h6>
              {[apiCurrency[1].coin, "USD"].map((item, i) => (
                <button
                  disabled={
                    selectAsset.exchangeId === "Loading..." ||
                    terminalStage !== 6
                  }
                  onClick={() => {
                    setPrice(i);
                    setPriceValue("");
                    setAmountValue("");
                  }}
                  className={i === price ? "selected" : ""}
                >
                  {item === "" ? "Q.P" : item}
                </button>
              ))}
            </h6>
            <input
              onKeyPress={terminal.handleKeyPress}
              disabled={
                selectAsset.exchangeId === "Loading..." || terminalStage !== 6
              }
              value={priceValue}
              onChange={(e) => handlePrice(e.target.value)}
              placeholder={`0.00 ${price === 1 ? "USD" : apiCurrency[1].coin}`}
            />
          </div>
        </div>
      </div>
      <div className="market-main-buttons">
        {["SELL", "BUY"].map((item) => (
          <button onClick={() => handleSubmit(item.toLowerCase())}>
            {item}
          </button>
        ))}
      </div>
    </div>
  );
};
const buttons = [
  { keyId: nextId(), percent: "Max", value: 1 },
  { keyId: nextId(), percent: "25%", value: 0.25 },
  { keyId: nextId(), percent: "50%", value: 0.5 },
];

export default MarketComponent;
