import React, {useContext, useEffect, useRef, useState} from 'react';
import './terminal-c.style.scss';
import TerminalProfileComponent from "./terminal-profile/terminal-profile.component";
import ChartTransactionTrendingComponent from "./chart-transaction-trending/chart-transaction-trending.component";
import LiquidPassiveManagedComponent from "./liquid-passive-managed/liquid-passive-managed.component";
import TerminalBuyComponent from "./terminal-buy/terminal-buy.component";
import {IcedTerminal} from "../../main-context/main.context";
import ReactGridLayout from "react-grid-layout";
import ChatAppComponent from "./terminal-buy/chat-app/chat-app.component";
import TradeStreamComponent from "./terminal-buy/trade-stream/trade-stream.component";
import FundAppComponent from "./terminal-buy/fund-app/fund-app.component";
import BinanceComponent from "./binance/binance.component";
import BuySellComponent from "./buy-sell/buy-sell.component";
import LoadingAnimation from '../../../../../lotties/LoadingAnimation';
import TradeIndexTerminal from '../../../TradeTerminal/TradeIndexTerminal';
const TerminalComponent = React.memo(()=>{
    const terminal  = useContext(IcedTerminal);
    const [elementSize, setElementSize] = useState({height: 0, width: 0});
    const [changeSize, setChangeSize] = useState(false);
    const [layout, setLayout] = useState([]);
    const {layoutState} = terminal;
    const dashRef = useRef();
    useEffect(()=>{
        terminal.getExchangeList();
        terminal.getAvailablePairsList();
        if(terminal.terminalCurrencyList.length !== 0)
            terminal.setLiquidTerminalList();
    },[terminal.mainLoader]);

    useEffect(()=>{
        let b = document.getElementById("dashboard");
        setElementSize({height: b.offsetHeight, width: b.offsetWidth});
        window.addEventListener('resize', handleChange)
    },[changeSize,dashRef.current?.offsetWidth, terminal.selectedTab]);
    useEffect(()=>{
        terminal.setSelectedTab({selectedTab: terminal.tabs[0]}, true);
    },[]);
    const handleChange = () =>{
      setChangeSize(!changeSize);
    };
    useEffect(()=>{
        let arr = [];
        layoutState.map(x => {
            arr.push({i: x.component[0], x: x.size[2], y: x.size[3],w: x.size[0], h:x.size[1]})
        });
        console.log('layout', arr);
        setLayout(arr);
    },[layoutState]);

    const selectComponent = (val, id) =>{
        switch (val) {
            case "profile": return <TerminalProfileComponent/>;
            case "chart": return <ChartTransactionTrendingComponent/>;
            case "convert": return <BuySellComponent/>;
            case "liquid": return <LiquidPassiveManagedComponent/>;
            case "exchange": return <TradeIndexTerminal />
            case "buy": return  <TerminalBuyComponent/>;
            case "Chats": return <ChatAppComponent/>;
            case "Trade Stream": return <TradeStreamComponent out = {true} cId ={id}/>;
            case "Funds": return <FundAppComponent independent={true} cId={id}/>;
            case "binance": return <BinanceComponent/>;
            default:return ;
        }
    };
    return (
        <div className="w-100 h-100 dashboard-container" ref={dashRef} id="dashboard">
            {
                false?
                    <div className="terminal-component-loading">
                        <LoadingAnimation />
                    </div> :
                    <ReactGridLayout onResize={(e) => terminal.setChangeChart()} margin={[0, 0]} isResizable={true}
                                     className="layout" layout={layout} isDraggable ={false}
                                     cols={12} rowHeight={elementSize.height * 0.10} width={elementSize.width - 3}>
                        {
                            layoutState.map(res =>
                                <div className="p-2"
                                     key={res.component[0]}>{selectComponent(res.component[1], res.component[0])}</div>
                            )}
                    </ReactGridLayout>
            }
        </div>
    );
});
export default TerminalComponent;
