import React, {Component, useContext, useEffect, useState} from 'react';
import './manual.style.scss';
import {IcedTerminal} from "../../../../main-context/main.context";
import ExecutionLoading from "../../../../../lotties/execution-loading";
const ManualComponent = ({ conversionButton, setConversionButtons, selectConversionButton, setSelectConversionButton}) =>{
    let total = 0;
    const icedTerminal = useContext(IcedTerminal);
    const [totals, setTotals] = useState({ETH: [], USD: [], BCH: [], BTC: []});
    const [multiplier, setMultiplier] = useState(1);
    const {selectAsset,apiCurrency,liquidTerminalList,setTerminalExecutionValue,buySell, formatter, terminalExecutionTotalValue, value, setValue} = icedTerminal;
    useEffect(()=>{

        if(apiCurrency[1].coin === '')
        {setConversionButtons([ {type:'BP', coin: apiCurrency[1].coin}, {type:'QP', coin: apiCurrency[0].coin}])}
        else if( apiCurrency[1].coin !== 'USD'){
            setConversionButtons([{type:'USD', coin: 'USD'}, {type:'BP', coin: apiCurrency[1].coin}, {type:'QP', coin: apiCurrency[0].coin}])
        }else{
            setConversionButtons([{type:'BP', coin: apiCurrency[1].coin}, {type:'QP', coin: apiCurrency[0].coin}])
        }
        setValue(''); setTerminalExecutionValue(0);
        buySell?setMultiplier(selectAsset.price):setMultiplier(selectAsset.price);
        let a ={};
        if(liquidTerminalList.length > 0){
            liquidTerminalList.map((x)=>{a = {...a,[x.coin]: [x.balance, x.USDeqv]}});setTotals(a);
        }
    },[liquidTerminalList,apiCurrency, selectAsset, buySell]);

    const handlePercentage = (val, percentage)=>{
        if(val === '') {
            setValue('');setTerminalExecutionValue(0);return;
        }
        percentage === 0?setValue(val):console.log();
        if(selectConversionButton.type === 'BP'){
            if(percentage === 0.5){
                (apiCurrency[1].coin === 'USD') ? setValue(parseFloat(totals[apiCurrency[1].coin][0] * 0.5).toFixed(2))
                    : setValue(parseFloat(totals[apiCurrency[1].coin][0] * 0.5).toFixed(5));
                setTerminalExecutionValue((totals[apiCurrency[1].coin][0] * 0.5)/multiplier);
            }
            else if(percentage === 0.25){
                (apiCurrency[1].coin === 'USD') ? setValue(parseFloat(totals[apiCurrency[1].coin][0] * 0.25).toFixed(2))
                    : setValue(parseFloat(totals[apiCurrency[1].coin][0] * 0.25).toFixed(5));
                setTerminalExecutionValue((totals[apiCurrency[1].coin][0] * 0.25)/multiplier);
            }else{
                (apiCurrency[1].coin === 'USD') ? setValue(val)
                    : setValue(val);
                setTerminalExecutionValue((val)/multiplier);

            }
        }else if(selectConversionButton.type === 'QP'){
            if(percentage === 0.5){
                setValue(parseFloat(totals[apiCurrency[0].coin][0] * 0.5).toFixed(5));
                setTerminalExecutionValue((totals[apiCurrency[0].coin][0] * 0.5)*multiplier);
            }
            else if(percentage === 0.25){
                setValue(parseFloat(totals[apiCurrency[0].coin][0] * 0.25).toFixed(5));
                setTerminalExecutionValue((totals[apiCurrency[0].coin][0] * 0.25)*multiplier);
            }else {
                setValue(val);
                setTerminalExecutionValue((val)*multiplier);
            }
        }else if(selectConversionButton.type === 'USD'){
            if(percentage === 0.5){
                setValue(parseFloat(totals[apiCurrency[0].coin][1] * 0.5).toFixed(2));
                setTerminalExecutionValue((totals[apiCurrency[0].coin][0] * 0.5)*multiplier);
            }
            else if(percentage === 0.25){
                setValue(parseFloat(totals[apiCurrency[0].coin][1] * 0.25).toFixed(2));
                setTerminalExecutionValue((totals[apiCurrency[0].coin][0] * 0.25)*multiplier);
            }else{
                setValue(val);
                setTerminalExecutionValue((val)*multiplier);
            }
        }
    };
    return (
        icedTerminal.executionMarketLoading ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <ExecutionLoading/>
            </div> :
            <div className="manual-component">
                <div className="enter-size-container">
                    <div className="enter-size-container-text">
                        <span>Enter Order Amt. In</span>
                        <span>{
                            conversionButton.map((x) => <button
                                onClick={() => {
                                    setValue('');
                                    setTerminalExecutionValue(0);
                                    setSelectConversionButton(x)
                                }}
                                className={selectConversionButton.type === x.type ? 'selected' : ''}>{x.coin === '' ? x.type : x.coin}</button>)
                        }
                    </span>
                    </div>
                    <div className="enter-size-container-input">
                        <button disabled={selectAsset.exchangeId.includes('...')}
                                onClick={() => handlePercentage(0, 0.25)}>25%
                        </button>
                        <div className="input-container">
                            <input disabled={selectAsset.exchangeId.includes('...')}
                                   onChange={(e) => handlePercentage(e.target.value, 0)} value={value}
                                   onKeyPress={(e) => (e.which < 46 || e.which > 57) ? e.preventDefault() : ''}
                                   placeholder="Enter"/>
                        </div>
                        <button disabled={selectAsset.exchangeId.includes('...')}
                                onClick={() => handlePercentage(0, 0.5)}>50%
                        </button>
                    </div>
                </div>
                <div className="exchange-container">
                    <p>Exchange</p>
                    <div className={selectAsset.image === '' ? 'image-not-loaded' : ''}>
                        {selectAsset.image === '' ? <h6>Select Exchange</h6> : <img src={selectAsset.fullImageBW}/>}
                    </div>
                </div>
                <div className="binance-market-price">
                    <p>{selectAsset.exchangeId} Market Price</p>
                    <div>
                    <span>{ apiCurrency[1].coin === 'USD' || apiCurrency[1].coin=== 'USDT'? formatter.format(multiplier) : (apiCurrency[1].coin,parseFloat(multiplier)) }
                    </span>
                        <span> {apiCurrency[1].coin === 'USD' || apiCurrency[1].coin=== 'USDT' ? '' : 'BTC'}</span>
                    </div>
                </div>
            </div>

    );
};


export default ManualComponent;
