import React, {useContext, useState} from 'react';
import './managed-conversion.style.scss';
import {IcedTerminal} from "../../../../main-context/main.context";
const ManagedConversionComponent = () =>{
    const [firstButton , setFirstButton] = useState(butonOne[0]);
    const [secondButton , setSecondButton] = useState(buttonTwo[0]);
    const terminal = useContext(IcedTerminal);
    return(
        <div className="managed-conversion">
            <div className="managed-conversion-body">
                <div className="managed-conversion-body-scroll">
                    {terminal.terminalCurrencyList.map((x)=><button>
                        <img src={x.asset_type === 'Crypto'?x.coinImage:x.coinFlag}/>
                    </button>)}
                </div>
                <div className="managed-conversion-body-main">
                    <div className="managed-conversion-body-main-top-section">
                        <div className="managed-market-limit-section">
                            {butonOne.map((x)=>
                                <button
                                    className={firstButton === x?'selected': ''}
                                    onClick={()=>setFirstButton(x)}
                                >{x}</button>)}
                        </div>
                        <div className="managed-buy-sell-section">
                            {buttonTwo.map((x)=>
                                <button
                                    className={x === secondButton?'selected':''}
                                    onClick={()=>setSecondButton(x)}
                                >{x}</button>)}
                        </div>
                    </div>
                    <div className="managed-conversion-body-main-top-section-input">
                        <div>
                            <input placeholder="" required/><span>Enter Amount</span><img src={terminal.fromCurrency.coinImage}/>
                        </div>
                        <div>
                            <input placeholder="" required/><span>Price of BTC</span><img src={terminal.fromCurrency.coinFlag}/>
                        </div>

                    </div>

                </div>

            </div>
            <div className="managed-conversion-footer">
                <button>$7,459</button>
                <button>$0.00</button>
                <button><i className="fas fa-angle-right"/></button>

            </div>
        </div>
    )
};

const butonOne = ["Market", "Limit"];
const buttonTwo = ["Buy", "Sell"]

export default ManagedConversionComponent;
