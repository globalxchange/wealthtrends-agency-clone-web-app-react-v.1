import React, {useState, useContext} from 'react';
import {IcedTerminal} from "../../../../main-context/main.context";
const LiquidConversionComponent = () =>{
    const terminal = useContext(IcedTerminal);
    const [selectedButton, setSelectedButton] = useState(buySellButton[0]);
    return(
        <div className="liquid-conversion">
            <div className="liquid-conversion-body">
                <div className="liquid-country-scroll">
                    {terminal.terminalCurrencyList.map(
                        (x)=><button><img src={x.asset_type==='Crypto'?x.coinImage:x.coinFlag}/></button>
                    )}
                </div>
                <div className="liquid-main-conversion">
                    <div className="liquid-main-conversion-top">
                        {
                            buySellButton.map((x)=>
                                <button
                                    className = {selectedButton === x?'selected': ''}
                                    onClick={()=>setSelectedButton(x)}>{x}</button>)
                        }

                    </div>
                    <div className="liquid-main-conversion-body">

                        <div><input placeholder="" required/><span>Enter Amount</span><img src={terminal.fromCurrency.coinImage}/></div>
                        <div><input placeholder="" required/><span>Amount of IFO</span></div>
                    </div>

                </div>

            </div>
            <div className="liquid-conversion-footer">
                <button>$7,959</button>
                <button>$0.00</button>
                <button><i className="fas fa-angle-right"/></button>

            </div>
        </div>
    )
};
const buySellButton = ["Buy", "Sell"];

export default LiquidConversionComponent;
