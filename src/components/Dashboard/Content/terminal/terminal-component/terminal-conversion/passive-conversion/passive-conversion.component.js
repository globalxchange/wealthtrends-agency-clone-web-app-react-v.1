import React, {Component, useState} from 'react';
import './passive-conversion.style.scss';
const PassiveConversionComponent = () =>{
    const [buySell, setBuySell] = useState(buySellButton[0]);
    const [period, setPeriod] = useState(radio[0]);
    return (
        <div className="passive-conversion">
            <div className="passive-conversion-header">
                {buySellButton.map((x) =>
                    <button
                        onClick={() => setBuySell(x)}
                        className={buySell === x ? 'selected-buy-sell' : ''}
                    >{x}</button>)}

            </div>
            <div className="passive-conversion-body">
                <div className="passive-conversion-body-radio">
                    {
                        radio.map((x) =>
                            <label for = {x.value} className="passive-one-line">
                            <input id ={x.value} type="radio" checked={x.value === period.value} onChange={() => setPeriod(x)}
                                   value={x.value} name="duration"/><span>{x.display}</span>
                        </label>)
                    }
                </div>
                <div className="passive-conversion-body-enter-amount">
                    <div className="passive-input-container">
                        <input type="number" required/>
                        <span>Enter Amount</span>
                    </div>
                    <h6>BTC</h6>
                </div>
                <div className="passive-conversion-body-interest-rate">
                    <input type="text" value="6%" disabled={true}/>
                    <span>Locked Interest Rate %</span>
                </div>

            </div>
            <div className="passive-conversion-footer">
                <div>Total</div>
                <div>
                    <span>0.00 USD</span>
                    <h6>Deposit BTC</h6>

                </div>

            </div>
        </div>
    );
};

const radio = [{value: 3, display: '3 Months'}, {value: 6, display: '6 Months'}];
const buySellButton = ["Buy","Sell"];

export default PassiveConversionComponent;
