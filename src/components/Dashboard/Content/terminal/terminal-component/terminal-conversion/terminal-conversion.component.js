import React, {useContext, useEffect, useState} from 'react';
import './terminal-conversion.style.scss';
import {IcedTerminal} from "../../../main-context/main.context";
import ManualComponent from "./manual/manual.component";
import AlgoComponent from "./algo/algo.component";
import {fullScreenButton} from "../../../../images/terminal-images";
const TerminalConversionComponent = React.memo(()=>{
    const header = ["Market", "Limit"];
    const terminal = useContext(IcedTerminal);
    const {layoutState} = terminal;
    const [selectHeader, setSelectHeader] = useState(header[0]);
    const [conversionButton, setConversionButtons] = useState([ {type:'BP', coin: 'BP'}, {type:'QP', coin: 'QP'}]);
    const [selectConversionButton, setSelectConversionButton] = useState(conversionButton[conversionButton.length-2]);
    useEffect(()=>{

    },[terminal.LPMHeader, terminal.commonSubHeader, terminal]);

    const selectComponent = () =>{
        switch (terminal.subHeader) {
            case "Terminal": if(selectHeader === 'Market'){
                return <ManualComponent

                    conversionButton = {conversionButton}
                    setConversionButtons = {setConversionButtons}
                    selectConversionButton = {selectConversionButton}
                    setSelectConversionButton = {setSelectConversionButton}
                />}
                             else {return <AlgoComponent/>}
            default: return ;

        }
    };
    const handleFullSize = () =>{
        if(terminal.layoutState[4].size[1]=== 10){
            terminal.changeLayout([{size: [9,5,0,0], component: ['b', 'chart']},  layoutState[1], layoutState[2], layoutState[3],{size: [3,5,9,5], component: ['c', 'convert']}])
        }else{
            terminal.changeLayout([{size: [9,5,0,10], component: ['b', 'chart']}, layoutState[1], layoutState[2], layoutState[3],{size: [12,10,0,0], component: ['c', 'convert']}])
        }
    };
    const {placeMarketTrade, selectAsset, apiCurrency, buySell, formatter, terminalExecutionTotalValue} = terminal;
    const handleClick = () =>{
        placeMarketTrade(selectAsset.exchangeId, apiCurrency[0].coin, apiCurrency[1].coin, terminalExecutionTotalValue, buySell);
    };
    return (
        <div className="terminal-conversion">
            <div className="terminal-conversion-header position-relative">
                {
                    header.map((x,i) =>
                        <button
                            onClick={() =>i===1?console.log(): setSelectHeader(x)}
                            className={selectHeader === x ? 'selected' : ''}>{x}</button>)
                }
                <img className="full-button" src={fullScreenButton} onClick={()=>handleFullSize()}/>
            </div>
            <div className="terminal-conversion-body">
                {selectComponent()}
            </div>
            <div className="terminal-conversion-footer">
                {terminal.subHeader !== "Terminal" ? '' : <>
                    <button>

                        {selectConversionButton.type === 'BP' ? terminal.valueFormatter(apiCurrency[0].coin, parseFloat(terminalExecutionTotalValue)) :
                            apiCurrency[1].coin === 'USD' ?
                                formatter.format(terminal.terminalExecutionTotalValue)
                                : terminal.valueFormatter(apiCurrency[1].coin, parseFloat(terminalExecutionTotalValue))
                        }{' '}

                        {selectConversionButton.type === 'BP' ? apiCurrency[0].coin : apiCurrency[1].coin}
                    </button>
                    {terminal.executionMarketLoading?<button>Loading...</button>:<button onClick={handleClick}>Execute
                        {terminal.buySell === "Buy" ? ' Buy' : ' Sell'}</button>}
                </>}
            </div>

        </div>
    );
});
export default TerminalConversionComponent;
