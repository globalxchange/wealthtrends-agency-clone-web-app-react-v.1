import React, {useContext, useEffect, useRef, useState} from 'react';
import socketIOClient  from 'socket.io-client';
import nextId from "react-id-generator";
import './exchange.style.scss'
import {IcedTerminal} from "../../../../main-context/main.context";

const ExchangeComponent = ({buySell}) =>{
    const terminal  = useContext(IcedTerminal);
    const [displayList, setDisplayList] = useState([]);
    const [count, setCount] = useState(0);
    const {setSelectAsset,selectAsset,baseAsset,exchangeDataTwo,numbers,setNumbers,setUpdate3, update3, exchangeList,exchangeListUpdate, exchangeTableLoader} = terminal;

    let tradeWS = useRef();
    const dL = useRef();
    const first = useRef(0);
    const second = useRef(0);
    const third = useRef(0);
    const fourth = useRef(0);
    const handleRowClick = (obj) =>{
        setSelectAsset(obj);
        terminal.setHeader("Managed");
        terminal.setSubHeader("Terminal");
    };
    useEffect(()=>{
    },[update3])
    useEffect(()=>{
        dL.current = [...exchangeList];
        setDisplayList([...exchangeList]);
        let temp = {Bitfinex: null, "Binance": null, "Kraken": null, "Okex": null};
        exchangeList.map((obj,i)=>{
            if(obj.exchangeId === 'Bitfinex')
                temp =    {...temp,Bitfinex: i };
            if(obj.exchangeId === 'Binance')
                temp =    {...temp,Binance: i };
            if(obj.exchangeId === 'Kraken')
                temp =    {...temp,Kraken: i };
            if(obj.exchangeId === 'Okex')
                temp =    {...temp,Okex: i };
            if(obj.exchangeId === "ZB")
                temp = {...temp, ZB: i};
            if(obj.exchangeId === "Bitmax")
                temp = {...temp, Bitmax: i }
        })
        setNumbers({...temp});
    },[exchangeList,exchangeListUpdate]);

    useEffect(()=>{
        if(displayList.length >= 1){
            if(count === 0){
                webSocket()
            }
            setCount(1);
        }
        let total=0 , tVolume = 0, percentT= 0, i;
        for(i = 0; i < dL.current.length; i ++){
            total = total + dL.current[i].price;
            tVolume = tVolume + dL.current[i]['24Hr_Volume'];
            percentT = percentT + dL.current[i].percentChange;
        }
        terminal.updateExchangeAverageValue([total, tVolume, percentT/dL.current.length])
    },[displayList, first.current, second.current, third.current]);

    useEffect(()=>{
        tradeWS.current = socketIOClient("https://terminal.apimachine.com");
        return () =>{
            tradeWS.current.close();
            setNumbers( {Bitfinex: null, Binance: null, Kraken: null, Okex: null, ZB: null, Bitmax: null })
        }
    },[]);
    useEffect(()=>{
    },[first.current, second.current, third.current, fourth.current]);
    const webSocket = () => {
        console.log('Socket DATA Connection');
        tradeWS.current.on("data", data => {
            first.current = first.current + 1;
            let pair = `${terminal.apiCurrency[0].coin}${terminal.apiCurrency[1].coin}`;
            let a = data.mData.filter(res => {
                return res.symbol === pair;
            });
            if (numbers.Binance === null)
                return;
            setUpdate3(update3 + 1);
            dL.current[numbers.Binance] = {
                ...dL.current[numbers.Binance],
                price: !a[0] ? 0 : a[0].askPrice,
                ['24Hr_Volume']: !a[0] ? 0 : a[0].quoteVolume,
                percentChange: !a[0] ? 0 : parseFloat(a[0].priceChangePercent)
            };
        });
        tradeWS.current.on("dataBitfinex", data => {
            // setUpdate1(update1 + 1);
            second.current = second.current + 1;
            let pair = `t${terminal.apiCurrency[0].coin}${terminal.apiCurrency[1].coin}`;
            let a = data.mData.filter(res => {
                return res[0] === pair;
            });
            if (numbers.Bitfinex === null)
                return;
            setUpdate3(update3 + 1);
            dL.current[numbers.Bitfinex] = {
                ...dL.current[numbers.Bitfinex],
                price: !a[0] ? 0 : a[0][1],
                ['24Hr_Volume']: !a[0] ? 0 : a[0][2],
                percentChange: !a[0] ? 0 : 0
            };
        });
        tradeWS.current.on("dataKraken", data => {
            let pair = `${terminal.apiCurrency[0].coin}-${terminal.apiCurrency[1].coin}`;
            let a = data.mData.filter(res => {
                return res.instrument === pair;
            });
            if (numbers.Kraken === null)
                return;
            setUpdate3(update3 + 1);
            dL.current[numbers.Kraken] = {
                ...dL.current[numbers.Kraken],
                price: !a[0] ? 0 : a[0].ask,
                ['24Hr_Volume']: !a[0] ? 0 : a[0].baseVolume,
                percentChange: !a[0] ? 0 : parseFloat(a[0].percentChange)
            };
        });
        tradeWS.current.on("dataOkex", data => {
            fourth.current = fourth.current + 1;
            let pair = `${terminal.apiCurrency[0].coin}-${terminal.apiCurrency[1].coin}`;
            let a = data.mData.filter(res => {
                return res.product_id === pair;
            });
            if (numbers.Okex === null)
                return;
            setUpdate3(update3 + 1);
            dL.current[numbers.Okex] = {
                ...dL.current[numbers.Okex],
                price: !a[0] ? 0 : a[0].ask,
                ['24Hr_Volume']: !a[0] ? 0 : a[0].base_volume_24h,
                percentChange: !a[0] ? 0 : 1
            };
        });
        tradeWS.current.on("dataZB", data => {
            let pair = `${terminal.apiCurrency[0].coin.toLowerCase()}${terminal.apiCurrency[1].coin.toLowerCase()}`;
            let a = data.mData.filter(res => {
                return res[0] === pair
            });
            if (numbers.ZB === null)
                return;
            setUpdate3(update3 + 1);
            dL.current[numbers.ZB] = {
                ...dL.current[numbers.ZB],
                price: !a[0]?'':a[0][1].last,
                ['24Hr_Volume']: !a[0]?'':a[0][1].vol,
                percentChange: 0
            }
        })

        tradeWS.current.on("dataBitmax", data => {
                let pair = `${terminal.apiCurrency[0].coin}/${terminal.apiCurrency[1].coin}`;
                let a = data.mData.filter(res => {
                    return res.symbol === pair
                });
            if (numbers.ZB === null)
                return;
            setUpdate3(update3 + 1);
            dL.current[numbers.Bitmax] = {
                ...dL.current[numbers.Bitmax],
                price: a[0]?.close,
                ['24Hr_Volume']: a[0]?.volume,
                percentChange: 0
            }
            }
        )
    };
    useEffect(()=>{
    },[exchangeList, exchangeListUpdate]);
    return(
        <React.Fragment>
            <div className={terminal.darkMode?"exchange-countries-body-dark":"exchange-countries-body"}>
                <div className="exchange-countries-table-header">
                    {
                        tableHeader.map((x) => <button key={nextId()}>{x}</button>)
                    }
                </div>
                <div className="exchange-countries-table-body">
                    {   exchangeTableLoader?<h4 className="mt-4 text-center">Loading.....</h4>:
                        // exchangeDataTwo.length === 0 ?
                        //     <h4 className="mt-5 text-center">NO DATA....</h4>
                        //     :
                            !dL.current?'':dL.current
                                .map((obj) =>
                                    <div key ={obj.exchangeId} onClick={()=>{handleRowClick(obj)}} className={`exchange-countries-table-row ${selectAsset.exchangeId === obj.exchangeId?'selected':''}`}>
                                        <div><img src={obj.image}/>
                                            <h6>{obj.exchangeId}</h6>
                                        </div>
                                        <div><h6>{obj.price === 0?'Coming':baseAsset?terminal.formatter.format(buySell?(obj.price):(obj.price)):
                                            buySell?terminal.valueFormatter(terminal.apiCurrency[1].coin,parseFloat(obj.price), 's'):terminal.valueFormatter(terminal.apiCurrency[1].coin,parseFloat(obj.price), 's')}</h6></div>
                                        {/*<div><h6>{obj.price}</h6></div>*/}
                                        <div><h6>{obj['24Hr_Volume']===0?'Soon':baseAsset?terminal.formatter.format(obj['24Hr_Volume']):terminal.valueFormatter(terminal.apiCurrency[1].coin,parseFloat(obj['24Hr_Volume']), 's')}</h6></div>
                                        <div><h6 style={obj.percentChange < 0?{color: '#ff0000'}:{color: '#3AA551'}}>{Math.abs(obj.percentChange).toFixed(2) + '%'}</h6></div>
                                        <div>
                                            <button onClick={(e)=>{e.stopPropagation()}}><span>Trade</span></button>
                                            <button onClick={(e)=>{e.stopPropagation()}}><span>Algo</span></button></div>
                                    </div>)
                    }
                </div>
            </div>
        </React.Fragment>
    )
};

const tableHeader = ["Exchange", "Last Price", "Volume(24H)", "Gain", "Action"];

export default ExchangeComponent;
