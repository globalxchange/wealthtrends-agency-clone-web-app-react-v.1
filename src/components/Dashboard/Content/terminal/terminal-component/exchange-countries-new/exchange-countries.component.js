import React, { useContext, useEffect, useState} from 'react';
import nextId from "react-id-generator";
import './exchange-countries.style.scss';
import {fullScreenButton, terminalRight} from "../../../../images/terminal-images";
import ExchangeComponent from "./exchange/exchange.component";
import {IcedTerminal} from "../../../main-context/main.context";
import ExchangeHeaderComponent from "./exchange-header/exchange-header.component";
import WelcomeStepComponent from "./welcome-step/welcome-step.component";
import ChooseBaseAssetComponent from "./choose-base-asset/choose-base-asset.component";
import ChooseQuoteAssetComponent from "./choose-quote-asset/choose-quote-asset.component";
const ExchangeCountriesComponent = React.memo(()=>{
    const terminal  = useContext(IcedTerminal);
    const [filterFocus, setFilterFocus] = useState(false);
    const {terminalStage,setTerminalStage, exchangeAverageValue,layoutState} = terminal;

    useEffect(()=>{
    },[terminalStage]);
    useEffect(()=>{
        terminal.filterExchangeList();
    },[terminal.apiCurrency, terminal.filterExchangeList]);

    const selectComponent = () =>{
        switch (terminalStage) {
            case 0: return <WelcomeStepComponent/>;
            case 1: return <ChooseBaseAssetComponent/>;
            case 2:
            case 3:
            case 4: return <ChooseQuoteAssetComponent filterFocus = {filterFocus}/>;
            case 5: return <WelcomeStepComponent/>;
            case 6: return <ExchangeComponent/>;
            default: return ;
        }
    };
    const handleFullSize = () =>{
        if(terminal.layoutState[3].size[1]=== 10){
            terminal.changeLayout([{size: [9,5,0,0], component: ['b', 'chart']},layoutState[1], layoutState[2],{size: [6,5,3,5], component: ['e', 'exchange']},layoutState[4]])
        }else{
            terminal.changeLayout([{size: [9,5,0,10], component: ['b', 'chart']},layoutState[1], layoutState[2], {size: [12,10,0,0], component: ['e', 'exchange']},layoutState[4]])
        }
    };

    return (
        <div className={terminal.darkMode?"exchange-countries-dark":"exchange-countries"}>
            <div className="exchange-countries-header">
            </div>
            <div className="exchange-countries-body-header position-relative">
                <ExchangeHeaderComponent filterFocus = {filterFocus} setFilterFocus = {setFilterFocus}/>
                <img className="full-button" src={fullScreenButton} onClick={()=>handleFullSize()}/>
            </div>
            {selectComponent()}
            <div className={terminalStage === 6?"exchange-countries-footer": 'exchange-countries-footer-hide'}>
                <div onClick={()=>setTerminalStage(2)} className="back-button" style={terminalStage === 3?{display: 'flex'}:{display: 'none'}}><img src={terminalRight}/> Back</div>
                <button className="terminal">Terminal Average</button>
                {
                    exchangeAverageValue.map((x, i) => i === 2?
                        <button key={nextId()}>{Math.abs(parseFloat(x)).toFixed(2)}%</button>:<button>{terminal.valueFormatter(terminal.apiCurrency[1].coin,parseFloat(x))}</button>)
                }
                <button/>
            </div>
        </div>
    );
});



export default ExchangeCountriesComponent;
