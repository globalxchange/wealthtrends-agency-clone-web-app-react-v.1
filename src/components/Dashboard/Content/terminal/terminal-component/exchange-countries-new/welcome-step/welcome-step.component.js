import React, {Component, useContext, useEffect} from 'react';
import './welcome-step.style.scss';
import {loader, loaderBlack, terminalRight, welcomeTerminal} from "../../../../../images/terminal-images";
import {IcedTerminal} from "../../../../main-context/main.context";
import LoadingAnimation from '../../../../../../../lotties/LoadingAnimation';

const WelcomeStepComponent = () =>{
    const terminal = useContext(IcedTerminal);
    const {terminalStage, apiCurrency,exchangeList} = terminal;
    useEffect(()=>{
    },[apiCurrency,exchangeList]);

    return (
        <div className={terminal.darkMode?"welcome-step-dark":"welcome-step"}>
            <div onClick={() =>terminalStage === 0? terminal.setTerminalStage(1):''}>
                <div className="image-container">
                    {
                        terminalStage === 0?<img className={"main-logo"} src={ welcomeTerminal }/>:<LoadingAnimation />

                    }

                </div>
                {
                    terminalStage === 0 ?
                    <h2>Welcome To Your Terminal<img src={terminalRight}/></h2> :
                    <h3>Fetching {apiCurrency[0].coin}/{apiCurrency[1].coin} Prices From {exchangeList.length} Exchanges</h3>
                }
            </div>
        </div>
    );
};

export default WelcomeStepComponent;
