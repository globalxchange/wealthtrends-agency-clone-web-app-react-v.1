import React, {Component, useContext, useEffect, useRef, useState} from 'react';
import nextId from "react-id-generator";
import './choose-quote-asset.style.scss';
import {bicoinFull, BTC, ETH, ethereumFull} from "../../../../../images/terminal-images";
import {IcedTerminal} from "../../../../main-context/main.context";
const ChooseQuoteAssetComponent = ({filterFocus}) =>{
    const coinListElement = useRef();
    const icedTerminal= useContext(IcedTerminal);
    const [coinListNumber, setCoinListNumber] = useState(8);
    const [frequentlyList, setFrequencyList] = useState([]);
    const [suggestionList, setSuggestionList] = useState([]);
    const {terminalStage,setBuySell,allCoins, terminalSearchTerm,fillExchangeList, setTerminalStage,setAPICurrency, apiCurrency,getExchangeTableData, getExchangeTableDataTwo} = icedTerminal;
    const handleCLick = (val, x) =>{
        setTerminalStage(val);
        console.log('handleCLick', x);
        setAPICurrency([{coin: x, image: `https://cryptoicons.org/api/icon/${x.toLowerCase()}/30`, name: '',exchange: x.length},apiCurrency[1]])
    };
    useEffect(()=>{
        if(filterFocus && terminalSearchTerm !== '')
            setCoinListNumber(allCoins.length);
        else
            setCoinListNumber(20);
    },[filterFocus, terminalSearchTerm]);
    const handleClickTwo = (type) =>{
        (type === "Buy")?setBuySell("Buy"): setBuySell("Sell");
        // getExchangeTableData();
        getExchangeTableDataTwo();
        fillExchangeList();
    };
    useEffect(()=>{
        setFrequencyList(allCoins);
        setSuggestionList(allCoins);
    },[terminalSearchTerm, allCoins]);

    const handleScroll = () => {
        if (coinListElement.current.offsetHeight + coinListElement.current.scrollTop > coinListElement.current.scrollHeight - (0.5 * coinListElement.current.offsetHeight)) {
            setCoinListNumber(coinListNumber + 3);
        }
    };
    return (
        <div className={icedTerminal.darkMode?"choose-quote-asset-dark":"choose-quote-asset"}>
            <div className={terminalStage === 3 ? "d-none" : "choose-quote-asset-head"}>
                {terminalStage === 4 ? '' : `Frequently Traded Against ${apiCurrency[1].coin}`}
            </div>
            <div
                onScroll={handleScroll}
                ref={coinListElement}
                style={terminalStage === 3 ? {height: '100%'} : {}} className="choose-quote-asset-body">
                {
                    terminalStage === 3 ?
                        suggestionList.filter(x => {
                            return x.coin !== apiCurrency[1].coin && (x.coin.toLowerCase().startsWith(terminalSearchTerm.toLowerCase()));
                        }).slice(0, coinListNumber)
                            .map((obj) =>
                                <div
                                    key={obj.coin}
                                    onClick={() => {
                                        handleCLick(4, obj.coin, obj.exchanges)
                                    }}
                                    className="choose-quote-asset-body-row">
                                    <span>
                                        <img height="30" width="20" onError={icedTerminal.onImageError}
                                             src={`https://cryptoicons.org/api/icon/${obj.coin.toLowerCase()}/30`}/>
                                        {obj.coin}</span>
                                    <span>Compare Over {apiCurrency[1].coin === 'USD' ? 1 : 4} Exchanges</span>
                                </div>)
                        :
                        (terminalStage === 2 ? frequentlyList : buySellList).filter(res => {
                            return terminalStage === 2 ? res.coin !== apiCurrency[1].coin : true
                        }).slice(0, coinListNumber)
                            .map((obj) =>
                                <div
                                    key={terminalStage === 2 ? obj.coin : obj.name}
                                    onClick={() => terminalStage === 2 ? handleCLick(4, obj.coin) : handleClickTwo(obj.name)}
                                    className="choose-quote-asset-body-row">
                                    <span>
                                        <img height="30" onError={icedTerminal.onImageError}
                                             className={terminalStage === 2 ? '' : 'd-none'}
                                             src={terminalStage === 2 ? `https://cryptoicons.org/api/icon/${obj.coin.toLowerCase()}/30` : ''}/>
                                        {terminalStage === 2 ? obj.coin : obj.name}
                                    </span>
                                    {terminalStage === 2 ?
                                        <span>Compare Over {apiCurrency[1].coin === 'USD' ? 1 : 4} Exchanges</span> :
                                        <span>{apiCurrency[0].coin}/{apiCurrency[1].coin}</span>}
                                </div>)
                }
            </div>

        </div>
    );
};
const buySellList = [
    {coin: '', name: "Buy", detail: 'ETH/USD', image: ''},
    {coin: '', name: "Sell", detail: 'ETH/USD', image: ''},

];
const suggestionData = [
    {coin: 'ETH', name: "Ethereum", detail: 'Compare Over 452 Exchanges',exchange: localStorage.getItem('exchangeLength'), image: ethereumFull},
    {coin: 'ETH', name: "Ethereum Classic", detail: 'Compare Over 13 Exchanges',exchange: localStorage.getItem('exchangeLength'), image: ethereumFull},
    {coin: 'BTC', name: "Bitcoin", detail: 'Compare Over 17 Exchanges',exchange: localStorage.getItem('exchangeLength'), image: bicoinFull},
    {coin: 'BTC', name: "Bitcoin Classix", detail: 'Compare Over 93 Exchanges',exchange: localStorage.getItem('exchangeLength'), image: bicoinFull},
];

export default ChooseQuoteAssetComponent;
