import React, {useContext} from 'react';
import './exchange-header.style.scss';
import {IcedTerminal} from "../../../../main-context/main.context";
import {searchIcon} from "../../../../../images/terminal-images";

const ExchangeHeaderComponent = ({setFilterFocus}) => {
    const icedTerminal = useContext(IcedTerminal);
    const { terminalStage, setTerminalStage,setTerminalSearchTerm, apiCurrency,setAPICurrency, buySell, setBuySell,resetExchangeAverageValue } = icedTerminal;
    const handleClick = (val) =>{
          if(terminalStage === 0){
              setTerminalStage(1);
          }
          else if(terminalStage === 2 && val === 1){
              setTerminalStage(1);
              setAPICurrency([{coin: '', image: ''},{coin: '', image: ''}]);
          }
          else if(terminalStage === 2 && val === 2){
              setTerminalStage(3);
          }
          else if(terminalStage === 4 && val === 2){
              setTerminalStage(2);
              setAPICurrency([{coin: '', image: ''},apiCurrency[1]]);
          }
          else if(terminalStage === 6){
              if(val === 1){
                  setTerminalStage(1);
              }else if(val === 2){
                  setTerminalStage(2);
              }else{
                  setTerminalStage(4)
              }
              // icedTerminal.setNumbers({Bitfinex: null, "Binance": null, "Kraken": null, "Okex": null});
              // setAPICurrency([{coin: '', image: ''},{coin: '', image: '', name: '', exchange: 0}]);
              // setBuySell(true);
              // resetExchangeAverageValue([0,0,0]);
              // setTerminalStage(1);
              // setTerminalSearchTerm('xxxx')
              // icedTerminal.setSelectAsset({exchangeId:'Loading...', ['24Hr_Volume']: 0,fullLogo:'',imageCurrency:'', percentChange: 0, price: 0, image: '', buyPrice: 0, sellPrice: 0})
          }
    };

    return (
        <div className={icedTerminal.darkMode?"exchange-header-dark":"exchange-header"}>
            {terminalStage === 3 ? '' :
                <div style={terminalStage === 0 || terminalStage === 6 || terminalStage === 1 ? {fontWeight: 600} : {opacity: 0.5}}
                     onClick={() => handleClick(1)} className="exchange-header-container">
                    {apiCurrency[1].coin === ''?<span style={terminalStage === 0?{fontWeight: 400}:{}}>Choose Base Asset</span>:<span className="filled"><img onError={icedTerminal.onImageError} src={`https://cryptoicons.org/api/icon/${apiCurrency[1].coin.toLowerCase()}/30`}/>{apiCurrency[1].coin}</span>}
                </div>}
            <div style={terminalStage === 2 || terminalStage === 6 || terminalStage === 0 || terminalStage === 3 ? {} : {opacity: 0.5}}
                 onClick={() => handleClick(2)} className="exchange-header-container">
                {terminalStage === 3?
                    <input autoFocus={true} onFocus={()=>setFilterFocus(true)} onBlur={()=>setFilterFocus(false)} onChange={(e)=>setTerminalSearchTerm(e.target.value)} placeholder="Search The Name Of The Asset You Want To Trade"/>:
                    apiCurrency[0].coin === ''?<span>Choose Quote Asset<img  onError={icedTerminal.onImageError} className={terminalStage === 2?"search-image":"d-none"} src={searchIcon}/></span>:<span className="filled"><img onError={icedTerminal.onImageError} src={`https://cryptoicons.org/api/icon/${apiCurrency[0].coin.toLowerCase()}/30`}/>{apiCurrency[0].coin}</span>
                }
            </div>
            {terminalStage === 3 ? '' :
                <div
                    style={terminalStage === 4 || terminalStage === 6 || terminalStage === 0 || terminalStage === 5 || terminalStage === 6 ? {} : {opacity: 0.5}}
                    onClick={() => handleClick(3)} className="exchange-header-container">
                    {apiCurrency[0].coin === ''?<span>Direction</span>:<span className="filled">{buySell.length === 0?'Direction': buySell}</span>}
                </div>}
        </div>
    );
};

export default ExchangeHeaderComponent;
