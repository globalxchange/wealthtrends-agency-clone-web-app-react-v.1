import React, { useContext, useEffect, useRef, useState } from 'react';
import nextId from "react-id-generator";
import './choose-base-asset.style.scss'
import { IcedTerminal } from "../../../../main-context/main.context";
const ChooseBaseAssetComponent = () => {
    const assetListElement = useRef();
    const [assetList, setAssetList] = useState([]);
    const [convert, setConvert] = useState(0);
    const [assetListNumber, setAssetListNumber] = useState(10);
    const icedTerminal = useContext(IcedTerminal);
    const { setAPICurrency, apiCurrency, loggedIn, loginModal, setLoginModal } = icedTerminal
    useEffect(() => {
        setConvert(icedTerminal.cryptoUSD['BTC']);
        let list = icedTerminal.liquidTerminalList.filter((x) => {
            return x.coin === 'BTC' || x.coin === 'USD' || x.coin === 'USDT'
        });

        // console.log(coinList);
        let listR = list.reverse();
        setAssetList([...listR]);
    }, [icedTerminal.liquidTerminalList]);

    useEffect(()=>{        
        if(loggedIn){
            
            icedTerminal.setLoginModal(false)

        }else{
            icedTerminal.setLoginModal(true)
        }
    },[loggedIn])

    const handleClick = (obj) => {
        if (obj.coin === 'BTC')
            icedTerminal.setBaseAsset(false);
        else
            icedTerminal.setBaseAsset(true);
        icedTerminal.setTerminalStage(2);
        setAPICurrency([apiCurrency[0], {
            coin: obj.coin,
            image: `https://cryptoicons.org/api/icon/${obj.coin.toLowerCase()}/30`
        }])
    };
    const handleScroll = () => {
        console.log(assetListElement.current.scrollHeight, assetListElement.current.scrollTop + assetListElement.current.offsetHeight);
    };
    return (
        <div className={icedTerminal.darkMode ? "choose-base-asset-dark" : "choose-base-asset"}>
            <div className="choose-base-asset-head">
                {cbaHeaders.map((x) => <button>{x}</button>)}
            </div>
            <div onScroll={handleScroll} ref={assetListElement} className="choose-base-asset-body">
                {
                    assetList.length === 0 ?
                        <div className="d-flex h-100 justify-content-center align-items-center">Loading</div>
                        :
                        assetList.map((x) =>
                            <div key={x.coin} onClick={() => { handleClick(x) }} className="choose-base-asset-body-row">
                                <button><img onError={icedTerminal.onImageError} src={`https://cryptoicons.org/api/icon/${x.coin.toLowerCase()}/30`} />{x.coin}</button>
                                <button>{x.coin === 'BTC' ? x.balance.toFixed(4) : icedTerminal.formatter.format(x.balance)}</button>
                                <button>{x.coin === 'BTC' ? icedTerminal.formatter.format(x.USDeqv) : icedTerminal.formatter.format(x.balance)}</button>
                                <button>0.00%</button>
                                <button>{icedTerminal.allCoins.length}</button>
                            </div>)}
            </div>

        </div>
    )
}
const cbaHeaders = ["Asset", "Balance", "Value (USD)", "Gain", "Pairs"];
export default ChooseBaseAssetComponent;
