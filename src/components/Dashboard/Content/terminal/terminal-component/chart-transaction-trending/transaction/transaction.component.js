import React, {Component} from 'react';
import nextId from "react-id-generator";
import './transaction.style.scss'
import {BTC} from "../../../../../images/terminal-images";
const TransactionComponent = () =>{
    return (
        <div className="transaction-main">
            <div className="transaction-table-header">
                {
                    transactionHeader.map((x) => <button>{x}</button>)
                }

            </div>
            <div className="transaction-table-body">
                {
                    [1, 1, 1, 1, 11,1,11, 1].map(() =>
                        <div key={nextId()} className="transaction-table-body-row">
                            <button><img className="mr-2" src={dummyRow.img}/>{dummyRow.coin}</button>
                            <button>{dummyRow.s_id}</button>
                            <button>{dummyRow.t_id}</button>
                            <button>{dummyRow.time}</button>
                            <button>{dummyRow.status}</button>
                            <button>{dummyRow.amount}</button>
                        </div>)
                }


            </div>

        </div>
    );
};

const transactionHeader = ["Crypto Trade", "Sender ID", "Transaction ID", "Time", "Status", "Amount"];
const dummyRow = {img: BTC, coin: 'Bitcoin', s_id: 'sfd-5scsdss', t_id: 'D6DHYCUS', time: '10:30:45', status: 'completed', amount: '0.0014 BTC'};
export default TransactionComponent;
