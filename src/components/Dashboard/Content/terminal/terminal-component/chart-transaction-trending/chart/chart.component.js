import React, { useContext, useEffect, useState } from "react";
import { IcedTerminal } from "../../../../main-context/main.context";
import "./chart.style.scss";
import CurvedChart from "./chart/curved-chart";
const ChartComponent = () => {
  const icedTerminal = useContext(IcedTerminal);
  const [chartSize, setChartSize] = useState({ height: 0, width: 0, half: 0 });
  const [fullScreen, setFullScreen] = useState(false);
  useEffect(() => {
    window.addEventListener("resize", changeSize);
    let ele = document.getElementById("curved-chart");
    setChartSize({
      height: ele.offsetHeight,
      width: ele.offsetWidth,
      half: ele.offsetHeight * 0.5,
    });
    return () => {
      window.removeEventListener("resize", changeSize);
    };
  }, [icedTerminal.chartSidebar]);

  const changeSize = () => {
    let ele = document.getElementById("curved-chart");
    setChartSize({
      height: ele.offsetHeight,
      width: ele.offsetWidth,
      half: ele.offsetHeight * 0.5,
    });
  };
  return (
    <div className={icedTerminal.darkMode ? "chart-body-dark" : "chart-body"}>
      <div
        id="curved-chart"
        className={`chart-curve-chart ${fullScreen ? "full" : "normal"}`}
      >
        <CurvedChart
          height={chartSize.height}
          width={chartSize.width}
          half={chartSize.half}
        />
      </div>
    </div>
  );
};

export default ChartComponent;
