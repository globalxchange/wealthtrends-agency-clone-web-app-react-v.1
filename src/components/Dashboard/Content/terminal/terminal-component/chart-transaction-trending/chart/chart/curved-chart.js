import React, {Component, useContext} from 'react';
import {IcedTerminal} from "../../../../../main-context/main.context";

const CurvedChart = ({height, width, half}) =>{
    const icedTerminal = useContext(IcedTerminal);
    const {baseAsset,selectAsset, formatter} = icedTerminal;
    return (
        <div className="curved-chart-chart-container">
            <div className="curved-chart-chart-container-values">
                <h1>{baseAsset?formatter.format(selectAsset.price):icedTerminal.valueFormatter(icedTerminal.apiCurrency[1].coin, selectAsset.price)+' BTC'}</h1>
                <h6>
                    <span className={selectAsset.percentChange < 0?'text-danger': '#70C582'}>
                        {Math.abs(selectAsset.percentChange).toFixed(2)}% </span>
                    <span>({baseAsset?formatter.format(0.01 * selectAsset.percentChange *selectAsset.price):icedTerminal.valueFormatter(icedTerminal.apiCurrency[1].coin, 0.01 * selectAsset.percentChange *selectAsset.price)})</span></h6>
            </div>
            <svg height={height} width={width}>
                <defs>
                    <linearGradient id="grad1" x1="0%" y1="0%" x2="0%" y2="100%">
                        <stop offset="50%" style={{stopColor: '#98d2ff', stopOpacity: '1'}}/>
                        <stop offset="100%" style={{stopColor: '#ffffff', stopOpacity: '1'}}/>
                    </linearGradient>
                </defs>
                <path
                    // fill = "#9bd3ff"
                    fill="url(#grad1)"
                    className="curved-chart"
                    d={`M0 ${height} L0 270 Q27 ${half - 50} ${0.0834 * width} ${half - 25} T${0.1668 * width} 210  T${0.2502 * width} ${half + 70} T${0.3336 * width} ${half + 15} T${0.417 * width} ${half + 15} T${width * 0.5004} ${half + 30} T${0.5838 * width} ${half + 100} T${0.6672 * width} ${half + 10} T${0.7506 * width} ${half + 35} T${0.834 * width} ${half + 23} T${0.9174 * width} ${half + 45} ${width} ${half - 5} L${width} ${height}`}/>

            </svg>
        </div>
    );
};
export default CurvedChart;
