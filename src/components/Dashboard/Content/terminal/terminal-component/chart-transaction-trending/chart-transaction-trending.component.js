import React, { useState, useContext, useRef, useEffect } from "react";
import "./chart-transaction-trending.style.scss";
import { IcedTerminal } from "../../../main-context/main.context";
import ChartComponent from "./chart/chart.component";
import TradingViewWidget, { Themes } from "react-tradingview-widget";
// import {TradingViewChart} from 'nvest-trading-view'
import {
  fullScreenButton,
  price,
  sentiment,
  tradingView,
  volume,
} from "../../../../images/terminal-images";
import TransactionComponent from "./transaction/transaction.component";
import { Agency } from "../../../../../Context/Context";
const ChartTransactionTrendingComponent = React.memo(() => {
  const terminal = useContext(IcedTerminal);
  const agency = React.useContext(Agency);
  const { tradePairObj } = agency;
  const chartContainerRef = useRef();
  const [symbol, setSymbol] = useState("BTCUSD");
  const { selectAsset, apiCurrency, layoutState } = terminal;
  const [dropDowns, setDropDowns] = useState([
    { text: "Price", img: price, value: "" },
    { text: "Volume", img: volume, value: "1.3B" },
    { text: "Sentiment", img: sentiment, value: "8.2/10" },
    { text: "Trading View", img: tradingView, value: "" },
    { text: "History", img: "", value: "" },
  ]);

  const [selectFromDropDown, setSelectFromDropDown] = useState(dropDowns[3]);
  let tabHeaders = [
    { text: "", img: selectAsset.fullImage, p: "Selected Exchange" },
    {
      text: "",
      img:
        terminal.apiCurrency[0].coin === ""
          ? ""
          : `https://cryptoicons.org/api/icon/${apiCurrency[0].coin.toLowerCase()}/30`,
      p: "Selected Asset",
    },
    {
      text: `${selectFromDropDown.text} ${selectFromDropDown.text.includes("Trading") ? "" : ""
        }`,
      img: selectFromDropDown.img,
    },
  ];
  const selectComponent = () => {
    if (!chartContainerRef.current) return;

    switch (selectFromDropDown.text) {
      case "Volume":
        return <ChartComponent />;
      case "Price":
        return <ChartComponent />;
      case "Trading View":
        return (
          <TradingViewWidget
            theme={terminal.darkMode ? Themes.DARK : Themes.LIGHT}
            interval={60}
            symbol={`${symbol}`}
            locale="en"
            autosize
          />
        );
      // case "Trading View":
      //   return 
      // (
      //   <div className="w-100 h-100 trading-view"><TradingViewChart light={!terminal.darkMode}/></div>
      // );
      case "Sentiment":
        return <ChartComponent />;
      case "History":
        return <TransactionComponent />;
      default:
        return;
    }
  };
  const node = useRef();
  const handleClick = (obj, i) => {
    if (i === 2) {
      terminal.setChartSidebar(!terminal.chartSidebar);
    }
  };
  useEffect(() => {
    if(!tradePairObj.exchangeBank)return
    setSymbol(tradePairObj.base._id + tradePairObj.quote._id );
  }, [tradePairObj.quote, tradePairObj.exchangeBank]);
  const handleMouseDown = (e) => {
    if (node.current === null) return;
    else if (node.current.contains(e.target)) {
    } else {
      terminal.setChartSidebar(false, e.target.className);
    }
  };

  const handleFullSize = () => {
    if (terminal.layoutState[0].size[1] === 10) {
      terminal.changeLayout([
        { size: [9, 5, 0, 0], component: ["b", "chart"] },
        layoutState[1],
        layoutState[2],
        layoutState[3],
        layoutState[4],
      ]);
    } else {
      terminal.changeLayout([
        { size: [12, 10, 0, 0], component: ["b", "chart"] },
        layoutState[1],
        layoutState[2],
        layoutState[3],
        layoutState[4],
      ]);
    }
  };
  return (
    <div
      className={
        terminal.darkMode
          ? "chart-transaction-trending-dark"
          : "chart-transaction-trending"
      }
    >
      <div ref={chartContainerRef} className="chart-transaction-trending-body">
        {selectComponent()}
        <div
          ref={node}
          className={
            terminal.chartSidebar
              ? "chart-transaction-trending-sidebar"
              : "chart-transaction-trending-sidebar-hide"
          }
        >
          <div className="chart-transaction-trending-sidebar-body">
            {dropDowns
              .filter((y) => {
                return y.text !== selectFromDropDown.text;
              })
              .map((x, i) => (
                <button
                  onClick={() => {
                    setSelectFromDropDown(x);
                    terminal.setChartSidebar();
                  }}
                  style={
                    i % 2 !== 1
                      ? {
                        backgroundColor: "rgba(255,255,255,0.1)",
                        color: "white",
                      }
                      : terminal.darkMode
                        ? { color: "white" }
                        : {}
                  }
                >
                  <div>
                    <span>
                      <img onError={terminal.onImageError} src={x.img} />
                    </span>
                    <span>{x.text}</span>
                  </div>
                  <div>
                    <span>{x.value}</span>
                  </div>
                </button>
              ))}
          </div>
          <div className="chart-transaction-trending-sidebar-footer">
            Chart Center
          </div>
        </div>
      </div>
    </div>
  );
});

export default ChartTransactionTrendingComponent;
