import React, {useContext, useEffect} from 'react';
import './terminal.style.scss'
import TerminalComponent from "./terminal-component/terminal.component";
import {IcedTerminal} from "../main-context/main.context";

const TerminalMain = React.memo(()=>{
    const terminal = useContext(IcedTerminal);
    // useEffect(()=>{
    //
    // },[terminal.executionMarketLoading]);
    return (
        <React.Fragment>
            <div className="terminal-main">
                <TerminalComponent/>
            </div>
            {/* To Do: Uncomment this for Terminal */}
            {/* <div style={terminal.executionSuccess === 'success'?{backgroundColor: '#34ce57'}:{backgroundColor: 'red'}}
                 className={terminal.executionSuccess !== ''?"right-popup":"right-popup-none"}>
                {terminal.executionSuccess === "success"?'Market Order Success':'Market Order Failed'}
            </div> */}
        </React.Fragment>
    );
});

export default TerminalMain;
