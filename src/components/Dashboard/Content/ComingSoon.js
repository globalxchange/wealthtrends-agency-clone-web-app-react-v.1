import React from 'react'

export default function ComingSoon() {
    return (
        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
            <h1>Coming Soon</h1>
        </div>
    )
}
