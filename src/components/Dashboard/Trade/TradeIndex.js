import React from 'react'
import TradeMainBody from './trade-main-body/TradeMainBody'
import TradeMainHeader from './trade-mian-header/TradeMainHeader'
import './trade.index.style.scss'

export default function TradeIndex() {
    return (
        <div className="trade-main">
            <div className="trade-main-header-normal">
                <TradeMainHeader />

            </div>
            <div className="trade-main-body">
                <TradeMainBody />

            </div>
            
        </div>
    )
}
