import React, { useContext } from 'react'
import { Agency } from '../../../../Context/Context'
import Images from '../../../../../assets/a-exporter'
import Constant from '../../../../../json/constant'
import './trade-tab-header.style.scss'
export default function TradeTabHeader() {
    const agency = useContext(Agency);
    const { tradeTabStep, setTradeTabStep, tradePairObj,setCStep, cStep, tradeDashboardTab, resetTradeDashboard } = agency;

    const selectContent = (code) => {
        switch (code) {
            case "base": return tradePairObj.base !== null ? <h6><img src={tradePairObj.base.coin_metadata.coinImage} /> {tradePairObj.base.coin_metadata.coinName}</h6> : <h6>Choose Base Asset</h6>;
            case "quote": return tradePairObj.quote !== null ? <h6><img src={tradePairObj.quote.coin_metadata.coinImage} /> {tradePairObj.quote.coin_metadata.coinName}</h6> : <h6>Choose Quote Asset</h6>;
            case "direction": return tradePairObj.exchangeBank !== null ? <h6><img className="mr-2" src={tradePairObj.exchangeBank.icon} />{tradePairObj.exchangeBank.banker}</h6> : <h6>Choose Exchange</h6>;
            default: return;
        }
    }

    const handleClick = (code, step) => {
        if (tradeDashboardTab) {
            resetTradeDashboard()
        }
        switch (code) {
            case "base":
                if (tradeTabStep < step) { return }
                else { setTradeTabStep(0); }
                break;
            case "quote":
                if (tradeTabStep < step) { return }
                else { setTradeTabStep(1); }
                break;
            case "direction":
                if (tradeTabStep < step) { return }
                else {
                    cStep ? setCStep(0) : setTradeTabStep(2);
                }
                break;
            default: return;
        }

    }
    return (
        <div className="trade-tab-header-child">
            {
                Constant.tradeHeaderOption.map((obj, step) =>
                    <div onClick={() => handleClick(obj.code, step)} className={`header-block ${tradeTabStep === step ? "current-block" : tradeTabStep < step ? "opaque-block" : ""}`}>
                        {
                            selectContent(obj.code)
                        }
                    </div>
                )
            }
        </div>
    )
}

const tempObj = {
    coin: "Bitcoin",
    action: "Buy",
    icon: Images.bitcoin
}

const tempObjTwo = {
    coin: "Canadian Dollar",
    action: "Buy",
    icon: Images.bitcoin
}