import React from 'react'
import LoadingAnimation from '../../../../../../../lotties/LoadingAnimation'
export default function TabDashLoading() {
    return (
        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
            <LoadingAnimation type= "login" size = {{height: 250, width: 250}} />
        </div>
    )
}
