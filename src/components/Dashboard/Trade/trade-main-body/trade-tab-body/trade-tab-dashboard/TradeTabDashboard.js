import React, { useContext } from 'react'
import { Agency } from '../../../../../Context/Context'
import TabDashCalculator from './tab-dash-calculator/TabDashCalculator';
import TabDashLoading from './tab-dash-loading/TabDashLoading';
import TabDashResult from './tab-dash-result/TabDashResult';
import TabDashSummary from './tab-dash-summary/TabDashSummary';
import './trade-tab-dashboard.style.scss'
export default function TradeTabDashboard() {
    const agency = useContext(Agency);
    const {tradeDashboardTab} = agency;

    const selectComponent = () =>{
        switch(tradeDashboardTab){
            case 0: return <TabDashCalculator />;
            case 1: return <TabDashSummary />;
            case 2: return <TabDashLoading />;
            case 3: return <TabDashResult /> ;
            default: return;
        }
    }

    return (
        <div className="trade-tab-dashboard">
            {
                selectComponent()
            }           
        </div>
    )
}
