import React from 'react'
import LoadingAnimation from '../../../../../../../lotties/LoadingAnimation'

export default function CalculatorSell({ load, percentages, handlePlaceOrder, valueFormatter, availableAmount, tradePairObj, sellValue, handleChange }) {
    const handlePercentage = (obj) => {
        let temp = {}
        switch (obj.value) {
            case "max": temp = { target: { value: availableAmount } }; handleChange(temp, false); break;
            case "25%": temp = { target: { value: 0.25 * availableAmount } }; handleChange(temp, false); break;
            case "50%": temp = { target: { value: 0.5 * availableAmount } }; handleChange(temp, false); break;

            default:
                break;
        }
    }

    return (
        load ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />
            </div>
            :
            <div className="calculator-buy">
                <div className="calculator-buy-wrapper">
                    <h6>How Much Of Your {valueFormatter(availableAmount, tradePairObj.quote.coin_metadata.coinSymbol)} {tradePairObj.quote.coin_metadata.coinSymbol} Do You Want To Sell?</h6>
                    <div className="input-wrapper">
                        <h6>
                            <span>Sell</span>
                            <span>
                                {
                                    percentages.map(obj => <button onClick={() => handlePercentage(obj)}>{obj.value}</button>)
                                }
                            </span>
                        </h6>
                        <div>
                            <input autoFocus onKeyPress={(e) => e.which === 13 && sellValue ? handlePlaceOrder() : {}} value={sellValue} onChange={(e) => handleChange(e, false)} placeholder="0.00" />
                            <span>{tradePairObj.quote.coin_metadata.coinName}</span>
                        </div>
                    </div>
                </div>
                <button className="calculator-generate" onClick={() => handlePlaceOrder()}>{load ? "Loading...." : "Get InstaQuote"}</button>

            </div>
    )
}
