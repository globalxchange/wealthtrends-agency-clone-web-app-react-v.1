import React, { useContext, useEffect } from 'react'
import nextId from 'react-id-generator'
import { decryptPlaceTrade, placeTradeAPI } from '../../../../../../../services/postAPIs';
import { Agency } from '../../../../../../Context/Context'
import CalculatorBoth from './CalculatorBoth';
import CalculatorBuy from './CalculatorBuy';
import CalculatorSell from './CalculatorSell';
import './tab-dash-calculator.style.scss'

export default function TabDashCalculator() {
    const agency = useContext(Agency);
    const { currentApp, setTradeDashboardStep,mainBalance, readyForTrade, setCStep, cStep, setReadyForTrade, setTradeFinalObj, setTradeStats } = agency

    const checkDecimals = /^\d+([.]\d{0,})?$/

    const { tradePairObj, valueFormatter, setTradeBuySell, sellValue, setSellValue, buyValue, setBuyValue } = agency
    const [load, setLoad] = React.useState(false);
    const [availableAmount, setAvailableAmount] = React.useState('')


    const handleChange = (e, num) => {
        setReadyForTrade(false);
        if (!num) {

            setBuyValue('');
            if (!e.target.value) {
                setSellValue('');
                return;
            }

            let check = checkDecimals.test(e.target.value);
            if (!check)
                return

            setSellValue(e.target.value)
        } else {

            setSellValue('');
            if (!e.target.value) {
                setBuyValue('');
                return;
            }
            let check = checkDecimals.test(e.target.value);
            if (!check)
                return
            setBuyValue(e.target.value)

        }
    }
    const handlePlaceOrder = async () => {
        setLoad(true)
        if (!sellValue) {
            // alert("Buy")
            let token = localStorage.getItem('idToken');
            let email = localStorage.getItem('userEmail');
            let tempObj = {
                token: token,
                email: email,
                app_code: currentApp.app_code,
                profile_id: currentApp.profile_id,
                purchased_amount: buyValue,
                stats: true,
                path_id: tradePairObj.exchangeBank.pathId
            }
            let first = await placeTradeAPI(tempObj);
            let encrypt = first.data.data;
            let second = await decryptPlaceTrade(encrypt);
            if (second.data.status) {
                setCStep(0)
                setTradeFinalObj(tempObj);
                setTradeStats(second.data);
                setTradeBuySell(true);
                setSellValue(second.data.finalFromAmount);
                setReadyForTrade(true);

                // setTradeDashboardStep(1);
            }
            setLoad(false);
            setCStep(0)

            console.log("Path Info Buy", second)
        } else {
            // alert("Sell")
            setLoad(true)
            let token = localStorage.getItem('idToken');
            let email = localStorage.getItem('userEmail');
            let tempObj = {
                token: token,
                app_code: currentApp.app_code,
                email: email,
                profile_id: currentApp.profile_id,
                from_amount: sellValue,
                stats: true,
                path_id: tradePairObj.exchangeBank.pathId
            }

            let first = await placeTradeAPI(tempObj);
            let encrypt = first.data.data;
            let second = await decryptPlaceTrade(encrypt);
            if (second.data.status) {
                setCStep(0)
                setTradeFinalObj(tempObj);
                setTradeBuySell(false)
                setTradeStats(second.data);
                setBuyValue(second.data.finalToAmount)
                setReadyForTrade(true);
                // setTradeDashboardStep(1);
            }
            setLoad(false)
            setCStep(0)
            console.log("Path Info Sell", second.data)
        }

    }
    const selectCalculator = () => {
        switch (cStep) {
            case 0: return <CalculatorBoth setCStep={setCStep} tempArray={tempArray} setTradeDashboardStep={setTradeDashboardStep} load={load} readyForTrade={readyForTrade} sellValue={sellValue} buyValue={buyValue} valueFormatter={valueFormatter} tradePairObj={tradePairObj} />;
            case 1: return <CalculatorSell percentages={tempArray[0].percentage} availableAmount={availableAmount} valueFormatter={valueFormatter} load={load} handlePlaceOrder={handlePlaceOrder} tradePairObj={tradePairObj} sellValue={sellValue} handleChange={handleChange} />;
            case 2: return <CalculatorBuy availableAmount={availableAmount} valueFormatter={valueFormatter} load={load} handlePlaceOrder={handlePlaceOrder} tradePairObj={tradePairObj} buyValue={buyValue} handleChange={handleChange} />
            default: break;
        }
    }

    const setUpAvailableAmount = () => {
        let tempObj = [...mainBalance.crypto, ...mainBalance.fiat].find(obj =>{ return tradePairObj.quote?._id === obj.coinSymbol });
        setAvailableAmount(tempObj.coinValue);

    }
    useEffect(() => {
        console.log("Buy sell", buyValue, sellValue)
    }, [buyValue, sellValue])



    useEffect(() => {
        if (!tradePairObj.quote) return;
        setUpAvailableAmount()
    }, [tradePairObj.quote])

    return (
        <div className="tab-dash-calculator">
            <div className="dash-calculator-header">
                <button>Limit</button>
                <button>Market</button>
            </div>
            <div className="dash-calculator-body">
                {
                    selectCalculator()
                }
                {/* <CalculatorBoth tempArray={tempArray} setTradeDashboardStep={setTradeDashboardStep} load={load} handlePlaceOrder={handlePlaceOrder} readyForTrade={readyForTrade} sellValue={sellValue} buyValue={buyValue} handleChange={handleChange} valueFormatter={valueFormatter} tradePairObj={tradePairObj}  /> */}
                {/* <CalculatorSell load ={load} handlePlaceOrder={handlePlaceOrder} tradePairObj={tradePairObj} sellValue={sellValue} handleChange={handleChange}/> */}
            </div>
        </div>
    )
}
const tempArray = [
    {
        title: "Sell",
        percentage: [
            { keyId: nextId(), value: "max" },
            { keyId: nextId(), value: "25%" },
            { keyId: nextId(), value: "50%" },
        ]
    },
    {
        title: "Buy",
        percentage: []
    },
]