import React from 'react'

export default function CalculatorBoth({ tempArray, setCStep, setTradeDashboardStep, handlePlaceOrder, load, readyForTrade, sellValue, buyValue, handleChange, valueFormatter, tradePairObj }) {
    return (
        <div className="calculator-both">
            {
                tempArray.map((obj, num) => <div className="calculator-amount">
                    <h6 className="amount-header">
                        <span>{obj.title}</span>
                        {/* <span>
                        {
                            obj.percentage.map(obj => <button>{obj.value}</button>)
                        }
                    </span> */}
                    </h6>
                    <div>
                        <input
                            onFocus={() => setCStep(num === 0 ? 1 : 2)}
                            value={!num ? sellValue : buyValue} onChange={(e) => handleChange(e, num)} placeholder={`${valueFormatter(0, !num ? tradePairObj?.list?.[1] : tradePairObj?.list?.[0])}  `} />
                        <h6>{!num ? tradePairObj?.quote?.coin_metadata?.coinName : tradePairObj?.base?.coin_metadata?.coinName}</h6>
                    </div>
                </div>
                )
            }
            <button onClick={() => readyForTrade ? setTradeDashboardStep(1) : console.log()} disabled={!sellValue && !buyValue} className="calculator-generate">{load ? "Loading...." : "Place Order"}</button>

        </div>
    )
}
