import React from 'react'
import LoadingAnimation from '../../../../../../../lotties/LoadingAnimation'

export default function CalculatorBuy({ load, valueFormatter, handlePlaceOrder, availableAmount, tradePairObj, buyValue, handleChange }) {
    return (
        load?
        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
            <LoadingAnimation />
        </div>
        :
        <div className="calculator-buy">
            <div className="calculator-buy-wrapper">
                <h6>With Your Balance Of {valueFormatter(availableAmount, tradePairObj?.quote?._id)} {tradePairObj?.quote?._id}. You Can Buy A Maximum Of {valueFormatter(availableAmount * (1 / tradePairObj?.exchangeBank?.finalRate), tradePairObj.base.coin_metadata.coinSymbol)} {tradePairObj.base.coin_metadata.coinSymbol}. How Much Buy Do You Want To Buy?</h6>
                <div className="input-wrapper">
                    <h6>Buy</h6>
                    <div>
                        <input autoFocus onKeyPress={(e) => e.which === 13 && buyValue ? handlePlaceOrder() : {}} value={buyValue} onChange={(e) => handleChange(e, true)} placeholder="0.00" />
                        <span>{tradePairObj.base.coin_metadata.coinName}</span>
                    </div>
                </div>
            </div>
            <button className="calculator-generate" onClick={() => handlePlaceOrder()}>{load ? "Loading...." : "Get InstaQuote"}</button>
        </div>
    )
}
