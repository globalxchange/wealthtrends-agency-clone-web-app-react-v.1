import React, { useContext } from 'react'
import nextId from 'react-id-generator';
import Images from '../../../../../../../assets/a-exporter';
import Constant from '../../../../../../../json/constant'
import LoadingAnimation from '../../../../../../../lotties/LoadingAnimation';
import { Agency } from '../../../../../../Context/Context';
import './tab-dash-result.style.scss';
export default function TabDashResult() {
    const agency = useContext(Agency);
    const { finalTradeResponse, resetTradeDashboard,mainBalance, setCheckedId, setTradeState, resetFullTrade, tradePairObj, setAssetSelected, setAssetClassSelected, valueFormatter } = agency;

    const handleClick = (val) => {
        setTradeState(false);
        resetFullTrade();
        if (!val) {

            if (tradePairObj.base.coin_metadata.type === "crypto") {
                setCheckedId(Constant.navbarList[0].id)
                setAssetSelected(Constant.navbarList[0]);
                let a = setTimeout(() => {
                    let coin = mainBalance.crypto.find(obj =>{console.log("comparing trade",obj.coinSymbol ,tradePairObj.base.coin_metadata.coinSymbol ); return obj.coinSymbol === tradePairObj.base.coin_metadata.coinSymbol});
                    console.log("comparing trade coin",coin );
                    setAssetClassSelected(coin);

                    clearTimeout(a);

                }, 200)

            } else {
                setCheckedId(Constant.navbarList[1].id)
                setAssetSelected(Constant.navbarList[1]);
                let a = setTimeout(() => {
                    let coin = mainBalance.fiat.find(obj =>{console.log("comparing trade",obj.coinSymbol ,tradePairObj.base.coin_metadata.coinSymbol ); return obj.coinSymbol === tradePairObj.base.coin_metadata.coinSymbol});
                    console.log("comparing trade coin",coin );
                    setAssetClassSelected(coin);

                    clearTimeout(a);

                }, 200)

            }
        } else {
            if (tradePairObj.quote.coin_metadata.type === "crypto") {
                setCheckedId(Constant.navbarList[0].id)
                setAssetSelected(Constant.navbarList[0]);
                let a = setTimeout(() => {
                    let coin = mainBalance.crypto.find(obj =>{console.log("comparing trade",obj.coinSymbol ,tradePairObj.quote.coin_metadata.coinSymbol ); return obj.coinSymbol === tradePairObj.quote.coin_metadata.coinSymbol});
                    console.log("comparing trade coin",coin );
                    setAssetClassSelected(coin);
                    clearTimeout(a);
                }, 200)

            } else {
                setCheckedId(Constant.navbarList[1].id)
                setAssetSelected(Constant.navbarList[1]);
                let a = setTimeout(() => {
                    let coin = mainBalance.fiat.find(obj =>{console.log("comparing trade",obj.coinSymbol ,tradePairObj.quote.coin_metadata.coinSymbol ); return obj.coinSymbol === tradePairObj.quote.coin_metadata.coinSymbol});
                    console.log("comparing trade coin",coin );
                    setAssetClassSelected(coin);
                    clearTimeout(a);

                }, 200)

            }

        }

    }
    return (
        <div className="tab-dash-result">
            <div style={!finalTradeResponse.status?{backgroundColor: 'red'}:{}} className="tab-dash-result-header">
                <h6>{!finalTradeResponse.status ? "Trade Failed" : "Completed Successfully"}</h6>
                <span onClick={resetTradeDashboard}>X</span>

            </div>
            <div className="tab-dash-result-body">
                <h6 className={!finalTradeResponse.status ? "d-none" : "completed-text"}>
                    Your Have Successfully Sold {valueFormatter(finalTradeResponse?.userResult?.debit_obj?.amount, finalTradeResponse?.userResult?.debit_obj?.coin)} {tradePairObj.quote.coin_metadata.coinName} For {valueFormatter(finalTradeResponse?.bankerResult?.credit_obj?.amount, finalTradeResponse?.bankerResult?.credit_obj?.coin)} {tradePairObj.base.coin_metadata.coinName} With {tradePairObj.exchangeBank.banker}
                </h6>
                {
                    !finalTradeResponse.status ?
                        <div className="h-50 w-100 d-flex justify-content-center flex-column align-items-center ml-auto mr-auto">
                            <LoadingAnimation type="failed" loop={false} size={{ height: 150, width: 150 }} />
                            <h5 className="text-danger text-center">{finalTradeResponse.message}</h5>
                        </div>
                        :
                        blocks.map((obj, i) => <div className="block-wrapper">
                            <h6>New {!i ? tradePairObj.base.coin_metadata.coinSymbol : tradePairObj.quote.coin_metadata.coinSymbol} Balance</h6>
                            <div>
                                <button>{
                                    !i ?
                                        valueFormatter(finalTradeResponse?.bankerResult?.credit_obj?.updated_balance, finalTradeResponse?.bankerResult?.credit_obj.coin) :
                                        valueFormatter(finalTradeResponse?.userResult?.debit_obj?.updated_balance, finalTradeResponse?.userResult?.debit_obj?.coin)
                                }
                                </button>

                                <button>
                                    {!i ? tradePairObj.base.coin_metadata.coinSymbol : tradePairObj.quote.coin_metadata.coinSymbol}
                                    <img src={obj.icon} /></button>
                            </div>
                        </div>)
                }
                <div className="completed-buttons">
                    <button className={!finalTradeResponse.status?"d-none":""} onClick={() => handleClick(false)}>{tradePairObj.base?.coin_metadata.coinSymbol} Vault</button>
                    <button className={!finalTradeResponse.status?"d-none":""} onClick={() => handleClick(true)}>{tradePairObj.quote?.coin_metadata.coinSymbol} Vault</button>

                </div>
            </div>


        </div>
    )
}
const blocks = [
    { keyId: nextId(), coin: "BTC", icon: Images.bitcoin, value: 0.0362 },
    { keyId: nextId(), coin: "CAD", icon: Images.bitcoin, value: 0.0362 },
]