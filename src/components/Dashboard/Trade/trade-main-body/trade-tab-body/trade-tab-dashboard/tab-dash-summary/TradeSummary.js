import React from 'react'
import nextId from 'react-id-generator'
import { Agency } from '../../../../../../Context/Context';

export default function TradeSummary({ setSummaryStep, feesRate, feesCoin }) {
    const agency = React.useContext(Agency);
    const { tradeStats, decimalValidation, valueFormatter, currencyImageList } = agency;
    const getNativeValue = (id) => {
        switch (id) {
            case "broker": return decimalValidation(tradeStats?.brokerData?.fee * feesRate, feesCoin ) ? parseFloat(tradeStats?.brokerData?.fee * feesRate).toFixed(6) : valueFormatter(tradeStats?.brokerData?.fee * feesRate,feesCoin)
            case "agency": return decimalValidation(tradeStats?.appData?.app_fee * feesRate, feesCoin) ? parseFloat(tradeStats?.appData?.app_fee * feesRate).toFixed(6) : valueFormatter(tradeStats?.appData?.app_fee * feesRate, feesCoin)
            case "banker": return decimalValidation(tradeStats?.bankerData?.trade_fee_native * feesRate, feesCoin) ? parseFloat(tradeStats?.bankerData?.trade_fee_native * feesRate).toFixed(6) : valueFormatter(tradeStats?.bankerData?.trade_fee_native * feesRate, feesCoin)
        }
    }

    const getUSDValue = (id) => {
        switch (id) {
            case "broker": return valueFormatter(tradeStats?.brokerData?.fee_usd, "USD")
            case "agency": return valueFormatter(tradeStats?.appData?.app_fee_usd, "USD")
            case "banker": return valueFormatter(tradeStats?.bankerData?.total_fee_usd, "USD")
        }
    }

    const getPercentage = (id) => {
        switch (id) {
            case "broker": return parseFloat(tradeStats?.brokerData?.broker_fee_percentage)
            case "agency": return parseFloat(tradeStats?.appData?.app_fee_percentage)
            case "banker": return parseFloat(tradeStats?.bankerData?.banker_fee_percentage)
        }
    }

    return (
        <div className="trade-summary-main">
            <div className="trade-summary-header">
                <div>
                    <h4>Trade Audit</h4>
                    <p>Three Components Of Each Fee</p>
                </div>
                <button onClick={() => setSummaryStep(3)}><img src={currencyImageList[feesCoin]} />{ feesCoin}</button>

            </div>
            <div className="trade-summary-body">
                {
                    feesList.map(obj => <div className="trade-fees-wrapper" key={obj.keyId}>
                        <div>
                            <h6>{obj.name}</h6>
                            <p>{parseFloat(getPercentage(obj.id)).toFixed(2)}% Of The Fee</p>

                        </div>    <div>
                            <h6>{getNativeValue(obj.id)}</h6>
                            <p>{getUSDValue(obj.id)}</p>

                        </div>
                    </div>)
                }

            </div>

        </div>
    )
}

const feesList = [
    { keyId: nextId(), name: "Broker Fees", id: "broker", text: "Fees Set By Your Upline" },
    { keyId: nextId(), name: "Platform Fees", id: "agency", text: "Fees Set By This App" },
    { keyId: nextId(), name: "Banker Fees", id: "banker", text: "Fees Set By The Exchange" },
]