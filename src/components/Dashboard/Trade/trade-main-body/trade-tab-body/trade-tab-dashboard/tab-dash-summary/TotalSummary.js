import React from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../../../../assets/a-exporter';
import { Agency } from '../../../../../../Context/Context';

export default function TotalSummary({ defaultValue, setTradeDashboardStep, finalTradeExecute, rate, otherCoin, valueFormatter, tradeStats, setSummaryStep }) {

    const [availableAmount, setAvailableAmount] = React.useState('');
    const agency = React.useContext(Agency);
    const { mainBalance, tradePairObj, sellValue,currencyImageList} = agency;
    const getValues = (type) => {
        switch (type) {
            case "sell": return valueFormatter(tradeStats?.finalFromAmount, tradeStats?.purchased_from)
            case "buy": return valueFormatter(tradeStats?.finalToAmount, tradeStats?.coin_purchased)
            case "rate": return valueFormatter(tradeStats?.final_1_to_currency_in_from_currency * rate, otherCoin?.coinSymbol)
        }
    }
    const getCoins = (type) => {
        switch (type) {
            case "sell": return tradeStats?.purchased_from
            case "buy": return tradeStats?.coin_purchased
            case "rate": return otherCoin?.coinSymbol
        }
    }
    const setUpAvailableAmount = () => {
        let tempObj = [...mainBalance.crypto, ...mainBalance.fiat].find(obj => { return tradePairObj.quote?._id === obj.coinSymbol });
        setAvailableAmount(tempObj.coinValue);
    }
    React.useEffect(() => {
        if (!tradePairObj.quote) return;
        setUpAvailableAmount()
    }, [tradePairObj.quote])
    return (
        <div className="total-summary">
            <div className="trade-summary-body">
                {
                    dataBoxes.map((obj, num) => <div key={obj.keyId} className="t-s-data-boxes">
                        <p>{obj.text} {num === 2?tradePairObj.base?.coin_metadata?.coinName:''}</p>
                        <div>
                            <h6>{getValues(obj.id)}</h6>
                            <button
                                onClick={() => num === 2 ? setSummaryStep(1) : {}}
                            ><img className="currency-coin" src={currencyImageList[getCoins(obj.id)]} />{getCoins(obj.id)}
                                {num === 2 ? <img className="dropdown-pic" src={Images.triangle} /> : ''}
                            </button>
                        </div>
                    </div>)
                }

            </div>
            <div className="trade-summary-footer">
                {
                    buttons.map((obj, num) => <button
                        disabled={num && (parseFloat(sellValue) > parseFloat(availableAmount))}
                        onClick={() => !num ? setTradeDashboardStep(0) : finalTradeExecute()}
                        key={obj.keyId}>
                        {obj.name}
                    </button>)
                }

            </div>

        </div>
    )
}
const dataBoxes = [
    { keyId: nextId(), text: "You Are Selling", id: "sell" },
    { keyId: nextId(), text: "You Are Buying", id: "buy" },
    { keyId: nextId(), text: "Price Per", id: "rate" },
]
const buttons = [
    { keyId: nextId(), name: "Edit Trade", id: "edit" },
    { keyId: nextId(), name: "Place Trade", id: "place" },
]