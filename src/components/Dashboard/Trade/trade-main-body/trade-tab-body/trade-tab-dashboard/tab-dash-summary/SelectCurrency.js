import React from 'react'
import nextId from 'react-id-generator';
import LoadingAnimation from '../../../../../../../lotties/LoadingAnimation';

export default function SelectCurrency({ allCoins, handleOtherCoin, currencyLoad }) {
    const [searchTerm, setSearchTerm] = React.useState('');
    const [coinListLocal, setCoinListLocal] = React.useState([]);
    const [coinType, setCoinType] = React.useState("fiat")

    React.useEffect(() => {
        setSearchTerm('');
        if (coinType === "fiat") {
            setCoinListLocal([...allCoins.fiat])
        } else {
            setCoinListLocal([...allCoins.crypto])
        }
    }, [coinType])
    return (
        currencyLoad ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />
            </div>
            :
            <div className="select-currency-main">
                <div className="select-currency-header">
                    <input onChange={(e) => setSearchTerm(e.target.value.toLowerCase())} autoFocus placeholder="Search Currency" />
                </div>
                <div className="select-currency-tabs">
                    {
                        [
                            { keyId: nextId(), id: "fiat", name: "Fiat" },
                            { keyId: nextId(), id: "crypto", name: "Crypto" }
                        ].map(obj => <button
                            onClick={() => setCoinType(obj.id)}
                            className={obj.id === coinType ? "selected-type" : ""}
                            key={obj.keyId}>
                            {obj.name}
                        </button>)
                    }

                </div>
                <div className="select-currency-scroll">
                    {
                        coinListLocal.filter(x => {
                            return x.coinName.toLowerCase().startsWith(searchTerm) || x.coinSymbol.toLowerCase().startsWith(searchTerm)
                        })
                            .map(obj => <div
                                onClick={() => handleOtherCoin(obj)}
                                className="single-coin-wrapper">
                                <img src={obj?.coinImage} />
                                <h6>{obj.coinName}</h6>
                            </div>
                            )
                    }

                </div>

            </div>
    )
}
