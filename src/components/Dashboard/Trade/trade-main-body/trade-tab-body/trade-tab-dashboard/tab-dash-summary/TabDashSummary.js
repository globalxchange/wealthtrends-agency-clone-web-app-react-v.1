import React, { useContext } from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../../../../assets/a-exporter';
import { decryptPlaceTrade, placeTradeAPI } from '../../../../../../../services/postAPIs';
import { Agency } from '../../../../../../Context/Context'
import './tab-dash-summary.style.scss'
import TotalSummary from './TotalSummary';
import SelectCurrency from './SelectCurrency';
import TradeSummary from './TradeSummary';
import { conversionAPI } from '../../../../../../../services/getAPIs';
export default function TabDashSummary() {
    const agency = useContext(Agency);
    const { tradeStats, tradePairObj, cryptoUSD, allCoins, finalTradeExecute, valueFormatter, setTradeDashboardStep } = agency
    const [summaryStep, setSummaryStep] = React.useState(0);
    const [rate, setRate] = React.useState(1);
    const [feesRate, setFeesRate] = React.useState(1)
    const [feesCoin, setFeesCoin] = React.useState(tradeStats?.fees_in_coin)
    const [otherCoin, setOtherCoin] = React.useState();
    const [currencyLoad, setCurrencyLoad] = React.useState(false)

    const summaryPage = () => {
        switch (summaryStep) {
            case 0: return <TotalSummary finalTradeExecute={finalTradeExecute} setTradeDashboardStep={setTradeDashboardStep} rate={rate} otherCoin={otherCoin} valueFormatter={valueFormatter} tradeStats={tradeStats} tradePairObj={tradePairObj} setSummaryStep={setSummaryStep} />;
            case 1: return <SelectCurrency handleOtherCoin={handleOtherCoin} currencyLoad={currencyLoad}  allCoins={allCoins} />
            case 2: return <TradeSummary feesRate={feesRate} feesCoin={feesCoin} setSummaryStep={setSummaryStep} />
            case 3: return <SelectCurrency handleOtherCoin={handleFeesCoin} currencyLoad={currencyLoad}  allCoins={allCoins} />
        }
    }
    const handleFeesCoin = async(obj) =>{
        setFeesCoin(obj.coinSymbol);
        if (obj.coinSymbol === tradePairObj?.base?._id) {
            setFeesRate(1);
        } else {
            setCurrencyLoad(true);
            let res = await conversionAPI(tradePairObj?.base?._id, obj?.coinSymbol);
            console.log("conversion", res.data)
            setFeesRate(res.data[`${tradePairObj?.base?._id?.toLowerCase()}_${obj?.coinSymbol.toLowerCase()}`])
        }
        setCurrencyLoad(false)
        setSummaryStep(2)

    }
    const handleOtherCoin = async (obj) => {
        setOtherCoin(obj)
        if (obj.coinSymbol === tradePairObj?.quote?._id) {
            setRate(1);
        } else {
            setCurrencyLoad(true);
            let res = await conversionAPI(tradePairObj?.quote?._id, obj?.coinSymbol);
            console.log("other rate", res.data, res.data["usd_inr"], res.data[`${tradePairObj?.quote?._id?.toLowerCase()}_${obj?.coinSymbol.toLowerCase()}`])
            setRate(res.data[`${tradePairObj?.quote?._id?.toLowerCase()}_${obj?.coinSymbol.toLowerCase()}`])
        }
        setCurrencyLoad(false)
        setSummaryStep(0)
    }
    React.useEffect(() => {
        let find = [...allCoins.fiat, ...allCoins.crypto].find(obj => { return obj.coinSymbol === tradePairObj?.quote?._id });
        console.log("foundCoin", find)
        setOtherCoin(find)
    }, [allCoins])

    return (
        <div className="tab-dash-summary">
            <div className="dash-summary-header">
                <img onClick={() =>summaryStep === 3?setSummaryStep(2): setSummaryStep(0)} className={`d-back-button ${!summaryStep ? "d-none" : ""}`} src={Images.tradeBackButton} />
                <h6>
                    {summaryStep === 1 || summaryStep === 3? "Select Currency To View Price" : "Trade Summary"}
                </h6>
                <img onClick={() => setSummaryStep(2)} className={`d-expand-button ${!summaryStep ? "" : "d-none"}`} src={Images.expandButton} />
            </div>
            <div className="dash-summary-body">
                {summaryPage()}
            </div>

        </div>
    )
}
