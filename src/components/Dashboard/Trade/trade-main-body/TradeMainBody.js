import React, { useContext } from 'react'
import { Agency } from '../../../Context/Context'
import './trade-main-body.style.scss'
import TradeTabBody from './trade-tab-body/TradeTabBody'
import TradeTabHeader from './trade-tab-header/TradeTabHeader'
import Images from '../../../../assets/a-exporter';
import {useHistory} from 'react-router-dom'
export default function TradeMainBody() {
    const history = useHistory();
    const agency = useContext(Agency);
    const {tradeDashboardTab } = agency

    return (
        <React.Fragment>
            <div className={`trade-tab-body ${tradeDashboardTab?"extend__height":""}`}>
                <TradeTabBody />
            </div>
            <div onClick={()=>history.push('/dashboard/Terminals')} className={!tradeDashboardTab?"trade-tab-footer": "d-none"}>
                <img src={Images.goToTerminals} />
            </div>           
        </React.Fragment>
    )
}
