import React from 'react'
import Images from '../../../../assets/a-exporter'
// import TradeTabHeader from '../../TradeTerminal/trade-main-body/trade-tab-header/TradeTabHeader';
import TradeTabHeader from '../trade-main-body/trade-tab-header/TradeTabHeader';
export default function TradeMainHeaderDefault() {
    return (
        <div className="trade-main-header-default-normal">
            <div className="t-s-h-d-terminals">
                <img src={Images.moreInformation} />
                
            </div>
            <div className="t-s-h-d-right-side">
                {/* <TradeTabHeader /> */}
                <TradeTabHeader />

            </div>
            
        </div>
    )
}
