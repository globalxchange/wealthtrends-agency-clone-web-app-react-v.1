import React, { useContext } from 'react'
import { Agency } from '../../../Context/Context'
import './trade-main-header.style.scss'
import TradeMainHeaderDefault from './TradeMainHeaderDefault';
import TradeMainHeaderTabList from './TradeMainHeaderTablist';
export default function TradeMainHeader() {
    const agency = useContext(Agency);
    const { tradeTabStep } = agency;
    return (
        <React.Fragment>
            {
                <TradeMainHeaderDefault /> 
            }

        </React.Fragment>
    )
}
