import React from 'react'
import Images from '../../../../assets/a-exporter'
import { Agency } from '../../../Context/Context'
export default function TradeMainHeaderTabList() {
    const agency = React.useContext(Agency)
    const {tradePairObj} = agency
    return (
        <div className="trade-main-header-tab-list">
            <div className="tab-list-left">
                <img src={Images.terminalLogo}/>

            </div>
            <div className="tab-list-right">
                <h6>
                    <img src={tradePairObj?.base?.coin_metadata?.coinImage}/>
                    Buy {tradePairObj?.base?.coin_metadata?.coinName}
                </h6>

            </div>
            
            
        </div>
    )
}
const tempObj ={
    coin: "Bitcoin",
    action: "Buy",
    icon: Images.bitcoin
}