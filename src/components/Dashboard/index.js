import React, { useEffect, useState, useContext, lazy, Suspense } from 'react'
import { Redirect, useHistory } from 'react-router-dom';
import LoadingAnimation from '../../lotties/LoadingAnimation';
import { authenticate } from '../../services/postAPIs'
import { Agency } from '../Context/Context';
import NotAuthorized from './Content/NotAuthorised/NotAuthoised';
import DashboardComponent from './DashboardComponent'

export default function Dashboard() {
    const history = useHistory();
    const [logout, setLogout] = React.useState(false);
    const agency = React.useContext(Agency)
    useEffect(() => {
        let app = localStorage.getItem("appCode");
        if(!app){
            setLogout(true)
        }
        agency.initialLoads()
        authenticate()
            .then(res => {
                if (res.data.status) {
                    agency.setRefreshSession(false)
                    agency.verification(false)
                    agency.setCheckingLoggedIn(false)
                } else {
                    history.push('/dashboard/NoAccess')
                    agency.setCheckingLoggedIn(true)
                    agency.setRefreshSession(true)
                }
            })
    }, [])
    return (
        <>
            {
                logout?
                <Redirect to="/login" />
                :
                <DashboardComponent />
            }

        </>
    )
}
