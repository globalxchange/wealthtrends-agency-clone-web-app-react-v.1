import React from 'react'
import './search-box.style.scss'
import Images from '../../../assets/a-exporter'
export default function SearchBox() {
    return (
        <div className="search-box">
            <div className="search-header">
                <input placeholder="Search Anything"/>
                <img src ={Images.search} alt="" />

            </div>
            
        </div>
    )
}
