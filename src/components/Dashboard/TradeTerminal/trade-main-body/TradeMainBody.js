import React, { useContext } from 'react'
import { Agency } from '../../../Context/Context'
import './trade-main-body.style.scss'
import TradeTabBody from './trade-tab-body/TradeTabBody'
import TradeTabHeader from './trade-tab-header/TradeTabHeader'
export default function TradeMainBody() {
    const agency = useContext(Agency);
    const {tradeDashboardTab } = agency
    return (
        <React.Fragment>
            <div className="trade-tab-header">
                <TradeTabHeader />
            </div>
            <div className={`trade-tab-body ${tradeDashboardTab?"extend__height":""}`}>
                <TradeTabBody />
            </div>
            <div className={!tradeDashboardTab?"trade-tab-footer": "d-none"}>
            </div>           
        </React.Fragment>
    )
}
