import React, { useEffect } from 'react'
import { tradeForBaseAsset, tradeForExchangeList, tradeForQuoteAsset } from '../../../../../../services/getAPIs';
import { Agency } from '../../../../../Context/Context'
import './trade-tab-list.style.scss'
import Images from '../../../../../../assets/a-exporter'
import LoadingAnimation from '../../../../../../lotties/LoadingAnimation';
export default function TradeTabList({ searchTerm }) {
    const agency = React.useContext(Agency);
    const { tradeTabStep, tradePairObj, setTradePairObj, setTradeTabStep, resetTradeDashboard, valueFormatter } = agency;
    const [currentList, setCurrentList] = React.useState([]);
    const [loading, setLoading] = React.useState(false);

    React.useEffect(() => {
        getTradeList()
    }, [tradeTabStep])

    //On Selecting Asset based on Step number
    const onClickAction = (obj) => {
        switch (tradeTabStep) {
            case 0: setTradePairObj({ ...tradePairObj, base: obj, list: [obj.coin_metadata.coinSymbol, tradePairObj.list[1]] }); setTradeTabStep(1); setCurrentList([]); break;
            case 1: setTradePairObj({ ...tradePairObj, quote: obj, list: [tradePairObj.list[0], obj.coin_metadata.coinSymbol] }); setTradeTabStep(2); setCurrentList([]); break;
            case 2: setTradePairObj({ ...tradePairObj, exchangeBank: obj }); resetTradeDashboard(); setTradeTabStep(3); break;
        }
    }
    //Call API based on step
    const getTradeList = async () => {
        switch (tradeTabStep) {
            case 0:
                setLoading(true);
                let res = await tradeForBaseAsset();
                let tempArr = res.data.pathData.to_currency;
                setCurrentList(tempArr);
                setLoading(false);
                break;
            case 1:
                setLoading(true);
                let resTwo = await tradeForQuoteAsset(tradePairObj.base._id);
                setCurrentList(resTwo.data.pathData.from_currency);
                setLoading(false);
                break;
            case 2:
                setLoading(true);
                let resThree = await tradeForExchangeList(tradePairObj.base._id, tradePairObj.quote._id);
                let temp = resThree.data.pathData.banker.map((obj, i) => {
                    console.log("final rate", obj)
                    return {
                        icon: resThree.data.pathData.banker[i].icons.image1,
                        titleImage: resThree.data.pathData.banker[i].icons.image1,
                        banker: resThree.data.pathData.banker[i]._id,
                        // banker: resThree.data.pathData.banker.find(x =>{return x._id === obj._id})?.displayName,
                        pathId: resThree.data.pathData.paymentPaths[0]?.pathInfo[i]?.path_id,
                        // pathId: resThree.data.pathData.paymentPaths[0]?.pathInfo.find(x =>{return x.banker === obj._id})?.path_id,
                        finalRate: resThree.data.pathData.paymentPaths[0]?.pathInfo[i]?.rate?.instantRate?.finalRate
                        // finalRate: resThree.data.pathData.paymentPaths[0]?.pathInfo.find(x =>{return x.banker === obj._id}).rate?.instantRate?.finalRate
                    }
                })
                setCurrentList(temp);
                setLoading(false);
                break;
        }
    }

    return (
        <div className="trade-tab-list">
            {
                loading || !currentList.length ?
                    <div className="d-flex justify-content-center flex-column align-items-center w-100 h-100">
                        <LoadingAnimation type="no-data" size={{ height: 100, width: 100 }} />
                        <h6>{loading ? "Fetching Assets For You..." : "No Asset.... "}</h6>
                    </div>
                    :
                    currentList.filter(x => {
                        return tradeTabStep === 0 || tradeTabStep === 1 ? x?._id?.toString()?.toLowerCase().startsWith(searchTerm?.toString()?.toLowerCase()) : true
                    })
                        .map(obj =>
                            <div onClick={() => onClickAction(obj)} className={`tab-list-row ${tradeTabStep === 3 && tradePairObj.exchangeBank?.banker === obj.banker ? "selected-bank" : ""}`}>
                                <h6>
                                    <img src={tradeTabStep === 2 || tradeTabStep === 3 ? obj?.icon : obj.coin_metadata?.coinImage} />
                                    {tradeTabStep === 2 || tradeTabStep === 3 ? obj?.banker : obj._id}
                                </h6>
                                <span>{tradeTabStep === 2 || tradeTabStep === 3 ? valueFormatter(obj?.finalRate, tradePairObj.quote._id) + ' ' + tradePairObj.quote._id : obj.select_type?.instant}</span>
                            </div>
                        )
            }

        </div>
    )
}

const tempObj = {
    coin: "Bitcoin",
    action: "Buy",
    icon: Images.bitcoin
}