import React, { useContext, useEffect } from 'react'
import Images from '../../../../../assets/a-exporter';
import { Agency } from '../../../../Context/Context'
import './trade-tab-body.style.scss'
import TradeTabList from './trade-tab-list/TradeTabList';
export default function TradeTabBody() {
    const agency = useContext(Agency);
    const { tradeTabStep } = agency;
    const [searchActive, setSearchActive] = React.useState();
    const [searchTerm, setSearchTerm] = React.useState('')
    const selectText = () => {
        switch (tradeTabStep) {
            case 0: return "Trading Options";
            case 1: return "Exchanges"
            case 2:
            case 3:
                return "Price"
            default: return;
        }
    }
    React.useEffect(()=>{
        setSearchTerm('')
    },[tradeTabStep])

    return (
        <div className="trade-tab-body-child">
            <div className="trade-tab-body-sub-body">
                <div className={`sub-body-list-wrapper `}>
                    <div className="trade-tab-body-sub-header">
                        <span>{
                            searchActive &&(tradeTabStep === 0 || tradeTabStep ===1)
                                ?
                                <input value={searchTerm} onChange={(e)=>setSearchTerm(e.target.value)} placeholder="Type In The Name Of Any Asset" /> : "Asset"
                        } <img className={tradeTabStep === 0 || tradeTabStep === 1?"":"d-none"} onClick={() => setSearchActive(!searchActive)} src={Images.searchDark} /> </span>
                        <span>{selectText()}</span>
                    </div>
                    <div className="tab-list-wrapper">
                        <TradeTabList searchTerm={searchTerm} />

                    </div>

                </div>
                {/* <div className={`sub-body-dashboard-wrapper ${tradeTabStep === 3 ? "dashboard-adjust" : ""}`}>
                    {tradeTabStep === 3 ? <TradeTabDashboard /> : ""}

                </div> */}

            </div>

        </div>
    )
}
