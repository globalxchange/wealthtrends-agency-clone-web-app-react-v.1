import React from 'react'
import { Agency } from '../../Context/Context'
import TradeMainBody from './trade-main-body/TradeMainBody'
import TradeMainHeader from './trade-mian-header/TradeMainHeader'
import './trade.index.style.scss'

export default function TradeIndexTerminal() {
    const agency = React.useContext(Agency);
    React.useEffect(()=>{

        return () =>{
            agency.resetFullTradeTerminal()
        }
    },[])
    return (
        <div className="trade-main-terminal">
            <div className="trade-main-header">
                <TradeMainHeader />

            </div>
            <div className="trade-main-body">
                <TradeMainBody />

            </div>
            
        </div>
    )
}
