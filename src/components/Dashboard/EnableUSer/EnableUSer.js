



import React,{useContext,useEffect,useState} from 'react'
import { Agency } from '../../Context/Context'
import OtpInput from 'react-otp-input';
import {Modal} from 'react-bootstrap'
import QRCode from 'qrcode.react';
import axios from 'axios'
import {CopyToClipboard} from 'react-copy-to-clipboard';
import copyedimag from '../../../assets/copyedimag.png'


import './UserEnabele.scss'
import * as animationData from "..//../common-components/VSA/Controller/loader.json";
import Lottie from "react-lottie";
export default function EnableUSers() {
    const agency = useContext(Agency);

    const { enableuserfunctiom ,enableuserfunctiomclose,enableuser} = agency;
const [SecretCodes,setSecretCode]=useState('')
const [qrcodeValues,setqrcodeValue]=useState('')
const [copied,  setcopied]=    useState(false)
const [otp, setOTP] = useState("");
const [showtoggle,setshowtoggle]=useState('')
const [showheader,setshowheader]=useState('Enable 2FA')
const [showtogglemain,setshowtogglemain]=useState('Enable 2FA')
const handleChange = otp => setOTP(otp)
const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData.default,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };
const copyfunction=()=>{
    
}
const disable2fa= () => {

    setshowtoggle("loadibg")
    axios.post("https://gxauth.apimachine.com/gx/user/mfa/disable", {
 email: localStorage.getItem("userEmail"),
   accessToken:localStorage.getItem("accessToken"), 
   
    })     
    
 
    .then(async res => {

        console.log("hgj655756",res)
        if (res.data.status) {
            
            setshowtoggle('sucessDisable2FA')
        }
        else {
            // alert(res.data.message)
            setshowtoggle("main2fadisable")
           
        }
      })

      


  }
const efaapi=[
    {
        name:"Enable 2FA",
    },
    {
        name:"Disable 2FA",
    },
]
const tooglehedaefun=(e)=>
{
    setshowtogglemain(e) 
    if(e==="Disable 2FA")
    {
        setshowtoggle("Disable2FA")
        setshowheader('Warning')
        return
    }
    

}

 
      const twofactorcheck= () => {
   axios.post("https://gxauth.apimachine.com/gx/user/mfa/status", {
           email: localStorage.getItem("userEmail"),
            }).then(async res => {
    
                console.log("asdasdasdasdahgjghjghjsdasdasdad", res)
                if (res.data.status) { 
               if(res.data.mfa_enabled===true)
               {
              
                setshowtoggle("main2fadisable")
                
               }
                  if(res.data.mfa_enabled===false)
               {
                 encodequrfunction()
              
               }
                }
                else {
                
                
        
                }
              })
        
          }

          const encodequrfunction= () => {
        
        
            axios.post("https://gxauth.apimachine.com/gx/user/mfa/set", {
           email: localStorage.getItem("userEmail"),
           accessToken:localStorage.getItem("accessToken"), 
           app_code: localStorage.getItem("appCode"),
           
            })          .then(async res => {
    
                console.log("asdasdasdasdasdasdasdad", res)
                if (res.data.status) {
                
                    setshowtoggle("main2fenable") 
                    setqrcodeValue(res.data.SecretCode)
                 setSecretCode(res.data.qrcodeValue)
                
                }
                else {
                
                
        
                }
              })
        
        
        
          }
      const fincalclose=()=>{
        enableuserfunctiomclose()
        setshowtoggle("main")
        setshowheader('Enable 2FA')
        setshowtogglemain('Enable 2FA')
        setOTP('')
     
      }

      const finalloptfunction= () => {
   
  if(otp.length===6)
  {
    setshowtoggle("loadibg")
    axios.post("https://gxauth.apimachine.com/gx/user/mfa/set/verify", {
        email: localStorage.getItem("userEmail"),
        accessToken:localStorage.getItem("accessToken"), 
         code:otp,
          
           })
             .then(async res => {
               console.log("asdasdasdasdasdasdasdasdasdasdasdasd", res)
               if (res.data.status) {
                setshowtoggle("sucess")
                setshowheader('Success')
               }
               else {
               
                setshowtoggle("codeerror")
                setOTP('')
               }
             })
  }
    
  else{
      alert("Please Enter Six Digit NUmber")
  }
       
    
    
    
      }

const imgonefucn=()=>{
    setshowheader('Verify 2FA')
    setshowtoggle("sec")
}


const showtoglle=()=>{
    switch(showtoggle)
    {

case "loadibg":
    return(
<div className="alignmentloading">
<Lottie
options={defaultOptions}
width="200px"
height="200px"
/>
</div>
    )

    case  "main2fenable" :
        return(
            <div className="EnableUserMaindiv">

<p style={{

fontSize:"22px",
justifyContent: "center",
fontWeight: "bold",


}}>Security Settings</p>
<h6 style={{justifyContent:"center"}}>It Seems That You Have Not Setup Any 2FA</h6>
      
    <div className="bottomslected">
        <label onClick={()=>setshowtoggle('main')}>
        Enable 2FA
        </label>
        </div>
    
            </div> 
        )

        case  "main2fadisable" :
            return(
                <div className="EnableUserMaindiv">
    
    <p style={{
    
    fontSize:"22px",
    justifyContent: "center",
    fontWeight: "bold",
    
    
    }}>Security Settings</p>
    <h6 style={{justifyContent:"center"}}>It Seems That Your 2FA Is Already Setup</h6>
          
        <div className="bottomslected">
            <label onClick={()=>setshowtoggle('Disable2FA')}>
            Disable 2FA
            </label>
            </div>
        
                </div> 
            )


        case "main":
        return(
            <>
                 {/* <div className="d-flex">
                    {
                        efaapi.map(item=>{
                            return(
                                <div className={showtogglemain===item.name?"divenbalestepsectio":"divenbalestepsectiofalse"} onClick={()=>tooglehedaefun(item.name)}>

<p className="Disable2FA">{item.name}</p>
                                    </div>
                            )
                        })
                    }
                </div> */}
            <div className="EnableUserMaindiv ">

           
            <h2>Step 1</h2>
            <p>Download The Google Authenticator App Onto Your Phone.</p>
                
            
            
                <div className="vidtop">
                <h2>Step 2</h2>
            <p>Add {localStorage.getItem("appName")}  As A Profile By Scanning This QRCode Or Entering The Code. </p>
                </div>
                <div className="qrcodeiv"> 
                <QRCode style={{ height: '100px', width: '100px' }} value={SecretCodes} />
                </div>
            
                <div className="d-flex sectiomcopydiv">
            
                    <h3 className="qurcodetext"> 
                    {qrcodeValues}
                    </h3>
               
                <CopyToClipboard text={qrcodeValues}
                      onCopy={() => setcopied(true)} >
                   
            <img  style={{cursor:"pointer"}} src={copyedimag} alt="" onClick={copyfunction}>
            
                </img>
            
            
            
                    </CopyToClipboard>
            
                    
                
                </div>
                <div className="bottomslected">
                <label onClick={imgonefucn}>
                Ok I Have Done This
                </label>
                </div>
            
                
                    </div>
                    </>
        )
        case  "sucessDisable2FA" :
            return(
                <div className="EnableUserMaindiv">

                    <h6>You Have Successfully Disabled 2FA For Your {localStorage.getItem("appName")} Account.</h6>
         
        
        
        <div className="bottomslected">
            <label onClick={()=>fincalclose()}>
            Close
            </label>
            </div>
        
                </div> 
            )
        case "Disable2FA":
            return(
                <div className="EnableUserMaindiv">

                <h6 style={{textAlign:"center"}}>Are You Sure You Want To Disable 2FA For {localStorage.getItem("userEmail")}.</h6>
  
    <div className="bottomslected">
        <label onClick={disable2fa}>
        Disable 2FA
        </label>
        </div>
    
            </div> 
            )
        case "sec":
            return(
                <div className="EnableUserMaindiv">
                <h2>Step 3</h2>
                    <p>Once You Have Added The {localStorage.getItem("appName")} Profile To The Google Authenticator App, Enter The Six Digit Code To Complete Setup.</p>
                    <div className="">
                    <OtpInput
          value={otp}
          onChange={handleChange}
           numInputs={6}
           otpType="number"
           disabled={false}
                />
                    </div>
        
        
        <div className="bottomslected">
            <label onClick={finalloptfunction}>
            Verify
            </label>
            </div>
        
                </div>
            )

            case  "codeerror" :
                return(
                    <div className="EnableUserMaindiv">
                    <h2>Incorrect Code</h2>
                        <p>It Seems That You Have Entered An Incorrect Code. Please Enter The Latest One</p>
                        <div className="">
                        <OtpInput
              value={otp}
              onChange={handleChange}
               numInputs={6}
               otpType="number"
               disabled={false}
                    />
                        </div>
            
            
            <div className="bottomslected">
                <label onClick={finalloptfunction}>
                Verify
                </label>
                </div>
            
                    </div> 
                )

                case  "sucess" :
                    return(
                        <div className="EnableUserMaindiv">

                            <h6>You Have Successfully Enabled 2FA For Your {localStorage.getItem("appName")} Account. You Will Not Be Able To Login Without Using The Code.</h6>
                 
                
                
                <div className="bottomslected">
                    <label onClick={()=>fincalclose()}>
                    Close
                    </label>
                    </div>
                
                        </div> 
                    )
    }
}


      useEffect(() => {
        twofactorcheck()
      }, [])
  return (
      <>
    <Modal show={enableuser} dialogClassName="my-custommdoel" onHide={fincalclose}aria-labelledby="contained-modal-title-vcenter"   centered>
{
    showtoggle==="main"  || showtoggle=="loadibg" ||showtoggle==="main2fadisable" || showtoggle==="main2fenable"?
null
    :
   <div className="userdeleteheader">
       <>
       <h2 className=" warringdelete"> {showheader}</h2>
       
   </>
    </div>
}   
    <Modal.Body>
    
      {/*    */}

{showtoglle()}
    </Modal.Body>
    
    </Modal>
    
          
         
    
       </>
  )
}
