import arrowDown from "./arrow-down.svg";
import face from "./face.svg";
import BTC from "./BTC.svg";
import leftArrow from "./left-arrow.svg";
import ETH from "./ETH.svg";
import fire from "./fire.svg";
import chevron from "./Chevron.svg";
import terminalLogo from "./terminal-logo.svg";
import logo from "./logo.svg";
import fullScreen from "./full-screen icon.png";
import dashedCircle from "./dashed-circle.png";
import user from "./user.png";
import comingSoon from "./coming-soon.png";
import binance from "./binance.svg";
import bitcoin from "./bitcoin.svg";
import arrowDownBlack from "./arrow-down-black.svg";
import tradingView from "./trading-view.svg";
import sentiment from "./sentiment.svg";
import volume from "./volume.svg";
import price from "./price.svg";
import welcomeTerminal from "./terminal-welcone.svg";
import terminalRight from "./terminal-right.svg";
import loader from "./loader.svg";
import searchIcon from "./search-icon.svg";
import loaderBlack from "./loader-black.png";
import lock from "./lock.svg";
import Eapp from "./ea-app.svg";
import chats from "./chats.svg";
import funds from "./funds.png";
import stream from "./stream.svg";
import backButton from "./back.svg";
import fullScreenButton from "./full-screen.svg";
import ethereumFull from "./ethereum_Logo-01.png";
import bicoinFull from "./bitcoin-logo.svg";
import defaultCoin from "./defaultCoins.png";
import cancel from "./Icon - X.svg";
import backGray from "./Icon - Back.svg";
import circle from "./cicle.svg";
import circleDark from "./circle-dark.svg";
import fundsLogo from "./Funds New Logo White 1.png";
import onHold from "./onHold.svg";
import accountingTool from "./accounting.svg";
import algoSwith from "./algoswith.svg";
import bgImage from "./bg-image.jpg";
import micOff from "./Union.svg";
import trandlate from "./Trandlate.svg";
import cameraOff from "./CameraOff.svg";
import screenShare from "./ScreenShare.svg";
import addPeople from "./Add.svg";
import chatPeople from "./Chat.svg";
import exit from "./out.svg";
import profileOne from "./profile-1.jpg";
import profileTwo from "./profile-2.jpg";
import profileThree from "./profile-3.jpg";
import slabSmall from "./small-slab.svg";
import slabOne from "./slab-one.svg";
import slabTwo from "./slab-two.svg";
import emoji from "./emoji.svg";
import media from "./media.svg";
import sendButton from "./send.svg";
import threeDots from "./three-dots.svg";
import hamburger from "./hamburger.svg";
import addTab from "./addTab.svg";
import cross from "./cross.svg";
import agency from "./agency.svg";
import logoBlack from "./logo-black.svg";
import streamBlack from "./stream-black.svg";
import terminalLogoWhite from "./terminal-logo-white.svg";
import monitor from "./monitor.png";
import landingMid from "./landing-hand.png";
import landingLeft from "./man.png";
import landingRight from "./landing-lady.png";
import tokenOptions from "./token option.svg";
import pulse from "./Pulse 1.png";
import brokerApp from "./broker-app.png";
import instantCryptoCurrency from "./instantCrypto.png";
import vault from "./Vault _ Icon _ White _ Transparent 1.svg";
import teller from "./Tellers White Logo 1.png";
import cryptoLottery from "./cryptoLottery.svg";
import instaWallet from "./Insta_wallet_logo copy 1.png";
import rs1 from "./brain.png";
import rs2 from "./brain-2.png";
import rs3 from "./brain-3.png";
import rs4 from "./brain-4.png";
import fundsSVG from "./Funds white icon 1.svg";
import searchWhite from "./search-white.svg";
import snapPay from "./snapay Logo with name-01 1.svg";
import fxAgency from "./fxagency.svg";
import terminalMobile from "./terminal-mobile.png";
import terminalDesktop from "./terminal-desktop.png";
import terminalLong from "./Logo - Terminals.svg";
import defaultUser from "./user-default.jpg";
import blueDot from './blue-dot.svg';
import arrowBlue from './ARROW-DOWN-BLUE.svg';
import searchBlue from './SEARCH-BLUE.svg';
import tradeStreamLogo from './trade-stream-logo.svg';
import blueSquare from './Rectangle 104.svg';
import gainArrow from './gain-arrow.svg';
import lockArrow from './locked.svg';
import gameIcon from './game-icon.svg';
import pageArrow from './pagination-arrow.svg';
import streamAsset from './stream-asset.svg';
import streamWatch from './stream-watch.svg';
import streamCompetition from './stream-competitions.svg';
import streamFunds from './stream-funds.svg';
import streamTerminals from './stream-terminals.svg';
import streamCrypto from './stream-crypto.svg';
import streamLive from './stream live.svg';
import streamMarket from './stream-market.svg';
import blueTradeStream from './blue-trade-stream.svg';
import whiteTradeStream from './white-trade-stream.svg';
import emailLogo from './email-logo.svg';
import passwordLogo from './password-logo.svg';
export {
  streamCrypto, streamLive, streamMarket,
  blueDot,blueTradeStream,whiteTradeStream,
  streamAsset, passwordLogo,emailLogo,
  streamWatch, 
  streamCompetition, 
  streamFunds, 
  streamTerminals,
  pageArrow,
  blueSquare,
  gameIcon,
  arrowBlue,
  gainArrow, lockArrow,
  searchBlue,
  terminalLong,
  tradeStreamLogo,
  terminalDesktop,
  terminalMobile,
  fxAgency,
  defaultUser,
  fundsSVG,
  searchWhite,
  snapPay,
  tokenOptions,
  pulse,
  vault,
  brokerApp,
  cryptoLottery,
  instantCryptoCurrency,
  teller,
  rs1,
  rs2,
  rs3,
  rs4,
  landingLeft,
  instaWallet,
  landingRight,
  landingMid,
  monitor,
  terminalLogoWhite,
  streamBlack,
  logoBlack,
  agency,
  cross,
  addTab,
  arrowDown,
  face,
  BTC,
  leftArrow,
  ETH,
  fire,
  chevron,
  logo,
  terminalLogo,
  dashedCircle,
  fullScreen,
  user,
  comingSoon,
  loaderBlack,
  binance,
  bitcoin,
  arrowDownBlack,
  price,
  sentiment,
  tradingView,
  volume,
  welcomeTerminal,
  terminalRight,
  loader,
  searchIcon,
  lock,
  Eapp,
  chats,
  funds,
  stream,
  backButton,
  fullScreenButton,
  bicoinFull,
  ethereumFull,
  defaultCoin,
  cancel,
  backGray,
  circle,
  circleDark,
  fundsLogo,
  accountingTool,
  algoSwith,
  onHold,
  bgImage,
  addPeople,
  cameraOff,
  chatPeople,
  exit,
  micOff,
  screenShare,
  trandlate,
  profileOne,
  profileTwo,
  profileThree,
  slabOne,
  slabSmall,
  slabTwo,
  emoji,
  media,
  sendButton,
  threeDots,
  hamburger,
};
