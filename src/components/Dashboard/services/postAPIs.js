import axios from 'axios';

export const login =(email, password) =>{
    return axios.post(`https://gxauth.apimachine.com/gx/user/login`,{
        email: email, password: password
    })
};

export const terminalCoins = () =>{
    let email, token;
    email = localStorage.getItem('userEmail');
    token = localStorage.getItem('idToken');
    console.log('terminal sending', email, token);
    return axios.post('https://terminal.apimachine.com/balance',
        {name: email, token: token}
    )
};
export const authenticate = () =>{
    let email = localStorage.getItem('userEmail');
    let token = localStorage.getItem('idToken');

    return axios.post(`https://comms.globalxchange.com/coin/verifyToken`,
        {email: email, token: token})
};
export const placeTrade = (exchangeId, paidId1, paidId2, amount, direction) =>{
    let email = localStorage.getItem('userEmail');
    let token = localStorage.getItem('idToken');
    console.log('place trade pre', exchangeId, paidId1, paidId2, amount, direction);
    return axios.post(`https://terminal.apimachine.com/place/${exchangeId.toLowerCase()}/${paidId1}/${paidId2}/${amount}/${direction.toLowerCase()}/market/0`,
        {
            name: email,
            token: token
        }
    )
};
export const addMail = (body) =>{
    let UserEmail = 'shailesh@nvestbank.com';
    let MailName = "advertisement";
    let Subject = body.subject;
    let Body = body.body;
    return axios.post(`https://nvestemailservice.azurewebsites.net/api/MailStructures`,{
        UserEmail: UserEmail, MailName: MailName, Subject: Subject, Body: Body
    })
};
export const getUserForChat = () =>{
    let email = localStorage.getItem('userEmail');
    let token = localStorage.getItem('idToken');
    return axios.post('https://chatsapi.globalxchange.io/gxchat/get_user', {
         email,
         token
    });
};
export const vaultBalance = () =>{
    let email = localStorage.getItem('userEmail');
    let token = localStorage.getItem('idToken');
    return axios.post(`https://terminal.apimachine.com/vaultBalance`,{
        name: email,
        token: token
    })
};
