import axios from 'axios';

export const getAssetData = (first, second) =>{
    return axios.get(`https://terminal.apimachine.com/assetData/${first}/${second}`)
};
export const getForexCryptoList = () =>{
    let email = localStorage.getItem('userEmail');
    return axios.get(`https://comms.globalxchange.com/coin/vault/coins_data?email=${email}`)
};
export const getUserDetails = () =>{
    let email = localStorage.getItem('userEmail');
  return axios.get(`https://comms.globalxchange.com/user/details/get?email=${email}`)
};
export const getIcedList = () =>{
    let email = localStorage.getItem('userEmail');
    return axios.get(`https://comms.globalxchange.com/coin/vault/earnings/ice/gettxns?email=${email}`)
};
export const cryptoUSD = () =>{
    return axios.get(`https://comms.globalxchange.com/coin/getCmcPrices?convert=USD`)
};
export const getMails = () =>{
    return axios.get(`https://nvestemailservice.azurewebsites.net/api/MailStructures?email=shailesh@nvestbank.com`);
};

export const exchangeData = () =>{
    return axios.get(`https://storeapi.apimachine.com/dynamic/iceprotocol/exchangeList?key=62816d7e-e570-46a2-8c9d-0c1cc97f7bf8`);
};
export const exchangeValues = (first, second) =>{
    return axios.get(`https://terminal.apimachine.com/history/${first}/${second}/20`)
};
export const getAvailavlePairs = () =>{
  return axios.get('https://terminal.apimachine.com/availablePairs');
};
export const getAllCoins = () =>{
  return axios.get(`https://terminal.apimachine.com/availableCoins`)
};
export const getMarketData = () =>{
    return axios.get(`https://terminal.apimachine.com/marketDataFetch`);
};
export const getMarketValue = (exchange, first, second) =>{
    return axios.get(`https://terminal.apimachine.com/marketCoinPrice${exchange}/${first}/${second}`)
};
export const binanceList = () =>{
  return axios.get(`https://terminal.apimachine.com/marketDataFetch`)
};
