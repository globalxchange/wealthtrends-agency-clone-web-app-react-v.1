import React, { useContext } from 'react'
import './otp-wrapper.style.scss'
import OtpInput from 'react-otp-input';
export default function OTPWrapper({ email, setChangePassword, setOTP }) {
    const [value, setValue] = React.useState('');
    const handleChange = (e) => {
        console.log(e)
        setOTP(e)
        setValue(e)
    }
    return (
        <React.Fragment>
            <div className="otp-wrapper w-100">
                <h6 className="otp-title">Enter The Six Digit Code That Was Just Sent To {email}</h6>
                <div className="row-wrapper">
                    <OtpInput value={value} onChange={handleChange} numInputs={6} />
                </div>

            </div>
            <button className="otp-next-button" disabled={value.length !== 6} onClick={() => setChangePassword(3)}>
                Next
            </button>
        </React.Fragment>
    )
}
