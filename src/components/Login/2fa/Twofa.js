import React, { useState } from "react";
import OtpInput from 'react-otp-input';
// import  {OTPInput} from "otp-input-react";
import { useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
const Twofa = ({email,password,setLoading,customerror}) => {
  const [otp, setOTP] = useState("");
  const history = useHistory();
  const handleChange = otp => setOTP(otp);
  const handle2Fa = e => {
    setLoading(true);
    e.preventDefault();
    // this.setState({
    //   code: localStorage.getItem("code")
    // });
    // const { totp_code } = this.state;
    // let code = "";
    // for (const n of totp_code) {
    //   code = code + n.toString();
    // }
    // console.log(code, "TOTP");
    var login_email = email.toLowerCase();
  
    axios.post("https://gxauth.apimachine.com/gx/user/login", {
      email: login_email,
      password: password,
      totp_code: otp
    })
    .then(async res => {
  
      if(res.data.status)
      {
        document.documentElement.setAttribute("data-theme", "light");
        localStorage.setItem("userEmail", res.data.user.email);
        localStorage.setItem("userEmailPermanent", res.data.user.email);
        localStorage.setItem("deviceKey", res.data.device_key);
        localStorage.setItem("accessToken", res.data.accessToken);
        localStorage.setItem("idToken", res.data.idToken);
        localStorage.setItem("refreshToken", res.data.refreshToken);
        localStorage.setItem("appCode", "WealthTrends");
        localStorage.setItem("appName", "WealthTrends");
           setLoading(false);
         history.replace("/dashboard");
      }
      else
      { 
        setLoading(false);

      }
         
  })
  
  }
console.log("asdada",email,otp)
  return (
    <>
    <div style={{height:"100%"}}>


     <div className="optUnregisterConformation">
<p className="optparagrah">{customerror}</p>

    <OtpInput
  value={otp}
  onChange={handleChange}
   numInputs={6}
   otpType="number"
   disabled={false}
        />
          
     </div>
<label className="buttonunregister" onClick={handle2Fa}>Submit</label> 
</div>
    </>
  );
};

export default Twofa;
