import React from 'react'
import LoadingAnimation from '../../../lotties/LoadingAnimation'

export default function LoadingStep({ type, message, loop }) {
    return (
        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
            <LoadingAnimation type={type} size={{ height: 250, width: 250 }} loop={loop} />
            {/* <h6>{message}</h6> */}
        </div>
    )
}
