import React from 'react'
import LoginCards from '../LoginCards/LoginCards'

export default function LoginCardWrapper({setMobileLogin,mobileLogin }) {
    return (
        <>
              <div className="login-right-text">
                <h2 className="d-none d-md-block font-weight-normal">
                Go And Conquer The World
                </h2>
                <h1 className="d-none d-md-block">It’s Yours For The Taking</h1>
                <h1 className="d-md-none">
                  Grow Your Crypto Brand By Offering Your Clients
                </h1>
              </div>
              <LoginCards />

              <div className="d-md-none login-right-text-below">
                <h4>Fiat Currency E-Wallets</h4>
                <p>
                  Enable Your Clients To Deposit, Transact, & Send Over 13 Of
                  The Worlds Most Popular Currencies
                </p>
              </div>
              <button
                onClick={() => setMobileLogin(!mobileLogin)}
                className="login-button d-flex d-md-none"
              >
                Login
              </button>
            </>
    )
}
