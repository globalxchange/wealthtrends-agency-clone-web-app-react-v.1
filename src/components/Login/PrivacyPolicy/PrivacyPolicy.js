import React, { useEffect, useState } from 'react'
import './privacy-policy.scss';
import Interweave from 'interweave'
export default function PrivacyPolicy({ content, privacyPolicy }) {
    const [displayContent, setDisplayContent] = useState({ title: '' })
    useEffect(() => {
        if (privacyPolicy.type === "privacy") {
            console.log("first", content[0])
            setDisplayContent(content[0].formData)
        } else {
            console.log("first", content[1])
            setDisplayContent(content[1].formData)
        }
    }, [privacyPolicy])
    return (
        <div className="privacy-policy-main">
            <h1>{displayContent?.title}</h1>
            <div className="content-wrapper">
                <Interweave content={displayContent.text} />
            </div>

        </div>
    )
}
