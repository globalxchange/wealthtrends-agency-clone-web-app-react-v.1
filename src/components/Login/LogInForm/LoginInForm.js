import React from 'react'
import InputBox from '../InputBox/InputBox'

export default function LoginInForm({ setPrivacyPolicy,handleLoginStep, handleChange, handlefinalclick,setChangePassword, handleClick, credentials, error }) {
  return (

    <React.Fragment>
        <InputBox
          placeholder="Email"
          onChange={handleChange}
          value={credentials.email}
          type="text"
          name="email"
          setChangePassword={() => console.log()}
        />
        <InputBox
          setChangePassword={(val) => setChangePassword(val)}
          placeholder="Password"
          onChange={handleChange}
          value={credentials.password}
          type="password"
          name="password"
          showExtra={false}
          extra ="Forgot Password?"
        />
      <button
        className="login-button"
        onClick={() =>
          handleClick()
        }
      >
        <span className={error !== null ? "" : "d-none"}>
          Incorrect Username or Password. Try Again
          </span>
          Login
        </button>
        {/* <div className="below-form-text">
          <p>Forgot Your Password? <span onClick={()=>handleLoginStep()}>Click Here</span></p>

          {
            localStorage.getItem("appCode")==="VSA"?
<p>Click Here To See Our <span onClick={()=>{setPrivacyPolicy({enable:"true", type:"privacy"})}}>Privacy Policy</span> & <span onClick={()=>{setPrivacyPolicy({enable:"true", type:"terms"})}}>Terms</span></p>
            :
            null
          }
          

        </div> */}
    </React.Fragment>
  )
}
