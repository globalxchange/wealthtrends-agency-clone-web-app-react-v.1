import React, { useState } from "react";
import "./confirm-password.style.scss";
import InputBox from "../InputBox/InputBox";
export default function ConfirmPassWord({
  loading,
  newPassword,
  setNewPassword,
  handleChangePassword,
}) {
  const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
  const [passwords, setPasswords] = React.useState({ first: "", second: "" });
  const [valid, setValid] = useState(false);
  const [match, setMatch] = useState(false);

  const handlePassword = (e) => {
    if (e.target.name === "first") {
      setNewPassword(e.target.value);
      let check = passwordRegex.test(e.target.value);
      setValid(check);
      if (check) {
        setPasswords({ ...passwords, first: e.target.value });
      }
    } else {
      if (e.target.value !== passwords.first) setMatch(false);
      else setMatch(true);
      setPasswords({ ...passwords, second: e.target.value });
    }
  };
  return (
    <React.Fragment>
      <div className="confirm-password">
        {/* <h6>Update Your Password</h6> */}
        <div className="w-100">
          <InputBox
            placeholder="Enter A New Password"
            onChange={handlePassword}
            type="password"
            name="first"
            value={newPassword}
            style={valid ? { color: "#4caf50" } : { color: "orange" }}
            showExtra={true}
            extra={valid ? "Strong Password" : "Not Strong Yet"}
          />
        </div>
        <div className="w-100 mt-5">
          <InputBox
            // disabled={true}
            placeholder="Confirm New Password"
            onChange={handlePassword}
            value={passwords.second}
            type="password"
            name="second"
            extra={match ? "Match" : "Do not match with your password"}
            style={
              match ? { color: "#4caf50" } : { color: "red", fontWeight: 400 }
            }
            showExtra={passwords.second !== ""}
          />
        </div>
      </div>
      <button
        className="confirm-button"
        disabled={!match}
        onClick={() => handleChangePassword(passwords.first)}
      >
        {loading ? "Confirming..." : "Complete"}
      </button>
    </React.Fragment>
  );
}
