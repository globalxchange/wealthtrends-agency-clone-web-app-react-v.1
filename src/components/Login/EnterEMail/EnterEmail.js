import React, { useEffect } from 'react'
import './enter-email.style.scss'
import InputBox from '../InputBox/InputBox'
export default function EnterEmail({ handleEmail,value, message, setChangePassword, error, loading, submitMail }) {
    return (
        <div className="enter-email">
            {/* <h6>Enter Your Registered Email</h6> */}
            <InputBox
                placeholder="Enter Your Email"
                onChange={handleEmail}
                value={value}
                type="text"
                name="email"
                showExtra={error !== null ? false : true}
                style={{ color: 'red' }}
                extra={message}
            />
            <div className="d-flex justify-content-between">
                {/* <button onClick={() => { setChangePassword(0) }}>Back</button> */}
                <button onClick={submitMail}>{loading ? "Loading..." : "Next"}</button>
            </div>
            
            <p onClick={() => { setChangePassword(0) }} className="go-back" >I Remembered My Password, <span>Go Back</span></p>

        </div>
    )
}
