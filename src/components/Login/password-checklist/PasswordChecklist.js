import React from 'react'
import './password-checklist.style.scss'
import Constant from '../../../json/constant'
export default function PasswordChecklist({newPassword}) {
    const [state, setState] = React.useState({number: false, cap: false, special: false, length: false, all: false})
    const oneNumberRegex = /.*[0-9].*/gm
    const oneCapRegex = /.*[A-Z].*/gm
    const oneSpecialRegex = /.*[@#!$%^&+=].*/gm

    const checkPassword= () =>{
        let tempObj = {};
        let a = oneNumberRegex.test(newPassword);
        let b = oneCapRegex.test(newPassword);
        let c = oneSpecialRegex.test(newPassword);
        let d = newPassword.length >= 8;
        let e = a && b && c && d
        setState({number: a, cap: b, special: c, length: d, all: e})
        console.log("password",state)
    }

    React.useEffect(()=>{
        checkPassword()

    },[newPassword])
    
    return (
        <div className="password-checklist">
            <h1>{Constant.passwordChecklist.title}</h1>
            {
            
            Constant.passwordChecklist.list.map(obj =><h5>{obj.text} <p className={state[obj.type]?"valid": "invalid"} /></h5>)
            }
            
        </div>
    )
}
