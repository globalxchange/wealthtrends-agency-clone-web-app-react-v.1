import React,{useState} from 'react'
import './mobile-login.style.scss'
import Images from '../../../assets/a-exporter'


export default function MobileLogin({handleChange, submit }) {

    return (
        <div className="mobile-login w-100 h-100">
            <div className="w-100 logo-wrapper">
                <img src={Images.agencyDark}/>
            </div>
            <div className="w-100">
            <form className="w-100 mb-5 mt-3 mobile-input-box">
                <input onChange={handleChange} name= "email" type ="text" required/>
                <p>Email</p>
            </form>
            <div className="w-100 mb-5 mt-3 mobile-input-box">
                <input onChange={handleChange} name="password" type ="password" required/>
                <p>Password</p>
            </div>
            </div>
            <button onClick={()=>submit()}>
                Enter

            </button>

          

            
        </div>
    )
}
