import React from 'react'
import Images from '../../../assets/a-exporter';
import './input-box.style.scss';

export default function InputBox({ placeholder, setChangePassword,value='', onChange, showExtra, style, name, extra, type = "text" }) {
    const [passwordType, setPasswordType] = React.useState(false);
    return (
        <div className="w-100 input-box-wrapper">
            <div className="input-box">
                <input value={value} name={name} onChange={onChange} type={passwordType ? "text" : type} required />
                <p>{placeholder}</p>
                <img onClick={() => setPasswordType(!passwordType)} className={type === "password" ? "" : "d-none"} height="20px" width="30px" src={passwordType ? Images.doNotShowPassword : Images.showPassword} />
            </div>
            <span style={style} onClick={() => setChangePassword(1)} className={showExtra ? "forgot-password" : "d-none"}>{extra}</span>
        </div>
    )
}
