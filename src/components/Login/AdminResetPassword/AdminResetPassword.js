import React, { useState } from 'react'
import InputBox from '../InputBox/InputBox'
import OtpInput from 'react-otp-input';
import { useHistory } from "react-router-dom";
import axios from "axios";
export default function AdminResetPassword({ customerror, setChangePassword, email, setLoading }) {
    const history = useHistory();

    const [FnewPassword, setFnewPassword] = useState("")
    const [CnewPassword, setCnewPassword] = useState("")
    const [CurrnetPassword, setCurrnetPassword] = useState("")
    const [shownwpadd, setshownwpadd] = useState(false)
    const [rules, setrules] = useState(false)

    const [valid, setValid] = useState(false);
    const [otp, setOTP] = useState("");
    const valueTest = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/
    const enabled = valueTest.test(setFnewPassword)
    const handleChange = async (otp) => await setOTP(otp);
    const [value, setValue] = React.useState('');
    const handleChange1 = (e) => {
        console.log(e)

        setValue(e)
    }
    const changeHandler = e => {

        setFnewPassword(e.target.value)
        let check = valueTest.test(e.target.value)
        setValid(check)
    };
    const changeHandler1 = e => {

        setCnewPassword(e.target.value)
    };
    const changeHandlercurrent = e => {

        setCurrnetPassword(e.target.value)
    };
    console.log("adsdasdasd,", FnewPassword, CnewPassword)


    const EmailVerficationforget = e => {
        e.preventDefault();
        if (otp.length === 6) {
            setshownwpadd(true)
        }
        else {
            alert("try agian")
        }
    }

    const ConformeRestPassword = async () => {

        setLoading(true)


        var login_email = email.toLowerCase();
        if (FnewPassword !== "" && CnewPassword !== "" && FnewPassword === CnewPassword) {


            axios.post("https://gxauth.apimachine.com/gx/user/password/forgot/confirm", {
                email: login_email,
                code: value,
                newPassword: FnewPassword,
            })
                .then(async res => {
                    console.log("sasd", res)
                    if (res.data.status) {



                        setChangePassword(0)
                        setLoading(false);

                    }
                    else {
                        setLoading(false);
                        alert("some thing went Worng")

                    }
                })
        }
        else {
            alert("some thing went Worng")
            setLoading(false);
        }


    }

    return (

        <React.Fragment>


            <> <div style={{ height: "100%", paddingTop: "0rem" }}>
                <p className="optparagrah">It Seems That You Need To Reset Your Password. Please Do That Now</p>
                <div className="otp-wrapper w-100">
                    <h6 className="otp-title">Enter Code From Email</h6>
                    <div className="row-wrapper">
                        <OtpInput value={value} onChange={handleChange1} numInputs={6} />
                    </div>

                </div>

                <input
                    type="password"
                    placeholder="New Password"
                    className="forcelyinputbox"
                    name="FnewPassword"
  style={{width:"100%",fontSize:"15px"}}
                    onChange={changeHandler}
                    required
                />
                <p className="textrules" onClick={() => setrules(!rules)}>Password Rules</p>


                <input
                    type="password"
                    placeholder="Re-Type Your Password"
                    className="forcelyinputbox"
                    name="CnewPassword"
  style={{width:"100%",fontSize:"15px"}}
                    onChange={changeHandler1}
                    required
                />

                {
                    rules ?
                        <div className="passwordrules" style={{ "align": "center", MarginTop: "2rem" }}>
                            <li >Atleast 8 Characters</li>
                            <li >One Upper Case letter</li>
                            <li >One Lower Case letter</li>
                            <li >One Number</li>
                            <li >One Special Character</li>
                        </div>

                        : null
                }




                {
                    valid && FnewPassword !== "" && CnewPassword !== "" && FnewPassword === CnewPassword && value !== "" ?
                        <label disabled={!valid} className="skjad" onClick={ConformeRestPassword}>Submit</label>
                        : null
                }


            </div>
            </>








            {/* 

        <InputBox
          setChangePassword={(val) => setChangePassword(val)}
          placeholder="Password"
          onChange={handleChange}
          type="password"
          name="password"
          showExtra={false}
          extra ="Forgot Password?"
        />
      <button
        className="login-button"
        onClick={() =>
          handleClick(credentials.email, credentials.password)
        }
      >
        <span className={error ? "" : "d-none"}>
          Incorrect Username or Password. Try Again
          </span>
          Login
        </button>
        <div className="below-form-text">
          <p>Forgot Your Password? <span onClick={()=>handleLoginStep()}>Click Here</span></p>
          <p>Click Here To See Our <span onClick={()=>{setPrivacyPolicy({enable:"true", type:"privacy"})}}>Privacy Policy</span> & <span onClick={()=>{setPrivacyPolicy({enable:"true", type:"terms"})}}>Terms</span></p> */}


        </React.Fragment>
    )
}
