import React from 'react'
import Images from '../../../assets/a-exporter';
import { getUserDetailsByEmail, getUserDetailsByUsername } from '../../../services/getAPIs';
import LoadingAnimation from '../../../lotties/LoadingAnimation';
import NextSVG from '../../common-components/assetsSection/NextSVG';

export default function BBSTepOne({ appInfo, setBrokerInfo, setStep, setByBroker, byEmail }) {
    const [entity, setEntity] = React.useState('');
    const [loading, setLoading] = React.useState(false)

    const getClipboardText = () => {
        navigator.clipboard.readText()
            .then(
                text => setEntity(text)
            )
    }

    const handleSubmit = async () => {
        setLoading(true)
        if (byEmail) {
            let res = await getUserDetailsByEmail(entity);
            if (res.data.status) {
                setBrokerInfo(res.data.user);
                setStep(true);
            } else {
                setBrokerInfo(null);
            }
        } else {
            let res = await getUserDetailsByUsername(entity);
            if (res.data.status) {
                setBrokerInfo(res.data.user);
                setStep(true);
            } else {
                setBrokerInfo(null);
            }
        }
        setLoading(false)
    }

    return (
        loading ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />

            </div>
            :
            <div className="by-broker-step-one">
                <img src={!appInfo?.app_icon ? Images.agencyDark : appInfo.app_icon} />
                <p>{
                    byEmail ?
                        "Please Enter Your Brokers Email To Initiate Registration"
                        :
                        "Please Enter Your BrokerSync™ Code To Initiate Registration"
                }</p>
                <div className="bb-one-input">
                    <input onKeyPress={(e) => e.which === 13 && entity ? handleSubmit() : console.log()} onChange={e => setEntity(e.target.value)} value={entity} placeholder={byEmail ? "Ex. broker@gmail.com" : "Ex. broker123"} />
                    <button onClick={() => getClipboardText()}><NextSVG type="paste" /> </button>
                </div>
                <div className="bb-one-buttons">
                    <button onClick={() => setByBroker(false)}>Go Back</button>
                    <button onClick={() => handleSubmit()} disabled={!entity}>{loading ? "Finding..." : "Proceed"}</button>
                </div>
            </div>
    )
}
