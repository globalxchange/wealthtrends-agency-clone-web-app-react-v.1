import React from 'react'
import BBSTepOne from './BBSTepOne'
import BBSTepTwo from './BBSTepTwo';
import './by-broker.style.scss'
export default function ByBroker({ appInfo, setBrokerInfoMain, setByBroker }) {
    const [byEmail, setByEmail] = React.useState(false);
    const [brokerInfo, setBrokerInfo] = React.useState(null);
    const [step, setStep] = React.useState(false)
    React.useEffect(() => {
        return () => {
            setByBroker(false);
        }
    }, [])
    return (
        <div className="col-md-8 col-12 by-broker-main">
            {
                step ? <BBSTepTwo setBrokerInfoMain={setBrokerInfoMain} appInfo={appInfo} brokerInfo={brokerInfo} setStep={setStep} /> : <BBSTepOne setStep={setStep} setBrokerInfo={setBrokerInfo} setByBroker={setByBroker} byEmail={byEmail} appInfo={appInfo} />
            }

            <p onClick={() => setByEmail(!byEmail)} className={step ? "d-none" : "bb-free-floating-text"}>
                {byEmail ? "Use BrokerSync™ Instead" : "User Broker’s Email Address Instead"}
            </p>

        </div>
    )
}
