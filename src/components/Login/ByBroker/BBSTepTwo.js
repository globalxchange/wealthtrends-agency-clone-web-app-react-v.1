import React from 'react'
import Images from '../../../assets/a-exporter';
import { useHistory } from 'react-router-dom'

export default function BBSTepTwo({ brokerInfo, setBrokerInfoMain, appInfo, setStep }) {
    const history = useHistory();
    const handleClick = () => {
        setBrokerInfoMain(brokerInfo);
        history.push(`/register`)
    }
    return (
        <div className="by-broker-step-two">
            <img src={!appInfo?.app_icon ? Images.agencyDark : appInfo.app_icon} />
            <p>Is This Your Broker?</p>
            <div className="broker-info">
                <div className="broker-info-upper">
                    <img src={!brokerInfo.profile_img ? Images.face : brokerInfo.profile_img} />
                    <h6>{brokerInfo?.first_name} {brokerInfo.last_name}</h6>
                    <span>{brokerInfo.email}</span>

                </div>
                <div className="broker-info-lower">
                    <h4>Bio</h4>
                    <p>{brokerInfo.bio}</p>

                </div>

            </div>
            <div className="bb-two-buttons">
                <button onClick={() => setStep(false)}>No It Is Not</button>
                <button onClick={() => handleClick()}>Yes It Is  </button>
            </div>


        </div>
    )
}
