import React, { useState } from "react";
import "./login-cards.scss";
import Constant from "../../../json/constant";
import VideoPlayer from "../../common-components/VideoPlayer/VideoPlayer";
import { useEffect } from "react";
import { getCarouselData } from "../../../services/getAPIs";
import Interweave from 'interweave'
export default function LoginCards() {
  const [selectedId, setSelectedId] = useState("id2");
  const [carouselData, setCarouselData] = useState([{title:''}]);
  const [videoData, setVideoData] = useState(Constant.loginCards);
  const [currentNum, setCurrentNum] = useState(1)

  useEffect(()=>{
    
    getCarouselData()
      .then(res =>{
        setCarouselData(res.data.data);
        let temp = res.data.data;
        let tempArray = [];

        Constant.loginCards.forEach((obj, i)=>{
          tempArray = [...tempArray, {...obj,...temp[i].formData }]
        })

        console.log("Full Array", tempArray)
        setVideoData([...tempArray])

        
      })
  },[])

  return (
    <>
      <div className="login-right w-100 d-flex">
        {/* <h6 className="w-100 above-text d-none d-md-flex"><span className="d-none d-md-block mr-1">You Can Now </span>Launch Your Own</h6> */}
        <div className="login-cards-wrapper d-flex align-items-center">
          {Constant.loginCards.map((obj) => (
            <input
              className="d-none"
              key={obj.keyId}
              onChange={() => console.log()}
              type="radio"
              checked={selectedId === obj.id}
              id={obj.id}
            />
          ))}
          <div className="cards-wrapper position-relative">
            {videoData.map((obj, i) => (
              <label
                key={obj.cardId}
                className="position-absolute d-flex justify-content-center align-items-center"
                onClick={() => {setCurrentNum(i); setSelectedId(obj.id)}}
                htmlFor={obj.cardId}
                id={obj.cardId}
              >
                <VideoPlayer videoLink={obj.videolink} image ={obj.videothumbnail} currentId ={obj.id} selectedId={selectedId}/>
              </label>
            ))}
          </div>
        </div>
        <div className="w-100 below-text d-flex">
            <h5>{carouselData?.[currentNum]?.formData?.title}</h5>
            <Interweave content= {carouselData?.[currentNum]?.formData?.description} />
          {/* <p>Give Your Users Access To The Global Markets By Enabling Both Crypto & Fiat Support For Your App Today</p> */}
            <button onClick={()=>window.open('https://'+carouselData?.[currentNum]?.formData?.buttonlink, '_blank')}>{carouselData?.[currentNum]?.formData?.buttontext}</button>
        </div>
      </div>
    </>
  );
}
