import React, { useState, useEffect } from "react";
import "./login.style.scss";
import {
  getColorCodeByApp,
  getUserDetailsByUsername,
  getUserDetailsByEmail,
} from "../../services/getAPIs";
import axios from "axios";
import { useHistory } from "react-router-dom";
import InputBox from "./InputBox/InputBox";
import LoginCards from "./LoginCards/LoginCards";
import Constant from "../../json/constant";
import PreRegister from "../PreRegister/PreRegister";
import {
  login,
  authenticate,
  requestForgotPassword,
  passwordReset,
} from "../../services/postAPIs";
import LoadingAnimation from "../../lotties/LoadingAnimation";
import MobileLogin from "./MobileLogin/MobileLogin";
import { isMobile } from "react-device-detect";
import PrivacyPolicy from "./PrivacyPolicy/PrivacyPolicy";
import Images from "../../assets/a-exporter";
import { getTCContent } from "../../services/getAPIs";
import LoginCardWrapper from "./LoginCardWrapper/LoginCardWrapper";
import LoginInForm from "./LogInForm/LoginInForm";
import EnterEmail from "./EnterEMail/EnterEmail";
import OTPWrapper from "./OTPWrapper/OTPWrapper";
import ConfirmPassWord from "./ConfirmPassword/ConfirmPassWord";
import ConformOpt from "./ConformOpt/ConformOpt";
import ByBroker from "./ByBroker/ByBroker";
import TwoFa from "./2fa/Twofa";
import Reset from "./Forcelyrest/Forcelyrest";
import LoadingStep from "./loading-step/LoadingStep";
import PasswordChecklist from "./password-checklist/PasswordChecklist";
import Wt from "../../assets/wt.png";
import PortalModal from "../common-components/PortalModal/PortalModal";
import AdminResetPassword from "./AdminResetPassword/AdminResetPassword";
export default function Login({ setAppInfo, appInfo, setBrokerInfoMain }) {
  const [credentials, setCredentials] = useState({ email: "", password: "" });
  const [customerror, setcustomerror] = useState("");
  const [error, setError] = useState(null);
  const [preRegister, setPreRegister] = useState(false);
  const [loading, setLoading] = useState(false);
  const [mobileLogin, setMobileLogin] = useState(false);
  const [changePassword, setChangePassword] = useState(0);
  const [newPassword, setNewPassword] = useState("");
  const [changed, setChanged] = useState(null);
  const [content, setContent] = useState([]);
  const [lastStepLoad, setLastStepLoad] = useState(false);
  const [byBroker, setByBroker] = React.useState(false);
  const [otp, setOTP] = useState("");
  const [emailLoad, setEmailLoad] = useState(false);
  const [innerHeight, setInnerHeight] = React.useState(0);
  const [backgroudimage, setbackgroudimage] = useState("");
  const [appicon, setappicon] = useState("");
  const [email, setEmail] = useState({
    email: "",
    error: false,
    message: "Looking For Email",
    loading: false,
  });
  const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const [privacyPolicy, setPrivacyPolicy] = useState({
    enable: false,
    type: "",
  });
  const loginMain = React.useRef();

  const setUpColorCode = async () => {
    let res = await getColorCodeByApp("WealthTrends");
    if (!res.data.Apps[0].colors) {
      document.documentElement.style.setProperty("--main-color", "#88C920");
      localStorage.setItem("mainColor", "#88C920");
      document.documentElement.style.setProperty("--text-color", "#182542");
      localStorage.setItem("textColor", "#182542");

      document.documentElement.style.setProperty(
        "--background-color",
        "#ffffff"
      );
      localStorage.setItem("bgColor", "#ffffff");
      document.documentElement.style.setProperty("--border-color", "#e5e5e5");
      localStorage.setItem("borderColor", "#e5e5e5");
    } else {
      let { colors } = res.data.Apps[0];
      document.documentElement.style.setProperty(
        "--main-color",
        colors.mainColor
      );
      localStorage.setItem("mainColor", colors.mainColor);
      document.documentElement.style.setProperty(
        "--text-color",
        colors.textColor
      );
      localStorage.setItem("textColor", colors.textColor);
      document.documentElement.style.setProperty(
        "--background-color",
        colors.backgroundColor
      );
      localStorage.setItem("bgColor", colors.backgroundColor);
      document.documentElement.style.setProperty(
        "--border-color",
        colors.borderColor
      );
      localStorage.setItem("borderColor", colors.borderColor);
    }
  };

  useEffect(() => {
    setUpColorCode();
  }, []);

  const fetchBalances = async (coin) => {
    let data = await axios.get(
      "https://comms.globalxchange.com/gxb/apps/get?app_code=WealthTrends"
    );
    if (data.data.status) {
      setAppInfo(data.data.apps[0]);
      console.log("data.data.apps", data.data.apps);
      setbackgroudimage(data.data.apps[0].cover_photo);
      setappicon(data.data.apps[0].app_icon);
      let resT = await getUserDetailsByEmail(data.data.apps[0].created_by);
      console.log("response two", resT.data);
      setBrokerInfoMain(resT.data.user);
    }
  };

  useEffect(() => {
    fetchBalances();
  }, []);

  const loginStep = () => {
    switch (changePassword) {
      case 0:
        return (
          <LoginInForm
            handleLoginStep={() => setChangePassword(1)}
            setPrivacyPolicy={setPrivacyPolicy}
            setChangePassword={setChangePassword}
            isMobile={isMobile}
            handleChange={handleChange}
            credentials={credentials}
            error={error}
            handleClick={handleClick}
          />
        );
      case 1:
        return (
          <EnterEmail
            setChangePassword={setChangePassword}
            error={email.error}
            message={email.message}
            email={email}
            value={email.email}
            handleEmail={handleEmail}
            submitMail={submitMail}
            loading={emailLoad}
          />
        );
      case 2:
        return (
          <OTPWrapper
            email={email.email}
            setChangePassword={setChangePassword}
            setOTP={setOTP}
          />
        );
      case 3:
        return (
          <ConfirmPassWord
          newPassword={newPassword}
            setNewPassword={setNewPassword}
            handleChangePassword={handleChangePassword}
            setChangePassword={setChangePassword}
            loading={lastStepLoad}
          />
        );
      case 4:
        return (
          <ConformOpt
            customerror={customerror}
            email={credentials.email}
            setLoading={setLoading}
            setChangePassword={setChangePassword}
            setcustomerror={setcustomerror}
            customerror={customerror}
          />
        );

      case 5:
        return (
          <TwoFa
            password={credentials.password}
            email={credentials.email}
            setLoading={setLoading}
            setcustomerror={setcustomerror}
            customerror={customerror}
          />
        );

      case 6:
        return (
          <Reset
            email={credentials.email}
            customerror={customerror}
            setLoading={setLoading}
            setChangePassword={setChangePassword}
          />
        );
      case 7:
        return (
          <LoadingStep
            type={changed === null ? "login" : changed ? "done" : "failed"}
            message={
              changed === null
                ? emailLoad
                  ? "Sending You OTP"
                  : "Updating Your Password"
                : changed
                ? "Successfully Changed"
                : "Please.... Try Again"
            }
          />
        );
      case 9:
        return (
          <AdminResetPassword
            password={credentials.password}
            email={credentials.email}
            setLoading={setLoading}
            setcustomerror={setcustomerror}
            setChangePassword={setChangePassword}
          />
        );
    }
  };
  const handleEmail = (e) => {
    setEmail({ ...email, email: e.target.value });
  };
  const submitMail = async () => {
    setEmailLoad(true);
    setChangePassword(7);
    let check = emailRegex.test(String(email.email).toLowerCase());
    setEmail({ ...email, error: !check });
    if (check) {
      let res = await requestForgotPassword(email.email);
      if (res.data.status) {
        setChangePassword(2);
      } else {
        setChangePassword(1);
        setEmail({ ...email, error: true, message: res.data.message });
      }
      setEmailLoad(false);
    }
  };

  const handleChangePassword = async (password) => {
    setChangePassword(7);
    let obj = {
      email: email.email,
      code: otp,
      newPassword: password,
    };
    let res = await passwordReset(obj);
    if (res.data.status) setChanged(true);
    else setChanged(false);

    let a = setTimeout(() => {
      setChangePassword(0);
      setChanged(null);
      clearTimeout(a);
    }, 2000);
  };
  const history = useHistory();

  const handleChange = (e) => {
    error !== null ? setError(null) : console.log();
    setCredentials({ ...credentials, [e.target.name]: e.target.value });
  };
  const handleClick = () => {
    setLoading(true);
    login(credentials.email, credentials.password).then((res) => {
      if (res.data.status) {
        if (res.data.status === true) {
          if (res.data.mfa === true) {
            setcustomerror(
              "It Seems That You Need To Verify Your 2FA. Please Do That Now"
            );
            setChangePassword(5);
            setError(null);
            setLoading(false);
            return;
          }
        }
        if (res.data.passwordResetRequired === true) {
          setChangePassword(9);
          setLoading(false);
          setError(null);
          setLoading(false);
          setcustomerror(
            "It Seems That You Need To Reset Your Password. Please Do That Now"
          );
        } else {
          document.documentElement.setAttribute("data-theme", "light");
          localStorage.setItem("userEmail", credentials.email);
          localStorage.setItem("userEmailPermanent", credentials.email);
          localStorage.setItem("deviceKey", res.data.device_key);
          localStorage.setItem("accessToken", res.data.accessToken);
          localStorage.setItem("idToken", res.data.idToken);
          localStorage.setItem("refreshToken", res.data.refreshToken);
          localStorage.setItem("appId", Constant.appId["WealthTrends"]);

          setLoading(false);
          history.replace("/dashboard");
        }
      } else {
        setError(true);
        setLoading(false);
      }
      if (res.data.status === false) {
        if (res.data.resend_code === true) {
          setcustomerror(
            "It Seems That You Have Not Finished Your Email Registration From When You First Signed Up. Please Confirm Your Email To Continue Logging In"
          );
          setChangePassword(4);
          setError(null);
          setLoading(false);
        }
      }
      if (res.data.status === false) {
        if (res.data.mfa === true) {
          setcustomerror(
            "It Seems That You Need To Verify Your 2FA. Please Do That Now"
          );
          setChangePassword(5);
          setError(null);
          setLoading(false);
        }
      }

      if (res.data.status === false) {
        if (res.data.resetPassword === true) {
          setcustomerror(
            "It Seems That You Need To Reset Your Password. Please Do That Now"
          );
          setChangePassword(6);
          setLoading(false);
          setError(false);
        }
      }
    });
  };
  useEffect(() => {
    setInnerHeight(window.innerHeight);
    getTCContent().then((res) => {
      setContent(res.data.data);
    });

    // authenticate().then((res) => {
    //   if (res.data.status) {
    //     history.replace("/dashboard");
    //   }
    // });
  }, [privacyPolicy]);
  return preRegister ? (
    <PreRegister
      handlefinalclick={handleClick}
      setCredentials={setCredentials}
      setPreRegister={setPreRegister}
      appInfo={appInfo}
    />
  ) : (
    <div
      ref={loginMain}
      style={isMobile ? { height: `${innerHeight}px` } : {}}
      className="login-main"
    >
      <PortalModal
        clickFunction={() => setError(null)}
        status={error}
        message={error !== null ? "Failed To Login. Please Try Again" : ""}
      />
      <div
        className={
          loading
            ? "login-overlay d-flex justify-content-center align-items-center"
            : "d-none"
        }
      >
        {loading ? (
          <LoadingAnimation
            type="login"
            size={{ height: isMobile ? 200 : 400, width: isMobile ? 200 : 400 }}
          />
        ) : (
          ""
        )}
      </div>
      <div className="row w-100 h-100">
        <div
          className={`col-md-4 col-12 ${
            mobileLogin ? "d-flex" : ""
          } position-relative d-md-flex login-left  align-items-center`}
        >
          <div
            onClick={() => setChangePassword(1)}
            className={changePassword !== 0 ? "d-none" : "bottom-section"}
          >
            I Can’t Seem To Access My Account
          </div>
          <button
            onClick={() => setPrivacyPolicy({ enable: false, type: "" })}
            className={privacyPolicy.enable ? "close-button-login" : "d-none"}
          >
            <img src={Images.rightArrow} />
          </button>
          <form
            onSubmit={(e) => {
              e.preventDefault();
              handleClick();
            }}
            style={
              changePassword !== 0 ? {} : { alignContent: "space-between" }
            }
            className="login-form"
          >
            <div className="login-form-title">
              <h1>Login</h1>
              <p>If You Are Already A Wealth Trends Member</p>
            </div>
            {loginStep()}
          </form>
        </div>
        {byBroker ? (
          <ByBroker
            setBrokerInfoMain={setBrokerInfoMain}
            setByBroker={setByBroker}
            appInfo={appInfo}
          />
        ) : (
          <div className="col-md-8 col-12 login-right-main-new">
            <div className="login-right-main-new-wrapper">
              <img src={appInfo?.app_icon} />
              <p>
                If This Is Your First Time Here. You Can Register For Free.<br/> Select Which Process Applies To You{" "}
              </p>
              <div>
                <button onClick={() => setPreRegister(true)}>
                  I Was Pre-Registered
                </button>
                <button onClick={() => history.push(`/register`)}>
                  I Got Here By Myself
                </button>
                <button onClick={() => setByBroker(true)}>
                  I was Referred By A Broker
                </button>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
