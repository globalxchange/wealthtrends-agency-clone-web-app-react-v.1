import React from 'react'
import { fetchInvestmentPaths } from '../../../../services/getAPIs'
import './investment-paths.style.scss'
export default function InvestmentPaths() {
    const [list, setList] = React.useState([])

    const setUpList = () => {
        let res
        try {
            res = fetchInvestmentPaths(localStorage.getItem("appCode"))

            setList([...res.data.paths])

        } catch (error) {


        }


    }

    React.useEffect(() => [
        setUpList()
    ], [])



    return (
        <div className="investment-paths-main">
            {
                list.map(obj => <div className="investment-path-child">
                    <div>
                        <h3>{obj.investment}</h3>
                    </div>

                </div>
                )
            }

        </div>
    )
}
