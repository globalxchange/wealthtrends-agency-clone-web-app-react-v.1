import React from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../assets/a-exporter'
import LoadingAnimation from '../../../../lotties/LoadingAnimation';
import { Agency } from '../../../Context/Context';
import AgencyOwnership from '../agency-ownership/AgencyOwnership';
import BondOfferings from '../bond-offerings/BondOfferings';
import InvestmentPaths from '../investment-paths/InvestmentPaths';
import SyndicateBondsOfferings from '../syndicate-bonds-offerings/SyndicateBondsOfferings';
import './offering-wrapper.style.scss'
const OfferingWrapper = React.memo(({ searchTerm }) => {
    const agency = React.useContext(Agency);
    const { currentApp } = agency;
    const [selectedOffering, setSelectedOffering] = React.useState('ccs');
    const selectComponent = React.useCallback(() => {
        switch (selectedOffering) {
            case "bonds": return <BondOfferings searchTerm={searchTerm} />;
            case "ccs": return <SyndicateBondsOfferings searchTerm={searchTerm} />;
            default: return <AgencyOwnership />
        }

    }, [selectedOffering])
    return (
        <div className="offering-wrapper-main">
            {/* <div className="offering-wrapper-header">
                {
                    headerList.map((obj, num) =>
                        <span
                            onClick={() => setSelectedOffering(obj.id)}
                            className={obj.id === selectedOffering ? "selected-header normal-header" : "normal-header"}>
                            <img src={num === 0 ? currentApp.app_icon : obj.icon} />
                            <span>{
                            // num === 1 ? `${currentApp.app_name} Syndicates` :
                             obj.short}</span>
                        </span>
                    )
                }

            </div>
            <div className="offering-wrapper-body">
                {
                    selectComponent()
                }

            </div> */}
            <InvestmentPaths searchTerm ={searchTerm} />

        </div>
    )
})
export default OfferingWrapper
const headerList = [
    { keyId: nextId(), name: "", icon: Images.ccsLogo, id: "ccs",short: "Syndicates", display: "CCS Wealth Syndicates" },
    { keyId: nextId(), name: "name", icon: Images.terminalLogo, id: "bonds", short: "Bonds", display: "Bonds Offering" },
    { keyId: nextId(), name: "", icon: Images.agencyDark, id: "agency",short: "Agency", display: "Agency Ownership" }
]