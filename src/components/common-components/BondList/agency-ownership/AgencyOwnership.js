import React from 'react'
import Constant from '../../../../json/constant'
import LoadingAnimation from '../../../../lotties/LoadingAnimation';
import { getAppOverview } from '../../../../services/getAPIs';
import { Agency } from '../../../Context/Context'
import './agency-ownership.style.scss'
export default function AgencyOwnership() {
    const agency = React.useContext(Agency);
    const [loading, setLoading] = React.useState(true)
    const [coin, setCoin] = React.useState("");
    const [classesList, setClassesList] = React.useState([])
    const [details, setDetails] = React.useState({})
    const { currentApp, valueFormatter, setCollapseSidebar,setExternalAction, setUserChatEnable } = agency;
    const setUpClasses = async () => {
        let res = await getAppOverview(currentApp.app_code);
        setDetails(res.data.apps[0])
        setCoin(!res.data.apps[0]?.ownership_coin ? "TBD" : res.data.apps[0]?.ownership_coin);
        setLoading(false)
        let temp = Constant.ownershipClasses.map((obj, num) => {
            if (!num) {
                return {
                    ...obj,
                    amount: res.data.apps[0].ownership_capital,
                    coin: !res.data.apps[0]?.ownership_coin ? "TBD" : res.data.apps[0]?.ownership_coin,

                }
            } else {
                return obj
            }
        })
        setClassesList([...temp]);
        setLoading(false)

    }
    const handleClick = () =>{
        setExternalAction("invest")
        setCollapseSidebar(true)
        setUserChatEnable(true)
    }
    React.useEffect(() => {
        setUpClasses()
    }, [])

    return (
        loading ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                <LoadingAnimation />

            </div>
            :
            <div className="agency-ownership-main">
                {
                    classesList.map(obj =>
                        <div className="ownership-wrapper">
                            <div>
                                <h5>{obj.title}</h5>
                                <span>{obj.subtitle}</span>
                            </div>
                            <div>
                                <h5>{valueFormatter(obj.amount, coin)}</h5>
                                <span>Total Allocation <b>({obj.coin})</b></span>
                            </div>
                            <div>
                                <button onClick={()=>handleClick()} disabled={obj.coin === "TBD"}>
                                    Invest
                                </button>
                            </div>


                        </div>
                    )
                }

            </div>
    )
}
