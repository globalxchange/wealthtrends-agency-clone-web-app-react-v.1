import React from 'react'
import './investment-table.style.scss'
import { Agency } from '../../../Context/Context';
import Constant from '../../../../json/constant';
export default function InvestmentTable() {
    const agency = React.useContext(Agency);
    const { allMarketCoins, setSecondGate, setAssetSelected, setAssetClassSelected, setScrollValue, handleAnalytics, conversionConfig, valueFormatter } = agency;

    const handleClick = (data, type, len, i) => {
        handleAnalytics("");
        setAssetSelected(Constant.navbarList[4]);
        afterSomeTime(data, i);
        setSecondGate(true);
    }
    const afterSomeTime = (data, i) => {
        let a = setTimeout(() => {
            setAssetClassSelected(data);
            if (i > 1) {
                setScrollValue(i)
            }
            clearTimeout(a);

        }, 300);
    }
    return (
        <div className="investment-table">
            {
                allMarketCoins.market.map((data, i) => <div onClick={() => handleClick(data, "crypto", '', i)}>
                    <span><img height="20px" src={data.coinImage} />{data.coinName}</span>
                    <span>{valueFormatter(data.coinValue, data.coinSymbol)}</span>
                    <span>{valueFormatter(!data.withdrawal_balance ? data.coinValue : data.withdrawal_balance, data.coinSymbol)}</span>
                    <span>{conversionConfig.enable ? valueFormatter(data.price.USD * conversionConfig.rate, conversionConfig.coin) + ` ${conversionConfig.coin}` : valueFormatter(data.price.USD, "USD")}</span>
                    <span>{conversionConfig.enable ? valueFormatter(data.coinValueUSD * conversionConfig.rate, conversionConfig.coin) + ` ${conversionConfig.coin}` : valueFormatter(data.coinValueUSD, "USD")}</span>
                </div>)
            }

        </div>
    )
}
