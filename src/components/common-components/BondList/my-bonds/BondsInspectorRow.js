import React from 'react'
import nextId from 'react-id-generator';
import Images from '../../../../assets/a-exporter';
import LoadingAnimation from '../../../../lotties/LoadingAnimation';
import { getIcedContractAsPerStatus, getBondLevelTwoData } from '../../../../services/getAPIs';
import { Agency } from '../../../Context/Context'

export default function BondsInspectorRow({ obj, listInUSD, currentList, setInspector, handleClick }) {
    const agency = React.useContext(Agency);
    const { valueFormatter, cryptoUSD, displayBondDate, setDisplayBondDate } = agency;
    const [status, setStatus] = React.useState("active");
    const [newObj, setNewObj] = React.useState({});
    const [loading, setLoading] = React.useState(false);
    const [levelTwo, setLevelTwo] = React.useState(true);
    const [levelTwoData, setLevelTwoData] = React.useState([]);
    const [dropdown, setDropdown] = React.useState({
        status: false,
        which: null
    });

    const returnSortedDate = (date) => {
        let dArray = date.split('/');
        return `${months[dArray[0]]} ${dArray[1]}${days(dArray[1])} ${dArray[2]}`

    }


    const setUpLevelTwoData = async () => {
        let res = await getBondLevelTwoData(status, obj.coinSymbol);
        if (!res.data?.icedContracts?.[0]?.contracts) {
            setLevelTwoData([])
        } else {
            setLevelTwoData(res.data.icedContracts[0].contracts);
        }
        setLoading(false)
    }

    const callAPI = async () => {
        let res = await getIcedContractAsPerStatus(status, obj.coinSymbol);
        setNewObj(res.data.icedContracts[0]);
        setLoading(false);
    }
    React.useEffect(() => {
        setLoading(true);
        callAPI();
        setUpLevelTwoData();
        setLevelTwoData([])
    }, [status, obj])
    return (
        <React.Fragment>
            <div className={!displayBondDate ? "" : "bir-overlay"} />
            <div onClick={()=>setDropdown({status: false, which: null})} className="bond-inspector-left">
                <h1 onClick={(e) =>{e.stopPropagation(); setInspector(null)}}><img src={obj.coinImage} /><span>{obj.coinName}</span></h1>
                <div onClick={e => e.stopPropagation()} className="dropdown-wrapper">
                    <div onClick={() => dropdown.status && dropdown.which === "coin" ? console.log() : setDropdown({ status: true, which: "coin" })} className="bir-single-dropdown">
                        {obj.coinSymbol}
                        <img src={Images.triangle} />
                        {dropdown.status ?
                            <div className={dropdown.which === "coin" ? "bir-dropdown-list" : "d-none"}>
                                {
                                    currentList.map(obj => <h6 onClick={() => { setInspector(obj); setDropdown({ status: false, which: null }) }}>{obj._id}</h6>)
                                }

                            </div>
                            : ''
                        }

                    </div>
                    <div onClick={() => dropdown.status && dropdown.which === "status" ? console.log() : setDropdown({ status: true, which: "status" })} className="bir-single-dropdown">
                        {status?.substring(0, 1).toUpperCase()}{status?.substring(1)}
                        <img src={Images.triangle} />
                        {dropdown.status ?
                            <div className={dropdown.which === "status" ? "bir-dropdown-list" : "d-none"}>
                                {
                                    header.map(obj => <h6 onClick={() => { setStatus(obj.id); setDropdown({ status: false, which: null }) }}>{obj.name.toUpperCase()}</h6>)
                                }

                            </div>
                            : ''
                        }
                    </div>

                </div>
            </div>
            <div onWheel={(e) => e.stopPropagation()} style={levelTwo ? {} : !levelTwoData.length ? {} : { paddingBottom: '30%' }} className="bond-inspector-right">
                {levelTwo ?
                    <>
                        <div className="bond-inspector-header">
                            {
                                header.map(x =>
                                    <button
                                        className={x.id === status ? "selected-status" : ""}
                                        onClick={() => setStatus(x.id)}
                                    >
                                        {x.name} {x.id === status && !loading ? `(${!newObj?.count ? 0 : newObj?.count})` : ''}
                                    </button>
                                )
                            }

                        </div>
                        <div className="bond-inspector-body">
                            {
                                loading ?
                                    <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                                        <LoadingAnimation type="login" size={{ height: 200, width: 200 }} />
                                    </div>
                                    :
                                    <>

                                        <div className="inspector-data-display">
                                            <div className="inspector-data-display-top">
                                                {
                                                    listData.map(objTwo => <h6>
                                                        <span>{objTwo.fieldName}</span>
                                                        <span>{valueFormatter(newObj?.[objTwo.id] * (listInUSD ? cryptoUSD[obj.coinSymbol] : 1), listInUSD ? "USD" : obj.coinSymbol)} </span>
                                                    </h6>)
                                                }
                                            </div>
                                            <div className="inspector-data-display-bottom">
                                                <h6>
                                                    <span>Current Value Of Bonds</span>
                                                    <span>{valueFormatter(newObj?.currentVoc * (listInUSD ? cryptoUSD[obj.coinSymbol] : 1), listInUSD ? "USD" : obj.coinSymbol)}</span>
                                                </h6>
                                            </div>
                                        </div>
                                        <div className="inspector-data-footer">
                                            <button onClick={() => handleClick(obj)}>Earning Vault</button>
                                            <button onClick={() => { setLevelTwo(false); setUpLevelTwoData() }}>Bonds List</button>
                                        </div>
                                    </>
                            }

                        </div>
                    </>
                    :
                    <>
                        {
                            loading ?
                                <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                                    <LoadingAnimation />
                                </div>
                                :
                                !levelTwoData.length ?
                                    <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                                        <LoadingAnimation type="no-data" />
                                    </div>

                                    :
                                    levelTwoData.map(obj => <div style={displayBondDate === obj._id ? { zIndex: 27 } : {}} className="bond-list-row">
                                        <div className="bond-list-row-upper">
                                            <div onMouseLeave={() => setDisplayBondDate('')} className="bond-list-row-upper-info">
                                                <h4>{obj.coin}</h4>
                                                <span onMouseEnter={() => setDisplayBondDate(obj._id)}>{obj.completed_stats.days}/{obj.remaining_stats.days} days</span>
                                                {
                                                    displayBondDate === obj._id ?
                                                        <div className="hover-info">
                                                            <div className="hover-info-above">
                                                                <div>
                                                                    <span>Issuance Date:</span>
                                                                    <p>{returnSortedDate(obj.date.split(',')[0])}</p>
                                                                </div>
                                                                <div>
                                                                    <span>Issuance Date:</span>
                                                                    <p>{returnSortedDate(obj.redemption_date.split(',')[0])}</p>
                                                                </div>
                                                            </div>
                                                            <div className="hover-info-below">
                                                                Bond Hash
                                                </div>
                                                            <h6></h6>
                                                        </div>
                                                        : ''
                                                }
                                            </div>
                                            <div className="bond-list-row-upper-info">
                                                <h4>{valueFormatter(obj.current_voc)} {obj.coin}</h4>
                                                <span>Current Value Of Bond</span>
                                            </div>

                                        </div>
                                        <p onClick={() => window.open(`https://assets.io/bonds/${obj._id}`, '_blank')}>View Asset Hash</p>

                                    </div>
                                    )
                        }
                    </>
                }

            </div>
        </React.Fragment>
    )
}
const header = [
    { keyId: nextId(), name: "Active", id: "active" },
    { keyId: nextId(), name: "Completed", id: "completed" },
    { keyId: nextId(), name: "Redeemed", id: "redeemed" },
]
const listData = [
    { keyId: nextId(), fieldName: "Total Value Of Bonds", id: "outstandingValue" },
    { keyId: nextId(), fieldName: "Total Invested", id: "investment" },
    { keyId: nextId(), fieldName: "Total Earned Interest", id: "total_interest_earned" },
    { keyId: nextId(), fieldName: "Total Remaining Earnings", id: "remaining_earning_power" },
]
const months = {
    1: 'January',
    2: 'February',
    3: 'March',
    4: 'April',
    5: 'May',
    6: 'June',
    7: 'July',
    8: 'August',
    9: 'September',
    10: 'October',
    11: 'November',
    12: 'December',
}
const days = (day) => {
    switch (day) {
        case 1: return 'st';
        case 2: return 'nd';
        case 3: return 'rd';
        default: return 'th'
    }
}