import React from 'react'
import { getBondBalance, getIcedContracts, getIcedEarningBalance } from '../../../../services/getAPIs';
import { Agency } from '../../../Context/Context';
import nextId from 'react-id-generator'
import './my-bonds.style.scss'
import MyBondRow from './MyBondRow'
import Constant from '../../../../json/constant';
import LoadingAnimation from '../../../../lotties/LoadingAnimation';
import BondsInspectorRow from './BondsInspectorRow';
export default function MyBonds({ searchTerm, active, listInUSD }) {
    const [loading, setLoading] = React.useState(false);
    const [currentList, setCurrentList] = React.useState([]);
    const agency = React.useContext(Agency);
    const [inspector, setInspector] = React.useState(null);
    const { setSecondGate, handleAnalytics, setAssetClassSelected, setAssetSelected, nameImageList } = agency;

    const collectData = async () => {

        try {
            let res = await getIcedContracts();
            console.log("res dataaa", res.data)
            let resTwo = await getIcedEarningBalance();
            let matchObj;
            if (resTwo.data.status) {
                matchObj = resTwo.data.result[0].balances
            } else {
                matchObj = []
            }

            let temp = res.data.icedContracts.map(
                obj => {
                    return {
                        ...obj,
                        keyId: nextId(),
                        coinSymbol: obj._id,
                        coinName: nameImageList[obj._id].coinName,
                        balance: matchObj.find((objTwo) => { return objTwo.coinSymbol === obj._id })?.coinValue,
                        coinValue: matchObj.find((objTwo) => { return objTwo.coinSymbol === obj._id })?.coinValue,
                        coinValueUSD: matchObj.find((objTwo) => { return objTwo.coinSymbol === obj._id })?.coinValueUSD,
                        coinName: nameImageList[obj._id].coinName,
                        coinImage: nameImageList[obj._id].coinImage,
                        _24hrchange: nameImageList?.[obj._id]._24hrchange
                    }
                }
            );
            setCurrentList([...temp]);
        } catch (e) {
            console.log("contracts api", e);
        }
    }
    const handleClick = (obj) => {
        if (!active)
            return
        console.log("coin Symbol", obj)
        handleAnalytics(""); setAssetSelected(Constant.navbarList[3]);
        setSecondGate(true);
        let a = setTimeout(() => {
            setAssetClassSelected(obj);
            clearTimeout(a);
        }, 100);
    }
    React.useEffect(() => {
        collectData()
    }, [])
    return (
        <div onWheel={()=>setInspector(null) } className="my-bonds-main">
            {
                !currentList.length ?
                    <div className="h-100 w-100 d-flex justify-content-center align-items-center">
                        <LoadingAnimation type="no-data" size={{ height: 200, width: 200 }} />
                    </div>
                    :
                    currentList.filter(obj => { return obj?._id?.toString().toLowerCase().startsWith(searchTerm?.toString().toLowerCase()) })
                        .map((obj) =>
                            <div className={obj?.keyId === inspector?.keyId ? "my-bond-inspector-row-wrapper" : "my-bond-row-wrapper"}>
                                {
                                    obj?.keyId === inspector?.keyId
                                        ?
                                        <BondsInspectorRow currentList={currentList} listInUSD={listInUSD} setInspector={setInspector} handleClick={handleClick} obj={obj} />
                                        :
                                        <MyBondRow listInUSD={listInUSD} handleClick={() => active ? setInspector(obj) : console.log()} obj={obj} />
                                }
                            </div>
                        )
            }
        </div>
    )
}
