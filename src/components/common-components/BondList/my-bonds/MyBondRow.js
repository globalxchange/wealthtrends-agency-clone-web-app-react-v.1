import React, { useEffect } from 'react'
import nextId from 'react-id-generator'
import Images from '../../../../assets/a-exporter'
import { Agency } from '../../../Context/Context';
export default function MyBondRow({ obj,listInUSD, handleClick }) {
    const agency = React.useContext(Agency);
    const { valueFormatter, cryptoUSD } = agency;
    const [usd, setUsd] = React.useState(listInUSD);
    const [mul, setMul] = React.useState(1);

    useEffect(() => {
        setUsd(listInUSD);
        if (listInUSD) {
            setMul(cryptoUSD[obj._id])

        } else {
            setMul(1)
        }
    }, [listInUSD])
    return (
        <div onClick={handleClick} className="my-bond-row" key={obj.keyId}>
            <div className="my-bond-row-top">
                <div>
                    <h3 className="coin-name"><img src={obj.coinImage} />{obj.coinName}</h3>
                    <p><span>{obj.count}</span> Active Bonds</p>
                </div>
                <div>
                    <h3>{valueFormatter(obj.outstandingValue * mul, usd ? "USD" : obj._id)}</h3>
                    <p><span>Value Of Bonds</span></p>
                    {/* <p>
                        <span onClick={() => setUsd(false)} className={!usd ? "active" : ""}>{obj._id}</span>
                        |
                        <span onClick={() => setUsd(true)} className={usd ? "active" : ""}>USD</span>
                        </p> */}
                </div>
            </div>
            <div className="my-bond-row-bottom">
                {
                    values.map(x =>
                        <div>
                            <h5>{valueFormatter(obj[x.link] * mul, usd ? "USD" : obj._id)}</h5>
                            <span>{x.name}</span>
                        </div>
                    )
                }
            </div>
        </div>
    )
}
const values = [
    { keyId: nextId(), name: "Investment", value: "0.0326", link: "investment" },
    { keyId: nextId(), name: "Remaining Earning Power", value: "0.0001", link: "remaining_earning_power" },
    { keyId: nextId(), name: "Current VOC ", value: "0.0136", link: "currentVoc" },
]