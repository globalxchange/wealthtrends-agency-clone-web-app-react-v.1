import React from 'react'
import nextId from 'react-id-generator';
import { Link } from 'react-router-dom'
import { Agency } from '../../../Context/Context'
import './investment-paths.style.scss'
export default function InvestmentPaths({ searchTerm }) {
    const agency = React.useContext(Agency);
    const { investmentPathOffering, setSelectedInvestmentType } = agency;
    const [list, setList] = React.useState([]);




    return (
        <div className="investment-paths-main">
            {
                investmentPathOffering.filter((x) => { return x.name.toLowerCase().startsWith(searchTerm.toLowerCase()) }).map(obj => <div key={obj._id + nextId()} className="investment-path-row">
                    <div className="path-row-left">
                        <img src={obj.icon} />
                        <div>
                            <Link onClick={() => setSelectedInvestmentType(obj)} to="/dashboard/Offering">
                                <h5>{!searchTerm ? obj.name : obj.name.toLowerCase().startsWith(searchTerm.toLowerCase()) ? <><mark style={{ backgroundColor: "yellow", padding: 0 }}>{obj.name.substring(0, searchTerm.length)}</mark>{obj.name.substring(searchTerm.length)}</> : obj.name}</h5>
                            </Link>
                            <span>Asset Type</span>
                        </div>
                    </div>
                    <div className="path-row-right">
                        <h5>{obj.count}</h5>
                        <span>{"Exclusive Listings"}</span>


                    </div>


                </div>
                )
            }
        </div>
    )
}
