import React from "react";
import nextId from "react-id-generator";
import "./earnings-tablesstyle.scss";
import Images from "../../../../assets/a-exporter";
import { Agency } from "../../../Context/Context";
import { getEarningsDetails } from "../../../../services/getAPIs";
import LoadingAnimation from "../../../../lotties/LoadingAnimation";
export default function EarningsTable({ list }) {
  const agency = React.useContext(Agency);
  const {
    valueFormatter,
    currencyImageList,
    conversionConfig,
    setCollapseSidebar,
    setDifferentiator,
    setUserChatEnable,
    setUpdatePortfolio,
    setDirectCoinOverview,
  } = agency;
  const [totalEarning, setTotalEarning] = React.useState(true);
  const [loading, setLoading] = React.useState(true);
  const [earningDetails, setEarningDetails] = React.useState();
  const setUpEarningDetails = async () => {
    setLoading(true);
    let res = await getEarningsDetails();
    console.log("earning details", res.data);
    setEarningDetails(res.data.users[0]);
    setLoading(false);
  };
  const handleClick = (obj) => {
    setDirectCoinOverview({
      status: true,
      coin: obj,
      type: "bond",
      len: 0,
      i: 0,
    });
    setUserChatEnable(true);
    setCollapseSidebar(true);
  };
  React.useEffect(() => {
    setUpEarningDetails();
  }, []);
  return loading ? (
    <div className="w-100 h-100 d-flex justify-content-center align-items-center">
      <LoadingAnimation />
    </div>
  ) : (
    <div className="earnings-table">
      <div className="earnings-table-header">
        {headers.map((obj) => (
          <h6 key={obj.keyId}>{obj.name}</h6>
        ))}
        <div>
          <img src={Images.searchWhite} />
          <img src={Images.expand} />
        </div>
      </div>
      <div className="earnings-table-body">
        {!list.length ? (
          <div className="w-100 h-100 d-flex justify-content-center align-items-center">
            <LoadingAnimation type="no-data" />
          </div>
        ) : (
          list.map((obj) => (
            <div
              onClick={() => handleClick(obj)}
              className="earnings-table-row"
            >
              <h6>
                <img src={obj.coinImage} />
                {obj.coinName}
              </h6>
              <h6>{obj.count}</h6>
              <h6>
                {valueFormatter(obj.coinValue, obj.coinSymbol)} {obj.coinSymbol}
              </h6>
              <h6>{valueFormatter(obj.coinValueUSD, "USD")} USD</h6>
            </div>
          ))
        )}
      </div>
      <div className="earnings-table-footer">
        <div className="e-t-f-left">
          {earnings.map((obj) => (
            <h6
              className={obj.id === totalEarning ? "selected-type" : ""}
              onClick={() => setTotalEarning(obj.id)}
              key={obj.keyId}
            >
              {obj.name}
            </h6>
          ))}
        </div>

        <div className="e-t-f-right">
          <h6>
            {totalEarning
              ? valueFormatter(
                  conversionConfig.enable
                    ? earningDetails.totalBondEarnings * conversionConfig.rate
                    : earningDetails.totalBondEarnings,
                  conversionConfig.enable ? conversionConfig.coin : "USD"
                )
              : valueFormatter(
                  conversionConfig.enable
                    ? earningDetails.icedInterest * conversionConfig.rate
                    : earningDetails.icedInterest,
                  conversionConfig.enable ? conversionConfig.coin : "USD"
                )}
          </h6>
          <button
            onClick={() => {
              setCollapseSidebar(true);
              setDifferentiator("profile");
              setUserChatEnable(true);
              setUpdatePortfolio(true);
            }}
          >
            USD <img src={currencyImageList["USD"]} />
          </button>
        </div>
      </div>
    </div>
  );
}
const headers = [
  { keyId: nextId(), name: "Asset" },
  { keyId: nextId(), name: "Active Bonds" },
  { keyId: nextId(), name: "Balance" },
  { keyId: nextId(), name: "Balance USD" },
];

const earnings = [
  { keyId: nextId(), name: "Total Earnings", id: true },
  { keyId: nextId(), name: "Net Balance", id: false },
];


