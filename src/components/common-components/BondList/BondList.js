import React from 'react'
import './bond-list.style.scss'
import Images from '../../../assets/a-exporter'
import SwitchComponent from '../switch/Switch'
import './bond-list.style.scss'
import BondOfferings from './bond-offerings/BondOfferings'
import MyBonds from './my-bonds/MyBonds'
import nextId from 'react-id-generator'
import SyndicateBondsOfferings from './syndicate-bonds-offerings/SyndicateBondsOfferings'
import { Agency } from '../../Context/Context'
import OfferingWrapper from './offering-wrapper/OfferingWrapper'
import InvestmentTable from './investment/InvestmentTable'
import Constant from '../../../json/constant';
import AssetList from '../AssetList/AssetList';
import EarningsTable from './earnings-table/EarningsTable'
export default function BondList({ fullScreen, id, active,  setFullScreen }) {
    const [offering, setOffering] = React.useState(true);
    const [searchActive, setSearchActive] = React.useState();
    const [searchTerm, setSearchTerm] = React.useState("");
    const [selectedHeader, setSelectedHeader] = React.useState(headerList[0]);
    const agency = React.useContext(Agency);
    const [listInUSD, setListInUSD] = React.useState(false);
    const { currentApp, allMarketCoins } = agency

    const selectBond = (type) => {
        switch (type) {
            case "investment": return <InvestmentTable />
            case "bonds": return <MyBonds listInUSD={listInUSD} active={active} searchTerm={searchTerm} />;
            case "offerings": return <OfferingWrapper searchTerm={searchTerm} />;
            case "standard": return <BondOfferings searchTerm={searchTerm} />;
            case "syndicate": return <SyndicateBondsOfferings searchTerm={searchTerm} />;
            case "earnings": return <EarningsTable list ={allMarketCoins.bonds} />
            default: return;

        }
    }
    const handleSwitch = () => {
        if (offering) {
            setSelectedHeader(headerList[1])
        } else {
            setSelectedHeader(headerList[0])
        }

        setOffering(!offering);
    }
    React.useEffect(() => {
        // alert(selectedHeader.id)
    }, [selectedHeader])
    React.useEffect(() => {
        // alert(selectedHeader.id)
        if (id !== "id4") {
            setSelectedHeader(headerList[0])
        } else {
            setSelectedHeader(headerListBonds[0])

        }
    }, [id])
    return (
        <div className="bond-list-main">
            <div className="bond-list-main-header">
                <div className="bond-list-header-left">
                    <h4>{id === "id4" ? "Bonds" : "Investment Products"}</h4>
                    <span>Supported by {currentApp.app_name}</span>
                </div>
                <div className="bond-list-header-right">
                    {
                        (id !== "id4" ? headerList : headerListBonds).map(obj =>
                            <button
                                className={obj.keyId === selectedHeader.keyId ? "selected-header" : ""}
                                onClick={() => setSelectedHeader(obj)}><img height="20px" src={obj.icon} />{obj.other}</button>
                        )
                    }

                </div>
            </div>
            <div className={`bond-list-header ${selectedHeader.id === "earnings"?"d-none":""}`}>
                {
                    selectedHeader.id === "investment" ?
                        <div className="investment-header">
                            {
                                Constant.assetListHeaderCrypto.map(obj => <span key={obj.keyId + nextId()}>{obj.name}</span>)
                            }

                        </div>
                        :
                        <div className={selectedHeader.id === "bonds" ? "switch-wrapper-bond" : "switch-wrapper-bond"}>
                            <span className={!listInUSD ? "switched" : ""}>Native</span>
                            <SwitchComponent trigger={listInUSD} onClick={() => setListInUSD(!listInUSD)} />
                            <span className={listInUSD ? "switched" : ""}>USD</span>
                        </div>

                }

                <h6 className="extra-images">
                    {
                        searchActive ?
                            <span onClick={() => { setSearchTerm(''); setSearchActive(false) }}>X</span>
                            :
                            <img onClick={() => setSearchActive(!searchActive)} src={Images.searchWhite} />
                    }
                    <img onClick={() => setFullScreen(!fullScreen)} src={Images.expand} />
                </h6>
            </div>
            <div style={selectedHeader.id ==="earnings"?{height: '85%'}:{}} className="bond-list-body">
                {
                    (id !== "id4" ? headerList : headerListBonds).map(obj =>
                        <div key={obj.keyId}
                            className={obj.keyId === selectedHeader.keyId ? "active-bond" : "all-bonds-wrapper"}
                        >
                            {selectBond(obj.id)}
                        </div>
                    )
                }

            </div>

        </div>
    )
}
const headerList = [
    { keyId: nextId(), name: "Holdings", id: "investment", other: "Holdings", icon: Images.holdingIcon },
    { keyId: nextId(), name: "My Offerings", id: "offerings", other: "Offerings", icon: Images.agencyDark },
    // { keyId: nextId(), name: "Standard Bonds", id: "standard" },
]
const headerListBonds = [
    // { keyId: nextId(), name: "Offerings", id: "offerings" },
    { keyId: nextId(), name: "Earnings", id: "earnings", other: "Earnings", icon: Images.holdingIcon },
    { keyId: nextId(), name: "My Holdings", id: "bonds", other: "Holdings", icon: Images.holdingIcon },
    { keyId: nextId(), name: "Syndicate Bonds", id: "syndicate", other: "Syndicates", icon: Images.agencyDark },
]