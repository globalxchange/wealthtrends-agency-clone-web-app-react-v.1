import React from 'react'
import { ConsoleView } from 'react-device-detect'
import nextId from 'react-id-generator'
import Images from '../../../../assets/a-exporter'
import Constant from '../../../../json/constant'
import LoadingAnimation from '../../../../lotties/LoadingAnimation'
import { getSyndicateBondsOffering } from '../../../../services/getAPIs'
import { getOfferingEquity, simulateContract } from '../../../../services/postAPIs'
import { Agency } from '../../../Context/Context'
import './syndicate-bonds-offerings.style.scss'

export default function SyndicateBondsOfferings({ searchTerm }) {
    const [selectRow, setSelectRow] = React.useState({ status: false, _id: null, template_id: null });
    const [offeringList, setOfferingList] = React.useState([]);
    const [loading, setLoading] = React.useState(false);
    const [loadingTwo, setLoadingTwo] = React.useState(false);
    const [curtain, setCurtain] = React.useState(false);
    const [curtainData, setCurtainData] = React.useState(null);
    const [curtainInUsd, setCurtainInUsd] = React.useState(false);
    const [days, setDays] = React.useState('');
    const agency = React.useContext(Agency);
    const { currencyImageList, nameImageList, cryptoUSD, overviewApp, valueFormatter, handleKeyPress, currentApp } = agency;

    const getSyndicateOfferings = async () => {
        let res;
        try {
            res = await getSyndicateBondsOffering(overviewApp.created_by);
            let tempArr = res.data.custom_bonds
            console.log("new bond", tempArr, overviewApp.created_by);
            setOfferingList(tempArr);
            setLoading(false)

        } catch (e) {
            console.error(e)
        }
        // overviewApp
    }
    React.useEffect(() => {
        console.log("offering list", offeringList)
    }, [offeringList])
    const getMaxBondValue = (obj) => {
        let val = Math.ceil(obj.fourth.coinValue / obj.third.amount);
        val = parseFloat(val).toFixed(2);
        return val
    }
    const simulate = async (obj) => {
        setLoadingTwo(true)
        try {
            const res = await simulateContract({
                coin: obj.coinsData[0]?.coin,
                payCoin: obj.coinsData[0]?.coin,
                bond_type: "custom",
                bond_template: "",
                days: 0,
                num_of_bonds: days, 
                bond_template_id: selectRow.template_id,
                simulate: true,
                stats: true
            });
            // {
            //     "coin": "USDT",
            //     "bond_type":"custom",
            //     "days": 0,
            //     "num_of_bonds": 1, 
            //     "bond_template_id":"66me5aukfyuzh6v149",
            //     "simulate": true,  
            //         "payCoin": "USDT",
            //         "stats": true
            // }
            setCurtainData({ ...res.data, gross: days * res.data.interestRate, grossUsd: days * res.data.interestValueUsd })
            setSelectRow({ status: true, _id: obj._id, template_id: null })
            console.log("simulate", selectRow)
            setCurtain(true);
            // setDays('');
            setLoadingTwo(false)
        } catch (e) {
            console.error(e)

        }
    }
    const closeSimulation = () => {
        setSelectRow({ status: false, _id: null, template_id: null });
        setCurtain(false);
        setDays('');
        setCurtainInUsd(false);
    }
    const currentRow = (obj) => {
        return selectRow._id === obj?._id
    }
    const refresh = (obj) => {
        console.log("compa new", selectRow, obj)
        if (selectRow?._id === obj._id) {
            setSelectRow({ status: true, _id: obj?._id, template_id: obj.bond_template_id });
        } else {
            setSelectRow({ status: false, _id: obj?._id, template_id: obj.bond_template_id });
            setDays('');
            setCurtain(false);
            setCurtainInUsd(false);
        }

    }


    const displayCurtainData = (type) => {
        switch (type) {
            case "grossRoi": return `${parseFloat(curtainData?.base_interest * curtainData?.days).toFixed(2)}%`;
            case "grossEarnings": return valueFormatter(curtainData?.earningPower, curtainData?.coin);
            case "grossEarningsUsd": return valueFormatter(curtainData?.earningPower * cryptoUSD[curtainData?.coin], "USD");
            case "totalFeesProfit": return `${parseFloat(curtainData?.feeRate * curtainData?.days).toFixed(6)}%`;
            case "netRoi": return `${parseFloat(((curtainData?.base_interest * curtainData?.days) - curtainData?.feeRate) ).toFixed(2)}%`;
            default: return ''

        }
    }
    const displayCurtainDataForSign = (type) =>{
        switch (type) {
            case "grossRoi": return parseFloat(curtainData?.base_interest * curtainData?.days).toFixed(2);
            case "grossEarnings": return curtainData?.earningPower
            case "grossEarningsUsd": return curtainData?.earningPower * cryptoUSD[curtainData?.coin];
            case "totalFeesProfit": return curtainData?.feeRate * curtainData?.days
            case "netRoi": return (curtainData?.base_interest * curtainData?.days) - curtainData?.feeRate
            default: return ''

        }

    }

    React.useEffect(() => {
        setLoading(true);
        getSyndicateOfferings()

    }, [])



    return (
        <div className="syndicate-bond-offering">
            <div className="bond-offering-header">
                {
                    Constant.syndicateHeader.map(obj => <span>{obj.name}</span>)
                }
            </div>
            <div className="bond-offering-body">
                {
                    loading ?
                        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                            <LoadingAnimation type="login" size={{ height: 200, width: 200 }} />
                        </div>
                        :
                        !offeringList.length ?
                            <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                                <LoadingAnimation type="no-data" size={{ height: 150, width: 150 }} />

                            </div>
                            :

                            offeringList.filter(obj => { return obj.coinsData[0]?.coin.toString().toLowerCase().startsWith(searchTerm.toString().toLowerCase()) })
                                .map(obj =>
                                    <div key={obj?._id} className={`bond-offering-row ${selectRow.status && obj?._id === selectRow?._id ? "full-curtain" : "normal-curtain"}`}>
                                        <div className="curtain-header">
                                            <span><img src={currencyImageList[`${obj.coinsData[0]?.coin}`]} />{nameImageList[obj.coinsData[0]?.coin]?.coinName}</span>
                                            <span>{valueFormatter(obj.coinsData[0]?.bondCost * (curtainInUsd && currentRow(obj) ? cryptoUSD[obj.coinsData[0]?.coin] : 1), curtainInUsd && currentRow(obj) ? "USD" : obj.coinsData[0]?.coin)} {curtainInUsd && currentRow(obj) ? "USD" : obj.coinsData[0]?.coin}</span>
                                            <span>{parseFloat(obj.daily_interest_rate).toFixed(2)}%</span>
                                            <span>{parseFloat(obj.daily_interest_rate * 365).toFixed(2)}%</span>
                                            <span>
                                                {valueFormatter(((obj.daily_interest_rate / 100) * (obj.coinsData[0]?.bondCost) * (curtainInUsd && currentRow(obj) ? cryptoUSD[obj.coinsData[0]?.coin] : 1)), curtainInUsd && currentRow(obj) ? "USD" : obj.coinsData[0]?.coin)} {curtainInUsd && currentRow(obj) ? "USD" : obj.coinsData[0]?.coin}
                                            </span>
                                            <form onSubmit={(e) => { e.preventDefault(); simulate(obj) }}>
                                                <input onKeyPress={(e) => handleKeyPress(e)} onFocus={() => refresh(obj)} value={currentRow(obj) ? days : ''} onChange={(e) => setDays(e.target.value)} placeholder="0.00" />
                                                <button onClick={() => { !days && !currentRow(obj) ? console.log() : simulate(obj) }}>{
                                                    loadingTwo && currentRow(obj) ?
                                                        <button className="spinner" />
                                                        :
                                                        <img src={Images.sendFlyer} />
                                                }</button>
                                            </form>

                                        </div>
                                        <div className="curtain-body">
                                            <div className="curtain-body-top">
                                                {
                                                    curtainBox.map(obj =>
                                                        <div key={obj.keyId}>
                                                            <h5 className={displayCurtainDataForSign(curtainInUsd && obj.link === "grossEarnings" ? obj.linkUSD : obj.link) < 0 ? "negative" : ""}>
                                                                {
                                                                    curtain ? displayCurtainData(curtainInUsd && obj.link === "grossEarnings" ? obj.linkUSD : obj.link) : ''
                                                                }
                                                                <span className={obj.link === "grossEarnings" ? "extra-span" : "d-none"}>{curtainInUsd ? "USD" : curtainData?.coin}</span>
                                                            </h5>
                                                            <span>{obj.name}</span>
                                                        </div>
                                                    )
                                                }

                                            </div>

                                            <div className="curtain-body-bottom">
                                                <p onClick={() => setCurtainInUsd(!curtainInUsd)}>View Data In {!curtainInUsd ? "US Dollars" : curtainData?.coin}</p>
                                                <h6
                                                >
                                                    {
                                                        ["Close Simulation", "Create Bond"]
                                                            .map((obj, i) =>
                                                                <button
                                                                    onClick={() => !i ? closeSimulation() : console.log()}
                                                                >{obj}</button>
                                                            )
                                                    }
                                                </h6>

                                            </div>


                                        </div>
                                    </div>
                                )
                }
            </div>

        </div>
    )
}

const curtainBox = [
    { keyId: nextId(), value: "0.12%", name: "Gross ROI", link: "grossRoi", linkUSD: "grossRoiUsd" },
    { keyId: nextId(), value: "0.0001", name: "Gross Earnings", link: "grossEarnings", linkUSD: "grossEarningsUsd" },
    { keyId: nextId(), value: "0.12%", name: "Total Fees Of Profit", link: "totalFeesProfit", linkUSD: "totalFeesProfitUsd" },
    { keyId: nextId(), value: "0.0011", name: "Net ROI", link: "netRoi", linkUSD: "netRoiUsd" },
]
const arrays = [
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },

]

