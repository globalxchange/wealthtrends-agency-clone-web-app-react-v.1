import React from 'react'
import { ConsoleView } from 'react-device-detect'
import nextId from 'react-id-generator'
import Images from '../../../../assets/a-exporter'
import Constant from '../../../../json/constant'
import LoadingAnimation from '../../../../lotties/LoadingAnimation'
import { fetchOfferingList } from '../../../../services/getAPIs'
import { getOfferingEquity, simulateContract } from '../../../../services/postAPIs'
import { Agency } from '../../../Context/Context'
import './bond-offering.style.scss'

export default function BondOfferings({searchTerm}) {
    const [selectRow, setSelectRow] = React.useState({status: false, _id: null});
    const [offeringList, setOfferingList] = React.useState([]);
    const [loading, setLoading] = React.useState(false);
    const [loadingTwo, setLoadingTwo] = React.useState(false);
    const [curtain, setCurtain] = React.useState(false);
    const [curtainData, setCurtainData] = React.useState(null);
    const [curtainInUsd, setCurtainInUsd] = React.useState(false);
    const [days, setDays] = React.useState('');
    const agency = React.useContext(Agency);
    const { currencyImageList, nameImageList,overviewApp, valueFormatter, handleKeyPress, currentApp } = agency;
    const getOfferingList = async () => {
        try {
            let res = await fetchOfferingList();
            let resTwo = await getOfferingEquity(currentApp);


            let temp = res.data.config_data;
            let tempTwo = resTwo.data.coins_data;
            console.log("offering for", temp, tempTwo)
            let tempArr = []
            let i = 0, len = temp.length;
            for (i = 0; i < temp.length; i = i + 3) {
                let cut = temp.slice(i, i + 3);
                tempArr = [
                    ...tempArr,
                    {
                        first: cut.find(obj => { return obj.type === "dailyFee" }),
                        second: cut.find(obj => { return obj.type === "interestCompressor" }),
                        third: cut.find(obj => { return obj.type === "contractCost" }),
                        fourth: tempTwo.find(obj => { return obj.coinSymbol === cut[0]?.coin })
                    }
                ]
                console.log("offering for", tempArr)
            }
            setOfferingList([...tempArr]);
            setLoading(false);
        } catch (e) {
            console.error("offering fetched failed", e);
            setLoading(false);
        }
    }

    const getMaxBondValue = (obj) => {
        let val = Math.ceil(obj?.fourth?.coinValue / obj?.third?.amount);
        val = parseFloat(val).toFixed(2);
        return val
    }
    const simulate = async (obj) => {
        setLoadingTwo(true)
        try {
            const res = await simulateContract({ coin: obj?.first?.coin, days: days, sim: true });
            setCurtainData({ ...res.data, gross: days * res.data.interestRate, grossUsd: days * res.data.interestValueUsd })
            setSelectRow({status: true, _id:  obj.first._id})
            setCurtain(true);
            // setDays('');
            setLoadingTwo(false)
        } catch (e) {
            console.error(e)

        }
    }
    const closeSimulation = () => {
        setSelectRow({status: false, _id: null});
        setCurtain(false);
        setDays('');
        setCurtainInUsd(false);
    }
    const currentRow = (obj) => {
        return selectRow._id === obj.first._id
    }
    const refresh = (obj) =>{
        console.log("compa", selectRow, obj)
        if (selectRow?._id === obj._id) {
            setSelectRow({ status: true, _id: obj?.first._id });
        } else {
            setSelectRow({ status: false, _id: obj?.first._id });
            setDays('');
            setCurtain(false);
            setCurtainInUsd(false);
        }

        
    }

    const displayCurtainData = (value, type, coin) => {
        switch (type) {
            case "interestRate": return `${parseFloat(value).toFixed(2)}%`;
            case "interestValue": return curtainInUsd ? valueFormatter(value, "USD") : parseFloat(value, coin).toFixed(6);
            case "gross": return `${parseFloat(value).toFixed(2)}%`;;
            case "earningPower": return valueFormatter(value, curtainInUsd ? "USD" : coin);
            default: return ''

        }
    }
    React.useEffect(() => {
        setLoading(true);
        getOfferingList();

    }, [])



    return (
        <div className="bond-offering">
            <div className="bond-offering-header">
                {
                    Constant.bondOfferingHeader.map(obj => <span>{obj.name}</span>)
                }
            </div>
            <div className="bond-offering-body">
                {
                    loading ?
                        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
                            <LoadingAnimation type="login" size={{ height: 200, width: 200 }} />
                        </div>
                        :

                        offeringList.filter(obj => {return obj?.first?.coin.toString().toLowerCase().startsWith(searchTerm.toString().toLowerCase())})
                        .map(obj =>
                            <div key={obj.first._id} className={`bond-offering-row ${selectRow.status && obj.first._id === selectRow._id ? "full-curtain" : "normal-curtain"}`}>
                                <div className="curtain-header">
                                    <span><img src={currencyImageList[`${obj?.first?.coin}`]} />{nameImageList[obj?.first?.coin]?.coinName}</span>
                                    <span>{valueFormatter(curtain && currentRow(obj) ? curtainInUsd ? curtainData?.investment_usd : curtainData?.investment : obj?.third?.amount,  curtainInUsd && currentRow(obj) ? "USD" : obj?.first?.coin)} {curtainInUsd && currentRow(obj)?"USD":obj?.first?.coin}</span>
                                    <span>{parseFloat(obj?.second?.base_compression_rate).toFixed(2)}%</span>
                                    <span>{valueFormatter(curtainInUsd && currentRow(obj)?obj?.fourth?.coinValueUSD :obj?.fourth?.coinValue,curtainInUsd && currentRow(obj)?"USD": obj?.fourth?.coinSymbol)}</span>
                                    <span>{getMaxBondValue(obj)}</span>
                                    <form onSubmit={(e)=>{e.preventDefault(); simulate(obj)}}>
                                        <input onKeyPress={(e)=>handleKeyPress(e)} onFocus={()=>refresh(obj)} value={currentRow(obj)?days: ''} onChange={(e) => setDays(e.target.value)} placeholder="0.00" />
                                        <button onClick={() => { !days && !currentRow(obj) ? console.log() : simulate(obj) }}>{
                                            loadingTwo && currentRow(obj) ?
                                                <button className="spinner" />
                                                :
                                                <img src={Images.sendFlyer} />
                                        }</button>
                                    </form>

                                </div>
                                <div className="curtain-body">
                                    <div className="curtain-body-top">
                                        {
                                            curtainBox.map(obj =>
                                                <div key={obj.keyId}>
                                                    <h5>
                                                        {
                                                            curtain ? displayCurtainData(curtainData?.[curtainInUsd ? obj.linkUSD : obj.link], obj.link, curtainData?.coin) : ''
                                                        }
                                                    </h5>
                                                    <span>{obj.name}</span>
                                                </div>
                                            )
                                        }

                                    </div>

                                    <div className="curtain-body-bottom">
                                        <p onClick={() => setCurtainInUsd(!curtainInUsd)}>View Data In {!curtainInUsd ? "US Dollars" : curtainData?.coin}</p>
                                        <h6
                                        >
                                            {
                                                ["Close Simulation", "Create Bond"]
                                                    .map((obj, i) =>
                                                        <button
                                                            onClick={() => !i ? closeSimulation() : console.log()}
                                                        >{obj}</button>
                                                    )
                                            }
                                        </h6>

                                    </div>


                                </div>
                            </div>
                        )
                }
            </div>

        </div>
    )
}

const curtainBox = [
    { keyId: nextId(), value: "0.12%", name: "Daily ROI", link: "interestRate", linkUSD: "interestRate" },
    { keyId: nextId(), value: "0.0001", name: "Daily P/L", link: "interestValue", linkUSD: "interestValueUsd" },
    { keyId: nextId(), value: "0.12%", name: "Gross ROI", link: "gross", linkUSD: "interestRate" },
    { keyId: nextId(), value: "0.0011", name: "Gross  P/L", link: "earningPower", linkUSD: "grossUsd" },
]
const arrays = [
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },
    { keyId: nextId(), value: "0.12%", name: "Daily ROI" },

]

