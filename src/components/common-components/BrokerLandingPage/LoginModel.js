import React, { useState, useEffect,useContext } from "react";
import Modal from "react-modal";
// import LoginLock from "../../images/altass.png";
import OtpInput from "react-otp-input";
import { Button, Alert, message } from "antd";
import Axios from "axios";
import { withRouter } from "react-router-dom";
import SignUpModal from "./SignUpModal";
import { brokerage } from "../../Brokercontextapi/Contextapi";
 import Restmodel from './ResetPin'
Modal.setAppElement("#root");

const modelStyle = {
  overlay: {
    zIndex: "9999999999",
    backgroundColor: "rgba(103, 103, 103, 0.75)"
  },
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    transform: "translate(-50%, -50%)",
    border: "none",
    background: "none",
    overflow: "auto",
    borderRadius: "none",
    height: "auto",
    display: "flex",
    justifyContent: "center",
    maxWidth: "80%",
    paddingTop: "5rem",
    paddingBottom: "5rem",
    paddingLeft: "0",
    paddingRight: "0"
  }
};

const LoginModel = ({ open, closeModal, history }) => {
  const {Reset,isRestLoginModalOpen,ResetClose,Crmfunctioncall} = useContext(brokerage);
  const [isLoading, setIsLoading] = useState(false);
  const [bck, setbck]=useState(false)
  const [pin, setPin] = useState("");
  const [error, setError] = useState("");
  const [isSignUpModalOpen, setIsSignUpModalOpen] = useState(false);
  const [IsResetModalOpen, setIsResetModalOpen] = useState(false);


  const otpfunction=(opt)=>
  {
    console.log("go",opt.toString().length)
    setPin(opt)
    
    if(opt.toString().length === 4)
    {
      setbck(true)
 
    }
    else{
      setbck(false)
    }

  }
  const loginHandler = (e) => {
    e.preventDefault();
    if (pin.toString().length < 4) {
      setError("Please input a 4-digit PIN");
     
    } else {
      setIsLoading(true);
     
      Axios.post("https://comms.globalxchange.com/coin/vault/broker/login", {
        email: localStorage.getItem("userEmail"),
        token: localStorage.getItem("idToken"),
        pin: pin.toString()
      })
        .then(resp => {
          console.log("Broker Login Resp =>", resp.data);
          if (resp.data.status) {
            closeModal();
            message.success("Logged In Successfully...");
           
            localStorage.setItem("broker-logged", true);
            // history.push("/broker-dashboard-home");
            history.push("/brokerbashboard");
            Crmfunctioncall()
           
          } else {
            setError(
              resp.data.message
                ? resp.data.message
                : "Some thing went wrong...!"
            );
          }
        })
        .catch(error => {
          console.log("Broker Login Error =>", error);
          setError("Some thing went wrong...!");
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  };

  useEffect(() => {
    return () => {};
  }, [open]);

  useEffect(() => {
    setError("");
    console.log("PIN IS =>", pin);
    return () => {};
  }, [pin]);
  const signup = () => {
    setIsSignUpModalOpen(true);
  };


  
  return (
    <>
    <div>
    <Modal
      isOpen={open}
      onRequestClose={closeModal}
      style={modelStyle}
      contentLabel="Login to Broker Dashboard"
    >
      <>
      <form  onSubmit={loginHandler}>
      <div style={{background:"white"}}>
      <div className="broker-modal-container broker-login-modal-container">

        {/* <img src={LoginLock} alt="" className="login-icon" /> */}
        <h4 className="login-header">Enter Afilliate Pin</h4>
        {/* <p className="desc">
          If you don't have and PIN and you're already a Atlas Affiliate, Please create
          one by Signing Up
        </p> */}
        <div className="pin-input-wrapper">
          
          
          <OtpInput
            value={pin}
            onChange={otp => otpfunction(otp) }
            autoFocus
            numInputs={4}
            isInputNum
            separator={<span></span>}
            inputStyle={{
              width: "3rem",
              height: "3rem",
              margin: "0 0.5rem",
              fontSize: "1.5rem",
              borderRadius: 4,
              color: "#515978",
              border: "1px solid rgba(0,0,0,0.3)"
            }}
          />
        </div>
        {error.length > 0 ? (
          <div className="error-wrapper">
            <Alert message={error} type="error" showIcon />
          </div>
        ) : null}

      <p className="custom-p-protal"><span style={{  color: "#182542"}} onClick={Reset}>Click Here </span> To Reset Your Pin</p>
        </div>
      
      <button type="submit" className={bck?"reset-pin1":"reset-pin"} 
  
      >Submit</button>
     
      </div>
      </form>
      </>
    </Modal>
    </div>
    {/* <SignUpModal
        open={isSignUpModalOpen}
        closeModal={() => setIsSignUpModalOpen(false)}
      /> */}
       <Restmodel   
       open={isRestLoginModalOpen}
       closeModal={ResetClose}
      />
    </> 
  );
};

export default withRouter(LoginModel);
