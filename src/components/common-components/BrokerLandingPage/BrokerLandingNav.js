import React, { useState, useContext, useEffect } from "react";
// import brokerLogo from "../../images/af.png";
import { Link } from "react-router-dom";
// import { notification } from "antd";
import { brokerage } from "../../Brokercontextapi/Contextapi";
import LoginModel from "./LoginModel";
import SignUpModal from "./SignUpModal";

const BrokerLandingNav = () => {
  const {context,isLoginModalOpen,loginNav,loginNavclose} = useContext(brokerage);

  // const [isLoginModalOpen, setIsLoginModalOpen] = useContext(false);
  // const [isSignUpModalOpen, setIsSignUpModalOpen] = useState(false);

  // const { is_broker } = context;

 

  // const signup = () => {
  //   setIsSignUpModalOpen(true);
  // };

  useEffect(() => {
    console.log("Checking Context =>", context);
    return () => {};
  }, []);

  return (
    <>
      <div className="spacingthebroker ">
        <div className="broker-landing-nav-wrapper">
          <div>
          <p className="textheader">All Your Clients To Become </p>
          <p className="textheader">Brokers For Your Services</p>
          </div>
  
          <div className="">
            <div className="accessportal" onClick={loginNav}>
            
              <label className="">BrokerApp</label>
       
            </div>
        
          </div>
        </div>
        <LoginModel
        open={isLoginModalOpen}
         closeModal={loginNavclose}
      />
      </div>
   
      {/* <SignUpModal
        open={isSignUpModalOpen}
        closeModal={() => setIsSignUpModalOpen(false)}
      /> */}
    </>
  );
};

export default BrokerLandingNav;
