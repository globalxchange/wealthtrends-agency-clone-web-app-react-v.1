import React from "react";
// import brokerLogo from "../../images/gx-broker-logo.png";

const partnerList = [
  { name: "GX", image: "brokerLogo" },
  { name: "GX", image: "brokerLogo" },
  { name: "GX", image: "brokerLogo" },
  { name: "GX", image: "brokerLogo" },
  { name: "GX", image: "brokerLogo" },
  { name: "GX", image: "brokerLogo" }
];

const BrokerLandingPartners = () => {
  return (
    <div className="broker-partners-wrapper">
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <h3 className="partners-header">
              Lorem ipsum, dolor sit amet consectetur adipisicing elit vitae!
            </h3>
            <p className="partner-intro">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero
              error dolor quam eius explicabo illum, ea doloribus mollitia quo!
              Nam suscipit laboriosam iste ad vitae labore doloremque
              consequuntur fuga quas?
            </p>
          </div>
          <div className="col-md-6">
            <div className="partner-list">
              {partnerList.map((item, index) => (
                <div className="partner-img-wrapper">
                  <img
                    key={index}
                    className="partner-img"
                    src={item.image}
                    alt={item.name}
                  />
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BrokerLandingPartners;
