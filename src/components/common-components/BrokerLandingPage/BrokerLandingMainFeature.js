import React from "react";
// import brokerFeatureImage from "../../images/broker-crypto-bg.svg";

const mainFeatures = [
  {
    title: "Offer Online Payments",
    body:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quis optio laborum delectus necessitatibus, voluptates nisi iste earum?"
  },
  {
    title: "Increase your Revenue",
    body:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quis optio laborum delectus necessitatibus, voluptates nisi iste earum?"
  },
  {
    title: "Strengthen your Offerings",
    body:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quis optio laborum delectus necessitatibus, voluptates nisi iste earum?"
  },
  {
    title: "Get Preferential Training & Enablement",
    body:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quis optio laborum delectus necessitatibus, voluptates nisi iste earum?"
  }
];

const BrokerLandingMainFeature = () => {
  return (
    <div className="container">
      <div className="broker-main-feature-wrapper">
        <h1 className="main-feature-header">
          Lorem ipsum dolor sit amet consectetur
          <br /> adipisicing elit. Vel a, fugiat
        </h1>
        <div className="row">
          <div className="col-md-6">
            <div className="feature-list">
              {mainFeatures.map((item, index) => (
                <div key={index} className="main-feature-item">
                  <h4 className="feature-header">{item.title}</h4>
                  <p className="feature-body">{item.body}</p>
                </div>
              ))}
            </div>
          </div>
          <div className="col-sm-6">
            <div className="feature-branding">
              <div
                className="feature-image-background"
                // style={{ backgroundImage: `url(${brokerFeatureImage})` }}
              />
              <p className="feature-image-desc">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima
                sed, magni nesciunt esse facilis totam sit mollitia ratione,
                officiis delectus eveniet provident voluptas atque fugit.
                Dolore, placeat expedita. Rem, voluptas.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BrokerLandingMainFeature;
