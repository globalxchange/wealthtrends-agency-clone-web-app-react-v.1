import React, { useState, useEffect ,useContext} from "react";
import Modal from "react-modal";
// import LoginLock from "../../images/altass.png";
import { brokerage } from "../../Brokercontextapi/Contextapi";
import OtpInput from "react-otp-input";

import { Button, Alert, message } from "antd";
import Axios from "axios";
import { withRouter } from "react-router-dom";
import SignUpModal from "./SignUpModal";
// import LoginModel from './LoginModel'
Modal.setAppElement("#root");

const modelStyle = {
  overlay: {
    zIndex: "9999999999",
    backgroundColor: "rgba(103, 103, 103, 0.75)"
  },
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    transform: "translate(-50%, -50%)",
    border: "none",
    background: "none",
    overflow: "auto",
    borderRadius: "none",
    height: "auto",
    display: "flex",
    justifyContent: "center",
    maxWidth: "80%",
    paddingTop: "5rem",
    paddingBottom: "5rem",
    paddingLeft: "0",
    paddingRight: "0"
  }
};

const ResetPin = ({ open, closeModal, history }) => {
  const {context,isLoginModalOpen,loginNav,loginNavclose,Resetbacklog} = useContext(brokerage);
  const [isLoading, setIsLoading] = useState(false);
  const [Rpin, setRPin] = useState("");
  const [Repin, setRePin] = useState("");
  const [error, setError] = useState("");
 
 
 
console.log(Rpin)
  return (
    <>
    <div>
    <Modal
      isOpen={open}
      onRequestClose={closeModal}
      style={modelStyle}
      contentLabel="Login to Broker Dashboard"
    >
      <>
      <div style={{background:"white"}}>
      <div className="broker-modal-container broker-login-modal-container">

        {/* <img src={LoginLock} alt="" className="login-icon" /> */}
        <h4 className="login-header">Reset Afilliate Pin</h4>
        {/* <p className="desc">
          If you don't have and PIN and you're already a Atlas Affiliate, Please create
          one by Signing Up
        </p> */}
        <p className="reset-text">Enter your 4-digit code your received via E-Mail</p>
           <div className="pin-input-wrapper-custom">
          <OtpInput
            value={Repin}
            onChange={setRePin}
            autoFocus
            numInputs={4}
            isInputNum
            separator={<span></span>}
            inputStyle={{
              width: "3rem",
              height: "3rem",
              margin: "0 0.5rem",
              fontSize: "1.5rem",
              borderRadius: 4,
              color: "#515978",
              border: "1px solid rgba(0,0,0,0.3)"
            }}
          />
        </div>
        <p className="reset-text">Enter your new PIN</p>
        <div className="pin-input-wrapper-custom">
          <OtpInput
            value={Rpin}
            onChange={setRPin}
            autoFocus
            numInputs={4}
            isInputNum
            separator={<span></span>}
            inputStyle={{
              width: "3rem",
              height: "3rem",
              margin: "0 0.5rem",
              fontSize: "1.5rem",
              borderRadius: 4,
              color: "#515978",
              border: "1px solid rgba(0,0,0,0.3)"
            }}
          />
        </div>
        {error.length > 0 ? (
          <div className="error-wrapper">
            <Alert message={error} type="error" showIcon />
          </div>
        ) : null}

      <p className="custom-p-protal"><span style={{  color: "#000000"}}  onClick={Resetbacklog}>Click Here </span> To Affiliate Pin</p>
      
      
        {/* <div className="login-btn-wrapper">
          <Button type="primary" loading={isLoading} onClick={loginHandler}>
            {isLoading ? "Logging In" : "Log In"}
          </Button>
        </div> */}
        
        {/* <div className="login-btn-wrapper">
          <Button type="primary"  >
          Create pin
          </Button>
        </div> */}



        {/* <div className="sign-up-btn ant-btn login-btn-wrapper" onClick={signup}>
              <label className="signup ">
                Sign Up
                <span>
                  <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                </span>
              </label>
            </div> */}


      </div>
      
      <button className="reset-pin">Reset Pin</button>
      </div>
      </>
    </Modal>
    </div>
    {/* <LoginModel
    open={isLoginModalOpen}
    closeModal={() => setIsLoginModalOpen(false)}
  /> */}
    </>
  );
};

export default  ResetPin
