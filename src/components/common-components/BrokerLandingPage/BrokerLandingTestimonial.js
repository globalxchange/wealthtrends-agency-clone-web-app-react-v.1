// import React, { useState } from "react";
// import "react-responsive-carousel/lib/styles/carousel.min.css";
// import { Carousel } from "react-responsive-carousel";

// const testimonials = [
//   {
//     header: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
//     body:
//       "Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe,consectetur totam dicta tempore distinctio esse oditperspiciatis non vel iste corrupti velit sunt sed, eos namaliquam voluptate dolor repudiandae. Eum, facere tempore iste eaaperiam perspiciatis debitis commodi beatae, delectus doloribus,esse harum. Amet obcaecati incidunt quo pariatur rerum beataerecusandae repellat. Pariatur veniam recusandae dicta, sequiincidunt rerum. Eveniet, neque laudantium! Iusto quisquam eiusconsequatur ipsum voluptates rerum vitae non harum, iddistinctio magnam exercitationem inventore ducimusnecessitatibus voluptatem unde. Reprehenderit ratione delectusnobis rem ipsam accusantium aliquam.",
//     image: "https://i.pravatar.cc/110",
//     authorName: "John Doe",
//     orgName: "Google Inc."
//   },
//   {
//     header: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
//     body:
//       "Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe,consectetur totam dicta tempore distinctio esse oditperspiciatis non vel iste corrupti velit sunt sed, eos namaliquam voluptate dolor repudiandae. Eum, facere tempore iste eaaperiam perspiciatis debitis commodi beatae, delectus doloribus,esse harum. Amet obcaecati incidunt quo pariatur rerum beataerecusandae repellat. Pariatur veniam recusandae dicta, sequiincidunt rerum. Eveniet, neque laudantium! Iusto quisquam eiusconsequatur ipsum voluptates rerum vitae non harum, iddistinctio magnam exercitationem inventore ducimusnecessitatibus voluptatem unde. Reprehenderit ratione delectusnobis rem ipsam accusantium aliquam.",
//     image: "https://i.pravatar.cc/110",
//     authorName: "John Doe",
//     orgName: "Google Inc."
//   },
//   {
//     header: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
//     body:
//       "Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe,consectetur totam dicta tempore distinctio esse oditperspiciatis non vel iste corrupti velit sunt sed, eos namaliquam voluptate dolor repudiandae. Eum, facere tempore iste eaaperiam perspiciatis debitis commodi beatae, delectus doloribus,esse harum. Amet obcaecati incidunt quo pariatur rerum beataerecusandae repellat. Pariatur veniam recusandae dicta, sequiincidunt rerum. Eveniet, neque laudantium! Iusto quisquam eiusconsequatur ipsum voluptates rerum vitae non harum, iddistinctio magnam exercitationem inventore ducimusnecessitatibus voluptatem unde. Reprehenderit ratione delectusnobis rem ipsam accusantium aliquam.",
//     image: "https://i.pravatar.cc/110",
//     authorName: "John Doe",
//     orgName: "Google Inc."
//   },
//   {
//     header: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
//     body:
//       "Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe,consectetur totam dicta tempore distinctio esse oditperspiciatis non vel iste corrupti velit sunt sed, eos namaliquam voluptate dolor repudiandae. Eum, facere tempore iste eaaperiam perspiciatis debitis commodi beatae, delectus doloribus,esse harum. Amet obcaecati incidunt quo pariatur rerum beataerecusandae repellat. Pariatur veniam recusandae dicta, sequiincidunt rerum. Eveniet, neque laudantium! Iusto quisquam eiusconsequatur ipsum voluptates rerum vitae non harum, iddistinctio magnam exercitationem inventore ducimusnecessitatibus voluptatem unde. Reprehenderit ratione delectusnobis rem ipsam accusantium aliquam.",
//     image: "https://i.pravatar.cc/110",
//     authorName: "John Doe",
//     orgName: "Google Inc."
//   },
//   {
//     header: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
//     body:
//       "Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe,consectetur totam dicta tempore distinctio esse oditperspiciatis non vel iste corrupti velit sunt sed, eos namaliquam voluptate dolor repudiandae. Eum, facere tempore iste eaaperiam perspiciatis debitis commodi beatae, delectus doloribus,esse harum. Amet obcaecati incidunt quo pariatur rerum beataerecusandae repellat. Pariatur veniam recusandae dicta, sequiincidunt rerum. Eveniet, neque laudantium! Iusto quisquam eiusconsequatur ipsum voluptates rerum vitae non harum, iddistinctio magnam exercitationem inventore ducimusnecessitatibus voluptatem unde. Reprehenderit ratione delectusnobis rem ipsam accusantium aliquam.",
//     image: "https://i.pravatar.cc/110",
//     authorName: "John Doe",
//     orgName: "Google Inc."
//   }
// ];

// const BrokerLandingTestimonial = () => {
//   const [carouselCurrentPos, setCarouselCurrentPos] = useState(0);
//   return (
//     <div className="broker-testimonial-wrapper">
//       <div className="container-fluid">
//         <h3 className="testimonial-header">
//           Offer a trading experience
//           <br />
//           people love to talk about
//         </h3>
//         <div className="testimonial-carousel-wrapper">
//           <Carousel
//             showThumbs={false}
//             showStatus={false}
//             showIndicators={false}
//             centerMode
//             autoPlay
//             showArrows={false}
//             centerSlidePercentage={50}
//             infiniteLoop
//           >
//             {testimonials.map((item, index) => (
//               <div key={index} className="testimonial-carousel-item">
//                 <h4 className="testimonial-title">{item.header}</h4>
//                 <p className="testimonial-body">{item.body}</p>
//                 <div className="testimonial-author">
//                   <img src={item.image} alt="" className="author-img" />
//                   <div className="names">
//                     <h6 className="author-name">{item.authorName}</h6>
//                     <h6 className="org-name">{item.orgName}</h6>
//                   </div>
//                 </div>
//               </div>
//             ))}
//           </Carousel>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default BrokerLandingTestimonial;
