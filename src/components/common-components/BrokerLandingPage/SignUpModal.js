import React, { useState, useEffect } from "react";
import Modal from "react-modal";
// import LoginLock from "../../images/altass.png";
import OtpInput from "react-otp-input";
import { Button, Alert, message } from "antd";
import Axios from "axios";

Modal.setAppElement("#root");

const modelStyle = {
  overlay: {
    zIndex: "9999999999",
    backgroundColor: "rgba(103, 103, 103, 0.75)"
  },
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    transform: "translate(-50%, -50%)",
    border: "none",
    background: "none",
    overflow: "auto",
    borderRadius: "none",
    height: "auto",
    display: "flex",
    justifyContent: "center",
    maxWidth: "80%",
    paddingTop: "5rem",
    paddingBottom: "5rem",
    paddingLeft: "0",
    paddingRight: "0"
  }
};

const SignUpModal = ({ open, closeModal }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [pin, setPin] = useState("");
  const [error, setError] = useState("");

  const signupHandler = () => {
    if (pin.toString().length < 4) {
      setError("Please input a 4-digit PIN");
    } else {
      setIsLoading(true);
      Axios.post(
        "https://comms.globalxchange.com/coin/vault/broker/license/signup",
        {
          email: localStorage.getItem("user_account"),
          token: localStorage.getItem("token"),
          pin: pin.toString()
        }
      )
        .then(resp => {
          console.log("Broker SignUp Resp =>", resp.data);
          if (resp.data.status) {
            closeModal();
            message.success("Signed Up Successfully...");
          } else {
            setError(
              resp.data.message
                ? resp.data.message
                : "Some thing went wrong...!"
            );
          }
        })
        .catch(error => {
          console.log("Broker SignUp Error =>", error);
          setError("Some thing went wrong...!");
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  };

  useEffect(() => {
    setError("");
    console.log("PIN IS =>", pin);
    return () => {};
  }, [pin]);

  return (
    <Modal
      isOpen={open}
      onRequestClose={closeModal}
      style={modelStyle}
      contentLabel="Signup to Broker Dashboard"
    >
      <div className="broker-modal-container broker-sign-up-container">
        {/* <img src={LoginLock} alt="" className="login-icon" /> */}
        <h4 className="login-header">Create a 4-Digit PIN</h4>
        <p className="desc">Create an 4-Digit Pin for your Broker Dashboard</p>
        <div className="pin-input-wrapper">
          <OtpInput
            value={pin}
            onChange={setPin}
            autoFocus
            numInputs={4}
            isInputNum
            separator={<span>-</span>}
            inputStyle={{
              width: "2.5rem",
              height: "2.5rem",
              margin: "0 0.5rem",
              fontSize: "1.5rem",
              borderRadius: 4,
              color: "#515978",
              border: "1px solid rgba(0,0,0,0.3)"
            }}
          />
        </div>
        {error.length > 0 ? (
          <div className="error-wrapper">
            <Alert message={error} type="error" showIcon />
          </div>
        ) : null}
        <div className="login-btn-wrapper">
          <Button type="primary" loading={isLoading} onClick={signupHandler}>
            {isLoading ? "Create" : "Create"}
          </Button>
        </div>
      </div>
    </Modal>
  );
};

export default SignUpModal;
