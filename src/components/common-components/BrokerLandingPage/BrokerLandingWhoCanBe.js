import React, { useState } from "react";

const whoCanBeList = [
  { title: "Lorem Ipsum", icon: "fa fa-rss", image: "" },
  { title: "Lorem Ipsum", icon: "fa fa-rss", image: "" },
  { title: "Lorem Ipsum", icon: "fa fa-rss", image: "" },
  { title: "Lorem Ipsum", icon: "fa fa-rss", image: "" },
  { title: "Lorem Ipsum", icon: "fa fa-rss", image: "" }
];

const BrokerLandingWhoCanBe = () => {
  const [selectedItemIndex, setSelectedItemIndex] = useState(0);

  return (
    <div className="broker-who-can-be-wrapper">
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <h4 className="who-can-be-header">Who can become GX Broker ?</h4>
            <p className="who-can-be-intro">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Recusandae odit consectetur optio omnis placeat veniam a
              blanditiis, error porro dicta voluptates dolorum eligendi.
            </p>
            <div className="who-can-be-list">
              {whoCanBeList.map((item, index) => (
                <div
                  key={index}
                  className={`who-can-be-item ${
                    selectedItemIndex === index ? "active" : ""
                  }`}
                  onClick={() => setSelectedItemIndex(index)}
                >
                  <i className={item.icon} />
                  <h4 className="list-item-title">{item.title}</h4>
                </div>
              ))}
            </div>
          </div>
          <div className="col-md-6">
            <div className="who-can-be-image-wrapper">
              <img src="" alt="" className="who-can-be-image" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BrokerLandingWhoCanBe;
