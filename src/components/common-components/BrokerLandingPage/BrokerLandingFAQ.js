import React, { useState } from "react";

const faqList = [
  {
    qstn: "What is GXBroker ?",
    title: "I am Crypto Wale, and I can be a GXBroker!!",
    body:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur labore ipsa earum voluptatibus aliquid, eveniet debitis provident ab illo iure repellat suscipit rerum odit tempora magni recusandae dolorum asperiores exercitationem?"
  },
  {
    qstn: "What is GXBroker ?",
    title: "I am Crypto Wale, and I can be a GXBroker!!",
    body:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur labore ipsa earum voluptatibus aliquid, eveniet debitis provident ab illo iure repellat suscipit rerum odit tempora magni recusandae dolorum asperiores exercitationem?"
  },
  {
    qstn: "What is GXBroker ?",
    title: "I am Crypto Wale, and I can be a GXBroker!!",
    body:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur labore ipsa earum voluptatibus aliquid, eveniet debitis provident ab illo iure repellat suscipit rerum odit tempora magni recusandae dolorum asperiores exercitationem?"
  },
  {
    qstn: "What is GXBroker ?",
    title: "I am Crypto Wale, and I can be a GXBroker!!",
    body:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur labore ipsa earum voluptatibus aliquid, eveniet debitis provident ab illo iure repellat suscipit rerum odit tempora magni recusandae dolorum asperiores exercitationem?"
  },
  {
    qstn: "What is GXBroker ?",
    title: "I am Crypto Wale, and I can be a GXBroker!!",
    body:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur labore ipsa earum voluptatibus aliquid, eveniet debitis provident ab illo iure repellat suscipit rerum odit tempora magni recusandae dolorum asperiores exercitationem?"
  },
  {
    qstn: "What is GXBroker ?",
    title: "I am Crypto Wale, and I can be a GXBroker!!",
    body:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur labore ipsa earum voluptatibus aliquid, eveniet debitis provident ab illo iure repellat suscipit rerum odit tempora magni recusandae dolorum asperiores exercitationem?"
  },
  {
    qstn: "What is GXBroker ?",
    title: "I am Crypto Wale, and I can be a GXBroker!!",
    body:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur labore ipsa earum voluptatibus aliquid, eveniet debitis provident ab illo iure repellat suscipit rerum odit tempora magni recusandae dolorum asperiores exercitationem?"
  },
  {
    qstn: "What is GXBroker ?",
    title: "I am Crypto Wale, and I can be a GXBroker!!",
    body:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur labore ipsa earum voluptatibus aliquid, eveniet debitis provident ab illo iure repellat suscipit rerum odit tempora magni recusandae dolorum asperiores exercitationem?"
  }
];

const BrokerLandingFAQ = () => {
  const [selectedItem, setSelectedItem] = useState(faqList[0]);

  return (
    <div className="broker-landing-faq-wrapper">
      <div className="container">
        <h3 className="faq-header">Frequently Asked Questions</h3>
        <div className="row">
          <div className="col-md-6">
            <div className="faq-list-wrapper">
              {faqList.map((item, index) => (
                <div
                  key={index}
                  className={`faq-list-item ${
                    selectedItem === item ? "active" : ""
                  }`}
                  onClick={() => setSelectedItem(item)}
                >
                  <div className="faq-dot" />
                  <div className="faq-qstn">{item.qstn}</div>
                </div>
              ))}
            </div>
          </div>
          <div className="col-md-6">
            <div className="faq-overview-wrapper">
              <h5 className="faq-sub-header">{selectedItem.title}</h5>
              <p className="faq-body">{selectedItem.body}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BrokerLandingFAQ;
