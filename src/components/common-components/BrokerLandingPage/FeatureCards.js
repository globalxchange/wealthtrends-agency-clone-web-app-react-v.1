import React, { useState, useEffect, useContext, useRef } from "react";
import shorupan from '../../../assets/shorupan.jpg'
import Youtude from '../../../assets/youbuttons.png'
// import Altass from '../../images/altass.png'

import { brokerage } from "../../Brokercontextapi/Contextapi";
import ReactPlayer from 'react-player'
import AliceCarousel from 'react-alice-carousel'
import {Modal} from 'react-bootstrap'
import "react-alice-carousel/lib/alice-carousel.css";

const responsive = {
  0: { items: 2 },
  500:{item:2},
  1024: { items: 3 },
  1600: {items: 3},
  2000:{items: 4}
}
const featureCardsimg = [
  {
    videofile:"https://www.youtube.com/watch?v=dSx6pcJBj9w",
    title: "Oppurtunity",
    thamnail:shorupan,
    playbuttone: Youtude,
    content:"Atlas Founder Explains The Vision",
    series:"Compnay Overview"
  },
  {
    videofile:"https://www.youtube.com/watch?v=dSx6pcJBj9w",
    title: "Testimonials",
    thamnail:shorupan,
    playbuttone: Youtude,
    content:"Atlas Founder Explains The Vision",
    series:"Compnay Overview"
  },
  {
    videofile:"https://www.youtube.com/watch?v=dSx6pcJBj9w",
    title: "Leadership",
    thamnail:shorupan,
    playbuttone: Youtude,
    content:"Atlas Founder Explains The Vision",
    series:"Compnay Overview"
  },
  {
    videofile:"https://www.youtube.com/watch?v=dSx6pcJBj9w",
    title: "Products",
    thamnail:shorupan,
    playbuttone: Youtude,
    content:"Atlas Founder Explains The Vision",
    series:"Compnay Overview"
  },
  // {
  //   videofile:"https://www.youtube.com/watch?v=dSx6pcJBj9w",
  //   title: "Oppurtunitys",
  //   thamnail:shorupan,
  //   playbuttone: "fab fa-youtube",
  //   content:"Atlas Founder Explains The Vision",
  //   series:"Compnay Overview"
  // },

 
];
// const responsive = {
//   0: {
//     items: 1,
// },
// 1024: {
//     items: 3
// }
// };




const FeatureCards = () => {
  
  useEffect(() => {
    // setgalary(galleryItems)
  }, []);
  


  const [galary, setgalary] = useState('');
  const [featureinfo, setfeatureinfo] = useState([]);
  const [featureinfoV, setfeatureinfoV] = useState(false);
  const [Showvido, setShowvido] = useState(false);
const[currentIndex, setcurrentIndex]=useState(0);
  const handleClosez = () => 
  {
  setShowvido(false);
  setfeatureinfoV(false);
}
  // const handleShow = () => setShow(true);
  const datashowvideo=(item)=>
{
 
 setfeatureinfo([item])
  setShowvido(true)
}
// const galleryItems=()=> {
// return (
 
//     featureCards.slice(0,5).map((item,i)=>
//       {
      
//         return(
//           <div>
//           <div key = {i}  className="feature-carousel" 
       
//           style={
//             {
//             backgroundImage:`url(${item.thamnail})`
            
//             }
//           }   >
//             <div  className="black-iten"
//             ></div>
//            <div className="custom-sectionfeature">
//              <img className="btnm" src={Youtude} alt=""onClick={(e)=>datashowvideo(item)}/>
//           {/* <i className={item.playbuttone}    ></i> */}
//           </div>
//           <h1 className="fe-title">{item.title}</h1>
//           </div>
//           </div>
//         )
//       }
      
      
//       )
// )
  
// }
const slidePrev=()=>
{
  
}
const handleOnDragStart = (e) => e.preventDefault()

  // const handleSelectFeature = item => {
  //   payWithSidebarOpen ([
  //       {
  //         title: item.title,
  //         subtitle: item.intro ,
  //         rate: parseInt(item.money),
  //         img:item.imgtag
  //       }
  //     ],
  //     "0",
  //     0,
  //     callBack,
  //     null,
  //     paymentId,
  //     merchantName,
  //     false,
  //     merchantMailId
      
  //     );
  //   }


  
const arr=()=>

{
  let p =0
   p =currentIndex +1
  setcurrentIndex(p)
}
const datashowvideomodel=()=>
{
  setfeatureinfoV(true)
}


  return (
    <>
    <div className="spacingthevideo">
      <div className="row feature-rows">



      <div className="wrraapimg">
{
   featureCardsimg.map((item,i)=>
  {
  
    return(
   
      <div key = {i}  className="wrapperitem"style={
        {
        backgroundImage:`url(${item.thamnail})`
        
        }
      }   >
        <div  className="black-iten"
        ></div>
       <div className="custom-sectionfeature">
         <img className="btnm" src={Youtude} alt=""onClick={(e)=>datashowvideo(item)}/>
      {/* <i className={item.playbuttone}    ></i> */}
      </div>
      <h1 className="fe-title">{item.title}</h1>
      </div>

    )
  }
     )
}

</div>


    

{/* <AliceCarousel
          dotsDisabled={true}
          buttonsDisabled={true}
          infinite={false}
          items={galary}
          onDragStart={handleOnDragStart}
          responsive={responsive}
         
          slideToIndex={currentIndex}
          mouseTrackingEnabled={true}
          touchTrackingEnabled={true}  
          swipeDisabled={false}  
          fadeOutAnimation={true}
          swipeDelta="100px"
          preventEventOnTouchMove={true}
          onSlideChanged={onSlideChanged}
          // onSlideChanged={tr}
        /> */}
<i  onClick={() => arr()} class="fas fa-chevron-right prevous-carosule"></i>
<i onClick={() => setcurrentIndex(currentIndex - 1)}  class="fas fa-chevron-left prevous-carosule1"></i>
 {/* <div style={{overflowX:"scroll",paddingBottom:"1rem"}}>
    <div className="d-flec">
      {
    featureCards.slice(0,5).map((item,i)=>
      {
      
        return(
          <div key = {i}  className="feature-carousel" 
       
          style={
            {
            backgroundImage:`url(${item.thamnail})`
            
            }
          }   >
            <div  className="black-iten"
            ></div>
           <div className="custom-sectionfeature">
          <i className={item.playbuttone}   onClick={(e)=>datashowvideo(item)} ></i>
          </div>
          <h1 className="fe-title">{item.title}</h1>
          </div>
        )
      })
    }
    </div> 
    </div> */}

    
      
<div className="feacture-model">
      <Modal className="feacture-model" show={Showvido} onHide={handleClosez} animation={false}  centered backdrop="static">
        <Modal.Header closeButton >
          <Modal.Title>
            <div className="featureheader">
              {/* <img className="imgsection-feature"src={Altass} alt="logo"/> */}
<div className="feature-videosectio">
{
  featureinfo.map(item=>
 {
   return(
<>


  <h1 className="altas-header">{item.content}</h1>
  <p className="feature-p">Series: {item.series}</p>

</>
 )})
}
</div>

            </div>
            
            </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        {/* {
    videofile:"https://www.youtube.com/watch?v=dSx6pcJBj9w",
    title: "Oppurtunity",
    thamnail:shorupan,
    playbuttone: "fab fa-youtube",
    content:"Atlas Founder Explains The Vision",
    series:"Compnay Overview"
  }, */}
{
  featureinfo.map(item=>
 {
   return(
<>
<div>
  {
    featureinfoV?
    <ReactPlayer className="vide-res" url={item.videofile} playing={featureinfoV} />
    :
    <>
<div className="top-riding">
<img className="btnm" src={Youtude} alt=""onClick={(e)=>datashowvideomodel()}/> 
</div>
<ReactPlayer className="vide-res" url={item.videofile} playing={false} />
</>
  }


</div>
</>
 )})
}

        {/* <ReactPlayer url='https://www.youtube.com/watch?v=ysz5S6PUM-U' playing /> */}
        </Modal.Body>
      
      </Modal>
      </div>
    </div>
</div>

    </>
  );
};

export default FeatureCards;
