import React, { useState, useContext, useEffect } from "react";
import Axios from "axios";
import { brokerage } from "../../Brokercontextapi/Contextapi";
import { Typography } from "antd";

const BrokerLandingIntro = () => {


  useEffect(() => {
    // Axios.get(
    //   `https://comms.globalxchange.com/coin/vault/user/license/get?email=${localStorage.getItem(
    //     "user_account"
    //   )}`
    // ).then(resp => {
    //   if (resp.data.licenses[0]) {
    //     if (resp.data.licenses[0].license_code) {
    //       setPromoCode(resp.data.licenses[0].license_code);
    //     }
    //   }
    // });
    return () => {};
  }, []);

  return (
    <div className="spacingthebroker">
      <div className="row intro-row-custom">
        <div className="col-md-12 p-0">
          <div className="intro-text-wrapper">
            <h5 className="custom-paragrap">
            Atlas Affiliates is a affiliate marketing opportunity that allows Atlas members to build a global business. All you have to do is be an active Atlas customer and then you can create access the Atlas Affiliate Portal.
            </h5>
            {/* <div className="sign-up-wrapper">
              <div className="submit-button" onClick={initiatePayment}>
                Get Broker Promo
                <span>
                  <i className="fa fa-long-arrow-right" aria-hidden="true"></i>
                </span>
              </div>
            </div> */}
          </div>
        </div>
    
      </div>
    </div>
  );
};

export default BrokerLandingIntro;
