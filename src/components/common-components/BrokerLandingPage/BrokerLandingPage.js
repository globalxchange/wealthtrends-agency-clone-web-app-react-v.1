import React, { useEffect } from "react";
// import SideNavigation from "../FixedNavigation/FixedNavigation";
import "./broker-landing-page.scss";
import BrokerLandingNav from "./BrokerLandingNav";
import FeatureCards from "./FeatureCards";
import BrokerLandingIntro from "./BrokerLandingIntro";
import BrokerLandingMainFeature from "./BrokerLandingMainFeature";
import BrokerLandingPartners from "./BrokerLandingPartners";
import BrokerLandingWhoCanBe from "./BrokerLandingWhoCanBe";
import BrokerLandingFAQ from "./BrokerLandingFAQ";
import BrokerLandingTestimonial from "./BrokerLandingTestimonial";
import './Broker.scss'

const BrokerLandingPage = () => {
  return (
    <div className="FixedSideBarWraperSection">
      {/* <SideNavigation /> */}
      <div
        id="scroll-wrapper"
        className="rightSidePartWraperSectioncustombroker"
      >
      
          <BrokerLandingNav />
          <BrokerLandingIntro />
           <FeatureCards />
     
  
      </div>
    </div>
  );
};

export default BrokerLandingPage;
