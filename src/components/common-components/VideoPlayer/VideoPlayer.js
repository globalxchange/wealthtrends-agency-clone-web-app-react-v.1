import React, { useState, useEffect } from 'react'
import './video-player.style.scss'
import Images from '../../../assets/a-exporter'
import {isMobile} from 'react-device-detect'
import ReactPlayer from 'react-player'
export default function VideoPlayer({ image, currentId,videoLink, selectedId }) {
    const [overCard, setOverCard] = useState(false);
    const [play, setPlay] = useState(false);

    useEffect(()=>{
        if(currentId !== selectedId)
            setPlay(false)
    },[selectedId])

    return (
        <div
            onMouseEnter={()=>isMobile?'':setOverCard(true)}
            onMouseLeave={()=>isMobile?'':setOverCard(false)}
            onClick={()=>isMobile?setOverCard(!overCard):''}
            style={{
                backgroundImage: `url('${image}')`,
                backgroundSize: "cover"
            }} className={"w-100 h-100 video-player"}>
            <ReactPlayer
                style={play ? { display: 'block' } : { display: 'none' }}
                onPause={() => setPlay(false)}
                onEnded={() => setPlay(false)}
                playing={play}
                width="100%"
                height="100%"
                url={videoLink}
                controls={true}
            />

            <div className={currentId === selectedId && overCard ? "video-overlay d-flex align-items-center" : "d-none"}>
                <div>
                    <button onClick={()=>setPlay(true)}><img src={Images.triangle} /></button>
                </div>
            </div>
        </div>
    )
}
