import React,{useEffect} from 'react'
import './skeleton.scss'
export default function Skeleton({style}) {
    return (
        <div style = {style} className="skeleton">
            
        </div>
    )
}
