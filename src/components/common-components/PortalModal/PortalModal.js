import React from 'react'
import ReactDOM from 'react-dom';
import './portal-modal.style.scss'
import Images from '../../../assets/a-exporter';
export default function PortalModal({ status = null, message = "Hello World", clickFunction = (param) => { } }) {
    const [localStatus, setLocalStatus] = React.useState(null);

    React.useEffect(()=>{
        setLocalStatus(status);
    },[status])
    React.useEffect(()=>{
        if(localStatus === null)return;
        let a = setTimeout(()=>{
            setLocalStatus(null);
            clearTimeout(a);
            clickFunction(null);
        },4000)
    },[localStatus])
    return ReactDOM.createPortal(
        <div className={`portal-modal ${!localStatus ? "failed" : "succeed"} ${localStatus !== null ? "show-modal" : ""}`}>
            <h6><b>Message:</b> {message}</h6>
            <img onClick={() => {setLocalStatus(null); clickFunction()}} src={Images.add} />
        </div>, document.body
    )
}
