import React, { useState, useContext, useEffect } from 'react'
import './asset-section.style.scss'
import AssetList from '../AssetList/AssetList'
import nextId from 'react-id-generator'
import { Agency } from '../../Context/Context';
import BondList from '../BondList/BondList';
// import { VideoPlayerPlugin } from '../../Dashboard/TradeStreamPlugin';
import VideoPlayerPlugin from '@nvest/trade-strean-videoplayer';
import Images from '../../../assets/a-exporter';
import NextSVG from './NextSVG';
import ComingSoon from './ComingSoon';
import MainAssetList from './MainAssetList';
import AgencyList from '../AgencyList/AgencyList';

export default function AssetsSectionComponent() {
    const [] = useState("id2");
    const [fullScreen, setFullScreen] = useState(false);
    const [currentList, setCurrentList] = useState([]);
    const [hideTradeStream, setHideStream] = useState(false)
    const [assetList, setAssetList] = useState(true)
    const agency = useContext(Agency);
    const { allMarketCoins, collapseSidebar, currentApp, moreOnApp, checkedId, setCheckedId } = agency

    const selectTable = (obj) => {
        switch (obj.field) {
            case "fiat":
            case "crypto":
            case "asset": return <AssetList list={allMarketCoins[obj.field]} myId={obj.id} active={obj.id === checkedId} fullScreen={fullScreen} setFullScreen={setFullScreen} />
            case "bonds":  return <BondList id={obj.id} fullScreen={fullScreen} active={obj.id === checkedId} setFullScreen={setFullScreen} />
            case "market": return <BondList id={obj.id} list={[]} fullScreen={fullScreen} active={obj.id === checkedId} setFullScreen={setFullScreen} />
            case "agency": return <AgencyList />
        }
    }
    useEffect(() => {
        switch (checkedId) {
            case 'id2': setCurrentList([allMarketCoins.crypto]); break;
            case 'id1': setCurrentList([allMarketCoins.fiat]); break;
            case 'id3': setCurrentList([allMarketCoins.asset]); break;
            case 'id4': setCurrentList([allMarketCoins.bonds]); break;
            default: return;
        }
    }, [checkedId])
    const titleObj = {
        id2: `Cryptocurrencies`,
        id1: `Fiat Currencies`,
        id3: `Interest Rates`,
        id4: `Investment Products `,
    }
    React.useEffect(() => {
        console.log("")
    }, [allMarketCoins])
    return (
        <div className="asset-section-wrapper">
            {/* <input type="checkbox" onChange={e => alert(e.target.checked)} /> */}
            <div className={`asset-section ${hideTradeStream ? "asset-goes-full" : ""} ${fullScreen ? "p-0" : "position-relative"}`}>
                <div
                    //  onClick={() => setHideStream(!hideTradeStream)}
                    className={`close-trade-stream ${hideTradeStream ? "trade-stream-open" : ""}`}>
                    {/* {hideTradeStream ? <img src={Images.TSLogo} /> : <NextSVG color="ff0000" />} */}
                    <button onClick={() => setAssetList(false)} className={assetList ? "" : "selected-type"}><NextSVG height="30px" width="30px" type="ts" turn={!assetList} /></button>
                    <button onClick={() => setAssetList(true)} className={!assetList ? "" : "selected-type"}><NextSVG height="30px" width="30px" type="asset" turn={assetList} /></button>

                </div>
                <div className="asset-section-header">
                    <h4>{titleObj[checkedId]}</h4>
                    <span>Supported by {currentApp.app_name}</span>
                </div>
                <div className={`asset-section-body ${fullScreen ? "position-relative p-0" : "position-relative"}`}>
                    {charts.map(obj => <input key={obj.keyId} onChange={() => console.log()} key={obj.id} className="d-none" checked={obj.id === checkedId} id={obj.id} type="radio" name="charts" />)}
                    <div className={`charts-wrapper ${fullScreen ? "" : "position-relative"}`}>
                        {
                            charts.map(obj =>
                                <label
                                    key={obj.keyId}
                                    onClick={() => setCheckedId(obj.id)}
                                    htmlFor={obj.id} id={obj.forId}
                                    style={fullScreen ? { top: 0, left: "-8.5%", width: "100%" } : collapseSidebar ? { width: "86%" } : {}}
                                    className={`chart-container position-absolute ${obj.id === checkedId ? "selected-section" : ""}`}
                                >
                                    <div className={obj.id !== checkedId ? "label-overlay" : "d-none"}>

                                    </div>
                                    {
                                        selectTable(obj)
                                    }
                                </label>)
                        }
                    </div>
                </div>
            </div>
            <div className={`trade-stream-wrapper ${hideTradeStream ? "trade-goes-null" : ""}`}>
                {/* <ComingSoon /> */}
                {
                    assetList ?
                        <MainAssetList />
                        :
                        <ComingSoon />
                }

            </div>
        </div>
    )
}
const charts = [
    { keyId: nextId(), forId: "cId1", id: "id1", field: "fiat" },
    { keyId: nextId(), forId: "cId2", id: "id2", field: "crypto" },
    { keyId: nextId(), forId: "cId3", id: "id3", field: "asset" },
    { keyId: nextId(), forId: "cId4", id: "id4", field: "bonds" },
    { keyId: nextId(), forId: "cId5", id: "id5", field: "market" },
    { keyId: nextId(), forId: "cId6", id: "id6", field: "agency" },
]
