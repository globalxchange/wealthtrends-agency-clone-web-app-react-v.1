import React from 'react'
import './main-asset-list.style.scss'
import Constant from '../../../json/constant';
import Images from '../../../assets/a-exporter';
import { Agency } from '../../Context/Context';
export default function MainAssetList() {
    const agency = React.useContext(Agency);
    const { assetSelected, setAssetSelected, handleAnalytics, currentApp, setAssetClassSelected, explainNavbarTerms,
        assetClassSelected, setSecondGate, setCheckedId, checkedId, currentUserDetails, setExplainNavbarTerm, collapseSidebar,
        setCollapseSidebar, netWorthController, setUserChatEnable, setGoToSettings } = agency;

    const handleClick = (obj) => {
        // alert(obj.id)
        if (checkedId === obj.id) {
            setSecondGate(false)
            setAssetSelected(null);
            setAssetClassSelected(null);
            handleAnalytics('')
        }
        else {
            if (assetClassSelected !== null) {
                setAssetSelected(obj)
                setSecondGate(true)
                setCheckedId(obj.id)

            } else {
                setSecondGate(true)
                setCheckedId(obj.id)

            }
            // setAssetSelected(obj)
        }
    }
    return (
        <div className="main-asset-list">
            <div className="main-asset-list-header">
                <h3>Asset Classes</h3>
                <p>Browse Through All The Asset Classes You Can Manage With {currentApp.app_name}</p>
            </div>
            <div className="main-asset-list-body">
                {
                    Constant.navbarList.map(obj => <div
                        onClick={() => handleClick(obj)}
                        className={checkedId === obj.id ? "selected-asset" : ""}
                    >
                        <img src={obj.icon} />

                    </div>
                    )
                }
            </div>
        </div>
    )
}
