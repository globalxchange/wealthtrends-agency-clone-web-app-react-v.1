import React from 'react'
import { Agency } from '../../Context/Context';
import Images from '../../../assets/a-exporter';
import VideoPlayerPlugin from '@nvest/trade-strean-videoplayer';

export default function ComingSoon() {
    const agency = React.useContext(Agency);
    const {moreOnApp} = agency
    return (
        <>
            <div className="trade-stream-coming-soon">
                <div>
                    <img src={moreOnApp?.whitetv_Logo} />
                    <p>Coming To A Screen Near You</p>
                </div>
                <span>Powered By <img src={Images.tradeStreamNew} /></span>
            </div>
            <VideoPlayerPlugin />

        </>
    )
}
