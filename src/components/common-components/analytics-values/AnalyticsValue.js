import React from 'react'
import Images from '../../../assets/a-exporter'
import './analytics.value.style.scss'
export default function AnalyticsValue({ image, positive, name, symbol, value }) {
    return (
        <div className="analytics-value">
            <div>
                <h6><img src={image} />{name}</h6>
            </div>
            <div>
                <h6><img src={positive?Images.greenTriangle :Images.redTriangle} /> <span className="value-span">{value}<span className="symbol-span">{symbol}</span></span> </h6>
            </div>
        </div>
    )
}
