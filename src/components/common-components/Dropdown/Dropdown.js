import React, { useState, useEffect, useContext } from 'react'
import './dropdown.style.scss'

import Constant from '../../../json/constant'
import Images from '../../../assets/a-exporter'
import { Agency } from '../../Context/Context'
export default function Dropdown({ title, list, status, onClick, onClickTwo, setDropdown, off, icon }) {
    const [listState, setStateList] = useState([])
    const agency = useContext(Agency);
    useEffect(() => {
        if (list === null) {
            return
        }
        setStateList([...Constant[list]]);
    }, [list, off]);


    return (
        <div style={{ paddingRight: "20px" }} className=" h-100 d-flex align-items-center  header-text">
            <img className="flag" alt="" src={icon} /><span >{title + `${agency.checkedId === "id3" ? " Earning" : ""} Vault`}</span>
            <span className="p-2" onClick={() => list === null || off ? '' : setDropdown()} style={off ? { visibility: "hidden" } : {}}><img className={list === null ? "d-none" : "arrow"} alt="" src={Images.triangle} /></span>
            <div className={`${status.status && title === status.title ? "table-dropdown" : "d-none"}`}>
                <h6 className="title">Filter By</h6>
                {
                    listState.map(obj => <p className="dropdown-item" onClick={() => onClick(obj)} key={obj.keyId}>{obj.name}</p>)
                }
            </div>
        </div>
    )
}
