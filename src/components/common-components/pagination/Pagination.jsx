import React, { useState, useEffect } from "react";
import "./pagination.style.scss";

export default function Pagination({ length = 5, activePage }) {
  const [numbers, setNumbers] = useState([]);
  const [selected, setSelected] = useState(1);

  useEffect(() => {
    let i = 2;
    let temp = [];
    for (i = 2; i < length; i++) {
      if (i > 5) break;
      temp = [...temp, i];
    }
    setNumbers([...temp]);
  }, [length]);

  useEffect(() => {
    activePage(selected);
  }, [selected]);

  const handleClick = (position, num) => {
    if (length <= 6) {
      setSelected(num);
      return;
    }
    if (position === 5) {
      if (num === 1) {
        setSelected(1);
        let temp = [2, 3, 4, 5];
        setNumbers([...temp]);
      } else {
        setSelected(length);
        let temp = [length - 4, length - 3, length - 2, length - 1];
        setNumbers([...temp]);
      }
    } else if (position === 0) {
      setSelected(num);
      if (selected >= 5) {
        let temp = numbers.map((x) => {
          return x - 1;
        });
        setNumbers([...temp]);
      } else {
        setNumbers([2, 3, 4, 5]);
      }
    } else if (position === numbers.length - 1) {
      setSelected(num);
      if (num === length - 1 || num > length - 1) return;
      let temp = numbers.map((x) => {
        return x + 1;
      });
      setNumbers([...temp]);
    } else {
      setSelected(num);
    }
  };
  return (
    <React.Fragment>
      <div className={length === 1 ? "d-none" : "pagination"}>
        <button
          onClick={() => handleClick(5, 1)}
          className={selected === 1 ? "selected" : ""}
        >
          1
        </button>
        <button className={numbers[0] === 2 ? "d-none" : ""} disabled>
          ...
        </button>
        {numbers.map((num, i) => (
          <button
            onClick={() => handleClick(i, num)}
            className={selected === num ? "selected" : ""}
            key={num}
          >
            {num}
          </button>
        ))}
        <button
          className={numbers[3] === length - 1 || length <= 5 ? "d-none" : ""}
          disabled
        >
          ...
        </button>
        <button
          onClick={() => handleClick(5, length)}
          className={selected === length ? "selected" : ""}
        >
          {length}
        </button>
      </div>
      <div className={length === 1 ? "pagination" : "d-none"}>
        <button className="selected">1</button>
      </div>
    </React.Fragment>
  );
}
