import React, {useContext,useState,useEffect} from 'react'
import {brokerage} from '../../Brokercontextapi/Contextapi'
import { Input, Select } from 'antd';
import './Altas.scss'
import fetch from "../../fetchurl";

import arrow from '../../../assets/ar.png'
import Fb from '../../../assets/fb.png'
import What from '../../../assets/desktop.svg'
import axios from "axios";
import {
    FacebookShareButton,
    FacebookIcon,
    WhatsappShareButton,
    WhatsappIcon,
  
  } from "react-share";
const { Option } = Select;
export default function SendFooter() {
    const {broker_name,handleatlasChange}= useContext(brokerage)
   

    return (
        <div className="atlasfooter">
           
   <div className="row">
       <div className="col-md-4 div-border-atlas">
<h1 className="footer-h1">Email</h1>
<p className="footer-p">Invite Friends</p>
<div>
<input className="Emailinput"  placeholder="Enter Your Friends Email" type="text"/>
<img  className="arr-footer" src={arrow} alt=""/>
</div>


       </div>


       <div className="col-md-4 div-border-atlas">
<h1 className="footer-h1">SMS</h1>
<p className="footer-p">Invite Friends</p>
<div>
<input className="Emailinput"  placeholder="Enter Your Friends Numbers" type="text"/>
<img  className="arr-footer" src={arrow} alt=""/>
</div>


       </div>

       <div className="col-md-4 div-border-atlas1">
<h1 className="footer-h1">Social</h1>
<p className="footer-p">Share Via Social</p>
<div>

    <>
    <div className="mainsection-social" >
<WhatsappShareButton url={"whatsapp://send?text="}>
    {/* <WhatsappIcon/> */}
    <div className="whatsappsection">
    <img  style={{width:" 1.6rem"}}src={What} alt=""/>
    </div>
  {/* {shareCount => <span className="myShareCountWrapper">{shareCount}</span>} */}
</WhatsappShareButton>

<FacebookShareButton url={"https://www.facebook.com/sharer/sharer.php?u="}>
    <div className="facebooksection">
    <img src={Fb} alt=""/>
    </div>
  {/* {shareCount => <span className="myShareCountWrapper">{shareCount}</span>} */}
</FacebookShareButton>
</div>
</>

{/* <input className="Emailinput"  placeholder="Enter Your Friends Numbers" type="text"/>
<img  className="arr-footer" src={arrow} alt=""/> */}
</div>


       </div>
       </div>   

        </div>
    )
}
