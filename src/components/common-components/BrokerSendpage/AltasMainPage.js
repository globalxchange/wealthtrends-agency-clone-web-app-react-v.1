import React,{useState,useContext,useEffect} from 'react'
import {brokerage}from '../../Brokercontextapi/Contextapi'
import AltasNav from './BrokerSendPage'
import { Tabs } from 'antd';
import Altasenddash from "./AltasSenddash"
import 'antd/dist/antd.css';
import AltasendFooter from './SendFooter'
import Geneology from './Geneology'
import { marketplaceCategories, academyCategories } from "../BrokerDashboard/filterCategories";
import GXTV from '../BrokerDashboard/DashboardFragments/GXTV/GXTV'
// import { StickyContainer, Sticky } from 'react-sticky';
import Contrller from '../BrokerDashboard/DashboardFragments/Controller/Controller'
// import Stats from "./DashboardFragments/Network/Stats";
import Stats from "../BrokerDashboard/DashboardFragments/Network/Stats";
import atlasdrp from "../../../assets/atlasdrp.png"
import OnOutsiceClick from 'react-outclick';
import AtlascrmEnter from "./AtlasCrmEnter"
const { TabPane } = Tabs;

// const renderTabBar = (props, DefaultTabBar) => (
//   <Sticky bottomOffset={80}>
//     {({ style }) => (
//       <DefaultTabBar {...props} className="site-custom-tab-bar" style={{ ...style }} />
//     )}
//   </Sticky>
// );
export default function AltasMainPage() {

  const [isMobile, setIsMobile,] = useState(window.innerWidth <= 500);
  const [searchQuery, setSearchQuery] = useState("");
  const [atlastabsub,setatlastabsub]=useState("")

  const {Crmfunctioncall,oustsideclickfun,subatlashandleAntTab,atlastabhandleAntTab,mainatlastab,atlastab,atlasbuttonclick,atlasdropdown,atlasNavshow,atladrphidefunction1} = useContext(brokerage)
    const handleAntTab=()=>
    {

    }
    const ref = React.useRef();


    useEffect(() => {
      Crmfunctioncall()
      return () => {
       
      }
    }, [])
  return (
    
    <div >
        <OnOutsiceClick container={ref}  onOutsideClick={atladrphidefunction1}>
            </OnOutsiceClick>   
      <>
      <div className="FixedSideBa]rWraperSection">
      {/* <SideNavigation/> */}
      <div style={{    width: "100%", overflow:"hidden"}}
        // id="scroll-wrapper"
        // className="rightSidePartWraperSectioncustom"
      >
      <AltasNav/>
<div>
{/* <StickyContainer> */}
<div className="MainTabSection"  ref={ref}>
    <Tabs defaultActiveKey={mainatlastab} activeKey={mainatlastab} onChange={(key) => {
              subatlashandleAntTab(key)}}>
      <TabPane tab="Share" key="Share" >
          <>
          <div>
        <Altasenddash/>
        <AltasendFooter/>
        </div>
        </>

      </TabPane>
    
      <TabPane tab="Earning" key="Earning">
        </TabPane>
      <TabPane tab="CRM" key="CRM">


        
        <div className="crmtab">
          
      <Tabs activeKey={atlastab}  onChange={(key) => {
              atlastabhandleAntTab(key)}}>
    <TabPane tab="Analytics" key="Analytics">
      <div className="overviewmainpage">
       <Contrller/>
       </div>
       </TabPane>
       <TabPane tab="Geneology" key="2">
       <Geneology/>
       </TabPane>
      <TabPane tab="User" key="3">
    
       <Stats tableToShow={"Users"}/>
  
       </TabPane>
 
       <TabPane tab="Cutomers" key="4">
     
      <Stats tableToShow={"Customers"}  />

       </TabPane>

       <TabPane tab="Affiliates" key="5">
     
    <Stats tableToShow={"Brokers"}  />
     
       </TabPane>
       <TabPane tab="Team" key="6">
     
<h1>Coming Soom</h1>
      
        </TabPane>
  
      </Tabs>
    <div>
      <img className="imgcustonarrow" src={atlasdrp} alt="" onClick={atlasdropdown}/>


      {atlasNavshow?<>
   <div className="custom-arrow"></div>
      <div className="crmtaboverflow">
      <OnOutsiceClick container={ref}  onOutsideClick={oustsideclickfun}>
     
    <Tabs tabPosition="right"  activeKey={atlastab} onChange={(key) => {atlastabhandleAntTab(key)}}>
          <TabPane tab="More CRM Options" key="7">
            comiing soon
          </TabPane>
          <TabPane tab="Tab 2" key="8">
          comiing soon
          </TabPane>
          <TabPane tab="Tab 3" key="9">
          comiing soon
          </TabPane>
        </Tabs>
        </OnOutsiceClick>
    </div>
   
     </>:""}
     </div>
        {
     atlasbuttonclick?""
:
<AtlascrmEnter/>
 }


       </div>
      </TabPane>
  
      
      <TabPane tab="TV" key="3">
   <div className="atlastvmain">
   <GXTV
          isMobile={isMobile}
          categories={academyCategories}
          searchQuery={searchQuery}
        />
   </div>
      </TabPane>
      <TabPane tab="Marketing" key="4">
       comming soon
      </TabPane>
   
      <TabPane tab="Account" key="5">
       comming soon
      </TabPane>
    </Tabs>
 <div>
 
   
 </div>
  {/* </StickyContainer>   */}
</div>
</div>
</div>
</div>
      </>
    </div>
  )
}
