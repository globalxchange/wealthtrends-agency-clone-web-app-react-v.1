import React, {useContext,useState,useEffect} from 'react'
import {brokerage} from '../../Brokercontextapi/Contextapi'
import { Input, Select } from 'antd';
import './Altas.scss'
import fetch from "../../fetchurl";
import Search from '../../../assets/search.png'
import USD from '../../../assets/USD.png'
import Drp from '../../../assets/drps.png'
import axios from "axios";
import {Altasdrop} from './AtlasdropApi'
import OnOutsiceClick from 'react-outclick';
const { Option } = Select;
export default function BrokerSendPage() {
    const {CrmDataName,dropdownlistAtlass,broker_name,atladrphidefunction,atlasdrpclick,atladrphidefunction1,handleatlasChange,atlasnavigationdata,SelectedAtlasDropname,atlastoggle,listingitemsatlas,atlasdropfunction,handleatlasChangefliterAtlas}= useContext(brokerage)
    const [loading, setisloading] = useState(false);
    const [total, settotal] = useState(0.00);
    const dropdownlist=[
        {
        name:"Share",
        },
           {
           name:"CRM",
            },
            {
                name:"Marketing",
                },
                {
                    name:"Tv",
                    },
                    {
                        name:"Account",
                        },
    ]

    
    const fetchBalances = async (coin)=>{
        setisloading(true)
        let data = await axios
          .get(
            `${fetch.url}coin/fiat/fiat_balances?email=${localStorage.getItem(
              "userEmail"
            )}&convert=USD`
          );
         
    
          if(data.data.status){
    
                settotal(Number(data.data.total_sum.toFixed(2)))
                setisloading(false)
              }
        
    
          
          else{
            setisloading(false)
          }
    
      }
  
    useEffect( ()=>{
        fetchBalances() 
   
     }, [] );
  



    
   
     const ref = React.useRef();

    const selectAfter = (
       
       <Select className="kamal" defaultValue={SelectedAtlasDropname}className="select-after" onChange={value => atlasdropfunction(value)}>
        
       
        {
            dropdownlistAtlass.map(item=>
                {

                    return(
                        <Option value={item.name}>{item.name}</Option>

                    )
                })
        }
        
        </Select>
      );
    return (
        <div className="atlasnavbar" >
            <div className="sendmain">
            <h1 className="h1namealtas">{broker_name !== "" ? `${broker_name}'s` : ""}   </h1>
            <p className="pnamealtas">Atlas Affiliate Portal</p>
            <div    className="searchMainaltas">
            <Input name="handleatlasChange" className="inputheightatlas" addonAfter={selectAfter} placeholder={atlasdrpclick} onChange={value => handleatlasChangefliterAtlas(value)} onClick={atladrphidefunction} />
           
           <span className="span-drp-img"><img className="img-drp" src={Drp} alt=""/></span>
            <span className="spanseachimage"><img src={Search} alt=""/></span>
         
{
    atlastoggle?

            <div className="dropdown-list-main">
                {
                    atlasnavigationdata.length>0?
                <>
{
atlasnavigationdata.map(item=>

    {


    return(
        <p className="listofdropdowns" onClick={()=>listingitemsatlas(item)} >{item.name}</p>
    ) })
    }
    </>
    :
    <div class="text-vsa">
    <span class="letter r">N</span>
    <span class="letter b">o</span>
    <span class="letter o">M</span>
    <span class="letter g">a</span>
    <span class="letter p">t</span>
    <span class="letter o">c</span>
    <span class="letter b">h</span>
    <span class="letter p">F</span>
    <span class="letter r">o</span>
    <span class="letter r">u</span>
    <span class="letter o">n</span>
    <span class="letter b">d</span>
    <span class="letter p">.</span>
    <span class="letter r">.</span>
    <span class="letter b">.</span>
  </div>

}

   

            </div>

:null}
{/*   
  <OnOutsiceClick container={ref}  onOutsideClick={atladrphidefunction1}>
            </OnOutsiceClick>         */}
        </div>
        </div>
        <div className="main-subnav">
    <div className="withdrawsection1">
        <h1 className="atlas-img-valueh1">180</h1>
       <p className="atlas-img-value"> Personal Volume</p>
        <label className="altas-withdrow" >Renew</label>
    </div>
    <div className="withdrawsection">
        <h1 className="atlas-img-valueh1">${total}</h1>

        <p className="atlas-img-value">
            <span className="atlas-img-value-span"><img src={USD} alt="logo"/></span>  EARNINGS
        </p>
        <label className="altas-withdrow" >Withdraw</label>
    </div>

    </div>

        </div>
    )
}
