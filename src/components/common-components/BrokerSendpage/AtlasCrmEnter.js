import React,{useState,useContext,useEffect} from 'react'
import {brokerage}from '../../Brokercontextapi/Contextapi'
import {Atlascrm,dayListFull,dayListFullsmallletter,} from './AtlasCrmEnterApi'

export default function AtlasCrmEnter() {
    const {cardfunction,CrmData,CrmDataName} = useContext(brokerage)


    return (
<div>
<div className="row m-0 ">


            <div className="col-md-6 firstcrmcontiner">
            {
                CrmData.length>0?
                <>
                <div className="selectbroker">
                    {
                       CrmData.length>=2?
                       <div>
                       <h1 className="textaligmcrm">Select One Of Your {dayListFull[CrmData.length]} CRM’s To Analyze</h1>
                     <p className="patlascrm">You are apart of {dayListFullsmallletter[CrmData.length]} compensation structures as an Atlas Affiliate. Each structure has its own CRM interface fo you to use.</p>
                       </div>
                    :
                    <>
                     <div>
                       <h1 className="textaligmcrm">Welcome To Your {CrmDataName}s CRM</h1>
                     <p className="patlascrm">User the Atlas Affiliate CRM to analyze and keep up to date with your business. We have combined the latest sales and marketing technology to get you ahead.</p>
                       </div>
                    </>
                       
                    }

                 
                </div>
                </>
                :null
            }
            </div>
        <div className="col-md-6 altas-card-rw"> 
         <div className="atlascrmcardsmain">
            {
                CrmData.length>0?

            <>
            {
                CrmData.map(item=>
                    {
                        return(
                            <div className="atlascard">
                                <div className="atlercrmtitlesectiom" style={{background:item.colors[0]}}>
                                <img src={item.logo} className="crd-img-s" alt=""/>
                                </div >

                                <div className="subcardcontainers">
                                <h2 className="atlercrmnumber">{item.levels}</h2>
                                <p className="levelsormembers">Levels</p>
                             <h2 className="atlercrmnumber">{item.users}</h2> 
                             <p className="levelsormembers"> Members</p>
                             <label className="enterlabel"  onClick={()=>cardfunction(item)}>Enter</label>
                                </div>
                           
                            </div>
                           
                        )
                    })
            }
            </>
            :null
            }
        </div>
        </div>
        </div>
</div>
    )
}
