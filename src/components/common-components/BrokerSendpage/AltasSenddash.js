import React,{useState} from 'react'
import Meeting from '../../../assets/meeting.png'
import Group from '../../../assets/Group.png'
import Group1 from '../../../assets/Group1.png'
import {CopyToClipboard} from 'react-copy-to-clipboard';

export default function AltasSenddash() {


    const [value, setvalue] = useState( `signup.atlasandbeyond.app/${localStorage.getItem("uusseerrname")}`)
    const [copied,  setcopied]=    useState(false)
 
  return (
    <div className="sectiondevalign">
      <div className="container main-seb-div">
<div>
    <h1 className="dashtitle">Follow These Three Steps</h1>
    <p className="atlassubtitle">To Build Your Atlas Business Today</p>
</div>


<div className="altas-dash">
    <div>
        <img className="searchdashimg" src={Group} alt=""/>
        <p className="atlasp">Register</p>
    </div>
    <div>
        <img  className="searchdashimg"src={Meeting} alt=""/>
        <p className="atlasp1">Share</p>
    </div>
    <div>
        <img className="searchdashimg" src={Group1} alt=""/>
        <p className="atlasp1">Earn</p>
    </div>

    <div className="copy-main-section">
        <input className="inputcopy" value={value} readonly="readonly" 
           />
{/*  
        <CopyToClipboard text={this.state.value}
          onCopy={() => this.setState({copied: true})}>
          <span>Copy to clipboard with span</span>
        </CopyToClipboard> */}
 
        <CopyToClipboard text={value}
          onCopy={() => setcopied(true)} >
          <button className="copybutton">{copied ?"Copied":"Copy URL"}</button>
        </CopyToClipboard>
 
       
        <div>
        <p className="altas-main-p">Change Half Back Of Link</p>
        </div>
      </div>
</div>
      </div>
    </div>
  )
}
