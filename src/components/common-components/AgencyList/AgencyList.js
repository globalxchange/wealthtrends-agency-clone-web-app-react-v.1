import React from 'react'
import './agency-list.style.scss'
import { Agency } from '../../Context/Context';
import nextId from 'react-id-generator';
import Images from '../../../assets/a-exporter';
export default function AgencyList() {
    const agency = React.useContext(Agency);
    const { setCollapseSidebar, setDifferentiator, setUserChatEnable, setExternalAction, currentApp } = agency;
    const handleClick = () => {
        setExternalAction("invest")
        setCollapseSidebar(true)
        setUserChatEnable(true)
        setDifferentiator("bot")
    }
    return (
        <div className="agency-list-main">
            <div className="agency-list-main-header">
                <div className="agency-list-main-header-left">
                    <h5><img src={currentApp.app_icon} />{currentApp.app_name}</h5>
                    <p>Exchange Token</p>
                </div>
                <div className="agency-list-main-header-right">
                    {
                        [{ name: "My Ownership", disable: true }, { name: "Offerings", disable: false }].map(obj => <button className={obj.disable?"disable-this":""}>{obj.name}</button>)
                    }

                </div>
            </div>
            <div className="agency-list-main-body">
                {
                    classL.map(obj => <div onClick={() => handleClick()} key={obj.keyId} className="agency-list-row">
                        <img src={obj.icon} />
                        <div>
                            <h5>{obj.name}</h5>
                            <p>{obj.text}</p>
                        </div>
                    </div>
                    )
                }
            </div>
        </div>
    )
}
const classL = [
    { keyId: nextId(), name: "Class A", text: "Offered In Bitcoin", icon: Images.classA },
    { keyId: nextId(), name: "Class B", text: "Offered In Bitcoin", icon: Images.classB },
]