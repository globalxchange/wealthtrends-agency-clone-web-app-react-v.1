import React from "react";

const HistoryItem = ({ img, title, time, amount }) => {
  const amountFormatter = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  });
  return (
    <div className="d-flex assets-item card m-2 p-2">
      <div className="col-md-5 d-flex flex-row">
        <img src={img} alt="" className="img-coin my-auto mx-2" />
        <div className="title my-auto">{title}</div>
      </div>
      <div className="col-md-4 d-flex flex-row">
        <h5 className="value my-auto">{time}</h5>
      </div>
      <div className="col-md-3 d-flex flex-row">
        <h4 className="my-auto">${amountFormatter.format(amount)}</h4>
      </div>
    </div>
  );
};

export default HistoryItem;
