import React, { useState } from "react";
import { Paper } from "@material-ui/core";
import Card from "react-credit-cards";

// // note that you can also export the source data via CountryRegionData. It's in a deliberately concise format to
// // keep file size down
// import {
//   CountryDropdown,
//   RegionDropdown,
//   CountryRegionData
// } from "react-country-region-selector";

const AddCardForms = () => {
  const [maxLength, setMaxLength] = useState(19);

  const [cardNumber, setCardNumber] = useState("");

  const cardNumberValidator = e => {
    var str = e.target.value
      .replace(/ /g, "")
      .replace(/[^0-9]/g, "")
      .substring(0, maxLength)
      .replace(/(.{4})/g, "$1 ")
      .trim();
    setCardNumber(str);
  };

  const [expiry, setExpiry] = useState("");
  const validateExpiry = e => {
    var str = e.target.value.replace(/ /g, "").replace(/[^0-9]/g, "");
    var num = parseInt(str);
    if ((num > 1 && num < 10) || (num > 12 && num < 20)) {
      str = "0" + num;
    }
    str = str
      .substring(0, 4)
      .replace(/(.{2})/g, "$1/")
      .replace(/\/$/, "");
    setExpiry(str);
  };

  const [cvv, setCvv] = useState("");
  const validateCvv = e => {
    var str = e.target.value
      .replace(/ /g, "")
      .replace(/[^0-9]/g, "")
      .substring(0, 3);
    setCvv(str);
  };

  const [focused, setFocused] = useState("");

  const handleInputFocus = ({ target }) => {
    setFocused(target.name);
  };

  const [fname, setFname] = useState("");
  // const [sname, setSname] = useState("");
  // const [addrs1, setAddrs1] = useState("");
  // const [addrs2, setAddrs2] = useState("");
  // const [country, setCountry] = useState("");
  // const [city, setCity] = useState("");
  // const [region, setRegion] = useState("");
  // const [pin, setPin] = useState("");
  // const [additionalInfo, setAdditionalInfo] = useState("");
  const [cardType, setCardType] = useState("fas fa-2x fa-credit-card");

  const [isValidCard, setIsValid] = useState(false);

  const handleCallback = (values, isValid) => {
    setMaxLength(values.maxLength);
    setIsValid(isValid);
    console.log(values, isValid);
    switch (values.issuer) {
      case "visa":
      case "visaelectron":
        setCardType("fab fa-2x fa-cc-visa");
        break;
      case "mastercard":
      case "maestro":
        setCardType("fab fa-2x fa-cc-mastercard");
        break;
      case "dinersclub":
        setCardType("fab fa-2x fa-cc-diners-club");
        break;
      case "amex":
        setCardType("fab fa-2x fa-cc-amex");
        break;
      case "jcb":
        setCardType("fab fa-cc-jcb");
        break;
      case "discover":
        setCardType("fab fa-cc-discover");
        break;
      default:
        setCardType("fas fa-2x fa-credit-card");
    }
  };

  return (
    <div className="cards-form d-flex flex-column">
      <div className="add-card pt-4 pb-1">
        <Paper>
          <div className="p-4">
            <Card
              number={cardNumber}
              name={fname}
              expiry={expiry}
              cvc={cvv}
              focused={focused}
              callback={handleCallback}
            />
          </div>
          <form className="p-4 cards-form">
            {/* ref={c => (this.form = c)} onSubmit={this.handleSubmit}> */}
            <Paper elevation={3}>
              <div className="p-3">
                <div className="form-group input-ccnum">
                  <input
                    type="tel"
                    name="number"
                    className="form-control"
                    placeholder="Card Number"
                    pattern="[\d| ]{12,22}"
                    required
                    onChange={cardNumberValidator}
                    onFocus={handleInputFocus}
                    value={cardNumber}
                    // onBlur={}
                  />
                  <i className={cardType} />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    name="name"
                    className="form-control"
                    placeholder="Cardholder's Name"
                    required
                    onChange={e => {
                      setFname(e.target.value);
                    }}
                    onFocus={handleInputFocus}
                    value={fname}
                  />
                </div>
                <div className="row">
                  <div className="col-6">
                    <input
                      type="tel"
                      name="expiry"
                      className="form-control"
                      placeholder="MM/YY"
                      pattern="\d\d/\d\d"
                      required
                      onChange={validateExpiry}
                      onFocus={handleInputFocus}
                      value={expiry}
                    />
                  </div>
                  <div className="col-6">
                    <input
                      type="tel"
                      name="cvc"
                      className="form-control"
                      placeholder="CVC"
                      pattern="\d{3,4}"
                      required
                      onChange={validateCvv}
                      onFocus={handleInputFocus}
                      value={cvv}
                    />
                  </div>
                </div>
                {/* <input type="hidden" name="issuer" value={issuer} /> */}
                <div className="form-actions">
                  <button className="btn btn-darkblue btn-block">
                    NEXT STEP
                  </button>
                </div>
                <div className="form-actions">
                  <button className="btn btn-outline-darkblue btn-block">
                    CANCEL
                  </button>
                </div>
              </div>
            </Paper>
          </form>
        </Paper>
      </div>
    </div>
  );
};

export default AddCardForms;
