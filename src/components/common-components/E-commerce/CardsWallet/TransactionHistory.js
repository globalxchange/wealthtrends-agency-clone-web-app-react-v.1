import React, { useState, useEffect } from "react";
import dollar from "./static/images/dollar.svg";
import Invoice from "../SavedPaymentMethods/Invoice";
import HistoryItem from "./HistoryItem";

const TransactionHistory = ({ isTransact }) => {
  console.log("isTransact =>", isTransact);
  const [isIncome, setIsIncome] = useState(true);
  const [incomeSelected, setIncomeSelected] = useState(
    "option option--one selected"
  );
  const [expenseSelected, setExpenseSelected] = useState("option option--two");
  useEffect(() => {
    if (isIncome) {
      setIncomeSelected("option option--one selected");
      setExpenseSelected("option option--two");
    } else {
      setIncomeSelected("option option--one");
      setExpenseSelected("option option--two selected");
    }
  }, [isIncome]);

  const income = [
    {
      img: dollar,
      title: "Salary",
      time: "4:00",
      amount: 5500
    },
    {
      img: dollar,
      title: "Bonus",
      time: "12:00",
      amount: 50
    }
  ];

  const expense = [
    {
      img: dollar,
      title: "Cofee",
      time: "5:00",
      amount: 5
    },
    {
      img: dollar,
      title: "Uber",
      time: "11:00",
      amount: 12
    }
  ];

  return (
    <>
      {isTransact ? (
        <Invoice />
      ) : (
        <div className="card your-assets d-flex flex-column h-100 my-0">
          <div className="tab-container">
            <div
              className={incomeSelected}
              tabIndex={0}
              onClick={() => setIsIncome(true)}
            >
              Payments
            </div>
            <div
              className={expenseSelected}
              tabIndex={0}
              onClick={() => setIsIncome(false)}
            >
              Purchases
            </div>
          </div>
          <div className="d-none d-md-flex assets-item assets-item-head px-3 pb-3 text-left">
            <div className="col-md-5">Expense</div>
            <div className="col-md-4">Time</div>
            <div className="col-md-3">Amount</div>
          </div>
          <div className="assets-wrapper">
            {isIncome
              ? income.map(({ img, title, time, amount }) => {
                  return (
                    <HistoryItem
                      img={img}
                      title={title}
                      time={time}
                      amount={amount}
                      income={true}
                    />
                  );
                })
              : expense.map(({ img, title, time, amount }) => {
                  return (
                    <HistoryItem
                      img={img}
                      title={title}
                      time={time}
                      amount={amount}
                    />
                  );
                })}
          </div>
        </div>
      )}
    </>
  );
};

export default TransactionHistory;
