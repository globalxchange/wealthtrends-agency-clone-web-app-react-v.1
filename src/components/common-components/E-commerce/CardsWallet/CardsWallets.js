import React from "react";
import Card from "react-credit-cards";
import "./static/scss/cards-wallet.scss";

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import TransactionHistory from "./TransactionHistory";
import useWindowDimensions from "./WindowSize";

const CardsWallet = ({ isTransact, setIsTransact, pos, setPos }) => {
  const cards = [
    {
      name: "John Doe",
      number: "**** **** **** 0123",
      expiry: "10/25",
      issuer: "mastercard"
    },
    {
      name: "John Doe",
      number: "**** **** **** 4566",
      expiry: "10/35",
      issuer: "visa"
    },
    {
      name: "John Doe",
      number: "**** **** **** 9823",
      expiry: "10/25",
      issuer: "mastercard"
    },
    {
      name: "John Doe",
      number: "**** **** **** 0496",
      expiry: "10/25",
      issuer: "visa"
    },
    {
      name: "John Doe",
      number: "**** **** **** 0009",
      expiry: "10/25",
      issuer: "amex"
    },
    {
      name: "John Doe",
      number: "**** **** **** 0666",
      expiry: "10/25",
      issuer: "mastercard"
    }
  ];

  const { width } = useWindowDimensions();

  return (
    <div className="cards-wallet d-flex flex-column">
      <div className="card-style">
      <h1>Coming Soon</h1>
      </div>
      {/* <Carousel
        showThumbs={false}
        showStatus={false}
        showIndicators={false}
        centerMode={width > 767 || cards.length === 1}
        centerSlidePercentage={cards.length === 1 ? 100 : 50}
        infiniteLoop
        selectedItem={pos}
        onChange={x => {
          setPos(x);
        }}
      >
        {cards.map(({ name, number, expiry, issuer }, index) => (
          <div
            className="cc-container py-5"
            key={number}
            onClick={() => {
              if (pos === index) setIsTransact(!isTransact);
              else setPos(index);
            }}
          >
            <Card
              name={name}
              number={number}
              expiry={expiry}
              preview={true}
              issuer={issuer}
            />
          </div>
        ))}
      </Carousel>
      <div className="trans-history h-100">
        <TransactionHistory isTransact={isTransact} />
      </div> */}
    </div>
  );
};

export default CardsWallet;
