import React from "react";

import paypal from "./static/images/logos/paypal.svg";
import paypal_rect from "./static/images/logos/paypal-rect.svg";
import bankofamerica from "./static/images/logos/bankofamerica.svg";
import bankofamerica_rect from "./static/images/logos/bankofamerica-rect.svg";
import barclays from "./static/images/logos/barclays.svg";
import barclays_rect from "./static/images/logos/barclays-rect.svg";
import binance from "./static/images/logos/binance.svg";
import binance_rect from "./static/images/logos/binance-rect.svg";
import bitfinex from "./static/images/logos/bitfinex.svg";
import bitfinex_rect from "./static/images/logos/bitfinex-rect.svg";
import coinbase from "./static/images/logos/coinbase.svg";
import paytm from "./static/images/logos/paytm.svg";
import td_canada from "./static/images/logos/td-canada-trust.svg";
import td_canada_rect from "./static/images/logos/td-canada-trust-rect.svg";
import zelle from "./static/images/logos/zelle.svg";
import fxbg from "./static/images/fx-bg.jpg";
import gxbg from "./static/images/gx-bg.jpg";
import fx_rect from "./static/images/logos/fx-rect.svg";
import gx_rect from "./static/images/logos/gx-rect.svg";

import bgmm from "./static/images/bg-coin.svg";
import bgtf from "./static/images/bg-graph.svg";
import mm from "./static/images/logos/mm-rect.svg";
import tf from "./static/images/logos/tf-rect.svg";
import pay from "./static/images/pay-btn.svg";

function CustomCardItem({ type, setIsTransact }) {
  const types = {
    paypal: {
      img: paypal,
      rect: paypal_rect,
      style: {
        background:
          "radial-gradient(circle, rgba(39,144,195,1) 5%, rgba(35,47,96,1) 100%)"
      }
    },
    bankofamerica: {
      img: bankofamerica,
      rect: bankofamerica_rect,
      style: {
        background:
          "linear-gradient(138deg, rgb(226, 23, 62) 10%, rgb(51, 102, 204) 116%)"
      }
    },
    barclays: {
      img: barclays,
      rect: barclays_rect,
      style: {
        background:
          "linear-gradient(130deg, rgba(32,196,244,1) 37%, rgba(0,110,152,1) 92%)"
      }
    },
    binance: {
      img: binance,
      rect: binance_rect,
      style: {
        background:
          "radial-gradient(circle, rgba(248,214,131,1) 9%, rgba(243,186,47,1) 45%)"
      }
    },
    bitfinex: {
      img: bitfinex,
      rect: bitfinex_rect,
      style: {
        background:
          "radial-gradient(circle, rgba(151,197,84,1) 10%, rgba(112,155,48,1) 79%)"
      }
    },
    coinbase: {
      img: coinbase,
      rect: coinbase,
      style: {
        background:
          "linear-gradient(130deg, rgba(142,199,231,1) 11%, rgba(0,129,201,1) 52%)"
      }
    },
    paytm: {
      img: paytm,
      rect: paytm,
      style: {
        background:
          "linear-gradient(45deg, rgb(5, 45, 114) 18%, rgb(1, 186, 242) 71%)"
      }
    },
    td_canada: {
      img: td_canada,
      rect: td_canada_rect,
      style: {
        background:
          "radial-gradient(circle, rgba(211,239,219,1) 0%, rgba(48,180,87,1) 52%)"
      }
    },
    zelle: {
      img: zelle,
      rect: zelle,
      style: {
        background:
          "radial-gradient(circle, rgba(171,124,230,1) 0%, rgba(109,29,212,1) 59%)"
      }
    },
    fx: {
      img: "",
      rect: fx_rect,
      style: {
        backgroundImage: `url(${fxbg})`,
        backgroundSize: "cover"
      }
    },
    gx: {
      img: "",
      rect: gx_rect,
      style: {
        backgroundImage: `url(${gxbg})`,
        backgroundSize: "cover"
      }
    },
    mm: {
      img: "",
      rect: mm,
      style: {
        backgroundImage: `url(${bgmm})`,
        backgroundSize: "cover"
      }
    },
    tf: {
      img: "",
      rect: tf,
      style: {
        backgroundImage: `url(${bgtf})`,
        backgroundSize: "cover"
      }
    }
  };
  const card = types[type];
  return (
    <div className="cc-container py-5">
      <div className="card cu-card" style={card.style}>
        <div
          className="pay-wrap"
          onClick={() => {
            setIsTransact(true);
          }}
        >
          <img
            src={pay}
            alt=""
            className="pay"
            onClick={() => {
              setIsTransact(true);
            }}
          />
        </div>
        <img src={card.img} alt="" className="center-logo" />
        <img src={card.rect} alt="" className="rect" />
      </div>
    </div>
  );
}

export default CustomCardItem;
