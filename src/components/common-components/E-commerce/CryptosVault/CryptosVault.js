import React, { useEffect, useState,useContext } from "react";

import "./sass/crypto-vault.scss";
import VaultAssets from "./VaultAssets";
import useWindowDimensions from "../CardsWallet/WindowSize";
import { Carousel } from "react-responsive-carousel";
import CustomCardItem from "../CardsWallet/CustomCardItem";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Invoice from "../SavedPaymentMethods/Invoice";
import { brokerage } from "../../../Brokercontextapi/Contextapi";
const CryptosVault = ({ isTransact, setCoin, pos, setPos, posTab }) => {
  const { width } = useWindowDimensions();

  const cards = ["gx", "fx", "mm", "tf"];
  const [open, setOpen] = useState(false);
  const {
    cust
  } = useContext(brokerage);

  // useEffect(() => {
  //   setIsTransact(false);
  // }, [pos]);

  const toggle = () => {
    setOpen(!open);
  };

  return (
    <div className="crypto-vault">
      <Carousel
        showThumbs={false}
        showStatus={false}
        showIndicators={false}
        centerMode={width > 767 || cards.length === 1}
        centerSlidePercentage={cards.length === 1 ? 100 : 50}
        infiniteLoop
        selectedItem={pos}
        onChange={x => {
          setPos(x);
        }}
      >
        {cards.map((card, i) => (
        
          <div
            key={card}
            onClick={() => {
              setPos(i);
              
            }}
          >
            <CustomCardItem type={card} setIsTransact={toggle} />
          </div>
        ))}
      </Carousel>
      {isTransact ? (
        
        <div className="your-assets crypto-assets">
          {cards[pos]==="mm" ||cards[pos]==="tf" ?
         <h1>Coming Soon</h1>
         :
         <Invoice posTab={posTab} pos={pos} setCoin={setCoin} /> 
         }
         </div>
      ) : (
        <div>
       
        <VaultAssets type={cards[pos]} open={false} />
        
      
      </div>
      )}
    </div>
  );
};

export default CryptosVault;
