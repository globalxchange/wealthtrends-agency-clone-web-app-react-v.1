import React, { useState, useEffect, useContext } from "react";
import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import { brokerage } from "../../../Brokercontextapi/Contextapi"
// import QrCodeScan from "../my-wallet/QRCodeScan";

function Withdraw({
  show,
  setShow,
  cryptoImage,
  cryptoTitle,
  cryptoCode,
  balanceUSD,
  balanceCrypto
}) {
  const {
    btc_address,
    eth_address,
    email,
    login_email,
    affiliate_id
  } = useContext(brokerage);

  const amountFormatter = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  });
  const amountFormatterCrypto = new Intl.NumberFormat("en-US", {
    minimumSignificantDigits: 5,
    maximumSignificantDigits: 5
  });

  const [rate, setRate] = useState(0);

  const [error_msg, setError_msg] = useState("");

  const [first, setFirst] = useState(null);
  useEffect(() => {
    axios
      .post("https://comms.globalxchange.com/coin/get_all_prices")
      .then(r => {
        var res = r.data;
        console.log("res =>:", res);
        switch (cryptoCode) {
          case "BTC":
            setRate(res.Data[0].price);
            break;
          case "GXT":
            setRate(res.Data[4].price);
            break;
          case "USDT":
            setRate(res.Data[5].price);
            break;
          case "ETH":
            setRate(res.Data[3].price);
            break;
          // case "SEF":setRate(res.)
          default:
            setRate(0);
        }
      })
      .catch(err => {
        console.log("res =>:", err);
        setFirst(0);
      });
  }, [cryptoCode, first]);

  const useStyles = makeStyles(theme => ({
    paper: {
      position: "absolute",
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      transform: "translateY(-50%)",
      width: "500px",
      maxWidth: "85%",
      top: "50%",
      left: "0",
      right: "0",
      margin: "auto"
    }
  }));

  const classes = useStyles();

  const maxUsd = e => {
    e.preventDefault();
    setFocused("amount-usd");
    setAmountUsd(balanceUSD);
  };

  const [amountUsd, setAmountUsd] = useState("");
  const [amountCrypto, setAmountCrypto] = useState("");
  const [cryptoAdress, setCryptoAdress] = useState("");

  const [focused, setFocused] = useState("");

  const handleInputFocus = ({ target }) => {
    setFocused(target.name);
  };

  const onChangeVaidator = e => {
    const re = new RegExp(/^\d*\.?\d*$/);
    if (e.target.value === "" || re.test(e.target.value)) {
      setAmountUsd(e.target.value);
    }
  };

  const onChangeVaidatorCry = e => {
    const re = new RegExp(/^\d*\.?\d*$/);
    if (e.target.value === "" || re.test(e.target.value)) {
      setAmountCrypto(e.target.value);
    }
  };

  useEffect(() => {
    if (focused === "amount-usd") {
      if (rate !== 0) {
        setAmountCrypto(amountUsd / rate);
      }
    }
  }, [amountUsd, rate]);

  useEffect(() => {
    if (focused === "crypto-val") {
      setAmountUsd(amountCrypto * rate);
    }
  }, [amountCrypto, rate]);

  const withdrawCrypto = e => {
    e.preventDefault();
    if (parseFloat(amountUsd) !== 0) {
      if (cryptoCode === "BTC" || cryptoCode === "ETH") {
        var from;
        if (cryptoCode === "BTC") {
          from = btc_address;
        } else if (cryptoCode === "ETH") {
          from = eth_address;
        }
        axios
          .post("https://comms.globalxchange.com/coin/transfer/placeWithdraw", {
            email: email,
            affiliate_id: affiliate_id,
            login_email: login_email,
            token: localStorage.getItem("token"),
            coin: cryptoCode,
            amount: amountCrypto,
            usd_value: amountUsd,
            from: from,
            to: cryptoAdress
          })
          .then(r => {
            console.log("placeWithdraw", r);
            setError_msg(r.data.message);
          })
          .catch(err => console.log("placeWithdrawErr", err));
      } else if (
        cryptoCode === "GXT" ||
        cryptoCode === "DAI" ||
        cryptoCode === "USDT" ||
        cryptoCode === "SEF"
      ) {
        let contract;
        switch (cryptoCode) {
          case "GXT":
            contract = "0x60c87297a1feadc3c25993ffcadc54e99971e307";
            break;
          case "DAI":
            contract = "0x6b175474e89094c44da98b954eedeac495271d0f";
            break;
          case "UDST":
            contract = "0xdac17f958d2ee523a2206206994597c13d831ec7";
            break;
          case "SEF":
            contract = "0x5aA65272ef4561F11Bda38bC8be74870eABa2c98";
            break;
          default:
            contract = "";
        }
        axios
          .post("https://comms.globalxchange.com/coin/send_Token", {
            email: email,
            token: localStorage.getItem("token"),
            from: eth_address,
            to: cryptoAdress,
            contract,
            amount: amountCrypto,
            fun_type: "Send",
            coin_type: cryptoCode
          })
          .then(r => {
            console.log("placeWithdraw", r);
            if (r.data.message.contains("insufficient funds")) {
              setError_msg("Insufficient Funds");
            }
          })
          .catch(err => console.log("placeWithdrawErr", err));
      }
    }
  };

  const [show1, setshow1] = useState(false);

  const handleScan = data => {
    if (data) {
      setCryptoAdress(data);
      setshow1(false);
    }
    console.log("typed", cryptoAdress);
  };

  const handleError = err => {
    console.error(err);
  };

  return (
    <Modal
      open={show}
      onClose={() => {
        setShow(false);
      }}
      className="popUpTabSection popUppaddingLeft popupVerticleCenter transitionNon"
    >
      <div className={classes.paper}>
        <div className="sendWalletMainWraperSectionBlock">
          <div>
            <form>
              <div className="SendDropdwonSectionWraper">
                <div className="SendDropdwonSectionBlock">
                  <div className="CurrencyLabelSectionWraper">
                    <label>Wallets</label>
                  </div>
                  <div className="currencySendDropDownDesktop">
                    <div className="customdropdownBlockSection3">
                      <img src={cryptoImage} alt="" />
                      <span>{cryptoTitle}</span>
                    </div>
                    <div> </div>
                  </div>
                </div>
                <div className="FromSendOptionSectionWrapper">
                  <div className="CurrencyLabelSectionWraper">
                    <label>Balance</label>
                  </div>
                  <div className="desktopSendDropdownSection">
                    <p>
                      {amountFormatterCrypto.format(balanceCrypto)}
                      {cryptoCode} ({amountFormatter.format(balanceUSD)})
                    </p>
                  </div>
                </div>
              </div>
              <div className="RecipientAddressSection">
                <label>To</label>
                <input
                  name="crypto-adress"
                  type="text"
                  placeholder={`Enter a ${cryptoCode} address`}
                  onFocus={handleInputFocus}
                  className="RecipientAddressSectioninputBlock"
                  value={cryptoAdress}
                  onChange={e => {
                    setCryptoAdress(e.target.value);
                  }}
                />
                {/* <QrCodeScan
                  result={cryptoAdress}
                  handleScan={handleScan}
                  handleError={handleError}
                  show1={show1}
                  handleShow1={() => {
                    setshow1(true);
                  }}
                  handleClose1={() => {
                    setshow1(false);
                  }}
                /> */}
                <br />
              </div>
              <div className="RecipientAddressSection RecipientMrg RecipientCenter">
                <label>Amount</label>
                <div
                  className="sendMaxSectionWraper"
                  style={{
                    display: "inline-block",
                    border: "1px solid rgb(209, 209, 209)",
                    borderRadius: 4
                  }}
                >
                  <input
                    placeholder="$0.00"
                    name="amount-usd"
                    className="RecipientAddressSectionAmountinputBlock"
                    type="text"
                    style={{ border: "none" }}
                    value={amountUsd}
                    onChange={onChangeVaidator}
                    onFocus={handleInputFocus}
                  />
                  <button onClick={maxUsd}>Max</button>
                </div>
                <img
                  alt=""
                  src="/static/media/exchange.a7752bad.svg"
                  className="RecipientAddressSectionAmountImageBlock"
                />
                <input
                  name="crypto-val"
                  placeholder={`0.00 ${cryptoCode}`}
                  className="RecipientAddressSectionAmountinputBlock"
                  type="text"
                  onChange={onChangeVaidatorCry}
                  value={amountCrypto}
                  onFocus={handleInputFocus}
                />
              </div>
              <div className="sendContinueSectionWraper">
                <button
                  className="btn btn-primary"
                  onClick={withdrawCrypto}
                  disabled={amountUsd > balanceUSD && cryptoAdress !== ""}
                >
                  Submit Withdrawal
                </button>
                {error_msg !== "" ? (
                  <p className="text-danger text-center">{error_msg}</p>
                ) : null}
                {amountUsd > balanceUSD ? (
                  <p className="text-danger text-center">
                    The Entered Amount Is Greater Than Your Balance
                  </p>
                ) : null}
              </div>
            </form>
            <div />
          </div>
        </div>
      </div>
    </Modal>
  );
}

export default Withdraw;
