/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useState, useEffect } from "react";
import { Card } from "@material-ui/core";
import VaultAssetsItem from "./VaultAssetsItem";
// import bitcoin from "./images/coins/bitcoin.svg";
// import ethereum from "./images/coins/ethereum.svg";
// import litecoin from "./images/coins/litecoin.svg";
// import monero from "./images/coins/monero.svg";
// import riple from "./images/coins/riple.svg";
// import bat from "./images/coins/bat.svg";
// import augur from "./images/coins/augur.svg";

import btc from "./images/coins/btc.svg";
import eth from "./images/coins/eth.svg";
import usdt from "./images/coins/usdt.svg";
import gx from "./images/coins/gx.svg";

import can_dollar from "./images/coins/can-dollar.svg";
import aus_dollar from "./images/coins/aus-dollar.svg";
import dollar from "./images/coins/dollar.svg";
import euro from "./images/coins/euro.svg";
import pound from "./images/coins/pound.svg";
import rupee from "./images/coins/rupee.svg";
import yen from "./images/coins/yen.svg";
import aed from "./images/coins/aed.svg";
import hk from "./images/coins/hk.svg";
import { brokerage } from "../../../Brokercontextapi/Contextapi";

import Axios from "axios";
import Invoice from "../SavedPaymentMethods/Invoice";

const VaultAssets = ({ type, cardOpen, toggle, coin, setCoin }) => {
  const { btc_address, eth_address, email } = useContext(brokerage);
  const [effect, setEffect] = useState(false);
  const [cryptobalance, setCryptoBalance] = useState({});
  const [fiatBalance, setFiatBalance] = useState({});

  useEffect(() => {
    Axios.post("https://comms.globalxchange.com/coin/get_all_balances", {
      email: email
    })
      .then(result => {
        if (result.data !== cryptobalance) {
          setCryptoBalance(result.data);
        }
        console.log("result.datas", result);
      })
      .catch(err => {
        console.log("result.data".err);
      });

    Axios.get(`https://comms.globalxchange.com/coin/fiat/fiat_balances?email=${email}`)
      .then(result => {
        if (result.data.fiat_balances !== fiatBalance) {
          setFiatBalance(result.data.fiat_balances);
        }
        console.log("result.data", result);
      })
      .catch(err => {
        console.log("result.data".err);
      });
    return () => {};
  }, [effect]);

  const gxAssets = [
    {
      img: btc,
      title: "Bitcoin",
      value: 1.0003,
      code: "BTC",
      address: btc_address
    },
    {
      img: eth,
      title: "Ethereum",
      value: 34.243,
      code: "ETH",
      address: "0x" + eth_address
    },
    {
      img: gx,
      title: "GXToken",
      value: 45000,
      code: "GXT",
      address: "0x" + eth_address
    },
    {
      img: usdt,
      title: "Tether",
      value: 0.0001,
      code: "USDT",
      address: "0x" + eth_address
    },
    {
      img: gx,
      title: "SEFCoin",
      value: 40108,
      code: "SEF",
      address: "0x" + eth_address
    }
  ];

  const fxAssets = [
    {
      img: dollar,
      title: "US Dollar",
      value: 5494.0,
      code: "USD"
    },
    {
      img: aus_dollar,
      title: "Australian Dollar",
      value: 5456.0,
      code: "AUD"
    },
    {
      img: can_dollar,
      title: "Canadian Dollar",
      value: 6454.0,
      code: "CAD"
    },
    {
      img: euro,
      title: "Euro",
      value: 5494.4,
      code: "EUR"
    },
    {
      img: pound,
      title: "Pound Sterling",
      value: 4410.8,
      code: "GBP"
    },
    {
      img: rupee,
      title: "Indian Rupee",
      value: 124101.0,
      code: "INR"
    },
    {
      img: yen,
      title: "Japanese Yen",
      value: 4101.0,
      code: "JPY"
    },
    {
      img: yen,
      title: "Chinese Yuan",
      value: 2101.05,
      code: "CNY"
    },
    {
      img: aed,
      title: "United Arab Emirates Dirham",
      value: 124101.5,
      code: "AED"
    },
    {
      img: hk,
      title: "Honng Kong Dollar",
      value: 124101.1,
      code: "HKD"
    }
  ];

  const assetsList = () => {
    switch (type) {
      case "fx": {
        return fxAssets.map(({ img, title, value, code }) => {
          return (
            <VaultAssetsItem
              key={code}
              img={img}
              title={title}
              code={code}
              balance={fiatBalance}
              cardOpen={cardOpen}
              coin={coin}
              setCoin={setCoin}
            />
          );
        });
      }
      case "gx": {
        return gxAssets.map(({ img, title, value, code, address }) => {
          return (
            <VaultAssetsItem
              key={code}
              img={img}
              title={title}
              // type={}
              code={code}
              crypto={true}
              address={address}
              balance={cryptobalance}
              cardOpen={cardOpen}
              coin={coin}
              setCoin={setCoin}
            />
          );
        });
      }
      default: {
        return <></>;
      }
    }
  };

  return (
    <>
      <Card className="your-assets crypto-assets">
        <div className="d-none d-md-flex assets-item mx-2 px-2 text-left py-3">
          <div className="col-md-5">Currency</div>
          <div className="col-md-4">Balance</div>
          {cardOpen ? (
            <div className="col-md-3 text-center d-flex">
              <div className="ml-auto btn btn-danger" onClick={toggle}>
                Close
              </div>
            </div>
          ) : (
            <div className="col-md-3 text-center">Actions</div>
          )}
        </div>
        <div className="assets-wrapper">{assetsList()}</div>
      </Card>
    </>
  );
};

export default VaultAssets;
