import React, { useState, useEffect } from "react";
import buy from "./images/buy.svg";
import sell from "./images/sell.svg";
import more from "./images/more.svg";
import { Tooltip } from "@material-ui/core";
import Withdraw from "./Withdraw";

const VaultAssetItems = ({
  img,
  title,
  type,
  code,
  crypto,
  address,
  balance,
  cardOpen,
  coin,
  setCoin
}) => {
  const amountFormatter = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  });
  const amountFormatterCrypto = new Intl.NumberFormat("en-US", {
    minimumSignificantDigits: 5,
    maximumSignificantDigits: 5
  });

  const [copyStr, setCopyStr] = useState("Copy Address");
  const [show, setShow] = useState(false);

  const [balanceUSD, setBalanceUSD] = useState(0);
  const [balanceAsset, setBalanceAsset] = useState(0);
  useEffect(() => {
    if (crypto) {
      console.log("objec", balance);
      switch (code.toLowerCase()) {
        case "btc":
          if (balance.btc !== undefined) {
            setBalanceAsset(parseFloat(balance.btc.btc_balance));
            setBalanceUSD(parseFloat(balance.btc.price_usd));
          }
          break;
        case "eth":
          if (balance.eth !== undefined) {
            setBalanceAsset(parseFloat(balance.eth.eth_balance));
            setBalanceUSD(parseFloat(balance.eth.price_usd));
          }
          break;
        case "gxt":
          if (balance.gxt !== undefined) {
            setBalanceAsset(parseFloat(balance.gxt.gxt_balance));
            setBalanceUSD(parseFloat(balance.gxt.price_usd));
          }
          break;
        case "sef":
          if (balance.sef !== undefined) {
            setBalanceAsset(parseFloat(balance.sef.sef_balance));
            setBalanceUSD(parseFloat(balance.sef.price_usd));
          }
          break;
        case "usdt":
          if (balance.usdt !== undefined) {
            setBalanceAsset(parseFloat(balance.usdt.usdt_balance));
            setBalanceUSD(parseFloat(balance.usdt.price_usd));
          }
          break;
        default:
      }
    } else {
      if (balance !== undefined) {
        switch (code.toLowerCase()) {
          case "usd":
            setBalanceAsset(parseFloat(balance.usd_balance));
            break;
          case "inr":
            setBalanceAsset(parseFloat(balance.inr_balance));
            break;
          case "aud":
            setBalanceAsset(parseFloat(balance.aud_balance));
            break;
          case "cad":
            setBalanceAsset(parseFloat(balance.cad_balance));
            break;
          case "cny":
            setBalanceAsset(parseFloat(balance.cny_balance));
            break;
          case "aed":
            setBalanceAsset(parseFloat(balance.aed_balance));
            break;
          case "eur":
            setBalanceAsset(parseFloat(balance.eur_balance));
            break;
          case "gbp":
            setBalanceAsset(parseFloat(balance.gbp_balance));
            break;
          case "jpy":
            setBalanceAsset(parseFloat(balance.jpy_balance));
            break;
          default:
            setBalanceAsset(0);
        }
      }
      console.log("objec", balance);
      console.log("obje", code.toLowerCase());
    }
    return () => {};
  }, [code, balance]);

  let coinTitle = coin ? (coin.title ? coin.title : "") : "";

  return (
    <div className="d-flex assets-item card m-2 p-2">
      <div className="col-md-5 d-flex flex-row">
        <img src={img} alt="" className="img-coin my-auto mx-2" />
        <div className="title my-auto">{title}</div>
      </div>
      <div className="col-md-4 d-flex flex-row">
        <h5 className="value my-auto">
          {crypto
            ? amountFormatterCrypto.format(balanceAsset)
            : amountFormatter.format(balanceAsset)}
        </h5>
      </div>
      <div className="col-md-3 d-flex flex-row justify-content-around">
        {cardOpen ? (
          <>
            {coinTitle !== title ? (
              <button
                className="btn btn-secondary px-2 py-1 my-auto"
                onClick={() => setCoin({ title: title, code: code })}
              >
                Select
              </button>
            ) : (
              <button className="btn btn-primary px-2 py-1 my-auto">
                Selected
              </button>
            )}
          </>
        ) : (
          <>
            <Tooltip title={copyStr}>
              <img
                src={buy}
                alt=""
                className="img-btn btn py-0"
                onClick={() => {
                  navigator.clipboard.writeText(address);
                  setCopyStr("Copied");
                }}
              />
            </Tooltip>
            <Tooltip title="Withdraw">
              <img
                src={sell}
                alt=""
                className="img-btn btn py-0"
                onClick={() => {
                  setShow(!show);
                }}
              />
            </Tooltip>
          </>
        )}
        {/* <Tooltip title="More">
          <img src={more} alt="" className="img-btn btn py-0" />
        </Tooltip> */}
      </div>
      <Withdraw
        show={show}
        setShow={setShow}
        cryptoImage={img}
        cryptoTitle={title}
        cryptoCode={code}
        balanceUSD={parseFloat(balanceUSD)}
        balanceCrypto={parseFloat(balanceAsset)}
      />
    </div>
  );
};

export default VaultAssetItems;
