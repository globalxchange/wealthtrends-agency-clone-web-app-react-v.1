import React, { useEffect } from "react";



import Header from './EcommercHeader'
import Content from './E-coomerce'

const BrokerLandingPage = () => {
  return (
    <div className="FixedSideBarWraperSection">
      <div
        id="scroll-wrapper"
        className="emmmorceshow"
      >
      
          <Header />
          <Content />
           {/* <FeatureCards /> */}
          {/* <BrokerLandingMainFeature />
          <BrokerLandingPartners />
          <BrokerLandingWhoCanBe />
          <BrokerLandingFAQ />
          <BrokerLandingTestimonial />  */}
  
      </div>
    </div>
  );
};

export default BrokerLandingPage;
