import React, { useContext } from "react";
import { brokerage } from "../../../Brokercontextapi/Contextapi"

function OrderSummary() {
  const { thingsToPurchase } = useContext(brokerage);

  const amountFormatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: 3,
    maximumFractionDigits: 3
  });
  var total = 0;
  thingsToPurchase.map(things => {
    total = total + things.rate;
  });
  return (
    <div className="h-100 flex-grow-1">
      <div className="d-flex flex-column px-5 h-100">
        <div className="position-relative d-flex flex-column flex-grow-1 border-top">
          <div className="p-3 text-black-50 flex-row d-flex justify-content-between">
            <h3 className="m-0">Price Summary</h3>
          </div>
          <div
            className="flex-grow-1 px-3 flex-grow-h-0"
            style={{ overflowY: "auto" }}
          >
            {thingsToPurchase.map((things, i) => {
              return (
                <div className="p-2 m-1" key={i}>
                  <h6>
                    <i className="fas fa-star-of-life" /> {things.title}
                  </h6>
                  <div className="flex-row d-flex justify-content-between">
                    <p className="col-9">{things.subtitle}</p>
                    <b className="col-3">
                      {amountFormatter.format(things.rate)}
                    </b>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="p-3 text-black-50 flex-row d-flex justify-content-between">
          <h3 className="m-0">Total</h3>{" "}
          <h3 className="m-0"> {amountFormatter.format(total)}</h3>
        </div>
      </div>
    </div>
  );
}

export default OrderSummary;
