import React, { useState, useContext, useEffect } from "react";
import { brokerage } from "../../../Brokercontextapi/Contextapi"
import Axios from "axios";
import axios from "axios";
export default function VaultPortfolioValue() {



  const currency = [
    {
      id: 1,
      currency: "INR",
      flag:
        "https://upload.wikimedia.org/wikipedia/en/thumb/4/41/Flag_of_India.svg/1200px-Flag_of_India.svg.png"
    },
    {
      id: 2,
      currency: "USD",
      flag:
        "https://upload.wikimedia.org/wikipedia/en/thumb/a/a4/Flag_of_the_United_States.svg/1280px-Flag_of_the_United_States.svg.png"
    },
    {
      id: 3,
      currency: "AUD",
      flag:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Flag_of_Australia_%28converted%29.svg/1200px-Flag_of_Australia_%28converted%29.svg.png"
    },
    {
      id: 4,
      currency: "CAD",
      flag:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Canada_%28Pantone%29.svg/1200px-Flag_of_Canada_%28Pantone%29.svg.png"
    },
    {
      id: 5,
      currency: "CNY",
      flag:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSLlgSID9IJoWRvXjPm6WnvoR-D__J7D2ueCjCIT3H6XSJws3ms"
    },
    {
      id: 6,
      currency: "AED",
      flag:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQKoF4PQqSQvLwbJD6ExJ4-NO5k3wocEFJ2lL425XJP5ErF4unV"
    },
    {
      id: 7,
      currency: "EUR",
      flag:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTT5eoW6QlpgFJtV9YlpFOaoec-6FFYpAhzkxpDIiB08WSq5OOU"
    },
    {
      id: 8,
      currency: "GBP",
      flag:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRgAhSxtc0Y42tc3Cp-LqBhoddB05I0Eiqt09ficpdGtFfi82BR"
    },
    {
      id: 9,
      currency: "JPY",
      flag:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ4Lq2F0I2MfmhEG-60lfoCJAWTzR4IbrZ29YTr7jfLr1NOn8aW"
    }
  ];

  const changeDisplayCurrency = currency => {
    let currencySymbol;
    //fetchBalances(currency);
    switch (currency) {
      case "USD":
        currencySymbol = "$";
        break;
      case "CAD":
        currencySymbol = "$";
        break;
      case "AUD":
        currencySymbol = "$";
        break;
      case "INR":
        currencySymbol = "₹";
        break;
      case "CNY":
        currencySymbol = "¥";
        break;
      case "AED":
        currencySymbol = "AED";
        break;
      case "EUR":
        currencySymbol = "€";
        break;
      case "GBP":
        currencySymbol = "£";
        break;
      case "JPY":
        currencySymbol = "¥";
        break;
      default:
        currencySymbol = "$";
    }
    setActiveCurrency(currencySymbol);
  };

  const [activeCurrency, setActiveCurrency] = useState("$");
  const [currencyDropdown, setCurrencyDropdown] = useState(false);
  const [currencyType, setCurrency] = useState("USD");

  const { total_portfolio_balance } = useContext(brokerage);
  let usd_value = total_portfolio_balance.substr(1);
  usd_value = usd_value.replace(",", "");
  usd_value = parseFloat(usd_value);
  const [ratePerUsd, setRatePerUsd] = useState(1);
  const [selectedCurrencyValue, setSelectedCurrencyValue] = useState(0);

  const [totalSum, setTotalSum] = useState(0);
  const [globalValutCurrency, setGlobalCurrency] = useState("USD");
  const [loadingCurrencyChange, setLoadingCurrencyChange] = useState(false);
  console.log("total_portfolio_balance :", usd_value);
  const amountFormatter = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: 2
  });





  useEffect(() => {
    axios
      .get(
        `https://comms.globalxchange.com/coin/getCmcPrices?convert=${currencyType}`
      )
      .then(res => {});
  }, [currencyType]);

  useEffect(() => {
    setLoadingCurrencyChange(true);
    axios
      .get(
        `https://comms.globalxchange.com/coin/vault/user/balances/all?email=${localStorage.getItem(
          "login_account"
        )}&convert=${currencyType}`
      )
      .then(res => {
        setTotalSum(amountFormatter.format(res.data.total_sum));
        setLoadingCurrencyChange(false);

      });
  }, [currencyType]);








  useEffect(() => {
    setSelectedCurrencyValue("......");
    Axios.get(
      `https://comms.globalxchange.com/forex/convert?buy=${currencyType}&from=USD`
    ).then(res => {
      switch (currencyType) {
        case "USD":
          setRatePerUsd(1);
          break;
        case "CAD":
          setRatePerUsd(res.data.usd_cad);
          break;
        case "AUD":
          setRatePerUsd(res.data.usd_aud);
          break;
        case "INR":
          setRatePerUsd(res.data.usd_inr);
          break;
        case "CNY":
          setRatePerUsd(res.data.usd_cny);
          break;
        case "AED":
          setRatePerUsd(res.data.usd_aed);
          break;
        case "EUR":
          setRatePerUsd(res.data.usd_eur);
          break;
        case "GBP":
          setRatePerUsd(res.data.usd_gbp);
          break;
        case "JPY":
          setRatePerUsd(res.data.usd_jpy);
          break;
        default:
      }
    });
  }, [currencyType]);

  useEffect(() => {
    setSelectedCurrencyValue(amountFormatter.format(usd_value * ratePerUsd));
    return () => {};
  }, [ratePerUsd]);

  return (
    <div className="card saved-payment-uname card-inner py-2">
      <h2 className="text-center">
        {activeCurrency}
       {totalSum}
        {/* <span>
          {" "}
          (10.2%) <i className="fas fa-arrow-circle-up" />
        </span> */}
      </h2>
      <h6 className="text-center">
        Vault Portfolio Value in
        <span className="position-relative">
          <span
            className="my-auto"
            onClick={() => {
              setCurrencyDropdown(!currencyDropdown);
            }}
          >
            {" "}
            {currencyType} <i className="fas fa-angle-down ml-2" />
          </span>
          <div
            className={
              currencyDropdown ? "position-absolute main-dropdown" : "d-none"
            }
          >
            {currency.map(x => (
              <div
                onClick={() => {
                  setCurrency(x.currency);
                  setCurrencyDropdown(false);
                  changeDisplayCurrency(x.currency);
                }}
                className="d-flex flex-row m-1 btn country-list"
              >
                <img height="16px" width="27px" src={x.flag} alt="" />
                <span>{x.currency}</span>
              </div>
            ))}
          </div>
        </span>
      </h6>
    </div>
  );
}
