import React from "react";
import { Tabs } from "antd";

const { TabPane } = Tabs;

function SelectPaymentTabs({
  paymentMethodsTab,
  callback,
  posInTab,
  tabContent
}) {
  return (
    <Tabs defaultActiveKey={paymentMethodsTab} onChange={callback}>
      <TabPane tab="Vaults" key="0" >
        {tabContent[0][posInTab]}
      </TabPane>
      <TabPane tab="Cards" key="1" disabled={true}>
        {tabContent[1][posInTab]}
      </TabPane>
      <TabPane tab="Bank Accounts" key="2" disabled={true}>
        {tabContent[2][posInTab]}
      </TabPane>
      <TabPane tab="Alternative" key="3" disabled={true}>
        {tabContent[3][posInTab]}
      </TabPane>
      <TabPane tab="Exchanges" key="4" disabled={true}>
        {tabContent[4][posInTab]}
      </TabPane>
    </Tabs>
  );
}

export default SelectPaymentTabs;
