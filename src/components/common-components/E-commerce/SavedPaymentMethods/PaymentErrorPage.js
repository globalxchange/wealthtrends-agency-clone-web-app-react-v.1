import React from "react";

function PaymentErrorPage({ responseMsg }) {
  return (
    <div className="payment_error d-flex flex-column">
      <div className="m-auto">
        <div className="error__circle my-3">!</div>
        <div className="error__text">
          <div className="error__text error__text--lg my-2">
            oops! Payment failed
          </div>
          <div className="error__text error__text--sm my-1">{responseMsg}</div>
        </div>
      </div>
    </div>
  );
}

export default PaymentErrorPage;
