import React, { useContext } from "react";
import OrderSummary from "./OrderSummary";
import { brokerage } from "../../../Brokercontextapi/Contextapi"

function PaymentSuccessPage() {
  const { paymentSuccessTitle } = useContext(brokerage);

  return (
    <div className="paymentSuccess d-flex flex-column">
      <div
        className="d-flex flex-column justify-content-center"
        style={{
          paddingTop: "80px",
          paddingBottom: "80px",
          backgroundColor: "#186ab4",
          backgroundSize: "cover"
        }}
      >
        <h2 className="text-center text-light">{paymentSuccessTitle.title}</h2>
        <p className="text-center text-light">{paymentSuccessTitle.subTitle}</p>
      </div>
      <div className="py-4">
        <div className="my-2 mx-5 row">
          <div className="col-md-6">
            <h3>Purchase Details</h3>
            <p>
              Lorem ipsum dolor sit,
              <br />
              voluptatem dignissimos
              <br />
              labore consequatur magnam
              <br />
              dicta possimus reiciendis!
            </p>
          </div>
          <div className="col-md-6 d-flex flex-column justify-content-around">
            <div className="btn btn-gxblue mx-4">Track Order</div>
            <div className="btn btn-gxblue mx-4">Invoice</div>
          </div>
        </div>
        <OrderSummary />
      </div>
    </div>
  );
}

export default PaymentSuccessPage;
