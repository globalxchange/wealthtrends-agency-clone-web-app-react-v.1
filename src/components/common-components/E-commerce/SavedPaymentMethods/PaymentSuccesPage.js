import React, { useState, useContext, useEffect } from "react";
import Lottie from "react-lottie";
import * as animationData from "./blast_bg.json";
import { brokerage } from "../../../Brokercontextapi/Contextapi"
import trophy from "./static/images/trophy.svg";

function PaymentSuccesPage() {
  const [isStopped, setIsStopped] = useState(false);
  const [isPaused, setIsPaused] = useState(false);

  const { thingsToPurchase } = useContext(brokerage);

  const buttonStyle = {
    display: "block",
    margin: "10px auto"
  };

  const defaultOptions = {
    loop: false,
    autoplay: true,
    animationData: animationData.default,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };

  const [subTitle, setSubTitle] = useState("");
  useEffect(() => {
    
    console.log("thingsToPurchase :", thingsToPurchase[0]);
    thingsToPurchase[0]
      ? setSubTitle(thingsToPurchase[0].title.replace("Subscribing to", ""))
      : setSubTitle("");
  }, [thingsToPurchase]);

  return (
    <div
      style={{
        overflow: "hiden",
        maxHeight: "calc( 100vh - 60px )",
        position: "relative"
      }}
    >
      <Lottie
        options={defaultOptions}
        height="calc( 100vh - 60px )"
        width="auto"
      />
      <div
        className="d-flex"
        style={{
          position: "absolute",
          top: "0",
          bottom: "0",
          left: "0",
          right: "0",
          margin: "auto"
        }}
      >
        <div className="m-auto" style={{ maxWidth: "300px",textAlign:"center" }}>
          <img className="mx-auto" src={trophy} alt="" />
          <h3>Congratulations!</h3>
          <h5>You Have Now Joined{subTitle} Pulse Stream</h5>
        </div>
      </div>
    </div>
  );
}

export default PaymentSuccesPage;
