import React, { useState, useContext, useEffect } from "react";
import { Drawer, Paper } from "@material-ui/core";
import CardsWallet from "../CardsWallet/AddCardForms";
import "./static/sass/saved-payment-method.scss";
import { message, notification } from "antd";
import AddCardForms from "../CardsWallet/AddCardForms";
import "react-credit-cards/es/styles-compiled.css";
import CryptosVault from "../CryptosVault/CryptosVault";
import CryptoSpending from "../CryptosVault/CryptoSpending";
import { brokerage } from "../../../Brokercontextapi/Contextapi"
import BanksWallet from "../CardsWallet/BankWallet";
import AlternativeWallet from "../CardsWallet/AlternativeWallet";
import ExchangeWallet from "../CardsWallet/ExchangeWallet";
import VaultPortfolioValue from "./VaultPortfolioValue";
import Axios from "axios";
import SelectPaymentTabs from "./SelectPaymentTabs";
import PaymentSuccessPage from "./PaymentSuccessPage";
import PaymentErrorPage from "./PaymentErrorPage";
import PaymentSuccesPage from "./PaymentSuccesPage";

const SavedPaymentMethods = () => {
  // payWithSidebarOpen: this.payWithSidebarOpen, //Function To Open Side Barto Pay
  // cancelPayment: this.cancelPayment, //Cancel Payment Transction On Sidebar
  // paymentMethodsTabTogle: this.paymentMethodsTabTogle, //Open To See Saved Payment Methods
  // paymentMethodsTabOpen:this.state.paymentMethodsTabOpen, //Payment Methods Is Open
  // thingsToPurchase:this.state.thingsToPurchase, //Array Of Purchase List [{title:"XYX",subtitle:"abc",rate:50,img:{someIcon}}]
  // paymentMethodsTab:this.state.paymentMethodsTab, //Tab Index
  // cardInpaymentMethodsTab:this.sttate.cardInpaymentMethodsTab, //Card in Tab Position
  // isPuchasePaymentMethods:this.state.isPuchasePaymentMethods // True If purchase
  // callBackIsSuccessPaymentMethodsTab: this.state.callBackIsSuccessPaymentMethodsTab;
  // /* Call Back Funtion To PassPayment Sidebar the, function(isSuccess:bool,message:text,type:text)*/
  const cards = ["gx", "fx", "mm", "tf"];

  const {
    paymentMethodsTabTogle,
    paymentMethodsTabOpen,
    cancelPayment,
    paymentMethodsTab,
    cardInpaymentMethodsTab,
    isPuchasePaymentMethods,
    callBackIsSuccessPaymentMethodsTab,
    setSubscription,
    setSubscribeResponse,
    buyingFromPulse,
    setBuyingFromPulse,
    thingsToPurchase,
    subscribeResponse,
    gxid,
    paymentId,
    merchantName,
    isBackendPayment,
    merchantMailId
  } = useContext(brokerage);

  const selectCoinNotification = () => {
    notification["warning"]({
      message: "Select A Coin",
      description: "Select A Valid Coin To Check Out",
      placement: "topLeft"
    });
  };

  const [posTab, setPosTab] = useState(0);

  var total = 0;
  thingsToPurchase.map(things => {
    total = total + things.rate;
  });

  const [responseMsg, setResponseMsg] = useState("");

  const completePurchaseForPulse = () => {
    console.log("completePurchaseForPulse");
    Axios.post(`https://comms.globalxchange.com/coin/vault/charge`, {
      token: localStorage.getItem("token"),
      email: localStorage.getItem("user_account"),
      amount: total,
      coin: coin,
      merchant: "GXPulse",
      usd_value: total,
      purchase_id: gxid,
      merchant_email: localStorage.getItem("user_profile_email")
    })
      .then(res => {
        if (res.data.status) {
          console.log(res.data);
          setSubscription(true);
          setSubscribeResponse(res.data.result, () =>
            console.log("subscribeResponse", subscribeResponse)
          );
          setBuyingFromPulse(false);
          message.loading("Subcription in Process...");
          setIsSuccess("success");
          callBackIsSuccessPaymentMethodsTab(true, "Payment Success", coin);
        } else {
          setResponseMsg(res.data.message);
          console.log(res.data.message);
          setIsSuccess("failed");
          callBackIsSuccessPaymentMethodsTab(false, "Payment Failed", coin);
        }
      })
      .catch(err => {
        console.log("completePurchaseForPulse Error :", err);
        setResponseMsg("Something Went Wrong!");
        setIsSuccess("failed");
        callBackIsSuccessPaymentMethodsTab(false, "Payment Failed", coin);
      });
  };
  const completePurchaseForOther = async () => {
    if (isBackendPayment) {
      let isBackendPaymentSuccess = await callBackIsSuccessPaymentMethodsTab(
        false,
        "",
        coin
      );
      if (isBackendPaymentSuccess) {
        setIsSuccess("success");
      } else {
        setResponseMsg("Something Went Wrong!");
        setIsSuccess("failed");
      }
    } else {
      console.log("completePurchaseForOther");
      Axios.post(`https://comms.globalxchange.com/coin/vault/charge`, {
        token: localStorage.getItem("idToken"),
        email: localStorage.getItem("user_account"),
        amount: total,
        coin: coin,
        merchant: merchantName,
        usd_value: total,
        purchase_id: paymentId,
        merchant_email: merchantMailId
      })
        .then(res => {
          if (res.data.status) {
            console.log(res.data);
            setSubscription(true);
            setSubscribeResponse(res.data.result, () =>
              console.log("subscribeResponse", subscribeResponse)
            );
            message.loading("Subcription in Process...");
            setIsSuccess("success");
            callBackIsSuccessPaymentMethodsTab(true, "Payment Success", "");
          } else {
            setResponseMsg(res.data.message);
            console.log(res.data.message);
            setIsSuccess("failed");
            callBackIsSuccessPaymentMethodsTab(false, "Payment Failed", "");
          }
        })
        .catch(err => {
          console.log("completePurchaseForPulse Error :", err);
          setResponseMsg("Something Went Wrong!");
          setIsSuccess("failed");
          callBackIsSuccessPaymentMethodsTab(false, "Payment Failed", "");
        });
    }
  };

  const [isTransact, setIsTransact] = useState(true);
  const [pos, setPos] = useState(0);

  const [type, setType] = useState("");
  const [coin, setCoin] = useState("");

  useEffect(() => {
    setType(coin);
    return () => {};
  }, [coin]);

  const [isSuccess, setIsSuccess] = useState("Pending");

  useEffect(() => {
    if (isPuchasePaymentMethods) {
      setIsSuccess("Pending");
    }
  }, [isPuchasePaymentMethods]);
  useEffect(() => {
    setPos(cardInpaymentMethodsTab);
    return () => {};
  }, [cardInpaymentMethodsTab]);

  useEffect(() => {
    setPosTab(parseInt(paymentMethodsTab));
    return () => {};
  }, [paymentMethodsTab]);

  const callback = key => {
    setPosTab(parseInt(key));
    setPosInTab(0);
    setPos(0);
  };

  const next = () => {
    if (posInTab < tabContent[posTab].length - 1) {
      setPosInTab(posInTab + 1);
    }
  };

  const prev = () => {
    if (posInTab > 0) {
      setPosInTab(posInTab - 1);
    }
  };

  const [posInTab, setPosInTab] = useState(0);

  const tabContent = [
    [
      <CryptosVault
        isTransact={isPuchasePaymentMethods}
        setIsTransact={setIsTransact}
        pos={pos}
        setPos={setPos}
        setCoin={setCoin}
        coin={coin}
        posTab={posTab}
      />,
      <CryptoSpending />
    ],
    [
      <CardsWallet
        // pos={pos}
        // setPos={setPos}
        // isTransact={isPuchasePaymentMethods}
        // setIsTransact={setIsTransact}
      />,
      // <AddCardForms />
    ],
    [
      <BanksWallet
        //  pos={pos}
        // setPos={setPos}
        // isTransact={isPuchasePaymentMethods}
        // setIsTransact={setIsTransact}
      />
    ],
    [
      <AlternativeWallet
        // pos={pos}
        // setPos={setPos}
        // isTransact={isPuchasePaymentMethods}
        // setIsTransact={setIsTransact}
      />
    ],
    [
      <ExchangeWallet
        pos={pos}
        setPos={setPos}
        isTransact={isPuchasePaymentMethods}
        setIsTransact={setIsTransact}
      />
    ]
  ];

  const hider = isPuchasePaymentMethods ? "" : "hide-pay-btn";

  useEffect(() => {
    if (posTab === 0) {
      switch (pos) {
        case 3:
          setType("Tokenised Funds");
          break;
        case 2:
          setType("Money Market");
         
          
          break;
        case 1:
          setType("Forex Vault");
          break;
        default:
          setType("Crypto Vault");
      }
    } else if (posTab === 1) {
      setType("Credit Card");

    } else if (posTab === 2) {
      setType("Bank Account");
    } else if (posTab === 3) {
      setType("Alternative");
    } else if (posTab === 4) {
      setType("Exchanges");
    }
    return () => {};
  }, [posTab, pos]);

  const completePurchase = () => {
    if (coin !== "") {
      if (buyingFromPulse === true) {
        completePurchaseForPulse();
      } else {
        completePurchaseForOther();
      }
    } else {
      selectCoinNotification();
    }
  };

  var PageToRender = <></>;
  var purchaseButtons = <></>;

  switch (isSuccess) {
    case "success":
      PageToRender = <PaymentSuccesPage />;
      purchaseButtons = (
        <div className="d-flex flex-row">
          <button
            className="mx-2 ml-0"
            onClick={() => {
              cancelPayment();
              setIsSuccess("Pending");
            }}
          >
            Go To Vault Page
          </button>
          <button className="mx-2 ml-0" onClick={paymentMethodsTabTogle}>
            Close
          </button>
        </div>
      );
      break;
    case "failed":
      PageToRender = (
        <>
          <VaultPortfolioValue />
          <PaymentErrorPage responseMsg={responseMsg} />
        </>
      );
      purchaseButtons = (
        <div className="d-flex flex-row">
          <button className="mx-2 ml-0" onClick={paymentMethodsTabTogle}>
            Close
          </button>
          <button
            className="mx-2 ml-0"
            onClick={() => {
              setIsSuccess("Pending");
              setPos(0);
              setPosTab(0);
            }}
          >
            Retry Payment
          </button>
        </div>
      );
      break;
    default:
      PageToRender = (
        <>
          <VaultPortfolioValue />
          <SelectPaymentTabs
            paymentMethodsTab={paymentMethodsTab}
            callback={callback}
            posInTab={posInTab}
            tabContent={tabContent}
          />
        </>
      );
      purchaseButtons = (
        
        <div className="d-flex flex-row">
          {
          cards[pos]==="mm" || cards[pos]==="tf"? "":
      <>
          <button
            className="mx-2 ml-0"
            onClick={() => {
              setIsTransact(true);
              cancelPayment();
              callBackIsSuccessPaymentMethodsTab(false, "Payment Rejected", "");
              paymentMethodsTabTogle();
              setIsSuccess("Pending");
            }}
          >
            Reject Transaction
          </button>
          <button className="mx-2 mr-0" onClick={completePurchase}>
            Complete Purchase With {type}
          </button>
          </>
            }
        </div>
      );
  }

  useEffect(() => {
    if (isSuccess !== "Pending") {
      cancelPayment();
      setIsSuccess("Pending");
    }
    
  }, [paymentMethodsTabOpen]);
  console.log("kamal",paymentMethodsTabOpen)
  return (
    <Drawer
      anchor="right"
      open={paymentMethodsTabOpen}
      onClose={paymentMethodsTabTogle}
    >

      <div className={"saved-payment-method"}>
        {PageToRender}
        <div className="bottom-bar w-100">

          {isPuchasePaymentMethods ? (

            <>{purchaseButtons}</>

          ) : posInTab === 0 && posTab === 0 ? (
            <div className="d-flex flex-row">
              <button className="mx-2 ml-0">Buy</button>
              <button className="mx-2">Transfer</button>
              <button className="mx-2 mr-0">Sell</button>
            </div>
          ) : posInTab === 0 && posTab === 1 ? (
            <div className="d-flex justify-content-between">
              {posInTab === 0 ? (
                <button onClick={paymentMethodsTabTogle}>Close</button>
              ) : (
                <button onClick={prev}>Back</button>
              )}
              {posInTab === tabContent[posTab].length - 1 ? null : (
                <button onClick={next}>Add Card</button>
              )}
            </div>
          ) : (
            <div className="d-flex justify-content-between">
              {posInTab === 0 ? (
                <button onClick={paymentMethodsTabTogle}>Close</button>
              ) : (
                <button onClick={prev}>Back</button>
              )}
              {posInTab === tabContent[posTab].length - 1 ? null : (
                <button onClick={next}>Next</button>
              )}
            </div>
          )}
        </div>
      </div>
    </Drawer>
  );
};

export default SavedPaymentMethods;
