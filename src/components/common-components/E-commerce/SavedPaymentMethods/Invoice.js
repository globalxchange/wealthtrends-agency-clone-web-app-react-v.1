import React, { useContext, useState, useEffect } from "react";
import { Card } from "@material-ui/core";
import { brokerage } from "../../../Brokercontextapi/Contextapi"
import star from "./static/images/star.svg";
import CryptoSelector from "../currency_crypto_selector/CryptoSelector";
import CurrencySelector from "../currency_selector/CurrencySelector";
import Axios from "axios";

function Invoice({ posTab, pos, setCoin }) {
  const { thingsToPurchase } = useContext(brokerage);
  const [currencyDropdown, setCurrencyDropdown] = useState(false);
  const [currencyType, setCurrency] = useState("USD");

  const amountFormatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  });

  const amountFormatterTwo = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  });

  const amountFormatterFive = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: 5,
    maximumFractionDigits: 5
  });
  const [toatalUsd, setToatalUsd] = useState(0);
  var total = 0;
  useEffect(() => {
    thingsToPurchase.map(things => {
      total = total + things.rate;
    });
    setToatalUsd(parseFloat(total));
  }, [thingsToPurchase]);

  const [image, setImage] = useState();

  const callBack = (activeCurrency, currencyType, image) => {
    setCurrency(currencyType);
    setImage(image);
  };

  const [amount, setAmount] = useState(0);

  useEffect(() => {
    setAmount("....");
    Axios.get(
      `https://comms.globalxchange.com/forex/convert?buy=${currencyType.toUpperCase()}&from=USD`
    ).then(res => {
      switch (currencyType.toUpperCase()) {
        case "USD":
          setAmount(amountFormatterTwo.format(toatalUsd * 1));
          break;
        case "CAD":
          setAmount(amountFormatterTwo.format(toatalUsd * res.data.usd_cad));
          break;
        case "AUD":
          setAmount(amountFormatterTwo.format(toatalUsd * res.data.usd_aud));
          break;
        case "INR":
          setAmount(amountFormatterTwo.format(toatalUsd * res.data.usd_inr));
          break;
        case "CNY":
          setAmount(amountFormatterTwo.format(toatalUsd * res.data.usd_cny));
          break;
        case "AED":
          setAmount(amountFormatterTwo.format(toatalUsd * res.data.usd_aed));
          break;
        case "EUR":
          setAmount(amountFormatterTwo.format(toatalUsd * res.data.usd_eur));
          break;
        case "GBP":
          setAmount(amountFormatterTwo.format(toatalUsd * res.data.usd_gbp));
          break;
        case "JPY":
          setAmount(amountFormatterTwo.format(toatalUsd * res.data.usd_jpy));
          break;
        default:
      }
    });
    Axios.get(
      "https://comms.globalxchange.com/coin/getCmcPrices?convert=USD"
    ).then(res => {
      console.log(`res.data :${currencyType}`, res.data.btc_price);
      switch (currencyType.toLowerCase()) {
        case "btc": {
          setAmount(
            `${amountFormatterFive.format(
              parseFloat(toatalUsd / res.data.btc_price)
            )} ${currencyType}`
          );
          break;
        }
        case "eth": {
          setAmount(
            `${amountFormatterFive.format(
              parseFloat(toatalUsd / res.data.eth_price)
            )} ${currencyType}`
          );
          break;
        }
        case "gxt": {
          setAmount(toatalUsd / parseFloat(res.data.gxt_price));
          break;
        }
        case "usdt": {
          setAmount(
            amountFormatterTwo.format(
              toatalUsd / parseFloat(res.data.usdt_price)
            )
          );
          break;
        }
      }
    });
  }, [currencyType, toatalUsd]);

  useEffect(() => {
    try {
      setCoin(currencyType);
    } catch (error) {}
  }, [currencyType]);
  let dropDown = <></>;
  if (posTab === 0 && pos === 0) {
    dropDown = (
      <h4 className="m-0 p-2 d-flex">
        <img
          src={image}
          className="my-auto mr-1 ml-auto"
          alt=""
          style={{ height: "0.9em", width: "auto" }}
        />
        <span className="position-relative">
          <span
            className="my-auto"
            onClick={() => {
              setCurrencyDropdown(!currencyDropdown);
            }}
          >
            {" "}
            {currencyType} <i className="fas fa-angle-down ml-2" />
          </span>
          <CryptoSelector
            setCurrencyDropdown={setCurrencyDropdown}
            currencyDropdown={currencyDropdown}
            callBack={callBack}
          />
        </span>
      </h4>
    );
  } else if (posTab === 0 && pos === 1) {
    dropDown = (
      <h4 className="m-0 p-2 d-flex">
        <img
          src={image}
          className="my-auto mr-1 ml-auto"
          alt=""
          style={{ height: "0.9em", width: "auto" }}
        />
        <span className="position-relative">
          <span
            className="my-auto"
            onClick={() => {
              setCurrencyDropdown(!currencyDropdown);
            }}
          >
            {" "}
            {currencyType} <i className="fas fa-angle-down ml-2" />
          </span>
          <CurrencySelector
            setCurrencyDropdown={setCurrencyDropdown}
            currencyDropdown={currencyDropdown}
            callBack={callBack}
          />
        </span>
      </h4>
    );
  } else {
    dropDown = <></>;
  }
  return (
    <Card className="h-100 invoice">
      <div className="d-flex flex-column h-100">
        <div className="position-relative d-flex flex-column flex-grow-1">
          <div
            className="px-3 text-black-50 flex-row d-flex justify-content-between"
            style={{ backgroundColor: "#f0f3fad9" }}
          >
            <h4 className="m-0 p-2">Price Summary</h4>
            {dropDown}
          </div>
          <div
            className="flex-grow-1 p-3 flex-grow-h-0"
            style={{ overflowY: "auto" }}
          >
            {thingsToPurchase.map((things, i) => {
              return (
                <div className="card p-2 m-1 d-flex flex-row" key={i}>
                  <img
                    className="my-auto"
                    src={things.img ? things.img : star}
                    alt=""
                    width={50}
                    height={50}
                  />
                  <div className="col d-flex flex-column flex-grow-1 justify-content-around">
                    <h5 className="m-0">{things.title}</h5>
                    <p className="m-0">{things.subtitle}</p>
                  </div>
                  <h4 className="ml-auto text-right text-nowrap my-auto">
                    {amountFormatter.format(things.rate)}
                  </h4>
                </div>
              );
            })}
          </div>
        </div>
        <div
          className="px-3 text-black-50 flex-row d-flex justify-content-between"
          style={{ backgroundColor: "#f0f3fad9" }}
        >
          <h4 className="m-0 p-2">Total</h4>{" "}
          <h4 className="m-0 ml-auto p-2 text-right text-nowrap"> {amount}</h4>
        </div>
      </div>
    </Card>
  );
}

export default Invoice;
