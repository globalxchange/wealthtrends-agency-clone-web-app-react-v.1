import React, { useState, useEffect } from "react";

export default function CurrencySelector({
  currencyDropdown,
  setCurrencyDropdown,
  callBack
}) {
  const currency = [
    {
      id: 1,
      currency: "INR",
      flag:
        "https://upload.wikimedia.org/wikipedia/en/thumb/4/41/Flag_of_India.svg/1200px-Flag_of_India.svg.png"
    },
    {
      id: 2,
      currency: "USD",
      flag:
        "https://upload.wikimedia.org/wikipedia/en/thumb/a/a4/Flag_of_the_United_States.svg/1280px-Flag_of_the_United_States.svg.png"
    },
    {
      id: 3,
      currency: "AUD",
      flag:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Flag_of_Australia_%28converted%29.svg/1200px-Flag_of_Australia_%28converted%29.svg.png"
    },
    {
      id: 4,
      currency: "CAD",
      flag:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Canada_%28Pantone%29.svg/1200px-Flag_of_Canada_%28Pantone%29.svg.png"
    },
    {
      id: 5,
      currency: "CNY",
      flag:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSLlgSID9IJoWRvXjPm6WnvoR-D__J7D2ueCjCIT3H6XSJws3ms"
    },
    {
      id: 6,
      currency: "AED",
      flag:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQKoF4PQqSQvLwbJD6ExJ4-NO5k3wocEFJ2lL425XJP5ErF4unV"
    },
    {
      id: 7,
      currency: "EUR",
      flag:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTT5eoW6QlpgFJtV9YlpFOaoec-6FFYpAhzkxpDIiB08WSq5OOU"
    },
    {
      id: 8,
      currency: "GBP",
      flag:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRgAhSxtc0Y42tc3Cp-LqBhoddB05I0Eiqt09ficpdGtFfi82BR"
    },
    {
      id: 9,
      currency: "JPY",
      flag:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ4Lq2F0I2MfmhEG-60lfoCJAWTzR4IbrZ29YTr7jfLr1NOn8aW"
    }
  ];

  const changeDisplayCurrency = currency => {
    let currencySymbol;
    //fetchBalances(currency);
    switch (currency) {
      case "USD":
        currencySymbol = "$";
        break;
      case "CAD":
        currencySymbol = "$";
        break;
      case "AUD":
        currencySymbol = "$";
        break;
      case "INR":
        currencySymbol = "₹";
        break;
      case "CNY":
        currencySymbol = "¥";
        break;
      case "AED":
        currencySymbol = "AED";
        break;
      case "EUR":
        currencySymbol = "€";
        break;
      case "GBP":
        currencySymbol = "£";
        break;
      case "JPY":
        currencySymbol = "¥";
        break;
      default:
        currencySymbol = "$";
    }
    setActiveCurrency(currencySymbol);
  };
  const [activeCurrency, setActiveCurrency] = useState("$");
  const [currencyType, setCurrency] = useState("USD");
  const [image, setImage] = useState(currency[1].flag);

  useEffect(() => {
    callBack(activeCurrency, currencyType, image);
  }, [activeCurrency, currencyType, image]);

  return (
    <div
      className={
        currencyDropdown ? "position-absolute main-dropdown" : "d-none"
      }
    >
      {currency.map(x => (
        <div
          key={x.id}
          onClick={() => {
            setCurrency(x.currency);
            setCurrencyDropdown(false);
            changeDisplayCurrency(x.currency);
            setImage(x.flag);
          }}
          className="d-flex flex-row m-1 btn country-list"
        >
          <img height="16px" width="27px" src={x.flag} alt="" />
          <span>{x.currency}</span>
        </div>
      ))}
    </div>
  );
}
