import React, { useState, useEffect } from "react";

import btc from "../CryptosVault/images/coins/btc.svg";
import eth from "../CryptosVault/images/coins/eth.svg";
import usdt from "../CryptosVault/images/coins/usdt.svg";
import gx from "../CryptosVault/images/coins/gx.svg";

export default function CryptoSelector({
  currencyDropdown,
  setCurrencyDropdown,
  callBack
}) {
  const currency = [
    {
      img: btc,
      title: "Bitcoin",
      code: "BTC"
    },
    {
      img: eth,
      title: "Ethereum",
      code: "ETH"
    },
    // {
    //   img: gx,
    //   title: "GXToken",
    //   code: "GXT"
    // },
    {
      img: usdt,
      title: "Tether",
      code: "USDT"
    },
    // {
    //   img: gx,
    //   title: "SEFCoin",
    //   code: "SEF"
    // }
  ];

  const [activeCurrency, setActiveCurrency] = useState("BTC");
  const [currencyType, setCurrency] = useState("BTC");
  const [image, setImage] = useState(btc);

  useEffect(() => {
    console.log("activeCurrency :" + currencyType, activeCurrency);

    callBack(activeCurrency, currencyType, image);
  }, [activeCurrency, currencyType, image]);

  return (
    <div
      className={
        currencyDropdown ? "position-absolute main-dropdown" : "d-none"
      }
    >
      {currency.map(x => (
        <div
          key={x.code}
          onClick={() => {
            setCurrency(x.code);
            setCurrencyDropdown(false);
            setActiveCurrency(x.code);
            setImage(x.img);
          }}
          className="d-flex flex-row m-1 btn country-list"
        >
          <img height="16px" width="16px" src={x.img} alt="" />
          <span>{x.code}</span>
        </div>
      ))}
    </div>
  );
}
