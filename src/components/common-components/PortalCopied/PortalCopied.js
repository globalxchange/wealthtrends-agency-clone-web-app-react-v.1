import React from "react";
import ReactDOM from "react-dom";
import Images from "../../../assets/a-exporter";
import { Agency } from "../../Context/Context";
import "./portal-copied.style.scss";

export default function PortalCopied() {
  const agency = React.useContext(Agency);
  const { setCopiedOverlay, copiedOverlay } = agency;
  return (
    <div
      className={`copied-portal ${copiedOverlay.status ? "" : "close-overlay"}`}
    >
      <div
        className={`copied-card ${
          copiedOverlay.status ? "full-width" : "no-width"
        }`}
      >
        <div className="copied-card-left">
          <img src={Images.copiedLogo} />
          <div>
            <h6>{copiedOverlay.title}</h6>
            <p>{copiedOverlay.data}</p>
          </div>
        </div>
        <div
          onClick={() => setCopiedOverlay({ ...copiedOverlay, status: false })}
          className="copied-card-right"
        >
          <img src={Images.addDark} />
        </div>
      </div>
    </div>
  );
}
