import React, { useContext, useRef, useState, useEffect } from 'react'
import './main-modal.style.scss'
import { Agency } from '../../Context/Context'
import ReactCrop from 'react-image-crop'

export default function MainModal({ children }) {
    const cropperRef = useRef()
    const agency = useContext(Agency);
    const { mainModal, setMainModal, setAspectRatio, aspectRatio, croppingImage, manualUpload, imageUploader, setCroppedImage, croppedImage, imageUploading } = agency;
    const [crop, setCrop] = useState({ aspect: 1 / 1 });
    // const [croppedImage, setCroppedImage] = useState()
    let imageRef = useRef()
    const handleImageLoaded = (image) => {
        imageRef.current = image
    }
    useEffect(() => {
        if (mainModal) {
            document.addEventListener("mousedown", handleMouseDown);
        } else {
            document.removeEventListener("mousedown", handleMouseDown);
        }
    }, [mainModal])
    const handleMouseDown = (e) => {
        if (cropperRef.current === null) return;
        else if (cropperRef.current.contains(e.target)) {
        } else {
            setMainModal(false)
        }
    }
    const onCropComplete = async (aspectRatio) => {
        if (imageRef.current && aspectRatio.width && aspectRatio.height) {
            const croppedImageUrl = await getCroppedImg(imageRef.current, aspectRatio);
            console.log("cropped", croppedImageUrl)
            // setCroppedImageURL(croppedImageUrl)

        }
    }
    const getCroppedImg = (image, aspectRatio) => {

        console.log("cropped  in", aspectRatio)
        const canvas = document.createElement("canvas");
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = aspectRatio.width;
        canvas.height = aspectRatio.height;
        const ctx = canvas.getContext("2d");

        ctx.drawImage(
            image,
            aspectRatio.x * scaleX,
            aspectRatio.y * scaleY,
            aspectRatio.width * scaleX,
            aspectRatio.height * scaleY,
            0,
            0,
            aspectRatio.width,
            aspectRatio.height
        )

        return new Promise((resolve, reject) => {
            const reader = new FileReader()
            canvas.toBlob(blob => {
                blob.name = 'cropped.jpg';
                resolve(blob);

                reader.readAsDataURL(blob)
                reader.onloadend = () => {
                    dataURLtoFile(reader.result, 'cropped.jpg')
                }
            }, 'image/jpeg', 1);
        });
    }
    const dataURLtoFile = (dataurl, filename) => {

        let arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);

        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        let croppedImg = new File([u8arr], filename, { type: mime });
        console.log("cropped  in", croppedImg)
        setCroppedImage({ done: false, url: croppedImg })
    }
    React.useEffect(() => {
        if (!croppedImage.done || manualUpload) return;
        imageUploader(croppedImage.url);
        setMainModal(false)
        setAspectRatio({ aspect: 1 / 1 })
    }, [croppedImage])
    return (
        <div className={mainModal ? "main-modal" : "d-none"}>
            <div ref={cropperRef} className="cropper-wrapper">
                <div className="wrapper-header"><h6>Crop You Image</h6></div>
                <div className="wrapper-body">
                    {MainModal ? <ReactCrop src={croppingImage} crop={aspectRatio} onChange={nCrop => setAspectRatio(nCrop)} onImageLoaded={handleImageLoaded} onComplete={onCropComplete} /> : ''}
                </div>
                <div className={`wrapper-footer ${!croppedImage.url?"disable-footer":""}`} onClick={() => { setCroppedImage({ ...croppedImage, done: true }); }}><h6>{imageUploading ? 'Uploading. Please Wait....' : 'Continue'}</h6></div>
            </div>
        </div>
    )
}
