import React,{useState,useEffect} from 'react'
import  './Controller.scss'
// import Moment from 'moment';
// import { extendMoment } from 'moment-range';
import {Line} from 'react-chartjs-2';
import{ControllDetail ,Alltimelist,Allprofite,allAffiliates,Alldata,allyear,Allrevenu,AllUser,AllCustomers,AllTxn,oneTimes,Reocurring} from './ControllerApi'
// import downs from '../../../images/down.png'
import downs from '../../../../assets/downarrow.png' 
import { Menu, Dropdown } from 'antd';
import axios from "axios";
import clipboard from '../../../../assets/clipboard.png'
import rect from '../../../../assets/rect.png'
import ys from '../../../../assets/Y.png'
import S1 from '../../../../assets/s1.png'
import S2 from '../../../../assets/s2.png'
import S3 from '../../../../assets/s3.png'
import Searchfliter from '../../../../assets/Searchfliter.png'
import { Tabs } from 'antd';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import  Chart  from  './ControllerChat'
import {v4 as uuidv4} from 'uuid'
import NewUserController from './NewUserController'
import Successfull from './SucessFullpage'
import CryptoJS from "crypto-js";
// const Moment = require('moment');
// const MomentRange = require('moment-range');
export default function Controller() {

const [user, setuse] = useState(0)
const [productprice, setproductprice] = useState(0)
const [Useradd,setUseradd]=useState([])
const [liveUser,setliveUser]=useState("By User")
const [Usercollection,setUsercollection]=useState([])
const [UsercollectionFliter,setUsercollectionFliter]=useState([])
const [interestratedifferential,setinterestratedifferential]=useState(0)
const [LiveUserEmail,setLiveUserEmail]=useState(" No Email")
const [LiveUserName,setLiveUserName]=useState("All Users")
const [activeuser, setactiveuser] = useState(0)
const [countwithdraw,setcountwithdraw]=useState(0)
const [tradingRenve,settradingRenve]=useState(0)
const [countdeposit, setcountdeposit] = useState(0)
const [productname,setproductname]=useState("All Product")
const [prodropdown, setprodropdown] = useState(false)
const [alldropdown, setalldropdown] = useState(false)
const [altimename,setaltimename]=useState('All Time')
const [revenue, setrevenue] = useState(0)
const [product, setproduct] = useState(ControllDetail)
const [Alltime, setAlltime] = useState([])
const [Alltimeprofite,setAlltimeprofite]=useState(Allprofite)
const [txn, settxn] = useState(0)
const [Deposit, setsetDeposit]=useState(0)
const [auser, setauser]=useState(0)
const [oneTime, setoneTime]=useState(0)
const [revpertxn, setrevpertxn]=useState(0)
const [Subscribers, setSubscribers]=useState(0)
const [Withdraw, setWithdraw]=useState(0)
const [tabsname, settabsname] = useState('Profit')
const [Affcostpertxn,setAffcostpertxn]=useState(0)
const [userclassname,setuserclassname]=useState('controller-firstsection')
const [revprofitclassname,setrevclassname]=useState('controller-Sectionsection')
const [profitclassname,setactiveuserclassname]=useState('controller-Sectionsection')
const [customerclassname,setcustomerclassname]=useState('controller-div-section')
const [affilateclassname,setaffilateclassname]=useState('controller-div-section')
const [txnsclassname,settxnsclassname]=useState('controller-foursection')
const [OneTimeclassname,setOneTimeclassname]=useState('controller-foursection')
const [Subscribersclassname,setSubscribersclassname]=useState('controller-foursection')
const [RevPerTxnclassname,setRevPerTxnclassname]=useState('controller-foursection')
const [AffiliateCostclassname,setWithdrawclassname]=useState('controller-foursection-cust')
const [AffCostPerTxnclassname,setAffCostPerTxnclassname]=useState('controller-foursection-cust')
const [lableyear,setlableyear]=useState([])
const [sad, setsad]=useState([])
const [UserAction,setUserAction]=useState("chart")
const [NewUserName,SetNewUserName]=useState("")
const [NewUserEmail,SetNewUserEmail]=useState("")
const [userlist, setuserlist]=useState([])
const [EmailList, setEmailList]=useState([])
const [Gxidcheck,setGxidcheck]=useState(false)
const [GxEmailcheck,setGxEmailcheck]=useState(false)
const [showCreateuser,setshowCreateuser]=useState(true)
const [muiltpleuser,setmuiltpleuser]=useState(false)
const [Loading,setLoading]=useState(false)


function callback(key) {
  console.log(key);
}

const callmainaddsurefunction=()=>
{
  setUserAction("NewUser")
}
const callbackmainaddsurefunction=()=>
{
  setUserAction("chart")
}
const backadduserfunction=()=>{
  setmuiltpleuser(true)
  setshowCreateuser(true)
  SetNewUserName("")
  SetNewUserEmail("")
  setGxEmailcheck(false)
  setGxidcheck(false)
}

const addmultipleuserfunction=()=>{
 
    if (NewUserName!== "" && NewUserEmail!=="" && GxEmailcheck===true && Gxidcheck===true ) {
 
        var newItem = {
         name:NewUserName,
         email:NewUserEmail,
          key:  uuidv4()
        };
     setUseradd(Useradd.concat(newItem))
    setmuiltpleuser(true)
    SetNewUserName("")
    SetNewUserEmail("")
    setGxEmailcheck(false)
    setGxidcheck(false)
    
      }
    else
    {
      alert("User Already Exits")
    }
}
const LiveData=[
  {
    name:"By User"
  },
  {
    name:"By Product"
  }
]
const sucessdata=[
  {
    name:`Reach Out To ${NewUserName}`,
    img:S1
    
  },
  {
    name:`Configure ${NewUserName} Account`,
    img:S2
  },
  {
    name:"Close User Creation Manager",
    img:S3
  },
]
const InputVlae=async(e)=>{
  
 await SetNewUserName(e)
  console.log("NewUserName",NewUserName)
}
const InputVlae1=(e)=>{
  
  SetNewUserEmail(e)
     
}



const newuseraddfunction=()=>
{
  setshowCreateuser(false)
  setLoading(true)

   if (NewUserName!== "" && NewUserEmail!=="") {
   axios.post("https://gxauth.apimachine.com/gx/user/admin/signup", {
    username:NewUserName,
      email:NewUserEmail,
      app_code:localStorage.getItem("appCode"),
  
  })
    .then(async res => {
     
      if(res.data.status)
      {
        setLoading(false)
        setUserAction("Sucess")
       console.log(res.data,"man")
      } 
      else
      {
        setLoading(false);
        setshowCreateuser(true)
      alert("some thing went Worng")  
      
      }
  })
}
else
{
  setLoading(false);
  setshowCreateuser(true)
  alert("Please Fill the Flieds")  
}

}
const checkuser=async()=>{

  let data = await axios.get("https://comms.globalxchange.com/listUsernames");
  if(data.data.status){
    let payload = data.data.payload;
    let decrypt = CryptoJS.Rabbit.decrypt(payload, "gmBuuQ6Er8XqYBd");
    decrypt = decrypt.toString(CryptoJS.enc.Utf8);
    decrypt = JSON.parse(decrypt);
    console.log("decrypt");
    console.log(decrypt);
    let usernames = decrypt.usernames ? decrypt.usernames : [];
    let emails = decrypt.emails ? decrypt.emails : [];
    await setuserlist(usernames)
    await setEmailList(emails)
  
  }
}
const usernameValid = async(value)=>{

  //console.log(value);
  if(value.length>3){
    if(userlist.includes(value.toLowerCase() )){
    setGxidcheck(false);
    } else {setGxidcheck(true);}
  } else {
    setGxidcheck(false);}}


const addItem=(e)=> {
 

    if (NewUserName!== "" && NewUserEmail!=="") {
 
      var newItem = {
       name:NewUserName,
       email:NewUserEmail,
        key:  uuidv4()
      };
   setUseradd(Useradd.concat(newItem))
  setshowCreateuser(false)
  SetNewUserName("")
  SetNewUserEmail("")
  
    }
}

const emailValid = async (value)=>{

  let isValid = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value.toLowerCase());
  if(isValid){
    if(EmailList.includes(value.toLowerCase())){
      //console.log("Email Exists");
      setGxEmailcheck(false);
     
    } else {
      //console.log("Emal Valid");
      setGxEmailcheck(true);
    }
    }
    else {
      setGxEmailcheck(false);
      
    }

  } 
 
const data = {
  labels:lableyear,

  datasets: [
    {
      label:`${tabsname}`,
      fill: false,
      lineTension: 0,
      scaleLineColor: 'transparent',
      backgroundColor: '#182542',
      borderColor: '#182542',
      borderCapStyle: 'b',
      steppedLine:false,
      showLin:false,
      // borderColor:"transparent",
      maintainAspectRatio: false,
      borderDash: [0],
      borderDashOffset: 0.0,
      borderJoinStyle: 'm',
      pointBorderColor: '#000000',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 5,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: '#000000',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data:sad
    }
  ]
};
const menu = (
  <Menu>
    <p className="menu-drp-cont">X-Axis Frequency</p>
    <Menu.Item key="0">
      <a className="menu-drp-cont-a">Years</a>
    </Menu.Item>
    <Menu.Item key="1">
    <a className="menu-drp-cont-a">Quarters</a>
    </Menu.Item>
    <Menu.Item key="2">
    <a className="menu-drp-cont-a">Months</a>
    </Menu.Item>
       <Menu.Item key="3">
    <a className="menu-drp-cont-a">Weeks</a>
    </Menu.Item>
    <Menu.Item key="4">
    <a className="menu-drp-cont-a">Days</a>
    </Menu.Item>
  </Menu>
);
const AnalyStep = () => {
  switch (UserAction) {
    case "chart":
      return(
  <Chart
        Alltimeprofite={Alltimeprofite}
        tabsname={tabsname}
        data={data}
        callback={callback}
        menu={menu}
        />
      )
    case "NewUser":
      return (
      

        <NewUserController
        callbackmainaddsurefunction={callbackmainaddsurefunction}
        InputVlae={InputVlae}
        addItem={addItem}
        InputVlae1={InputVlae1}
        usernameValid={usernameValid}
        Gxidcheck={Gxidcheck}
        emailValid={emailValid}
        backadduserfunction={backadduserfunction}
        Useradd={Useradd}
        GxEmailcheck={GxEmailcheck}
        showCreateuser={showCreateuser}
        muiltpleuser={muiltpleuser}
        newuseraddfunction={newuseraddfunction}
        Loading={Loading}
        NewUserName={NewUserName}
        NewUserEmail={NewUserEmail}
        addmultipleuserfunction={addmultipleuserfunction}
        />

    
      );
      case "Sucess":
        return(
    <Successfull
        sucessdata={sucessdata}
        NewUserEmail={NewUserEmail}
        NewUserName={NewUserName}
        />
      )
  }

}
const { TabPane } = Tabs;

const LiveUserClickfung=(item)=>
{
  setLiveUserEmail(item.email)
  setLiveUserName(item.name)
}
const LiveUserUpdateFun=(item)=>
{
  setliveUser(item)
}





const fetchBalances = async ()=>{
  await axios
    .get(
      `https://comms.globalxchange.com/coin/vault/service/app/revenue/data/get?app_code=${localStorage.getItem(
        "appCode"
      )}`
    )
   .then(res => {
     console.log("asdasd",res)
      setrevenue(res.data.revenue)
    });

  

    
  

}
const fetchBalanceuse= async ()=>{
  await axios
    .get(
      `https://comms.globalxchange.com/gxb/apps/users/get?app_code=${localStorage.getItem(
        "appCode"
      )}`
    )
   .then(res => {
     if(res.status)
     {
    
      setuse(res.data.users.length)
     }

    });

  

    
  

}
const fetchActiveuser= async ()=>{
  await axios
    .get(
      `https://nitrogen.apimachine.com/activeInactiveUser?appCode=${localStorage.getItem(
        "appCode"
      )}`
    )
   .then(res => {
     if(res.status)
     {
       let s=[res.data]
      console.log("aszdasdasd",s[0]['Active User'].length)
       setactiveuser(s[0]['Active User'].length)
     }

    });
}
const fetchDeposit= async ()=>{
  await axios
    .get(
      `https://nitrogen.apimachine.com/totalSubAppDeposits?subApp=${localStorage.getItem(
        "appCode"
      )}`
    )
   .then(res => {
  if(res.data.status===true)
     {
    
      console.log("aszdsadasdasdasd",res)
       let s=[res.data]
       let balaance=s[0]['data']['Internal Deposit']['USD_value'] + s[0]['data']['external Deposit']['USD_value']
       let count=s[0]['data']['Internal Deposit']['count'] + s[0]['data']['external Deposit']['count']
  
     console.log("aszdsadasdasdasd",count)
     setsetDeposit(balaance)
     setcountdeposit(count)
   
     }
     else
     {
      console.log("aszdsadasdasdasd",res)
      setsetDeposit(0)
      setcountdeposit(0)
     }

    });
}
const fetchWidthdraw= async ()=>{
  await axios
    .get(
      `https://nitrogen.apimachine.com/totalSubAppWithdrawal?subApp=${localStorage.getItem(
        "appCode"
      )}`
    )
   .then(res => {
  if(res.data.status===true)
     {
    
      console.log("aszdsadasdasdasd",res)
       let s=[res.data]
       let balaance=s[0]['data']['External Withdrawal']['USD Total'] + s[0]['data']['Internal Withdrawal']['USD_value']
       let count=s[0]['data']['External Withdrawal']['Count'] + s[0]['data']['Internal Withdrawal']['count']
  
     console.log("aszdsadasdasdasd",count)
     setWithdraw(balaance)
     setcountwithdraw(count)
   
     }
     else
     {
      console.log("aszdsadasdasdasd",res)
      setsetDeposit(0)
      setcountwithdraw(0)
     }

    });
}
const TradingRenve= async ()=>{
  await axios
    .get(
      `https://comms.globalxchange.com/coin/vault/service/app/revenue/data/get?revenue_from=trade&app_code=${localStorage.getItem(
        "appCode"
      )}`
    )
   .then(res => {
  if(res.data.status===true)
     {
    settradingRenve(res.data.revenue)
   
   
     }
     else
     {
      settradingRenve(0)
     }

    });
}
const interestratedifferentialFunction= async ()=>{
  await axios
    .get(
      `https://comms.globalxchange.com/coin/vault/service/app/revenue/data/get?app_code=${localStorage.getItem("appCode")}&revenue_from=interest_rate_differential`
    )
   .then(res => {
  if(res.data.status===true)
     {
      setinterestratedifferential(res.data.revenue)
   
   
     }
     else
     {
      setinterestratedifferential(0)
     }

    });
}
const productsFunction= async ()=>{

  await axios
    .get(
      `https://comms.globalxchange.com/coin/vault/service/app/revenue/data/get?app_code=${localStorage.getItem("appCode")}&revenue_from=products`
    )
   .then(res => {
  if(res.data.status===true)
     {
      setproductprice(res.data.revenue)
   
   
     }
     else
     {
      setproductprice(0)
     }

    });
}
const liveuserinfofuntion= async ()=>{
  await axios
    .get(
      `https://comms.globalxchange.com/gxb/apps/users/get?app_code=${localStorage.getItem(
        "appCode"
      )}`
    )
   .then(res => {
  if(res.data.status===true)
     {
      setUsercollection(res.data.users)
   
      setUsercollectionFliter(res.data.users)
     }
     else
     {
      settradingRenve(0)
     }

    });
}

useEffect( ()=>{
   fetchBalances() 
   fetchBalanceuse()
   fetchActiveuser()
   fetchDeposit()
   fetchWidthdraw()
   TradingRenve()
   productsFunction()
   interestratedifferentialFunction()
   liveuserinfofuntion()
   checkuser()
}, [] );

const handleatlasChangetable = async e => {

  let value =e.target.value;


 let n1= UsercollectionFliter.filter((user)=>{
        return (user.name).toUpperCase().includes((value).toUpperCase());
   
  })
  setUsercollection(n1)
}

const AlltimefliterFuntion=async(item)=>
{
  setaltimename(item)
 const asd=async()=>
  {
    setalldropdown(!alldropdown)
    if(item==="Past 30 Days")
    {
      let seventhDay = new Date();
      seventhDay.setDate(seventhDay.getDate() - 30);
      
      let filteredData = ControllDetail.filter((d) => {
        return new Date(d.datelist)>= seventhDay;
      });
  
      await setAlltime(filteredData)
  }
}

  await asd()
if(Alltime.length>0)
{

 await fliteringdatafunction()

}


 if(item==="Month To Date")
  {
    let currentMonth = new Date().getMonth() + 1
    let currentYear = new Date().getFullYear()
    
    //Get the year and month from the iterated date
    
    console.log("currentMonth",currentMonth)
    //Then filter the dates
    let events = ControllDetail.filter(e => {
      console.log("sadasdasdasdwtf",e.datelist.split('/')[0])
     let month= e.datelist.split('/')[0]; // Or, var month = e.date.split('-')[1];
        let year=e.year
        return (currentMonth === +month)  && (currentYear === year) ;
    });

    setproduct(events)


    let sum = product.map(o => o.profit).reduce((a, c) => { return a + c });
    let sum1 = product.map(o => o.revenue).reduce((a, c) => { return a + c });
    let txn = product.map(o => o.txn).reduce((a, c) => { return a + c });
    let onetime= product.map(o => o.onetime).reduce((a, c) => { return a + c });
    let Subscribers=product.map(o => o.subscriber).reduce((a, c) => { return a + c });
    let revpertxn=product.map(o => o.revpertxn).reduce((a, c) => { return a + c });
    let  AffiliateCost=product.map(o => o.afficatecost).reduce((a, c) => { return a + c });
    let  affcostpertxn=product.map(o => o.affcostpertxn).reduce((a, c) => { return a + c });
  
    setAffcostpertxn(affcostpertxn)
    setSubscribers(Subscribers)
    setrevpertxn(revpertxn)
    settxn(txn)
    setoneTime(onetime)

   let sigle= product.filter(item=>
      {
        return item.mode==="customers"
      })
  
  
      let affiliate= product.filter(item=>
        {
          return item.mode==="Affiliates"
        })
      setauser(affiliate.length)
 
    // setactiveuser(sum?sum:"loading")
    // setrevenue(sum1?sum1:"loading")
    // setuse(product.length?product.length:"loading")
    
  }

  
 if(item==="1 Year" ||item==="Past 1 Year" ||item==="Year To Date"  )
 {

   let currentYear = new Date().getFullYear()

   //Then filter the dates
   let eventxs = ControllDetail.filter(e => {
       let year=e.year
       return (currentYear === year) ;
   });

   setproduct(eventxs)


   let sum = product.map(o => o.profit).reduce((a, c) => { return a + c });
   let sum1 = product.map(o => o.revenue).reduce((a, c) => { return a + c });
   let txn = product.map(o => o.txn).reduce((a, c) => { return a + c });
   let onetime= product.map(o => o.onetime).reduce((a, c) => { return a + c });
   let Subscribers=product.map(o => o.subscriber).reduce((a, c) => { return a + c });
   let revpertxn=product.map(o => o.revpertxn).reduce((a, c) => { return a + c });
   let  AffiliateCost=product.map(o => o.afficatecost).reduce((a, c) => { return a + c });
   let  affcostpertxn=product.map(o => o.affcostpertxn).reduce((a, c) => { return a + c });
 
   setAffcostpertxn(affcostpertxn)
   setSubscribers(Subscribers)
   setrevpertxn(revpertxn)
   settxn(txn)
   setoneTime(onetime)

  let sigle= product.filter(item=>
     {
       return item.mode==="customers"
     })
 
 
     let affiliate= product.filter(item=>
       {
         return item.mode==="Affiliates"
       })
     setauser(affiliate.length)

  //  setactiveuser(sum?sum:"loading")
  //  setrevenue(sum1?sum1:"loading")
  //  setuse(product.length?product.length:"loading")
   
 }

}
  // if(item==="Month To Date")
  // {
  //   let currentMonth = new Date().getMonth() + 1
  //   let currentYear = new Date().getFullYear()
    
  //   //Get the year and month from the iterated date
    
  //   console.log("currentMonth",currentMonth)
  //   //Then filter the dates
  //   let events = ControllDetail.filter(e => {
  //     console.log("sadasdasdasdwtf",e.datelist.split('/')[0])
  //       let  month = e.datelist.split('/')[0]; // Or, var month = e.date.split('-')[1];
  //       return (currentMonth === +month) ;
  //   });

  //   setproduct(events)


  //   let sum = product.map(o => o.profit).reduce((a, c) => { return a + c });
  //   let sum1 = product.map(o => o.revenue).reduce((a, c) => { return a + c });
  //   let txn = product.map(o => o.txn).reduce((a, c) => { return a + c });
  //   let onetime= product.map(o => o.onetime).reduce((a, c) => { return a + c });
  //   let Subscribers=product.map(o => o.subscriber).reduce((a, c) => { return a + c });
  //   let revpertxn=product.map(o => o.revpertxn).reduce((a, c) => { return a + c });
  //   let  AffiliateCost=product.map(o => o.afficatecost).reduce((a, c) => { return a + c });
  //   let  affcostpertxn=product.map(o => o.affcostpertxn).reduce((a, c) => { return a + c });
  //   setWithdraw(AffiliateCost)
  //   setAffcostpertxn(affcostpertxn)
  //   setSubscribers(Subscribers)
  //   setrevpertxn(revpertxn)
  //   settxn(txn)
  //   setoneTime(onetime)

  //  let sigle= product.filter(item=>
  //     {
  //       return item.mode==="customers"
  //     })
  
  
  //     let affiliate= product.filter(item=>
  //       {
  //         return item.mode==="Affiliates"
  //       })
  //     setauser(affiliate.length)
  // setsetDeposit(sigle.length)
  //   setactiveuser(sum?sum:"loading")
  //   setrevenue(sum1?sum1:"loading")
  //   setuse(product.length?product.length:"loading")
    
  // }







const fliteringdatafunction=async()=>
{

  let sum = Alltime.map(o => o.profit).reduce((a, c) => { return a + c });
  let sum1 = Alltime.map(o => o.revenue).reduce((a, c) => { return a + c });
  let txn = Alltime.map(o => o.txn).reduce((a, c) => { return a + c });
  let onetime= Alltime.map(o => o.onetime).reduce((a, c) => { return a + c });
  let Subscribers=Alltime.map(o => o.subscriber).reduce((a, c) => { return a + c });
  let revpertxn=Alltime.map(o => o.revpertxn).reduce((a, c) => { return a + c });
  let  AffiliateCost=Alltime.map(o => o.afficatecost).reduce((a, c) => { return a + c });
  let  affcostpertxn=Alltime.map(o => o.affcostpertxn).reduce((a, c) => { return a + c });

  await setAffcostpertxn(affcostpertxn)
  await setSubscribers(Subscribers)
  await setrevpertxn(revpertxn)
  await settxn(txn)
  await setoneTime(onetime)
 let sigle= Alltime.filter(item=>
    {
      return item.mode==="customers"
    })


    let affiliate= ControllDetail.filter(item=>
      {
        return item.mode==="Affiliates"
      })
      await setauser(affiliate.length)
     
      // await setactiveuser(sum)
      await setrevenue(sum1?sum1:"loading")
      // await setuse(Alltime.length?Alltime.length:"loading")
}


const prodctbasefliterfunction=(item)=>
{

  setprodropdown(!prodropdown)
  setproductname(item.product)
if(item.mode==="customers")
{
 
}
else
{
 
}
if(item.mode==="Affiliates")
{
  setauser(1)
}
else
{
  setauser(0)
}


setuse(1)

  // setactiveuser(item.profit)
  // setrevenue(item.revenue)
  setSubscribers(item.subscriber)
  setrevpertxn(item.revpertxn)
  settxn(item.txn)
  setoneTime(item.onetime)

  setAffcostpertxn(item.affcostpertxn)
}
 const fetchcustomdata=(data)=>
{

    
   let s= ControllDetail.map(item=>
    {
        return(
            item.profit

        )
    })
   setsad(s)
}
const fetchcustomdataaffiliate=(data)=>
{

    
   let s= ControllDetail.map(item=>
    {
        return(
            item.afficatecost

        )
    })
   setsad(s)
}

const fetchcustomdatarevenue=()=>
{

    
   let s= ControllDetail.map(item=>
    {
        return(
            item.revenue

        )
    })
   setsad(s)
}
const fetchcustomdataonetime=()=>
{

    
   let s= ControllDetail.map(item=>
    {
        return(
            item.onetime

        )
    })
   setsad(s)
}


const fetchcustomdatarecording=()=>
{

    
   let s= ControllDetail.map(item=>
    {
        return(
            item.subscriber

        )
    })
   setsad(s)
}

const fetchcustomdatataxn=()=>
{

    
   let s= ControllDetail.map(item=>
    {
        return(
            item.txn

        )
    })
   setsad(s)
}
const fetchcustomdatacustomer=()=>
{

    
   let s= ControllDetail.map(item=>
    {
        return(
            item.mode.length

        )
    })
   setsad(s)
}
const fetchcustomuesr=(data)=>
{

    
   let s= ControllDetail.map(item=>
    {
        return(
            item.id

        )
    })
   setsad(s)
}



const fetchcustomdatayear=()=>
{

    
   let s= ControllDetail.map(item=>
    {
        return(
            item.totaldate

        )
    })
   setlableyear(s)
}

useEffect(() => {


 
  fetchcustomdata('profit')
  // const data=fetchcustomdata('profit')
  fetchcustomdatayear()
 
  // const moment = MomentRange.extendMoment(Moment);
  
  // const start = moment()
  // const end = moment().add(2, 'months')
  // const range = moment.range(start, end)
  // const arrayOfDates = Array.from(range.by('days'))
  // console.log(arrayOfDates)

  // var seventhDay = new Date();
  //   seventhDay.setDate(seventhDay.getMonth());
    
  //   var filteredData = product.filter((d) => {
  //     return new Date(d.datelist) === seventhDay;

  //   });





  let sum = product.map(o => o.profit).reduce((a, c) => { return a + c });
  let sum1 = product.map(o => o.revenue).reduce((a, c) => { return a + c });
  let txn = product.map(o => o.txn).reduce((a, c) => { return a + c });
  let onetime= product.map(o => o.onetime).reduce((a, c) => { return a + c });
  let Subscribers=product.map(o => o.subscriber).reduce((a, c) => { return a + c });
  let revpertxn=product.map(o => o.revpertxn).reduce((a, c) => { return a + c });
  let  AffiliateCost=product.map(o => o.afficatecost).reduce((a, c) => { return a + c });
  let  affcostpertxn=product.map(o => o.affcostpertxn).reduce((a, c) => { return a + c });
  
  setAffcostpertxn(affcostpertxn)
  setSubscribers(Subscribers)
  setrevpertxn(revpertxn)
  settxn(txn)
  setoneTime(onetime)
 let sigle= product.filter(item=>
    {
      return item.mode==="customers"
    })


    let affiliate= product.filter(item=>
      {
        return item.mode==="Affiliates"
      })
    setauser(affiliate.length)

  // setactiveuser(sum?sum:"loading")
  // setrevenue(sum1?sum1:"loading")
  // setuse(product.length?product.length:"loading")
  // console.log("fetchcustomdatafetchcustomdata",events)
  setactiveuserclassname('controller-thridsection')
  return () => {
  }

},[] )


const UserClicksfunction=(item)=>
{
  if(item==='user')
  {
    fetchcustomuesr()
    settabsname(item)
    setuserclassname('controller-firstsectionTrue')
    setrevclassname('controller-Sectionsection')
    setactiveuserclassname('controller-Sectionsection')
    setcustomerclassname('controller-div-section')
    setaffilateclassname('controller-div-section')
    setAlltimeprofite(AllUser)
    settxnsclassname('controller-foursection')
    setOneTimeclassname('controller-foursection')
    setSubscribersclassname('controller-foursection')
    setRevPerTxnclassname('controller-foursection')
    setWithdrawclassname('controller-foursection-cust')
    setAffCostPerTxnclassname('controller-foursection-cust')
  }
  if(item==='Revenue')
  {
    fetchcustomdatarevenue()
    settabsname(item)
    setAlltimeprofite(Allrevenu)
  setrevclassname('controller-thridsection')
  setuserclassname('controller-firstsection')
  setactiveuserclassname('controller-Sectionsection')
  setcustomerclassname('controller-div-section')
  setaffilateclassname('controller-div-section')
  settxnsclassname('controller-foursection')
  setOneTimeclassname('controller-foursection')
  setSubscribersclassname('controller-foursection')
  setRevPerTxnclassname('controller-foursection')
  setWithdrawclassname('controller-foursection-cust')
  setAffCostPerTxnclassname('controller-foursection-cust')
}
  if(item==='Profit')
  {
    fetchcustomdata()
    settabsname(item)
    setactiveuserclassname('controller-thridsection')
    setrevclassname('controller-Sectionsection')
    setAlltimeprofite(Allprofite)
    setuserclassname('controller-firstsection')
    setcustomerclassname('controller-div-section')
    setaffilateclassname('controller-div-section')
    settxnsclassname('controller-foursection')
    setOneTimeclassname('controller-foursection')
    setSubscribersclassname('controller-foursection')
    setRevPerTxnclassname('controller-foursection')
    setWithdrawclassname('controller-foursection-cust')
    setAffCostPerTxnclassname('controller-foursection-cust')
  }
  if(item==='Customers')
  {
    fetchcustomdatacustomer()
    settabsname(item)
    setAlltimeprofite(AllCustomers)
    setactiveuserclassname('controller-Sectionsection')
    setrevclassname('controller-Sectionsection')
    setuserclassname('controller-firstsection')
    setcustomerclassname('controller-div-sectionTrue')
    setaffilateclassname('controller-div-section')
    settxnsclassname('controller-foursection')
    setOneTimeclassname('controller-foursection')
    setSubscribersclassname('controller-foursection')
    setRevPerTxnclassname('controller-foursection')
    setWithdrawclassname('controller-foursection-cust')
    setAffCostPerTxnclassname('controller-foursection-cust')
  }
  // if(item==='Affiliates')
  // {
  //   settabsname(item)
  //   setactiveuserclassname('controller-Sectionsection')
  //   setrevclassname('controller-Sectionsection')
  //   setuserclassname('controller-firstsection')
  //   setcustomerclassname('controller-div-section')
  //   setaffilateclassname('controller-div-sectionTrue')
  //   settxnsclassname('controller-foursection')
  //   setOneTimeclassname('controller-foursection')
  //   setSubscribersclassname('controller-foursection')
  //   setRevPerTxnclassname('controller-foursection')
  //   setWithdrawclassname('controller-foursection-cust')
  //   setAffCostPerTxnclassname('controller-foursection-cust')
  // }
//   const [txnsclassname,settxnsclassname]=useState('controller-foursection')
// const [OneTimeclassname,setOneTimeclassname]=useState('controller-foursection')
// const [Subscribersclassname,setSubscribersclassname]=useState('controller-foursection')
// const [RevPerTxnclassname,setRevPerTxnclassname]=useState('controller-foursection')
  if(item==='Txns')
  {
    fetchcustomdatataxn()
    settabsname(item)
    setAlltimeprofite(AllTxn)

    settxnsclassname('controller-foursectionTrue')
    setOneTimeclassname('controller-foursection')
    setSubscribersclassname('controller-foursection')
    setRevPerTxnclassname('controller-foursection')
    setuserclassname('controller-firstsection')
    setrevclassname('controller-Sectionsection')
    setactiveuserclassname('controller-Sectionsection')
    setcustomerclassname('controller-div-section')
    setaffilateclassname('controller-div-section')
    setWithdrawclassname('controller-foursection-cust')
    setAffCostPerTxnclassname('controller-foursection-cust')
  }

  if(item==='OneTime')
  {
    fetchcustomdataonetime()
    settabsname(item)
    setAlltimeprofite(oneTimes)
    setOneTimeclassname('controller-foursectionTrue')
    settxnsclassname('controller-foursection')
    setSubscribersclassname('controller-foursection')
    setRevPerTxnclassname('controller-foursection')
    setuserclassname('controller-firstsection')
    setrevclassname('controller-Sectionsection')
    setactiveuserclassname('controller-Sectionsection')
    setcustomerclassname('controller-div-section')
    setaffilateclassname('controller-div-section')
    setWithdrawclassname('controller-foursection-cust')
    setAffCostPerTxnclassname('controller-foursection-cust')
  }
  if(item==='Reocurring')
  {
    fetchcustomdatarecording()
    settabsname(item)
    setAlltimeprofite(Reocurring)
    setSubscribersclassname('controller-foursectionTrue')
    setRevPerTxnclassname('controller-foursection')
    settxnsclassname('controller-foursection')
    setOneTimeclassname('controller-foursection')
    setuserclassname('controller-firstsection')
    setrevclassname('controller-Sectionsection')
    setactiveuserclassname('controller-Sectionsection')
    setcustomerclassname('controller-div-section')
    setaffilateclassname('controller-div-section')
    setWithdrawclassname('controller-foursection-cust')
    setAffCostPerTxnclassname('controller-foursection-cust')
  }
  if(item==='RevPerTxn')
  {
    settabsname(item)
    setRevPerTxnclassname('controller-foursectionTrue')
    settxnsclassname('controller-foursection')
    setSubscribersclassname('controller-foursection')
    setOneTimeclassname('controller-foursection')
    setuserclassname('controller-firstsection')
    setrevclassname('controller-Sectionsection')
    setactiveuserclassname('controller-Sectionsection')
    setcustomerclassname('controller-div-section')
    setaffilateclassname('controller-div-section')
    setWithdrawclassname('controller-foursection-cust')
    setAffCostPerTxnclassname('controller-foursection-cust')
  }


if(item==='AffiliateCost')
{
  fetchcustomdataaffiliate()
  settabsname(item)

  setWithdrawclassname('controller-foursection-custtrue')
  setuserclassname('controller-firstsection')
  setrevclassname('controller-Sectionsection')
  setactiveuserclassname('controller-Sectionsection')
  setcustomerclassname('controller-div-section')
  setaffilateclassname('controller-div-section')
  setAffCostPerTxnclassname('controller-foursection-cust')
  setSubscribersclassname('controller-foursection')
  setRevPerTxnclassname('controller-foursection')
  settxnsclassname('controller-foursection')
  setOneTimeclassname('controller-foursection')
}
if(item==='Affiliates')
{
  setAlltimeprofite(allAffiliates)
  settabsname(item)
  setWithdrawclassname('controller-foursection-cust')
  setWithdrawclassname('controller-foursection-custtrue')
  setuserclassname('controller-firstsection')
  setrevclassname('controller-Sectionsection')
  setactiveuserclassname('controller-Sectionsection')
  setcustomerclassname('controller-div-section')
  setaffilateclassname('controller-div-section')
  setSubscribersclassname('controller-foursection')
  setRevPerTxnclassname('controller-foursection')
  settxnsclassname('controller-foursection')
  setOneTimeclassname('controller-foursection')
}
}


console.log("productproductproduct",product)
console.log("fetchcustomdatafetchcustomdata",product)
console.log("Useradd",Useradd)
  return (
 
      <div className="row m-0 mainsectionpadding ">
              <div className="col-md-3 controller-sub-main-page">
    
    {/* <div class="dropdown">
<div className="drop-image-div" onClick={()=>setalldropdown(!alldropdown)}>
<button  class="dropbtn"> {altimename}</button>
<img src={downs} alt=""/>
</div>
{
  alldropdown?

<div id="myDropdown" class="dropdown-content">
<div className="drp-lenght">
{Alltimelist.length>0?
<>
  {
    Alltimelist.map(item=>
      {
      return(
        <>
    <p onClick={()=>AlltimefliterFuntion(item.name)}>{item.name}</p>
        </>
      )}
      )

  }
  </>
  :
<a>No More Matches....</a>
}
</div>
</div>
:null}
</div> */}

{/* <div class="dropdown asp">
<div className="drop-image-div" onClick={()=>setprodropdown(!prodropdown)}>
<button onclick="myFunction()" class="dropbtn"> {productname}</button>
<img src={downs} alt=""/>
</div>
{
  prodropdown?

<div id="myDropdown" class="dropdown-content">
  <div className="seachsection">
  <input type="text" placeholder="Search.." id="myInput" onChange={handleatlasChangetable}/>
<img src={education} alt="" className="searchicon"/>
</div>
<div className="drp-lenght">
{product.length>0?
<>
  {
    product.map(item=>
      {
      return(
        <>
    <p onClick={()=>prodctbasefliterfunction(item)}>{item.product}</p>
        </>
      )}
      )

  }
  </>
  :
<a>No More Matches....</a>
}
</div>
</div>
:null}
</div> */}


<div className="full-cntrl-sale">

<div className="sale-contrl-div">
  <div className="liveMain">


  {
    LiveData.map(item=>{
      return(
        <div  className={item.name===liveUser?"livesubdivActive":"livesubdiv"} onClick={()=>LiveUserUpdateFun(item.name)}>
<h3 className="fontLiveData">{item.name}</h3>
        </div>
       
        
      )
    })
    }
      </div>
</div>
<div className="mainSelectedcontroldiv">

<p  className="csalesheadercustom" >Currently Selected</p>
<div className="control-salescustom">

<div className="d-flex">
<img src={rect} alt=""/>
<div className="marginrightimg">
<p className=" csalesheader">{LiveUserName}</p>
<p className="cslaes-p"> {LiveUserEmail}</p>

</div>

</div>
<CopyToClipboard text={LiveUserName} >
        <img src={clipboard} alt=""/>
      </CopyToClipboard>
        </div>



</div>
<div className="seachsection">
  <input type="text" placeholder="Search All Your Users..|" id="myInput" onChange={handleatlasChangetable}/>
<img src={Searchfliter} alt="" className="searchicon"/> 
</div>
<div className="controller-sale-div-list">
{
  Usercollection.map(item=>
    {
      return(
        <div className="control-sales" >
<img src={rect} alt=""/>
<div className="marginrightimg">
<p className="csalesheader">{item.name}</p>
<p className="cslaes-p">{item.email}</p>
</div>

        </div>
      )
    }
  )
}
</div>
<div className="sale-contrl-divbottom">
  <p style={{margin:0}} onClick={callmainaddsurefunction}>Add New User</p>
</div>
</div>
    </div>
<div className="col-md-9 controller-main-page">

  
    <div className="controller">
        <div className="controll-mainfirst-section">
      <div className={userclassname}>
          <div className="controller-subdivsection" onClick={()=>UserClicksfunction('user')}>
          <p className="controller_P_text" >Users</p>
          </div>
          <div className="controller-subdivsecondsection">
          <p className="controller_p_num">{user}</p>              
       </div>
      </div>
<div className={revprofitclassname} > 
          <div className="controller-subdivsection" onClick={()=>UserClicksfunction('Revenue')}>
            <p className="controller_P_text">Revenue</p>
          </div>
          <div className="controller-subdivsecondsection">
    <p className="controller_p_num"> ${new Intl.NumberFormat().format(Number(`${revenue}`).toFixed(2))}</p>              
 </div>
      </div>



      <div className={profitclassname}>
          <div className="controller-subdivsection" onClick={()=>UserClicksfunction('Profit')}>
            <p className="controller_P_text">Customers</p>
          </div>
          <div className="controller-subdivsecondsection">
    <p className="controller_p_num"> {activeuser}</p>              
 </div>
 </div>
 </div>


<div className="maintab-controlller-sub">
    <div  className="controller-customdivision" >
<div className={customerclassname}onClick={()=>UserClicksfunction('Customers')}>
    <div>
    <p className="controller_p_num">${new Intl.NumberFormat().format(Number(`${Deposit}`).toFixed(2))} </p>
    <p className="controller_P_text-custom"><span style={{fontWeight:"bold"}}>{countdeposit}</span> Deposits</p>

    </div>
</div>
{/* <div className={affilateclassname}onClick={()=>UserClicksfunction('Affiliates')}>
    <div>
    <p className="controller_P_text-custom">Affiliates</p>
    <p className="controller_p_num">{auser}</p>
    </div>
</div> */}
    </div>

{/* kkkk */}
    <div  className="controller-Sectionsection-cuz" >
<div className={txnsclassname}onClick={()=>UserClicksfunction('Txns')}>
    <div>
    <p className="controller_p_num">${new Intl.NumberFormat().format(Number(`${tradingRenve}`).toFixed(2))} </p>
    <p className="controller_P_text-custom">Trading Revenue</p>
 
    </div>
</div>
<div className={OneTimeclassname}onClick={()=>UserClicksfunction('OneTime')}>
    <div>
    <p className="controller_p_num">${new Intl.NumberFormat().format(Number(`${interestratedifferential}`).toFixed(2))} </p>
          <p className="controller_P_text-custom">Interest Differential</p>
   
    </div>
</div>
<div className={Subscribersclassname}onClick={()=>UserClicksfunction('Reocurring')}>
    <div>
    <p className="controller_p_num">${new Intl.NumberFormat().format(Number(`${productprice}`).toFixed(2))} </p>
    <p className="controller_P_text-custom">Product Sales</p>
 
    </div>
</div>
{/* <div className={RevPerTxnclassname}onClick={()=>UserClicksfunction('RevPerTxn')}>
    <div>
    <p className="controller_P_text-custom">Rev Per Txn</p>
    <p className="controller_p_num">${revpertxn}</p>
    </div>
</div> */}
    </div>    





    <div  className="controller-Sectionsection-cuz">
<div className={AffiliateCostclassname}onClick={()=>UserClicksfunction('Affiliates')}>
    <div>
    <p className="controller_p_num">${new Intl.NumberFormat().format(Number(`${Withdraw}`).toFixed(2))}</p>
    <p className="controller_P_text-custom"><span style={{fontWeight:"bold"}}>{countwithdraw}</span> Withdrawals </p>
 
    </div>
</div>
{/* <div className={AffCostPerTxnclassname}onClick={()=>UserClicksfunction('AffCostPerTxn')}>
    <div>
    <p className="controller_P_text-custom">Aff Cost Per Txn</p>
    <p className="controller_p_num">${Affcostpertxn}</p>
    </div>
</div> */}
    </div>    



</div>




      </div>


{AnalyStep()}




      </div>


      {/* <div>
        <h2>Line Example</h2>
        <Line data={data} />
      </div> */}
    </div>
  )
}
