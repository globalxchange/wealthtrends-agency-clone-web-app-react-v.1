import React from 'react'
import clipboard from '../../../../assets/clipboard.png'
export default function SucessFullpage({sucessdata,NewUserEmail,NewUserName}) {
  return (
    <div className="todoListMain">

      <div className="header">
          <div>
        
      
          <h2 className="scessheader">Congratulations</h2>
          <p  className="sucessparagraph">{NewUserName} Is Now A {localStorage.getItem("appCode")} User. An email has been sent to {NewUserEmail} which contains the temporary login credentials to his/her new account.</p>


          <p className="sucessparagraph">Feel Free To Go Through The Following Resource If You Need More Clarity </p>
          <div className="buttoncomplete">
          Complete The Registration
          <img src={clipboard} alt=""/>
          </div>
      </div>
    
      </div>
      <div className="header">
        <div>
        <h2 className="scessheader">What Would You Like To Do Next?</h2>
         {
           sucessdata.map(item=>
            {
              return(
                <div className="suceessboxtext">
<img src={item.img} alt=""/>
<p className="textsuess">{item.name}</p>
                </div>
              )
            })
         }
        </div>
   
          </div>
    </div>
  )
}
