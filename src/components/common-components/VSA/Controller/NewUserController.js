import React from 'react'
import Mark from '../../../../assets/Mark.png'
import Lottie from "react-lottie";
import * as animationData from "./loader.json";
export default function NewUserController({callbackmainaddsurefunction,backadduserfunction,newuseraddfunction,addmultipleuserfunction,InputVlae,NewUserName,NewUserEmail,Loading,addItem,InputVlae1,usernameValid,Gxidcheck,emailValid,GxEmailcheck,Useradd,showCreateuser,muiltpleuser}) {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData.default,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };
  return (
    <div className="todoListMain">



{
  muiltpleuser?

<div className="header">

<h1 className="usertitle">Add Multiple Users</h1>
<p className="userparagraph">Enter Email & Usernames For All Of Them</p>
<form  >

{
    Useradd.map(item=>
    {
        return(
            <div style={{position:"relative",width:"18rem"}}>
            <input className="Userinputbox" value={item.email} disabled >
              
              </input>
             <img src={Mark} className="indicatormark" alt=""/>
            </div>
        )
    })
}




<div className="displaybutton" style={Useradd.length<=1?{opacity:"0.44"}:{opacity:"1"}} disabled={Useradd.lenght<1?true:false}>
<label className="cancellist" onClick={backadduserfunction} >Add User</label>  
<label className="adduserlist" >Procced</label>
</div>

</form>
</div>
  :null
}






{
  showCreateuser?
  <div className="header">

  <h1 className="usertitle">Add New {localStorage.getItem("appCode")} User</h1>
  <p className="userparagraph"> Step 1: Create Username & Enter Email</p>
<form  >


    <div style={{position:"relative",width:"18rem"}}>
    <input className="Userinputbox" placeholder="Username"  value={NewUserName} onChange={(e)=>{ e.target.value = e.target.value.replace(/[^a-zA-Z0-9-]/g,"",);usernameValid(e.target.value);InputVlae(e.target.value)}}>
      
      </input>
      <label className={Gxidcheck?"indicatorGreen":"indicatorred"}></label>
    </div>
    <div style={{position:"relative",width:"18rem"}}>
  <input className="Userinputbox"  placeholder="Email"   value={NewUserEmail} onChange={(e)=>{emailValid(e.target.value);InputVlae1(e.target.value)}}>
      
      </input>

  <label className={GxEmailcheck?"indicatorGreen":"indicatorred"}></label>
  </div>

{
  muiltpleuser?
  <div className="displaybutton">
  <label className="cancellist" onClick={callbackmainaddsurefunction} >Cancel</label>  
  <label className="adduserlist" onClick={addItem}>Add To List</label>
  </div>
  :
  <div className="displaybutton">
  <label className="cancellist" onClick={addmultipleuserfunction}>All Multiple</label>  
  <label className="adduserlist" onClick={newuseraddfunction}>Procced</label>
  </div>
}


  
</form>
</div>
  :
  null
}


{

Loading?
  <div className="header">
  <Lottie
  options={defaultOptions}
width="50%"
/>
</div>
: null

}

  </div>
  )
}
