import React from 'react'
import { Menu, Dropdown } from 'antd';
import { Tabs } from 'antd';
import {Line} from 'react-chartjs-2';
import ys from '../../../../assets/Y.png'

import{ControllDetail ,Alltimelist,Allprofite,allAffiliates,Alldata,allyear,Allrevenu,AllUser,AllCustomers,AllTxn,oneTimes,Reocurring} from './ControllerApi'
export default function ControllerChat({Alltimeprofite,callback,tabsname,data,menu}) {
  const { TabPane } = Tabs;
  return (
    <>
          <div className="controller-submaain-section">
        <div className="row m-0 row-cntrl">
          <div className=" controllersubmenu">

  {
   Alltimeprofite.map(item=>{
      return(
        <div className="controller-sub-div">
          <div className="controllersubdiv-p-div">
          <p className="controllersubdiv-p">{item.name}</p>
          </div>
          <div className="controllersubdiv-pvalue">
          <p className="controllersubd-pvalue">0.00</p>
          </div>
        </div>
      )
    })
  }

          </div>
          <div className=" tabandyear">
            <div className="controllTabVsa  header-controller-sub">
          <Tabs  onChange={callback}>
    <TabPane defaultActiveKey={tabsname} tab={tabsname} key="1">
  <div className="chart-height">
        <Line data={data}
         width={100}
         height={300}
        options={{
          responsive: true,
       
          showLin:false,
          borderColor:"transparent",
          maintainAspectRatio: false,
          scales:{
            yAxes:[{
                gridLines:{
                    drawBorder: false,
                }
            }]
        }
        }
      
      
      }
        />
      </div>
    </TabPane>
    <TabPane tab="All Time" key="2">
  
    </TabPane>
    <TabPane tab="All Products" key="3">
 
    </TabPane>

  </Tabs>

  <Dropdown overlay={menu} trigger={['click']} className="custom" placement="bottomRight">
    <div>
    <img src={ys} alt="/" onClick={e => e.preventDefault()}/>
    </div>
  </Dropdown>
  </div>



          </div>

        </div>

      </div>
    </>
  )
}
