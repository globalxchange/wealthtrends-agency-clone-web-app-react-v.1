import React, {useContext,useState,useEffect} from 'react'
// import {brokerage} from '../contextapi/Contextapi'
import { Input, Select } from 'antd';
import fetch from "../../fetchurl";

import Search from '../../../assets/searchVsa.png'
import USD from '../../../assets/Usa.png'
// import Drp from '../../images/drps.png'
import axios from "axios";
import {Dropdownlist} from './Vsaapi'
const { Option } = Select;
export default function VsaNav() {
    // const {broker_name,handleatlasChangefliter,setdropshow,SelectedDropname,setdropshowtable,selecteddropdown,SelectedDropmenu,answerdrop}= useContext(brokerage)
    const [loading, setisloading] = useState(false);
  
    const [total, settotal] = useState("0.00");

    const fetchBalances = async (coin)=>{
        setisloading(true)
        let data = await axios
          .get(
            `${fetch.url}coin/fiat/fiat_balances?email=${localStorage.getItem(
              "user_account"
            )}&convert=USD`
          );
         
    
          if(data.data.status){
    
                settotal(Number(data.data.total_sum.toFixed(2)))
                setisloading(false)
              }
        
    
          
          else{
            setisloading(false)
          }
    
      }
    useEffect( ()=>{
        // fetchBalances() 
     }, [] );
  

 

    
  


    const selectAfter = (
        <Select  className="select-after" >
        {
            Dropdownlist.map(item=>
                {
                    return(
                        <Option value={item.name} >{item.name}</Option>

                    )
                })
        }
        
        </Select>
      );
    return (
        <div className="atlasnavbar">
            <div className="sendmain">
            <h1 className="h1namealtas">{localStorage.getItem("appCode")}</h1>
            <p className="pnamealtas">Virtual Sales Agency</p>
            <div className="searchMainaltas">
            <Input name="handleatlasChange" className="vsaheightatlas" addonAfter={selectAfter} placeholder="Search Portal......."/>
           {/* <span className="span-drp-img"><img className="img-drp" src={Drp} alt=""/></span> */}
            <span className="spanseachimage"><img src={Search} alt=""/></span>
            {/* {setdropshowtable?

            <div className="dropdown-list-main">
                {
                    SelectedDropmenu.length>0?
                <>
{
SelectedDropmenu.map(item=>
    {
 return(
        <p className="listofdropdowns" onClick={()=>answerdrop(item.name)}>{item.name}</p>
    ) })
 }

    </>
    :
    <div class="text-vsa">
    <span class="letter r">N</span>
    <span class="letter b">o</span>
    <span class="letter o">M</span>
    <span class="letter g">a</span>
    <span class="letter p">t</span>
    <span class="letter o">c</span>
    <span class="letter b">h</span>
    <span class="letter p">F</span>
    <span class="letter r">o</span>
    <span class="letter r">u</span>
    <span class="letter o">n</span>
    <span class="letter b">d</span>
    <span class="letter p">.</span>
    <span class="letter r">.</span>
    <span class="letter b">.</span>
  </div>

}

   

            </div>
             :null} */}
        </div>
        </div>
        <div className="main-subnav">
    <div className="withdrawsection1">
<h1 className="atlas-img-valueh1">{total===0.00?"0.00":`$${total}`}</h1>

<p className="atlas-img-value">
    <span className="atlas-img-value-span"><img src={USD} alt="logo"/></span> Agency Liquidity
</p>
<label className="altas-withdrow" >Analyze</label>
    </div>
    <div className="withdrawsection">
        <h1 className="atlas-img-valueh1">{total===0.00?"0.00":`$${total}`}</h1>

        <p className="atlas-img-value">
            <span className="atlas-img-value-span"><img src={USD} alt="logo"/></span> Operator Vaults 
        </p>
        <label className="altas-withdrow" >Withdraw</label>
    </div>

    </div>

        </div>
    )
}
