import React from "react";
import Modal from "react-modal";
import "../../../../card_styles.css";
import ProfileAvatarUploader from "./ProfileAvatarUploader";

Modal.setAppElement("#root");

const modelStyle = {
  overlay: { zIndex: "99999" },
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    transform: "translate(-50%, -50%)",
    border: "none",
    background: "none",
    overflow: "auto",
    borderRadius: "none",
    height: "calc(100vh - 40px)",
    display: "flex",
    justifyContent: "center",
    maxWidth: "80%",
    paddingTop: "5rem",
    paddingBottom: "5rem",
    paddingLeft: "0",
    paddingRight: "0"
  }
};

const StatsDetailsModel = ({ open, handler, data, changeStatus }) => {
  console.log("Model Data=>", data);
  return (
    <Modal
      isOpen={open}
      onRequestClose={() => handler({}, false)}
      style={modelStyle}
      contentLabel="Transaction Details"
    >
      <div className="model-container">
        <div className="card" style={{ width: 500 }}>
          {data.mobileCard
            ? data.mobileCard.isShow
              ? data.mobileCard.data.map((item, index) => {
                  return (
                    <div className="card-item" key={index}>
                      <label className="item-header">{item.title}</label>
                      <label className="item-value">{item.value}</label>
                    </div>
                  );
                })
              : null
            : null}
        </div>
        <div className="card" style={{ width: 500 }}>
          {data.firstCard
            ? data.firstCard.isShow
              ? data.firstCard.data.map((item, index) => {
                  return (
                    <div className="card-item" key={index}>
                      <label className="item-header">{item.title}</label>
                      <label className="item-value">{item.value}</label>
                    </div>
                  );
                })
              : null
            : null}
        </div>

        <div className="card" style={{ width: 500 }}>
          {data.secondCard ? (
            data.secondCard.status !== "pending" ? (
              <ProfileAvatarUploader
                email={data.secondCard.email}
                changeStatus={changeStatus}
                token={localStorage.getItem("token")}
                uploadText="Upload a profile picture"
                closeModel={handler}
              />
            ) : (
              <h3 className="text-center mt-5 mb-5">
                We are verifying the image...
              </h3>
            )
          ) : null}
        </div>
        {data.thirdCard ? (
          <div className="card" style={{ width: 500, height: 250 }}></div>
        ) : null}
      </div>
    </Modal>
  );
};

export default StatsDetailsModel;
