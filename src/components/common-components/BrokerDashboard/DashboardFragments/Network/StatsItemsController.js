import React from "react";
import "../../../NewTransactionHistory/VaultLedgerNav/vault-ledger-mobile.css";
import nextIcon from "../../../../images/ledger-right-arrow.svg";
import nextDisIcon from "../../../../images/ledger-right-arrow-disabled.svg";
import prevIcon from "../../../../images/ledger-left-arrow.svg";
import prevDisIcon from "../../../../images/ledger-left-arrow-disabled.svg";

const StatsItemsController = ({ items, active, onChange, isMobile }) => {
  const itemKeys = Object.keys(items);

  const pos = itemKeys.indexOf(active);
  const isEnd = pos >= itemKeys.length - 1;
  const isStart = pos <= 0;

  const handleNext = () => {
    if (!isEnd) {
      onChange(itemKeys[pos + 1]);
    }
  };
  const handlePrev = () => {
    if (!isStart) {
      onChange(itemKeys[pos - 1]);
    }
  };

  return (
    <>
      {isMobile ? (
        <div className="ledger-mobile-nav" style={{ padding: "1rem 0" }}>
          <div className="prev-btn-wrapper" onClick={handlePrev}>
            <img src={isStart ? prevDisIcon : prevIcon} alt="" />
          </div>
          <div className="current-tab-wrapper" onClick={handleNext}>
            <label
              className="current-tab"
              style={{ whiteSpace: "nowrap", fontSize: "1.25rem" }}
            >
              {items[itemKeys[pos]].title}
            </label>
          </div>
          <div className="next-btn-wrapper" onClick={handleNext}>
            <img src={isEnd ? nextDisIcon : nextIcon} alt="" />
          </div>
        </div>
      ) : (
        <div className="stats-controll">
          <ul>
            {itemKeys.map((keyName, index) => (
              <li
                className={active === keyName ? "active" : null}
                key={index}
                onClick={() => {
                  onChange(keyName);
                }}
              >
                {items[keyName].title}
              </li>
            ))}
          </ul>
        </div>
      )}
    </>
  );
};

export default StatsItemsController;
