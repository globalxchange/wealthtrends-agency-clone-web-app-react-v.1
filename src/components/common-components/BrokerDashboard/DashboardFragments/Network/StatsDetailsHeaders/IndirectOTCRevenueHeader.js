import React, { useState } from "react";
import SearchBar from "./SearchBar";
import FullScreenToggler from "./FullScreenToggler";

const IndirectOTCRevenueHeader = ({
  isScrolled,
  isMobile,
  onSearch,
  isExpanded,
  expandTable
}) => {
  const [isSearchOpen, setSearchOpen] = useState(false);

  return (
    <div className={"details-table-item details-table-header scroll-shadow"}>
      {!isSearchOpen ? (
        <>
          <div
            className="flexed-item"
            style={{
              width: isMobile ? "20%" : "3%"
            }}
          />
          <div
            className="flexed-item"
            style={{
              width: isMobile ? "60%" : "22%",
              paddingLeft: "15px"
            }}
          >
            <label>Name</label>
          </div>
          {isMobile ? null : (
            <>
              <div
                className="flexed-item"
                style={{
                  width: "8%"
                }}
              >
                <label>Country</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "14%"
                }}
              >
                <label>Date(EST)</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "8%"
                }}
              >
                <label>Coin</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "18%"
                }}
              >
                <label>Upline</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "11%"
                }}
              >
                <label>Total Fees</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "11%"
                }}
              >
                <label>My Revenue</label>
              </div>
            </>
          )}
        </>
      ) : (
        <div
          className="flexed-item"
          style={{
            width: isMobile ? "20%" : "65%"
          }}
        />
      )}
      <FullScreenToggler isExpanded={isExpanded} expandTable={expandTable} />

      <SearchBar
        setSearchOpen={setSearchOpen}
        isOpen={isSearchOpen}
        onSearch={onSearch}
      />
    </div>
  );
};

export default IndirectOTCRevenueHeader;
