import React from "react";
import "./search-bar.css";
import searchIcon from "../../../../../../assets/LedgerIcon/search.svg";


const SearchBar = ({ setSearchOpen, onSearch }) => {
  return (
    <div className="search-wrapper">
      <input
        id="search-box"
        type="text"
        className="search-box"
        name="q"
        onChange={e => onSearch(e.target.value)}
        onFocus={() => setSearchOpen(true)}
        onBlur={() => setSearchOpen(false)}
      />
      <label htmlFor="search-box">
        <img className="search-icon" src={searchIcon} alt="" height={20} />
      </label>
    </div>
  );
};

export default SearchBar;
