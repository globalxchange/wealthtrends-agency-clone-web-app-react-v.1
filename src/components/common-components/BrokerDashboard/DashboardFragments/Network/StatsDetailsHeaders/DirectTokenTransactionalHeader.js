import React, { useState } from "react";
import SearchBar from "./SearchBar";
import FullScreenToggler from "./FullScreenToggler";

const DirectTokenTransactionalHeader = ({
  isScrolled,
  isMobile,
  onSearch,
  isExpanded,
  expandTable
}) => {
  const [isSearchOpen, setSearchOpen] = useState(false);

  return (
    <div className={"details-table-item details-table-header scroll-shadow"}>
      {!isSearchOpen ? (
        <>
          <div
            className="flexed-item"
            style={{ width: isMobile ? "20%" : "3%" }}
          />
          <div
            className="flexed-item"
            style={{ width: isMobile ? "60%" : "26%", paddingLeft: "15px" }}
          >
            <label>Name</label>
          </div>
          {isMobile ? null : (
            <>
              <div className="flexed-item" style={{ width: "9%" }}>
                <label>Country</label>
              </div>
              <div className="flexed-item" style={{ width: "19%" }}>
                <label>Date (EST)</label>
              </div>
              <div className="flexed-item" style={{ width: "24%" }}>
                <label>Volume</label>
              </div>
              <div className="flexed-item" style={{ width: "13%" }}>
                <label>Commission</label>
              </div>
            </>
          )}
        </>
      ) : (
        <div
          className="flexed-item"
          style={{ width: isMobile ? "20%" : "65%" }}
        />
      )}{" "}
      <FullScreenToggler isExpanded={isExpanded} expandTable={expandTable} />
      <SearchBar
        setSearchOpen={setSearchOpen}
        isOpen={isSearchOpen}
        onSearch={onSearch}
      />
    </div>
  );
};

export default DirectTokenTransactionalHeader;
