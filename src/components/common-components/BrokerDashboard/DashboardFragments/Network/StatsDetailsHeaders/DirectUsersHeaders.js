import React, { useState } from "react";
import SearchBar from "./SearchBar";
import FullScreenToggler from "./FullScreenToggler";

const DirectUsersHeaders = ({
  isScrolled,
  isMobile,
  onSearch,
  isExpanded,
  expandTable
}) => {
  const [isSearchOpen, setSearchOpen] = useState(false);

  return (
    <div className={"details-table-item details-table-header scroll-shadow"}>
      {!isSearchOpen ? (
        <>
          <div
            className="flexed-item"
            style={{ width: isMobile ? "20%" : "3%" }}
          />
          <div
            className="flexed-item"
            style={{ width: isMobile ? "60%" : "45%", paddingLeft: "15px" }}
          >
            <label>Name</label>
          </div>
          {isMobile ? null : (
            <>
              <div className="flexed-item" style={{ width: "17%" }}>
                <label>Country</label>
              </div>
              <div className="flexed-item" style={{ width: "30%" }}>
                <label>Date of Signup (EST)</label>
              </div>
            </>
          )}
        </>
      ) : (
        <div
          className="flexed-item"
          style={{ width: isMobile ? "20%" : "65%" }}
        />
      )}
      <FullScreenToggler isExpanded={isExpanded} expandTable={expandTable} />

      <SearchBar
        setSearchOpen={setSearchOpen}
        isOpen={isSearchOpen}
        onSearch={onSearch}
      />
    </div>
  );
};

export default DirectUsersHeaders;
