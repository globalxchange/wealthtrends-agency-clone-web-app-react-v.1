import React, { useState } from "react";
import SearchBar from "./SearchBar";
import FullScreenToggler from "./FullScreenToggler";

const DirectOTCRevenueHeader = ({
  isScrolled,
  isMobile,
  onSearch,
  isExpanded,
  expandTable
}) => {
  const [isSearchOpen, setSearchOpen] = useState(false);

  return (
    <div className={"details-table-item details-table-header scroll-shadow"}>
      {!isSearchOpen ? (
        <>
          <div
            className="flexed-item"
            style={{
              width: isMobile ? "20%" : "3%"
            }}
          />
          <div
            className="flexed-item"
            style={{
              width: isMobile ? "60%" : "26%",
              paddingLeft: "15px"
            }}
          >
            <label>Name</label>
          </div>
          {isMobile ? null : (
            <>
              <div
                className="flexed-item"
                style={{
                  width: "8%"
                }}
              >
                <label>Country</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "18%"
                }}
              >
                <label>Date(EST)</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "10%"
                }}
              >
                <label>Coin</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "15%"
                }}
              >
                <label>Fees</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "15%"
                }}
              >
                <label>My Revenue</label>
              </div>
            </>
          )}
        </>
      ) : (
        <div
          className="flexed-item"
          style={{
            width: isMobile ? "20%" : "65%"
          }}
        />
      )}{" "}
      <FullScreenToggler isExpanded={isExpanded} expandTable={expandTable} />
      <SearchBar
        setSearchOpen={setSearchOpen}
        isOpen={isSearchOpen}
        onSearch={onSearch}
      />
    </div>
  );
};

export default DirectOTCRevenueHeader;
