import React, { useState } from "react";
import SearchBar from "./SearchBar";
import FullScreenToggler from "./FullScreenToggler";

const TotalDigitalRevenueHeader = ({
  isScrolled,
  isMobile,
  onSearch,
  isExpanded,
  expandTable
}) => {
  const [isSearchOpen, setSearchOpen] = useState(false);

  return (
    <div className={"details-table-item details-table-header scroll-shadow"}>
      {!isSearchOpen ? (
        <>
          <div
            className="flexed-item"
            style={{
              width: isMobile ? "20%" : "3%"
            }}
          />
          <div
            className="flexed-item"
            style={{
              width: isMobile ? "60%" : "24%",
              paddingLeft: "15px"
            }}
          >
            <label>Name</label>
          </div>
          {isMobile ? null : (
            <>
              <div
                className="flexed-item"
                style={{
                  width: "18%"
                }}
              >
                <label>Date(EST)</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "12%"
                }}
              >
                <label>Coin</label>
              </div>

              <div
                className="flexed-item"
                style={{
                  width: "22%"
                }}
              >
                <label>Interest Earned</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "18%"
                }}
              >
                <label>Broker Commissions</label>
              </div>
            </>
          )}
        </>
      ) : (
        <div
          className="flexed-item"
          style={{
            width: isMobile ? "20%" : "65%"
          }}
        />
      )}
      <FullScreenToggler isExpanded={isExpanded} expandTable={expandTable} />

      <SearchBar
        setSearchOpen={setSearchOpen}
        isOpen={isSearchOpen}
        onSearch={onSearch}
      />
    </div>
  );
};

export default TotalDigitalRevenueHeader;
