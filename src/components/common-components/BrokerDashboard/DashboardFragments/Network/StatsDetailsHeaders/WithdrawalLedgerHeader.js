import React, { useState } from "react";
import SearchBar from "./SearchBar";
import FullScreenToggler from "./FullScreenToggler";

export const WithdrawalLedgerHeader = ({
  isScrolled,
  isMobile,
  onSearch,
  isExpanded,
  expandTable
}) => {
  const [isSearchOpen, setSearchOpen] = useState(false);

  return (
    <div className={"details-table-item details-table-header scroll-shadow"}>
      {!isSearchOpen ? (
        <>
          <div
            className="flexed-item"
            style={{
              width: isMobile ? "20%" : "3%"
            }}
          />
          <div
            className="flexed-item"
            style={{
              width: isMobile ? "60%" : "22%",
              paddingLeft: "15px"
            }}
          >
            <label>Method</label>
          </div>
          {isMobile ? null : (
            <>
              <div
                className="flexed-item"
                style={{
                  width: "14%"
                }}
              >
                <label>Date(EST)</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "14%"
                }}
              >
                <label>USD Amount</label>
              </div>

              <div
                className="flexed-item"
                style={{
                  width: "15%"
                }}
              >
                <label>Value</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "16%"
                }}
              >
                <label>Balance</label>
              </div>
              <div
                className="flexed-item"
                style={{
                  width: "16%"
                }}
              >
                <label>Status</label>
              </div>
            </>
          )}
        </>
      ) : (
        <div
          className="flexed-item"
          style={{
            width: isMobile ? "20%" : "65%"
          }}
        />
      )}

      <FullScreenToggler isExpanded={isExpanded} expandTable={expandTable} />

      <SearchBar
        setSearchOpen={setSearchOpen}
        isOpen={isSearchOpen}
        onSearch={onSearch}
      />
    </div>
  );
};
