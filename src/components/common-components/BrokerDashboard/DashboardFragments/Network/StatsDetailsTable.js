import React, { useState, useRef, useEffect } from "react";
import "../../../../new_faith.css";
import {
  TotalUsersHeaders,
  DirectUsersHeaders,
  IndirectTokenTransactionalHeader,
  DirectTokenTransactionalHeader
} from "./StatsDetailsHeaders/";
import {
  DirectTokenTransactionalItem,
  TotalUserItem,
  IndirectTokenTransactionalItem,
  DirectUserItem
} from "./StatsDetailsItems/";
import TotalOTCVolumeHeader from "./StatsDetailsHeaders/TotalOTCVolumeHeader";
import TotalOTCVolumeItem from "./StatsDetailsItems/TotalOTCVolumeItem";
import TotalOTCRevenueHeader from "./StatsDetailsHeaders/TotalOTCRevenueHeader";
import TotalOTCRevenueItem from "./StatsDetailsItems/TotalOTCRevenueItem";
import DirectOTCRevenueHeader from "./StatsDetailsHeaders/DirectOTCRevenue";
import DirectOTCRevenueItem from "./StatsDetailsItems/DirectOTCRevenueItem";
import IndirectOTCRevenueHeader from "./StatsDetailsHeaders/IndirectOTCRevenueHeader";
import IndirectOTCRevenueItem from "./StatsDetailsItems/IndirectOTCRevenueItem";
import TotalDigitalVolumeHeader from "./StatsDetailsHeaders/TotalDigitalVolumeHeader";
import TotalDigitalVolumeItem from "./StatsDetailsItems/TotalDigitalVolumeItem";
import TotalDigitalRevenueHeader from "./StatsDetailsHeaders/TotalDigitalRevenueHeader";
import TotalDigitalRevenueItem from "./StatsDetailsItems/TotalDigitalRevenueItem";
import DirectDigitalRevenueHeader from "./StatsDetailsHeaders/DirectDigitalRevenueHeader";
import DirectDigitalRevenueItem from "./StatsDetailsItems/DirectDigitalRevenueItem";
import { WithdrawalLedgerHeader } from "./StatsDetailsHeaders/WithdrawalLedgerHeader";
import WithdrawalLedgerItem from "./StatsDetailsItems/WithdrawalLedgerItem";

const StatsDetailsTable = ({
  dataSet,
  listType,
  currentPage,
  postPerPage,
  isMobile,
  modelHandler,
  onSearch,
  comingSoon,
  isExpanded,
  expandTable
}) => {
  const [childHeight, setChildHeight] = useState(0);

  const offset = (currentPage - 1) * postPerPage;

  const tableRef = useRef();

  const renderList = () => {
    switch (listType) {
      case "Total Users":
        return (
          <>
            {isMobile ? null : (
              <TotalUsersHeaders
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <TotalUserItem
                key={index}
                data={item}
                isMobile={isMobile}
                modelHandler={modelHandler}
                heightHandler={setChildHeight}
              />
            ))}
          </>
        );
      case "Direct Users":
        return (
          <>
            {isMobile ? null : (
              <DirectUsersHeaders
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <DirectUserItem
                key={index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Indirect Users":
        return (
          <>
            {isMobile ? null : (
              <TotalUsersHeaders
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <TotalUserItem
                key={index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Total Customers":
        return (
          <>
            {isMobile ? null : (
              <TotalUsersHeaders
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <TotalUserItem
                key={index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Direct Customers":
        return (
          <>
            {isMobile ? null : (
              <DirectUsersHeaders
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <DirectUserItem
                key={index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Indirect Customers":
        return (
          <>
            {isMobile ? null : (
              <TotalUsersHeaders
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <TotalUserItem
                key={index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Total Affiliates":
        return (
          <>
            {isMobile ? null : (
              <TotalUsersHeaders
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <TotalUserItem
                key={index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Direct Affiliates":
        return (
          <>
            {isMobile ? null : (
              <DirectUsersHeaders
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <DirectUserItem
                key={index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Indirect Affiliates":
        return (
          <>
            {isMobile ? null : (
              <TotalUsersHeaders
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <TotalUserItem
                key={index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Total Token Transactional Volume":
        return (
          <>
            {isMobile ? null : (
              <IndirectTokenTransactionalHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <IndirectTokenTransactionalItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Direct Token Transactional Revenue":
        return (
          <>
            {isMobile ? null : (
              <DirectTokenTransactionalHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <DirectTokenTransactionalItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Indirect Token Transactional Revenue":
        return (
          <>
            {isMobile ? null : (
              <IndirectTokenTransactionalHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <IndirectTokenTransactionalItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Broker Dealer Transactional Revenue":
        return (
          <>
            {isMobile ? null : (
              <IndirectTokenTransactionalHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <IndirectTokenTransactionalItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Total OTC Transactional Volume":
        return (
          <>
            {isMobile ? null : (
              <TotalOTCVolumeHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <TotalOTCVolumeItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Total OTC Transactional Revenue":
        return (
          <>
            {isMobile ? null : (
              <TotalOTCRevenueHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <TotalOTCRevenueItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Direct OTC Transactional Revenue":
        return (
          <>
            {isMobile ? null : (
              <DirectOTCRevenueHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <DirectOTCRevenueItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Indirect OTC Transactional Revenue":
        return (
          <>
            {isMobile ? null : (
              <IndirectOTCRevenueHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <IndirectOTCRevenueItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Brokerage Money Market Earnings":
        return (
          <>
            {isMobile ? null : (
              <TotalDigitalVolumeHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <TotalDigitalVolumeItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Total Digital Transactional Revenue":
        return (
          <>
            {isMobile ? null : (
              <TotalDigitalRevenueHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <TotalDigitalRevenueItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Direct Digital Transactional Volume":
        return (
          <>
            {isMobile ? null : (
              <DirectDigitalRevenueHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <DirectDigitalRevenueItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );

      case "Indirect Digital Transactional Volume":
        return (
          <>
            {isMobile ? null : (
              <DirectDigitalRevenueHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <DirectDigitalRevenueItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      case "Broker Dealer Revenue":
        return (
          <>
            {isMobile ? null : (
              <DirectDigitalRevenueHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <DirectDigitalRevenueItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
                brokerRevenue
              />
            ))}
          </>
        );
      case "Total Withdrawn":
        return (
          <>
            {isMobile ? null : (
              <WithdrawalLedgerHeader
                isMobile={isMobile}
                onSearch={onSearch}
                isExpanded={isExpanded}
                expandTable={expandTable}
              />
            )}
            {dataSet.slice(offset, offset + postPerPage).map((item, index) => (
              <WithdrawalLedgerItem
                key={offset + index}
                data={item}
                isMobile={isMobile}
                heightHandler={setChildHeight}
                modelHandler={modelHandler}
              />
            ))}
          </>
        );
      default:
        return [];
    }
  };

  return (
    <div
      className={`statementRightWraper new-faith-table stats-table table-row h-100 ${
        isExpanded ? "expanded-stats-table" : ""
      }`}
    >
      <div
        class={
          "statementdataTableBlock transactionHistoryScroll stats-table-block" +
          (isMobile && dataSet.length > 0 ? " stats-mobile" : "")
        }
        ref={tableRef}
        // style={isMobile ? { height: "60vh" } : null}
      >
        {comingSoon ? (
          <h3 className="text-center mt-5">Coming Soon...</h3>
        ) : (
          renderList()
        )}
      </div>
    </div>
  );
};

export default StatsDetailsTable;
