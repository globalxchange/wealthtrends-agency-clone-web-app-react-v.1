import React, { useState, useEffect, useContext } from "react";
import "./stats.scss";
import Statistics from "./Statistics";
import { brokerage } from "../../../../Brokercontextapi/Contextapi";
import Axios from "axios";
import fetch from "../../../../fetchurl.js";

let axiosToken = null;

const Stats = ({ tableToShow, navBarHeight }) => {
  const context = useContext(brokerage);
  const [data, setData] = useState({
    title: "Users",
    unit: "Users",
    items: [
      {
        field: "Total Users",
        mobileTitle: "Total"
      },
      {
        field: "Direct Users",
        mobileTitle: "Direct"
      },
      {
        field: "Indirect Users",
        mobileTitle: "Indirect"
      }
    ]
  });
  const [isPulled, setIsPulled] = useState(false);
  const [isMobile, setIsMobile] = useState(window.innerWidth <= 500);

  useEffect(() => {
    window.addEventListener("resize", screenResizeListener);
    getDataFromApi();
    return () => {
      window.removeEventListener("resize", screenResizeListener);
    };
  }, []);

  const screenResizeListener = () => {
    setIsMobile(window.innerWidth <= 500);
  };

  useEffect(() => {
    getDataFromApi();
    return () => {};
  }, [tableToShow, context.affiliate_id, context.balance, context.withdraw]);

  const getDataFromApi = () => {
    if (axiosToken) {
      axiosToken.cancel("Cancelled");
    }

    let currentDataSet;
    switch (tableToShow) {
      case "Users":
        currentDataSet = {
          title: "Users",
          unit: "Users",
          items: [
            {
              field: "Total Users",
              mobileTitle: "Total"
            },
            {
              field: "Direct Users",
              mobileTitle: "Direct"
            },
            {
              field: "Indirect Users",
              mobileTitle: "Indirect"
            }
          ]
        };
        setData(currentDataSet);
        setIsPulled(true);

        if (context.affiliate_id !== "") {
          axiosToken = Axios.CancelToken.source();
          Axios.get(fetch.url + "brokerage/stats/getUsers", {
            params: {
              affiliate_id: context.affiliate_id
            },
            cancelToken: axiosToken.token
          })
            .then(getUsersRes => {
              const { data } = getUsersRes;
              currentDataSet = {
                title: "Users",
                unit: "Users",
                items: [
                  {
                    field: "Total Users",
                    mobileTitle: "Total",
                    value: data.all ? data.all.length : "Retrieving",
                    list: data.all ? data.all.users : []
                  },
                  {
                    field: "Direct Users",
                    mobileTitle: "Direct",
                    value: data.direct ? data.direct.length : "Retrieving",
                    list: data.direct ? data.direct.users : []
                  },
                  {
                    field: "Indirect Users",
                    mobileTitle: "Indirect",
                    value: data.indirect ? data.indirect.length : "Retrieving",
                    list: data.indirect ? data.indirect.users : []
                  }
                ]
              };
              setData(currentDataSet);
            })
            .catch(err => {});
        }

        break;
      case "Customers":
        currentDataSet = {
          title: "Customers",
          unit: "Customers",
          items: [
            {
              field: "Total Customers",
              mobileTitle: "Total"
            },
            {
              field: "Direct Customers",
              mobileTitle: "Direct"
            },
            {
              field: "Indirect Customers",
              mobileTitle: "Indirect"
            }
          ]
        };
        setData(currentDataSet);

        axiosToken = Axios.CancelToken.source();
        Axios.get(fetch.url + "brokerage/stats/getUsers", {
          params: {
            affiliate_id: context.affiliate_id
          },
          cancelToken: axiosToken.token
        })
          .then(getUsersRes => {
            const { data } = getUsersRes;

            const directCustomers = data.customers.users.filter(
              item => item.ref_affiliate === context.affiliate_id
            );
            const indirectCustomers = data.customers.users.filter(
              item => item.ref_affiliate !== context.affiliate_id
            );

            currentDataSet = {
              title: "Customers",
              unit: "Customers",
              items: [
                {
                  field: "Total Customers",
                  mobileTitle: "Total",
                  value: data.customers ? data.customers.length : "Retrieving",
                  list: data.customers.users
                },
                {
                  field: "Direct Customers",
                  mobileTitle: "Direct",
                  value: data.customers ? directCustomers.length : "Retrieving",
                  list: directCustomers
                },
                {
                  field: "Indirect Customers",
                  mobileTitle: "Indirect",
                  value: data.customers
                    ? indirectCustomers.length
                    : "Retrieving",
                  list: indirectCustomers
                }
              ]
            };
            setData(currentDataSet);
          })
          .catch(err => {});
        break;
      case "Brokers":
        currentDataSet = {
          title: "Brokers",
          unit: "Brokers",
          items: [
            {
              field: "Total Affiliates",
              mobileTitle: "Total"
            },
            {
              field: "Direct Affiliates",
              mobileTitle: "Direct"
            },
            {
              field: "Indirect Affiliates",
              mobileTitle: "Indirect"
            }
          ]
        };
        setData(currentDataSet);

        axiosToken = Axios.CancelToken.source();
        Axios.get(fetch.url + "brokerage/stats/getUsers", {
          params: {
            affiliate_id: context.affiliate_id
          },
          cancelToken: axiosToken.token
        })
          .then(getUsersRes => {
            const { data } = getUsersRes;

            const directBrokers = data.brokers.users.filter(
              item => item.ref_affiliate === context.affiliate_id
            );
            const indirectBrokers = data.brokers.users.filter(
              item => item.ref_affiliate !== context.affiliate_id
            );

            currentDataSet = {
              title: "Brokers",
              unit: "Brokers",
              items: [
                {
                  field: "Total Affiliates",
                  mobileTitle: "Total",
                  value: data.brokers ? data.brokers.length : "Retrieving",
                  list: data.brokers.users
                },
                {
                  field: "Direct Affiliates",
                  mobileTitle: "Direct",
                  value: data.brokers ? directBrokers.length : "Retrieving",
                  list: directBrokers
                },
                {
                  field: "Indirect Affiliates",
                  mobileTitle: "Indirect",
                  value: data.brokers ? indirectBrokers.length : "Retrieving",
                  list: indirectBrokers
                }
              ]
            };
            setData(currentDataSet);
          })
          .catch(err => {});
        break;
      case "Token Transactions":
        currentDataSet = {
          title: "Token Transactions",
          unit: "USD",
          items: [
            {
              field: "Total Token Transactional Volume",
              mobileTitle: "Total"
            },
            {
              field: "Direct Token Transactional Revenue",
              mobileTitle: "Direct"
            },
            {
              field: "Indirect Token Transactional Revenue",
              mobileTitle: "Indirect"
            },
            {
              field: "Broker Dealer Transactional Revenue",
              mobileTitle: "Transactional Revenue"
            }
          ]
        };
        setData(currentDataSet);

        axiosToken = Axios.CancelToken.source();
        Axios.get(fetch.url + "brokerage/stats/token_txns", {
          params: {
            email: context.email,
            getTop: 6
          },
          cancelToken: axiosToken.token
        })
          .then(tokensDataRes => {
            const { data } = tokensDataRes;
            currentDataSet = {
              title: "Token Transactions",
              unit: "USD",
              items: [
                {
                  field: "Total Token Transactional Volume",
                  mobileTitle: "Total",
                  value: data.volume ? data.volume.ttv : "Retrieving",
                  list: data.all
                },
                {
                  field: "Direct Token Transactional Revenue",
                  mobileTitle: "Direct",
                  value: data.revenue ? data.revenue.dtr : "Retrieving",
                  list: data.direct
                },
                {
                  field: "Indirect Token Transactional Revenue",
                  mobileTitle: "Indirect",
                  value: data.revenue ? data.revenue.itr : "Retrieving",
                  list: data.indirect
                },
                {
                  field: "Broker Dealer Transactional Revenue",
                  mobileTitle: "Transactional Revenue",
                  value: data.revenue ? data.revenue.bdr : "Retrieving",
                  list: data.all
                }
              ]
            };
            setData(currentDataSet);
          })
          .catch(err => {});
        break;
      case "OTC Transactions":
        currentDataSet = {
          title: "OTC Transactions",
          unit: "USD",
          items: [
            {
              field: "Total OTC Transactional Volume",
              mobileTitle: "Total Volume"
            },
            {
              field: "Total OTC Transactional Revenue",
              mobileTitle: "Total Revenue"
            },
            {
              field: "Direct OTC Transactional Revenue",
              mobileTitle: "Direct Revenue"
            },
            {
              field: "Indirect OTC Transactional Revenue",
              mobileTitle: "Indirect Revenue"
            },
            {
              field: "Broker Dealer Transactional Revenue",
              mobileTitle: "Transactional Revenue"
            }
          ]
        };
        setData(currentDataSet);

        axiosToken = Axios.CancelToken.source();
        Axios.get(fetch.url + "brokerage/stats/otc_txns", {
          params: {
            email: context.email,
            getTop: 6
          },
          cancelToken: axiosToken.token
        })
          .then(otcDataRes => {
            const { data } = otcDataRes;
            currentDataSet = {
              title: "OTC Transactions",
              unit: "USD",
              items: [
                {
                  field: "Total OTC Transactional Volume",
                  mobileTitle: "Total Volume",
                  value: data.volume ? data.volume.ttv : "Retrieving",
                  list: data.all
                },
                {
                  field: "Total OTC Transactional Revenue",
                  mobileTitle: "Total Revenue",
                  value: data.revenue ? data.revenue.ttr : "Retrieving",
                  list: data.all
                },
                {
                  field: "Direct OTC Transactional Revenue",
                  mobileTitle: "Direct Revenue",
                  value: data.revenue.dtr ? data.revenue.dtr : "Retrieving",
                  list: data.direct
                },
                {
                  field: "Indirect OTC Transactional Revenue",
                  mobileTitle: "Indirect Revenue",
                  value: data.revenue ? data.revenue.itr : "Retrieving",
                  list: data.indirect
                },
                {
                  field: "Broker Dealer Transactional Revenue",
                  mobileTitle: "Transactional Revenue",
                  value: data.revenue ? data.revenue.bdr : "Retrieving",
                  list: data.all
                }
              ]
            };
            setData(currentDataSet);
          })
          .catch(err => {});

        break;
      case "Digital Transactions":
        currentDataSet = {
          title: "Digital Transactions",
          unit: "USD",
          items: [
            {
              field: "Brokerage Money Market Earnings",
              mobileTitle: "Total Volume"
            },
            {
              field: "Total Digital Transactional Revenue",
              mobileTitle: "Total Revenue"
            },
            {
              field: "Direct Digital Transactional Volume",
              mobileTitle: "Direct"
            },
            {
              field: "Indirect Digital Transactional Volume",
              mobileTitle: "Indirect"
            },
            {
              field: "Broker Dealer Revenue",
              mobileTitle: "Broker Dealer Revenue"
            }
          ]
        };
        setData(currentDataSet);

        axiosToken = Axios.CancelToken.source();
        Axios.get(fetch.url + "brokerage/stats/digital_txns", {
          params: { email: context.email },
          cancelToken: axiosToken.token
        })
          .then(digiDataRes => {
            const { data } = digiDataRes;
            currentDataSet = {
              title: "Digital Transactions",
              unit: "USD",
              items: [
                {
                  field: "Brokerage Money Market Earnings",
                  mobileTitle: "Total Volume",
                  value: data.volume.ttv ? data.volume.ttv : "Retrieving",
                  list: data.all
                },
                {
                  field: "Total Digital Transactional Revenue",
                  mobileTitle: "Total Revenue",
                  value: data.revenue.ttr ? data.revenue.ttr : "Retrieving",
                  list: data.all
                },
                {
                  field: "Direct Digital Transactional Volume",
                  mobileTitle: "Direct",
                  value: data.revenue.dtr ? data.revenue.dtr : "Retrieving",
                  list: data.direct
                },
                {
                  field: "Indirect Digital Transactional Volume",
                  mobileTitle: "Indirect",
                  value: data.revenue.itr ? data.revenue.itr : "Retrieving",
                  list: data.indirect
                },
                {
                  field: "Broker Dealer Revenue",
                  mobileTitle: "Broker Dealer Revenue",
                  value: data.revenue.bdr ? data.revenue.bdr : "Retrieving",
                  list: data.all
                }
              ]
            };
            setData(currentDataSet);
          })
          .catch(err => {});
        break;
      case "Credit Card Transactions":
        currentDataSet = {
          title: "Credit Card Transactions",
          unit: "USD",
          items: [
            {
              field: "Total OTC Transactional Volume-InstaCryptoPurchase.com",
              mobileTitle: "Total Volume",
              value: "Coming Soon"
            },
            {
              field: "Total OTC Transactional Revenue-InstaCryptoPurchase.com",
              mobileTitle: "Total Revenue",
              value: "Coming Soon"
            },
            {
              field: "Direct OTC Transactional Revenue-InstaCryptoPurchase.com",
              mobileTitle: "Direct Revenue",
              value: "Coming Soon"
            },
            {
              field:
                "Indirect OTC Transactional Revenue-InstaCryptoPurchase.com",
              mobileTitle: "Indirect Revenue",
              value: "Coming Soon"
            }
          ]
        };
        setData(currentDataSet);
        break;
      case "Withdrawal Ledger":
        console.log("Withdrawal Ledger => ", context.withdraw);
        let withdrawals = [...context.withdraw];
        withdrawals.shift();
        currentDataSet = {
          title: "Withdrawal Ledger",
          unit: "USD",
          items: [
            {
              field: "Total Withdrawn",
              mobileTitle: "Total Withdrawn",
              value: context.withdraw[0]
                ? context.withdraw[0].withdrawl_amt
                : "$ 0.00",
              list: context.withdraw ? withdrawals : []
            },
            {
              field: "Withdrawal Balance",
              mobileTitle: "Withdrawal Balance",
              nonClickable: true,
              value: context.balance,
              list: context.withdraw
            }
          ]
        };
        setData(currentDataSet);
        break;
      default:
    }
  };
  return (
    <div className="fragment-container" style={{height: "calc(100vh - 266px)"}}>
    <div className="h-100 stats-container">
      <div className="h-100 stats-wrapper">
        <div className="h-100">
          <div className="h-100">
            <div className="h-100 stats-container">
              <Statistics
                data={data}
                tableToShow={tableToShow}
                navBarHeight={navBarHeight}
              />
            </div>
            {/* {!isMobile ? (
              <div className="col-md-4">
                <GXBrokerInsights
                  height={notificHeight}
                  topTokens={data.topTokens ? data.topTokens : {}}
                  topOtc={data.topOtc ? data.topOtc : {}}
                />
              </div>
            ) : (
              <div className="col-md-4">
                <Link to="/gxbroker-insights-mobile">
                  <label style={{ margin: 0 }}>GXBroker Insights</label>
                </Link>
              </div>
            )} */}
          </div>
        </div>
      </div>
    </div>
    </div>
  );
};

export default Stats;
