import React from "react";

const formatterWithTwoDigits = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 2
});

const UserItem = ({ data }) => {
  return (
    <div className="user-item">
      <div className="user-rank">
        <div
          className="user-avatar"
          style={{
            backgroundImage:
              "url('https://www.w3schools.com/w3css/img_avatar3.png')"
          }}
        />
      </div>
      <div className="user-info">
        <div className="d-flex flex-column">
          <label className="user-name">{data.name}</label>
          <label className="user-country">{data.email}</label>
        </div>
        <div className="user-stats">
          <label className="user-volume">
            {formatterWithTwoDigits.format(data.volume)}
          </label>
        </div>
      </div>
    </div>
  );
};

export default UserItem;
