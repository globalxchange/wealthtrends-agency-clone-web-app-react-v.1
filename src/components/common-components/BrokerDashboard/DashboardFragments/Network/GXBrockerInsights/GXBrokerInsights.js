import React, { useState } from "react";
import "./gx-broker-insights.css";
import UserList from "./UserList";

const GXBrokerInsights = ({ topTokens, topOtc }) => {
  const [currentTab, setCurrentTab] = useState("Tokens");

  return (
    <div className="gx-insights-wrapper d-table h-100">
      <div className="table-row">
        <div className="gx-insights-header">
          <h3 className="mb-5">GXBroker Insights</h3>
        </div>
      </div>
      <div className="table-row">
        <div className="row insight-section-header">
          <div className="col-md-6">
            <li
              className={
                "broker-header" + (currentTab === "Tokens" ? " active" : "")
              }
              onClick={() => setCurrentTab("Tokens")}
            >
              Tokens
            </li>
          </div>
          <div className="col-md-6">
            <li
              className={
                "users-header" + (currentTab === "OTC" ? " active" : "")
              }
              onClick={() => setCurrentTab("OTC")}
            >
              OTC
            </li>
          </div>
        </div>
      </div>

      <div className="table-row h-100">
        <div className="row top-user-wrapper h-100">
          {currentTab === "Tokens" ? (
            topTokens.users ? (
              <UserList data={topTokens.users} />
            ) : (
              <div className="col-12 h-100">
                <div className="users-card d-table h-100 w-100">
                  <div className="table-row h-100">
                    <h3 className="text-center my-auto">Retrieving...</h3>
                  </div>
                </div>
              </div>
            )
          ) : topOtc.users ? (
            <UserList data={topOtc.users} />
          ) : (
            <div className="col-12 h-100">
              <div className="users-card d-table h-100 w-100">
                <div className="table-row h-100">
                  <h3 className="text-center my-auto">Retrieving...</h3>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default GXBrokerInsights;
