import React from "react";
import TopUser from "./TopUser";
import UserItem from "./UserItem";

const UserList = ({ data }) => {
  return (
    <div className="col-12 h-100">
      <div className="users-card d-table h-100 w-100">
        {data.length > 0 ? (
          <>
            <div className="table-row">
              <TopUser data={data[0]} />
            </div>
            <div className="table-row h-100">
              <div className="user-item-container h-100">
                {data.slice(1, 6).map((item, index) => {
                  return <UserItem key={index} data={item} />;
                })}
              </div>
            </div>
          </>
        ) : (
          <h3 className="text-center">No data found...</h3>
        )}
      </div>
    </div>
  );
};

export default UserList;
