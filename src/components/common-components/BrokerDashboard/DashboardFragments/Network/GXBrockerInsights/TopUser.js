import React from "react";
import trophy from "../../../../../images/trophy.svg";

const formatterWithTwoDigits = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 2
});

const TopUser = ({ data }) => {
  return (
    <div className="top-user">
      <div className="user-info">
        <div
          className="user-avatar"
          style={{
            backgroundImage:
              "url('https://www.w3schools.com/w3css/img_avatar3.png')"
          }}
        />
        <div className="d-flex flex-column">
          <label className="user-name">{data.name}</label>
          <label className="user-country">{data.email}</label>
        </div>
      </div>
      <div className="user-stats">
        <img src={trophy} alt="" height={25} />
        <label className="user-volume">
          {formatterWithTwoDigits.format(data.volume)}
        </label>
      </div>
    </div>
  );
};

export default TopUser;
