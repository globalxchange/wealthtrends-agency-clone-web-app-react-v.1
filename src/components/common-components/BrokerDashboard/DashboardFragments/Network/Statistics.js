import React, { Component } from "react";
import StatsItem from "./StatsItems";
import StatsDetailsTable from "./StatsDetailsTable";
import Pagination from "../../../pagination/Pagination";
import DetailsModel from "./StatsDetailsModel";

class Statistics extends Component {
  constructor(props) {
    super(props);

    this.controllerRef = React.createRef();
    this.cardRef = React.createRef();
    this.state = {
      activeDetails: props.data.items[0].field,
      currentPage: 1,
      currentDataSet: props.data.items[0].list ? props.data.items[0].list : [],
      filteredDataSet: props.data.items[0].list ? props.data.items[0].list : [],
      isMobile: false,
      isModelOpen: false,
      modelData: {},
      isExpanded: false
    };
    this.onDetailsChangeHandler = this.onDetailsChangeHandler.bind(this);
    this.modelHandler = this.modelHandler.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.expandTable = this.expandTable.bind(this);
  }

  onDetailsChangeHandler = activeDetails => {
    const { data } = this.props;
    let currentDataSet = data.items.find(x => x.field === activeDetails).list;
    this.setState({
      activeDetails,
      currentDataSet: currentDataSet ? currentDataSet : [],
      currentPage: 1,
      filteredDataSet: currentDataSet ? currentDataSet : []
    });
  };

  onSearch = keyword => {
    const { currentDataSet } = this.state;
    if (keyword === "") {
      this.setState({ filteredDataSet: currentDataSet, currentPage: 1 });
    } else {
      const filteredDataSet = currentDataSet.filter(item => {
        try {
          if (item.name || item.email) {
            if (
              item.name.toLowerCase().includes(keyword.toLowerCase()) ||
              item.email.toLowerCase().includes(keyword.toLowerCase())
            )
              return true;
          }
          if (item.txn.name || item.txn.email) {
            if (
              item.txn.name.toLowerCase().includes(keyword.toLowerCase()) ||
              item.txn.email.toLowerCase().includes(keyword.toLowerCase())
            )
              return true;
          }
        } catch (error) {
          return false;
        }
        return false;
      });
      this.setState({ filteredDataSet, currentPage: 1 });
    }
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      activeDetails: nextProps.data.items[0].field,
      currentPage: 1,
      currentDataSet: nextProps.data.items[0].list
        ? nextProps.data.items[0].list
        : [],
      filteredDataSet: nextProps.data.items[0].list
        ? nextProps.data.items[0].list
        : []
    });
  }

  componentDidMount() {
    this.screenResizeListener();
    window.addEventListener("resize", this.screenResizeListener);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.screenResizeListener);
  }

  screenResizeListener = () => {
    this.setState({ isMobile: window.innerWidth <= 500 });
  };

  setCurrentPage = currentPage => {
    this.setState({ currentPage });
  };

  modelHandler = (data, bool) => {
    this.setState({ modelData: data, isModelOpen: bool });
  };

  expandTable = isExpanded => {
    this.setState({ isExpanded });
  };

  render() {
    const {
      activeDetails,
      currentDataSet,
      currentPage,
      isMobile,
      isModelOpen,
      modelData,
      filteredDataSet,
      isExpanded
    } = this.state;

    const { tableToShow, navBarHeight } = this.props;

    const postPerPage = isMobile ? 5 : 10;

    const renderStatsItemCards = () => {
      const { data } = this.props;

      return data.items.map((item, index) => {
        return (
          <StatsItem
            key={index}
            keyValue={tableToShow}
            titleShow={isMobile ? item.mobileTitle : item.field}
            title={item.field}
            noOfCards={data.items.length}
            value={item.value}
            unit={data.unit}
            width={isMobile ? 43 : 100 / data.items.length}
            onChange={this.onDetailsChangeHandler}
            active={activeDetails}
            nonClickable={item.nonClickable}
          />
        );
      });
    };

    return (
      <>
        <DetailsModel
          open={isModelOpen}
          handler={this.modelHandler}
          data={modelData}
        />
        <div className="h-100 statistic-wrapper d-table">
          <div className="table-row" ref={this.cardRef}>
            <div className="stats-details">{renderStatsItemCards()}</div>
          </div>
          {activeDetails !== "" ? (
            <StatsDetailsTable
              dataSet={filteredDataSet}
              listType={activeDetails}
              currentPage={currentPage}
              postPerPage={postPerPage}
              isMobile={isMobile}
              modelHandler={this.modelHandler}
              onSearch={this.onSearch}
              comingSoon={tableToShow === "Credit Card Transactions"}
              isExpanded={isExpanded}
              expandTable={this.expandTable}
            />
          ) : (
            <div className="table-row h-100" />
          )}
          <div className="table-row">
            {activeDetails !== "" ? (
              <Pagination
                pageNeighbours={1}
                postsPerPage={postPerPage}
                currentPage={currentPage}
                totalPosts={currentDataSet.length}
                paginate={this.setCurrentPage}
              />
            ) : null}
          </div>
        </div>
      </>
    );
  }
}

export default Statistics;
