import React from "react";
import coin from "../../../../../../assets/LedgerIcon/coin.svg";
import { formatNameForMobile, getDate, getTime } from "./utils";
import { CADIcon } from "../../../../../../assets/LedgerIcon";

const DirectUserItem = ({ data, isMobile, modelHandler }) => {
  const clickHandler = () => {
    console.log("Click Data=>", data);
    const modalData = {
      mobileCard: {
        isShow: isMobile,
        data: [
          {
            title: "Signup Date",
            value: getDate(data.timestamp) + " at" + getTime(data.timestamp)
          },
          { title: "Country", value: data.country }
        ]
      },
      firstCard: {
        isShow: false,
        data: [
          { title: "Affiliate ID", value: data.affiliate_id },
          {
            title: "Email",
            value: data.email
          }
        ]
      },
      secondCard: {
        isShow: data.status ? false : true,
        email: data.email,
        status: data.user_img_status
      }
    };

    modelHandler(modalData, true);
  };

  return (
    <div className="details-table-item" onClick={clickHandler}>
      <div className="flexed-item" style={{ width: isMobile ? "20%" : "3%" }}>
        <img src={coin} alt="" height="30px" />
      </div>
      <div
        className="flexed-item"
        style={{ width: isMobile ? "60%" : "45%", paddingLeft: "15px" }}
      >
        <label className="user-name">
          {isMobile ? formatNameForMobile(data.name) : data.name}
        </label>
      </div>
      {isMobile ? null : (
        <>
          <div className="flexed-item" style={{ width: "17%" }}>
            {/* <label>{data.country ? data.country : "-"}</label> */}
            <img className="user-country-icon" src={CADIcon} alt="" />
          </div>
          <div className="flexed-item" style={{ width: "30%" }}>
            <label className="user-signup-date">
              {getDate(data.timestamp)}
            </label>
            <label className="user-signup-time">
              {getTime(data.timestamp)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "35px" }} />
        </>
      )}
    </div>
  );
};

export default DirectUserItem;
