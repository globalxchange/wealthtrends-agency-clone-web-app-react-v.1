import React from "react";
import coin from "../../../../../../assets/LedgerIcon/coin.svg";
import {
  formatNameForMobile,
  getDate,
  getTime,
  parseCountryFromAddress,
  formatterWithTwoDigits,
  formatFromExpValues,
  formatToExpValue
} from "./utils";

const WithdrawalLedgerItem = ({ data, isMobile }) => {
  return (
    <div
      id="details-table-item"
      className="details-table-item"
      //   onClick={clickHandler}
    >
      <div className="flexed-item" style={{ width: isMobile ? "20%" : "3%" }}>
        <img src={coin} alt="" height="30px" />
      </div>
      <div
        className="flexed-item"
        style={{ width: isMobile ? "60%" : "22%", paddingLeft: "15px" }}
      >
        <label className="token-txn-name">{data.pid}</label>
      </div>
      {isMobile ? null : (
        <>
          <div className="flexed-item" style={{ width: "14%" }}>
            <label className="token-txn-date">{getDate(data.ts)}</label>
            <label className="token-txn-time">{getTime(data.ts)}</label>
          </div>
          <div className="flexed-item" style={{ width: "14%" }}>
            <label className="token-txn-date">
              {formatterWithTwoDigits.format(
                data.usd_amt
                  ? data.usd_amt
                  : data.withdrawl_amt
                  ? data.withdrawl_amt
                  : 0
              )}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "15%" }}>
            <label className="token-txn-broker">{data.quote}</label>
          </div>
          <div className="flexed-item" style={{ width: "16%" }}>
            <label className="token-txn-commission">
              {formatterWithTwoDigits.format(data.bal)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "16%" }}>
            <label className="token-txn-commission">
              {data.rejected && data.rejected === true
                ? "Rejected"
                : data.status}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "35px" }} />
        </>
      )}
    </div>
  );
};

export default WithdrawalLedgerItem;
