import React from "react";
import coin from "../../../../../../assets/LedgerIcon/coin.svg";
import { formatNameForMobile, getDate, getTime } from "./utils";
import { CADIcon } from "../../../../../../assets/LedgerIcon";

const itemRef = React.createRef();

const TotalUserItem = ({ data, isMobile, modelHandler }) => {
  const clickHandler = () => {
    const modalData = {
      mobileCard: {
        isShow: isMobile,
        data: [
          {
            title: "Signup Date",
            value: getDate(data.timestamp) + " at" + getTime(data.timestamp)
          },
          { title: "Country", value: data.country },
          { title: "Broker Name", value: data.broker_name }
        ]
      },
      firstCard: {
        isShow: true,
        data: [
          { title: "Affiliate ID", value: data.affiliate_id },
          {
            title: "Email",
            value: data.email
          }
        ]
      }
    };

    modelHandler(modalData, true);
  };

  return (
    <div
      id="details-table-item"
      className="details-table-item"
      ref={itemRef}
      onClick={clickHandler}
    >
      <div className="flexed-item" style={{ width: isMobile ? "20%" : "3%" }}>
        <img src={coin} alt="" height="30px" />
      </div>
      <div
        className="flexed-item"
        style={{ width: isMobile ? "60%" : "30%", paddingLeft: "15px" }}
      >
        <label className="user-name">
          {isMobile ? formatNameForMobile(data.name) : data.name}
        </label>
      </div>
      {isMobile ? null : (
        <>
          <div className="flexed-item" style={{ width: "10%" }}>
            {/* <label>{data.country ? data.country : "-"}</label> */}
            <img className="user-country-icon" src={CADIcon} alt="" />
          </div>
          <div className="flexed-item" style={{ width: "28%" }}>
            <label className="user-signup-date">
              {getDate(data.timestamp)}
            </label>
            <label className="user-signup-time">
              {getTime(data.timestamp)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "25%" }}>
            <label className="user-broker-name">{data.broker_name}</label>
          </div>
          <div className="flexed-item" style={{ width: "35px" }} />
        </>
      )}
    </div>
  );
};

export default TotalUserItem;
