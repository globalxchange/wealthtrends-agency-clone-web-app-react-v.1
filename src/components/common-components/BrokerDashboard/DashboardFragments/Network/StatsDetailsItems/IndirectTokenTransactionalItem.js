import React, { useState } from "react";
import coin from "../../../../../../assets/LedgerIcon/coin.svg";
import {
  formatNameForMobile,
  formatterWithTwoDigits,
  getDate,
  getTime,
  parseCountryFromAddress
} from "./utils";
import { CADIcon } from "../../../../../../assets/LedgerIcon";

const itemRef = React.createRef();

const IndirectTokenTransactionalItem = ({ data, isMobile, modelHandler }) => {
  const clickHandler = () => {
    const modalData = {
      mobileCard: {
        isShow: isMobile,
        data: [
          {
            title: "Date",
            value:
              getDate(data.txn.timestamp) + " at" + getDate(data.txn.timestamp)
          },
          {
            title: "Country",
            value: parseCountryFromAddress(data.txn.address)
          },
          {
            title: "Volume",
            value: formatterWithTwoDigits.format(data.txn.usd_amt)
          },
          {
            title: "Broker",
            value: data.txn.upline_name
          },
          {
            title: "DD",
            value: data.commissions.ps
          }
        ]
      },
      firstCard: {
        isShow: true,
        data: [
          { title: "Affiliate ID", value: data.txn.affiliate_id },
          {
            title: "Email",
            value: data.txn.email
          },
          {
            title: "TXN Type",
            value: data.debit ? "Withdraw" : "Deposit"
          }
        ]
      }
    };

    modelHandler(modalData, true);
  };

  return !data.commissions ? (
    "<div></div>"
  ) : (
    <div
      id="details-table-item"
      className="details-table-item"
      ref={itemRef}
      onClick={clickHandler}
    >
      <div className="flexed-item" style={{ width: isMobile ? "20%" : "3%" }}>
        <img src={coin} alt="" height="30px" />
      </div>
      <div
        className="flexed-item"
        style={{ width: isMobile ? "60%" : "28%", paddingLeft: "15px" }}
      >
        <label
          className="token-txn-name"
          style={isMobile ? { fontSize: "1.1rem" } : null}
        >
          {isMobile ? formatNameForMobile(data.txn.name) : data.txn.name}
        </label>
      </div>
      {isMobile ? null : (
        <>
          <div className="flexed-item" style={{ width: "10%" }}>
            <img className="token-txn-country-icon" src={CADIcon} alt="" />
            {/* <label className="token-txn-country">
              {parseCountryFromAddress(data.txn.address)}
            </label> */}
          </div>
          <div className="flexed-item " style={{ width: "20%" }}>
            <label className="token-txn-date">
              {getDate(data.txn.timestamp)}
            </label>
            {/*<h1>check</h1>*/}
            <label className="token-txn-time">
              {getTime(data.txn.timestamp)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "25%" }}>
            <label className="token-txn-broker">{data.txn.upline_name}</label>
            <label className="token-txn-dd">DD : {data.commissions.ps}</label>
          </div>
          <div className="flexed-item" style={{ width: "14%" }}>
            <label className="token-txn-commission">
              {data.commissions.ps > 0
                ? formatterWithTwoDigits.format(data.commissions.it_commission)
                : formatterWithTwoDigits.format(data.commissions.dt_commission)}
            </label>
          </div>
        </>
      )}
    </div>
  );
};

export default IndirectTokenTransactionalItem;
