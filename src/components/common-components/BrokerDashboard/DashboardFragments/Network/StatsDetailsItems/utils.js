import React from "react";
import mtzon from "moment-timezone";
import { Parser } from "html-to-react";

export const formatterWithTwoDigits = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 2
});

export const formatterWithThreeDigits = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 3
});

export const getTime = timestamp => {
  const date = new Date(timestamp);
  const m = mtzon.utc(date);
  return m
    .clone()
    .tz("America/New_York")
    .format("hh:mm A");
};

export const getDate = timestamp => {
  const date = new Date(timestamp);
  const m = mtzon.utc(date);

  const dateString = m
    .clone()
    .tz("America/New_York")
    .format("MMM Do YYYY");

  let formattedDate;

  if (dateString.match("st")) {
    formattedDate = dateString.replace("st", "<span class='ordinal'>st</span>");
  } else if (dateString.match("nd")) {
    formattedDate = dateString.replace("nd", "<span class='ordinal'>nd</span>");
  } else if (dateString.match("rd")) {
    formattedDate = dateString.replace("rd", "<span class='ordinal'>rd</span>");
  } else {
    formattedDate = dateString.replace("th", "<span class='ordinal'>th</span>");
  }

  const htmlParser = new Parser();

  return htmlParser.parse(formattedDate);
};

export const formatNameForMobile = fullName => {
  const names = fullName.split(" ");
  let retValue = names[0];
  if (names[1]) {
    retValue += " " + names[1].charAt(0).toUpperCase();
  }
  return retValue;
};

export const parseCountryFromAddress = address => {
  const country = address.split(",")[3];
  return country ? country : "-";
};

export const formatFromExpValues = value => {
  let strValue = value.toString();
  let formattedValue = `${strValue.substring(0, 3)}${strValue.substring(
    strValue.indexOf("e"),
    strValue.length
  )}`;
  return formattedValue;
};

export const formatToExpValue = value => {
  return value.toExponential(1);
};
