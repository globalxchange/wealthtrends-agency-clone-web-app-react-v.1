import React, { useState } from "react";
import coin from "../../../../../../assets/LedgerIcon/coin.svg";
import {
  formatNameForMobile,
  getDate,
  getTime,
  parseCountryFromAddress,
  formatterWithTwoDigits
} from "./utils";
import { CADIcon } from "../../../../../../assets/LedgerIcon";

const itemRef = React.createRef();

const DirectOTCRevenueItem = ({ data, isMobile, modelHandler }) => {
  const clickHandler = () => {
    const modalData = {
      mobileCard: {
        isShow: isMobile,
        data: [
          {
            title: "Date",
            value:
              getDate(data.txn.timestamp) + " at" + getTime(data.txn.timestamp)
          },
          {
            title: "Country",
            value: parseCountryFromAddress(data.txn.address)
          },
          {
            title: "Coin",
            value: "BTC"
          },
          {
            title: "Fee ($)",
            value: data.commissions.com
              ? formatterWithTwoDigits.format(data.commissions.com)
              : formatterWithTwoDigits.format(0)
          },
          { title: "Fee (%)", value: `${data.txn.broker_percentage} %` },
          {
            title: "My Revenue",
            value: formatterWithTwoDigits.format(data.commissions.dt_commission)
          }
        ]
      },
      firstCard: {
        isShow: true,
        data: [
          {
            title: "Email",
            value: data.txn.email
          },
          {
            title: "TXN Type",
            value: data.debit ? "Withdraw" : "Deposit"
          }
        ]
      }
    };

    modelHandler(modalData, true);
  };

  return (
    <div
      id="details-table-item"
      className="details-table-item"
      ref={itemRef}
      onClick={clickHandler}
    >
      <div className="flexed-item" style={{ width: isMobile ? "20%" : "3%" }}>
        <img src={coin} alt="" height="30px" />
      </div>
      <div
        className="flexed-item"
        style={{ width: isMobile ? "60%" : "26%", paddingLeft: "15px" }}
      >
        <label
          className="token-txn-name"
          style={isMobile ? { fontSize: "1.1rem" } : null}
        >
          {isMobile ? formatNameForMobile(data.txn.name) : data.txn.name}
        </label>
      </div>
      {isMobile ? null : (
        <>
          <div className="flexed-item" style={{ width: "8%" }}>
            <img className="token-txn-country-icon" src={CADIcon} alt="" />
            {/* <label className="token-txn-country">
              {parseCountryFromAddress(data.txn.address)}
            </label> */}
          </div>
          <div className="flexed-item" style={{ width: "18%" }}>
            <label className="token-txn-date">
              {getDate(data.txn.timestamp)}
            </label>
            <label className="token-txn-time">
              {getTime(data.txn.timestamp)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "10%" }}>
            <label className="token-txn-date">{"BTC"}</label>
          </div>
          <div className="flexed-item" style={{ width: "15%" }}>
            <label className="token-txn-commission">
              {data.commissions.com
                ? formatterWithTwoDigits.format(data.commissions.com)
                : formatterWithTwoDigits.format(0)}
            </label>
            <label className="token-txn-dd">{`${data.txn.broker_percentage} %`}</label>
          </div>
          <div className="flexed-item" style={{ width: "15%" }}>
            <label className="token-txn-commission">
              {formatterWithTwoDigits.format(data.commissions.dt_commission)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "35px" }} />
        </>
      )}
    </div>
  );
};

export default DirectOTCRevenueItem;
