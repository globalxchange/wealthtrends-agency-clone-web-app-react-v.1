import React, { useState } from "react";
import coin from "../../../../../../assets/LedgerIcon/coin.svg";
import {
  formatNameForMobile,
  getDate,
  getTime,
  parseCountryFromAddress,
  formatterWithTwoDigits,
  formatFromExpValues,
  formatToExpValue
} from "./utils";
import { CADIcon } from "../../../../../../assets/LedgerIcon";

const itemRef = React.createRef();
const TotalDigitalVolumeItem = ({ data, isMobile, modelHandler }) => {
  return (
    <>
      {data.txn ? (
        <div
          id="details-table-item"
          className="details-table-item"
          ref={itemRef}
          //   onClick={clickHandler}
        >
          <div
            className="flexed-item"
            style={{ width: isMobile ? "20%" : "3%" }}
          >
            <img src={coin} alt="" height="30px" />
          </div>
          <div
            className="flexed-item"
            style={{ width: isMobile ? "60%" : "24%", paddingLeft: "15px" }}
          >
            <label
              className="token-txn-name"
              style={isMobile ? { fontSize: "1.1rem" } : null}
            >
              {isMobile ? formatNameForMobile(data.txn.name) : data.txn.name}
            </label>
          </div>
          {isMobile ? null : (
            <>
              <div className="flexed-item" style={{ width: "18%" }}>
                <label className="token-txn-date">
                  {getDate(data.txn.timestamp)}
                </label>
                <label className="token-txn-time">
                  {getTime(data.txn.timestamp)}
                </label>
              </div>
              <div className="flexed-item" style={{ width: "12%" }}>
                <label className="token-txn-date">{data.txn.coin}</label>
              </div>
              <div className="flexed-item" style={{ width: "22%" }}>
                <label
                  className="token-txn-broker"
                  style={{ fontSize: "1.4rem" }}
                >
                  {data.txn.interest_calculated_on
                    ? `${formatToExpValue(data.txn.interest_calculated_on)} ETH`
                    : "0 ETH"}
                </label>
              </div>
              <div className="flexed-item" style={{ width: "16%" }}>
                <label
                  className="token-txn-commission"
                  style={{ fontSize: "1.4rem" }}
                >
                  {data.txn.earned_interest
                    ? `${formatFromExpValues(data.txn.earned_interest)} ETH`
                    : "0 ETH"}
                </label>
              </div>
              <div className="flexed-item" style={{ width: "35px" }} />
            </>
          )}
        </div>
      ) : null}
    </>
  );
};

export default TotalDigitalVolumeItem;
