import React, { useState } from "react";
import coin from "../../../../../../assets/LedgerIconcoin.svg";
import {
  formatNameForMobile,
  getDate,
  getTime,
  parseCountryFromAddress,
  formatterWithTwoDigits
} from "./utils";
import { CADIcon } from "../../../../../../assets/LedgerIcon";

const itemRef = React.createRef();
const MyRevenueItem = ({ data, isMobile, modelHandler }) => {
  return (
    <div
      id="details-table-item"
      className="details-table-item"
      ref={itemRef}
      //   onClick={clickHandler}
    >
      <div className="flexed-item" style={{ width: isMobile ? "20%" : "3%" }}>
        <img src={coin} alt="" height="30px" />
      </div>
      <div
        className="flexed-item"
        style={{ width: isMobile ? "60%" : "22%", paddingLeft: "15px" }}
      >
        <label
          className="token-txn-name"
          style={isMobile ? { fontSize: "1.1rem" } : null}
        >
          {isMobile ? formatNameForMobile(data.txn.name) : data.txn.name}
        </label>
      </div>
      {isMobile ? null : (
        <>
          <div className="flexed-item" style={{ width: "18%" }}>
            <label className="token-txn-date">
              {getDate(data.txn.timestamp)}
            </label>
            <label className="token-txn-time">
              {getTime(data.txn.timestamp)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "9%" }}>
            <label className="token-txn-date">{data.txn.coin}</label>
          </div>
          <div className="flexed-item" style={{ width: "12%" }}>
            <label className="token-txn-broker">
              {data.txn.interest_calculated_on
                ? formatterWithTwoDigits.format(data.txn.interest_calculated_on)
                : formatterWithTwoDigits.format(0)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "12%" }}>
            <label className="token-txn-commission">
              {data.txn.earned_interest
                ? formatterWithTwoDigits.format(data.txn.earned_interest)
                : formatterWithTwoDigits.format(0)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "12%" }}>
            <label className="token-txn-commission">
              {data.commissions.bdot
                ? formatterWithTwoDigits.format(data.commissions.bdot)
                : formatterWithTwoDigits.format(0)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "12%" }}>
            <label className="token-txn-commission">
              {data.commissions.bdot
                ? formatterWithTwoDigits.format(data.commissions.bdot)
                : formatterWithTwoDigits.format(0)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "35px" }} />
        </>
      )}
    </div>
  );
};

export default MyRevenueItem;
