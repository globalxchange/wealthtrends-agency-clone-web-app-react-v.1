import React, { useState } from "react";
import coin from "../../../../../../assets/LedgerIcon/coin.svg";
import {
  formatNameForMobile,
  getDate,
  getTime,
  parseCountryFromAddress,
  formatterWithTwoDigits,
  formatterWithThreeDigits
} from "./utils";
import { CADIcon } from "../../../../../../assets/LedgerIcon";

const itemRef = React.createRef();

const DirectTokenTransactionalItem = ({ data, isMobile, modelHandler }) => {
  const clickHandler = () => {
    const modalData = {
      mobileCard: {
        isShow: isMobile,
        data: [
          {
            title: "Date",
            value:
              getDate(data.txn.timestamp) + " at" + getTime(data.txn.timestamp)
          },
          {
            title: "Country",
            value: parseCountryFromAddress(data.txn.address)
          },
          {
            title: "Volume",
            value: formatterWithTwoDigits.format(data.txn.usd_amt)
          },
          {
            title: "Commission",
            value:
              data.commissions.ps > 0
                ? formatterWithTwoDigits.format(data.commissions.it_commission)
                : formatterWithTwoDigits.format(data.commissions.dt_commission)
          }
        ]
      },
      firstCard: {
        isShow: true,
        data: [
          { title: "Affiliate ID", value: data.txn.affiliate_id },
          {
            title: "Email",
            value: data.txn.email
          },
          {
            title: "TXN Type",
            value: data.debit ? "Withdraw" : "Deposit"
          }
        ]
      }
    };

    modelHandler(modalData, true);
  };

  return (
    <div
      id="details-table-item"
      className="details-table-item"
      ref={itemRef}
      onClick={clickHandler}
    >
      <div className="flexed-item" style={{ width: isMobile ? "20%" : "3%" }}>
        <img src={coin} alt="" height="30px" />
      </div>
      <div
        className="flexed-item"
        style={{ width: isMobile ? "60%" : "26%", paddingLeft: "15px" }}
      >
        <label
          className="token-txn-name"
          style={isMobile ? { fontSize: "1.1rem" } : null}
        >
          {isMobile ? formatNameForMobile(data.txn.name) : data.txn.name}
        </label>
      </div>
      {isMobile ? null : (
        <>
          <div className="flexed-item" style={{ width: "9%" }}>
            <img className="token-txn-country-icon" src={CADIcon} alt="" />
            {/* <label className="token-txn-country">
              {parseCountryFromAddress(data.txn.address)}
            </label> */}
          </div>
          <div className="flexed-item" style={{ width: "19%" }}>
            <label className="token-txn-date">
              {getDate(data.txn.timestamp)}
            </label>
            <label className="token-txn-time">
              {getTime(data.txn.timestamp)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "24%" }}>
            <label className="token-txn-volume">
              {formatterWithTwoDigits.format(data.txn.usd_amt)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "13%" }}>
            <label className="token-txn-commission">
              {data.commissions.ps > 0
                ? formatterWithTwoDigits.format(data.commissions.it_commission)
                : formatterWithTwoDigits.format(data.commissions.dt_commission)}
            </label>
          </div>
          <div className="flexed-item" style={{ width: "35px" }} />
        </>
      )}
    </div>
  );
};

export default DirectTokenTransactionalItem;
