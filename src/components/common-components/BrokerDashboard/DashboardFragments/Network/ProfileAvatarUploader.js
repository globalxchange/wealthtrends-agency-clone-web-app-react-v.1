import React, { Component } from "react";
import axios from "axios";
import fetch from "../../../../fetchurl";
import S3 from "aws-s3";

export class ProfileAvatarUploader extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleShowSimplex = this.handleShowSimplex.bind(this);
    this.handleCloseSimplex = this.handleCloseSimplex.bind(this);

    this.state = {
      show: false,
      showSimplex: false,
      file: null,
      imagePreviewUrl: "",
      name: "",
      error: "",
      isLoading: false,
      isDone: false
    };
  }

  handleClose() {
    this.setState({ show: false });
    
  }

  handleShow() {
    this.setState({ show: true });
  }

  handleCloseSimplex() {
    this.setState({ showSimplex: false });
  
  }

  handleShowSimplex() {
    this.setState({ showSimplex: true });
  }

  handleSubmit = async e => {
    e.preventDefault();
    if (this.state.file) {
      this.setState({ error: "", isLoading: true });
      console.log("handle uploading-", this.state.file);

      const config = {
        bucketName: "gxnitrousfiles",
        dirName: "user-images",

        region: "us-east-2",
        accessKeyId: "AKIA4HKGLPLPRBPVPM5X",
        secretAccessKey: "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"
      };

      const S3Client = new S3(config);
      //const newFileName = 'my-awesome-file';
      let uploaded = await S3Client.uploadFile(this.state.file);
      console.log("File Uploaded", uploaded);

      axios
        .post(fetch.url + "update_user_img", {
          user_img: uploaded.location,
          email: this.props.email,
          token: this.props.token
        })
        .then(resp => {
          console.log("Resp from profile pic update=>", resp);
          this.setState({ isDone: true, file: null });
          this.props.closeModel({}, false);
        })
        .catch(err => {
          console.log("Error from profile pic update=>", err);
          this.setState({ error: err });
        });
    } else {
      this.setState({ error: "Please select a file first" });
    }
  };

  handleImageChange(e) {
    e.preventDefault();

    let file = e.target.files[0];
    console.log("File=>", file);

    if (file.type.includes("image")) {
      this.setState({
        file: file,
        error: ""
      });
    } else {
      this.setState({ error: "Please select an image file" });
    }
  }

  render() {
    return (
      <div className="previewComponent">
        <form onSubmit={this.handleSubmit}>
          <div className="imageUploaderSectionWraper">
            <input
              className="fileInput"
              type="file"
              accept="image/x-png,image/jpeg"
              onChange={e => this.handleImageChange(e)}
            />
          </div>
          <div className="upLoadedImageimgPreview">
            <p
              style={{
                fontSize: "1.1em",
                fontWeight: "700",
                marginBottom: "0px"
              }}
            >
              {this.state.file
                ? `Uploaded File is : ${this.state.file.name}`
                : ""}
            </p>
          </div>
          <br />
          <div className="previewText">
            Click The Button Above And
            <span
              style={{
                fontWeight: "500",
                display: "block",
                fontSize: "1.2em"
              }}
            >
              {this.props.uploadText}
            </span>
          </div>
          {this.state.error !== "" ? (
            <p className="text-danger text-center">{this.state.error}</p>
          ) : null}
          <div className="uploadLogoAndName">
            <button className="btn btn-primary" disabled={!this.state.file}>
              {this.state.isDone
                ? "Uploaded"
                : this.state.isLoading
                ? "Uploading"
                : "Submit"}
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default ProfileAvatarUploader;
