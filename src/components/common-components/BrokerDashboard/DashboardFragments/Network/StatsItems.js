import React from "react";

const StatsItem = ({
  title,
  value,
  unit,
  width,
  keyValue,
  onChange,
  active,
  titleShow,
  noOfCards,
  nonClickable
}) => {
  console.log("value is for ", title, " is ", value);
  const amountFormatter = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  });

  const getTitleForCreditCardTransaction = () => {
    const titles = titleShow.split("-");
    return (
      <>
        {titles[0]}
        <span>
          <br /> ({titles[1]})
        </span>
      </>
    );
  };

  return (
    <div
      className={"stats-item" + (active === title ? " active" : "")}
      style={{ width: `${width}%` }}
      onClick={
        keyValue === "crediCardTransaction" || nonClickable
          ? null
          : () => onChange(title)
      }
    >
      <h6
        className="stats-title"
        style={
          keyValue === "crediCardTransaction"
            ? { fontSize: "0.85rem" }
            : noOfCards > 3
            ? { fontSize: "0.92rem" }
            : null
        }
      >
        {keyValue === "crediCardTransaction"
          ? getTitleForCreditCardTransaction()
          : titleShow}
      </h6>
      <h3
        className="stats-value"
        style={noOfCards > 3 ? { fontSize: "1.5rem" } : null}
      >
        {value !== undefined
          ? value === "Retrieving" || value === "Coming Soon"
            ? value
            : unit === "USD"
            ? `$ ${amountFormatter.format(parseFloat(value))}`
            : value
          : "Retrieving"}
      </h3>
      <h6 className="stats-unit">{unit}</h6>
    </div>
  );
};

export default StatsItem;
