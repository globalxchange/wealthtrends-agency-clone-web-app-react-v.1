import React from "react";
import { Table } from "react-bootstrap";

const Transactions = () => {
  return (
    <div className="row virtual-otc-transactions">
      <div className="col-md-4">
        <div className="otc-transactions-card-wrapper">
          <div className="otc-transactions-card exchange">
            <h3>18</h3>
            <span>Exchanges</span>
          </div>
          <div className="otc-transactions-card withdraw">
            <h3>26</h3>
            <span>Withdraws</span>
          </div>
        </div>
      </div>
      <div className="col-md-8">
        <div className="transaction-overview">
          <Table borderless>
            <thead>
              <tr>
                <th>EUR TO USD</th>
                <th>LAST 30 DAYS</th>
                <th>LAST 90 DAYS</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>High</td>
                <td>1.13323</td>
                <td>1.65662</td>
              </tr>
              <tr>
                <td>Low</td>
                <td>1.13323</td>
                <td>1.65662</td>
              </tr>
              <tr>
                <td>Average</td>
                <td>1.13323</td>
                <td>1.65662</td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
    </div>
  );
};

export default Transactions;
