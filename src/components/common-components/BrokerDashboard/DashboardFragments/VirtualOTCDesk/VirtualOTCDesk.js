import React, { useState, useContext } from "react";
import Converter from "./Converter";
import CommissionForm from "./CommissionForm";
import axios from "axios";
import { brokerage } from "../../../contextapi/Contextapi";

const VirtualOTCDesk = () => {
  const context = useContext(brokerage);

  const [brokerFeeLoading, setBrokerFeeLoading] = useState(false);
  const [brokerSellingFeeLoading, setBrokerSellingFeeLoading] = useState(false);

  const changeBrokerFee = value => {
    setBrokerFeeLoading(true);

    let options = { timeZone: "America/New_York" }, // you have to know that New York in EST
      estTime = new Date();
    let date = estTime.toLocaleString("en-US", options);
    let timestamp = Date.now();

    axios
      .post("https://comms.globalxchange.com/btcbrokerage/save_percentage", {
        broker_id: context.affiliate_id,
        date: date,
        timestamp: timestamp,
        percentage: value
      })
      .then(r => {
        console.log("Params=>", r.request);
        setBrokerFeeLoading(false);
        window.location.reload();
      })
      .catch(error => {
        console.debug("Error on upding broker fee", error);
        setBrokerFeeLoading(false);
      });
  };
  const changeBrokerSellingFee = value => {
    setBrokerSellingFeeLoading(true);
    axios
      .post("https://comms.globalxchange.com/otc/update_sell_percentage", {
        affiliate_id: context.affiliate_id,
        sell_percentage: value
      })
      .then(res => {
        console.log("Params=>", res.request);
        setBrokerSellingFeeLoading(false);
        context.update_sellPercentage(value);
      })
      .catch(error => {
        setBrokerSellingFeeLoading(false);
        console.log("Error on updating selling broker fee ", error);
      });
  };

  console.log("Context frm OTC", context);

  return (
    <div className="frag-wrapper">
      <Converter
        wholeSaleRate={context.nvest_rate}
        myFee={context.broker_fee}
        retailPrice={context.listed_price}
      />
      <CommissionForm
        changeBrokerFee={changeBrokerFee}
        changeBrokerSellingFee={changeBrokerSellingFee}
        brokerFeeLoading={brokerFeeLoading}
        brokerSellingFeeLoading={brokerSellingFeeLoading}
      />
    </div>
  );
};

export default VirtualOTCDesk;
