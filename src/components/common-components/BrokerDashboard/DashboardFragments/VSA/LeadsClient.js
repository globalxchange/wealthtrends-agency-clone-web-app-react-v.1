import React, { useState } from "react";
import { Table } from "react-bootstrap";
import "./LeadsClients.css";
import { IconButton } from "@material-ui/core";
import { ArrowBackSharp } from "@material-ui/icons";
import { Button } from "antd";
import Pipelines from "./Pipelines";
import LeadsClientsUserDetails from "./LeadsClientsUserDetails";

const LeadsClient = ({
  onClose,
  allData,
  activeCampaign,
  searchQuery,
  searchCategory
}) => {
  const data = allData.filter(item => item.campaign === activeCampaign);

  const [activeUserDetails, setActiveUserDetails] = useState(data[0]);
  const [isPipeLineOpen, setIsPipeLineOpen] = useState(false);
  const [isDetailsDrawerOpen, setIsDetailsDrawerOpen] = useState(false);

  const setDrawerData = data => {
    setIsDetailsDrawerOpen(true);
    setActiveUserDetails(data);
  };

  return (
    <>
      {!isDetailsDrawerOpen ? (
        <div className="close-btn-wrapper">
          <IconButton aria-label="close" onClick={() => onClose(false)}>
            <ArrowBackSharp />
          </IconButton>
        </div>
      ) : null}
      {isPipeLineOpen ? (
        <Pipelines
          setIsPipeLineOpen={setIsPipeLineOpen}
          allData={allData}
          searchQuery={searchQuery}
          searchCategory={searchCategory}
          setDrawerData={setDrawerData}
        />
      ) : (
        <div className="leads-clients-wrapper">
          <div className="row">
            <Button type="primary" onClick={() => setIsPipeLineOpen(true)}>
              Pipeline
            </Button>
          </div>
          <div className="row">
            <div className="py-2 w-100">
              {data.length > 0 ? (
                <div className="leads-table-wrapper">
                  <div className="leads-table-container">
                    <Table hover className="p-3 da-table leads-table">
                      <thead>
                        <tr>
                          <th>Lead ID / Name</th>
                          <th>Status</th>
                          <th>Actions</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        {data.map(item => (
                          <tr
                            key={item._id}
                            style={{ cursor: "pointer" }}
                            onClick={() => setDrawerData(item)}
                          >
                            <td>
                              <div className="d-flex align-items-center">
                                <img
                                  src={"https://i.pravatar.cc/110"}
                                  className="pic-user"
                                  alt="no_img"
                                />
                                <div>
                                  <h6 className="mb-0">{item._id}</h6>
                                  <small>{item.name}</small>
                                </div>
                              </div>
                            </td>
                            <td>
                              <small
                                className="user-tag"
                                style={{
                                  backgroundColor: "#ffcccc"
                                }}
                              >
                                {item.type}
                              </small>
                            </td>
                            <td>
                              <h6 className="mb-0">Login</h6>
                              <div>{item.date}</div>
                            </td>
                            <td>
                              <div className="d-flex justify-content-between flex-row">
                                <Button
                                  type="primary"
                                  style={{ position: "unset" }}
                                >
                                  Contact
                                </Button>
                                <Button
                                  type="primary"
                                  style={{ position: "unset" }}
                                >
                                  Edit
                                </Button>
                              </div>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </div>
                </div>
              ) : (
                <div className="empty-container">
                  <h1 className="text-center">No data found....</h1>
                </div>
              )}
            </div>
          </div>
        </div>
      )}
      <LeadsClientsUserDetails
        activeUserDetails={activeUserDetails}
        isDrawerOpen={isDetailsDrawerOpen}
        setIsDrawerOpen={setIsDetailsDrawerOpen}
      />
    </>
  );
};

export default LeadsClient;
