import React, { useState } from "react";
import { Tabs } from "antd";
import { Drawer } from "@material-ui/core";
import { IconButton } from "@material-ui/core";
import { ArrowBackSharp } from "@material-ui/icons";
const { TabPane } = Tabs;

const LeadsClientsUserDetails = ({
  activeUserDetails,
  isDrawerOpen,
  setIsDrawerOpen
}) => {
  const [activeTab, setActiveTab] = useState(1);

  const handleChange = key => {
    setActiveTab(key);
  };

  return (
    <Drawer
      anchor="right"
      open={isDrawerOpen}
      style={{ zIndex: "9999999" }}
      onClose={() => setIsDrawerOpen(false)}
    >
      <div className="close-btn-wrapper">
        <IconButton aria-label="close" onClick={() => setIsDrawerOpen(false)}>
          <ArrowBackSharp />
        </IconButton>
      </div>
      {activeUserDetails ? (
        <div className="leads-user-details-wrapper">
          <div className="leads-user-details-container">
            <div className="leads-user-details-basic-wrapper">
              <div className="d-flex leads-user-details-basic">
                <img
                  className="m-1 leads-user-avatar"
                  src={"https://i.pravatar.cc/110"}
                  style={{ width: "6rem" }}
                  alt="no_img"
                />
                <div className="m-1">
                  <h5 className="user-details-name">
                    {activeUserDetails.name}
                  </h5>
                  <p className="mb-2">Head of Development</p>
                </div>
              </div>
            </div>
            <div className="leads-user-details-more">
              <div className="my-2 more-container">
                <Tabs defaultActiveKey="1" onChange={handleChange}>
                  <TabPane tab="Profile" key="1">
                    <div className="d-flex">
                      <p className="mb-0 more-details-key">Email:</p>
                      <p className="ml-auto text-right mb-0 more-details-value">
                        {activeUserDetails.email}
                      </p>
                    </div>
                    <div className="d-flex">
                      <p className="mb-0 more-details-key">Phone:</p>
                      <p className="ml-auto text-right mb-0 more-details-value">
                        44(76)34254578
                      </p>
                    </div>
                    <div className="d-flex">
                      <p className="mb-0 more-details-key">Location:</p>
                      <p className="ml-auto text-right mb-0 more-details-value">
                        Melbourne
                      </p>
                    </div>
                    <div className="btn w-100 text-left user-btn">
                      Profile Overview
                    </div>
                    <div className="btn w-100 text-left user-btn">
                      Personal Information
                    </div>
                    <div className="btn w-100 text-left user-btn">
                      Account Information
                    </div>
                    <div className="btn w-100 text-left user-btn">
                      Change Password
                    </div>
                    <div className="btn w-100 text-left user-btn">
                      Email Setting
                    </div>
                    <div className="btn w-100 text-left user-btn">
                      Saved Credit Cards
                    </div>
                    <div className="btn w-100 text-left user-btn">
                      Tax Information
                    </div>
                    <div className="btn w-100 text-left user-btn">
                      Statements
                    </div>
                  </TabPane>
                  <TabPane tab="Marketing" key="2">
                    Content of Tab Pane 2
                  </TabPane>
                  <TabPane tab="Interactions" key="3">
                    Content of Tab Pane 3
                  </TabPane>
                </Tabs>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </Drawer>
  );
};

export default LeadsClientsUserDetails;
