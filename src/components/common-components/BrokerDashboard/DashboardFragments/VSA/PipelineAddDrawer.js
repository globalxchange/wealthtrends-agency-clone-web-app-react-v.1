import React from "react";
import { Drawer } from "@material-ui/core";

const PipelineAddDrawer = ({
  isDrawerOpen,
  setIsDrawerOpen,
  inputChange,
  handleSubDeal
}) => {
  return (
    <Drawer
      anchor="right"
      open={isDrawerOpen}
      style={{ zIndex: "9999999" }}
      onClose={() => setIsDrawerOpen(false)}
    >
      {/* {sideList('right')} */}
      <div
        className="container-fluid py-3"
        style={{ width: "40rem" }}
        role="presentation"
        // onClick={this.toggleDrawer("right", false)}
        // onKeyDown={this.toggleDrawer("right", false)}
      >
        <form onSubmit={e => e.preventDefault()}>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Name</label>
            <div className="col-sm-8">
              <input
                type="text"
                className="form-control"
                name="dealName"
                placeholder="Deal Name"
                onChange={e => inputChange(e)}
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Description</label>
            <div className="col-sm-8">
              <textarea
                type="text"
                className="form-control"
                name="dealDescription"
                placeholder="Description"
                onChange={e => inputChange(e)}
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Track</label>
            <div className="col-sm-8">
              <select className="custom-select mr-sm-2">
                <option value="1">Default</option>
                <option value="2">+ Add Track</option>
              </select>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Milestone</label>
            <div className="col-sm-8">
              <div
                style={{
                  border: "1px solid #ced4da",
                  borderRadius: "0.25rem"
                }}
              >
                <div
                  value="1"
                  className="milstone-select"
                  // onClick={selectNewFunc}
                >
                  New
                </div>
                <div
                  value="2"
                  className="milstone-select"
                  // onClick={selectProspectFunc}
                >
                  Prospect
                </div>
                <div
                  value="3"
                  className="milstone-select"
                  // onClick={selectProposalFunc}
                >
                  Proposal
                </div>
                <div
                  value="4"
                  className="milstone-select"
                  // onClick={selectWonFunc}
                >
                  Won
                </div>
                <div
                  value="5"
                  className="milstone-select"
                  // onClick={selectLostFunc}
                >
                  Lost
                </div>
              </div>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Amount</label>
            <div className="col-sm-3 pr-1">
              <select
                className="custom-select mr-sm-2"
                name="dealAmoutType"
                onChange={e => inputChange(e)}
              >
                <option value="1">USD</option>
                <option value="2">INR</option>
              </select>
            </div>
            <div className="col-sm-5 pl-1">
              <input
                type="number"
                className="form-control"
                name="dealAmount"
                placeholder="Deal Amount"
                onChange={e => inputChange(e)}
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Close Date</label>
            <div className="col-sm-8">
              <input
                type="datetime-local"
                className="form-control"
                name="dealCloseDate"
                placeholder="Close Date"
                onChange={e => inputChange(e)}
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Note</label>
            <div className="col-sm-8">
              <textarea
                type="text"
                className="form-control"
                placeholder="Note"
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Owner</label>
            <div className="col-sm-8">
              <select className="custom-select mr-sm-2">
                <option value="1">emailname@email.com</option>
                <option value="2">Select</option>
              </select>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Contacts</label>
            <div className="col-sm-8">
              <input
                type="text"
                className="form-control"
                placeholder="Enter Contact Name/Email"
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Companies</label>
            <div className="col-sm-8">
              <input
                type="text"
                className="form-control"
                placeholder="Enter Company"
              />
            </div>
          </div>
          <div className="text-right">
            <button
              type="submit"
              className="btn btn-danger mx-1"
              onClick={handleSubDeal}
            >
              Save
            </button>
            <button
              className="btn btn-outline-danger mx-1"
              onClick={() => setIsDrawerOpen(false)}
            >
              Cancel
            </button>
          </div>
        </form>
      </div>
    </Drawer>
  );
};

export default PipelineAddDrawer;
