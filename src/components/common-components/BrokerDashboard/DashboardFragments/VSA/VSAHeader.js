import React from "react";
import { Menu, Dropdown, Button, Icon } from "antd";

const VSAHeader = () => {
  const menu = (
    <Menu onClick={e => console.log(e.key)}>
      <Menu.Item key={"All Campaigns"}>
        <Icon type="filter" /> All Campaigns
      </Menu.Item>
      <Menu.Item key={"All Campaigns"}>
        <Icon type="filter" /> All Campaigns
      </Menu.Item>
      <Menu.Item key={"All Campaigns"}>
        <Icon type="filter" /> All Campaigns
      </Menu.Item>
      <Menu.Item key={"All Campaigns"}>
        <Icon type="filter" /> All Campaigns
      </Menu.Item>
    </Menu>
  );
  return (
    <div className="vsa-header">
      <Dropdown overlay={menu}>
        <Button>
          <Icon type="filter" /> All Campaigns <Icon type="down" />
        </Button>
      </Dropdown>
      <div className="add-campaigns-btn">
        <Icon type="plus" /> Add Campaign
      </div>
    </div>
  );
};

export default VSAHeader;
