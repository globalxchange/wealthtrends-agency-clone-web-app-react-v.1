import React from "react";
import DraggableCard from "./DraggableCard";

const PipelineBoards = ({ pipelines, title, setDrawerData }) => {
  return (
    <div className="pipeline-boards" draggable="false">
      <div
        className="d-flex p-2 mx-2 mb-0"
        style={{ backgroundColor: "#c9dde4" }}
      >
        <h6>{title}</h6>
        <div className="ml-auto">>></div>
      </div>
      <div className="dropzone" id="idNew">
        {pipelines.map((item, index) => (
          <DraggableCard
            key={index}
            data={item}
            setDrawerData={setDrawerData}
          />
        ))}
      </div>
    </div>
  );
};

export default PipelineBoards;
