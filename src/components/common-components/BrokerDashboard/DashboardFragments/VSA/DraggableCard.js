import React from "react";
import moment from "moment-timezone";

const DraggableCard = ({ pDrag, data, setDrawerData }) => {
  return (
    <div
      id="draggable"
      draggable="true"
      onDragStart={pDrag}
      className="mb-2"
      onClick={() => setDrawerData(data)}
    >
      <div>ID: {data._id}</div>
      <div>Name: {data.name}</div>
      <div>Email: {data.email}</div>
      <div>Campaign: {data.campaign}</div>
      <div>Create Date: {moment(data.timestamp).format("l HH:mm A")}</div>
    </div>
  );
};

export default DraggableCard;
