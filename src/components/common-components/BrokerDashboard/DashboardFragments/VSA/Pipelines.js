import React, { useState, useEffect, useContext } from "react";
import "./DealDrag.css";
import PipelineAddDrawer from "./PipelineAddDrawer";
import { brokerage } from "../../../contextapi/Contextapi";
import Axios from "axios";
import fetch from "../../../../fetchurl";
import PipelineBoards from "./PipelineBoards";
import moment from "moment";
import { Button } from "antd";

const Pipelines = ({
  searchQuery,
  searchCategory,
  setIsPipeLineOpen,
  allData,
  setDrawerData
}) => {
  const leadsData = allData;
  console.log("ALLDATA", allData);
  const [activeData, setActiveData] = useState(allData);
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  const [inputValues, setInputValues] = useState({});
  const [dealNewInfo, setDealNewInfo] = useState([]);

  const inputChange = e => {
    setInputValues({ ...inputValues, [e.target.name]: e.target.value });
  };

  const handleSubDeal = e => {
    e.preventDefault();
    let newData = {
      dealName: inputValues.dealName,
      dealDesc: inputValues.dealDescription,
      dealType: inputValues.dealAmoutType,
      dealAmount: inputValues.dealAmount,
      dealCloseDate: inputValues.dealCloseDate
    };
    setDealNewInfo([...dealNewInfo, newData]);
    setIsDrawerOpen(false);
  };

  let dragged;
  const onDrag = event => {};
  const onDragStart = event => {
    if (event.target.id === "draggable") {
      dragged = event.target;
      event.target.style.opacity = 0.5;
    } else {
      event.preventDefault();
    }
  };
  const onDragEnd = event => {
    event.target.style.opacity = "";
  };
  const onDragOver = event => {
    event.preventDefault();
  };
  const onDragEnter = event => {
    if (event.target.className === "dropzone") {
      event.target.style.background = "#ccdbe5";
    }
  };
  const onDragLeave = event => {
    if (event.target.className === "dropzone") {
      event.target.style.background = "";
    }
  };
  const onDrop = event => {
    event.preventDefault();
    if (event.target.className === "dropzone") {
      event.target.style.background = "";
      dragged.parentNode.removeChild(dragged);
      event.target.appendChild(dragged);
    }
  };

  useEffect(() => {
    document.addEventListener("drag", onDrag, false);
    document.addEventListener("dragstart", onDragStart, false);
    document.addEventListener("dragend", onDragEnd, false);
    document.addEventListener("dragover", onDragOver, false);
    document.addEventListener("dragenter", onDragEnter, false);
    document.addEventListener("dragleave", onDragLeave, false);
    document.addEventListener("drop", onDrop, false);
    return () => {
      document.removeEventListener("drag", onDrag);
      document.removeEventListener("dragstart", onDragStart);
      document.removeEventListener("dragend", onDragEnd);
      document.removeEventListener("dragover", onDragOver);
      document.removeEventListener("dragenter", onDragEnter);
      document.removeEventListener("dragleave", onDragLeave);
      document.removeEventListener("drop", onDrop);
    };
  }, []);

  const filter = () => {
    if (searchQuery !== "" && leadsData) {
      if (searchCategory) {
        switch (searchCategory.title) {
          case "Name":
            setActiveData(
              leadsData.filter(item =>
                item.name.toLowerCase().includes(searchQuery.toLowerCase())
              )
            );
            break;
          case "Date":
            setActiveData(
              leadsData.filter(
                item => moment(item.timestamp).isSame(searchQuery, "day")
                // item.name.toLowerCase().includes(searchQuery.toLowerCase())
              )
            );
            break;
          case "Campaign":
            setActiveData(
              leadsData.filter(item =>
                item.campaign.toLowerCase().includes(searchQuery.toLowerCase())
              )
            );
            break;
          case "Status":
            setActiveData(
              leadsData.filter(item =>
                item.type.toLowerCase().includes(searchQuery.toLowerCase())
              )
            );
            break;
          default:
            setActiveData(
              leadsData.filter(item =>
                item.name.toLowerCase().includes(searchQuery.toLowerCase())
              )
            );
        }
      } else {
        setActiveData(
          leadsData.filter(item =>
            item.name.toLowerCase().includes(searchQuery.toLowerCase())
          )
        );
      }
    } else {
      setActiveData(leadsData);
    }
  };

  useEffect(() => {
    filter();
    return () => {};
  }, [searchQuery]);

  return (
    <div className="container-fluid px-0" style={{ marginTop: "3.5rem" }}>
      <div className="actions-wrapper">
        <Button type="primary" onClick={() => setIsPipeLineOpen(false)}>
          Leads Clients
        </Button>
        <Button
          type="primary"
          className="btn btn-danger"
          onClick={() => setIsDrawerOpen(true)}
        >
          Create New
        </Button>
      </div>
      <div className="pipeline-boards-wrapper">
        <div className="d-flex my-4 pipeline-boards-container">
          <PipelineBoards
            pipelines={
              activeData ? activeData.filter(item => item.type !== "Lead") : []
            }
            title={"Engaged"}
            setDrawerData={setDrawerData}
          />
          <PipelineBoards
            pipelines={
              activeData ? activeData.filter(item => item.type === "Lead") : []
            }
            title={"Captured"}
            setDrawerData={setDrawerData}
          />
          <PipelineBoards
            pipelines={[]}
            title={"Educated"}
            setDrawerData={setDrawerData}
          />
          <PipelineBoards
            pipelines={[]}
            title={"Clipped"}
            setDrawerData={setDrawerData}
          />
          <PipelineBoards
            pipelines={[]}
            title={"Closed"}
            setDrawerData={setDrawerData}
          />
        </div>
      </div>
      <PipelineAddDrawer
        isDrawerOpen={isDrawerOpen}
        setIsDrawerOpen={setIsDrawerOpen}
        inputChange={inputChange}
        handleSubDeal={handleSubDeal}
      />
    </div>
  );
};

export default Pipelines;
