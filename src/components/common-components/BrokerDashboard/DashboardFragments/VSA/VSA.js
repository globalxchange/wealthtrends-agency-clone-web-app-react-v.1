import React, { useState, useEffect, useContext } from "react";
import VSACards from "./VSACards";
import LeadsClient from "./LeadsClient";
import { c1, c2, c3, c4, c5 } from "../../../../images/VSAImages/";
import Axios from "axios";
import fetch from "../../../../fetchurl";
import { brokerage } from "../../../contextapi/Contextapi";

const VSA = ({ searchQuery, setSearchQuery, searchCategory }) => {
  const context = useContext(brokerage);
  const [isLeadsOpen, setIsLeadsOpen] = useState(false);
  const [leadsData, setLeadsData] = useState(null);
  const [filteredLeads, setFilteredLeads] = useState([]);
  const [activeCampaign, setActiveCampaign] = useState("");

  let campaignData = [
    {
      key: "c1",
      image: c1,
      title: "Set Yourself Up For The Next Bitcoin Bull Run",
      status: "Live",
      category: "Markets",
      seen: 0,
      clicks: 0,
      leads: 0,
      converts: 0
    },
    {
      key: "c2",
      image: c2,
      title: "Bitcoin Is Breaking Resistance! Learn How You Can Profit From It",
      status: "Live",
      category: "Markets",
      seen: 0,
      clicks: 0,
      leads: 0,
      converts: 0
    },
    {
      key: "c3",
      image: c3,
      title: "Have You Heard Of GXBroker?",
      status: "Live",
      category: "General",
      seen: 0,
      clicks: 0,
      leads: 0,
      converts: 0
    },
    {
      key: "c4",
      image: c4,
      title: "What Is GX? Is It Just Hype?",
      status: "Live",
      category: "General",
      seen: 0,
      clicks: 0,
      leads: 0,
      converts: 0
    },
    {
      key: "c5",
      image: c5,
      title: "Have You Heard Of Learn How You Can #MasterBitcoinsToday",
      status: "Live",
      category: "General",
      seen: 0,
      clicks: 0,
      leads: 0,
      converts: 0
    }
  ];

  useEffect(() => {
    if (context.username !== "" && !leadsData) {
      Axios.get(`${fetch.url}campaign/getleads/${context.username}`).then(
        resp => {
          setLeadsData(resp.data.leads ? resp.data.leads : []);
          setFilteredLeads(resp.data.leads ? resp.data.leads : []);
        }
      );
    }
  });

  useEffect(() => {
    setSearchQuery("");
  }, [isLeadsOpen]);

  useEffect(() => {
    if (searchQuery !== "" && isLeadsOpen) {
      setFilteredLeads(
        leadsData.filter(
          item =>
            item.name.toLowerCase().includes(searchQuery.toLowerCase()) ||
            item.email.toLowerCase().includes(searchQuery.toLowerCase())
        )
      );
    }
    return () => {};
  }, [searchQuery]);

  console.log("searchCategory=>", searchCategory);

  if (!isLeadsOpen) {
    if (searchCategory && searchCategory.title !== "All") {
      campaignData = campaignData.filter(
        item =>
          item.category === searchCategory.title &&
          item.title.toLowerCase().includes(searchQuery.toLowerCase())
      );
    } else {
      campaignData = campaignData.filter(item =>
        item.title.toLowerCase().includes(searchQuery.toLowerCase())
      );
    }
  }

  return (
    <div className="frag-wrapper">
      {isLeadsOpen ? (
        <LeadsClient
          onClose={setIsLeadsOpen}
          allData={filteredLeads ? filteredLeads : []}
          activeCampaign={activeCampaign}
          searchQuery={searchQuery}
          searchCategory={searchCategory}
        />
      ) : (
        <>
          {/* <VSAHeader /> */}
          <div className="vsa-cards-wrapper">
            {campaignData.length > 0 ? (
              campaignData.map(item => (
                <VSACards
                  key={item.key}
                  onClick={() => {
                    setActiveCampaign(item.key);
                    setIsLeadsOpen(true);
                  }}
                  data={item}
                />
              ))
            ) : (
              <h3 className="text-center w-100">No campaigns found...</h3>
            )}
          </div>
        </>
      )}
    </div>
  );
};

export default VSA;
