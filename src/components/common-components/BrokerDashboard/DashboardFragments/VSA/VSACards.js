import React from "react";
import { Icon } from "antd";

const VSACards = ({ onClick, data }) => {
  return (
    <div className="vsa-cards" onClick={onClick}>
      <div
        className="vsa-featured-img"
        style={{ backgroundImage: `url(${data.image})` }}
      >
        <label className="vsa-status">{data.status}</label>
      </div>
      <div className="vsa-details">
        <div className="vsa-title-wrapper">
          <h4 className="vsa-title">{data.title}</h4>
          <div className="vsa-views">
            <Icon type="eye" theme="filled" /> {data.seen}
          </div>
        </div>
        <div className="vsa-keys">
          <div className="vsa-key-item">
            <label className="key-title">Clicks</label>
            <label className="key-value">{data.clicks}</label>
          </div>
          <div className="vsa-key-item">
            <label className="key-title">Leads</label>
            <label className="key-value">{data.leads}</label>
          </div>
          <div className="vsa-key-item">
            <label className="key-title">Converts</label>
            <label className="key-value">{data.converts}</label>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VSACards;
