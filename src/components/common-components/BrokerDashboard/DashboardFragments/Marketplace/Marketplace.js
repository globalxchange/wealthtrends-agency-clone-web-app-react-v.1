import React, { useState } from "react";
import Filter from "./Filter";
import CardItem from "./CardItem";
import marketplaceList from "./marketplaceList";
import MarketPlaceDetails from "./MarketPlaceDetails";
import GXLogo from "../../../../images/gxok.png";

const Marketplace = ({ isMobile, categories, searchQuery, navBarHeight }) => {
  const [activeFilter, setActiveFilter] = useState(categories[0].title);
  const [activeDetails, setActiveDetails] = useState(marketplaceList[0]);

  const renderCards = () => {
    const cardsArray = [];
    const currentCount = categories.find(item => item.title === activeFilter)
      .count;
    for (let i = 0; i < currentCount; i++) {
      if (
        marketplaceList[0].title
          .toLocaleLowerCase()
          .includes(searchQuery.toLocaleLowerCase())
      ) {
        cardsArray.push(<CardItem data={marketplaceList[0]} />);
      }
    }
    return cardsArray.length > 0 ? (
      cardsArray
    ) : (
      <h1 className="text-center w-100">No Items Found...</h1>
    );
  };

  return (
    <div className="frag-wrapper">
      <div className="row">
        <Filter
          categories={categories}
          active={activeFilter}
          setActive={setActiveFilter}
          title="Categories"
        />
        <div className="col-md-10">
          <div className="row">
            <div className="col-md-4">
              <div className="row">{renderCards()}</div>
            </div>
            {activeDetails && (
              <MarketPlaceDetails
                navBarHeight={navBarHeight}
                data={activeDetails}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Marketplace;
