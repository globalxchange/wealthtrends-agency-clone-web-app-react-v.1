import React, { useContext, useState, useEffect } from "react";
import { brokerage } from "../../../../Brokercontextapi/Contextapi";
import Axios from "axios";
import fetch from "../../../../fetchurl.js";

const formatterWithTwoDigits = new Intl.NumberFormat("en-US", {
  minimumFractionDigits: 2,
  maximumFractionDigits: 2
});

const AffiliateCards = () => {
  const context = useContext(brokerage);
  const [totalUsers, setTotalUsers] = useState(0);
  const [brokerageVolume, setBrokerageVolume] = useState(-1);
  const [realizedEarning, setRealizedEarning] = useState(-1);
  const [totalCryptoAssetBrokerage, setTotalCryptoAssetBrokerage] = useState(
    -1
  );
  const [brokerageTransactions, setBrokerageTransactions] = useState(-1);
  const [moneyMarketTransactions, setMoneyMarketTransactions] = useState(-1);
  const [totalOTCVolume, setTotalOTCVolume] = useState(-1);
  const revenues = [
    {
      title: "Total Sales",
      value: formatterWithTwoDigits.format(brokerageVolume),
      unit: "$",
      background: "cardsec"
    },
    {
      title: "Total Commissions",
      value: formatterWithTwoDigits.format(realizedEarning),
      unit: "$",
      background: "cardsec"
    },
    {
      title: "Earnings Wallet",
      value: formatterWithTwoDigits.format(
        context.logs[0] ? context.logs[0].balance : 0
      ),
      unit: "$",
      background: "cardsec"
    },
    {
      title: "Withdrawal Balance",
      value: formatterWithTwoDigits.format(parseFloat(context.balance)),
      unit: "$",
      background: "cardsec"
    },
    // {
    //   title: "Total Crypto Assets In Brokerage (USD)",
    //   value: formatterWithTwoDigits.format(
    //     parseFloat(totalCryptoAssetBrokerage)
    //   ),
    //   unit: "$",
    //   background: "cardsec"
    // },
    // {
    //   title: "Brokerage Transactions (All Time)",
    //   value: brokerageTransactions,
    //   unit: "",
    //   background: "cardsec"
    // },
    // {
    //   title: "Money Market Transactions (24 Hours)",
    //   value: moneyMarketTransactions,
    //   unit: "",
    //   background: "cardsec"
    // },
    // {
    //   title: "Total OTC Volume (All Time)",
    //   value: formatterWithTwoDigits.format(totalOTCVolume),
    //   unit: "$",
    //   background: "cardsec"
    // }
  ];

  const insights = [
    { value: totalUsers, title: "Total Users" },
    {
      value: formatterWithTwoDigits.format(context.staked_value),
      title: "Staked GXT",
      unit: "GXT"
    },
    {
      value: formatterWithTwoDigits.format(context.sef_coins_earned),
      title: "SEF Coin Miner",
      unit: "SEF"
    }
  ];

  useEffect(() => {
    if (context.affiliate_id !== "") {
      Axios.get(fetch.url + "brokerage/stats/getUsers", {
        params: {
          affiliate_id: context.affiliate_id
        }
      })
        .then(resp => {
          setTotalUsers(resp.data.all.length);
        })
        .catch(error => {
          setTotalUsers(0);
        });
    }
  }, [context.affiliate_id]);

  useEffect(() => {
    if (context.withdraw) {
      let earning = 0;
      context.withdraw.forEach(item => {
        if (item.status === "Approved")
          earning += parseFloat(item.withdrawl_amt);
      });
      setRealizedEarning(earning);
    }
  }, [context.withdraw]);

  useEffect(() => {
    Axios.get(fetch.url + "brokerage/stats/token_txns", {
      params: { email: localStorage.getItem("userEmail") }
    })
      .then(tokenResp => {
        Axios.get(fetch.url + "brokerage/stats/otc_txns", {
          params: { email: localStorage.getItem("userEmail") }
        }).then(otcResp => {
          Axios.get(fetch.url + "brokerage/stats/digital_txns", {
            params: { email: localStorage.getItem("userEmail") }
          }).then(digiResp => {
            setBrokerageVolume( 
              tokenResp.data.revenue.ttr +
                otcResp.data.revenue.ttr +
                digiResp.data.revenue.ttr
            );
            setTotalCryptoAssetBrokerage(digiResp.data.last_total_assets);
            setBrokerageTransactions(
              tokenResp.data.length.all +
                otcResp.data.length.all +
                digiResp.data.length.all
            );
            setMoneyMarketTransactions(digiResp.data.length.latest_txns);
            setTotalOTCVolume(otcResp.data.volume.ttv);
          });
        });
      })
      .catch(e => {
        setBrokerageVolume(0);
      });
    return () => {};
  }, []);

  return (
    <>
      {/* <div className="row justify-content-center">
        {insights.map((item, index) => (
          <div key={index} className="col-md-4">
            <div className="insights-card affiliate-insights">
              <h2>
                {item.value} {item.unit ? <span>{item.unit}</span> : null}
              </h2>
              <label>{item.title}</label>
            </div>
          </div>
        ))}
      </div> */}
      <div className="row justify-content-center">
        {revenues.map((item, index) => (
          <div key={index} className="col-md-3 revenues-card-wrapper">
            <div className={"revenues-card " + item.background}>
              <label className="title" style={{color: "black"}}>{item.title}</label>
              <h4 className="value" style={{color: "black"}}>
                {item.value < 0 ? "Retrieving" : `${item.unit} ${item.value}`}
              </h4>
              {/* <Link to="/statement">
                <label className="converted-value" style={{ color: "white" }}>
                  View Statements
                </label>
              </Link> */}
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default AffiliateCards;
