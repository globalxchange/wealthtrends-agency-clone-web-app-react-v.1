import React from "react";
import AffiliateCards from "./AffiliateCards";

const Controller = ({ isMobile }) => {
  return (
    <div className="insight-container frag-wrapper ">
      <AffiliateCards />
    </div>
  );
};

export default Controller;
