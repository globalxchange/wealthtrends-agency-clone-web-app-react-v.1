import React from "react";
import axios from "axios";
import { message } from "antd";
import fetch from "../../../../fetchurl.js";
import { brokerage } from "../../../contextapi/Contextapi";

class LiquidityBroker extends React.Component {
  static contextType = brokerage;

  state = {
    btc_balance: "",
    account_balance: "",
    btc_reserve_balance: "",
    usd_reserve_balance: "",
    btc_value: "",
    fetchedData: false,
    btc_withdraw_value: "",
    usd_withdraw_value: "",
    changed_status: ""
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    console.log("Email.........", this.context.email);
    let r = await axios.post(fetch.url + "get_user_balances", {
      email: localStorage.getItem("user_account")

    });

    console.log(r.data);

    if (r.data.status) {
      await this.setState({
        btc_balance: r.data.btc_balance,
        account_balance: r.data.usd_balance,
        btc_reserve_balance: r.data.btc_reserve_balance,
        usd_reserve_balance: r.data.usd_reserve_balance
      });

      this.setState({
        fetchedData: true
      });
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.fetchedData !== nextState.fetchedData) {
      return true;
    }
    if (this.state.changed_status !== nextState.changed_status) {
      return true;
    }
    if (this.props.cash_service_changed !== nextProps.cash_service_changed) {
      return true;
    }
    let r = this.props.affiliate_id !== nextProps.affiliate_id;
    if (r) {
      this.fetchData();
      return true;
    } else {
      return false;
    }
  }

  handleBTCAdd = async () => {
    var { btc_balance, btc_value } = this.state;
    var token = localStorage.getItem("token");

    btc_value = Number(parseFloat(btc_value).toFixed(5));
    if (btc_value <= btc_balance) {
      let r = await axios.post(fetch.url + "coin/transfer/otcreserve/deposit", {
        token: token,
        email: localStorage.getItem("user_account"),
        amount: btc_value
      });
      if (r.data.status) {
        await this.setState({
          btc_balance: r.data.btc_balance,
          btc_reserve_balance: r.data.btc_reserve_balance
        });
        this.setState({
          changed_status: Date.now()
        });
      } else {
        alert(r.data.message);
      }
    } else {
      alert("Not Enough Balance");
    }
  };

  handleWithdraw = async (coin, value) => {
    var token = localStorage.getItem("token");
    let r = await axios.post(fetch.url + "coin/transfer/otcreserve/withdraw", {
      token: token,
      email: this.context.email,
      amount: value,
      coin: coin
    });

    if (r.data.status) {
      if (coin === "BTC") {
        await this.setState({
          btc_balance: r.data.btc_balance,
          btc_reserve_balance: r.data.btc_reserve_balance
        });
        this.setState({
          changed_status: Date.now()
        });
      } else {
        await this.setState({
          account_balance: r.data.usd_balance,
          usd_reserve_balance: r.data.usd_reserve_balance
        });
        this.setState({
          changed_status: Date.now()
        });
      }
    } else {
      alert(r.data.message);
    }
  };

  EnableCash = async () => {
    try {
      if (this.context.cash_service) {
        message.destroy();
        message.warning("You have Already Enabled Cash Service!");
        return;
      }
      var token = localStorage.getItem("token");
      let res = await axios.post(fetch.url + "coin/trade/setCashService", {
        email: this.context.email,
        token: token
      });

      if (res.data.status) {
        this.context.updateCashService(res.data.status);
        message.destroy();
        message.success(res.data.message);
      } else {
        message.destroy();
        message.error(res.data.message);
      }
    } catch (e) {
      console.log(e.message);
    }
  };

  render() {
    return (
      <div className="frag-wrapper">
        <h3>OTC Reserve</h3>
        <h4>
          Vault BTC Balance: <b>{this.state.btc_balance}</b>
        </h4>
        <h4>
          Vault Account Balance: <b>{this.state.account_balance}</b>{" "}
        </h4>
        <br />
        <br />
        <h4>
          BTC Liquidity Balance: <b>{this.state.btc_reserve_balance}</b>
        </h4>
        <b>USD Liquidity Balance: {this.state.usd_reserve_balance}</b> &emsp;
        <input
          type="number"
          size="40"
          placeholder="withdraw to Vault"
          onChange={e => {
            this.setState({ usd_withdraw_value: e.target.value });
          }}
        />
        &emsp;
        <button
          onClick={() =>
            this.handleWithdraw("USD", this.state.usd_withdraw_value)
          }
        >
          Withdraw to Vault
        </button>
        <br /> <br />
        <input
          type="number"
          size="40"
          placeholder="Enter to add to BTC Reserve from Vault"
          onChange={e => {
            this.setState({ btc_value: e.target.value });
          }}
        />{" "}
        &emsp;
        <button onClick={this.handleBTCAdd}>Add from Vault</button>
        <br />
        <br />
        <input
          type="number"
          size="40"
          placeholder="Enter to withdraw to Vault from BTC Reserve"
          onChange={e => {
            this.setState({ btc_withdraw_value: e.target.value });
          }}
        />{" "}
        &emsp;
        <button
          onClick={() =>
            this.handleWithdraw("BTC", this.state.btc_withdraw_value)
          }
        >
          Withdraw to Vault
        </button>
        <br />
        <br />
        <h3>Cash Service!</h3>
        {!this.context.cash_service ? (
          <button className="btn btn-primary" onClick={() => this.EnableCash()}>
            Enable Cash
          </button>
        ) : (
          <button
            className="btn btn-secondary"
            onClick={async () => {
              var token = localStorage.getItem("token");
              let res = await axios.post(
                fetch.url + "coin/trade/setCashService",
                {
                  email: this.context.email,
                  disable: true,
                  token: token
                }
              );

              if (res.data.status) {
                this.context.updateCashService(false);
                message.destroy();
                message.success(res.data.message);
              } else {
                message.destroy();
                message.success(res.data.message);
              }
            }}
          >
            Disable Cash
          </button>
        )}
      </div>
    );
  }
}

export default LiquidityBroker;
