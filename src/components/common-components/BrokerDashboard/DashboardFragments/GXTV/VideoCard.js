import React from "react";

const VideoCard = ({ videoData, onClick }) => {
  return (
    <div className="col-md-3">
      <div class="videoItem">
        <a class="thumbnail" onClick={() => onClick(videoData)}>
          <img src={videoData.snippet.thumbnails.medium.url} alt="" />
          <div class="play"></div>
        </a>
        <p class="title">{videoData.snippet.title}</p>
        <p class="author">{videoData.snippet.channelTitle}</p>
      </div>
    </div>
  );
};

export default VideoCard;
