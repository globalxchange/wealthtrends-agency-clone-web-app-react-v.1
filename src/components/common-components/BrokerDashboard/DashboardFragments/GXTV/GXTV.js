import React, { useState } from "react";
import Filter from "../Marketplace/Filter";
import VideoGallery from "./VideoGallery";

const GXTV = ({ isMobile, categories, searchQuery }) => {
  const [activeFilter, setActiveFilter] = useState(categories[0].title);

  const onCategoryChange = category => {
   document.getElementById("scroll-wrapper").scrollTop = 60;
    setActiveFilter(category);
  };

  return (
    <div className="container frag-wrapper">
      <div className="row">
        <Filter
          categories={categories}
          active={activeFilter}
          setActive={onCategoryChange}
          title="Free GXBroker Courses"
          isMobile={isMobile}
        />
        <VideoGallery active={activeFilter} searchQuery={searchQuery} />
      </div>
    </div>
  );
};

export default GXTV;
