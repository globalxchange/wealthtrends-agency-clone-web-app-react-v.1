import React from "react";
import "../NewTransactionHistory/VaultLedgerNav/vault-ledger-mobile.css";
import nextIcon from "../../images/ledger-right-arrow.svg";
import nextDisIcon from "../../images/ledger-right-arrow-disabled.svg";
import prevIcon from "../../images/ledger-left-arrow.svg";
import prevDisIcon from "../../images/ledger-left-arrow-disabled.svg";

const BrokerDashMobileNav = ({ navItems, activeTab, onChange }) => {
  let mobileNav = [];
  navItems.forEach(item => {
    if (item.subMenu) {
      item.subMenu.forEach(sub => {
        mobileNav = [...mobileNav, { title: sub, parent: item.title }];
      });
    } else {
      mobileNav = [...mobileNav, { title: item.title }];
    }
  });

  const pos = mobileNav.indexOf(
    mobileNav.find(item => activeTab.includes(item.title))
  );
  const isEnd = pos >= mobileNav.length - 1;
  const isStart = pos <= 0;

  const handleNext = () => {
    console.log("POS=>", pos, "isEnd=>", isEnd, "MobileNav=>", mobileNav);
    if (!isEnd) {
      if (mobileNav[pos + 1].parent) {
        onChange(`${mobileNav[pos + 1].title},${mobileNav[pos + 1].parent}`);
      } else {
        onChange(mobileNav[pos + 1].title);
      }
    }
  };

  const handlePrev = () => {
    if (!isStart) {
      if (mobileNav[pos - 1].parent) {
        onChange(`${mobileNav[pos - 1].title},${mobileNav[pos - 1].parent}`);
      } else {
        onChange(mobileNav[pos - 1].title);
      }
    }
  };

  return (
    <div className="ledger-mobile-nav">
      <div className="prev-btn-wrapper" onClick={handlePrev}>
        <img src={isStart ? prevDisIcon : prevIcon} alt="" />
      </div>
      <div className="current-tab-wrapper" onClick={handleNext}>
        <label className="current-tab">{activeTab.split(",")[0]}</label>
      </div>
      <div className="next-btn-wrapper" onClick={handleNext}>
        <img src={isEnd ? nextDisIcon : nextIcon} alt="" />
      </div>
    </div>
  );
};

export default BrokerDashMobileNav;
