import React from "react";

const SearchBar = ({ searchQuery, setSearchQuery }) => {
  return (
    <div className="header-wrapper">
      <div className="searchBarMain">
        <i class="fa fa-search" aria-hidden="true"></i>
        <input
          type="text"
          name="header-search"
          value={searchQuery}
          onChange={e => setSearchQuery(e.target.value)}
          className="searchBarInput"
          placeholder="Search, discover, explore..."
        />
      </div>
    </div>
  );
};

export default SearchBar;
