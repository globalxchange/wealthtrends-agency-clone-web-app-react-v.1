import React, { useState, useEffect } from "react";
import { Menu, Button, Icon, Dropdown } from "antd";
import { DatePicker } from "antd";

const AdvancedSearchBar = ({
  searchQuery,
  setSearchQuery,
  categories,
  setSearchCategory,
  searchCategory
}) => {
  const handleMenuClick = e => {
    setSearchCategory(categories.find(item => item.title === e.key));
  };

  const menu = (
    <Menu onClick={handleMenuClick}>
      {categories.map((item, index) => (
        <Menu.Item key={item.title}>{item.title}</Menu.Item>
      ))}
    </Menu>
  );

  return (
    <div className="header-wrapper">
      <div className="searchBarMain">
        <i class="fa fa-search" aria-hidden="true"></i>
        {searchCategory ? (
          searchCategory.inputType === "date" ? (
            <DatePicker
              popupStyle={{ zIndex: "999999" }}
              placeholder={`Input a ${searchCategory.title.toLowerCase()} to search`}
              onChange={(date, dateString) => setSearchQuery(dateString)}
              format={"YYYY/MM/DD"}
            />
          ) : (
            <input
              type="text"
              name="header-search"
              value={searchQuery}
              onChange={e => setSearchQuery(e.target.value)}
              className="searchBarInput"
              placeholder={`Input a ${
                searchCategory.title !== "All"
                  ? searchCategory.title.toLowerCase()
                  : "text"
              } to search`}
            />
          )
        ) : (
          <input
            type="text"
            name="header-search"
            value={searchQuery}
            onChange={e => setSearchQuery(e.target.value)}
            className="searchBarInput"
            placeholder={`Input a ${
              searchCategory ? searchCategory.title.toLowerCase() : "text"
            } to search`}
          />
        )}

        <Dropdown
          overlay={menu}
          trigger={["click"]}
          overlayStyle={{ zIndex: "999999" }}
        >
          <Button>
            {searchCategory ? searchCategory.title : categories[0].title}
            <Icon type="down" />
          </Button>
        </Dropdown>
      </div>
    </div>
  );
};

export default AdvancedSearchBar;
