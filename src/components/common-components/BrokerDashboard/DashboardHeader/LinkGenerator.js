import React, { useState } from "react";
import { Dropdown, Button, Icon, Menu, Typography } from "antd";
import "./link-generator.scss";

const LinkGenerator = ({ isMobile }) => {
  const menu = [
    {
      name: "Atlas App Registration",
      link: `signup.atlasandbeyond.app/${localStorage.getItem("uusseerrname")}`
    },
    // {
    //   name: "GXBroker Dealer",
    //   link: `https://gxbrokerdealer.com/${localStorage.getItem("uusseerrname")}`
    // },
    // {
    //   name: "Bitcoin Bull Run",
    //   link: `https://c1.whatisgx.com/${localStorage.getItem("uusseerrname")}`
    // },
    // {
    //   name: "Bitcoin Breaking Resistence",
    //   link: `https://c2.whatisgx.com/${localStorage.getItem("uusseerrname")}`
    // },
    // {
    //   name: "Introducing GXBroker",
    //   link: `https://c3.whatisgx.com/${localStorage.getItem("uusseerrname")}`
    // },
    // {
    //   name: "Is GX Just Hype?",
    //   link: `https://c4.whatisgx.com/${localStorage.getItem("uusseerrname")}`
    // },
    // {
    //   name: "Master Bitcoin Today",
    //   link: `https://c5.whatisgx.com/${localStorage.getItem("uusseerrname")}`
    // }
  ];

  const [selectedOption, setSelectedOption] = useState(menu[0]);

  const handleMenuClick = e => {
    setSelectedOption(menu[e.key]);
  };

  const menuItems = (
    <Menu onClick={handleMenuClick}>
      {menu.map((item, index) => (
        <Menu.Item key={index}>{item.name}</Menu.Item>
      ))}
    </Menu>
  );

  console.log("Selected Option => ", selectedOption, "Menus => ", menu);

  return (
    <div className="header-wrapper">
      <div className="links-generator-container">
        <div className="links-generator-wrapper">
          <Dropdown overlay={menuItems}>
            <Button>
              {selectedOption.name} <Icon type="down" />
            </Button>
          </Dropdown>
          <div className="link-copy-wrapper">
            <Typography.Paragraph
              style={{ margin: "0px" }}
              copyable={{
                text: selectedOption.link
              }}
            >
              {isMobile
                ? selectedOption.link.length <= 28
                  ? selectedOption.link
                  : `${selectedOption.link.substring(0, 28)}...`
                : selectedOption.link.length <= 50
                ? selectedOption.link
                : `${selectedOption.link.substring(0, 50)}...`}
            </Typography.Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LinkGenerator;
