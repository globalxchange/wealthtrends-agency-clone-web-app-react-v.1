import React, { useState, useContext, useEffect, useRef } from "react";
import SideNavigation from "../FixedNavigation/FixedNavigation";
import "./broker-dashboard.css";
import BrokerDashNav from "./BrokerDashNav";
import { brokerage, Consumer } from "../contextapi/Contextapi";
import Controller from "./DashboardFragments/Controller/Controller";
import SearchBar from "./DashboardHeader/SearchBar";
import Marketplace from "./DashboardFragments/Marketplace/Marketplace";
import GXTV from "./DashboardFragments/GXTV/GXTV";
import { marketplaceCategories, academyCategories } from "./filterCategories";
import VirtualOTCDesk from "./DashboardFragments/VirtualOTCDesk/VirtualOTCDesk";
import VSA from "./DashboardFragments/VSA/VSA";
import AdvancedSearchBar from "./DashboardHeader/AdvancedSearchBar";
import BrokerDashMobileNav from "./BrokerDashMobileNav";
import LiquidityBroker from "./DashboardFragments/LiquidityBroker/LiquidityBroker";
import StatementDetails from "../statement/StatementDetails";
import DealersDetails from "../statement/DealersDetails";
import WithDrawalDetails from "../statement/WithDrawalDetails";
import Stats from "./DashboardFragments/Network/Stats";
import Export from "../statement/Export";
import "./export.css";
import LinkGenerator from "./DashboardHeader/LinkGenerator";
import Calender from "../Calender/Calender";
import Axios from "axios";

const AffiliateProgram = ({ openWithdrawals }) => {
  const [activeNav, setActiveNav] = useState(
    openWithdrawals ? "Withdrawal Ledger,Network" : "Overview"
  );
  const [searchQuery, setSearchQuery] = useState("");
  const [searchCategory, setSearchCategory] = useState(null);
  const [isMobile, setIsMobile] = useState(window.innerWidth <= 500);
  const [navBarHeight, setNavBarHeight] = useState(0);
  const [brokerDetails, setBrokerDetails] = useState();

  const context = useContext(brokerage);

  const headerRef = useRef();

  let header;
  let fragment;

  switch (activeNav) {
    case "Overview":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = <Controller isMobile={isMobile} />;
      break;
    case "AtlasTV":
      header = (
        <SearchBar searchQuery={searchQuery} setSearchQuery={setSearchQuery} />
      );
      fragment = (
        <GXTV
          isMobile={isMobile}
          categories={academyCategories}
          searchQuery={searchQuery}
        />
      );
      break;
    case "Marketing":
      header = (
        <AdvancedSearchBar
          searchQuery={searchQuery}
          setSearchQuery={setSearchQuery}
          setSearchCategory={setSearchCategory}
          searchCategory={searchCategory}
          categories={[
            { title: "All", inputType: "text" },
            { title: "General", inputType: "text" },
            { title: "Markets", inputType: "text" },
            { title: "Network Marketing", inputType: "text" },
            { title: "Toshi Markets", inputType: "text" },
            { title: "GXInstitutional", inputType: "text" }
          ]}
        />
      );
      fragment = (
        <VSA
          searchQuery={searchQuery}
          setSearchQuery={setSearchQuery}
          searchCategory={searchCategory}
        />
      );
      break;
    case "Users,Atlas CRM":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = <Stats tableToShow={"Users"} navBarHeight={navBarHeight} />;
      break;
    case "Customers,Atlas CRM":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = (
        <Stats tableToShow={"Customers"} navBarHeight={navBarHeight} />
      );
      break;
    case "Brokers,Atlas CRM":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = <Stats tableToShow={"Brokers"} navBarHeight={navBarHeight} />;
      break;
    case "Token Transactions,Atlas CRM":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = (
        <Stats tableToShow={"Token Transactions"} navBarHeight={navBarHeight} />
      );
      break;
    case "OTC Transactions,Atlas CRM":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = (
        <Stats tableToShow={"OTC Transactions"} navBarHeight={navBarHeight} />
      );
      break;
    case "Digital Transactions,Atlas CRM":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = (
        <Stats
          tableToShow={"Digital Transactions"}
          navBarHeight={navBarHeight}
        />
      );
      break;
    case "Credit Card Transactions,Atlas CRM":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = (
        <Stats
          tableToShow={"Credit Card Transactions"}
          navBarHeight={navBarHeight}
        />
      );
      break;
    case "Withdrawal Ledger,Atlas CRM":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = (
        <Stats tableToShow={"Withdrawal Ledger"} navBarHeight={navBarHeight} />
      );
      break;
    case "Marketplace":
      header = (
        <SearchBar searchQuery={searchQuery} setSearchQuery={setSearchQuery} />
      );
      fragment = (
        <Marketplace
          isMobile={isMobile}
          categories={marketplaceCategories}
          searchQuery={searchQuery}
          navBarHeight={navBarHeight}
        />
      );
      break;

    case "Level 1 - Virtual OTC Desk,OTCBroker":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = <VirtualOTCDesk />;
      break;
    case "Level 2 - Liquidity Broker,OTCBroker":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = <LiquidityBroker />;
      break;
    case "GXBroker Statements,Statements":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = (
        <Consumer>
          {context => (
            <div className="frag-wrapper">
              <StatementDetails
                logs={context.logs}
                spinner={context.spinner}
                affiliate_id={context.affiliate_id}
                brokers={context.brokers}
                get_Week_number={context.get_Week_number}
                tbal={context.tbal}
                balance={context.balance}
                limit={20}
                email={context.email}
                eth_addr={context.eth_addr}
              />
            </div>
          )}
        </Consumer>
      );
      break;
    case "GXBroker Dealer Statements,Statements":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = (
        <Consumer>
          {context => (
            <div className="frag-wrapper">
              <DealersDetails
                logs={context.logs}
                affiliate_id={context.affiliate_id}
                brokers={context.brokers}
                get_Week_number={context.get_Week_number}
                tbal={context.tbal}
                balance={context.balance}
                limit={20}
                email={context.email}
                eth_addr={context.eth_addr}
              />
            </div>
          )}
        </Consumer>
      );
      break;
    case "Withdrawals,Statements":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = (
        <Consumer>
          {context => (
            <>
              <WithDrawalDetails context={context} />
              <div className="export-wrapper">
                <Export
                  logs={context.logs}
                  affiliate_id={context.affiliate_id}
                  brokers={context.brokers}
                  get_Week_number={context.get_Week_number}
                  name={context.broker_name}
                  email={context.email}
                />
              </div>
            </>
          )}
        </Consumer>
      );
      break;
    case "Calender":
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = <Calender navBarHeight={navBarHeight} />;
      break;
    default:
      header = <LinkGenerator isMobile={isMobile} />;
      fragment = <Controller />;
  }

  useEffect(() => {
    setSearchQuery("");
    return () => {};
  }, [activeNav, searchCategory]);

  useEffect(() => {
    setSearchCategory(null);
    return () => {};
  }, [activeNav]);

  useEffect(() => {
    screenResizeListener();
    getBrokerDetails();
    window.addEventListener("resize", screenResizeListener);
    return () => {
      window.removeEventListener("resize", screenResizeListener);
    };
  }, []);

  const screenResizeListener = () => {
    setIsMobile(window.innerWidth <= 500);
    setNavBarHeight(headerRef.current.clientHeight);
  };

  const getBrokerDetails = () => {
    Axios.get(
      `https://comms.globalxchange.com/coin/vault/user/license/get?email=${localStorage.getItem(
        "user_account"
      )}`
    ).then(resp => {
      if (resp.data.licenses[0]) {
        setBrokerDetails(resp.data.licenses[0]);
      }
    });
  };

  const navLinks = [
    { title: "Overview", icon: "icon-controls", link: "#", enabled: true },
    {
      title: "AtlasTV",
      icon: "icon-academy",
      link: "#",
      enabled: brokerDetails
        ? brokerDetails.status === "active" || brokerDetails.status
        : false
    },
    // {
    //   title: "Marketing",
    //   icon: "icon-vsa",
    //   link: "#",
    //   enabled: brokerDetails
    //     ? brokerDetails.status === "active" || brokerDetails.status
    //     : false
    // },
    {
      title: "Atlas CRM",
      icon: "fas fa-network-wired",
      link: "#",
      subMenu: [
        "Users",
        "Customers",
        "Brokers",
        "Token Transactions",
        "OTC Transactions",
        "Digital Transactions",
        "Withdrawal Ledger",
        "Credit Card Transactions"
      ],
      enabled: brokerDetails
        ? brokerDetails.status === "active" || brokerDetails.status
        : false
    },
    // {
    //   title: "Marketplace",
    //   icon: "icon-marketplace",
    //   link: "#",
    //   enabled: true
    // },
    // {
    //   title: "OTCBroker",
    //   icon: "icon-broker",
    //   link: "#",
    //   subMenu: [
    //     "Level 1 - Virtual OTC Desk",
    //     "Level 2 - Liquidity Broker",
    //     "Level 3 - CashBroker™"
    //   ],
    //   enabled: brokerDetails
    //     ? brokerDetails.status === "active" || brokerDetails.status
    //     : false
    // },
    // {
    //   title: "Calender",
    //   icon: "fa fa-calendar-check-o",
    //   link: "#",
    //   enabled: brokerDetails
    //     ? brokerDetails.status === "active" || brokerDetails.status
    //     : false
    // }
    // {
    //   title: "Statements",
    //   icon: "fas fa-clipboard",
    //   link: "#",
    //   subMenu: [
    //     "GXBroker Statements",
    //     "GXBroker Dealer Statements",
    //     "Withdrawals"
    //   ]
    // }
    // {
    //   title: "Settings",
    //   icon: "icon-settings",
    //   link: "#",
    //   subMenu: ["BrokerID", "Manage Subscriptions"]
    // }
  ];

  return (
    <div className="FixedSideBarWraperSection">
      <SideNavigation />
      <div
        id="scroll-wrapper"
        className="rightSidePartWraperSection rightSidePartWraperSectionSecond stats-container"
      >
        <div
          className="affiliate-wrapper"
          style={{ paddingBottom: "calc(paddingBottom-0.2rem)" }}
        >
          <div className="fixed-nav" ref={headerRef}>
            <h1 className="text-center affiliate-broker">
              {context.broker_name !== "" ? `${context.broker_name}'s` : ""}{" "}
              Atlas Affiliate Portal
            </h1>
            {header}
            {isMobile ? (
              <BrokerDashMobileNav
                activeTab={activeNav}
                onChange={setActiveNav}
                navItems={navLinks}
              />
            ) : (
              <BrokerDashNav
                active={activeNav}
                setActive={setActiveNav}
                navLinks={navLinks}
              />
            )}
          </div>
          <div
            className="fragment-container"
            style={{ height: `calc(100vh - ${navBarHeight}px)` }}
          >
            {fragment}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AffiliateProgram;
