export const marketplaceCategories = [
  { title: "Broker", count: 1 },
  { title: "Personal", count: 0 },
  { title: "Business", count: 0 },
  { title: "Institutional", count: 0 },
  { title: "Sales", count: 0 },
  { title: "Marketing", count: 0 }
];

export const academyCategories = [
  { title: "All", count: 1 },
  {
    title: "Get Started",
    count: 1
  },

  // { title: "Crypto 101", count: 4 },
  // {
  //   title: "Toshi Markets Crypto\nBeginner Program",
  //   count: 8
  // },
  // { title: "Road To $5000", count: 0 },
  // { title: "Master GX", count: 0 },
  // { title: "Crypto Influence", count: 0 }
];
