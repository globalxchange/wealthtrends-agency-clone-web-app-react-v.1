import React, { useContext } from 'react'
import './asset-cell.style.scss'
import { Agency } from '../Context/Context';
export default function AssetClassCell({ icon, period, amount, currency, coin }) {
    const agency = useContext(Agency);
    const { valueFormatter, coinInitials} = agency;

    return (
        <div className="d-flex flex pl-3 pr-3 flex-wrap align-content-center w-100 h-100">
            <div className="price-text d-flex align-items-center m-0 w-100 justify-content-between">
                <img alt="" src={icon} />{valueFormatter(amount, coin)}</div>
            <p style={{fontSize: "70%", whiteSpace: "noWrap", overflow: "hidden"}} className="w-100 currency-text text-right m-0">{currency + period}</p>
        </div>
    )
}
