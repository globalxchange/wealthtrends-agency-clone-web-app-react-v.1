import React from 'react'
import './skeleton-main.scss'
export default function SkeletonMain() {
    return (
        <div className="skeleton-main">
            <div className="skeleton-header">
                <div className="skeleton-header-left">
                    <button/>

                </div>
                <div className="skeleton-header-right">
                    {
                        [155,156,157,158,159].map(item =><button key ={item}></button>)
                    }

                </div>

            </div>
            <div className="skeleton-asset-list">
                <div className="skeleton-asset-list-left">
                    <h4></h4>
                    <span></span>

                </div>
                <div className="skeleton-asset-list-right">
                    {
                        [160,161,162,163,175].map(item =><div key ={item}/>)
                    }

                </div>

            </div>
            <div className="skeleton-table-section">
                <div className="skeleton-table-section-header"/>
                <div className="skeleton-table-section-body position-relative">
                    {
                        [176,177,178].map(x =><div key ={x}/>)
                    }

                </div>

            </div>
            
        </div>
    )
}
