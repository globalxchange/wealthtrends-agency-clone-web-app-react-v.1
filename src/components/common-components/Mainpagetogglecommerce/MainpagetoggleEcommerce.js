import React,{useContext} from 'react'
import  Ecommerce from '../E-commerce/EcommerceLandingPage'
import { brokerage } from "../../Brokercontextapi/Contextapi";
import EcommCarcuole from '../ProfolioPageEcommerce/Protfolio'
import Charecc from '../Protfoliochart/Protofoliochart'
export default function MainpagetoggleEcommerce() {
    const agency = useContext(brokerage);
    const {SwithEcommer} = agency;


    const selectMainComponent = () =>{
        
        switch(SwithEcommer){
            case "E-commerce": return   <Ecommerce/>;
             case "EcommerceCaroule": return <EcommCarcuole />
             case "chart": return <Charecc/>;
            
            default: return   <Ecommerce/>; 
        }
    }

  return (
    <div>
         {selectMainComponent()}
    </div>
  )
}
