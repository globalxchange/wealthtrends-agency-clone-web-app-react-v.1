import React from 'react'
import { useHistory } from "react-router-dom";

export default function GoBack() {
    let history = useHistory();
    React.useEffect(() => {
        history.push('/')
        
    }, [])

    return <></>
}
