import React, { useState, useEffect, useContext } from 'react';
import './asset-list.style.scss';
import nextId from 'react-id-generator';
import Images from '../../../assets/a-exporter'
import AssetListRow from './AssetListRow';
import Constant from '../../../json/constant'
import { Agency } from '../../Context/Context';
import LoadingAnimation from '../../../lotties/LoadingAnimation';
import NextSVG from '../assetsSection/NextSVG';

const AssetList = React.memo(({ setFullScreen, myId,remove, fullScreen, active, list }) => {
  const agency = useContext(Agency);
  const { valueFormatter, setSecondGate, conversionConfig, setCollapseSidebar,
     setUpdatePortfolio, setUserChatEnable, currentApp, decimalValidation,
      handleAnalytics, setDirectCoinOverview, setAssetClassSelected,
       setScrollValue, setDifferentiator } = agency
  const [searchTerm, setSearchTerm] = useState("");
  const [localList, setLocalList] = useState([]);
  const [searchActive, setSearchActive] = useState(false);
  const [special, setSpecial] = useState(false)
  useEffect(() => {
    let temp = [];
    if (true) {
      temp = list.filter(obj => { return obj.coinName.toLowerCase().startsWith(searchTerm.toLowerCase()) })
    } else {
      temp = list.filter(obj => { return obj.coin_metdata.coinName.toLowerCase().startsWith(searchTerm.toLowerCase()) })
    }
    setLocalList(temp)


  }, [searchTerm]);

  React.useEffect(() => {
    if (localStorage.getItem("userEmail").toString().toLowerCase() === "barbian20@gmail.com") {
      setSpecial(true);
    }

  }, [])
  React.useEffect(() => {
    console.log("")
  }, [list])

  const getClassName = () => {
    switch (myId) {
      case 'id2': return 'asset-list-header-crypto';
      case 'id1': return 'asset-list-header-fiat';
      case 'id3': return 'asset-list-header-assets';
      case 'id4': return 'asset-list-header-crypto';
      default: return 'asset-list-header'
    }
  }
  const getHeaderList = () => {
    switch (myId) {
      case 'id2': return Constant.assetListHeaderCrypto;
      case 'id1': return Constant.assetListHeaderCrypto;
      case 'id3': return Constant.assetListHeaderAsset;
      case 'id4': return Constant.assetListHeaderCrypto;
      default: return Constant.assetListHeader;
    }

  }
  const handleAssetClick = (data, type, len, i) => {
    if (!active)
      return;
      setDirectCoinOverview({status: true, coin: data, type: type, len:len, i:i});
      setUserChatEnable(true);
      setCollapseSidebar(true);
    // switch (type) {
    //   case "fiat": handleAnalytics(""); setAssetSelected(Constant.navbarList[1]); afterSomeTime(data, i); setSecondGate(true); console.log(data); break;
    //   case "crypto": handleAnalytics(""); setAssetSelected(Constant.navbarList[0]); afterSomeTime(data, i); setSecondGate(true); console.log(data); break;
    //   case "asset": handleAnalytics(""); setAssetSelected(Constant.navbarList[2]); afterSomeTime(data, i); setSecondGate(true); console.log(data); break;
    // }
  }

  const afterSomeTime = (data, i) => {
    let a = setTimeout(() => {
      setAssetClassSelected(data);
      if (i > 1) {
        setScrollValue(i)
      }
      clearTimeout(a);

    }, 300);
  }
  const getAssetRow = (data, len, i) => {
    switch (myId) {
      case 'id1': return <AssetListRow
        myId={myId}
        active={active}
        onClick={() => handleAssetClick(data, "fiat", len, i)}
        image={data.coinImage}
        asset={data.coinName}
        first={valueFormatter(data.coinValue, data.coinSymbol)}
        second={valueFormatter(!data.withdrawal_balance ? data.coinValue : data.withdrawal_balance, data.coinSymbol)}
        third={conversionConfig.enable ? valueFormatter(data.price.USD * conversionConfig.rate, conversionConfig.coin) + ` ${conversionConfig.coin}` : valueFormatter(data.price.USD, "USD")}
        fourth={conversionConfig.enable ? valueFormatter(data.coinValueUSD * conversionConfig.rate, conversionConfig.coin) + ` ${conversionConfig.coin}` : valueFormatter(data.coinValueUSD, "USD")}
      />;
      case 'id2': return <AssetListRow
        myId={myId}
        active={active}
        image={data.coinImage}
        asset={data.coinName}
        onClick={() => handleAssetClick(data, "crypto", len, i)}
        first={valueFormatter(data.coinValue, data.coinSymbol)}
        second={valueFormatter(!data.withdrawal_balance ? data.coinValue : data.withdrawal_balance, data.coinSymbol)}
        third={conversionConfig.enable ? valueFormatter(data.price.USD * conversionConfig.rate, conversionConfig.coin) + ` ${conversionConfig.coin}` : special && data.coinSymbol === "USDT" ? valueFormatter(data.price.USD * 1.03, "USD") : valueFormatter(data.price.USD, "USD")}
        fourth={conversionConfig.enable ? valueFormatter(data.coinValueUSD * conversionConfig.rate, conversionConfig.coin) + ` ${conversionConfig.coin}` : special && data.coinSymbol === "USDT" ? valueFormatter(data.coinValueUSD * 1.03, "USD") : valueFormatter(data.coinValueUSD, "USD")}
      />;
      case 'id3': return <AssetListRow
        myId={myId}
        active={active}
        onClick={() => handleAssetClick(data, "asset", len, i)}
        image={data.coinImage}
        asset={data.coinName}
        first={`${parseFloat(data.interest_rate).toFixed(4)}%`}
        second={valueFormatter(!data.bal ? 0 : data.bal, data.coin)}
        third={decimalValidation(parseFloat(data.interest_rate * data.bal) / 100, data.coin) ? parseFloat(parseFloat(data.interest_rate * data.bal) / 100).toFixed(6) : valueFormatter(parseFloat(data.interest_rate * data.bal) / 100, data.coin)}
        fourth={decimalValidation(!data.balance ? 0 : data.balance, data.coin) ? parseFloat(data.balance).toFixed(6) : valueFormatter(!data.balance ? 0 : data.balance, data.coin)}
      />;
      case "id4": return <AssetListRow
      myId={myId}
      active={active}
      onClick={() => handleAssetClick(data, "bond", len, i)}
      image={data.coinImage}
      asset={data.coinName}
      first={valueFormatter(data.balance, data.coinSymbol)}
      second={valueFormatter(data.remaining_earning_power, data.coinSymbol)}
      
      />

    }

  }
  const handleChange = (e) => {
    setSearchTerm(e.target.value)
  }
  const titleObj = {
    id2: `Cryptocurrencies`,
    id1: `Fiat Currencies`,
    id3: `Interest Rates`,
    id4: `Investment Products `,
  }
  return (
    <div className="asset-list">
      <div className={`asset-list-main-header`}>
        <h4>{titleObj[myId]}</h4>
        <span>Supported by {currentApp.app_name}</span>

      </div>
      <div className={`${getClassName()} position-relative`}>
        {
          searchActive ?
            <input autoFocus={true} onChange={handleChange} placeholder="Search Any Asset" />
            :
            getHeaderList().map((obj, num) => <h6 key={obj.keyId}>{obj.name}{
               num === 3 && ["id1", "id2"].includes(myId)
                ?
                <div className="portfolio-wrapper">
                  {/* <img onClick={() => { setCollapseSidebar(true); setUserChatEnable(true); setUpdatePortfolio(true) }} src={Images.calculatorPortfolio} /> */}
                  <span onClick={() => { setCollapseSidebar(true);setDifferentiator("profile"); setUserChatEnable(true); setUpdatePortfolio(true) }}>
                    <NextSVG height="20px" width="19px" type ="portfolio"  />
                  </span>

                  <div>
                    <h4><img src={Images.portfolioAI} />Portfolio.ai</h4>
                    <p><b>Click</b> To Get Price Calculations</p>
                  </div>
                </div>
                : ''
            }
            </h6>)
        }
        <span>
          <img onClick={() => setSearchActive(!searchActive)} alt="" src={Images.searchWhite} />
          <img onClick={() => setFullScreen(!fullScreen)} alt="" src={Images.expand} /></span>
      </div>
      <div onScroll={()=>setSearchTerm('')}  className="asset-list-body">
        {
          !localList.length ?
            <div className="w-100 h-100 d-flex justify-content-center align-items-center flex-column">
              <LoadingAnimation type="no-data" size={{ height: 200, width: 200 }} />
              <h6>No Coins Available</h6>
            </div>
            :
            localList.map(
              (obj, i) => <React.Fragment key={obj.coinImage}>{getAssetRow(obj, localList.length, i + 1)}</React.Fragment>
            )
        }
      </div>
    </div>
  )
})
export default AssetList;