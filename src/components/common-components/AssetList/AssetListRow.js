import React, { useRef, useEffect, useState, useContext } from 'react'
import { Agency } from '../../Context/Context'
const AssetListRow = React.memo(({ myId, onClick, active, image, asset, first, second, third, fourth }) => {
  const agency = useContext(Agency)
  const { theme, valueFormatter } = agency;
  const getClassName = () => {
    switch (myId) {
      case 'id1': return 'asset-list-row-crypto';
      case 'id2': return 'asset-list-row-fiat';
      case 'id3': return 'asset-list-row-assets';
      case 'id4': return 'asset-list-row-crypto';
      default: return 'asset-list-header'
    }
  }

  return (
    <div onClick={onClick} className={`${getClassName()} ${active ? "implement-hover" : ""}`}>
      <div><img alt="" src={image} />{asset}</div>
      <div>{first}</div>
      <div>{second}</div>
      <div>{third}</div>
      <div>{fourth}</div>
    </div>
  )
}
)
export default AssetListRow;