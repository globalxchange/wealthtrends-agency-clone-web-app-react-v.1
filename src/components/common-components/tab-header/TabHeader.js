import React, { useContext, useEffect } from 'react'
import './tab-header.scss'
import { Agency } from '../../Context/Context'
export default function TabHeader({ icon, onClick, title }) {
    const agency = useContext(Agency)
    const { analyticsType,assetClassSelected, handleAnalytics } = agency;
    const [string, setString] = React.useState('')

    const setTitle= () =>{
        let str = analyticsType.split(' ');
        console.log("header title", str[0]+' ' + assetClassSelected?.coinSymbol);
        setString(str[0] + ' '+ assetClassSelected.coinSymbol)
    }
    useEffect(()=>{
        setTitle()
    },[title, assetClassSelected])
    return (
        <div
            // onClick={()=>handleAnalytics(title)} 
            className={`tab-header-item`}>
            {string}
            <span
            onClick={()=>handleAnalytics('')} 
            
            >X</span>
        </div>
    )
}
