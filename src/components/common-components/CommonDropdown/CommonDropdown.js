import React from 'react'
import Images from '../../../assets/a-exporter'
import './common-dropdown.style.scss'
export default function CommonDropdown({ list = [1,2 ], border = {}, dropdown = false, setDropdown, children, style = { height: 6, width: "25vh", z: 2 } }) {
    return (
        <div style={{ height: `${style.height}vh`, width: style.width, zIndex: style.z }} className="common-dropdown">
            <div style={{ height: `${style.height}vh`, ...border }} className={`cd-selected ${dropdown ? "remove-order" : ""}`}>
                {children}
                <img onClick={() => setDropdown(!dropdown)} className="dropdown-image" src={Images.triangle} />

            </div>
            <div style={list.length <= 5 ?
                { height: `${style.height * list.length}vh` }
                :
                { height: `${style.height * 5}vh` }
            } className={dropdown ? "cd-dropdown-list" : "d-none"}>
                {
                    list.map(obj => <h6 style={{height: `${style.height}vh`}}>
                        <img src={obj.icon} height="15px" />
                        {
                            obj?.name
                        }
                    </h6>
                    )
                }
            </div>
        </div>
    )
}
