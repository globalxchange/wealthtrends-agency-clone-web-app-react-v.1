import React,{useState} from 'react'
import './switch.style.scss'
export default function SwitchComponent({trigger, onClick}) {
    const [on, setOn] = useState(false)
    return (
        <div onClick ={()=>onClick()} className={`switch-main ${trigger?"on":"off"}`}>
            <div className="switch-ball"> </div>
        </div>
    )
}
