

export const fliterChart=[
   {
            name:"Positions",
            buyprice:5000,
            sellprice:5000,
            chart:["1000","3009","5000"],
            catlist:[
{
    catname1:"detail1",  
    cat1:1000,
    chart:["5000","7009"],
    catinisde:[
        {
            name:"buy",  
            buyprice:5000,
        },

        { name:"sale",  
        buyprice:7000,  
        }
    ]
   
},
{
    catname1:"detail2",  
    cat1:3000, 
    chart:["5000","9009"],
    catinisde:[
        {
            name:"buy",  
            buyprice:5000,
        },

        { name:"sale",  
        buyprice:9000,  
        }
    ]
},
{
    catname1:"detail1",  
    cat1:5000,
    chart:["2000","7009"],
    catinisde:[
        {
            name:"buy",  
            buyprice:2000,
        },

        { name:"sale",  
        buyprice:7000,  
        }
    ]
},

            ]
        

   },

   {
    name:"Volume",
    buyprice:5000,
    sellprice:5000,
    chart:["3000","5009","6000"],
   catlist:[
    {
        catname1:"detail1", 
        cat1:3000,  
        chart:["5000","709"],
        catinisde:[
            {
                name:"buy",  
                buyprice:5000,
            },
    
            { name:"sale",  
            buyprice:700,  
            }
        ]

    },
    {
        catname1:"detail2",
        cat1:5009,
        chart:["8000","7009"],
        catinisde:[
            {
                name:"buy",  
                buyprice:8000,
            },
    
            { name:"sale",  
            buyprice:7000,  
            }
        ]
    },
    {
        catname1:"detail3",
        cat1:6000,  
        chart:["6000","9009"],
        catinisde:[
            {
                name:"buy",  
                buyprice:6000,
            },
    
            { name:"sale",  
            buyprice:9000,  
            }
        ]
    }
   ]  
   },


   {
    name:"Drawdown",
    buyprice:3000,
    sellprice:5000,

    chart:["9009","5000"],
    catlist:[
        {
            catname1:"detail1",  
            cat1:9000,
            chart:["9000","7009"],
            catinisde:[
                {
                    name:"buy",  
                    buyprice:9000,
                },
        
                { name:"sale",  
                buyprice:7000,  
                }
            ]
        },
        {
            catname1:"detail2",  
            cat1:5000,
            chart:["4000","7009"],
            catinisde:[
                {
                    name:"buy",  
                    buyprice:4500,
                },
        
                { name:"sale",  
                buyprice:7000,  
                }
            ]
        }
                    ] 
   },
    
   {
    name:"P/L",

    chart:["9000","8009"],
    buyprice:9000,
    sellprice:5000,
    
    catlist:[
        {
            catname1:"detail1",  
            cat1:1000,
            chart:["5000","7009"],
            catinisde:[
                {
                    name:"buy",  
                    buyprice:5000,
                },
        
                { name:"sale",  
                buyprice:7000,  
                }
            ]
        },
        {
            catname1:"detail2",  
            cat1:3000, 
            chart:["5000","7009"],
            catinisde:[
                {
                    name:"buy",  
                    buyprice:5000,
                },
        
                { name:"sale",  
                buyprice:6000,  
                }
            ]
        }
                    ],  
   },
       

    
   
]
export const chartfliterApi=[
    {
        name:"All",
    },
    {
        name:"1W",
    },
    {
        name:"1M"
    },
    {
        name:"3M"
    },
    {
        name:"YTD"
    },
    {
        name:"1Y"
    }

]




export const catgo=[
    {
        name:"All"
    },
    {
        name:"Alerts"
    },
    {
        name:"Mentorship",
    },
    {
        name:"Copy Trader",
    },
    {
        name:"Exchanges",
    }
]