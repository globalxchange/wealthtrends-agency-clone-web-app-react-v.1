import React,{useContext,useEffect,useState,useRef} from 'react'


import { createChart, CrosshairMode } from "lightweight-charts";


import {chartfliterApi} from "./MakertApi"

 import axios from "axios";

 import Chart from 'kaktana-react-lightweight-charts'

 import { brokerage } from "../../Brokercontextapi/Contextapi";

 
  const Kat=(props)=> {
	  const [data,setDat]=useState([])
	 
	  const [ selectdays,setselectdays]=useState("All")
  useEffect(() => {
	
		axios.get("https://comms.globalxchange.com/coin/vault/earnings/analytics/graph/get/all?email=shorupan@gmail.com"
		).then(resp => {

		  if (resp.data) {

		fetchcustomdataaffiliate(resp.data.total)
		  }
		});

	  
	  return () => {

	  }
  }, [])


  let options={
	
	layout: {
		backgroundColor: "#FFF",
	
	  },

	  grid: {
		vertLines: {
		  visible: false,
	 
		},
		horzLines: {
		  visible: false,
	 
		}
	  },
	  priceScale: {
		borderVisible: false,
		position: 'left',
		color:'red',
	  
	  },
	  timeScale: {
		borderVisible: false,
		borderColor: "red",
		

	  },
	  crosshair: {
		mode: CrosshairMode.Normal
	  },
	  areaSeries  :{
		topColor: '#18254269',
		bottomColor: '#1825421a',
		lineColor: 'black',
	
	},
	// crosshair: {
    //     vertLine: {
    //         color: '#6A5ACD',
    //         width: 0.5,
    //         style: 1,
    //         visible: true,
    //         labelVisible: false,
    //     },
    //     horzLine: {
    //         color: '#6A5ACD',
    //         width: 0.5,
    //         style: 0,
    //         visible: true,
    //         labelVisible: true,
    //     },
    //     mode: 1,
	// },

	 
  }
  
  const fetchcustomdataaffiliate=async(data)=>
  {
  
	
  var launchOptimistic = data.map(function(elem,i) {
	var str = elem.date;
	var res = str.split(", " );
	
  
	return {
	 
	  time:res[0],
	  value: elem.totalPL
	} 
	 
  
	})


		setDat(launchOptimistic)
	
	
		 

  
   
  }

	const {
	
		reploadpage,
		SetDatachartcollect,
	  } = useContext(brokerage);

	
	  
	const  areaSeries = [{
		data,},
	  ]

	// areaSeries.applyOptions({
	// 	lineColor: 'rgba(255, 44, 128, 1)',
	// 	lineWidth: 1,
	// });


	
	
	const add={
		topColor: 'pink',
		bottomColor: 'rgba(21, 146, 230, 0)',
		lineColor: 'red',
		lineStyle: 0,
		lineWidth: 3,
	}

	
	const DaysFliters= async(item)=>
	{
		setselectdays(item)
		if(item==="All")
		{
		  axios.get("https://comms.globalxchange.com/coin/vault/earnings/analytics/graph/get/all?email=shorupan@gmail.com"
		  ).then(resp => {
			if (resp.data) {
  
		  fetchcustomdataaffiliate(resp.data.total)
			}
		  });
		}
	  if(item==="1W")
	  {
		axios.get("https://comms.globalxchange.com/coin/vault/earnings/analytics/graph/get/all?email=shorupan@gmail.com&days=7"
		).then(resp => {
		  if (resp.data) {

		fetchcustomdataaffiliate(resp.data.total)
		  }
		});
	  }
	   if(item==="1M")
	  {
		axios.get("https://comms.globalxchange.com/coin/vault/earnings/analytics/graph/get/all?email=shorupan@gmail.com&days=30"
		).then(resp => {
		  if (resp.data) {

		fetchcustomdataaffiliate(resp.data.total)
		  }
		});
	  }
	  if(item==="3M")
	  {
		axios.get("https://comms.globalxchange.com/coin/vault/earnings/analytics/graph/get/all?email=shorupan@gmail.com&days=90"
		).then(resp => {
		  if (resp.data) {

		fetchcustomdataaffiliate(resp.data.total)
		  }
		});
	  }
	  if(item==="YTD" || item==="1Y" )
	  {
		axios.get("https://comms.globalxchange.com/coin/vault/earnings/analytics/graph/get/all?email=shorupan@gmail.com&days=365"
		).then(resp => {
		  if (resp.data) {

		fetchcustomdataaffiliate(resp.data.total)
		  }
		});
	  }
	 
	}
	console.log("xxxxies",areaSeries)
  return (
 <>
 <div className="linechartsection">
 <Chart options={options}
 

 darkTheme="true"  addAreaSeries={add} areaSeries={areaSeries} autoWidth autoHeight />
 </div>
 <div className="dayflitersectiondiv">
				{
					chartfliterApi.map(item=>
						{
							return(
<p className={item.name===selectdays?"trueday":"flaseday"} onClick={() =>
									// props.history.push(`/MarketBuy`),
									 DaysFliters(item.name)

									// DaysFliters(item.name)
								}>{item.name}</p>
							)
						})
				}
			</div>
<div>
	
</div>
  </>
  )
}
export default Kat;