import React, { useContext } from 'react'
import './contact-card.style.scss'
import Images from '../../../assets/a-exporter'
import { Agency } from '../../Context/Context'
function ContactCard({ onClick, selected, image, name }) {
    const agency = useContext(Agency);
    const { theme } = agency;


    return (
        <div onClick={onClick} className={`contact-card ${selected ? 'selected-card' : ''}`}>
            <span>
                <img onError={e => e.target.src = Images.agency} src={image === null ? theme === "light" ? Images.agencyDark : Images.agency : image} />
                <span className="dot" />
            </span>
            <div>{name}</div>
        </div>
    )
}
export default ContactCard;