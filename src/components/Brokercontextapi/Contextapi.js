import React, { Component } from "react";
import fetch from "../fetchurl";
import axios from "axios";
import moment from "moment";
//import { CognitoUser } from "amazon-cognito-identity-js";
import userPool from "../common-components/loginform/config";
// import { TimeSeries } from "pondjs";
//import { Redirect } from "react-router-dom";
import {v4 as uuidv4} from "uuid";
import {atlasAll,BrokerCmrall,AtlasCmrall,AtlasTVall,atlasshareAll,Altasdrop,seletctcmr} from '../common-components/BrokerSendpage/AtlasdropApi'
// import {StoreDropdownlist,ControllerDropdownlist,ExchangeDropdownlist,AffiliateDropdownlist,alldatalist,} from '../VSA/Vsaapi'
export const brokerage = React.createContext();

Date.prototype.getWeek = function() {
  var date = new Date(this.getTime());
  date.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  date.setDate(date.getDate() + 3 - ((date.getDay() + 6) % 7));
  // January 4 is always in week 1.
  var week1 = new Date(date.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return (
    1 +
    Math.round(
      ((date.getTime() - week1.getTime()) / 86400000 -
        3 +
        ((week1.getDay() + 6) % 7)) /
        7
    )
  );
};

export default class Contextapi extends Component {
  state = {
    CrmData:[],
    dropdownlistAtlass:[],
    atlasnavigationdata:atlasshareAll,
    SwithEcommer:"E-commerce",
    mainatlastab:"Share",
    SelectedAtlasDropname:"All Data",
    setdropshowtable:false,
    atlasNavshow:false,
    CrmDataName:"",
    atlasdrpclick:"Search Portal....",
    atlastoggle:false,
    atlasnamecrm:"",
    atlasbuttonclick:false,
    atlastab:"",
    SelectedDropname:'All Data',
    answerdropname:'',
    vsaheightatlas:'',
    ControllerDashboard:true,
    BusinessControllerDashboard:false,
    // SelectedDropmenu:alldatalist,
    bussinesszip:'',
    vsatab:'Controller',
    vsatabsub:'Overview',
    bussinessAddress:'',
    bussinessprovince:'',
    storelife:'',
    bussinessname:'',
    storeMembership:'',
    productcurrency:0,
    productdescription:'',
    producttag:'',
    storestep1:true,
    storestep2:false,
    storestep3:false,
    storestep4:false,
    planchooseId:'',
    oneTime:true,
    storeansid:'',
    storeans:'',
    planchooseName:'',
    scountry: '', sregion: 'Region' ,
    seelingmodename:'',
    storeNamestatus:false,
    storeName:'',
    seelingmodeid:'',
    pstep1:true,
    pstep2:false,
    pstep3:false,
    pstep4:false,
    seelingplatfrom:'',
    altasNavsearch:"",
    horizontal:0,
    horizontals:0,
    imagePreviewUrl:'',
    imagePreviewUrlStore:'',
    imagestatus:false,
    isLoginModalOpen:false,
    isRestLoginModalOpen:false,
    showonlydata: false,
    cust:[],
    SubscriptionData:false,
    currency: [],
    userlink:
      "https://token.globalxchange.com/checkout/d8ff8394c1f6a61dba15833482cf82db1a4f924d",
    earningsheader: "All Time Earnings",
    earningvalue: [
      {
        earningname: "Direct Transactional Volume",
        earningvalue: "0",
        dollar: "US Dollar",
        id: 1,
        plus: "+"
      },
      {
        earningname: "Indirect Transactional Volume",
        earningvalue: "0",
        dollar: "US Dollar",
        id: 2,
        plus: "="
      },
      {
        earningname: "Total Transactional Volume",
        earningvalue: "0",
        dollar: "US Dollar",
        id: 3,
        plus: ""
      },
      {
        earningname: "Direct Transactional Revenue",
        earningvalue: "0",
        dollar: "US Dollar",
        id: 4,
        plus: "+"
      },
      {
        earningname: "Indirect Transactional Revenue",
        earningvalue: "0",
        dollar: "US Dollar",
        id: 5,
        plus: "="
      },
      {
        earningname: "Total Transactional Revenue",
        earningvalue: "0",
        dollar: "US Dollar",
        id: 6,
        plus: ""
      }
    ],
    otcbroker: [
      {
        otcbrokername: "Market Rate(Before Broker Fee)",
        otcbrokervalue: "$50000",
        dollar: "Per Bitcoin",
        id: 1
      },
      {
        otcbrokername: "Current Broker Fee",
        otcbrokervalue: "$50000",
        dollar: "Per Bitcoin",
        id: 2
      },
      {
        otcbrokername: "Listed Price(Including Fee)",
        otcbrokervalue: "$50000",
        dollar: "Per Bitcoin",
        id: 3
      }
    ],
    withdraw: [],
    transaction: [
      {
        TXN: "0001",
        Name: "Smith",
        Date: "1/23/2019, 12:55:48 AM",
        OfToken: "30 GXT",
        PaymentMethod: "USD",
        Amount: "$5000",
        Nation: "US Dollar",
        DirectBrokerage: "Gold"
      },
      {
        TXN: "0001",
        Name: "Smith",
        Date: "1/23/2019, 12:55:48 AM",
        OfToken: "30 GXT",
        PaymentMethod: "USD",
        Amount: "$5000",
        Nation: "US Dollar",
        DirectBrokerage: "Gold"
      },
      {
        TXN: "0001",
        Name: "Smith",
        Date: "1/23/2019, 12:55:48 AM",
        OfToken: "30 GXT",
        PaymentMethod: "USD",
        Amount: "$5000",
        Nation: "US Dollar",
        DirectBrokerage: "Gold"
      },
      {
        TXN: "0001",
        Name: "Smith",
        Date: "1/23/2019, 12:55:48 AM",
        OfToken: "30 GXT",
        PaymentMethod: "USD",
        Amount: "$5000",
        Nation: "US Dollar",
        DirectBrokerage: "Gold"
      },
      {
        TXN: "0001",
        Name: "Smith",
        Date: "1/23/2019, 12:55:48 AM",
        OfToken: "30 GXT",
        PaymentMethod: "USD",
        Amount: "$5000",
        Nation: "US Dollar",
        DirectBrokerage: "Gold"
      },
      {
        TXN: "0001",
        Name: "Smith",
        Date: "1/23/2019, 12:55:48 AM",
        OfToken: "30 GXT",
        PaymentMethod: "USD",
        Amount: "$5000",
        Nation: "US Dollar",
        DirectBrokerage: "Gold"
      },
      {
        TXN: "0001",
        Name: "Smith",
        Date: "1/23/2019, 12:55:48 AM",
        OfToken: "30 GXT",
        PaymentMethod: "USD",
        Amount: "$5000",
        Nation: "US Dollar",
        DirectBrokerage: "Gold"
      },
      {
        TXN: "0001",
        Name: "Smith",
        Date: "1/23/2019, 12:55:48 AM",
        OfToken: "30 GXT",
        PaymentMethod: "USD",
        Amount: "$5000",
        Nation: "US Dollar",
        DirectBrokerage: "Gold"
      },
      {
        TXN: "0001",
        Name: "Smith",
        Date: "1/23/2019, 12:55:48 AM",
        OfToken: "30 GXT",
        PaymentMethod: "USD",
        Amount: "$5000",
        Nation: "US Dollar",
        DirectBrokerage: "Gold"
      },
      {
        TXN: "0001",
        Name: "Smith",
        Date: "1/23/2019, 12:55:48 AM",
        OfToken: "30 GXT",
        PaymentMethod: "USD",
        Amount: "$5000",
        Nation: "US Dollar",
        DirectBrokerage: "Gold"
      }
    ],
    color: "1976D2",
    brandname: "",
    brandlogo: null,
    eth_addr: "",
    earn: [],
    affiliate_id: "",
    ref_affiliate: "",
    username: "",
    origin_username: "",
    user_btc: "",
    user_eth: "",
    balance: "0 USD",
    withdraw_balance: 0,
    name: "",
    withdrawals: [],
    twa: 0,
    txns: [],
    names: [],
    aff: "",
    gt: "",
    dgt: "",
    igt: "",
    email: "",
    login_email: "",
    nvest_rate: "............",
    broker_fee: ".............",
    listed_price: "...........",
    current_percentage: ".....",
    redirect: false,
    otc_txns: [],
    dtv: "",
    itv: "",
    dtr: "",
    itr: "",
    mdtv: 0,
    mitv: 0,
    mdtr: 0,
    mitr: 0,
    reg_check: "",
    selection: "",
    txn_ok: false,
    otc_dt: [],
    otc_idt: [],
    otc_dt_total: "",
    otc_idt_total: "",
    logs: [],
    tbal: [],
    broker_name: "",
    brand_img: "",
    clawback: 0,
    brokers: [],
    spinner: true,
    btc_address: "",
    ltc_address: "",
    doge_address: "",
    eth_address: "",
    username_msg: "",
    account_balance: "",
    portfolio_balance: "",
    total_portfolio_balance: "",
    mobile: "",
    upline_otc_fee: 1,
    broker_address: "",
    staked_value: 0,
    stake_allowed: "",
    sef_coins_earned: 0,
    sell_percentage: "",
    is_broker: false,
    faucet_done: false,
    admin_loggedIn: false,
    hodlpassword: "",
    visible: false,
    account_not_found: false,
    enable_staked_initialized: false,
    request_stake: false,
    request_staked_value: 0,
    totp1: false,
    interac_message: "",
    totp_mfa_setup: "",
    brand_logo_link: "",
    pulseValue: "",
    showPluse: false,
    pulsePassword: "",
    coinmarket: [],
    cash_service: false,
    upline_cash_service: false,
    cash_service_changed: false,
    cash_btc_available: 0,
    fiat_balances: {},

    //Global state for Add
    displayAdd: false,
    displayChildThree: true,
    displayWire: false,
    displayWireTwo: false,
    displayWireThree: false,
    greaterThan: false,
    convertValue: false,
    mot_account: false,
    typeAdd: "",
    total_withdraw_amount: 0,
    filterDigitalAssetTerm: "",
    filterDigitalAssetTermMarket: "",
    filterDigitalAssetTermDigital: "",
    filterDigitalAssetTermDigitalDeposit: "",
    digital_payment: false,
    digital_payment_two: false,
    market_payment: false,
    // <--Saved PaymentMethods-->
    paymentMethodsTabOpen: false, //Used To Toggle Payment Methods Drawer
    thingsToPurchase: [], //To Be Used In Saved Payment Methods
    paymentMethodsTab: "0", //For 0-Vaults 1-Cards 2-BankAccounts 3-Altenative 4-Exchanges
    cardInpaymentMethodsTab: 0, // Position Of Card Want To Select
    isPuchasePaymentMethods: false, //Make It true If it is purchase
    callBackIsSuccessPaymentMethodsTab: undefined, //Call Back Funtion To PassPayment Sidebar the, function(isSuccess:bool,message:text,type:text)
    paymentSuccessTitle: {}, //objet To set Succes Page Title {title:"Thanks",subTitle:"You Are Subscribed to"}
    paymentId: "", //Payment Id For Saved Payment
    merchantName: "Atlasandbeyond ", //MerchantNAme For Saved Payment
    isBackendPayment: false, //Is Payment From Backend
    merchantMailId: "Lindsay_scheel@yahoo.com", //Mail Id Of Merchant
    // <--/Saved PaymentMethods-->

    //Global state for Child drawer
    inChildDrawerId: 1,
    displayETransfer: false,
    displayGXBroker: false,

    // Global state for Send
    displaySend: true,
    displaySendOne: false,
    displaySendTwo: false,
    //gx pulse subscribe tier
    buyingFromPulse: false,
    subscription: false,
    subscribeResponse: "",
    icedPayment: false,
    gxid: uuidv4(),
    tradingFloorView: null,
    gx_business_account: false
  };

  storeChange = async e => {
    await this.setState({
      [e.target.name]: e.target.value,
     
    });
    if(this.state.storeName.length>4)
    {
      await this.setState({
        storeNamestatus:true,
      })
    }
  };


  handleatlasChange = async e => {
    await this.setState({
      [e.target.id]: e.target.value,
     
    });
   



   
  };



  

  setCurrenciesList = async () => {
    await axios
      .get(
        `https://comms.globalxchange.com/coin/vault/coins_data?email=mani@nvestbank.com`
      )
      .then(res => {
        console.log("firstaxios", res.data.coins);
        this.setState({
          currency: res.data.coins
        });
      });
  };
  setTradingFloorView = value => {
    this.setState({
      tradingFloorView: value
    });
  };

  changeIcedPayment = val => {
    this.setState({
      icedPayment: val
    });
  };

  setBuyingFromPulse = val => {
    this.setState({
      buyingFromPulse: val
    });
  };

  setSubscribeResponse = val => {
    console.log("value in context", val);
    this.setState({
      subscribeResponse: val
    });
  };

  setSubscription = val => {
    this.setState({
      subscription: val
    });
  };
  changeDisplayGXBroker = val => {
    this.setState({
      displayGXBroker: val
    });
  };

  changeDisplayETransfer = val => {
    this.setState({
      displayETransfer: val
    });
  };

  changeInChildDrawerId = val => {
    this.setState({
      inChildDrawerId: val
    });
  };

  changeDisplaySendTwo = val => {
    this.setState({
      displaySendTwo: val
    });
  };
  changeDisplaySendOne = val => {
    this.setState({
      displaySendOne: val
    });
  };
  changeDisplaySend = val => {
    this.setState({
      displaySend: val
    });
  };
  changeTypeAdd = type => {
    this.setState({
      addType: type
    });
  };
  cancelPayment = () => {
    this.setState({
      isPuchasePaymentMethods: false,
      thingsToPurchase: []
    });
  };

  defaultCallBackForPaymentSidebar = (isSuccess, message, type) => {
    console.log("No Call Back Funtion");
  };

  payWithSidebarOpen = (
    thingsToPurchase,
    paymentMethodsTab,
    cardInpaymentMethodsTab,
    callBackIsSuccessPaymentMethodsTab,
    title,
    paymentId,
    merchantName,

    isBackendPayment,
    merchantMailId
  ) => {
    this.setState({
      paymentMethodsTabOpen: true,
      isPuchasePaymentMethods: true,
      thingsToPurchase,
      paymentMethodsTab,
      cardInpaymentMethodsTab,
      merchantMailId: merchantMailId ? merchantMailId : "shorupan@gmail.com",
      callBackIsSuccessPaymentMethodsTab: callBackIsSuccessPaymentMethodsTab
        ? callBackIsSuccessPaymentMethodsTab
        : this.defaultCallBackForPaymentSidebar, //To Handle No Funtion Passed As Parameter
      paymentSuccessTitle: title
        ? title
        : {
            title: "Thank You",
            subTitle: "Your Purchase Completed Succesfully"
          }, //To Handle No Title Passed As Parameter
      merchantName: merchantName ? merchantName : "",
      isBackendPayment: isBackendPayment,
      paymentId: paymentId ? paymentId : uuidv4()
    });
  };

  paymentMethodsTabTogle = () => {
    this.setState({
      paymentMethodsTabOpen: !this.state.paymentMethodsTabOpen
    });
  };

  changeSubscription = val => {
    this.setState({ SubscriptionData: val });
  };   


  changeDigitalPayment = val => {
    this.setState({ digital_payment: val });
  };
  changeDigitalPaymentTwo = val => {
    this.setState({ digital_payment_two: val });
  };
  changeMarketPayment = val => {
    this.setState({ market_payment: val });
  };
  changeFilterDigitalAssetTerm = term => {
    this.setState({ filterDigitalAssetTerm: term }, () => {
      console.log("market mm", this.state.filterDigitalAssetTerm);
    });
  };
  changeFilterDigitalAssetTermMarket = term => {
    this.setState({ filterDigitalAssetTermMarket: term }, () => {
      console.log("market mm", this.state.filterDigitalAssetTerm);
    });
  };
  changeFilterDigitalAssetTermDigital = term => {
    this.setState({ filterDigitalAssetTermDigital: term }, () => {
      console.log("market mm", this.state.filterDigitalAssetTerm);
    });
  };
  changeFilterDigitalAssetTermDigitalDeposit = term => {
    this.setState({ filterDigitalAssetTermDigitalDeposit: term }, () => {
      console.log("market mm", this.state.filterDigitalAssetTermDigitalDeposit);
    });
  };

  changeConvertValue = val => {
    this.setState({ convertValue: val });
  };
  changeDisplay = val => {
    this.setState({ displayAdd: val });
  };
  changeChildThree = val => {
    this.setState({ displayChildThree: val });
  };
  changeWire = val => {
    this.setState({ displayWire: val });
  };
  changeWireTwo = val => {
    this.setState({ displayWireTwo: val });
  };
  changeWireThree = val => {
    this.setState({ displayWireThree: val });
  };
  changeGreaterThan = val => {
    this.setState({ greaterThan: val });
  };

  handlePulseModal = value => {
    this.setState({ showPluse: value });
  };

  totp1handleClose = () => {
    this.setState({ totp1: false });
  };

  totp1handleShow = () => {
    this.setState({ totp1: true });
  };

  hodlPasswordUpdate = e => {
    this.setState({ hodlpassword: e.target.value });
  };

  hodlUpdate = n2 => {
    this.setState({ visible: n2 });
  };

  handleProfileChange = async e => {
    await this.setState({
      [e.target.id]: e.target.value,
      username_msg: ""
    });
  };

  handleProfileSubmit = async e => {
    e.preventDefault();
    var user_check;
    if (
      this.state.username === this.state.origin_username ||
      this.state.username.length === 0
    )
      user_check = false;
    else user_check = true;
    axios
      .post(fetch.url + "set_affiliate_username", {
        affiliate_id: this.state.affiliate_id,
        username: this.state.username,
        user_btc: this.state.user_btc,
        user_eth: this.state.user_eth,
        user_check: user_check
      })
      .then(r => {
        if (r.data) {
          alert("Updated...");
          window.location.reload();
        } else
          this.setState({
            username_msg:
              "This username is already taken... Try again with a different one!!!"
          });
      });
  };

  updateAccountBalance = value => {
    var { account_balance } = this.state;
    account_balance = account_balance - value;
    console.log(account_balance, value);
    this.setState({
      account_balance: account_balance
    });
  };

  formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  });

  async componentDidMount() {
    this.setState(
      {
        SwithEcommer:"SwithEcommer"
      }
    )
  //   console.log(this.props);
  //   alert()
  //   var cog = userPool.getCurrentUser();
  //   console.log(cog);
  //   var token = localStorage.getItem("token");
  //   //console.log(token);
  //   if (token && token.length > 0) {
  //     console.log("Inside token not fun");
  //     var dd = await axios.post(fetch.url + "brokerage/verify_token", {
  //       token: token,
  //       userpoolid: "us-east-2_ALARK1RAa"
  //     });
  //     var user;
  //     if (dd.data) user = dd.data;
  //     else user = null;

  //     if (user !== null && Date.now() < user.exp * 1000) {
  //       console.log("Inside auth user");
  //       if (user["custom:EthereumAddress"]) {
  //         await this.setState({
  //           email: user.email.toLowerCase(),
  //           login_email: user.email.toLowerCase(),
  //           eth_addr: user["custom:EthereumAddress"]
  //         });
  //       } else {
  //         await this.setState({
  //           email: user.email.toLowerCase(),
  //           login_email: user.email.toLowerCase(),
  //           eth_addr: user.email.toLowerCase()
  //         });
  //       }
  //     } else {
  //       console.log("nulling");
  //       localStorage.setItem("token", "");
  //       return;
  //     }
  //   } else {
  //     console.log("else block token null");
  //     return;
  //   }

  //   var admin_email = localStorage.getItem("admin_email");
  //   var admin_loggedIn = localStorage.getItem("admin_loggedIn");
  //   if (
  //     admin_email !== null &&
  //     admin_email !== undefined &&
  //     admin_email.length > 0
  //   ) {
  //     let token = localStorage.getItem("token");
  //     let login_email = localStorage.getItem("login_account");

  //     let res = await axios.post(fetch.url + "coin/verifyUser", {
  //       email: login_email,
  //       token: token
  //     });

  //     if (res.data.status) {
  //       console.log("Valid");
  //       await this.setState({
  //         email: admin_email.toLowerCase(),
  //         admin_loggedIn: admin_loggedIn
  //       });
  //     } else {
  //       localStorage.setItem("admin_email", "");
  //       localStorage.setItem("admin_loggedIn", false);
  //     }
  //   }

  //   var eth_addr = this.state.eth_addr;
  //   var email = this.state.email;
  //   window.$crisp.push(["set", "user:email", email]);
  //   localStorage.setItem("user_account", email);
  //   console.log(email);
  //   var selection,
  //     fields = [],
  //     name;
  //   let dat = await axios.post(fetch.url + "get_affiliate_data_no_logs", {
  //     ref_eth: eth_addr,
  //     email: email
  //   });
  //   if (dat.data.length <= 0) {
  //     await this.setState({ account_not_found: true });
  //     return;
  //   }
  //   dat.data = dat.data[0];
  //   name = dat.data.name
  //     ? dat.data.name
  //     : dat.data.first_name + " " + dat.data.last_name;
  //   eth_addr =
  //     (dat.data.ref_eth && dat.data.ref_eth.length) > 0
  //       ? dat.data.ref_eth
  //       : this.state.eth_addr;

  //   await this.setState(
  //     {
  //       affiliate_id: dat.data.affiliate_id,
  //       ref_affiliate: dat.data.ref_affiliate,
  //       eth_addr: eth_addr,
  //       broker_name: name,
  //       account_balance: dat.data.account_balance
  //         ? dat.data.account_balance
  //         : 0,
  //       mobile: dat.data.mobile ? dat.data.mobile : "NA",
  //       faucet_done: dat.data.faucet_done ? dat.data.faucet_done : false,
  //       broker_address: dat.data.address ? dat.data.address : "",
  //       totp_mfa_setup: dat.data.totp_mfa_setup
  //         ? dat.data.totp_mfa_setup
  //         : false,
  //       totp1: dat.data.totp_mfa_setup ? false : true,
  //       brand_logo_link: dat.data.brand_logo_link
  //         ? dat.data.brand_logo_link
  //         : "",
  //       request_staked_value: dat.data.request_staked_value
  //         ? dat.data.request_staked_value
  //         : 0,
  //       request_stake:
  //         dat.data.request_staked_value &&
  //         parseFloat(dat.data.request_staked_value) >= 450
  //           ? true
  //           : false,
  //       enable_staked_initialized: dat.data.enable_staked_initialized
  //         ? dat.data.enable_staked_initialized
  //         : false,
  //       sell_percentage:
  //         dat.data.sell_percentage && parseFloat(dat.data.sell_percentage) >= 1
  //           ? parseFloat(dat.data.sell_percentage)
  //           : 1,
  //       cash_service: dat.data.cash_service ? dat.data.cash_service : false,
  //       fiat_balances: dat.data.fiat_balances,
  //       mot_account: dat.data.mot_account ? dat.data.mot_account : false,
  //       gx_business_account: dat.data.gx_business_account? dat.data.gx_business_account: false
  //     },
  //     () => {
  //       console.log("portfolio context account b", this.state.account_balance);
  //     }
  //   );

  //   if (dat.data.interac_message) {
  //     this.setState({
  //       interac_message: dat.data.interac_message
  //     });
  //   } else {
  //     axios
  //       .post(fetch.url + "coin/fiat/createMessageInterac", {
  //         email: this.state.email
  //       })
  //       .then(r => {
  //         if (r.data.status) {
  //           this.setState({ interac_message: r.data.msg });
  //         } else this.setState({ interac_message: "" });
  //       });
  //   }

  //   axios
  //     .post(fetch.url + "get_affiliate_data_no_logs", {
  //       affiliate_id: this.state.ref_affiliate
  //     })
  //     .then(r => {
  //       r = r.data;
  //       if (r.length > 0) {
  //         r = r[0];
  //         let cash_service = r.cash_service ? r.cash_service : false;
  //         let btc_reserve_balance = r.btc_reserve_balance
  //           ? parseFloat(r.btc_reserve_balance)
  //           : 0;
  //         if (btc_reserve_balance > 0) {
  //           let cash_btc = btc_reserve_balance * (80 / 100);
  //           cash_btc = Number(parseFloat(cash_btc.toFixed(5)));
  //           this.setState({
  //             upline_cash_service: cash_service,
  //             cash_btc_available: cash_btc
  //           });
  //         }
  //       }
  //     });

  //   axios
  //     .post("https://comms.globalxchange.com/btcbrokerage/current_percentage", {
  //       broker_id: this.state.ref_affiliate
  //     })
  //     .then(r => {
  //       var k = r.data;
  //       if (parseFloat(k) > 0) this.setState({ upline_otc_fee: k });
  //     });

  //   var clawback = 0;
  //   var clawback_obj = [];
  //   axios
  //     .post(fetch.url + "get_affiliate_logs_data", {
  //       email: email
  //     })
  //     .then(async data => {
  //       if (data.data[0].logs) {
  //         var logs = data.data[0].logs;
  //         logs.sort(function(a, b) {
  //           var t1 = a.txn.ts ? a.txn.ts : a.txn.timestamp;
  //           var t2 = b.txn.ts ? b.txn.ts : b.txn.timestamp;
  //           if (t1 < t2) return 1;
  //           if (t1 > t2) return -1;
  //           return 0;
  //         });
  //         //console.log(logs);
  //         var balen = 0;
  //         var tbal = [];
  //         var d = new Date();
  //         var week = d.getWeek();
  //         //console.log("Week", week);
  //         var clwb_2 = 0;
  //         for (var i = logs.length - 1; i >= 0; i--) {
  //           if (logs[i].type === "withdrawal") {
  //             balen = balen - Math.round(logs[i].txn.withdrawl_amt * 100) / 100;
  //           } else {
  //             if (logs[i].credit) {
  //               if (logs[i].commissions.dt_commission < 0) {
  //                 balen =
  //                   Math.round(
  //                     (balen +
  //                       (logs[i].commissions.it_commission
  //                         ? logs[i].commissions.it_commission
  //                         : 0) +
  //                       (logs[i].commissions.bd_commission
  //                         ? logs[i].commissions.bd_commission
  //                         : 0)) *
  //                       1000
  //                   ) / 1000;
  //               } else {
  //                 balen =
  //                   Math.round(
  //                     (balen +
  //                       (logs[i].commissions.dt_commission
  //                         ? logs[i].commissions.dt_commission
  //                         : 0 + logs[i].commissions.it_commission
  //                         ? logs[i].commissions.it_commission
  //                         : 0) +
  //                       (logs[i].commissions.bd_commission
  //                         ? logs[i].commissions.bd_commission
  //                         : 0)) *
  //                       1000
  //                   ) / 1000;
  //               }

  //               var balen1 = logs[i].txn.withdrawl_amt
  //                 ? Math.round(parseFloat(logs[i].txn.withdrawl_amt) * 100) /
  //                   100
  //                 : 0;
  //               //console.log(balen1);
  //               balen = balen + balen1;
  //               //console.log(balen);
  //             }
  //             if (logs[i].debit) {
  //               balen =
  //                 Math.round(
  //                   (balen +
  //                     (logs[i].commissions.dt_commission +
  //                       logs[i].commissions.it_commission +
  //                       (logs[i].commissions.bd_commission
  //                         ? logs[i].commissions.bd_commission
  //                         : 0))) *
  //                     1000
  //                 ) / 1000;
  //               clawback =
  //                 Math.round(
  //                   (logs[i].commissions.dt_commission +
  //                     logs[i].commissions.it_commission +
  //                     (logs[i].commissions.bd_commission
  //                       ? logs[i].commissions.bd_commission
  //                       : 0)) *
  //                     1000
  //                 ) / 1000;
  //               var clawback_voulme = logs[i].txn.usd_amt
  //                 ? logs[i].txn.usd_amt
  //                 : logs[i].txn.charged_stripe_amount
  //                 ? logs[i].txn.charged_stripe_amount
  //                 : logs[i].txn.final_charged_amount
  //                 ? logs[i].txn.final_charged_amount
  //                 : 0;
  //               var dt = new Date(logs[i].commissions.date);
  //               var wek = dt.getWeek();
  //               if (wek === week || wek === week - 1) {
  //                 //console.log(wek);
  //                 clwb_2 = clwb_2 + clawback;
  //               }
  //               var clawback_type =
  //                 logs[i].commissions.dt_commission > 0 ? "DTR" : "ITR";
  //               clawback_obj.push({
  //                 clawback_voulme: 2 * clawback_voulme,
  //                 clawback: 2 * clawback,
  //                 clawback_type: clawback_type
  //               });
  //             }
  //           }
  //           tbal.push(balen);
  //         }
  //         var bala = [];
  //         let final_bal = balen;
  //         var j = 0;
  //         for (i = tbal.length - 1; i >= 0; i--) {
  //           bala.push(tbal[i]);
  //           logs[j].balance = tbal[i];
  //           j++;
  //         }
  //         await this.setState({
  //           logs: logs,
  //           tbal: bala,
  //           clawback: clawback,
  //           clawback_obj: clawback_obj,
  //           clwb_2: clwb_2,
  //           balance: "...",
  //           withdraw_balance: final_bal,
  //           spinner: false
  //         });
  //         //console.log(bala);
  //         //console.log(clwb_2);
  //       } else {
  //         console.log("IN else..............");
  //         this.setState({ logs: [], spinner: false });
  //       }

  //       i = 4;
  //       var earnings;
  //       if (false) {
  //         earnings = this.state.earningvalue.map((data, id) => {
  //           if (i < fields.length) {
  //             i++;
  //             return {
  //               ...data,
  //               earningname: fields[i - 1].name ? fields[i - 1].name : "",
  //               earningvalue:
  //                 fields[i - 1].value.length > 0 ? fields[i - 1].value : "0.00",
  //               dollar: fields[i - 1].value1
  //                 ? fields[i - 1].value1
  //                 : "US Dollars"
  //             };
  //           } else {
  //             return {
  //               ...data
  //             };
  //           }
  //         });
  //       } else {
  //         let com_otc = await axios.post(
  //           fetch.url + "brokerage/commissions_otc_totals",
  //           { affiliate_id: this.state.affiliate_id }
  //         );
  //         //console.log(com_otc.data);
  //         var otc_values = [
  //           Math.round(com_otc.data.dtot * 100) / 100,
  //           Math.round(com_otc.data.indtot * 100) / 100,
  //           Math.round(com_otc.data.direct_total * 100) / 100,
  //           Math.round(com_otc.data.indirect_total * 100) / 100,
  //           Math.round(com_otc.data.bd_total * 100) / 100,
  //           Math.round(com_otc.data.bdtot * 100) / 100
  //         ];
  //         let com_token = await axios.post(
  //           fetch.url + "brokerage/commissions_token_totals",
  //           { affiliate_id: this.state.affiliate_id }
  //         );
  //         //console.log(com_token.data);
  //         var tok_values = [
  //           Math.round(com_token.data.dtot * 100) / 100,
  //           Math.round(com_token.data.indtot * 100) / 100,
  //           Math.round(com_token.data.direct_total * 100) / 100,
  //           Math.round(com_token.data.indirect_total * 100) / 100,
  //           Math.round(com_token.data.bd_total * 100) / 100,
  //           Math.round(com_otc.data.bdtot * 100) / 100
  //         ];
  //         var earn = [
  //           tok_values[0] + otc_values[0],
  //           tok_values[1] + otc_values[1] + tok_values[5] + otc_values[5],
  //           tok_values[0] +
  //             tok_values[1] +
  //             otc_values[0] +
  //             otc_values[1] +
  //             tok_values[5] +
  //             otc_values[5],
  //           tok_values[2] + otc_values[2],
  //           tok_values[3] + otc_values[3] + tok_values[4] + otc_values[4],
  //           tok_values[2] +
  //             tok_values[3] +
  //             otc_values[2] +
  //             otc_values[3] +
  //             tok_values[4] +
  //             otc_values[4]
  //         ];
  //         //console.log(earn);
  //         // if (clawback_obj.length > 0) {
  //         //   //console.log(clawback_obj);
  //         //   clawback_obj.forEach(z => {
  //         //     if (z.clawback_type === "DTR") {
  //         //       earn[0] = earn[0] - z.clawback_voulme;
  //         //       earn[2] = earn[2] - z.clawback_voulme;
  //         //       earn[3] = earn[3] - z.clawback;
  //         //       earn[5] = earn[5] - z.clawback;
  //         //     } else {
  //         //       earn[1] = earn[1] - z.clawback_voulme;
  //         //       earn[2] = earn[2] - z.clawback_voulme;
  //         //       earn[4] = earn[4] - z.clawback;
  //         //       earn[5] = earn[5] - z.clawback;
  //         //     }
  //         //   });
  //         // }
  //         console.log(earn);
  //         this.setState({
  //           earn: earn
  //         });
  //         earnings = this.state.earningvalue.map((data, id) => {
  //           return {
  //             ...data,
  //             earningvalue: Math.round(earn[id] * 100) / 100
  //           };
  //         });
  //       }
  //       this.setState({
  //         earningvalue: earnings
  //       });
  //       //console.log("dtv....");
  //       //console.log(earnings);

  //       axios
  //         .post(fetch.url + "withdrawal_address", {
  //           ref_eth: eth_addr,
  //           email: this.state.email
  //         })
  //         .then(async data => {
  //           ////console.log(data.data);
  //           let withd = data.data;
  //           if (data.data && data.data.length > 0) {
  //             withd.sort(function(a, b) {
  //               if (a.ts < b.ts) return 1;
  //               if (a.ts > b.ts) return -1;
  //               return 0;
  //             });
  //           }
  //           await this.setState({
  //             withdraw: withd,
  //             withdrawals: withd,
  //             total_withdraw_amount: withd[0]
  //               ? parseFloat(withd[0].withdrawl_amt)
  //               : 0
  //           });
  //           this.my_withdrawals("all", 1);
  //         });
  //     });

  //   //console.log(this.state.affiliate_id, this.state.ref_affiliate);
  //   if (dat.data.user_eth) {
  //     this.setState({
  //       user_eth: dat.data.user_eth
  //     });
  //   }
  //   if (dat.data.username) {
  //     this.setState({
  //       username: dat.data.username
  //     });
  //     localStorage.setItem("uusseerrname", dat.data.username);
  //   }

  //   if (dat.data.BTC_Address) {
  //     console.log("frromm DB...");
  //     await this.setState({
  //       btc_address: dat.data.BTC_Address,
  //       ltc_address: dat.data.LTC_Address,
  //       doge_address: dat.data.DOGE_Address
  //     });
  //   } else {
  //     axios
  //       .post(fetch.url + "coin/create_newAddress", {
  //         email: email,
  //         affiliate_id: this.state.affiliate_id,
  //         token: token
  //       })
  //       .then(async res => {
  //         console.log(res.data);
  //         if (res.data.status) {
  //           await this.setState({
  //             btc_address: res.data.address[1],
  //             ltc_address: res.data.address[0],
  //             doge_address: res.data.address[2]
  //           });
  //         }
  //       });
  //   }

  //   localStorage.setItem("user_btc_address", this.state.btc_address);
  //   localStorage.setItem("user_ltc_address", this.state.ltc_address);
  //   localStorage.setItem("user_doge_address", this.state.doge_address);

  //   let staked_value;

  //   if (dat.data.ETH_Address) {
  //     await this.setState({ eth_address: dat.data.ETH_Address });
  //     let data = await axios.get(
  //       fetch.url + "get_staked_value?address=0x" + dat.data.ETH_Address
  //     );
  //     staked_value = data.data.staked_value;
  //     this.setState({
  //       staked_value: staked_value,
  //       stake_allowed: dat.data.stake_allowed ? dat.data.stake_allowed : false,
  //       sef_coins_earned: dat.data.sef_coins_earned
  //         ? dat.data.sef_coins_earned
  //         : 0
  //     });
  //     localStorage.setItem(
  //       "user_eth_addr",
  //       "0x" + dat.data.ETH_Address.toLowerCase()
  //     );
  //   } else {
  //     axios
  //       .post(fetch.url + "coin/store_eth_address", {
  //         email: email,
  //         token: token
  //       })
  //       .then(async res => {
  //         if (res.data.status) {
  //           await this.setState({
  //             eth_address: res.data.eth_address
  //           });
  //           let data = await axios.get(
  //             fetch.url + "get_staked_value?address=" + res.data.eth_address
  //           );
  //           staked_value = data.data.staked_value;
  //           this.setState({
  //             staked_value: staked_value,
  //             stake_allowed: dat.data.stake_allowed
  //               ? dat.data.stake_allowed
  //               : false,
  //             sef_coins_earned: dat.data.sef_coins_earned
  //               ? dat.data.sef_coins_earned
  //               : 0
  //           });
  //           localStorage.setItem(
  //             "user_eth_addr",
  //             "0x" + res.data.eth_address.toLowerCase()
  //           );
  //         }
  //       });
  //   }

  //   if (
  //     staked_value >= 450 ||
  //     this.state.request_staked_value >= 450 ||
  //     parseFloat(staked_value) + parseFloat(this.state.request_staked_value) >=
  //       450
  //   ) {
  //     this.setState({ is_broker: true });
  //   }

  //   axios
  //     .post(fetch.url + "get_broker_names", {
  //       affiliate_id: this.state.affiliate_id
  //     })
  //     .then(r => {
  //       var brokers = [];
  //       if (r.data) {
  //         brokers = r.data;
  //       }
  //       var obj = {
  //         _id: "1",
  //         name: this.state.broker_name,
  //         email: this.state.email,
  //         affiliate_id: this.state.affiliate_id,
  //         ref_affiliate: this.state.ref_affiliate,
  //         timestamp: 0
  //       };
  //       brokers.push(obj);
  //       this.setState({ brokers: brokers });
  //     });

  //   let data = await axios.post(fetch.url + "admin_get_stats", {
  //     ref_eth: eth_addr,
  //     affiliate_id: this.state.affiliate_id
  //   });
  //   console.log("Selection.........");
  //   //console.log(data.data);
  //   if (data.data.admin_values !== undefined) {
  //     selection = data.data.admin_values;
  //     //console.log(selection);
  //   } else {
  //     selection = false;
  //   }
  //   if (data.data.fields) {
  //     fields = data.data.fields;
  //     name = data.data.name;
  //     console.log("connecting", data.data.fields);
  //     if (selection) {
  //       this.setState({
  //         balance: fields[fields.length - 1].value,
  //         name: name
  //       });
  //     } else
  //       this.setState({
  //         balance: "...",
  //         name: name
  //       });
  //   } else
  //     this.setState({
  //       name: data.data.name ? data.data.name : ""
  //     });
  //   //console.log(fields);
  //   var val = [];
  //   //this.broker_levels();

  //   this.setState({
  //     selection: selection
  //   });
  //   if (!selection) {
  //     let direct_signups = await axios.post(
  //       `${fetch.url}brokerage/direct_signups`,
  //       {
  //         ref_eth: eth_addr,
  //         affiliate_id: this.state.affiliate_id
  //       }
  //     );
  //     //console.log(direct_signups.data);
  //     //console.log("otc-affid", this.state.affiliate_id);
  //     let utbv = await axios.post(`${fetch.url}brokerage/brokers_customers`, {
  //       ref_eth: eth_addr,
  //       affiliate_id: this.state.affiliate_id
  //     });
  //     val.push(utbv.data.customers + utbv.data.brokers);
  //     val.push(utbv.data.brokers);
  //     val.push(utbv.data.customers);
  //     val.push(direct_signups.data.direct_signups);
  //   } else {
  //     val.push(fields[0].value.length > 0 ? fields[0].value : "0");
  //     val.push(fields[1].value.length > 0 ? fields[1].value : "0");
  //     val.push(fields[2].value.length > 0 ? fields[2].value : "0");
  //     val.push(fields[3].value.length > 0 ? fields[3].value : "0");
  //   }

  //   // console.log("APITEST val is ", val);

  //   axios
  //     .post("https://comms.globalxchange.com/broker_branding", {
  //       email: email,
  //       retrive: "enabled"
  //     })
  //     .then(data => {
  //       //console.log(data);
  //       if (data.data.logo_url)
  //         this.setState({ brand_img: data.data.logo_url });
  //     });

  //   var values = this.state.Brokerage.map((data, id) => {
  //     return {
  //       ...data,
  //       value: val[id]
  //     };
  //   });
  //   this.setState({
  //     Brokerage: values
  //   });

  //   //console.log(this.state.Brokerage);

  //   //await this.get_transactions();

  //   let k;
  //   let internal_source;
  //   axios
  //     .post("https://comms.globalxchange.com/btcbrokerage/current_percentage", {
  //       broker_id: this.state.affiliate_id
  //     })
  //     .then(r => {
  //       k = parseFloat(r.data) > 0 ? parseFloat(r.data) : 1;
  //     });
  //   axios
  //     .post("https://comms.globalxchange.com/btcbrokerage/nvest_i_s_get")
  //     .then(r => {
  //       internal_source = r.data;
  //       //console.log(r.data)
  //       //console.log(broker_id);
  //       axios
  //         .post("https://comms.globalxchange.com/btcbrokerage/get_btc_value", {
  //           source: "btc",
  //           internal_source: internal_source,
  //           source_amt: 10,
  //           broker_id: this.state.affiliate_id
  //         })
  //         .then(async y => {
  //           //console.log(y);
  //           let x = y.data.nvest_spot_quote / 10;
  //           let broker_fee = x * (1 + k / 100) - x;
  //           let listed_price = x * (1 + k / 100);
  //           await this.setState({
  //             nvest_rate: Math.round(x * 100) / 100,
  //             broker_fee: Math.round(broker_fee * 100) / 100,
  //             listed_price: Math.round(listed_price * 100) / 100,
  //             current_percentage: k
  //           });
  //         });
  //     });

  //   let aff_otc = [];
  //   let name_otc = [];

  //   axios
  //     .post("https://comms.globalxchange.com/btcbrokerage/reg_check", {
  //       broker_id: this.state.affiliate_id
  //     })
  //     .then(r => {
  //       if (r.data == false) {
  //         //console.log("falseeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee")
  //         var options = {
  //             timeZone: "America/New_York"
  //           }, // you have to know that New York in EST
  //           estTime = new Date();
  //         let date = estTime.toLocaleString("en-US", options);
  //         let timestamp = Date.now();
  //         //console.log(broker_id);

  //         axios
  //           .post("https://comms.globalxchange.com/btcbrokerage/register_btc", {
  //             broker_id: this.state.affiliate_id,
  //             date: date,
  //             timestamp: timestamp
  //           })
  //           .then(r => {
  //             //window.location.reload();
  //             console.log("registration success");
  //           });
  //       } else {
  //         console.log(r.data);
  //         console.log("already registered for otc");
  //       }
  //     });
  // }

  // admin_email = async email => {
  //   console.log(email);
  //   let dat = await axios.post(fetch.url + "get_affiliate_data", {
  //     email: email
  //   });
  //   await this.setState({
  //     affiliate_id: dat.data.affiliate_id,
  //     ref_affiliate: dat.data.ref_affiliate
  //   });

  //   //await this.get_transactions();
  // };

  // get_Week_number = ts => {
  //   var d = new Date(ts);
  //   return d.getWeek();
  // };

  // save_percentage = async percentage => {
  //   console.log("ggggggggggggggggggggggggggg");
  //   var options = {
  //       timeZone: "America/New_York"
  //     }, // you have to know that New York in EST
  //     estTime = new Date();
  //   let date = estTime.toLocaleString("en-US", options);
  //   let timestamp = Date.now();
  //   ////console.log(document.getElementById("percentage").value)
  //   axios
  //     .post("https://comms.globalxchange.com/btcbrokerage/save_percentage", {
  //       broker_id: this.state.affiliate_id,
  //       date: date,
  //       timestamp: timestamp,
  //       percentage: percentage
  //     })
  //     .then(r => {
  //       alert("Fee Percentage Updated Successfully");
  //       window.location.reload();
  //     });
  // };

  // arr = [0, 0, 0, 0];
  // my_withdrawals = async (st, j) => {
  //   const calc_bal = async twa => {
  //     var w = moment().isoWeek();
  //     var year = moment().year();
  //     var wearn = 0;
  //     var token;
  //     if (this.state.mot_account) {
  //       token = await axios.post(
  //         fetch.url + "brokerage/commissions_token_date",
  //         {
  //           affiliate_id: this.state.affiliate_id,
  //           number: w,
  //           year: parseInt(year),
  //           type: "week"
  //         }
  //       );

  //       wearn =
  //         wearn +
  //         token.data.direct_total +
  //         token.data.indirect_total +
  //         token.data.bd_total;
  //       console.log("Token wearn 1", wearn);
  //     }

  //     var otc = await axios.post(fetch.url + "brokerage/commissions_otc_date", {
  //       affiliate_id: this.state.affiliate_id,
  //       number: w,
  //       year: parseInt(year),
  //       type: "week"
  //     });

  //     wearn =
  //       wearn +
  //       otc.data.direct_total +
  //       otc.data.indirect_total +
  //       otc.data.bd_total;

  //     if (this.state.mot_account) {
  //       token = await axios.post(
  //         fetch.url + "brokerage/commissions_token_date",
  //         {
  //           affiliate_id: this.state.affiliate_id,
  //           number: w - 1,
  //           year: parseInt(year),
  //           type: "week"
  //         }
  //       );

  //       let tot =
  //         token.data.direct_total +
  //         token.data.indirect_total +
  //         token.data.bd_total;
  //       wearn = wearn + tot;
  //       console.log("token wearn 2", tot);
  //     }

  };

  bc_calci(diff, amt) {
    switch (diff) {
      case 1:
        return Math.round(amt * 0.0165 * 10000) / 10000;
      case 2:
        return Math.round(amt * 0.0111 * 10000) / 10000;
      case 3:
        return Math.round(amt * 0.0074 * 10000) / 10000;
      case 4:
        return Math.round(amt * 0.005 * 10000) / 10000;
      case 5:
        return Math.round(amt * 0.0033 * 10000) / 10000;
      case 6:
        return Math.round(amt * 0.0022 * 10000) / 10000;
      case 7:
        return Math.round(amt * 0.0015 * 10000) / 10000;
      case 8:
        return Math.round(amt * 0.001 * 10000) / 10000;
      case 9:
        return Math.round(amt * 0.0007 * 10000) / 10000;
      case 10:
        return Math.round(amt * 0.0004 * 10000) / 10000;
      default:
        return 0;
    }
  }

  bc_level = (aff, amt) => {
    const { affs, aff_index } = this;
    var ind, diff, bc;
    for (var i = 0; i < affs.length; i++) {
      if (affs[i].includes(aff)) {
        ind = i;
        diff = ind - aff_index;
        ////console.log(ind, diff);
        bc = this.bc_calci(diff, amt);
        break;
      }
    }
    return bc;
  };

  darr = [];
  Txn_default = w => {
    const { txns } = this.state;
    var td = [];
    var d,
      week,
      dgt = 0;
    if (w === 0) {
      if (this.darr.every((val, i, arr) => val === arr[0])) {
        txns.forEach(x => {
          if (x.num_of_tokens > 0) dgt = dgt + parseInt(x.num_of_tokens);
        });
        this.setState({
          transaction: this.state.txns,
          gt: dgt
        });
        return;
      }
    } else {
      if (this.darr.indexOf(w) >= 0) {
        //console.log(this.darr.indexOf(w));
        this.darr[this.darr.indexOf(w)] = 0;
        //console.log(this.darr);
      } else {
        this.darr.push(w);
        //console.log(this.darr);
      }
    }
    for (var i = 0; i < txns.length; i++) {
      d = new Date(parseInt(txns[i].timestamp));
      week = d.getWeek();
      if (this.darr.includes(week)) {
        if (txns[i].num_of_tokens > 0) {
          dgt = dgt + parseInt(txns[i].num_of_tokens);
        }
        td.push(txns[i]);
      }
      if (i === txns.length - 1) {
        this.setState({
          transaction: td,
          gt: dgt
        });
      }
    }
  };

  dtarr = [];
  Txn_direct = w => {
    //console.log("Yo, direct");
    const { txns } = this.state;
    var dt = [];
    var dtw = [];
    var dtv = 0,
      dtr = 0;
    var d,
      week,
      dgt = 0;
    txns.forEach(tx => {
      if (tx.affiliate_id === this.state.affiliate_id) {
        dt.push(tx);
      }
    });
    if (w === 0) {
      if (this.dtarr.every((val, i, arr) => val === arr[0])) {
        dt.forEach(x => {
          if (x.num_of_tokens > 0) dgt = dgt + parseInt(x.num_of_tokens);
          if (x.usd_amt > 0) {
            dtv = dtv + parseFloat(x.usd_amt);
            dtr = dtr + parseFloat(x.usd_amt) * 0.15;
          }
          // else {
          //   if (x.amount > 0) {
          //     dtv = dtv + parseFloat(x.amount);
          //     dtr = dtr + parseFloat(x.amount) * 0.15;
          //   }
          // }
        });
        this.setState({
          transaction: dt,
          dgt: dgt,
          dtv: dtv,
          dtr: dtr,
          mdtv: dtv + this.state.mdtv,
          mdtr: dtr + this.state.mdtr
        });
        console.log(dtv, "direct_volume", dtr, "direct revenue");
        return;
      }
    } else {
      if (this.dtarr.indexOf(w) >= 0) {
        //console.log(this.darr.indexOf(w));
        this.dtarr[this.dtarr.indexOf(w)] = 0;
        //console.log(this.darr);
      } else {
        this.dtarr.push(w);
        //console.log(this.darr);
      }
    }
    for (var i = 0; i < dt.length; i++) {
      d = new Date(parseInt(dt[i].timestamp));
      week = d.getWeek();
      if (this.dtarr.includes(week)) {
        if (dt[i].num_of_tokens > 0) {
          dgt = dgt + parseInt(dt[i].num_of_tokens);
        }
        if (dt[i].usd_amt > 0) {
          dtv = dtv + parseFloat(dt[i].usd_amt);
          dtr = dtr + parseFloat(dt[i].usd_amt) * 0.15;
        }
        // else {
        //   if (dt[i].amount > 0) {
        //     dtv = dtv + parseFloat(dt[i].amount);
        //     dtr = dtr + parseFloat(dt[i].amount) * 0.15;
        //   }
        // }
        dtw.push(dt[i]);
      }
      if (i === dt.length - 1) {
        this.setState({
          transaction: dtw,
          dgt: dgt,
          dtv: dtv,
          dtr: dtr,
          mdtv: dtv + this.state.mdtv,
          mdtr: dtr + this.state.mdtr
        });
      }
    }
  };

  diarr = [];
  Txn_indirect = w => {
    //console.log("Yo!!, Indirect");
    const { txns } = this.state;
    var dt = [];
    var dtw = [];
    var itv = 0,
      itr = 0;
    var d,
      week,
      dgt = 0;
    txns.forEach(tx => {
      if (tx.affiliate_id !== this.state.affiliate_id) {
        dt.push(tx);
      }
    });

    if (w === 0) {
      if (this.diarr.every((val, i, arr) => val === arr[0])) {
        dt.forEach(x => {
          if (x.num_of_tokens > 0) dgt = dgt + parseInt(x.num_of_tokens);
          if (x.usd_amt > 0) {
            itv = itv + parseFloat(x.usd_amt);
            itr = itr + this.bc_level(x.affiliate_id, x.usd_amt);
          }
          // else {
          //   if (x.amount > 0) {
          //     itv = itv + parseFloat(x.amount);
          //     itr = itr + this.bc_level(x.affiliate_id, x.amount);
          //   }
          // }
        });
        this.setState({
          transaction: dt,
          igt: dgt,
          itv: itv,
          itr: itr,
          mitv: itv + this.state.mitv,
          mitr: itr + this.state.mitr
        });
        console.log(itv, "Indirect", itr, "ITR");
        return;
      }
    } else {
      if (this.diarr.indexOf(w) >= 0) {
        //console.log(this.darr.indexOf(w));
        this.diarr[this.diarr.indexOf(w)] = 0;
        //console.log(this.darr);
      } else {
        this.diarr.push(w);
        //console.log(this.darr);
      }
    }

    for (var i = 0; i < dt.length; i++) {
      d = new Date(parseInt(dt[i].timestamp));
      week = d.getWeek();
      if (this.diarr.includes(week)) {
        if (dt[i].num_of_tokens > 0) {
          dgt = dgt + parseInt(dt[i].num_of_tokens);
        }
        if (dt[i].usd_amt > 0) {
          itv = itv + parseFloat(dt[i].usd_amt);
          itr = itr + this.bc_level(dt[i].affiliate_id, dt[i].usd_amt);
        }
        // else {
        //   if (dt[i].amount > 0) {
        //     itv = itv + parseFloat(dt[i].amount);
        //     itr = itr + this.bc_level(dt[i].affiliate_id, dt[i].amount);
        //   }
        // }
        dtw.push(dt[i]);
      }
      if (i === dt.length - 1) {
        this.setState({
          transaction: dtw,
          igt: dgt,
          itv: itv,
          itr: itr,
          mitv: itv + this.state.mitv,
          mitr: itr + this.state.mitr
        });
      }
    }
  };

  otc_direct = async () => {
    const { otc_txns } = this.state;
    let otc_dt = [];
    otc_txns.forEach(tx => {
      if (tx.broker_id === this.state.affiliate_id) {
        otc_dt.push(tx);
      }
    });

    await this.setState({
      otc_dt: otc_dt
    });
  };

  otc_indirect = async () => {
    const { otc_txns } = this.state;
    let otc_idt = [];
    otc_txns.forEach(tx => {
      if (tx.broker_id !== this.state.affiliate_id) {
        otc_idt.push(tx);
      }
    });

    await this.setState({
      otc_idt: otc_idt
    });
  };

  handleChangePageLayout = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  submitHandler = e => {
    e.preventDefault();
    const { pulsePassword } = this.state;
    if (pulsePassword == "gxtbitcoinadminpassword123") {
      this.setState({
        showPluse: false,
        pulsePassword: ""
      });
    } else {
      alert("Incorrect Password");
    }
  };

  fileSelectedHandler = e => {
    this.setState({
      brandlogo: e.target.files[0]
    });
    console.log("image", e.target.files[0]);
  };

  display_earnings = earn => {
    var earnings = this.state.earningvalue.map((data, id) => {
      return {
        ...data,
        earningvalue: Math.round(earn[id] * 100) / 100
      };
    });
    this.setState({
      earningvalue: earnings
    });
  };

  earnings_week = async w => {
    var token = await axios.post(
      fetch.url + "brokerage/commissions_token_date",
      {
        affiliate_id: this.state.affiliate_id,
        number: w,
        year: 2019,
        type: "week"
      }
    );
    var otc = await axios.post(fetch.url + "brokerage/commissions_otc_date", {
      affiliate_id: this.state.affiliate_id,
      number: w,
      year: 2019,
      type: "week"
    });
    var w_arr = [
      token.data.dtot + otc.data.dtot,
      token.data.indtot + otc.data.indtot,
      token.data.dtot + otc.data.dtot + token.data.indtot + otc.data.indtot,
      token.data.direct_total + otc.data.direct_total,
      token.data.indirect_total + otc.data.indirect_total,
      token.data.direct_total +
        otc.data.direct_total +
        token.data.indirect_total +
        otc.data.indirect_total
    ];

    this.display_earnings(w_arr);
    this.setState({
      earningsheader: `Week ${w}`
    });
  };

  earnings_month = async (m, month) => {
    console.log(m, month, "from context");
    var token = await axios.post(
      fetch.url + "brokerage/commissions_token_date",
      {
        affiliate_id: this.state.affiliate_id,
        number: m,
        year: 2019,
        type: "month"
      }
    );
    var otc = await axios.post(fetch.url + "brokerage/commissions_otc_date", {
      affiliate_id: this.state.affiliate_id,
      number: m,
      year: 2019,
      type: "month"
    });
    var m_arr = [
      token.data.dtot + otc.data.dtot,
      token.data.indtot + otc.data.indtot,
      token.data.dtot + otc.data.dtot + token.data.indtot + otc.data.indtot,
      token.data.direct_total + otc.data.direct_total,
      token.data.indirect_total + otc.data.indirect_total,
      token.data.direct_total +
        otc.data.direct_total +
        token.data.indirect_total +
        otc.data.indirect_total
    ];

    this.display_earnings(m_arr);
    this.setState({
      earningsheader: month
    });
  };

  weekly = async w => {
    //console.log("Yo!!!!!!!..........from context-Week", w);
    var wk = w;
    var earnings;
    // if (w.includes("ek")) {
    //   wk = 25;
    // } else {
    //   wk = parseInt(w);
    // }
    if (!this.state.selection) {
      this.dtarr = [];
      this.diarr = [];
      this.Txn_direct(wk);
      this.Txn_indirect(wk);
      await this.setState({
        earningsheader: `Week ${wk}`
      });
      console.log(
        this.state.dtr,
        this.state.itr,
        this.state.dtv,
        this.state.itv
      );
      var ear = [];
      ear.push(this.state.dtv);
      ear.push(this.state.itv);
      ear.push(this.state.dtv + this.state.itv);
      ear.push(this.state.dtr);
      ear.push(this.state.itr);
      ear.push(this.state.dtr + this.state.itr);
      earnings = this.state.earningvalue.map((data, id) => {
        return {
          ...data,
          earningvalue: Math.round(ear[id] * 100) / 100
        };
      });
      this.setState({
        earningvalue: earnings
      });
      return;
    }
    let data = await axios.post(fetch.url + "get_weekly_data", {
      ref_eth: this.state.eth_addr,
      affiliate_id: this.state.affiliate_id,
      week: wk
    });
    console.log(data.data);
    var fields = [],
      i = 0;

    if (data.data.week_fields) {
      fields = data.data.week_fields;
      earnings = this.state.earningvalue.map((data, id) => {
        if (i < fields.length) {
          i++;
          return {
            ...data,
            earningname: fields[i - 1].name ? fields[i - 1].name : "",
            earningvalue:
              fields[i - 1].value1.length > 0 ? fields[i - 1].value1 : "0.00",
            dollar: fields[i - 1].value2 ? fields[i - 1].value2 : "US Dollars"
          };
        } else {
          return {
            ...data,
            earningvalue: "0.00"
          };
        }
      });
    } else {
      earnings = this.state.earningvalue.map((data, id) => {
        return {
          ...data,
          earningvalue: "0.00"
        };
      });
    }
    this.setState({
      earningsheader: `Week ${wk}`,
      earningvalue: earnings
    });
    console.log(earnings);
  };

  monthly = async (m1, m2, m) => {
    console.log("Yo!!!......from context-Month");
    await this.setState({
      mdtr: 0,
      mitr: 0,
      mdtv: 0,
      mitv: 0
    });
    var earnings = this.state.earningvalue.map((data, id) => {
      return {
        ...data,
        earningvalue: "..."
      };
    });
    if (!this.state.selection) {
      this.dtarr = [];
      this.diarr = [];
      for (var i = m1; i <= m2; i++) {
        await this.Txn_direct(i);
        await this.Txn_indirect(i);
        await this.setState({
          earningsheader: `${m}`
        });
        if (i === m2) {
          for (i = 0; i < 10000000; i++) {
            if (i === 9999) {
              await this.setState({
                earningsheader: `${m}`
              });
              // console.log(
              //   this.state.mdtr,
              //   this.state.mitr,
              //   this.state.mdtv,
              //   this.state.mitv
              // );
              var ear = [];
              ear.push(this.state.mdtv);
              ear.push(this.state.mitv);
              ear.push(this.state.mdtv + this.state.mitv);
              ear.push(this.state.mdtr);
              ear.push(this.state.mitr);
              ear.push(this.state.mdtr + this.state.mitr);
              earnings = this.state.earningvalue.map((data, id) => {
                return {
                  ...data,
                  earningvalue: Math.round(ear[id] * 100) / 100
                };
              });
              this.setState({
                earningvalue: earnings
              });
              break;
            }
          }
          break;
        }
      }
      return;
    }
    this.setState({
      earningsheader: `${m}`,
      earningvalue: earnings
    });
    var data = [];
    var values = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let a;
    for (var j1 = m1; j1 <= m2; j1++) {
      a = await axios.post(fetch.url + "get_weekly_data", {
        ref_eth: this.state.eth_addr,
        affiliate_id: this.state.affiliate_id,
        week: j1
      });
      if (a.data.week_fields) {
        data.push(a.data.week_fields);
      }
      if (j1 === m2) {
        console.log(data);
        // console.log(a.data.week_fields);
        if (data.length > 0) {
          for (var i = 0; i < data.length; i++) {
            for (var j = 0; j < data[i].length; j++) {
              parseInt(data[i][j].value1.replace(",", ""));
              values[j] =
                values[j] + parseFloat(data[i][j].value1.replace(",", ""));
            }
            if (i === data.length - 1) {
              console.log(values);
              earnings = this.state.earningvalue.map((data, id) => {
                return {
                  ...data,
                  earningvalue: Math.round(values[id] * 100) / 100
                };
              });
              this.setState({
                earningvalue: earnings
              });
            }
          }
        } else {
          earnings = this.state.earningvalue.map((data, id) => {
            return {
              ...data,
              earningvalue: "0.00"
            };
          });
          this.setState({
            earningvalue: earnings
          });
          break;
        }
      }
    }
  };

  setPortfolioBalance = async value => {
    console.log("portfolio context 1", value);
    let val = value.replace("$", "");
    val = val.replace(/,/g, "");
    val = parseFloat(val);
    console.log("portfolio context val 2", val, this.state.account_balance);
    let total_portfolio_bal = 0.0;
    try {
      total_portfolio_bal = this.formatter.format(
        val + parseFloat(this.state.account_balance)
      );
    } catch (e) {
      console.log("total_portfolio_balance e", e, total_portfolio_bal);
    }
    console.log("total_portfolio_balance 3", total_portfolio_bal, value);
    await this.setState(
      {
        portfolio_balance: value,
        total_portfolio_balance: total_portfolio_bal
      },
      () => {
        console.log(
          "portfolio context final 4",
          this.state.total_portfolio_balance,
          total_portfolio_bal
        );
      }
    );
  };

  setStake_allowed = async () => {
    this.setState({ stake_allowed: true });
  };

  update_sellPercentage = per => {
    this.setState({ sell_percentage: parseFloat(per) });
  };

  UpdateBrokerState = async value => {
    value = parseFloat(value) + parseFloat(this.state.request_staked_value);
    if (parseFloat(value) >= 450) {
      this.setState({ is_broker: true });
    }
    return;
  };

  updateInteracMessage = async msg => {
    this.setState({ interac_message: msg });
  };

  updateTOTPMfa = async val => {
    console.log("came into Totp");
    this.setState({ totp_mfa_setup: val, totp1: !val });
  };

  updateBrandLogo = link => {
    this.setState({ brand_logo_link: link });
  };

  setCoinMarkets = async coinmarket => {
    console.log("In set COin MArkjets");

    await this.setState({ coinmarket: coinmarket });
    console.log("In set COin MArkjets", this.state.coinmarket);
  };

  updateCashService = async val => {
    console.log("Cash service", val);
    await this.setState({ cash_service: val });
    this.setState({
      cash_service_changed: !this.state.cash_service_changed
    });
  };

  formatter_num = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  });


  loginNav=()=>
  {
    this.setState(
      {
        isLoginModalOpen:true
      }
    )
  }
  Reset=()=>
  {
    this.setState({
      isRestLoginModalOpen:true,
      isLoginModalOpen:false
    })
  }
  ResetClose=()=>
  {
    this.setState({
      isRestLoginModalOpen:false
    })
  }
  Resetbacklog=()=>
  {
    this.setState(
      {
        isLoginModalOpen:true,
        isRestLoginModalOpen:false,
      }
    )
  }


  loginNavclose=()=>
  {
    this.setState(
      {
        isLoginModalOpen:false
      }
    )
  }


  handleImageChange= async(e)=> {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = async () => {
     await this.setState({
        file: file,
        imagePreviewUrl: reader.result,
        imagestatus:true
      });
    }
    // if(this.state.imagePreviewUrl!=='')
    // {
    //   await this.setState({
    //     imagestatus:true
    //   })
    // }

    reader.readAsDataURL(file)
  }


  handleStoreImageChange= async(e)=> {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = async () => {
     await this.setState({
        file: file,
        imagePreviewUrlStore: reader.result,
        storestep1:false,
        storestep3:true,
        storestep2:false,
      });
    }
    // if(this.state.imagePreviewUrl!=='')
    // {
    //   await this.setState({
    //     imagestatus:true
    //   })
    // }

    reader.readAsDataURL(file)
  }

  handleChangeHorizontal = async value => {
    

        await this.setState({
          horizontal: value
      
        })
      
      if(value>80)
      {
        await this.setState({
          pstep2:true,
          pstep1:false,
          pstep3:false,
        })
      }
 
      

    }

    SellingDeatil=async(item)=>

    {
await this.setState(
  {
    seelingmodeid:item.id,
    seelingmodename:item.name,
    pstep2:false,
    pstep1:false,
    pstep3:true,

  }
)
    }
    
    SellingDeatil3=async(item)=>

    {
await this.setState(
  {
    seelingplatfrom:item.name,
  pstep2:false,
    pstep1:false,
    pstep3:false,
    pstep4:true,

  }
)
    }


    selectCountry= (val) =>{
      this.setState({ country: val });
    }
   
    selectRegion =(val)=> {
      this.setState({ region: val });
    }

    plandetailfunc=async(item)=>
    {
     await this.setState({
      planchooseName:item.name,
      planchooseId:item.id
     }) 
if(this.state.planchooseName==="One Time")

{
await this.setState({
  oneTime:true,
})
}

if(this.state.planchooseName==="Subsciption")

{
await this.setState({
  oneTime:false,
})
}

    }

    StoreFunctionVis=(e)=>
    {
      e.preventDefault()
      this.setState({
        storestep1:false,
        storestep2:true,
      })
    }

    handleChangeHorizontalStore = async value => {
    

      await this.setState({
        horizontals: value
    
      })
    
    if(value>90)
    {
      await this.setState({
        storestep1:false,
        storestep2:false,
        storestep3:false,
        storestep4:true,
      })
    }

    

  }


  storequestion=async(item)=>
  {
   await this.setState({
  storeans:item,
})
  }

  
  storequestionlife=async(item)=>
  {
   await this.setState({
  storelife:item,
})
  }
  storequestionMemberShip=async(item)=>
  {
   await this.setState({
  storeMembership:item,
})
  }




    signupfun = async () => {


    let body={
      email: this.state.login_email,
      token: this.state.affiliate_id,
      product_code: uuidv4(),
      product_name: this.state.storeName,
      product_icon: this.state.imagePreviewUrlStore,
      revenue_account: "lindsay_scheel@yahoo.com",
      client_app: "Atlas",
      product_category_code: uuidv4(),
      sub_category_code: "sub_category_code", // optional, send only if sub category is there
      custom_product_label: "custom_label",
      points_creation: true,
      sub_text:this.state.producttag,
      full_description: this.state.productdescription,
      first_purchase: {
          price: this.state.productcurrency,
          coin: "USD"
      },
      billing_cycle: {
          lifetime: {
              price: this.state.productcurrency,
              coin: "USD"
           },
          monthly: {
              price: 99.99,
              coin: "USD"
          }
      },
      grace_period: 14,
      first_retry: 7,
      second_retry: 7,
      staking_allowed: false
   }
    await axios.post("https://comms.globalxchange.com/gxb/product/create", {
      ...body
    });
    console.log("body",body)
    
  };


bussinesssubmit= async()=>
{
  // let sad=localStorage.getItem("token")
 let body= {
    email: this.state.login_email,
    token: localStorage.getItem("token"), 
    business_name: this.state.bussinessname,
    short_name: "short_name", // this name will be tagged to the users
    tagline: "tagline",
    logo: this.state.imagePreviewUrl,
    colors: ["red","green"], // array
    business_category: "dont know",
    business_address: this.state.bussinessAddress,
    external_domain: "empty",
    business_email: this.state.login_email,
    business_number: uuidv4(),
    product_delivery: "online",
    gallery: ["viper"]
 }
 await axios.post("https://comms.globalxchange.com/gxb/product/business/app/define", {
  ...body
});
console.log("bussiness",body)
}
 handleAntTabVsa=(key)=>
{
console.log("sadasdasdasdasdasdad",key)
}



answerdrop=async(item)=>
{
 
await this.setState({
answerdropname:item,
setdropshowtable:!this.state.setdropshowtable,
})
if(this.state.answerdropname==="Create A Product")
{
  await this.setState(
    {
      vsatab:"Store",
      vsatabsub:'Orders'
    }
  )
}

if(this.state.answerdropname==="Controller Dashboard")
{
  await this.setState(
    {
      vsatab:"Controller",
      ControllerDashboard:true,
      BusinessControllerDashboard:false,
    }
  )
}
if(this.state.answerdropname==="Setup Business Profile")
{
  await this.setState(
    {
      vsatab:"Controller",
      ControllerDashboard:false,
      BusinessControllerDashboard:true,
    }
  )
}
// vsatab
}

 storehandleAntTab=async(key)=>
{
  await this.setState(
    {
      vsatab:key,
    }
  )
}


substorehandleAntTab=async(key)=>
{
  
    await this.setState(
      {
        vsatabsub:key,
      }
    )
 

}


atlastabhandleAntTab=async(key)=>
{
  if(this.state.atlasbuttonclick===true)
  {
  await this.setState(
    {
      atlastab:key,
    }
  )
}
}

cardfunction=async(key)=>
{
  await this.setState(
    {
  
      atlasnamecrm:key.name,
      atlasbuttonclick:true,
      atlastab:"Analytics"
    }
  )
  // if(this.state.atlasnamecrm==="atlasapp")
  // {
  //   await this.setState(
  //     {
  //       atlastab:"Analytics",
  //       atlasbuttonclick:true,
  //     }
  //   )
  // }
}

setdropshow=()=>
{
  this.setState(
    {
      setdropshowtable:!this.state.setdropshowtable,
    }
  )
}

atlasdropdown=()=>
{
  this.setState({
    atlasNavshow:!this.state.atlasNavshow,
  })
}


handleatlasChangefliterAtlas = async e => {
  if(this.state.SelectedAtlasDropname==="All Data")
  {
  
    let value =e.target.value;
  
  
    let n1= atlasAll.filter((user)=>{
           return (user.name).toUpperCase().includes((value).toUpperCase());
      
     })
     await this.setState({
      atlasnavigationdata: n1,
     
    });
    
  }


  
  if(this.state.SelectedAtlasDropname==="BrokerApp's CRM")
  {
  
    let value =e.target.value;
  
  
    let n1= BrokerCmrall.filter((user)=>{
           return (user.name).toUpperCase().includes((value).toUpperCase());
      
     })
     await this.setState({
      atlasnavigationdata: n1,
     
    });
    
  
  }
  if(this.state.SelectedDropname==="Atlas's CRM")
  {
  
    let value =e.target.value;
  
  
    let n1= AtlasCmrall.filter((user)=>{
           return (user.name).toUpperCase().includes((value).toUpperCase());
      
     })
     await this.setState({
      atlasnavigationdata: n1,
     
    });
    
  }
  if(this.state.SelectedDropname==="Tv")
  {
  
    let value =e.target.value;
  
  
    let n1= AtlasTVall.filter((user)=>{
           return (user.name).toUpperCase().includes((value).toUpperCase());
      
     })
     await this.setState({
      atlasnavigationdata: n1,
     
    });
    
  }
  if(this.state.SelectedDropname==="Share")
  {
  
    let value =e.target.value;
  
  
    let n1= atlasshareAll.filter((user)=>{
           return (user.name).toUpperCase().includes((value).toUpperCase());
      
     })
     await this.setState({
      atlasnavigationdata: n1,
     
    });
    
  }
  };
  atlasdropfunction=async(item)=>
  {
    await this.setState({
      SelectedAtlasDropname: item,
  
    })

    if( this.state.SelectedAtlasDropname==="Select CRM")
    {
      await this.setState(
        {
          // mainatlastab:"CRM",
          // atlastab:"",
          dropdownlistAtlass:seletctcmr,
          atlasbuttonclick:false,
          atlasnavigationdata:"",
          
  
        })
      }

if(this.state.SelectedAtlasDropname==="All Data")
{
  this.setState(
    {
      atlasnavigationdata:atlasshareAll,
      // atlastoggle:true,
    }
  )
}
if(this.state.SelectedAtlasDropname==="Share")
{
  this.setState(
    {
      atlasnavigationdata:atlasAll,
      // atlastoggle:true,
    }
  )
}
if(this.state.SelectedAtlasDropname==="BrokerApp's CRM")
{
  this.setState(
    {
      atlasnavigationdata:BrokerCmrall,
      // atlastoggle:true,
    }
  )
}
if(this.state.SelectedAtlasDropname==="Atlas's CRM")
{
  this.setState(
    {
      atlasnavigationdata:AtlasCmrall,
      // atlastoggle:true,
    }
  )
}
if(this.state.SelectedAtlasDropname==="Tv")
{
  this.setState(
    {
      atlasnavigationdata:AtlasTVall,
      // atlastoggle:true,
    }
  )
}


  }

Crmfunctioncall=async()=>
{
  await axios
  .get(
    `https://comms.globalxchange.com/gxb/user/crms/stats/get?email=${localStorage.getItem(
      "user_account"
    )}`
  )
  .then(res => {
    console.log("firstaxiosss", res.data.crms);
    
    this.setState(
      {
        CrmData:res.data.crms,
        CrmDataName:res.data.crms[0].name
      }
    )
    this.fliternavdrop()
  });

}

 fliternavdrop=async()=>
{ 
   let s="Atlas's CRM"
if(this.state.CrmDataName==="GXBroker"){
        let n =Altasdrop.filter(item=>
           {
            
               return item.name!==s
           }
           
           )
         await this.setState({
dropdownlistAtlass:n
          })
          }
   
}
  subatlashandleAntTab=async(key)=>
  {
    
      await this.setState(
        {
          mainatlastab:key,
        }
      )
   if(this.state.mainatlastab==="Share")
   {
    await this.Crmfunctioncall()
   }
  
  }


  listingitemsatlas=async(item)=>
  {

await this.setState(
  {
    atlasdrpclick:item.name,

    atlastoggle:false,
  })
  
  if( this.state.atlasdrpclick==="Choose CRM")
  {
    await this.setState(
      {
        mainatlastab:"CRM",
        atlastab:"",

        atlasbuttonclick:false,
        

      })
  }
  if(this.state.atlasdrpclick==="Get Affiliate Link")
  {
    await this.setState(
      {
        mainatlastab:"Share"
      })
  }
  }
  atladrphidefunction=()=>
  {
    this.setState(
      {
        atlastoggle:!this.state.atlastoggle
      })
  }
  atladrphidefunction1=()=>
  {
    this.setState(
      {
        atlastoggle:false
      })
  }

  oustsideclickfun=()=>
  {
    this.setState(
      {
        atlasNavshow:false
      }
    )
  }

  fucntogglecommer=(item)=>
  {
    this.setState({
      SwithEcommer:item
    })
  }
  render() {
    console.log("gx_business_account",this.state.dropdownlistAtlass)
    return (
      <>
        <brokerage.Provider
          value={{
            fucntogglecommer:this.fucntogglecommer,
            oustsideclickfun:this.oustsideclickfun,
            CrmDataName:this.CrmDataName,
            dropdownlistAtlass:this.dropdownlistAtlass,
            Crmfunctioncall:this.Crmfunctioncall,
            subatlashandleAntTab:this.subatlashandleAntTab,
            atladrphidefunction1:this.atladrphidefunction1,
            atladrphidefunction:this.atladrphidefunction,
            listingitemsatlas:this.listingitemsatlas,
            handleatlasChangefliterAtlas:this.handleatlasChangefliterAtlas,
            atlasdropfunction:this.atlasdropfunction,
            atlasdropdown:this.atlasdropdown,
            atlastab:this.atlastab,
            mainatlastab:this.mainatlastab,
            cardfunction:this.cardfunction,
            atlastabhandleAntTab:this.atlastabhandleAntTab,
            handleatlasChangefliter:this.handleatlasChangefliter,
            setdropshow:this.setdropshow,
            substorehandleAntTab:this.substorehandleAntTab,
            storehandleAntTab:this.storehandleAntTab,
            answerdrop:this.answerdrop,
            selecteddropdown:this.selecteddropdown,
            handleAntTabVsa:this.handleAntTabVsa,
            bussinesssubmit:this.bussinesssubmit,
            storequestionMemberShip:this.storequestionMemberShip,
            storequestionlife:this.storequestionlife,
            signupfun:this.signupfun,
            storequestion:this.storequestion,
            handleChangeHorizontalStore:this.handleChangeHorizontalStore,
            StoreFunctionVis:this.StoreFunctionVis,
            plandetailfunc:this.plandetailfunc,
            handleStoreImageChange:this.handleStoreImageChange,
            storeChange:this.storeChange,
            SellingDeatil3: this.SellingDeatil3,
            selectCountry: this.selectCountry,
            selectRegion: this.selectRegion,
            SellingDeatil: this.SellingDeatil,
            handleChangeHorizontal: this.handleChangeHorizontal,
            handleImageChange: this.handleImageChange,
            handleatlasChange: this.handleatlasChange,
            ResetClose: this.ResetClose,
            Reset: this.Reset,
            loginNavclose: this.loginNavclose,
            loginNav: this.loginNav,
            Resetbacklog: this.Resetbacklog,
            changeIcedPayment: this.changeIcedPayment,
            buyingFromPulse: this.state.buyingFromPulse,
            setBuyingFromPulse: this.setBuyingFromPulse,
            subscription: this.state.subscription,
            setSubscription: this.setSubscription,
            subscribeResponse: this.state.subscribeResponse,
            setSubscribeResponse: this.setSubscribeResponse,
            changeDisplayGXBroker: this.changeDisplayGXBroker,
            changeDisplayETransfer: this.changeDisplayETransfer,
            changeInChildDrawerId: this.changeInChildDrawerId,
            changeDisplaySendTwo: this.changeDisplaySendTwo,
            changeDisplaySendOne: this.changeDisplaySendOne,
            changeDisplaySend: this.changeDisplaySend,
            changeTypeAdd: this.changeTypeAdd,
            changeMarketPayment: this.changeMarketPayment,
            changeDigitalPaymentTwo: this.changeDigitalPaymentTwo,
            changeDigitalPayment: this.changeDigitalPayment,
            changeFilterDigitalAssetTerm: this.changeFilterDigitalAssetTerm,
            changeFilterDigitalAssetTermMarket: this
              .changeFilterDigitalAssetTermMarket,
            changeFilterDigitalAssetTermDigital: this
              .changeFilterDigitalAssetTermDigital,
            changeFilterDigitalAssetTermDigitalDeposit: this
              .changeFilterDigitalAssetTermDigitalDeposit,
            changeConvertValue: this.changeConvertValue,
            changeDisplay: this.changeDisplay,
            changeChildThree: this.changeChildThree,
            changeWire: this.changeWire,
            changeWireTwo: this.changeWireTwo,
            changeWireThree: this.changeWireThree,
            changeGreaterThan: this.changeGreaterThan,
            ...this.state,
            hodlpassword: this.state.hodlpassword,
            visible: this.state.visible,
            userlinks: this.state.userlink,
            earningsheader: this.state.earningsheader,
            earningvalue: this.state.earningvalue,
            otcbroker: this.state.otcbroker,
            withdraw: this.state.withdraw,
            transaction: this.state.transaction,
            color: this.state.color,
            colorselect: this.handleChangePageLayout,
            brandlogo: this.state.brandlogo,
            fileSelectedHandler: this.handleChangePageLayout,
            affiliate_id: this.state.affiliate_id,
            ref_affiliate: this.state.ref_affiliate,
            username: this.state.username,
            balance: this.state.balance,
            withdraw_balance: this.state.withdraw_balance,
            weekly: this.weekly,
            monthly: this.monthly,
            eth_addr: this.state.eth_addr,
            name: this.state.name,
            my_withdrawals: this.my_withdrawals,
            twa: this.state.twa,
            names: this.state.names,
            Txn_default: this.Txn_default,
            Txn_direct: this.Txn_direct,
            Txn_indirect: this.Txn_indirect,
            affs: this.affs,
            aff_index: this.aff_index,
            gt: this.state.gt,
            dgt: this.state.dgt,
            igt: this.state.igt,
            nvest_rate: this.state.nvest_rate,
            broker_fee: this.state.broker_fee,
            listed_price: this.state.listed_price,
            current_percentage: this.state.current_percentage,
            save_percentage: this.save_percentage,
            otc_txns: this.state.otc_txns,
            otc_aff: this.state.otc_aff,
            otc_names: this.state.otc_names,
            otc_total: this.state.otc_total,
            otc_dt: this.state.otc_dt,
            otc_idt: this.state.otc_idt,
            otc_direct: this.otc_direct,
            otc_indirect: this.otc_indirect,
            otc_dt_total: this.state.otc_dt_total,
            otc_idt_total: this.state.otc_idt_total,
            selection: this.state.selection,
            dtv: this.state.dtv,
            itv: this.state.itv,
            dtr: this.state.dtr,
            itr: this.state.itr,
            txn_ok: this.state.txn_ok,
            uuser_eth: this.state.user_eth,
            user_btc: this.state.user_btc,
            username_msg: this.state.username_msg,
            email: this.state.email,
            admin_email: this.admin_email,
            login_email: this.state.login_email,
            earnings_week: this.earnings_week,
            earnings_month: this.earnings_month,
            logs: this.state.logs,
            spinner: this.state.spinner,
            get_Week_number: this.get_Week_number,
            tbal: this.state.tbal,
            broker_name: this.state.broker_name,
            brand_img: this.state.brand_img,
            brokers: this.state.brokers,
            btc_address: this.state.btc_address,
            ltc_address: this.state.ltc_address,
            doge_address: this.state.doge_address,
            eth_address: this.state.eth_address,
            account_balance: this.state.account_balance,
            portfolio_balance: this.state.portfolio_balance,
            total_portfolio_balance: this.state.total_portfolio_balance,
            setPortfolioBalance: this.setPortfolioBalance,
            mobile: this.state.mobile,
            upline_otc_fee: this.state.upline_otc_fee,
            updateAccountBalance: this.updateAccountBalance,
            broker_address: this.state.broker_address,
            formatter: this.formatter,
            staked_value: this.state.staked_value,
            stake_allowed: this.state.stake_allowed,
            setStake_allowed: this.setStake_allowed,
            sef_coins_earned: this.state.sef_coins_earned,
            sell_percentage: this.state.sell_percentage,
            update_sellPercentage: this.update_sellPercentage,
            is_broker: this.state.is_broker,
            faucet_done: this.state.faucet_done,
            admin_loggedIn: this.state.admin_loggedIn,
            hodlPasswordUpdate: this.hodlPasswordUpdate,
            hodlUpdate: this.hodlUpdate,
            account_not_found: this.state.account_not_found,
            enable_staked_initialized: this.state.enable_staked_initialized,
            request_stake: this.state.request_stake,
            request_staked_value: this.state.request_staked_value,
            UpdateBrokerState: this.UpdateBrokerState,
            totp1: this.state.totp1,
            totp1handleClose: this.totp1handleClose,
            totp1handleShow: this.totp1handleShow,
            interac_message: this.state.interac_message,
            updateInteracMessage: this.updateInteracMessage,
            totp_mfa_setup: this.state.totp_mfa_setup,
            updateTOTPMfa: this.updateTOTPMfa,
            brand_logo_link: this.state.brand_logo_link,
            updateBrandLogo: this.updateBrandLogo,
            handlePulseModal: this.handlePulseModal,
            submitHandler: this.submitHandler,
            coinmarket: this.state.coinmarket,
            setCoinMarkets: this.setCoinMarkets,
            cash_service: this.state.cash_service,
            upline_cash_service: this.state.upline_cash_service,
            cash_btc_available: this.state.cash_btc_available,
            updateCashService: this.updateCashService,
            cash_service_changed: this.state.cash_service_changed,
            fiat_balances: this.state.fiat_balances,
            formatter_num: this.formatter_num,
            mot_account: this.state.mot_account,
            total_withdraw_amount: this.state.total_withdraw_amount,
            payWithSidebarOpen: this.payWithSidebarOpen, //Function To Open Side Bar to Pay
            cancelPayment: this.cancelPayment, //Cancel Payment Transction On Sidebar
            paymentMethodsTabTogle: this.paymentMethodsTabTogle, //Open To See Saved Payment Methods
            paymentMethodsTabOpen: this.state.paymentMethodsTabOpen, //Payment Methods Is Open
            thingsToPurchase: this.state.thingsToPurchase, //Array Of Purchase List
            paymentMethodsTab: this.state.paymentMethodsTab, //Tab Index
            cardInpaymentMethodsTab: this.state.cardInpaymentMethodsTab, //Card in Tab Position
            isPuchasePaymentMethods: this.state.isPuchasePaymentMethods, // True If purchase
            callBackIsSuccessPaymentMethodsTab: this.state
              .callBackIsSuccessPaymentMethodsTab, //Call Back Funtion To PassPayment Sidebar the, function(isSuccess:bool,message:text,type:text)
            paymentSuccessTitle: this.state.paymentSuccessTitle, //To Set Title On Payment Success Page
            paymentId: this.state.paymentId, //PaymentId Of Purhase Side Bar to Pay
            merchantName: this.state.merchantName, //Merchant Name On Side Bar to Pay
            isBackendPayment: this.state.isBackendPayment,
            tradingFloorView: this.state.tradingFloorView,
            setTradingFloorView: this.setTradingFloorView,
            setCurrenciesList: this.setCurrenciesList,
            changeSubscription: this.changeSubscription,
            SwithEcommer:this.state.SwithEcommer,
            gx_business_account: this.state.gx_business_account
          }}
        >
          {this.props.children}
        </brokerage.Provider>
      </>
    );
  }
}

export const Consumer = brokerage.Consumer;
