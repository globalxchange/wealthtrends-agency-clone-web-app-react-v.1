import React, {Component} from 'react';

export const displayContext = React.createContext()

export default class PaymentContextApi extends Component {
    state = {
        displayAdd: "false",
    }
    changeDisplay = () =>{
        this.setState({displayAdd: !this.state.displayAdd})
    }
    render() {
        return (
            <div>
            <displayContext.Provider
                value = {{...this.state,displayAdd: this.state.displayAdd,changeDisplay :this.changeDisplay}}

            >
                {this.props.children}
            </displayContext.Provider>
            </div>
        );
    }
}

export const Consumer = displayContext.Consumer;
