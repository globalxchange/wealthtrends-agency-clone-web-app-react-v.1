import React, { useState, useEffect } from "react";
import "./App.scss";
import "./Register.scss";
import { Route, Switch, Redirect } from "react-router-dom";
import Login from "./components/Login/Login";
import Dashboard from "./components/Dashboard/index";
import { AgencyProvider } from "./components/Context/Context";
import TestComponent from "./components/test-component/TestComponent";
import Bokerpage from "./components/common-components/BrokerLandingPage/BrokerLandingPage";
import Contaxtapi from "./components/Brokercontextapi/Contextapi";
import Altasbashboard from "./components/common-components/BrokerSendpage/AltasMainPage";
import Ecommerce from "./components/common-components/E-commerce/EcommerceLandingPage";
import { IcedTerminalProvider } from "./components/Dashboard/Content/main-context/main.context";
import Register from "./components/Register/Register";
import LearnComponent from "./components/Learn/LearnComponent";
function App() {
  const [appInfo, setAppInfo] = React.useState(null);
  const [brokerInfo, setBrokerInfoMain] = React.useState(null);
  const [loginLoading, setLoginLoading] = useState(false);
  const [loading, setLoading] = useState(true);

  React.useEffect(() => {
    localStorage.setItem("mainColor", "#88C920");
    setLoading(false);
  }, []);

  return loading ? null : (
    <React.Fragment>
      <div className="main-wrapper">
        <Switch>
          <Route
            exact
            path="/learn/:type/:id"
            render={() => <LearnComponent />}
          />
          <Route
            exact
            path="/register"
            render={(props) => (
              <Register
                {...props}
                setBrokerInfoMain={setBrokerInfoMain}
                brokerInfo={brokerInfo}
                appInfo={appInfo}
              />
            )}
          />
          <Route
            path="/dashboard"
            render={() => (
              <Contaxtapi>
                <IcedTerminalProvider>
                  {" "}
                  <AgencyProvider>
                    {" "}
                    <Dashboard />
                  </AgencyProvider>
                </IcedTerminalProvider>
              </Contaxtapi>
            )}
          />
          <Route
            path="/login"
            render={(props) => (
              <Login
                {...props}
                setBrokerInfoMain={setBrokerInfoMain}
                setAppInfo={setAppInfo}
                appInfo={appInfo}
              />
            )}
          />
          <Route
            exact
            path="/brokerbashboard"
            render={() => (
              <Contaxtapi>
                <Altasbashboard />
              </Contaxtapi>
            )}
          />
          <Route
            exact
            path="/brokerapp"
            render={() => (
              <Contaxtapi>
                <Bokerpage />
              </Contaxtapi>
            )}
          />

          <Route
            exact
            path="/marketplace"
            render={() => (
              <Contaxtapi>
                <Ecommerce />
              </Contaxtapi>
            )}
          />
          {/* <Route path ="/test" component={()=><TestComponent />} /> */}
          <Route path="/">
            <Redirect to="/login" />
          </Route>
        </Switch>
      </div>
    </React.Fragment>
  );
}

export default App;
