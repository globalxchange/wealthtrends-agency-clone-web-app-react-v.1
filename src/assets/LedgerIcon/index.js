import USDIcon from "./usdIcon.png";
import EURIcon from "./eurIcon.png";
import GBPIcon from "./gbpIcon.png";
import CADIcon from "./cadIcon.png";
import BTCIcon from "./btcIcon.png";
import ETHIcon from "./ethIcon.png";
import INRIcon from './INRIcon.png';
import AEDIcon from './aedIcon.png';
import AUDIcon from './ausIcon.png';
import CYNIcon from './cynIcon.png';
import JPYIcon from './jpyIcon.png';

export { USDIcon, EURIcon, GBPIcon, CADIcon, BTCIcon, ETHIcon, INRIcon,AEDIcon,AUDIcon, CYNIcon, JPYIcon };
