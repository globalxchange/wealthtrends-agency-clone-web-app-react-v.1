import Axios from 'axios'

export function fetchAllCoins() {
    let email = localStorage.getItem('userEmail');
    return Axios.get(`https://comms.globalxchange.com/coin/vault/get/all/coins`)


}
export function getTotalForexValue() {
    let email = localStorage.getItem('userEmail');
    return Axios.get(`https://comms.globalxchange.com/coin/fiat/fiat_balances?email=${email}`)
}
export const getAddedFriends = (app) => {
    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail');
    let config = {
        method: 'get',
        url: `https://friends.apimachine.com/friend?appcode=${app}`,
        headers: {
            'token': token,
            'email': email,
        }
    }
    return Axios(config);
}
export const getFriendsDetail = (app, friend) => {
    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail');

    let config = {
        method: 'get',
        url: `https://friends.apimachine.com/friend/friendDetails?appcode=${app}&friendEmail=${friend}`,
        headers: {
            'token': token,
            'email': email,
        }
    }
    return Axios(config);

}
export const IsGXUser = (email) => {
    let config = {
        method: 'get',
        // url: `https://friends.apimachine.com/friend/isGxUser?email=${email}`
        url: `https://friends.apimachine.com/user/isUserRegistered?email=${email}`
    }
    return Axios(config)

}
export const getUserDetails = () => {
    let email = localStorage.getItem('userEmail');
    return Axios.get(`https://comms.globalxchange.com/user/details/get?email=${email}`)
};

export const getTCContent = () => {
    return Axios.get('https://storeapi.apimachine.com/dynamic/dgpayment/policies?key=51c1c499-2ab1-46b7-aab2-d073a7d16728')
}
export const getIcedData = () => {
    let email = localStorage.getItem('userEmail');
    let config = {
        method: 'get',
        url: `https://comms.globalxchange.com/coin/vault/earnings/ice/gettxns?email=${email}`
    }
    return Axios(config)
}
export const getMarketData = () => {
    let email = localStorage.getItem('userEmail');
    let config = {
        method: 'get',
        url: `https://comms.globalxchange.com/coin/vault/earnings/data?email=${email}`
    }
    return Axios(config)

}
export const getCarouselData = () => {
    let config = {
        method: 'get',
        url: `https://storeapi.apimachine.com/dynamic/MyCryptoBrand/Carousel?key=03572f1a-6d30-4083-b303-54e3356fb9df`
    }
    return Axios(config)
}

export const getAppLinks = () => {
    let config = {
        method: 'get',
        url: `https://storeapi.apimachine.com/dynamic/Globalxchangetoken/applinks?key=4c69ba17-af5c-4a5c-a495-9a762aba1142`
    }
    return Axios(config)
}

export const getAllCoinsAvailable = () => {
    return Axios.get(`https://terminal.apimachine.com/availableCoins`)
};

export const exchangeData = () => {
    return Axios.get(`https://storeapi.apimachine.com/dynamic/iceprotocol/exchangeList?key=62816d7e-e570-46a2-8c9d-0c1cc97f7bf8`);
};

export const getMarketValue = (exchange, first, second) => {
    return Axios.get(`https://terminal.apimachine.com/marketCoinPrice${exchange}/${first}/${second}`)
};

export const getAvailablePairs = () => {
    return Axios.get('https://terminal.apimachine.com/availablePairs');
};

export const cryptoUSD = () => {
    return Axios.get(`https://comms.globalxchange.com/coin/getCmcPrices?convert=USD`)
};
export const getMarketCoins = () => {
    return Axios.get(`https://comms.globalxchange.com/coin/vault/get/all/coins`)
}
export const getGXAssetCoins = (app) => {
    return Axios.get(`https://comms.globalxchange.com/coin/iced/get/liquid/interest?app_code=${app}`)
}
export const getEarningBalance = (app) => {
    let email = localStorage.getItem('userEmail');
    return Axios.get(`https://comms.globalxchange.com/coin/vault/service/user/app/interest/balances/get?app_code=${app}&email=${email}`)
}
export const getContracts = () => {
    return Axios.get(`https://comms.globalxchange.com/coin/iced/contract/get`)
}

export const convertToUSD = () => {
    return Axios.get(`https://comms.globalxchange.com/coin/getCmcPrices?convert=USD`)
}

export const getRegisteredApp = () => {
    let email = localStorage.getItem('userEmail');
    return Axios.get(`https://comms.globalxchange.com/gxb/apps/registered/user?email=${email}`)
}

export const getMainInterest = (type) => {
    let email = localStorage.getItem('userEmail');
    return Axios.get(`https://comms.globalxchange.com/coin/vault/service/user/app/interest/balances/get?email=${email}&app_code=${type}`)
}
export const getEarningLogs = (app, coin) => {
    let email = localStorage.getItem('userEmail');
    return Axios.get(`https://comms.globalxchange.com/coin/vault/service/user/app/interest/logs/get?email=${email}&app_code=${app}&coin=${coin}`)
}
export const getMobileAppLink = () => {
    return Axios.get(`https://storeapi.apimachine.com/dynamic/Globalxchangetoken/applinks?key=4c69ba17-af5c-4a5c-a495-9a762aba1142`)
}
export const getConversionRate = (coin) => {
    return Axios.get(`https://comms.globalxchange.com/coin/getCmcPrices?convert=${coin}`, {
    })
}

export const tradeForBaseAsset = () => {
    let app = localStorage.getItem('appCode');
    return Axios.get(`https://comms.globalxchange.com/coin/vault/service/payment/stats/get?paymentMethod=vault&select_type=instant&app_code=${app}`)
}
export const tradeForQuoteAsset = (coin) => {
    let app = localStorage.getItem('appCode');
    return Axios.get(`https://comms.globalxchange.com/coin/vault/service/payment/stats/get?paymentMethod=vault&select_type=instant&to_currency=${coin}&app_code=${app}`)
}
export const tradeForExchangeList = (base, quote) => {
    let app = localStorage.getItem('appCode');
    return Axios.get(`https://comms.globalxchange.com/coin/vault/service/payment/stats/get?paymentMethod=vault&select_type=instant&to_currency=${base}&from_currency=${quote}&app_code=${app}`)
}
export const tradeAfterExchangeList = (base, quote, bank) => {
    let app = localStorage.getItem('appCode');
    return Axios.get(`https://comms.globalxchange.com/coin/vault/service/payment/stats/get?paymentMethod=vault&select_type=instant&to_currency=${base}&from_currency=${quote}&app_code=${app}&banker=${bank}`)
}
export const getUserAccountDetails = (app) => {
    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail');

    let config = {
        method: 'get',
        url: `https://friends.apimachine.com/user?appcode=${app}`,
        headers: {
            'token': token,
            'email': email,
        }
    }
    return Axios(config);
}
export const getAnalyticsValue = (method, coin) => {
    return Axios(`https://comms.globalxchange.com/coin/vault/service/payment/path/averages/get?paymentMethod=${method}&to_currency=${coin}`)
}
export const getUserList = (app) => {
    return Axios(`https://comms.globalxchange.com/gxb/apps/users/get?app_code=${app}`)
}
export const fetchOfferingList = () => {
    return Axios(`https://comms.globalxchange.com/coin/iced/admin/get/data`)
}
export const getIcedContracts = () => {
    let email = localStorage.getItem('userEmail');
    return Axios(`https://comms.globalxchange.com/coin/iced/contract/get?email=${email}&status=active`)
}
export const getIcedEarningBalance = () => {
    let email = localStorage.getItem('userEmail');
    return Axios(`https://comms.globalxchange.com/coin/iced/interest/balances/get?email=${email}`)
}
export const friendAppList = (email) => {
    return Axios(`https://comms.globalxchange.com/gxb/apps/registered/user?email=${email}`)

}
export const getBondBalance = () => {
    let email = localStorage.getItem('userEmail');
    return Axios(`https://comms.globalxchange.com/coin/iced/interest/balances/get?email=${email}`)
}
export const getBondsTransactionList = (coin) => {
    let email = localStorage.getItem('userEmail');
    return Axios(`https://comms.globalxchange.com/coin/iced/interest/logs/get?email=${email}&coin=${coin}`)
}
export const getAppOverview = (app) => {
    return Axios(`https://comms.globalxchange.com/gxb/apps/get?app_code=${app}`)

}

export const getSyndicateBondsOffering = (email) => {
    return Axios(`https://comms.globalxchange.com/coin/iced/banker/custom/bond/get?email=${email}`)
}

export const getIcedContractAsPerStatus = (status, coin) => {
    let email = localStorage.getItem('userEmail');

    return Axios(`https://comms.globalxchange.com/coin/iced/contract/get?email=${email}&status=${status}&coin=${coin}`)

}
export const getBondEarning = () => {
    let email = localStorage.getItem('userEmail');
    return Axios(`https://comms.globalxchange.com/coin/iced/interest/balances/get?email=${email}`)
}
export const getListForMoney = (app) => {
    let email = localStorage.getItem('userEmail');
    return Axios(`https://comms.globalxchange.com/coin/vault/service/user/app/interest/balances/get?app_code=${app}&email=${email}`)
}
export const ddCloudList = () => {
    return Axios(`https://fxagency.apimachine.com/article/navbar/5f80aca99cb18c06eeef6bb7`)
}
export const fetchPathId = (coin) => {
    return Axios(`https://comms.globalxchange.com/coin/vault/service/payment/paths/get?select_type=withdraw&banker=Global X Change&to_currency=${coin}`)

}
export const getStreamlinedFriendList = (app) => {
    return Axios(`https://comms.globalxchange.com/gxb/apps/streamline/destination/get?app_code=${app}`)
}
export const getCheckbookList = () => {
    let email = localStorage.getItem('userEmail');
    return Axios(`https://comms.globalxchange.com/coin/vault/service/blockcheck/contacts/get?email=${email}`)
}

export const fetchChecklist = () => {
    return Axios(`https://comms.globalxchange.com/coin/vault/service/blockcheck/request/get`)
}
export const fetchAllBankers = () => {
    return Axios(`https://teller2.apimachine.com/admin/allBankers`)
}
export const fetchNativePlatformList = (app) => {
    return Axios(`https://comms.globalxchange.com/gxb/apps/get?app_code=${app}`)
}
export const conversionAPI = (from, to) => {
    return Axios(`https://comms.globalxchange.com/forex/convert?buy=${from}&from=${to}`)
}
export const handleWithdrawalList = (coin) => {
    let email = localStorage.getItem('userEmail');
    return Axios(`https://comms.globalxchange.com/coin/vault/service/path/withdraw/txn/get?email=${email}&coin=${coin}`)
}
export const fetchTransactionDetails = (id) => {
    return Axios(`https://comms.globalxchange.com/coin/vault/service/txn/status/logs?id=${id}`)
}

export const getLearnOptions = (app) => {
    return Axios(`https://fxagency.apimachine.com/publication/appcode/?app_code=${app}`)
}
export const getCategory = (id) => {
    return Axios(`https://fxagency.apimachine.com/category/publication/${id}`)
}
export const getVideoList = (id) => {
    return Axios(`https://fxagency.apimachine.com/video/category/?category=${id}`)
}
export const getArticleList = (id) => {
    return Axios(`https://fxagency.apimachine.com/article/category/?category=${id}`)
}
export const fetchArticleIdId = (id) => {
    return Axios(`https://fxagency.apimachine.com/article/${id}`)

}
export const instaCurrencyVault = (type) => {
    return Axios(`https://comms.globalxchange.com/coin/vault/service/payment/stats/get?select_type=${type}`)
}
export const instaFindOtherCurrency = (type, coin) => {
    return Axios(`https://comms.globalxchange.com/coin/vault/service/payment/stats/get?select_type=${type}&to_currency=${coin}`)
}
export const instaFindCountry = (type, coin, otherCoin) => {
    return Axios(`https://comms.globalxchange.com/coin/vault/service/payment/stats/get?select_type=${type}&to_currency=${coin}&from_currency=${otherCoin}`)
}
export const instaFindMethod = (type, coin, country, toCoin) => {
    return Axios(`https://comms.globalxchange.com/coin/vault/service/payment/stats/get?select_type=${type}&to_currency=${coin}&country=${country}&from_currency=${toCoin}`)
}
export const instaFindBanker = (type, coin, country, toCoin, code) => {
    return Axios(`https://comms.globalxchange.com/coin/vault/service/payment/stats/get?select_type=${type}&to_currency=${coin}&country=${country}&from_currency=${toCoin}&paymentMethod=${code}`)
}
export const instaReadyToExecute = (type, coin, country, toCoin, code, banker) => {
    return Axios(`https://comms.globalxchange.com/coin/vault/service/payment/stats/get?select_type=${type}&to_currency=${coin}&country=${country}&from_currency=${toCoin}&paymentMethod=${code}&banker=${banker}`)

}
export const instaFetchPaths = (id) => {
    return Axios(`https://comms.globalxchange.com/coin/vault/service/payment/paths/get?path_id=${id}`)
}
export const getInstitutesList = () => {
    return Axios(`https://accountingtool.apimachine.com/getlist-of-institutes`)
}
export const getTransactionDepositList = (type, coin) => {
    let email = localStorage.getItem('userEmail');
    let app = localStorage.getItem('appCode');
    return Axios(`https://comms.globalxchange.com/coin/vault/service/${type}/txns/get?email=${email}&app_code=${app}&coin=${coin}`)
}
export const moreAboutApp = () => {
    let appCode = localStorage.getItem('appCode');
    return Axios(`https://comms.globalxchange.com/gxb/apps/get?app_code=${appCode}`)
}
export const getListOfUsernames = () => {
    return Axios(`https://comms.globalxchange.com/listUsernames`)

}
export const fetchValuesOfCoin = (coin) => {
    let email = localStorage.getItem('userEmail');
    return Axios(`https://nitrogen.apimachine.com/userBalanceNativeValue?email=${email}&coin=${coin.toLowerCase()}`)
}

export const getUploadedFile = (id) => {
    return Axios(`https://comms.globalxchange.com/coin/vault/service/get/user/file/uploads?id=${id}`)

}
export const fetchInvestType = () => {
    return Axios(`https://comms.globalxchange.com/coin/investment/types/get`)
}

export const fetchInvestmentPaths = (app) => {
    return Axios(`https://comms.globalxchange.com/coin/investment/path/get?app_codes=${app}`)
}
export const paymentPathForInvestmentType = (app, code) => {
    return Axios(`https://comms.globalxchange.com/coin/investment/path/get?app_codes=${app}&investmentType=${code}`)

}
export const getFiatConversionValue = (coin) => {
    return Axios(`https://api.ratesapi.io/api/latest?base=${coin}`)
}
export const getCurrentInterestPayment = () => {
    let email = localStorage.getItem('userEmail');
    return Axios(`https://comms.globalxchange.com/coin/iced/get/user/interest/payout/destination?email=${email}`)
}

export const getAllAppPresent = () => {
    return Axios(`https://comms.globalxchange.com/gxb/apps/get`)
}
export const conversionRateWIthNoFees = () => {
    return Axios(`https://comms.globalxchange.com/coin/getCmcprices?convert=USD&noSpread=true`)
}

export const getDataForInvestmentPath = (id) => {
    return Axios(`https://comms.globalxchange.com/coin/investment/path/get?path_id=${id}`)
}

// https://nitrogen.apimachine.com/userBalanceNativeValue?email=shorupan@gmail.com&coin=btc



export const getAllContentUnderNavbar = (id) => {
    return Axios(`https://fxagency.apimachine.com/navbar/content/${id}`)
}

export const getAllAds = (email, app) => {
    return Axios(`https://fxagency.apimachine.com/navbar/filter/${email}?app_code=${app}`)
}
export const getBannerAds = (app, email, name) => {
    return Axios(`https://fxagency.apimachine.com/navbar/filter?app_code=${app}&email=${email}&name=${name}`)

}
export const getUserDetailsByUsername = (user) => {
    return Axios(`https://comms.globalxchange.com/user/details/get?username=${user}`)

}

export const getUserDetailsByEmail = (user) => {
    return Axios(`https://comms.globalxchange.com/user/details/get?email=${user}`)

}
export const getListOfEmail = (id) => {
    let config = {
        method: 'get',
        url: `https://api.mailslurp.com/emails?inboxId=${id}&page=0&size=10&sort=ASC&unreadOnly=false`,
        headers: {
            'x-api-key': '86cee2f39d56b3b5a6b2e4c827cc1382d1be6bad16a9d35cd0e659ef9272d02c'
        }
    }
    return Axios(config);

}
export const getContentOfEmail = (id) => {
    let config = {
        method: 'get',
        url: `https://api.mailslurp.com/emails/${id}?decode=false`,
        headers: {
            'x-api-key': '86cee2f39d56b3b5a6b2e4c827cc1382d1be6bad16a9d35cd0e659ef9272d02c'
        }
    }
    return Axios(config);
}
export const getLatestMessages = (id) => {

    let config = {
        method: 'get',
        url: `https://api.mailslurp.com/waitForLatestEmail?inboxId=${id}&timeout=10000&unreadOnly=false`,
        headers: {
            'x-api-key': '86cee2f39d56b3b5a6b2e4c827cc1382d1be6bad16a9d35cd0e659ef9272d02c'
        }
    }
    return Axios(config);

}
export const getBondLevelTwoData = (status,coin) =>{
    let email = localStorage.getItem('userEmail');

    return Axios(`https://comms.globalxchange.com/coin/iced/contract/get?email=${email}&status=${status}&coin=${coin}`)

}
export const getColorCodeByApp = (app) => {
    return Axios(`https://comms.globalxchange.com/gxb/apps/get/colors?app_code=${app}`)
}
export const getColorDetails = () => {
    let app = localStorage.getItem('appCode');
    return Axios(`https://comms.globalxchange.com/gxb/apps/get/colors?app_code=${app}`)
}
export const checkForCustomPathId = (base, quote) => {
    let appCode = localStorage.getItem('appCode');
    return Axios(`https://comms.globalxchange.com/coin/vault/service/payment/paths/get?from_currency=${quote}&to_currency=${base}&select_type=instant&app_code=${appCode}`)
}

export const defaultCustomId = (base, quote) => {
    return Axios(`https://comms.globalxchange.com/coin/vault/service/payment/paths/get?from_currency=${quote}&to_currency=${base}&select_type=instant`)
}

export const getEarningsDetails = () => {
    let email = localStorage.getItem('userEmail');
    return Axios(`https://comms.globalxchange.com/coin/vault/service/users/holdings/data/get?email=${email}`)
}
export const getCoinDetails = (coin) =>{
    return Axios(`https://comms.globalxchange.com/coin/vault/get/all/coins?include=${coin}`)
}
export const buyingCoinsList = (coin) =>{
    let appCode = localStorage.getItem('appCode');
    return Axios(`https://comms.globalxchange.com/coin/vault/service/payment/stats/get?select_type=instant&paymentMethod=vault&app_code=${appCode}&from_currency=${coin}`)
}
export const getVideoInfo = (id) => {
  return Axios(`https://fxagency.apimachine.com/video/video_id/${id}`);
};
