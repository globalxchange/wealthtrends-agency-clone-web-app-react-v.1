import axios from 'axios'
import Axios from 'axios';
import jwt from 'jsonwebtoken'
import {v4 as uuidv4} from 'uuid'
export const login =(email, password) =>{
    console.log("login 1", email, password)
    return axios.post(`https://gxauth.apimachine.com/gx/user/login`,{
        email: email, password: password
    })
};

export const authenticate = () =>{
    let email = localStorage.getItem('userEmail');
    let token = localStorage.getItem('idToken');

    return axios.post(`https://comms.globalxchange.com/coin/verifyToken`,
        {email: email, token: token})
};

export const getVSAId = () =>{
    let email = localStorage.getItem('userEmail');
    return axios.post(`https://comms.globalxchange.com/gxb/apps/register/user`,
        {email: email, app_code: "VSA"}
    )
}
export const getUserBalances = (id, app) =>{
    return axios.post(`https://comms.globalxchange.com/coin/vault/service/balances/get`,
        {profile_id: id, app_code: app}
    )
}

export const getForexTransaction = (coin) =>{
    let email = localStorage.getItem('userEmail');
    return axios.post('https://comms.globalxchange.com/coin/fiat/get_requests', {
        email: email,
        coin: coin
    })
}
export const getCryptoTransaction = (coin) =>{
    let email = localStorage.getItem('userEmail');
    return axios.post('https://comms.globalxchange.com/coin/transfer/get_transactions', {
        email: email,
        coin: coin
    })
}
export const getForexCryptoTransaction = (app, profile_id, coin)=>{
    return axios.post('https://comms.globalxchange.com/coin/vault/service/txns/get',{
        app_code: app,
        profile_id: profile_id,
        coin: coin
    })

}
export const getTotalCryptoValue = () =>{
    let email = localStorage.getItem('userEmail');
    return axios.post('https://comms.globalxchange.com/coin/get_all_balances', {
        email: email
    })

}
export const addFriend =(obj)=>{
    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail');
    console.log("before adding a friend", obj)
    // return axios.post('https://friends.apimachine.com/friend/addFriend',{
    //     ...obj
    // })

    
    let config = {
        method: 'post',
        url: `https://friends.apimachine.com/friend/add`,
        headers: {
            'token': token,
            'email': email,
        },
        data: {
            ...obj
        }
    }
    return Axios(config);
}

export const getBaseAssets = () =>{
    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail');
    console.log('terminal sending', email, token);
    return Axios.post('https://terminal.apimachine.com/balance',
        {name: email, token: token}
    )
}


export const placeTrade = (exchangeId, paidId1, paidId2, amount, direction) =>{
    let email = localStorage.getItem('userEmail');
    let token = localStorage.getItem('idToken');
    console.log('place trade pre', exchangeId, paidId1, paidId2, amount, direction);
    return Axios.post(`https://terminal.apimachine.com/place/${exchangeId.toLowerCase()}/${paidId1}/${paidId2}/${amount}/${direction.toLowerCase()}/market/0`,
        {
            name: email,
            token: token
        }
    )
};

export const coinsFromVSA = (type) =>{
    return Axios.post(`https://comms.globalxchange.com/coin/vault/service/coins/get`,{
        app_code: type
    })
}

export const getMainBalance = (type, id, investment) => {
    return Axios.post(`https://comms.globalxchange.com/coin/vault/service/coins/get`, {
        app_code: type,
        profile_id: id,
        investmentCoin: investment
    })
}
export const getBTCAddress = (app) =>{
    let email = localStorage.getItem('userEmail');
    return Axios.post(`https://comms.globalxchange.com/coin/vault/service/coin/request/address`,{
        email: email,
        app_code: app
    })
}

export const requestForgotPassword = (email) =>{
    return Axios.post(`https://gxauth.apimachine.com/gx/user/password/forgot/request`,{
        email: email
    })
}
export const passwordReset = (obj) =>{
    return Axios.post(`https://gxauth.apimachine.com/gx/user/password/forgot/confirm`,{
        ...obj
    })
}
export const registerToApps =(app) =>{
    let email = localStorage.getItem('userEmail');  
    return Axios.post(`https://comms.globalxchange.com/gxb/apps/register/user`,{
        email: email,
        app_code: app,
        fromAppCreation: false
    })
}

export const subVaultTransfer = (obj) =>{
    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail'); 
    const key = "HUBQTVce7cUde4F";
    
    let tempObj = {
        ...obj,
        token: token,
        email: email,
        identifier: uuidv4()
    }
    let encoded = jwt.sign(tempObj, key, {algorithm: "HS512"})
    return Axios.post(`https://comms.globalxchange.com/coin/vault/service/transfer`,{
        data : encoded
    })

}
export const placeTradeAPI = (obj) =>{
    return Axios.post(`https://comms.globalxchange.com/coin/vault/service/encode/decode/data`,{
        data: {
            ...obj
        },
        encode: true
    })
}
export const decryptPlaceTrade = (code) =>{
    return Axios.post(`https://comms.globalxchange.com/coin/vault/service/trade/execute`,{
        data: code
    })
}
export const AddFiatAccount = (obj) =>{

    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail'); 
    let config = {
        method: 'put',
        url: `https://friends.apimachine.com/account/friend/addFiat`,
        headers: {
            'token': token,
            'email': email,
        },
        data: {
            ...obj
        }
    }

    return Axios(config)
}
export const AddUserFiatAccount = (obj) =>{

    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail'); 
    let config = {
        method: 'put',
        url: `https://friends.apimachine.com/account/user/addFiat`,
        headers: {
            'token': token,
            'email': email,
        },
        data: {
            ...obj
        }
    }

    return Axios(config)
}
export const addUserCryptoAccount = (obj) =>{

    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail'); 
    let config = {
        method: 'put',
        url: `https://friends.apimachine.com/account/user/addCrypto`,
        headers: {
            'token': token,
            'email': email,
        },
        data: {
            ...obj
        }
    }
    return Axios(config)
}

export const addCryptoAccount = (obj) =>{

    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail'); 
    let config = {
        method: 'put',
        url: `https://friends.apimachine.com/account/friend/addCrypto`,
        headers: {
            'token': token,
            'email': email,
        },
        data: {
            ...obj
        }
    }
    return Axios(config)
}
export const updateFiatAccount = (obj) =>{
    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail'); 
    let config = {
        method: 'put',
        url: `https://friends.apimachine.com/account/updateFiat`,
        headers: {
            'token': token,
            'email': email,
        },
        data: {
            ...obj
        }
    }
    return Axios(config)
}
export const updateCryptoAccount = (obj) =>{
    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail'); 
    let config = {
        method: 'put',
        url: `https://friends.apimachine.com/account/updateCrypto`,
        headers: {
            'token': token,
            'email': email,
        },
        data: {
            ...obj
        }
    }
    return Axios(config)
}
export const addNewUser = (obj) =>{
    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail'); 
    
    let config = {
        method: 'post',
        url: `https://friends.apimachine.com/user/newUser`,
        headers: {
            'token': token,
            'email': email,
        },
        data: {
            ...obj
        }
    }

    return Axios(config)
}

export const deleteAccount = ( id ) =>{
    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail'); 
    let config = {
        method: 'delete',
        url: `https://friends.apimachine.com/account/${id}`,
        headers: {
            'token': token,
            'email': email,
        }
    }
    return Axios(config);
}
export const getOfferingEquity = (data) =>{
    return Axios.post(`https://comms.globalxchange.com/coin/vault/service/coins/get`,{
        app_code: data.app_code,
        profile_id: data.profile_id
    })
}

export const simulateContract = (data) => {
    return Axios.post(`https://comms.globalxchange.com/coin/iced/contract/create`, {
        ...data
    })
}

export const mobileAppProcess = (obj) =>{
    return Axios.post(`https://comms.globalxchange.com/coin/vault/service/send/app/links/invite`,{
        ...obj
    })

}
export const blockcheckRequest = (obj) =>{
    return Axios.post(`https://comms.globalxchange.com/coin/vault/service/blockcheck/request/initiate`,{
        ...obj
    })
}

export const icedWithdrawStepOne = (obj) =>{
    return Axios.post(`https://comms.globalxchange.com/coin/iced/interest/withdraw`,{
        ...obj
    })
}
export const moneyWithdrawStepOne = (obj) =>{
    
    return Axios.post(`https://comms.globalxchange.com/coin/vault/service/user/app/interest/withdraw`,{
        ...obj
    })
}
export const controlledByMeOne = (obj) =>{
    return Axios.post(`https://comms.globalxchange.com/coin/vault/service/encode/decode/data`,{
        ...obj
    })

}
export const controlledByMeTwo = (obj) =>{
    return Axios.post(`https://comms.globalxchange.com/coin/vault/service/trade/execute`,{
        ...obj
    })

}
export const addContact = (obj) =>{
    return Axios.post(`https://comms.globalxchange.com/coin/vault/service/blockcheck/contact/add`,{
        ...obj
    })
}
export const getUserForChat = () =>{
    let email = localStorage.getItem('userEmail');
    let token = localStorage.getItem('idToken');
    return axios.post('https://chatsapi.globalxchange.io/gxchat/get_user', {
         email,
         token
    });
};
export const fetchBrainVideoLink = (id) =>{
    return Axios.post('https://vod-backend.globalxchange.io/get_user_profiled_video_stream_link',{
        video_id: id
    })
}
export const submitHash = (coin, hash) =>{
    let email = localStorage.getItem('userEmail');
    let appCode = localStorage.getItem('appCode');
    let obj;
    let link;
    if (coin === "TRX") {
        link = `https://comms.globalxchange.com/coin/vault/service/deposit/trx/external/request`;
        obj = {
            email: email,
            app_code: appCode,
            hash: hash
        }
    } else {
        link = `https://comms.globalxchange.com/coin/vault/service/deposit/eth/coins/request`;
        obj = {
            app_code: appCode,
            email: email,
            coin: coin,
            txn_hash: hash
        }

    }
    var config = {
        method: 'post',
        url: link,
        data: obj
    };
    return Axios(config);
}

export const updateUsername = (username) =>{
    let email = localStorage.getItem('userEmail');
    let obj ={
        email: email,
        field: "username",
        value: username,
        accessToken: localStorage.getItem('accessToken')
    }
    let config = {
        method: 'post',
        url: `https://comms.globalxchange.com/user/details/edit`,
        data: obj
    };
    return Axios(config);

}

export const fundAppOwnershipCapital = (app, amt) =>{
    let email = localStorage.getItem('userEmail');
    let token = localStorage.getItem('idToken');
    let appCode = localStorage.getItem('appCode');

    let obj = {
        email: email,
        token: token,
        user_app_code: appCode,
        to_app_code: app,
        amount: amt
    }
    let config = {
        method: 'post',
        url: `https://comms.globalxchange.com/coin/vault/service/fund/app/capital`,
        data: obj
    };
    return Axios(config);
}

export const uploadDocuments = (obj) =>{
    return Axios.post(`https://comms.globalxchange.com/coin/fiat/update_requests`,{
        ...obj
    })

}
export const updateDestinations = (obj) =>{
    let config = {
        method: 'post',
        url: `https://comms.globalxchange.com/coin/iced/set/user/interest/payout/destination`,

        data: {
            ...obj
        }
    }
    return Axios(config);

}
export const executeInvestmentPaths = (obj) =>{
    let config = {
        method: 'post',
        url: `https://comms.globalxchange.com/coin/investment/path/execute`,
        data: {
            ...obj
        }
    }
    return Axios(config);
    
}


export const getTronAddress = (obj) => {
    let config = {
        method: 'post',
        url: `https://comms.globalxchange.com/coin/vault/service/crypto/address/request`,
        data: {
            ...obj
        }
    }
    return Axios(config);

}
export const preRegisterLogin = (obj) =>{
    return axios.post(`https://gxauth.apimachine.com/gx/user/login`,{...obj})
}
export const createBlockCheckEmail = (obj) =>{
    let config = {
        method: 'post',
        url: `https://api.mailslurp.com/inboxes?description=new&emailAddress=${obj.email}&favourite=false&name=${obj.name}&tags=${localStorage.getItem("appCode")}&expiresAt=2025-03-09T09:31:05.677Z`,
        headers: {
            'x-api-key': "86cee2f39d56b3b5a6b2e4c827cc1382d1be6bad16a9d35cd0e659ef9272d02c"
        },
    }
    return Axios(config);

}
export const sendEmailToNewUser = (obj) =>{
    let config = {
        method: 'post',
        url: `https://gxauth.apimachine.com/gx/user/signup`,
        data: {
            ...obj
        }
    }
    return Axios(config);

}

export const registerNewUser = (obj) =>{
    let config = {
        method: 'post',
        url: `https://gxauth.apimachine.com/gx/user/confirm`,
        data: {
            ...obj
        }
    }
    return Axios(config);

}
export const resendCodeNewUser = (email) =>{
    let config = {
        method: 'post',
        url: `https://gxauth.apimachine.com/gx/user/confirm/resend`,
        data: {
            email: email
        }
    }
    return Axios(config);
}
export const updateUserBio = (data) =>{
    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail');
    let config = {
        method: 'post',
        url: `https://comms.globalxchange.com/user/details/update`,
        data: {
            email: email,
            token: token,
            ...data
        }
    }
    return Axios(config);
}
export const findAppId = (app) => {
    let token = localStorage.getItem('idToken');
    let email = localStorage.getItem('userEmail');
    let config = {
        method: 'post',
        headers: {
            email: email,
            token: token
        },
        url: `https://testchatsioapi.globalxchange.io/get_application`,
        data: {
            code: app
        }
    }
    return Axios(config);

}